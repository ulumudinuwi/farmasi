<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['master_pemeriksaan_title'] = 'Master Pemeriksaan Laboratorium';
$lang['master_pemeriksaan_icon'] = 'icon-certificate';
$lang['master_pemeriksaan_add'] = 'Tambah Pemeriksaan Laboratorium';
$lang['master_pemeriksaan_edit'] = 'Edit Pemeriksaan Laboratorium';

$lang['section_data'] = 'Data';
$lang['section_item'] = 'Rincian Pemeriksaan';
$lang['section_nilai_normal'] = 'Nilai Normal';
$lang['section_nilai_normal_umur'] = 'Kriteria Usia';
$lang['section_obat'] = 'Penggunaan Obat/BMHP';

$lang['field_kode'] = 'Kode';
$lang['field_nama'] = 'Nama';
$lang['field_satuan'] = 'Satuan';
$lang['field_nilai_normal'] = 'Nilai Rujukan';
$lang['field_jenis'] = 'Kelompok / Rincian';
$lang['field_jenis_kelompok'] = 'Kelompok';
$lang['field_jenis_rincian'] = 'Rincian';
$lang['field_parent_id'] = 'Parent';
$lang['field_deskripsi'] = 'Deskripsi';
$lang['field_status'] = 'Status';
$lang['field_quantity'] = 'Qty';
$lang['field_action'] = '&nbsp;';
$lang['field_obat'] = 'Obat/BMHP';

// NILAI NORMAL
$lang['field_datatype'] = 'Type';
$lang['field_value'] = 'Value';
$lang['field_label'] = 'Label';
$lang['field_jenis_kelamin'] = 'Jenis Kelamin';

$lang['field_umur_min_flag'] = 'Usia Minimum';
$lang['field_umur_tahun_min'] = 'Usia (Th) Minimum';
$lang['field_umur_bulan_min'] = 'Usia (Bln) Minimum';
$lang['field_umur_hari_min'] = 'Usia (Hr) Minimum';
$lang['field_umur_max_flag'] = 'Usia Maximum';
$lang['field_umur_tahun_max'] = 'Usia (Th) Maximum';
$lang['field_umur_bulan_max'] = 'Usia (Bln) Maximum';
$lang['field_umur_hari_max'] = 'Usia (Hr) Maximum';

// VALUES
$lang['value_range'] = 'Rentang Nilai';
$lang['value_boolean'] = 'Positif/Negatif';
$lang['value_text'] = 'Text';
$lang['value_lt'] = '&lt; (Kurang Dari)';
$lang['value_gt'] = '&gt; (Lebih Dari)';
$lang['value_lte'] = '&lt;= (Kurang Dari Sama Dengan)';
$lang['value_gte'] = '&gt;= (Lebih Dari Sama Dengan)';
$lang['value_true'] = 'Positif';
$lang['value_false'] = 'Negatif';
$lang['value_jenis_kelamin_1'] = 'Laki-Laki';
$lang['value_jenis_kelamin_2'] = 'Perempuan';
$lang['value_yes'] = 'Ya';
$lang['value_no'] = 'Tidak';

// MODALS
$lang['modal_lookup_obat_title'] = 'Pilih Obat/BMHP';