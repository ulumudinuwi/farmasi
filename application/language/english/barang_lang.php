<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['kelompok_label'] = 'Group';
$lang['pabrik_label'] = 'Principal';
$lang['vendor_label'] = 'Distributor';