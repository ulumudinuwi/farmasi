<?php
/**
 * PSR-4 Autoloader for Codeiginiter 3 application
 * 
 * @author  
 * @version 
 * @see    
 */
class Psr4Autoload
{
    /**
     * @var string Nampsapce prefix refered to application root
     */
    const DEFAULT_PREFIX = "App";
    
    /**
     * Register Autoloader
     * 
     * @param string $prefix PSR-4 namespace prefix
     */
    public static function register($prefix=null)
    {
        $prefix = ($prefix) ? (string)$prefix : self::DEFAULT_PREFIX;
        
        spl_autoload_register(function ($classname) use ($prefix) {
            // Prefix check
            if (strpos(ucfirst($classname), "{$prefix}\\")===0) {
                // Locate class relative path
                $classname = str_replace("{$prefix}\\", "", $classname);
                $classname = ltrim($classname, '\\');
                $arr = explode("\\", $classname);
                
                $classname = array_pop($arr);
                
                $arr = implode("\\", $arr);
                $arr = strtolower($arr);
                $arr = explode("\\", $arr);
                if(is_array($arr)){
                    array_push($arr, $classname);
                    $arr = implode("\\", $arr);
                }else{
                    $arr = $arr."\\".$classname;
                }
                
                $classname = $arr;
                $filepath = APPPATH.  str_replace('\\', DIRECTORY_SEPARATOR, ltrim($classname, '\\')) . '.php';
                
                if (file_exists($filepath)) {
                    require $filepath;
                }
            }
        });
    }
}
