<?php

defined('BASEPATH') OR exit('No direct script access allowed');

abstract class LisAbstract {
	/**
	 * PDO Connection | Handler
	 */
	protected $conn;

	protected $logsInfo = array();
	protected $logsError = array();

	protected $isConnected = false;

	protected $config = array();

	/**
	 * Setting Laboratorium
	 */
	protected $settings = array();

	/**
	 * Method to send data to LIS
	 */
	public abstract function send($data);

	/**
	 * Method to Delete Rows/File from LIS Result
	 */
	public abstract function clean($lis_id);

	/**
	 * Method to get Result Header
	 */
	public abstract function getHeader($ono);

	/**
	 * Method to get Result Details
	 */
	public abstract function getDetail($ono, $test_code);

	/**
	 * Method to cancel Order
	 */
	public abstract function cancel($ono);

	/**
	 * Method to connect to LIS Server
	 */
	public abstract function connect();

	/**
	 * Method to disconnect from LIS Server
	 */
	public abstract function disconnect();

	/** 
	 * Method to check connection to Server
	 */
	public abstract function checkConnection();

	/**
	 * Method to check if the connection is made
	 */
	public function isConnected() {
		return $this->isConnected;
	}

	/**
	 * Method to set Settings Laboratorium
	 */
	public function setSettings($settings) {
		$this->settings = $settings;
	}

	/** 
	 * $data => Data yang didapat dari Laboratorium_model
	 * @return data yang digunakan untuk di send ke LIS
	 */
	public function parseData($data) {
		$source = $data->layanan_id.'^'.$data->layanan;
		if ($data->layanan_jenis == 3) {
			$source = $data->ruang_kode.'^'.$data->ruang;
		}

		$kodeChunk = explode('-', $data->kode_transaksi);
	    $temp = [];
	    foreach ($data->details as $detail) {
	      $temp[] = (isset($this->settings['kode_lis']) && $this->settings['kode_lis']) ? $detail->kode_lis : $detail->kode;
	    }
	    $order_testid = implode('~', $temp);
	    $data = array(
	      'message_id' => 'O01',
	      'message_dt' => date('YmdHis'),
	      'version' => '2.3',
	      'order_control' => 'NW',
	      'pid' => $data->no_rekam_medis,
	      'apid' => '',
	      'pname' => $data->nama,
	      'address' => $data->alamat,
	      'ptype' => $data->layanan_jenis == 1 || $data->layanan_jenis == 2 ? 'OP' : 'IP',
	      'birth_dt' => date('YmdHis', strtotime($data->tgl_lahir)),
	      'sex' => $data->jenis_kelamin == 0 ? '2' : '1',
	      'ono' => $data->kode_transaksi,
	      'lno' => '', #$kodeChunk[count($kodeChunk) -1], # No Antrean
	      'request_dt' => date('YmdHis'),
	      'source' => $source,
	      'clinician' => $data->dokter_perujuk_kode.'^'.$data->dokter,
	      'room_no' => $data->bed,
	      'priority' => 'R',
	      'p_status' => '0',
	      'comment' => 'SIMRS iMedis',
	      'visitno' => '',
	      'order_testid' => $order_testid, 
	      'status' => '0',
	      'read_status' => '',
	      'order_status' => date('Y-m-d H:i:s'),
	    );

	    return $data;
	}
}