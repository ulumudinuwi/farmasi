<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 10/16/16
 * Time: 17:18
 */
class File_uploader
{

    private $folder_path = 'uploads/img/pegawai';

    private $JPEG = 'jpeg';
    private $PNG = 'png';
    private $JPG = 'jpg';
    private $DOCX = 'docx';
    private $DOCS = 'docs';
    private $PDF = 'pdf';

    private $MAX_SIZE = 1000000000;

    private $IMAGE_FORMAT_EXCEPTION_MESSAGE = 'File harus berformat png/jpg/jpeg untuk ';
    private $DOCS_FORMAT_EXCEPTION_MESSAGE = 'File harus DOCS/DOCX/PDF untuk ';
    private $SIZE_EXCEED_LIMIT_EXCEPTION_MESSAGE = 'File melebih dari 1 Gb';
    private $UPLOAD_FAILED_EXCEPTION_MESSAGE = 'Gagal Upload File';

    public function upload($file_index, $new_file_name = null, $upload_path = null)
    {

        if (!empty($upload_path)) $this->folder_path = $upload_path;

        $ext = pathinfo($_FILES[$file_index]['name']);
        $size = $_FILES[$file_index]['size'];

        if ($size > $this->MAX_SIZE) { // 1000000000 byte = 1 gb
            return new Exception($this->SIZE_EXCEED_LIMIT_EXCEPTION_MESSAGE . $file_index);
        }

        if (isset($_FILES[$file_index]) && $_FILES[$file_index]['error'] == 0) {
            $temp = explode(".", $_FILES[$file_index]["name"]);
            $newfilename = ($new_file_name != null) ? $new_file_name : substr(md5(time()), 0, 10) . '.' . end($temp);
            move_uploaded_file($_FILES[$file_index]['tmp_name'], $this->folder_path . $newfilename);
            return [
                'file' => $this->folder_path . $newfilename
            ];

        } else {
            return new Exception($this->UPLOAD_FAILED_EXCEPTION_MESSAGE . $file_index);

        }


    }

    public function upload_image($file_index, $new_file_name = null, $upload_path = null)
    {

        $this->folder_path = 'uploads/img/pasien';

        $ext = pathinfo($_FILES['image']['name']);
        $size = $_FILES['image']['size'];

        if ($ext['extension'] == $this->PNG || $ext['extension'] == $this->JPG || $ext['extension'] == $this->JPEG) {
            if ($size > $this->MAX_SIZE) { // 1000000000 byte = 1 gb
                return new Exception($this->SIZE_EXCEED_LIMIT_EXCEPTION_MESSAGE . $file_index);
            }

            if (isset($_FILES['image']) && $_FILES['image']['error'] == 0) {
                $temp = explode(".", $_FILES['image']["name"]);
                $newfilename = substr(md5(time()), 0, 10) . '.' . end($temp);
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_path . $newfilename);
                return [
                    'file' => $this->folder_path . ($new_file_name !== null) ? $new_file_name : $newfilename
                ];

            } else {
                return new Exception($this->UPLOAD_FAILED_EXCEPTION_MESSAGE . $file_index);

            }

        } else {
            return new Exception($this->IMAGE_FORMAT_EXCEPTION_MESSAGE);
        }

    }

    public function upload_docs($file_index)
    {

        $ext = pathinfo($_FILES[$file_index]['name']);
        $size = $_FILES[$file_index]['size'];
        if ($ext['extension'] == $this->DOCS || $ext['extension'] == $this->DOCX || $ext['extension'] == $this->PDF) {

            if ($size > $this->MAX_SIZE) { // 1000000000 byte = 1 gb
                return new Exception($this->SIZE_EXCEED_LIMIT_EXCEPTION_MESSAGE . $file_index);
            }

            if (isset($_FILES[$file_index]) && $_FILES[$file_index]['error'] == 0) {
                $temp = explode(".", $_FILES[$file_index]["name"]);
                $newfilename = substr(md5(time()), 0, 10) . '.' . end($temp);
                move_uploaded_file($_FILES[$file_index]['tmp_name'], $this->folder_path . $newfilename);
                return [
                    'file' => $this->folder_path . $newfilename
                ];

            } else {
                return new Exception($this->UPLOAD_FAILED_EXCEPTION_MESSAGE . $file_index);
            }
        } else {
            return new Exception($this->DOCS_FORMAT_EXCEPTION_MESSAGE);
        }


    }

    public function download()
    {
        $file = $this->input->get('file');
        if (!$file) {
            throw new Exception('Cannot find file');
        }

        $path = base64_decode($file);

        if (file_exists($path)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            header('Content-Type: ' . finfo_file($finfo, $path));
            $finfo = finfo_open(FILEINFO_MIME_ENCODING);
            header('Content-Transfer-Encoding: ' . finfo_file($finfo, $path));
            header('Content-disposition: attachment; filename="' . basename($path) . '"');
            readfile($path);
        } else {
            throw new Exception('File not found');
        }

    }
}