<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . '/lis/LisAbstract.php';

class LisFile extends LisAbstract {

	private $CI;

	private $dirSeparator = DIRECTORY_SEPARATOR;
	private $newLine = PHP_EOL;

	private $mode = 'samba';

	private $resultTemp = null;

	// File Result
	private $resultfile = null;

	public function __construct($config = array()) {
		$this->CI = get_instance();

		$default = array(
			'host' => '127.0.0.1', # HOST or ABSOLUTE PATH
			'port' => '80',
			'in_path' => 'in',
			'out_path' => 'out',
			'temppath' => FCPATH.'/temp/lis',
			'user' => 'root',
			'password' => '',
		);

		$params = ['host', 'port', 'user', 'password', 'in_path', 'out_path', 'temppath'];
		foreach ($params as $param) {
			$this->config[$param] = array_key_exists($param, $config) ? $config[$param] : $default[$param];
		}

		$this->mode = preg_match('/(\/\/|\\\\)/', $this->config['host']) ? 'samba' : 'file';

		$this->dirSeparator = ( stripos($this->config['host'], '\\\\') >= 0 && is_numeric($this->config['host']) ) ? '\\\\' : DIRECTORY_SEPARATOR;
		$this->newLine = ( stripos($this->config['host'], '\\\\') >= 0 && is_numeric($this->config['host']) ) ? '\n' : PHP_EOL;

		if ($this->mode == 'samba') {
			$smbInConfig = array(
				'service' => $this->config['host'].$this->dirSeparator.$this->config['in_path'],
				'username' => $this->config['user'],
				'password' => $this->config['password'],
			);

			$smbOutConfig = array(
				'service' => $this->config['host'].$this->dirSeparator.$this->config['out_path'],
				'username' => $this->config['user'],
				'password' => $this->config['password'],
			);

			$this->CI->load->library('smbclient', $smbInConfig, 'smbIn');
			$this->CI->load->library('smbclient', $smbOutConfig, 'smbOut');
			$this->smbIn = $this->CI->smbIn;
			$this->smbOut = $this->CI->smbOut;
		} else {
			$this->folderIn = $this->config['host'].$this->dirSeparator.$this->config['in_path'];
			$this->folderOut = $this->config['host'].$this->dirSeparator.$this->config['out_path'];
			// ?????
		}

		$this->connect();
	}

	/** 
	 * Send Data to LIS
	 */
	public function send($data) {
		if ($this->mode == 'samba') {
			$filename = $data['ono'].".txt";
			$tempfile = $this->getTempFile($this->dirSeparator."in".$this->dirSeparator.$filename);
			$file = fopen($tempfile, 'w+');

			$text = $this->getText($data);

			fwrite($file, $text);

			fclose($file);

			if (! file_exists($tempfile)) return false;

			$smbResult = $this->smbIn->put($tempfile, $data['ono'].".txt");
			if ($smbResult) {
				// unlink($tempfile);
				return true;
			}

			return false;
		} else {
			$filename = $this->folderIn.$this->dirSeparator.$data['ono'].".txt";
			$filename_backup = $this->folderIn.'_backup'.$this->dirSeparator.$data['ono']."__".uniqid().".txt";

			$file = fopen($filename, 'w+');
			$filebackup = fopen($filename_backup, 'w+');

			$text = $this->getText($data);

			fwrite($file, $text);
			fwrite($filebackup, $text);

			fclose($file);
			fclose($filebackup);

			if (! file_exists($filename)) return false;
			return true;
		}
	}

	/**
	 * Clean File
	 */
	public function clean($lis_id) {
		if ($this->mode == 'samba') {
			$filename = $lis_id;
			$tempfile = $this->getTempFile('out'.$this->dirSeparator.$lis_id);

			@unlink($tempfile);
			return $this->smbOut->del($filename);
		} else {
			$filename = $lis_id;
			@unlink($filename);
		}
	}

	/**
	 * Get Data from LIS
	 */
	public function getHeader($ono) {
		return null;
	}

	public function getDetail($ono, $test_code) {
		if ($this->mode == 'samba') {
			$tempOutDir = $this->config['temppath'].$this->dirSeparator.'out';
			$files = glob($tempOutDir.$this->dirSeparator.'*.R01');
			$existingFiles = array_map(function ($item) {
				$chunk = explode('/', $item);
				return $chunk[count($chunk) -1];
			}, $files);


			$files = $this->smbOut->dir('');

			# TODO

			// rsort($files);
			$files = array_reverse($files);
			$filename = null;


			if (! $this->resultTemp) {
				foreach ($files as $file) {
					if (array_search($file['filename'], $existingFiles) === FALSE ) {
						if ( ($file['filename'] == '.' || $file['filename'] == '..') ) continue;

						if (preg_match('/(\.R01)/', $file['filename']) == 0) continue;

						$this->smbOut->get($file['filename'], $this->config['temppath'].$this->dirSeparator.'out'.$this->dirSeparator.$file['filename']);
					}

					$data = $this->parseText($this->config['temppath'].$this->dirSeparator.'out'.$this->dirSeparator.$file['filename']);

					$order_testid = explode('^', $data->order_testid);
					$order_testcode = '';

					if (count($order_testid)) {
						$order_testcode = $order_testid[0];
					}

					if ($data->ono == $ono && $test_code == $order_testcode) {
						$filename = $file['filename'];
						$this->resultTemp = $data;
						break;
					}

				}
			}

			if ($this->resultTemp) {
				$this->resultfile = $filename;
				$obx = $this->resultTemp->obx;
				$this->resultTemp = null;
				return $obx;
			}
			
			return null;
		} else {
			$files = glob($this->folderOut.$this->dirSeparator.'*.R01');
			// rsort($files);
			$files = array_reverse($files);
			$filename = null;
			
			if (! $this->resultTemp) {
				foreach ($files as $file) {
					// echo $ono, $test_code, PHP_EOL;
					$data = $this->parseText($file);

					$order_testid = explode('^', $data->order_testid);
					$order_testcode = '';

					if (count($order_testid)) {
						$order_testcode = $order_testid[0];
					}

					if ($data->ono == $ono && $test_code == $order_testcode) {
						$filename = $file;
						$this->resultTemp = $data;
						break;
					}
				}
			}

			if ($this->resultTemp) {
				$this->resultfile = $filename;
				$obx = $this->resultTemp->obx;
				$this->resultTemp = null;
				return $obx;
			}

			return null;
		}
	}

	public function cancel($ono) {
		// TODO
	}

	public function connect() {
		if ($this->mode == 'samba') {
		    $this->isConnected = $this->checkConnection() || isset($this->smbIn);
		} else {
			$this->isConnected = is_dir($this->config['host']);
		}
	}

	public function disconnect() {
		$this->isConnected = false;
	}

	public function checkConnection() {
		$this->logsInfo[] = "Checking Connection to Server.";
	    exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($this->config['host'])), $res, $rval);
	    return $rval === 0;
	}

	public function getResultId() {
		return $this->resultfile;
	}




	private function getResult($test_code, $obx = array()) {
		foreach ($obx as $row) {
			if ($row->test_code == $test_code) {
				return $row;
			}
		}
		return null;
	}

	/** 
	 * Get send data as text
	 */
	private function getText($data) {
		$content = "";

		foreach ($data as $key => $val) {
			$key = trim($key);
			if ($key == 'message_id') $content .= '[MSH]'.$this->newLine;
			if ($key == 'order_control') $content .= '[OBR]'.$this->newLine;
			$content .= $key."=".$val.$this->newLine;
		}

		return $content;
	}

	/** 
	 * Get Return Data as an Array
	 */
	private function parseText($filename) {
		$obj = new stdClass();

		$segment = 1;

		$obxFields = ['test_code', 'test_name', 'data_type', 'result_value', 'unit', 'flag', 'reference_range', 'status', 'test_comment', 'validated_by', 'validated_on'];

		$f = fopen($filename, 'r');
		while (!feof($f)) {
			$line = fgets($f);

			if (empty(trim($line))) continue;

			if (trim($line) == '[MSH]') { 
				$segment = 1;
				continue;
			}
			if (trim($line) == '[OBR]') { 
				$segment = 2;
				continue;
			}
			if (trim($line) == '[OBX]') { 
				$obj->obx = array();
				$segment = 3;
				continue;
			}

			if ($segment === 1) {

			} elseif ($segment === 2) {
				$lA = explode('=', $line);
				$key = $lA[0];
				$obj->$key = trim($lA[1]);
			} elseif ($segment === 3) {
				$obx = new stdClass();
				$lA = explode('=', $line);
				$obxA = explode('|', $lA[1]);
				foreach ($obxFields as $i => $field) {
					if (array_key_exists($i, $obxA)) {
						$obx->$field = trim($obxA[$i]);
					}
				}
				$obj->obx[] = $obx;
			}
		}
		fclose($f);

		return $obj;
	}

	private function getTempFile($filename) {
		return $this->config['temppath'].'/'.$filename;
	}
}