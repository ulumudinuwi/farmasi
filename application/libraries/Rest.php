<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use GuzzleHttp\Client;


/**
 * Guzzle Handler.
 *
 * @package CodeIgniter
 * @subpackage Libraries
 * @category Libraries
 * @author Rizki Fauzi
 */

/**
 * Config BPJS jangan di hapus
 */

class Rest
{

    var $CI, $base_url, $api_key, $bpjs;

    function __construct()
    {
        // Get the instance
        $this->CI = &get_instance();
        $this->base_url = $this->CI->config->item('api_base_url');
        $this->api_key = $this->CI->config->item('api_token');
        $this->bpjs = $this->CI->config->item('bpjs');
    }

    function get($url, $data = array())
    {
        $client = new Client();
        try {
            $response = $client->request('GET', $this->base_url . $url . $this->createQueryString($data), [
                'headers' => ['token' => $this->api_key]
            ]);
            //guzzle response
            /* echo $response->getStatusCode(); // 200
             *  echo $response->getReasonPhrase(); // OK
             *  echo $response->getProtocolVersion(); // 1.1
             */
            return $response->getBody();
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return $responseBodyAsString;
        }
    }

    function post($url, $data = array())
    {
        $jsonData = json_encode($data);
        $client = new Client();
        try {
            $response = $client->request('POST', $this->base_url . $url, [
                'headers' => ['token' => $this->api_key],
                'body' => $jsonData
            ]);
            //guzzle response
            /* echo $response->getStatusCode(); // 200
             *  echo $response->getReasonPhrase(); // OK
             *  echo $response->getProtocolVersion(); // 1.1
             */
            return $response->getBody();
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return $responseBodyAsString;
        }
    }

    function createQueryString($data = array())
    {
        $queryString = '';
        if (!empty($data)) {
            $queryString = http_build_query($data);
        }

        $cekQueryString = !empty($queryString) ? '?' . $queryString : '';
        return $cekQueryString;
    }

    function update($url, $data = array())
    {
        $jsonData = json_encode($data);
        $client = new Client();
        try {
            $response = $client->request('POST', $this->base_url . $url, [
                'headers' => ['token' => $this->api_key],
                'body' => $jsonData
            ]);
            //guzzle response
            /* echo $response->getStatusCode(); // 200
             *  echo $response->getReasonPhrase(); // OK
             *  echo $response->getProtocolVersion(); // 1.1
             */
            return $response->getBody();
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return $responseBodyAsString;
        }
    }

    function delete($url, $data = array())
    {
        $client = new Client();
        try {
            $response = $client->request('DELETE', $this->base_url . $url . $this->createQueryString($data), [
                'headers' => ['token' => $this->api_key]
            ]);
            //guzzle response
            /* echo $response->getStatusCode(); // 200
             *  echo $response->getReasonPhrase(); // OK
             *  echo $response->getProtocolVersion(); // 1.1
             */
            return $response->getBody();
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return $responseBodyAsString;
        }
    }

    function bpjs()
    {

        date_default_timezone_set('UTC');
        $data = $this->bpjs['cons_id'];
        $secretKey = $this->bpjs['secret_key'];
        $tStamp = strval(time() - strtotime('1970-01-01 00:00:00'));
        $signature = hash_hmac('sha256', $data . "&" . $tStamp, $secretKey, true);
        $encodedSignature = base64_encode($signature);
        return [
            'consid' => $data,
            'timestamp' => $tStamp,
            'signature' => $encodedSignature
        ];
    }

    function curl($url, $method, $headers, $body = "", $port = 80)
    {
        $curl = curl_init();
        $opt = array(
            CURLOPT_PORT => $port,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $headers
        );
        if ($method == 'POST') {
            $opt = array(
                CURLOPT_PORT => $port,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $body
            );
        } else if ($method == 'PUT') {
            var_dump("put");
            $opt = array(
                CURLOPT_PORT => $port,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => http_build_query($body)
            );
        } else if ($method == 'DELETE') {
            $opt = array(
                CURLOPT_PORT => $port,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => $body
            );
        } else {
            $opt = array(
                CURLOPT_PORT => $port,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => $headers
            );
        }

        curl_setopt_array($curl, $opt);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if (!empty($err)) {
            return $err;
        } else {
            return $response;
        }
    }
}