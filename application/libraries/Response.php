<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @package CodeIgniter
 * @subpackage Libraries
 * @category Libraries
 * @author Ari Ardiansyah
 */

/**
 * Sample Rest Inspire Laravel Framework  
 */

class Response {
    public function json($response,$status)
    {
        // $status = $status || 200;
        // $response = $response || null;
        header("HTTP/1.1 ".$status);
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);
    }
}