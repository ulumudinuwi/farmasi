<div id="setting_modal" class="modal fade" data-backdrop="static" data-keyboard="false">>
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" id="setting_form">
				<div class="modal-header bg-primary">
					<h4>Setting Stock</h4>
				</div>
				<div class="modal-body bg-light lt">
					<div class="row m-t-md">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-4">Barang</label>
										<div class="col-md-8">
											<div class="form-control-static" id="setting_barang"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Satuan</label>
										<div class="col-md-8">
											<div class="form-control-static" id="setting_satuan"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4 input-required">Maximum</label>
										<div class="col-md-8">
											<input type="text" class="form-control text-right input-bulat" id="setting_maximum" name="setting_maximum" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4 input-required">Reorder</label>
										<div class="col-md-8">
											<input type="text" class="form-control text-right input-bulat" id="setting_reorder" name="setting_reorder" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4 input-required">Minimum</label>
										<div class="col-md-8">
											<input type="text" class="form-control text-right input-bulat" id="setting_minimum" name="setting_minimum" value="0">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer m-t-none">
					<button type="submit" id="setting_simpan" class="btn btn-primary">Simpan</button>
					<button type="button" id="setting_batal" class="btn btn-default" data-dismiss="modal">Batal</button>
				</div>
				<div>
					<input type="hidden" id="setting_id" name="setting_id" value="0" />
				</div>
			</form>
		</div>
	</div>
</div>