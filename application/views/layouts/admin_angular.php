<?php
$name = '';
$role = '';
if ($this->auth->loggedin()) {
    $name = $auth_user['first_name'] . ' ' . $auth_user['last_name'];
    $role = $auth_user['role_name'];
    if (strlen(trim($name)) == 0) {
        $name = $auth_user['username'];
    }
}
?>
<!DOCTYPE html>
<html lang="en" ng-app="rsbt">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $template['metas']; ?>

    <title><?php echo $template['title']; ?></title>

    <link rel="icon" href="<?php echo assets_url('img/favicon.png') ?>" type="image/png">
    <link rel="stylesheet" href="<?php echo bower_url('angular-toastr/dist/angular-toastr.css') ?>">
    <!--    <link rel="stylesheet" href="--><?php //echo bower_url('angular-bootstrap/ui-bootstrap-csp.css') ?><!--">-->
    <link rel="stylesheet" href="<?php echo bower_url('leaflet/dist/leaflet.css') ?>">
    <link rel="stylesheet" href="<?php echo bower_url('animate.css/animate.min.css') ?>">



    <!-- Global stylesheets -->
    <?php echo $template['css']; ?>
    <?php $URI = site_url() . "index.php" ?>
    <!-- Global stylesheets -->

    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/selects/select2.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/uniform.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switchery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switch.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('moment/min/moment.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('numeraljs/min/numeral.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('lodash/dist/lodash.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('jquery.inputmask/dist/jquery.inputmask.bundle.js') ?>"></script>



    <script>
        base_url = '<?php echo site_url() ?>';
        assets_url = '<?php echo assets_url()?>';
        moment.locale('id');
        var env = '<?php echo ENVIRONMENT;?>';

        if(env == 'production'){
            console = console || {};
            console.log = function(){};
        }

    </script>
    <!-- /core JS files -->

    <!-- /theme JS files -->
    <?php echo $template['js_header']; ?>


    <?php
    if (isset($template['script']) && $template['script'] != '')
        $this->load->view($template['script']);
    ?>
    <script type="text/javascript" src="<?php echo js_url('core/app.js') ?>"></script>


</head>

<body class="navbar-top has-detached-right" data-spy="scroll" data-target=".sidebar-detached" data-offset="100">

<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo site_url() ?>"><img
                src="<?php echo assets_url('img/imedis_logo.png') ?>" style="margin-top: -10px;height:40px"></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>


    </div>
    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">


            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo image_url('placeholder.jpg') ?>" alt="">
                    <span><?php echo $name ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="<?php echo site_url('auth/user/profile?redirect=' . urlencode(current_url())); ?>"><i
                                class="icon-user-plus"></i> My profile</a></li>
                    <li><a ng-click="switchShift(false)" ng-show="isBuka"><i class="icon-close2"></i> Tutup Shift</a>
                    </li>
                    <li><a ng-click="switchShift(true)" ng-show="!isBuka"><i class="glyphicon glyphicon-open"></i> Buka
                            Shift</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo site_url('auth/logout') ?>"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main sidebar-fixed">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="<?php echo image_url('placeholder.jpg') ?>"
                                                                class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold"><?php echo $name ?></span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-vcard text-size-small"></i> &nbsp;<?php echo $role ?>
                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <?php
                            function set_active($menus, $curr_uri, $acl)
                            {
                                foreach($menus as $index => $menu) {
                                    $is_active = false;
                                    $is_allowed = false;
                                    $has_children = isset($menu['children']) and is_array($menu['children']);
                                    if ($has_children) {
                                        $menus[$index]['children'] = set_active($menus[$index]['children'], $curr_uri, $acl);
                                        foreach($menus[$index]['children'] as $menu_item) {
                                            if ($menu_item['is_active']) {
                                                $is_active = $is_active || true;
                                            }
                                            if ($menu_item['is_allowed']) {
                                                $is_allowed = $is_allowed || true;
                                            }
                                        }
                                    } else {
                                        $is_active = strpos($curr_uri, $menu['uri']) === 0;
                                        $is_allowed = !isset($menu['uri']) || $acl->is_allowed($menu['uri']);
                                    }
                                    $menus[$index]['is_active'] = $is_active;
                                    $menus[$index]['is_allowed'] = $is_allowed;
                                }
                                return $menus;
                            }

                            function display_menu_item($menu, $curr_uri)
                            {
                                if (empty($curr_uri)) $curr_uri = 'home';
                                $is_active = (isset($menu['is_active']) && $menu['is_active']);
                                $is_allowed = (isset($menu['is_allowed']) && $menu['is_allowed']);
                                $has_children = isset($menu['children']) and is_array($menu['children']);
                                if ($is_allowed) {
                                    echo '<li' . ($is_active ? ' class="active"' : '') . '>';
                                    echo '<a href="' . ((isset($menu['uri']) and !empty($menu['uri'])) ? site_url($menu['uri']) : '#') . '"' . ($has_children ? ' class="has-ul"' : '') . '>';
                                    if (isset($menu['icon']) && !empty($menu['icon']))
                                        echo '<i class="' . $menu['icon'] . '"></i>';
                                    echo '<span>' . $menu['title'] . '</span>';
                                    echo '</a>';
                                    if ($has_children) {
                                        echo '<ul' . ($is_active ? '' : ' class="hidden-ul"') . '>';
                                        foreach($menu['children'] as $menu_item) {
                                            display_menu_item($menu_item, $curr_uri);
                                        }
                                        echo '</ul>';
                                    }
                                }
                            }

                            $curr_uri = $this->uri->uri_string();
                            if (empty($curr_uri)) $curr_uri = 'home';
                            $this->load->config('navigation');
                            $navigation = set_active($this->config->item('navigation'), $curr_uri, $this->acl);
                            foreach($navigation as $menu) {
                                display_menu_item($menu, $curr_uri);
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper" <?php echo (isset($is_angular)) ? "ng-controller='$ui_controller as vm' " : '' ?>>

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><?php echo $page_title ?></h4>
                    </div>

                    <?php if (isset($page_icons) && !empty($page_icons)): ?>
                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <?php echo $page_icons ?>
                            </div>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div id="vmt_content" class="content">


                <?php echo $template['content']; ?>


                <div class="footer text-muted">
                    &copy; 2016 Venus Media Teknologi
                </div>
            </div>
            <!-- /content area -->

        </div>


        <script>
            $(window).load(function () {

                $('input.tanggal-lahir').inputmask('99/99/9999');

                $('select.select2').select2({
                    minimumResultsForSearch: 10,
                    width: '100%'
                });

                // External table additions
                // ------------------------------

                // Enable Select2 select for the length option
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: 10,
                    width: 'auto'
                });

                // Primary
                $(".control-primary").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-primary-600 text-primary-800'
                });

                // Danger
                $(".control-danger").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-danger-600 text-danger-800'
                });

                // Success
                $(".control-success").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-success-600 text-success-800'
                });

                // Warning
                $(".control-warning").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-warning-600 text-warning-800'
                });

                // Info
                $(".control-info").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-info-600 text-info-800'
                });

                //TODO: kenapa searching harus di-redraw? Seharusnya pakai searching standar dia sudah otomatis redraw.
//                var table = $('.dTable').DataTable();
//                table.columns().every(function () {
//                    var that = this;
//                    $('input', this.footer()).on('keyup change', function () {
//                        that.search(this.value).draw();
//                    });
//                });
            });
        </script>
        <script type="text/ng-template" id="toast.html">
            <div class="{{toastClass}} {{toastType}}" ng-click="tapToast()">
                <div ng-switch on="allowHtml">
                    <div ng-switch-default ng-if="title" class="{{titleClass}}" aria-label="{{title}}">{{title}}</div>
                    <div ng-switch-default class="{{messageClass}}" aria-label="{{message}}">{{message}}</div>
                    <div ng-switch-when="true" ng-if="title" class="{{titleClass}}" ng-bind-html="title"></div>
                    <div ng-switch-when="true" class="{{messageClass}}" ng-bind-html="message"></div>
                </div>
            </div>
        </script>
        <?php echo $template['js_footer']; ?>
        <block-ui is-show="isProcessing"></block-ui>

        <script type="text/javascript" src="<?php echo bower_url('angular-resource/angular-resource.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo js_url('app.js') ?>"></script>
        <script type="text/javascript">

        </script>
</body>

</html>
