<?php
$name = '';
$role = '';
if ($this->auth->loggedin()) {
    $name = $auth_user['first_name'] . ' ' . $auth_user['last_name'];
    $role = $auth_user['role_name'];
    if (strlen(trim($name)) == 0) {
        $name = $auth_user['username'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="base_url" content="<?php echo base_url(); ?>">
    <meta name="assets_url" content="<?php echo assets_url(); ?>">
    <?php echo $template['metas']; ?>

    <title><?php echo $template['title']; ?></title>

    <link rel="icon" href="<?php echo assets_url('img/favicon.png') ?>" type="image/png">
    <link rel="stylesheet" href="<?php echo assets_url('css/pages/vue-select.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('css/pages/vue-datepicker.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('css/pages/vue-fonts.css') ?>">
    <!-- Global stylesheets -->
    <?php echo $template['css']; ?>
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/selects/select2.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/uniform.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switchery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switch.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('moment/min/moment-with-locales.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('numeraljs/min/numeral.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('lodash/dist/lodash.min.js') ?>"></script>
    <!-- Scrollbar -->
    <script type="text/javascript" src="<?php echo bower_url('jquery.scrollbar/jquery.scrollbar.min.js') ?>"></script>
    
    <!-- Tambahan Untuk E-RM -->
    <script type="text/javascript" src="<?php echo bower_url('datatables.net/js/jquery.dataTables.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/validation/validate.min.js') ?>"></script>
    <script>
        base_url = '<?php echo site_url() ?>';
        moment.locale('id');
        assets_url = '<?php echo assets_url(); ?>';
        var loadNotifikasi = "<?= site_url('api/event/notifikasi/getData/:ROLE_ID'); ?>",
            listenNotifikasi= "<?php echo site_url('api/event/notifikasi/listen_notifikasi?event=:EVENT'); ?>";
        var ROLE_ID = <?php echo $auth_user['role_id'];; ?>;
        var env = '<?php echo ENVIRONMENT;?>';

        if (env == 'production') {
            console = console || {};
            console.log = function () {
            };
        }
    </script>
    <!-- /core JS files -->

    <!-- /theme JS files -->
    <?php echo $template['js_header']; ?>
    <script type="text/javascript" src="<?php echo js_url('core/app.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('app.js?version=1.1') ?>"></script>

    <style>
        #dataTable_list_ed > thead > tr > th, #dataTable_list_ed > tbody > tr > td  {
            white-space: nowrap;
        }

        .input-group {
            width: 100%;
        }
    </style>
</head>

<body class="navbar-top has-detached-right" data-spy="scroll" data-target=".sidebar-detached" data-offset="100">

<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
        <?php 
            $appname = $this->config->item('app_name');
            $appversion = $this->config->item('app_version');
        ?>
        <a class="navbar-brand" href="<?php echo site_url() ?>">
            <img src="<?= base_url(get_option('rs_logo_without_text')) ?>" style="margin-top: -10px;height:40px">
            <span style="font-size: 20px;color: #fff;position: relative;top: -30px;left: 50px;">
                <?= $appname.' '.$appversion; ?>
            </span>
        </a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo image_url('placeholder.jpg') ?>" alt="">
                    <span><?php echo $name ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="<?php echo site_url('auth/user/profile?redirect=' . urlencode(current_url())); ?>"><i
                                class="icon-user-plus"></i> My profile</a></li>
                    <li><a class="logout" href="<?php echo site_url('auth/logout') ?>"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" id="badge_click">
                    <i class="icon-bubbles4"></i>
                    <div class="badge bg-warning-400" id="badge_mananger"></div>
                </a>
                
                <div class="dropdown-menu dropdown-content width-350">
                    <ul class="media-list dropdown-content-body">
                        <div class="section_notif">
                            
                        </div>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main sidebar-fixed">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="<?php echo image_url('placeholder.jpg') ?>"
                                                                class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold"><?php echo $name ?></span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-vcard text-size-small"></i> &nbsp;<?php echo $role ?>
                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <?php
                            function set_active($menus, $curr_uri, $acl)
                            {
                                foreach ($menus as $index => $menu) {
                                    $is_active = false;
                                    $is_allowed = false;
                                    $has_children = isset($menu['children']) and is_array($menu['children']);
                                    if ($has_children) {
                                        $menus[$index]['children'] = set_active($menus[$index]['children'], $curr_uri, $acl);
                                        foreach ($menus[$index]['children'] as $menu_item) {
                                            if ($menu_item['is_active']) {
                                                $is_active = $is_active || true;
                                            }
                                            if ($menu_item['is_allowed']) {
                                                $is_allowed = $is_allowed || true;
                                            }
                                        }
                                    } else {
                                        $is_active = strpos($curr_uri, $menu['uri']) === 0;
                                        $is_allowed = !isset($menu['uri']) || $acl->is_allowed($menu['uri']);
                                    }
                                    $menus[$index]['is_active'] = $is_active;
                                    $menus[$index]['is_allowed'] = $is_allowed;
                                }
                                return $menus;
                            }

                            function display_menu_item($menu, $curr_uri)
                            {
                                if (empty($curr_uri)) $curr_uri = 'home';
                                $is_active = (isset($menu['is_active']) && $menu['is_active']);
                                $is_allowed = (isset($menu['is_allowed']) && $menu['is_allowed']);
                                $has_children = isset($menu['children']) and is_array($menu['children']);
                                if ($is_allowed) {
                                    echo '<li' . ($is_active ? ' class="active"' : '') . '>';
                                    echo '<a href="' . ((isset($menu['uri']) and !empty($menu['uri'])) ? site_url($menu['uri']) : '#') . '"' . ($has_children ? ' class="has-ul"' : '') . '>';
                                    if (isset($menu['icon']) && !empty($menu['icon']))
                                        echo '<i class="' . $menu['icon'] . '"></i>';
                                    echo '<span>' . $menu['title'] . '</span>';
                                    echo '</a>';
                                    if ($has_children) {
                                        echo '<ul' . ($is_active ? '' : ' class="hidden-ul"') . '>';
                                        foreach ($menu['children'] as $menu_item) {
                                            display_menu_item($menu_item, $curr_uri);
                                        }
                                        echo '</ul>';
                                    }
                                }
                            }

                            $curr_uri = $this->uri->uri_string();
                            if (empty($curr_uri)) $curr_uri = 'home';
                            $this->load->config('navigation');
                            $navigation = set_active($this->config->item('navigation'), $curr_uri, $this->acl);
                            foreach ($navigation as $menu) {
                                display_menu_item($menu, $curr_uri);
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->
        
        <!-- Page header -->
        <div class="page-header page-header-default has-cover" style="border: 1px solid #ddd; border-bottom: 0;" id="vm_page_header">
            <div class="page-header-content">
                <div class="page-title">
                    <h5>
                        <i class="icon-arrow-box position-left"></i>
                        <span class="text-semibold" v-html="title"></span>
                        <!-- <small class="display-block">-</small> -->
                    </h5>
                </div>

                <div class="heading-elements" v-html="action">
                    
                </div>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
        </div>
        <!-- /page header -->

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
                             
                <?php echo $template['content']; ?>
            
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

<!-- Horizontal form modal -->
<div id="modal_shift" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Pilih Shift</h5>
            </div>

            <form id="form_shift" action="#" class="form-horizontal">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Shift</label>
                        <div class="col-sm-9">
                            <select name="jadwal_shift_id" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Pilih</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Clean Snippset ES6 + jQuery + VueJS-->
<script type="text/javascript" src="<?php echo js_url('app/sweetalert2.bundle.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/autoNumeric.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/vue.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/vue-router.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/axios.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/v-mask.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/vue-select.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/vue-autonumeric.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/vue-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/vue-content-loader.js') ?>"></script>

<script type="text/javascript" src="<?php echo js_url('app/global.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url('app/provider.js') ?>"></script>
<!-- component vue -->
<!-- component autocomplete -->
<script type="text/x-template" id="carimember">
  <div class="autocomplete">
    <input type="text" @input="onChange" v-model="search" @keyup.down="onArrowDown" @keyup.up="onArrowUp" @keyup.enter="onEnter" class="search-input" v-bind:placeholder="placeHolder">
    <i class="icon-search4"></i>
    <ul id="autocomplete-results" v-show="isOpen" class="autocomplete-results">
      <li class="loading" v-if="isLoading">
        Loading results...
      </li>
      <li v-else v-for="(result, i) in results" :key="i" @click="setResult(result)" class="autocomplete-result" :class="{ 'is-active': i === arrowCounter }">
        {{ result.nama_dokter.toUpperCase() }}
      </li>
    </ul>
  </div>
</script>

<!-- template for the modal diskon -->
<script type="text/x-template" id="modal-diskon">
    <div class="modal modal-mask" style="display: block;">

        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" @click="close()">×</button>
                    <h5 class="modal-title">Diskon</h5>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        <span class="text-semibold"> <i class="icon-primitive-dot"></i> 25% </span> Approve Manager<br>
                        <span class="text-semibold"> <i class="icon-primitive-dot"></i> > 25% </span> Approve Direktur<br>
                    </div>
                    <div class="form-group">
                        <label for="">Masukan Diskon</label>
                        <vue-autonumeric :placeholder="'Diskon'" :options="setupDefault.percent" v-model="disc">
                        </vue-autonumeric>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" @click="save()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/x-template" id="select2-template">
  <select v-bind:class="classname">
    <slot></slot>
  </select>
</script>
<!-- /horizontal form modal -->
<?php echo $template['js_footer']; ?>

<?php
if (isset($template['script']) && $template['script'] != '')
    $this->load->view($template['script']);
?>


<script>
    $(window).load(function () {

        $('.uc').keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });

        $(":input:not(input[type=button],input[type=submit],button, .rangetanggal-form, .disp_tanggal, input[disabled=disabled]):visible:first").focus();

        $(".el-hidden").hide();
        $("label.input-required").append(' <span class="text-danger">*</span>');

        $('[data-popup=tooltip]').tooltip();

        $('.rangetanggal-form').inputmask("##/##/#### - ##/##/####", {"placeholder": "      "});

        $('.modal').on('shown.bs.modal', function () {
            $("input").focus();
        });  
        
        // $('select').select2({
        //     width: '100%'
        // });

        // External table additions
        // ------------------------------

        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: 10,
            width: 'auto'
        });

        // Primary
        $(".control-primary").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-primary-600 text-primary-800'
        });

        // Danger
        $(".control-danger").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-danger-600 text-danger-800'
        });

        // Success
        $(".control-success").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-success-600 text-success-800'
        });

        // Warning
        $(".control-warning").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-warning-600 text-warning-800'
        });

        // Info
        $(".control-info").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-info-600 text-info-800'
        });
        //default
        $(".control-default").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-salte-600 text-slate-800'
        });
    });
</script>

</body>
</html>
