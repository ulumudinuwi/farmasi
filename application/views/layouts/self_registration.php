<?php
//$name = '';
//$role = '';
//if ($this->auth->loggedin()) {
//    $name = $auth_user['first_name'] . ' ' . $auth_user['last_name'];
//    $role = $auth_user['role_name'];
//    if (strlen(trim($name)) == 0) {
//        $name = $auth_user['username'];
//    }
//}
?>
<!DOCTYPE html>
<html lang="en" ng-app="rsbt">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $template['metas']; ?>

    <title><?php echo $template['title']; ?></title>

    <link rel="icon" href="<?php echo assets_url('img/logo/favicon.png') ?>" type="image/png">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo assets_url('css/icons/icomoon/styles.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo assets_url('css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo assets_url('css/core.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo assets_url('css/components.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo assets_url('css/colors.css') ?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Global stylesheets -->
    <?php echo $template['css']; ?>
    <?php $URI = site_url() . "index.php" ?>
    <!-- Global stylesheets -->

    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/wizards/stepy.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/selects/select2.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/uniform.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/jasny_bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/validation/validate.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/app.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('pages/wizard_stepy.js') ?>"></script>




    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switchery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switch.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('moment/min/moment.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('numeraljs/min/numeral.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('lodash/dist/lodash.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('jquery.inputmask/dist/jquery.inputmask.bundle.js') ?>"></script>



    <script>
        base_url = '<?php echo site_url() ?>';
        assets_url = '<?php echo assets_url()?>';
        moment.locale('id');
        var env = '<?php echo ENVIRONMENT;?>';

        if(env == 'production'){
            console = console || {};
            console.log = function(){};
        }

    </script>
    <!-- /core JS files -->

    <!-- /theme JS files -->
    <?php echo $template['js_header']; ?>


    <?php
    if (isset($template['script']) && $template['script'] != '')
        $this->load->view($template['script']);
    ?>
<!--    <script type="text/javascript" src="--><?php //echo js_url('core/app.js') ?><!--"></script>-->


</head>

<body style="background: transparent !important;">


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper" <?php echo (isset($is_angular)) ? "ng-controller='$ui_controller as vm' " : '' ?>>

            <!-- Page header -->

            <!-- /page header -->

            <!-- Content area -->
            <div id="vmt_content" class="content" style="margin:0 !important;padding: 0 !important;">


                <?php echo $template['content']; ?>


                <div class="footer text-muted">
                    &copy; <?php echo date('Y'); ?> VMT Software
                </div>
            </div>
            <!-- /content area -->

        </div>


        <script>
            $(window).load(function () {

                $('input.tanggal-lahir').inputmask('99/99/9999');

                $('select.select2').select2({
                    minimumResultsForSearch: 10,
                    width: '100%'
                });

                // External table additions
                // ------------------------------

                // Enable Select2 select for the length option
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: 10,
                    width: 'auto'
                });

                // Primary
                $(".control-primary").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-primary-600 text-primary-800'
                });

                // Danger
                $(".control-danger").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-danger-600 text-danger-800'
                });

                // Success
                $(".control-success").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-success-600 text-success-800'
                });

                // Warning
                $(".control-warning").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-warning-600 text-warning-800'
                });

                // Info
                $(".control-info").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-info-600 text-info-800'
                });

                //TODO: kenapa searching harus di-redraw? Seharusnya pakai searching standar dia sudah otomatis redraw.
//                var table = $('.dTable').DataTable();
//                table.columns().every(function () {
//                    var that = this;
//                    $('input', this.footer()).on('keyup change', function () {
//                        that.search(this.value).draw();
//                    });
//                });
            });
        </script>
        <script type="text/ng-template" id="toast.html">
            <div class="{{toastClass}} {{toastType}}" ng-click="tapToast()">
                <div ng-switch on="allowHtml">
                    <div ng-switch-default ng-if="title" class="{{titleClass}}" aria-label="{{title}}">{{title}}</div>
                    <div ng-switch-default class="{{messageClass}}" aria-label="{{message}}">{{message}}</div>
                    <div ng-switch-when="true" ng-if="title" class="{{titleClass}}" ng-bind-html="title"></div>
                    <div ng-switch-when="true" class="{{messageClass}}" ng-bind-html="message"></div>
                </div>
            </div>
        </script>
        <?php echo $template['js_footer']; ?>
        <block-ui is-show="isProcessing"></block-ui>

        <script type="text/javascript" src="<?php echo bower_url('angular-resource/angular-resource.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo js_url('app.js') ?>"></script>
        <script type="text/javascript">

        </script>
</body>

</html>
