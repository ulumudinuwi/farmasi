<style>
input[type=text].form-control {
    text-transform: none;
}
</style>
<form class="form-horizontal" method="post" id="form">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    Simpan
                </button>
                <button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
            </div>
            <legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

            <div class="row">
                <!-- Data -->
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong>Data</strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="option_name">
                                        Nama
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="option_name" id="option_name" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="option_type">
                                        Type
                                    </label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="option_type" id="option_type">
                                            <option value="">- Type -</option>
                                            <option value="text">Text</option>
                                            <option value="array">Array</option>
                                            <option value="object">Object</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="autoload">
                                        Autoload
                                    </label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="autoload" id="autoload">
                                            <option value="">- Autoload -</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="option_table">
                                        Table Lookup
                                    </label>
                                    <div class="col-lg-8">
                                        <select class="form-control" id="option_table" name="option_table">
                                            <option value="">- Tidak Ada -</option>
                                            <?php foreach ($tables as $table): ?>
                                                <option><?= $table ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="option_value">
                                        Value
                                    </label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" name="option_value" id="option_value"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="user_visibility">
                                        User Visibility
                                    </label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="user_visibility" id="user_visibility">
                                            <option value="0" selected="selected">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <!-- Input Hidden -->
            <input type="hidden" name="option_id" id="option_id" value="0" />

            <legend class="text-semibold"> &nbsp;</legend>
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    Simpan
                </button>
                <button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
            </div>
        </div>
    </div>
</form>

<!-- Modal Lookup -->
<div id="modal_lookup" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
            <div class="modal-header bg-slate">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Cari Data</h6>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover" id="table_lookup">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
$('select').select2();
$('.styled').uniform();
$('[data-popup=tooltip]').tooltip();

$(function() {

    // Default initialization
    $('.wysihtml5-default').wysihtml5({
        parserRules:  wysihtml5ParserRules,
        stylesheets: ["assets/css/components.css"]
    });


    // Simple toolbar
    $('.wysihtml5-min').wysihtml5({
        parserRules:  wysihtml5ParserRules,
        stylesheets: ["assets/css/components.css"],
        "font-styles": true, // Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, // Italics, bold, etc. Default true
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": false, // Button which allows you to edit the generated HTML. Default false
        "link": true, // Button to insert a link. Default true
        "image": false, // Button to insert an image. Default true,
        "action": false, // Undo / Redo buttons
        "color": false
    });


    // Editor events
    $('.wysihtml5-init').on('click', function() {
        $(this).off('click').addClass('disabled');
        $('.wysihtml5-events').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["assets/css/components.css"],
            events: {
                load: function() { 
                    $.jGrowl('Editor has been loaded.', { theme: 'bg-slate-700', header: 'WYSIHTML5 loaded' });
                },
                change_view: function() {
                    $.jGrowl('Editor view mode has been changed.', { theme: 'bg-slate-700', header: 'View mode' });
                }
            }
        });
    });


    // Style form components
    $('.styled').uniform();

});
</script>