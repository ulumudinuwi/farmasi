<script>
var OPTION_ID = "<?= $option_id ?>";
var url = {
    index: "<?php echo site_url('master/options/index'); ?>",
    getData: "<?php echo site_url('api/master/options/get_data/:OPTION_ID'); ?>",
    loadDataLookup: "<?php echo site_url('api/master/options/load_data_lookup'); ?>",
    save: "<?php echo site_url('api/master/options/save'); ?>"
};

var form = $("#form");

var table = $("#table_lookup"),
    tableDt,
    tableTimer;

function fillForm(option_id) {
    blockPage();
    $.getJSON(url.getData.replace(':OPTION_ID', OPTION_ID), function (res, status) {
        if (status === 'success') {
            var data = res.data;

            $("#option_name").val(data.option_name).trigger('change');
            $("#option_type").val(data.option_type).trigger('change');
            $("#autoload").val(data.autoload).trigger('change');
            $("#user_visibility").val(data.user_visibility).trigger('change');
            $("#option_id").val(data.option_id).trigger('change');

            $("#option_table").val(data.option_table).trigger('change');
            setTimeout(function () {
                $("#option_value").val(data.option_value).trigger('change');
                $.unblockUI();
            }, 1000);
        }
    });
}

$(".cancel-button").click(function () {
    window.location.assign(url.index);
});

$("#option_table").on('change', function () {
    let tableName = $(this).val();
    let div = $("#option_value").closest('div');
    if (tableName == "") {
        if ($("#option_value").prop('type') == 'textarea') {
            //
        } else {
            $("#option_value").remove();
            $('<textarea/>')
                .prop('name', 'option_value')
                .prop('id', 'option_value')
                .addClass('form-control')
                .appendTo(div);
        }
    } else {
        if ($("#option_value").prop('type') == 'text') {
            //
        } else {
            $("#option_value").remove();

            let inputGroup = $('<div/>')
                .addClass('input-group')
                .appendTo(div);
            let input = $('<input/>')
                .prop('type', 'text')
                .prop('name', 'option_value')
                .prop('id', 'option_value')
                .prop('readonly', true)
                .addClass('form-control')
                .appendTo(inputGroup);
            let span = $('<span/>')
                .addClass('input-group-btn')
                .appendTo(inputGroup);
            let btn = $('<button/>')
                .prop('type', 'button')
                .addClass('btn btn-primary')
                .html('Cari')
                .appendTo(span);

            btn.on('click', function () {
                tableDt.fnFilter($("#option_table").val(), 0);
                tableDt.fnDraw();
                $("#modal_lookup").modal('show');
            });
        }
    }
});

tableDt = table.dataTable({
    "sPaginationType": "full_numbers",
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": url.loadDataLookup,
    "columns": [
        {
            "data": "kode",
            "render": function (data, type, row, meta) {
                return '<a href="#" class="click" data-id="' + row.id + '">' + data + '</a>';
            },
            "className": ""
        },
        {
            "data": "nama",
            "render": function (data, type, row, meta) {
                return data;
            },
            "className": ""
        }
    ],
    "order": [ [0, "asc"] ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
        if (tableTimer) clearTimeout(tableTimer);
        tableTimer = setTimeout(function () {
            blockElement(table.selector);
            $.getJSON( sSource, aoData, function (json) {
                fnCallback(json);
            });
        }, 500);
    },
    "fnDrawCallback": function (oSettings) {
      var n = oSettings._iRecordsTotal;
      
      table.unblock();

      table.find('[data-popup=tooltip]').tooltip();
    }
});

table.on('click', '.click', function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    $("#option_value").val(id);
    $("#modal_lookup").modal('hide');
});

form.validate({
    rules: {
        option_name: { required: true },
        option_value: { required: true },
        option_type: { required: true },
        autoload: { required: true }
    },
    messages: {
        option_name: "Nama diperlukan.",
        option_value: "Value diperlukan.",
        option_type: "Type diperlukan.",
        autoload: "Autoload diperlukan."
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var inputGroup = $(element).closest('.input-group');
        var checkbox = $(element).closest('.checkbox-inline');

        if (inputGroup.length) {
            error.insertAfter(inputGroup);
        } else if (checkbox.length) {
            checkbox.append(error);
        } else {
            $(element).closest("div").append(error);
        }
    },
    submitHandler: function (form) {
        blockPage();
        var postData = $(form).serialize();
        $.ajax({
            url: url.save,
            type: 'POST',
            dataType: "json",
            data: postData,
            success: function (data) {
                console.log(data);
                successMessage('Success', "Data berhasil disimpan.");

                $('[type=submit]').prop('disabled', true);
                $('input, select, textarea, [type=submit]').prop('disabled', true);
                setTimeout(function () {
                    window.location.assign(url.index);
                }, 3000);
            },
            error: function () {
                $.unblockUI();
                errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan data.");
            },
            complete: function () {
                $.unblockUI();
            }
        });
    }
});

fillForm(OPTION_ID);
</script>