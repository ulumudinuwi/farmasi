<style>

</style>
<form class="form-horizontal" method="post" id="form" enctype='multipart/form-data'>
  <div class="panel panel-flat">
    <div class="panel-body">
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
      <legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

      <div class="row">
        <!-- Data -->
        <div class="col-sm-12">
          <fieldset>
            <legend class="text-bold">
              <i class="fa fa-list-alt position-left"></i>
              <strong>Data Kelompok</strong>
            </legend>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Nama</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama ...">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Deskripsi</label>
                  <div class="col-lg-8">
                    <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi ..." rows="6"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4 input-required">BPOM</label>
                  <div class="col-md-8">
                    <div class="input-group">
                      <select class="form-control" id="bpom" name="bpom">
                        <option value="">- Pilih -</option>
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Jenis</label>
                  <div class="col-lg-8">
                    <div class="input-group">
                      <select class="form-control" id="jenis_barang_id" name="jenis_barang_id">
                        <option value="">- Pilih -</option>
                        <?php
                          foreach ($jenis_barang as $row) {
                            echo "<option value=\"{$row->id}\">{$row->nama}</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </div> -->
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-lg-4 control-label">Foto Kategori</label>
                  <div class="col-lg-8">
                    <input type="file" class="form-control" name="foto_id" id="foto_id" accept="image/*">
                    <input type="hidden" class="form-control" name="old_foto_id" id="old_foto_id" accept="image/*">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">&nbsp;</label>
                  <div class="col-lg-8">
                    <img src="" width="100%" id="priviewImg">
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
  
      <!-- Input Hidden -->
      <input type="hidden" name="uid" id="uid" value="" />

      <legend class="text-semibold"> &nbsp;</legend>
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
    </div>
  </div>
</form>
