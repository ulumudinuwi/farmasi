<style>
	.form-group {
		margin-bottom: 2px;
	}
</style>
<div class="panel panel-white">

	<div class="panel-heading">
		<h5 class="panel-title">Pasien</h5>
	</div>

	<form class="form-horizontal" id="pasien_form" name="pasien_form" method="post" action="">

		<div class="panel-body">
				
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">No. RM</label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="no_rm" name="no_rm" value="" maxlength="20" autocomplete="off" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Nama <span class="text-danger">*</span></label>
						<div class="col-md-3">
							<select class="form-control" id="title_id" name="title_id">
							<?php
								foreach ($title_list as $title) {
									echo "<option value=\"".$title->id."\">".$title->singkatan."</option>";
								}
							?>
							</select>
						</div>
						<div class="col-md-6">
							<input class="form-control" type="text" id="nama" name="nama" value="" maxlength="30" autocomplete="off" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Jenis Kelamin</label>
						<div class="col-md-9">
							<label class="radio-inline">
								<input class="styled" type="radio" id="jenis_kelamin_1" name="jenis_kelamin" value="1" />
								Laki-laki
							</label>
							<label class="radio-inline">
								<input class="styled" type="radio" id="jenis_kelamin_2" name="jenis_kelamin" value="2" />
								Perempuan
							</label>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Kewarganegaraan</label>
						<div class="col-md-9">
							<label class="radio-inline">
								<input class="styled" type="radio" id="kewarganegaraan_1" name="kewarganegaraan" value="1" />
								WNI
							</label>
							<label class="radio-inline">
								<input class="styled" type="radio" id="kewarganegaraan_2" name="kewarganegaraan" value="1" />
								WNA
							</label>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Alamat</label>
						<div class="col-md-9">
							<textarea class="form-control" id="alamat" name="alamat" rows="2" maxlength="255"></textarea>
						</div>
					</div>
					<div class="form-group" id="provinsi_section">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<div class="form-group">
								<label class="control-label col-md-4" for="alamat_provinsi_id" style="padding-left:0;">Provinsi</label>
								<div class="col-md-8" style="padding:0;">
									<select class="form-control" id="alamat_provinsi_id" name="alamat_provinsi_id">
									<?php
										foreach ($provinsi_list as $provinsi) {
											echo "<option value=\"".$provinsi->id."\">".$provinsi->nama."</option>";
										}
									?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" id="kabupaten_section">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<div class="form-group">
								<label class="col-md-4 control-label" for="alamat_kabupaten_id" style="padding-left:0;">Kabupaten/Kota</label>
								<div class="col-md-8" style="padding:0;">
									<select class="form-control" id="alamat_kabupaten_id" name="alamat_kabupaten_id"></select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" id="kecamatan_section">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<div class="form-group">
								<label class="col-md-4 control-label" for="alamat_kecamatan_id" style="padding-left:0;">Kecamatan</label>
								<div class="col-md-8" style="padding:0;">
									<select class="form-control" id="alamat_kecamatan_id" name="alamat_kecamatan_id"></select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" id="kelurahan_section">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<div class="form-group">
								<label class="col-md-4 control-label" for="alamat_kelurahan_id" style="padding-left:0;">Kelurahan/Desa</label>
								<div class="col-md-8" style="padding:0;">
									<select class="form-control" id="alamat_kelurahan_id" name="alamat_kelurahan_id"></select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" id="kodepos_section">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<div class="form-group">
								<label class="col-md-4 control-label" for="alamat_kodepos" style="padding-left:0;">Kodepos</label>
								<div class="col-md-8" style="padding:0;">
									<input class="form-control" type="text" id="alamat_kodepos" name="alamat_kodepos" value="12" maxlength="" autocomplete="off" />
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">No. Identitas</label>
						<div class="col-md-9">
							<div class="col-md-4" style="padding-left: 0;">
								<select class="form-control" id="identitas_id" name="identitas_id">
								<?php
									foreach ($jenis_id_list as $key => $value) {
										echo "<option value=\"".$key."\">".$value."</option>";
									}
								?>
								</select>
							</div>
							<div class="col-md-8">
								<input class="form-control" type="text" id="no_identitas" name="no_identitas" value="" autocomplete="off" />
							</div>
						</div>
					</div>
					
				</div>
				
				<div class="col-md-6">
					
					<div class="form-group">
						<div class="col-md-3">
							<select class="form-control" id="jenis_telepon_1">
								<option value="1">HP</option>
								<option value="2">Telp. Rumah</option>
							</select>
						</div>
						<div class="col-md-9">
							<input class="form-control" type="text" id="telepon_1" name="telepon_1" value="" autocomplete="off" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-3">
							<select class="form-control" id="jenis_telepon_2">
								<option value="1">HP</option>
								<option value="2">Telp. Rumah</option>
							</select>
						</div>
						<div class="col-md-9">
							<input class="form-control" type="text" id="telepon_2" name="telepon_2" value="" autocomplete="off" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">Tempat Lahir</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="" autocomplete="off" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">Tanggal Lahir</label>
						<div class="col-md-9">
							<input type="hidden" id="tanggal_lahir" name="tanggal_lahir" value="" />
							<input class="combodate form-control" type="text" id="disp_tanggal_lahir" data-format="DD-MM-YYYY" data-template="D  MMM  YYYY" value="21-12-2012">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">Umur</label>
						<div class="col-md-9">
							<div class="col-md-2" style="padding: 0;">
								<input class="form-control" type="text" id="umur_tahun" name="umur_tahun" maxlength="3" placeholder="">
							</div>
							<div class="col-md-2" style="padding: 0;">
								<label style="padding-top: 7px;">&nbsp;Tahun</label>
							</div>
							<div class="col-md-2" style="padding: 0;">
								<input class="form-control" type="text" id="umur_bulan" name="umur_bulan" maxlength="2" placeholder="">
							</div>
							<div class="col-md-2" style="padding: 0;">
								<label style="padding-top: 7px;">&nbsp;Bulan</label>
							</div>
							<div class="col-md-2" style="padding: 0;">
								<input class="form-control" type="text" id="umur_hari" name="umur_hari" maxlength="2" placeholder="">
							</div>
							<div class="col-md-2" style="padding: 0;">
								<label style="padding-top: 7px;">&nbsp;Hari</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Golongan Darah</label>
						<div class="col-md-9">
							<div class="input-group col-md-12">
								<select class="form-control" id="golongan_darah" name="golongan_darah">
									<?php
										foreach ($golongan_darah_list as $key => $value) {
											echo "<option value=\"".$key."\">".$value."</option>";
										}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Agama</label>
						<div class="col-md-9">
							<div class="input-group col-md-12">
								<select class="form-control" id="agama" name="agama">
									<?php
										foreach ($agama_list as $key => $value) {
											echo "<option value=\"".$key."\">".$value."</option>";
										}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Suku</label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="suku" name="suku" value="" maxlength="25" autocomplete="off" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Status Kawin</label>
						<div class="col-md-9">
							<div class="input-group col-md-12">
								<select class="form-control" id="status_kawin" name="status_kawin"></select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Pendidikan</label>
						<div class="col-md-9">
							<div class="input-group col-md-12">
								<select class="form-control" id="pendidikan_id" name="pendidikan_id"></select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Pekerjaan</label>
						<div class="col-md-9">
							<div class="input-group col-md-12">
								<select class="form-control" id="pekerjaan_id" name="pekerjaan_id"></select>
							</div>
						</div>
					</div>
				</div>
			</div>
				
		</div>
		<div class="panel-footer">
			<div class="heading-elements">
				<div class="heading-btn pull-right">
					<button class="btn-success btn-labeled btn" type="submit">
						<b><i class="icon-floppy-disk"></i></b>Simpan <?php echo DATA_MODE_VIEW; ?>
					</button>
					<button class="btn btn-default" type="button" id="batal_button">
						Batal
					</button>
				</div>
			</div>
		</div>
		<div>
			<input type="hidden" id="uid" name="uid" value="0" />
			<input type="hidden" id="created_by" name="created_by" value="<?php echo $auth_user["id"]; ?>" />
			<input type="hidden" id="update_by" name="update_by" value="<?php echo $auth_user["id"]; ?>" />
			
			<input type="hidden" id="page" value="" />
		</div>
	</form>

</div>