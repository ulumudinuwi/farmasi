<style>
	table > thead > tr > th, table > tbody > tr > td  {
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<div class="col-md-12">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Barang</legend>
							</fieldset>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group el-hidden">
										<label class="control-label col-md-3 input-required">Kode</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="kode" name="kode" placeholder="Kode ..." disabled="true">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Nama</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama ...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Merk</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="merk" name="merk" placeholder="Merk ...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required">BPOM</label>
										<div class="col-md-7">
											<div class="input-group">
												<select class="form-control" id="bpom" name="bpom">
													<option value="">- Pilih -</option>
													<option value="0">Tidak</option>
													<option value="1">Ya</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Jenis</label>
										<div class="col-md-7">
											<div class="input-group">
												<select class="form-control" id="jenis_id" name="jenis_id"></select>
												<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
											</div>
										</div>
									</div>
									<!-- <div class="form-group">
										<label class="control-label col-md-3">PPN(%)</label>
										<div class="col-md-7">
											<div class="input-group">
												<input type="text" class="form-control input-decimal" id="ppn" name="ppn" placeholder="PPN ...">
											</div>
										</div>
									</div> -->
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Title Product</label>
										<div class="col-md-7">
											<div class="input-group">
												<textarea class="form-control" id="deskripsi_title" name="deskripsi_title"></textarea>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Kategori</label>
										<div class="col-md-7">
											<div class="input-group">
												<select class="form-control" id="kategori_id" name="kategori_id"></select>
												<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required"><?php echo $this->lang->line('pabrik_label'); ?></label>
										<div class="col-md-7">
											<div class="input-group">
												<select class="form-control" id="pabrik_id" name="pabrik_id"></select>
												<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required"><?php echo $this->lang->line('kelompok_label'); ?></label>
										<div class="col-md-7">
											<div class="input-group">
												<select class="form-control" id="kelompok_id" name="kelompok_id[]" multiple></select>
												<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Berat Barang</label>
										<div class="col-md-7">
											<div class="input-group">
												<input class="form-control" id="berat_barang" name="berat_barang"></input>
												<span class="input-group-addon">Gram</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Diskon </label>
										<div class="col-md-7">
											<div class="input-group">
												<input class="form-control input-decimal" id="diskon" name="diskon"></input>
												<span class="input-group-addon">%</span>
											</div>
											<span class="text-info text-size-mini"><i class="fa fa-info"></i> Diskon digunakan untuk e-commerse</span>

										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<div class="row mb-20">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-12">Deskripsi</label>
								<div class="col-sm-12">
									<textarea class="form-control wysihtml5-min" id="deskripsi" name="deskripsi" rows="8" placeholder="Deskripsi ..."></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-20">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-12">Information</label>
								<div class="col-sm-12">
									<textarea class="form-control wysihtml5-min" id="deskripsi_information" name="deskripsi_information" rows="8" placeholder="Information ..."></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-20">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-12">Benefits</label>
								<div class="col-sm-12">
									<textarea class="form-control wysihtml5-min" id="deskripsi_benefits" name="deskripsi_benefits" rows="8" placeholder="Benefits ..."></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-20">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-sm-12">Ingredients</label>
								<div class="col-sm-12">
									<textarea class="form-control wysihtml5-min" id="deskripsi_ingredients" name="deskripsi_ingredients" rows="8" placeholder="Ingredients ..."></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-20">
						<div class="col-md-6">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pembelian</legend>
							</fieldset>
							<div class="form-group">
								<label class="control-label col-md-3 input-required">Satuan</label>
								<div class="col-md-7">
									<div class="input-group">
										<select class="form-control" id="satuan_pembelian_id" name="satuan_pembelian_id"></select>
										<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 input-required">Harga</label>
								<div class="col-md-7">
									<input type="text" class="form-control input-decimal" id="harga_pembelian" name="harga_pembelian" placeholder="Harga ...">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Diskon MOU(%)</label>
								<div class="col-md-7">
									<div class="input-group">
										<input type="text" class="form-control input-decimal" id="diskon_mou" name="diskon_mou" placeholder="Diskon MOU ...">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Penggunaan</legend>
							</fieldset>
							<div class="form-group">
								<label class="control-label col-md-3 input-required">Satuan</label>
								<div class="col-md-7">
									<div class="input-group">
										<select class="form-control" id="satuan_penggunaan_id" name="satuan_penggunaan_id"></select>
										<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 input-required">Isi</label>
								<div class="col-md-7">
									<input type="text" class="form-control input-bulat" id="isi_satuan_penggunaan" name="isi_satuan_penggunaan" placeholder="Jumlah ..." value="1">
									<span class="text-info text-size-mini"><i class="fa fa-info"></i> 1 satuan pembelian berapa satuan penggunaan</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-20">
						<div class="col-md-12">
							<fieldset>
								<legend class="text-bold"><i class="icon-image-compare position-left"></i> Foto Produk</legend>
							</fieldset>
							<div class="panel panel-default">
								<div class="panel-body">
									<h2 class="text-slate-300 no-preview-image text-center">No Image Available</h2>
									<div id="content-foto"></div>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn">
											<button type="button" class="btn btn-primary btn-labeled" id="btn-tambah_foto">
												<b><i class="fa fa-plus"></i></b>
												Tambah
											</button>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="">
						<input type="hidden" id="prefix_kode" name="prefix_kode" value="">
						<input type="hidden" id="tmp_kategori_id" value="">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<?php $this->load->view('master/barang/foto-modal'); ?>