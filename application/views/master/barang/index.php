<?php echo messages(); ?>
<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
	}
</style>
<div class="panel panel-flat">
  <div class="row panel-body form-horizontal">
    <div class="col-md-8">
      <div class="form-group">
        <label class="control-label col-md-3">Jenis</label>
        <div class="col-md-6">
          <div class="input-group">
            <select class="form-control" id="search_jenis">
              <option value="">- Pilih -</option>
              <?php 
                foreach($jenis as $row) {
                  echo "<option value='".base64_encode($row->id)."'>{$row->nama}</option>";
                }
              ?>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Kategori</label>
        <div class="col-md-6">
          <div class="input-group">
            <select class="form-control" id="search_kategori"></select>
            <span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr class="no-margin-top no-margin-bottom">
	<div class="panel-body no-padding-top">
		<div class="table-responsive">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th rowspan="2">Kode</th>
            <th rowspan="2">Nama</th>
						<th rowspan="2"><?php echo $this->lang->line('pabrik_label'); ?></th>
						<th colspan="2" class="text-center">Satuan</th>
            <th rowspan="2" class="text-center">Status</th>
						<th rowspan="2" class="text-center">Action</th>
					</tr>
          <tr>
            <th class="text-center">Pembelian</th>
            <th class="text-center">Penggunaan</th>
          </tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->load->view('master/barang/detail-modal'); ?>
<script type="text/javascript">
var table;
var btnRefresh = $("#btn-refresh"),
    modalDetail = "#detail-modal",
    tableDetail = $("#table-detail");

var url = {
  edit: "<?php echo site_url('master/barang/form/:UID'); ?>",
  delete: "<?php echo site_url('api/master/barang/delete'); ?>",
  loadData: "<?php echo site_url('api/master/barang/load_data'); ?>",
  getData: "<?php echo site_url('api/master/barang/get_data'); ?>?uid=:UID",
  getKategori: "<?php echo site_url('api/master/kategori_barang/get_all'); ?>",
  updateStatus: "<?php echo site_url('api/master/barang/update_status'); ?>",
}

function fillElement(obj, element) {
  let parent = $(element).parent();
  parent.find('.loading-select').show();
  $.getJSON(obj.url, function(data, status) {
    if (status === 'success') {
      var option = '';
      option += '<option value="" selected="selected">- Pilih -</option>';
      for (var i = 0; i < data.list.length; i++) {
        var selected = ""
        if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';
        option += '<option value="' + btoa(data.list[i].id) + '"  ' + selected + '>' + data.list[i].kode + ' - ' + data.list[i].nama + '</option>';
      }
      $(element).html(option).trigger("change");
    }
    parent.find('.loading-select').hide();
  });
}

function fillDetail(uid) {
  $(modalDetail).modal('show');
  blockElement(modalDetail + ' .modal-dialog');
  $.getJSON(url.getData.replace(":UID", uid), function(res, status) {
    if(status === "success") {
      let data = res.data;

      var label = Object.keys(data);
      for (var i = 0; i < label.length; i++) {
        switch(label[i]) {
          case "kode":
          case "nama":
          case "jenis":
          case "kategori":
          case "pabrik":
          case "satuan_pembelian":
          case "satuan_penggunaan":
          case "deskripsi":
            $('#detail_' + label[i]).html(data[label[i]] ? data[label[i]] : '-');
            break;
          case "harga_pembelian":
            $('#detail_' + label[i]).html(`Rp. ${numeral(data[label[i]]).format('0.0,')}`);
            break;
          case "isi_satuan_penggunaan":
            $('#detail_' + label[i]).html(numeral(data[label[i]]).format('0,0'));
            break;
          case "diskon_mou":
            $('#detail_' + label[i]).html(numeral(data[label[i]]).format('0.0,'));
            break;
        }
      }
    }
    $(modalDetail + ' .modal-dialog').unblock();

    $('[data-toggle=tooltip]').tooltip();
  });
}

$(window).ready(function () {
  table = $("#table").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": url.loadData,
        "type": "POST",
        "data": function(p) {
            p.jenis_id = $('#search_jenis').val();
            p.kategori_id = $('#search_kategori').val();
        }
    },
    "columns": [
      {
        "data": "kode",
        "render": function (data, type, row, meta) {
          return `<a class="detail-row" data-uid="${row.uid}">${data}</a>`;
        }
      },
      { 
        "data": "nama",
      },
      { "data": "pabrik" },
      { 
        "data": "satuan_pembelian",
        "orderable": false,
        "searchable": false,
      },
      { 
        "data": "satuan_penggunaan",
        "orderable": false,
        "searchable": false,
      },
      { 
        "data": "status",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          if (data == 0) 
            return '<button type="button" class="toggle-status-row btn bg-success btn-xs" data-status="1" data-uid="' + row.uid + '">Aktifkan</button>';
          return '<button type="button" class="toggle-status-row btn bg-slate-400 btn-xs" data-status="0" data-uid="' + row.uid + '">Non Aktifkan</button>';
        },
        "className": "text-center"
      },
      {
        "data": "uid",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
        },
        "className": "text-center"
      }
    ],
  });

  btnRefresh = $("#btn-refresh");
  btnRefresh.click(function () {
    table.draw();
  });
});

$(document).ready(function() {
  $('#search_jenis, #search_kategori').change(function() {
    table.draw();
  });

  $('#search_jenis').change(function() {
    let obj = {
      value: $(this).val(),
      url: url.getKategori + '?mode=by&jenis_id=' + $(this).val()
    };
    fillElement(obj, '#search_kategori');
  });

  $('#table').on("click", ".detail-row", function() {
    var uid = $(this).data("uid");
    fillDetail(uid);
  });

  $("#table").on("click", ".edit-row", function () {
    var uid = $(this).data('uid');
    window.location.assign(url.edit.replace(':UID', uid));
  });

  $("#table").on("click", ".toggle-status-row", function () {
    var btn = $(this);
    btn.prop("disabled", true);
    var uid = $(this).data('uid');
    var statusData = $(this).data('status');

    var title;
    if (parseInt(statusData) === 1) title = "Mengaktifkan data";
    else title = "Menonaktifkan data";

    // Progress loader
    var cur_value = 1;
    var update = false;
    var progress;
    var timer;

    // Make a loader.
    var loader = new PNotify({
      title: title,
      text: '<div class="progress progress-striped active" style="margin:0">\
      <div class="progress-bar bg-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">\
      <span class="sr-only">0%</span>\
      </div>\
      </div>',
      addclass: 'bg-slate',
      icon: 'icon-spinner4 spinner',
      hide: false,
      buttons: {
        closer: true,
        sticker: false
      },
      history: {
        history: false
      },
      before_open: function(PNotify) {
        progress = PNotify.get().find("div.progress-bar");
        progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");

        // Pretend to do something.
        timer = setInterval(function() {
          if (cur_value >= 100) {
            // Remove the interval.
            window.clearInterval(timer);
            
            update = true;
            loader.remove();

            return;
          }
          cur_value += 5;
          progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
        }, 65);
      },
      after_close: function(PNotify, timer_hide) {
        btn.prop("disabled", false);
        clearInterval(timer);
        
        if (update) {
          $.post(url.updateStatus, {uid: uid, status: statusData}, function (data, status) {
            if (status === "success") {

              if (parseInt(statusData) === 0) {
                btn.removeClass('bg-success');
                btn.addClass('bg-slate-400');
                btn.data('status', 0);
                btn.html("Inactive");
                successMessage('Berhasil', 'Data berhasil dinonaktifkan.');
              } else {
                btn.removeClass('bg-slate-400');
                btn.addClass('bg-success');
                btn.data('status', 1);
                btn.html("Active");
                successMessage('Berhasil', 'Data berhasil diaktifkan.');
              }
              table.draw();
            }
          }).fail(function (error) {
            if (parseInt(statusData) === 0) 
              errorMessage('Peringatan', 'Terjadi kesalahan saat menonaktifkan data.');
            else 
              errorMessage('Peringatan', 'Terjadi kesalahan saat mengaktifkan data.');
          });
        }

        update = false;
        
      }
    });
  });
  $('#search_jenis').change();
});

</script>