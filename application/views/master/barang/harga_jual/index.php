<?php echo messages(); ?>
<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
	}
</style>
<div class="panel panel-flat">
	<div class="row panel-body form-horizontal">
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-md-3">Jenis</label>
				<div class="col-md-6">
					<div class="input-group">
						<select class="form-control" id="search_jenis">
							<option value="">- Pilih -</option>
							<?php 
								foreach($jenis as $row) {
									echo "<option value='".base64_encode($row->id)."'>{$row->nama}</option>";
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Kategori</label>
				<div class="col-md-6">
					<div class="input-group">
						<select class="form-control" id="search_kategori"></select>
						<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr class="no-margin-top no-margin-bottom">
	<div class="panel-body no-padding-top">
		<div class="table-responsive">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Kode</th>
						<th>Nama</th>
						<th><?php echo lang('pabrik_label'); ?></th>
						<th>Harga Jual</th>
						<th class="text-center" width="5%">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
var table;
var btnRefresh = $("#btn-refresh"),
	modalDetail = "#detail-modal",
	tableDetail = $("#table-detail");

var url = {
	edit: "<?php echo site_url('master/barang/form_harga_jual/:UID'); ?>",
	loadData: "<?php echo site_url('api/master/barang/load_data'); ?>",
	getKategori: "<?php echo site_url('api/master/kategori_barang/get_all'); ?>",
}

function fillElement(obj, element) {
	let parent = $(element).parent();
	parent.find('.loading-select').show();
	$.getJSON(obj.url, function(data, status) {
		if (status === 'success') {
			var option = '';
			option += '<option value="" selected="selected">- Pilih -</option>';
			for (var i = 0; i < data.list.length; i++) {
				var selected = ""
				if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';
				option += '<option value="' + btoa(data.list[i].id) + '"  ' + selected + '>' + data.list[i].kode + ' - ' + data.list[i].nama + '</option>';
			}
			$(element).html(option).trigger("change");
		}
		parent.find('.loading-select').hide();
	});
}

$(window).ready(function () {
	table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": url.loadData,
			"type": "POST",
			"data": function(p) {
				p.jenis_id = $('#search_jenis').val();
				p.kategori_id = $('#search_kategori').val();
				p.browse = 'master-harga_jual';
			}
		},
		"columns": [
			{ "data": "kode",},
			{ "data": "nama" },
			{ "data": "pabrik" },
			{ 
				"data": "harga_penjualan",
				"render": function (data, type, row, meta) {
					return `Rp. ${numeral(data).format()}`;
				},
				"orderable": false,
				"searchable": false,
			},
			{
				"data": "uid",
				"orderable": false,
				"searchable": false,
				"render": function (data, type, row, meta) {
					return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
				},
				"className": "text-center"
			}
		],
	});

	btnRefresh = $("#btn-refresh");
	btnRefresh.click(function () {
		table.draw();
	});
});

$(document).ready(function() {
	$('#search_jenis, #search_kategori').change(function() {
		table.draw();
	});

	$('#search_jenis').change(function() {
		let obj = {
			value: $(this).val(),
			url: url.getKategori + '?mode=by&jenis_id=' + $(this).val()
		};
		fillElement(obj, '#search_kategori');
	});

	$("#table").on("click", ".edit-row", function () {
		var uid = $(this).data('uid');
		window.location.assign(url.edit.replace(':UID', uid));
	});

	$('#search_jenis').change();
});

</script>