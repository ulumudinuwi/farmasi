<script>
var uid = "<?php echo $uid; ?>",
form = '#form';
var url = {
  index: "<?php echo site_url('master/kelompok_barang'); ?>",
  save: "<?php echo site_url('api/master/kelompok_barang/save'); ?>",
  getData: "<?php echo site_url('api/master/kelompok_barang/get_data/:UID'); ?>",
}

function fillForm(uid) {
  blockElement(form);
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;

      $("#uid").val(data.uid);
      $("#nama").val(data.nama);
      $("#jenis_barang_id").val(data.jenis_barang_id).trigger('change');

      $('.el-hidden').show('slow');
      $(form).unblock();
    }
  });
}

$(form).validate({
  rules: {
    nama: { required: true },
    jenis_barang_id: { required: true },
  },
  messages: {
    nama: "Nama Diperlukan",
    jenis_barang_id: "Jenis Diperlukan",
  },
  errorPlacement: function(error, element) {
    var placement = $(element).closest('.input-group');
    if (placement.length > 0) {
        error.insertAfter(placement);
    } else {
        error.insertAfter($(element));
    }
  },
  focusInvalid: true,
  submitHandler: function (form) {
    $('input, textarea, select').prop('disabled', false);
    
    blockElement($(form));
    var formData = $(form).serialize();
    $.ajax({
      data: formData,
      type: 'POST',
      dataType: 'JSON', 
      url: url.save,
      success: function(data){
          $(form).unblock();
          successMessage('Berhasil', "Kelompok barang berhasil disimpan.");
          window.location.assign(url.index);
      },
      error: function(data){
          $(form).unblock();
          errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
      }
    });
    return false;
  }
});

$(".cancel-button").click(function () {
  window.location.assign(url.index);
});

if(uid !== "") fillForm(uid);
</script>