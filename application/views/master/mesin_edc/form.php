<style>
input[type=text].form-control {
    text-transform: none;
}
</style>
<form class="form-horizontal" method="post" id="form">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    Simpan
                </button>
                <button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
            </div>
            <legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

            <div class="row">
                <!-- Data -->
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong>Data</strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="kode">
                                        Kode
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="kode" id="kode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="nama">
                                        Nama
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="nama" id="nama" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <legend class="text-semibold"> &nbsp;</legend>
            <div class="text-right">
                <!-- INPUT HIDDEN -->
                <input type="hidden" name="id" id="id" value="0" />
                <input type="hidden" name="uid" id="uid" value="0" />
                <!-- ./INPUT HIDDEN -->
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    Simpan
                </button>
                <button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
            </div>
        </div>
    </div>
</form>

<script>
let UID = "<?= $uid; ?>";
let URL = {
    index: "<?= site_url('master/mesin_edc'); ?>",
    getData: "<?= site_url('api/master/mesin_edc/get_data/:UID'); ?>",
    save: "<?= site_url('api/master/mesin_edc/save'); ?>",
    delete: "<?= site_url('api/master/mesin_edc/delete'); ?>",
    restore: "<?= site_url('api/master/mesin_edc/restore'); ?>",
};

let FORM = $("#form");

let BTN_RESTORE = $("#btn-restore");
let BTN_DELETE = $("#btn-delete");
//let BTN_CANCEL = $("#btn-cancel");
let BTN_CANCEL = $(".cancel-button");

$(function() {
    $('select').select2();
    $('[data-popup=tooltip]').tooltip();
});
</script>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/mesin_edc/form.js'); ?>"></script>