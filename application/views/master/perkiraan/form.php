<style>
input[type=text].form-control {
    text-transform: none;
}
</style>
<form class="form-horizontal" method="post" id="form">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    Simpan
                </button>
                <button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
            </div>
            <legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

            <div class="row">
                <!-- Data -->
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong>Data</strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="klasifikasi">
                                        Klasifikasi
                                    </label>
                                    <?php
                                        $jenisPerkiraan = isset($perkiraan->jenis_perkiraan) ? (int) $perkiraan->jenis_perkiraan : 0;
                                    ?>
                                    <div class="col-lg-8">
                                        <select type="text" class="form-control" name="klasifikasi" id="klasifikasi" >
                                            <?php
                                                $selected = $jenisPerkiraan === 0 ? ' selected="selected"' : '';
                                                echo '<option value=""'.$selected.'>[ Pilih Jenis Perkiraan: ]</option>';
                                                foreach ($jenis_perkiraan_list as $key => $value) {
                                                    $selected = $jenisPerkiraan === (int) $key ? ' selected="selected"' : '';
                                                    echo '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="kode">
                                        Kode Rekening
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="kode" id="kode"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="nama">
                                        Nama Rekening
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="nama" id="nama" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="rincian">
                                        Kelompok / Rincian
                                    </label>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <input type="radio" name="rincian" id="rincian" class="styled choice" value="1">
                                            <label style="padding-top: 4px">
                                                Kelompok 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <input type="radio" name="rincian" id="rincian1" class="styled choice" value="2"> 
                                            <label style="padding-top: 4px">
                                                Rincian
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group cRincian">
                                    <label class="col-lg-4 control-label" for="parent">
                                        Parent
                                    </label>
                                    <div class="col-lg-8">
                                        <select type="text" class="form-control" name="parent" id="parent" >
                                            <option value="">[ Pilih Parent] </option>
                                            <?php foreach ($parent_list as $row): ?>
                                                <option value="<?php echo $row->id ?>"><?php echo $row->kode." - ".$row->nama ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group cRincian">
                                    <label class="col-lg-4 control-label" for="nama_alias">
                                        Nama Alias
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="nama_alias" id="nama_alias" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="status">
                                        Status
                                    </label>
                                    <div class="col-lg-2">
                                        <div class="radio">
                                            <input type="radio" name="status" id="status" class="styled choice" value="1">
                                            <label style="padding-top: 4px">
                                                Aktif 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <input type="radio" name="status" id="status1" class="styled choice" value="0"> 
                                            <label style="padding-top: 4px">
                                                Tidak Aktif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <legend class="text-semibold"> &nbsp;</legend>
            <div class="text-right">
                <!-- INPUT HIDDEN -->
                <input type="hidden" name="id" id="id" value="0" />
                <input type="hidden" name="uid" id="uid" value="0" />
                <!-- ./INPUT HIDDEN -->
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    Simpan
                </button>
                <button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
            </div>
        </div>
    </div>
</form>

<script>
let UID = "<?= $uid; ?>";
let URL = {
    index: "<?= site_url('master/perkiraan'); ?>",
    getData: "<?= site_url('api/master/perkiraan/get_data/:UID'); ?>",
    save: "<?= site_url('api/master/perkiraan/save'); ?>",
    delete: "<?= site_url('api/master/perkiraan/delete'); ?>",
    restore: "<?= site_url('api/master/perkiraan/restore'); ?>",
};

let FORM = $("#form");

let BTN_RESTORE = $("#btn-restore");
let BTN_DELETE = $("#btn-delete");
//let BTN_CANCEL = $("#btn-cancel");
let BTN_CANCEL = $(".cancel-button");

$(function() {
    $('select').select2();
    $('[data-popup=tooltip]').tooltip();
    $('.cRincian').addClass('hide');
});
</script>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/perkiraan/form.js'); ?>"></script>