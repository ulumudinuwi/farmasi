<?php echo messages(); ?>
<form class="form-horizontal" method="POST" id="myForm">
    <input type="hidden" name="uid" value="<?php echo isset($dokter) ? $dokter->uid : '' ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <?php echo isset($sales) ? 'Sales : '.$sales->nama : '' ?>
                    </h6>
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-labeled">
                            <b><i class="icon-floppy-disk"></i></b>
                            Simpan
                        </button>
                        <a href="javascript:window.history.go(-1);" class="btn btn-default cancel-button">
                            Batal
                        </a>
                    </div>
                </div>
                <div class="line"></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-user position-left"></i>
                                        FORM DOKTER
                                    </h6>
                                </div>
                                
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No Dokter
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_member" value="<?php echo isset($dokter) ? $dokter->no_member : '' ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Nama
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="nama" value="<?php echo isset($dokter) ? $dokter->nama : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tempat Lahir
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="tempat_lahir" value="<?php echo isset($dokter) ? $dokter->tempat_lahir : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tanggal Lahir
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control date_mask" name="tgl_lahir" value="<?php echo isset($dokter) ? date_to_view($dokter->tgl_lahir) : '' ?>" onkeyup="global.method.checkDate(this)">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label input-required" for="jenis_kelamin">
                                            Jenis Kelamin
                                        </label>
                                        <div class="col-lg-6 parent_styled">
                                            <div class="col-lg-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="jenis_kelamin" class="styled" value="1" <?php if(isset($dokter) && $dokter->jenis_kelamin == 1) echo 'checked'; ?>> 
                                                        Laki-Laki 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="jenis_kelamin" class="styled" value="0" <?php if(isset($dokter) && $dokter->jenis_kelamin == 0) echo 'checked'; ?>> 
                                                        Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Hp
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_hp" value="<?php echo isset($dokter) ? $dokter->no_hp : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Email
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="email" value="<?php echo isset($dokter) ? $dokter->email : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Alamat
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <textarea name="alamat" class="form-control" rows="3"><?php echo isset($dokter) ? $dokter->alamat : '' ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Area
                                         <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <?php $area_id = isset($dokter) ? $dokter->area : '';?>
                                            <select name="area_id" class="form-control select_2">
                                                <option value="" selected disabled>PILIH AREA</option>
                                                <?php foreach($area_all as $row) { ?>
                                                <?php if(!empty($area_id) && $area_id == $row->id){ ?>
                                                    <option value="<?php echo $row->id ?>" selected><?php echo $row->nama ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $row->id ?>"><?php echo $row->nama ?></option>
                                                <?php }} ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Ktp
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_ktp" value="<?php echo isset($dokter) ? $dokter->no_ktp : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Npwp
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_npwp" value="<?php echo isset($dokter) ? $dokter->no_npwp : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Plafon
                                        </label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="plafon" value="<?php echo isset($dokter) ? $dokter->plafon : '' ?>" id="plafon">
                                            <span class="validate_error text-size-mini"><i class="fa fa-info"></i> Minimal Rp.20.000.000,00</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php $sales_id = isset($dokter) ? $dokter->sales_id : '';?>
                                        <label class="col-lg-4 control-label">
                                            Sales
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <select name="sales_id" class="form-control select_2">
                                                <option value="" selected>PILIH SALES</option>
                                                <?php foreach($sales_all as $id => $name) { ?>
                                                <?php if(!empty($sales_id) && $sales_id == $id){ ?>
                                                    <option value="<?php echo $id ?>" selected><?php echo $name ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                                <?php }} ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label input-required" for="st_diskon_mou">
                                            Diskon Mou
                                        </label>
                                        <div class="col-lg-8 parent_styled">
                                            <div class="col-lg-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="st_diskon_mou" class="styled" value="1" <?php if(isset($dokter) && $dokter->st_diskon_mou == 1) echo 'checked'; ?>> 
                                                        Ya 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="st_diskon_mou" class="styled" value="0" <?php if(isset($dokter) && $dokter->st_diskon_mou == 0) echo 'checked'; ?>> 
                                                        Tidak
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 form_mou">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="diskon_mou" value="<?php echo isset($dokter) ? $dokter->diskon_mou : '' ?>" id="diskon_mou" max="100" <?php if(isset($dokter) && $dokter->st_diskon_mou == 0) echo 'readonly="true"'; ?>>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-info searchBarang" disabled="" type="button">
                                                            <b>%</b>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tanggal Mulai Mou
                                        </label>
                                        <div class="col-lg-8">
                                            <input type="date" class="form-control" name="tgl_mulai_mou" value="<?php echo isset($dokter) ? $dokter->tgl_mulai_mou : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tanggal Jatuh Tempo Mou
                                        </label>
                                        <div class="col-lg-8">
                                            <input type="date" class="form-control" name="tgl_jatuh_tempo" value="<?php echo isset($dokter) ? $dokter->tgl_jatuh_tempo : '' ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-med-i-administration position-left"></i>
                                        FORM KLINIK
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Nama Klinik
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="nama_klinik" value="<?php echo isset($dokter) ? $dokter->nama_klinik : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Tlp Klinik
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_tlp_klinik" value="<?php echo isset($dokter) ? $dokter->no_tlp_klinik : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Alamat Klinik
                                        <!-- <span class="text-danger">*</span>!--></label>
                                        <div class="col-lg-8">
                                            <textarea name="alamat_klinik" class="form-control" rows="3"><?php echo isset($dokter) ? $dokter->alamat_klinik : '' ?></textarea>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-user position-left"></i>
                                        AKUN USER
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Username
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="hidden" class="form-control" name="username_default" value="<?php echo isset($akun_user) ? $akun_user->username : ''; ?>">
                                            <input type="text" class="form-control" name="username" value="<?php echo isset($akun_user) ? $akun_user->username : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Email
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="hidden" class="form-control" name="email_default" value="<?php echo isset($akun_user) ? $akun_user->email : ''; ?>">
                                            <input type="text" class="form-control" name="email" value="<?php echo isset($akun_user) ? $akun_user->email : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Password
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Konfirmasi Password
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-labeled">
                            <b><i class="icon-floppy-disk"></i></b>
                            Simpan
                        </button>
                        <a href="javascript:window.history.go(-1);" class="btn btn-default cancel-button">
                            Batal
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


