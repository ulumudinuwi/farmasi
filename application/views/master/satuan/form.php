<style>

</style>
<form class="form-horizontal" method="post" id="form">
  <div class="panel panel-flat">
    <div class="panel-body">
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
      <legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

      <div class="row">
        <!-- Data -->
        <div class="col-sm-12">
          <fieldset>
            <legend class="text-bold">
              <i class="fa fa-list-alt position-left"></i>
              <strong>Data Satuan</strong>
            </legend>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Nama</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama ...">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Singkatan</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control uc" name="singkatan" id="singkatan" placeholder="Singkatan ...">
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
  
      <!-- Input Hidden -->
      <input type="hidden" name="uid" id="uid" value="" />

      <legend class="text-semibold"> &nbsp;</legend>
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
    </div>
  </div>
</form>
