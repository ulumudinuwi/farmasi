<script>
var uid = "<?php echo $uid; ?>",
form = '#form';
var url = {
  index: "<?php echo site_url('master/kelas'); ?>",
  save: "<?php echo site_url('api/master/kelas/save'); ?>",
  getData: "<?php echo site_url('api/master/kelas/get_data/:UID'); ?>",
}

function fillForm(uid) {
  blockElement(form);
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;

      $("#uid").val(data.uid);
      $("#nama").val(data.nama);
      $("#jenis").val(data.jenis).trigger('change');

      $('.el-hidden').show('slow');
      $(form).unblock();
    }
  });
}

$(form).validate({
  rules: {
    nama: { required: true },
    jenis: { required: true },
  },
  messages: {
    nama: "Nama Diperlukan",
    jenis: "Jenis Diperlukan",
  },
  errorPlacement: function(error, element) {
    var placement = $(element).closest('.input-group');
    if (placement.length > 0) {
        error.insertAfter(placement);
    } else {
        error.insertAfter($(element));
    }
  },
  focusInvalid: true,
  submitHandler: function (form) {
    $('input, textarea, select').prop('disabled', false);
    
    blockElement($(form));
    var formData = $(form).serialize();
    $.ajax({
      data: formData,
      type: 'POST',
      dataType: 'JSON', 
      url: url.save,
      success: function(data){
          $(form).unblock();
          successMessage('Berhasil', "Kategori barang berhasil disimpan.");
          window.location.assign(url.index);
      },
      error: function(data){
          $(form).unblock();
          errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
      }
    });
    return false;
  }
});

$(".cancel-button").click(function () {
  window.location.assign(url.index);
});

if(uid !== "") fillForm(uid);
</script>