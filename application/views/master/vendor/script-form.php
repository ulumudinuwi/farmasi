<script>
var uid = "<?php echo $uid; ?>",
form = '#form';
var url = {
  index: "<?php echo site_url('master/distributor'); ?>",
  save: "<?php echo site_url('api/master/vendor/save'); ?>",
  getData: "<?php echo site_url('api/master/vendor/get_data/:UID'); ?>",
}

function fillForm(uid) {
  blockElement(form);
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;
      $('.el-hidden').show('slow');
      $("#uid").val(data.uid);
      $("#kode").val(data.kode);
      $("#nama").val(data.nama);
      $("#alamat").html(data.alamat).data("wysihtml5").editor.setValue(data.alamat);

      $("#penanggung_jawab").val(data.penanggung_jawab);
      $("#jabatan").val(data.jabatan);
      $("#telepon").val(data.telepon);
      $("#telepon").val(data.telepon);
      $("#fax").val(data.fax);
      $("#email").val(data.email);
      $("#website").val(data.website);

      $("#npwp").val(data.npwp);
      $("#bank").val(data.bank);
      $("#no_rekening").val(data.no_rekening);
      $("#no_rekening_atas_nama").val(data.no_rekening_atas_nama);
      $("#deskripsi").html(data.deskripsi).data("wysihtml5").editor.setValue(data.deskripsi);
      $(form).unblock();
    }
  });
}

$(document).ready(function() {
  $(".wysihtml5-min").wysihtml5({
    parserRules:  wysihtml5ParserRules
  });
  $(".wysihtml5-toolbar").remove();

  $(form).validate({
    rules: {
      nama: { required: true },
    },
    messages: {
      nama: "Nama Diperlukan",
    },
    focusInvalid: true,
    submitHandler: function (form) {
      $('input, textarea, select').prop('disabled', false);
      blockElement($(form));
      var formData = $(form).serialize();
      $.ajax({
        data: formData,
        type: 'POST',
        dataType: 'JSON', 
        url: url.save,
        success: function(data){
            $(form).unblock();
            successMessage('Berhasil', "Data berhasil disimpan.");
            window.location.assign(url.index);
        },
        error: function(data){
            $(form).unblock();
            errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
        }
      });
      return false;
    }
  });

  $(".cancel-button").click(function () {
    window.location.assign(url.index);
  });

  if(uid !== "") fillForm(uid);
});
</script>