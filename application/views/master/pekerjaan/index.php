<?php echo messages(); ?>
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-body uppercase">
			<button type="button" id="btn-refresh" class="btn bg-slate btn-float btn-rounded">
				<b>
					<i class="icon-database-refresh"></i>
				</b>
			</button>
			<div class="no-padding-top">
				<div class="table-responsive">
					<table class="table table-bordered table-striped" id="pekerjaan_table">
						<thead>
							<tr class="bg-slate">
								<th class="text-center">Nama</th>
								<th class="text-center" style="width: 15%;">Status</th>
								<th class="text-center" style="width: 10%;"></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var table,
    btnRefresh;

var url = {
  edit: "<?php echo site_url('master/kelompok_barang/form/:UID'); ?>",
  loadData: "<?php echo site_url('master/pekerjaan/load_data'); ?>",
  updateStatus: "<?php echo site_url('api/master/kelompok_barang/update_status'); ?>",
}

$(window).ready(function () {
  table = $("#pekerjaan_table").DataTable({
   "processing": true,
    "serverSide": true,
    "ajax": {
        "url": url.loadData,
        "type": "POST"
    },
    "columns": [
      { "data": "nama" },
      { 
        "data": "status",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          if (data == 0) 
            return '<button type="button" class="toggle-status-row btn bg-success btn-xs" data-status="1" data-uid="' + row.uid + '">Aktif</button>';
          return '<button type="button" class="toggle-status-row btn bg-slate-400 btn-xs" data-status="0" data-uid="' + row.uid + '">Non Aktif</button>';
        } 
      },
      {
        "data": "uid",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
        },
        "className": "text-center"
      }
    ],
  });

  $("#table").on("click", ".edit-row", function () {
    var uid = $(this).data('uid');
    window.location.assign(url.edit.replace(':UID', uid));
  });

  $("#table").on("click", ".toggle-status-row", function () {
    var btn = $(this);
    btn.prop("disabled", true);
    var uid = $(this).data('uid');
    var statusData = $(this).data('status');

    var title;
    if (parseInt(statusData) === 1) title = "Mengaktifkan data";
    else title = "Menonaktifkan data";

    // Progress loader
    var cur_value = 1;
    var update = false;
    var progress;
    var timer;

    // Make a loader.
    var loader = new PNotify({
      title: title,
      text: '<div class="progress progress-striped active" style="margin:0">\
      <div class="progress-bar bg-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">\
      <span class="sr-only">0%</span>\
      </div>\
      </div>',
      addclass: 'bg-slate',
      icon: 'icon-spinner4 spinner',
      hide: false,
      buttons: {
        closer: true,
        sticker: false
      },
      history: {
        history: false
      },
      before_open: function(PNotify) {
        progress = PNotify.get().find("div.progress-bar");
        progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");

        // Pretend to do something.
        timer = setInterval(function() {
          if (cur_value >= 100) {
            // Remove the interval.
            window.clearInterval(timer);
            
            update = true;
            loader.remove();

            return;
          }
          cur_value += 5;
          progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
        }, 65);
      },
      after_close: function(PNotify, timer_hide) {
        btn.prop("disabled", false);
        clearInterval(timer);
        
        if (update) {
          $.post(url.updateStatus, {uid: uid, status: statusData}, function (data, status) {
            if (status === "success") {

              if (parseInt(statusData) === 0) {
                btn.removeClass('bg-success');
                btn.addClass('bg-slate-400');
                btn.data('status', 0);
                btn.html("Inactive");
                successMessage('Berhasil', 'Data berhasil dinonaktifkan.');
              } else {
                btn.removeClass('bg-slate-400');
                btn.addClass('bg-success');
                btn.data('status', 1);
                btn.html("Active");
                successMessage('Berhasil', 'Data berhasil diaktifkan.');
              }
              table.draw();
            }
          }).fail(function (error) {
            if (parseInt(statusData) === 0) 
              errorMessage('Peringatan', 'Terjadi kesalahan saat menonaktifkan data.');
            else 
              errorMessage('Peringatan', 'Terjadi kesalahan saat mengaktifkan data.');
          });
        }

        update = false;
        
      }
    });
  });

  btnRefresh = $("#btn-refresh");
  btnRefresh.click(function () {
    table.draw();
  });
});

</script>