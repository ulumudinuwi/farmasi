<style>
input[type=text].form-control {
    text-transform: none;
}
.operasi-list {
    font-size: 80%;
    white-space: nowrap;
}
</style>
<form class="form-horizontal" method="post" id="form">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                <!-- Data -->
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong>
                                <?= lang('section_data'); ?>
                                &nbsp;
                                <label class="label label-danger" id="lbl-deleted">DELETED</label>
                            </strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="kode">
                                        <?= lang('field_kode'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="kode" id="kode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="nama">
                                        <?= lang('field_nama'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="nama" id="nama" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="nama">
                                        <?= lang('field_jenis'); ?>
                                    </label>
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="jenis" value="kelompok"> 
                                                <?= lang('field_jenis_kelompok'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="jenis" value="rincian"> 
                                                <?= lang('field_jenis_rincian'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="parent_id">
                                        <?= lang('field_parent_id'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="hidden" id="old_parent_id" name="old_parent_id" value="0" />
                                        <select class="form-control" id="parent_id" name="parent_id">
                                            <?php foreach ($parents as $parent): ?>
                                                <?php if ($parent->jenis == 'Root'): ?>
                                                    <option value="<?= $parent->id; ?>" selected="true">Root</option>
                                                <?php else: ?>
                                                    <option value="<?= $parent->id; ?>">
                                                        <?= $parent->kode.' - '.$parent->nama; ?>
                                                    </option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong><?= lang('field_deskripsi'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="summernote" id="disp_deskripsi"></div>
                                        <textarea id="deskripsi" name="deskripsi" class="hide"></textarea>
                                        <!-- <textarea class="form-control wysihtml5 wysihtml5-min" id="deskripsi" name="deskripsi"></textarea> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <!-- Rincian Pemeriksaan -->
            <div class="row mt-10" id="section-pemeriksaan">
                <div class="col-sm-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-certificate position-left"></i>
                            <strong><?= lang('section_item'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="table_pemeriksaan" class="table table-bordered">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center">
                                                    <?= lang('field_kode'); ?>
                                                </th>
                                                <th class="text-center">
                                                    <?= lang('field_nama'); ?>
                                                </th>
                                                <th class="text-center">
                                                    <?= lang('field_satuan'); ?>
                                                </th>
                                                <th class="text-center" style="width: 30%;">
                                                    <?= lang('field_nilai_normal'); ?>
                                                </th>
                                                <th class="text-center" style="width: 10%;">
                                                    <?= lang('field_status'); ?>
                                                </th>
                                                <th class="text-center" style="width: 15%;">
                                                    <?= lang('field_action'); ?>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6">
                                                    <button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah-item">
                                                        <b><i class="icon-plus-circle2"></i></b>
                                                        <?= lang('btn_add'); ?>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <!-- ./Rincian Pemeriksaan -->

            <!-- Penggunaan BMHP -->
            <div class="row mt-10" id="section-obat">
                <div class="col-sm-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-droplet position-left"></i>
                            <strong><?= lang('section_obat'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="table_obat" class="table table-bordered">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center">
                                                    <?= lang('field_kode'); ?>
                                                </th>
                                                <th class="text-center">
                                                    <?= lang('field_obat'); ?>
                                                </th>
                                                <th class="text-center" style="width: 15%;">
                                                    <?= lang('field_quantity'); ?>
                                                </th>
                                                <th class="text-center" style="width: 10%;">
                                                    <?= lang('field_status'); ?>
                                                </th>
                                                <th class="text-center" style="width: 15%;">
                                                    <?= lang('field_action'); ?>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5">
                                                    <button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah-obat">
                                                        <b><i class="icon-plus-circle2"></i></b>
                                                        <?= lang('btn_add'); ?>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <!-- ./Penggunaan BMHP -->

            <!-- Input Hidden -->
            <input type="hidden" name="id" id="id" value="0" />
            <input type="hidden" name="uid" id="uid" value="0" />

            <legend class="text-semibold"> &nbsp;</legend>
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    <?= lang('btn_save'); ?>
                </button>
                <button type="button" id="btn-restore" class="btn btn-info btn-labeled">
                    <b><i class="icon-reset"></i></b>
                    <?= lang('btn_restore'); ?>
                </button>
                <button type="button" id="btn-delete" class="btn btn-danger btn-labeled">
                    <b><i class="icon-trash"></i></b>
                    <?= lang('btn_delete'); ?>
                </button>
                <button type="button" id="btn-cancel" class="btn btn-default cancel-button">
                    <?= lang('btn_cancel'); ?>
                </button>
            </div>
        </div>
    </div>
</form>

<!-- Modal Pemeriksaan -->
<div id="modal_pemeriksaan_item" class="modal fade" data-backdrop="static">
<form id="form_pemeriksaan_item">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="nilai_normal_form" class="form-horizontal">
                <div class="modal-header bg-slate">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title"><?= lang('section_item'); ?></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Nilai Normal -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <fieldset>
                                <legend class="text-bold">
                                    <i class="icon-clipboard3 position-left"></i>
                                    <strong><?= lang('section_item'); ?></strong>
                                </legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_kode'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control mb-10" name="kode" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_nama'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control mb-10" name="nama" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_satuan'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control mb-10" name="satuan" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="mt-15">
                                <legend class="text-bold">
                                    <i class="icon-clipboard3 position-left"></i>
                                    <strong><?= lang('section_nilai_normal'); ?></strong>
                                </legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="table_item_nn" class="table table-bordered">
                                                <thead>
                                                    <tr class="bg-slate">
                                                        <th class="text-center">
                                                            <?= lang('field_nilai_normal'); ?>
                                                        </th>
                                                        <th class="text-center" style="width: 5%;">
                                                            <?= lang('field_action'); ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="2">
                                                            <button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah-item-nn">
                                                                <b><i class="icon-plus-circle2"></i></b>
                                                                <?= lang('btn_add'); ?>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" />
                    <input type="hidden" name="pemeriksaan_id" />
                    <input type="hidden" name="status" />
                    <input type="hidden" name="tr_selector" />
                    <button type="submit" class="btn btn-success btn-labeled">
                        <b><i class="icon-floppy-disk"></i></b>
                        <?= lang('btn_save'); ?>
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <b><?= lang('btn_close'); ?></b>
                    </button>
                </div>
            </form>
        </div>
    </div>
</form>
</div>
<!-- ./Modal Pemeriksaan -->

<!-- Modal Nilai Normal -->
<div id="modal_nilai_normal" class="modal fade" data-backdrop="static">
<form id="form_nilai_normal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="nilai_normal_form" class="form-horizontal">
                <div class="modal-header bg-slate">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title"><?= lang('section_nilai_normal'); ?></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Nilai Normal -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <fieldset>
                                <legend class="text-bold">
                                    <i class="icon-clipboard3 position-left"></i>
                                    <strong><?= lang('section_nilai_normal'); ?></strong>
                                </legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_datatype'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="datatype">
                                                    <option value="">- Type -</option>
                                                    <option value="range"><?= lang('value_range'); ?></option>
                                                    <option value="boolean"><?= lang('value_boolean'); ?></option>
                                                    <option value="text"><?= lang('value_text'); ?></option>
                                                    <option value="lt"><?= lang('value_lt'); ?></option>
                                                    <option value="gt"><?= lang('value_gt'); ?></option>
                                                    <option value="lte"><?= lang('value_lte'); ?></option>
                                                    <option value="gte"><?= lang('value_gte'); ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Range -->
                                        <div class="form-group group-range row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_value'); ?>
                                            </label>
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control text-right" name="input_range_min" />
                                            </div>
                                            <div class="col-lg-1 form-control-static text-center">
                                                s/d
                                            </div>
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control text-right" name="input_range_max" />
                                            </div>
                                        </div>
                                        <!-- Boolean -->
                                        <div class="form-group group-boolean row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_value'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <label class="radio-inline radio-left">
                                                    <input type="radio" name="input_boolean" value="1">
                                                    <?= lang('value_true'); ?>
                                                </label>
                                                <label class="radio-inline radio-left">
                                                    <input type="radio" name="input_boolean" value="0">
                                                    <?= lang('value_false'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <!-- TEXT -->
                                        <div class="form-group group-text row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_value'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="input_text" />
                                            </div>
                                        </div>
                                        <!-- Less Than (LT) -->
                                        <div class="form-group group-lt row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_value'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="input_lt" />
                                            </div>
                                        </div>
                                        <!-- Greater Than (GT) -->
                                        <div class="form-group group-gt row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_value'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="input_gt" />
                                            </div>
                                        </div>
                                        <!-- Less Than Equal (LTE) -->
                                        <div class="form-group group-lte row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_value'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="input_lte" />
                                            </div>
                                        </div>
                                        <!-- Greater Than Equal (GTE) -->
                                        <div class="form-group group-gte row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_value'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="input_gte" />
                                            </div>
                                        </div>

                                        <div class="form-group row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_label'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="nama">
                                            </div>
                                        </div>

                                        <div class="form-group row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_jenis_kelamin'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="jenis_kelamin">
                                                    <option value="0">- Semua -</option>
                                                    <option value="1"><?= lang('value_jenis_kelamin_1'); ?></option>
                                                    <option value="2"><?= lang('value_jenis_kelamin_2'); ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend class="text-bold">
                                    <i class="icon-clipboard3 position-left"></i>
                                    <strong><?= lang('section_nilai_normal_umur'); ?></strong>
                                </legend>
                                <div class="row">
                                    <!-- Umur MIN -->
                                    <div class="col-md-6">
                                        <div class="form-group row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_min_flag'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <label class="radio-inline radio-left">
                                                    <input type="radio" name="umur_min_flag" value="1">
                                                    <?= lang('value_yes'); ?>
                                                </label>
                                                <label class="radio-inline radio-left">
                                                    <input type="radio" name="umur_min_flag" value="0">
                                                    <?= lang('value_no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group group-umur_min row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_tahun_min'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="umur_min_tahun">
                                            </div>
                                        </div>
                                        <div class="form-group group-umur_min row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_bulan_min'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="umur_min_bulan">
                                            </div>
                                        </div>
                                        <div class="form-group group-umur_min row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_hari_min'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="umur_min_hari">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Umur Max -->
                                    <div class="col-md-6">
                                        <div class="form-group row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_max_flag'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <label class="radio-inline radio-left">
                                                    <input type="radio" name="umur_max_flag" value="1">
                                                    <?= lang('value_yes'); ?>
                                                </label>
                                                <label class="radio-inline radio-left">
                                                    <input type="radio" name="umur_max_flag" value="0">
                                                    <?= lang('value_no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group group-umur_max row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_tahun_max'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="umur_max_tahun">
                                            </div>
                                        </div>
                                        <div class="form-group group-umur_max row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_bulan_max'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="umur_max_bulan">
                                            </div>
                                        </div>
                                        <div class="form-group group-umur_max row mb-5">
                                            <label class="col-lg-4 control-label">
                                                <?= lang('field_umur_hari_max'); ?>
                                            </label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="umur_max_hari">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <p class="disp_nilai_normal pull-left"></p>
                    <input type="hidden" name="id" value="0" />
                    <input type="hidden" name="uid" value="0" />
                    <input type="hidden" name="item_id" value="" />
                    <input type="hidden" name="value" value="" />

                    <button type="submit" class="btn btn-success btn-labeled">
                        <b><i class="icon-floppy-disk"></i></b>
                        <?= lang('btn_save'); ?>
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <b><?= lang('btn_close'); ?></b>
                    </button>
                </div>
            </form>
        </div>
    </div>
</form>
</div>
<!-- ./Modal Nilai Normal -->

<!-- Modal Obat -->
<div id="modal_lookup_obat" class="modal fade" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
            <div class="modal-header bg-slate">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title"><?= lang('modal_lookup_obat_title'); ?></h6>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover" id="table_lookup_obat">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%;"><?= lang('field_pilih'); ?></th>
                            <th class="text-center"><?= lang('field_kode'); ?></th>
                            <th class="text-center"><?= lang('field_nama'); ?></th>
                            <th class="text-center"><?= lang('field_jenis'); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled" id="btn-tambah_lookup_obat_simpan">
                    <b><i class="icon-plus-circle2"></i></b>
                    <?= lang('btn_add'); ?>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- ./Modal Obat -->

<script>
var UID = "<?= $uid; ?>";

/**
 * URLS
 */
var URL = {
    index: "<?= site_url('master/laboratorium/pemeriksaan'); ?>",
    fetchParent: "<?= site_url('api/master/laboratorium/pemeriksaan/fetch_parent'); ?>",
    fetchLookupObat: "<?= site_url('api/master/laboratorium/pemeriksaan/fetch_lookup_obat'); ?>",
    delete: "<?= site_url('api/master/laboratorium/pemeriksaan/delete'); ?>",
    restore: "<?= site_url('api/master/laboratorium/pemeriksaan/restore'); ?>",
    getData: "<?= site_url('api/master/laboratorium/pemeriksaan/get_data/:UID'); ?>",
    save: "<?= site_url('api/master/laboratorium/pemeriksaan/save'); ?>",
};

var FORM = $("#form");

let BTN_RESTORE = $("#btn-restore");
let BTN_DELETE = $("#btn-delete");
let BTN_CANCEL = $("#btn-cancel");

let LABEL_DELETED = $("#lbl-deleted");

let FORM_PEMERIKSAAN_ITEM = $("#form_pemeriksaan_item"),
    FORM_NILAI_NORMAL = $("#form_nilai_normal");

let TABLE_PEMERIKSAAN = $("#table_pemeriksaan"),
    TABLE_OBAT = $("#table_obat"),
    TABLE_ITEM_NILAI_NORMAL = $("#table_item_nn");

// DATATABLE
let TABLE_LOOKUP_OBAT = $("#table_lookup_obat"),
    TABLE_LOOKUP_OBAT_DT,
    TABLE_LOOKUP_OBAT_TIMER;

let MODAL_LOOKUP_OBAT = $("#modal_lookup_obat"),
    MODAL_NILAI_NORMAL = $("#modal_nilai_normal"),
    MODAL_PEMERIKSAAN_ITEM = $("#modal_pemeriksaan_item");

    

let BTN_TAMBAH_PEMERIKSAAN = $("#btn-tambah-item"),
    BTN_TAMBAH_OBAT = $("#btn-tambah-obat"),
    BTN_TAMBAH_NILAI_NORMAL = $("#btn-tambah-item-nn");

let BTN_TAMBAH_LOOKUP_OBAT_SIMPAN = $("#btn-tambah_lookup_obat_simpan");

let LABEL_STATUS_ACTIVE = "<?= lang('status_active'); ?>",
    LABEL_STATUS_INACTIVE = "<?= lang('status_inactive'); ?>";


/**
 * COLUMNS OPTIONS
 */
let COLUMNS_OPT_LOOKUP_OBAT = [
    {
        "orderable": false,
        "render": (data, type, row, meta) => {
            let dataAttr = `data-id="${row.id}" data-uid="${row.uid}" data-kode="${row.kode}" data-nama="${row.nama}"`;

            let checkbox = `<div class="checkbox"><label><input type="checkbox" class="check" ${dataAttr} /></label></div>`;

            return checkbox;
        },
        "className": "text-center"
    },
    {
        "orderable": true,
        "data": "kode",
        "render": (data, type, row, meta) => {
            return data;
        },
        "className": "text-center"
    },
    {
        "orderable": true,
        "data": "nama",
        "render": (data, type, row, meta) => {
            return data;
        },
        "className": "text-left"
    },
    {
        "orderable": true,
        "data": "jenis",
        "render": (data, type, row, meta) => {
            return data;
        },
        "className": "text-left"
    }
];

$(function() {
    $('#disp_deskripsi').summernote();
    $('select').select2();
    $('[data-popup=tooltip]').tooltip();
});
</script>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/laboratorium/pemeriksaan/form.js'); ?>"></script>