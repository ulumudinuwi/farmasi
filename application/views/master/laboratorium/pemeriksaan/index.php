<style>
a.pemeriksaan.active {
    color: #555;
}
a.pemeriksaan.active:hover {
    color: #1E88E5;
}
a.pemeriksaan.inactive {
    color: #a9a9a9;
}
a.pemeriksaan.inactive:hover {
    color: #1E88E5;
}
a.pemeriksaan.deleted {
    text-decoration: line-through;
    color: #a9a9a9;
}
a.pemeriksaan.deleted:hover {
    text-decoration: none;
    color: #1E88E5;
}

.lvl-1 {
    padding-left: 0;
}
.lvl-2 {
    padding-left: 5px;
}
.lvl-3 {
    padding-left: 10px;
}
.lvl-4 {
    padding-left: 15px;
}
.lvl-5 {
    padding-left: 20px;
}
.lvl-6 {
    padding-left: 25px;
}
.lvl-7 {
    padding-left: 30px;
}
</style>
<!-- Basic collapsible -->
<div class="panel-group content-group-lg">
    <div class="row" id="pemeriksaan-container"></div>
</div>
<script>
let URL = {
    form: "<?= site_url('master/laboratorium/pemeriksaan/form/:UID'); ?>",
    fetchData: "<?= site_url('api/master/laboratorium/pemeriksaan/fetch_data'); ?>",
    getData: "<?= site_url('api/master/laboratorium/pemeriksaan/get_data/:UID'); ?>",
};

let CONTAINER = $("#pemeriksaan-container");
</script>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/laboratorium/pemeriksaan/index.js'); ?>"></script>