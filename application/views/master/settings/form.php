<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-lg-4 control-label input-required"><?php echo strtoupper($option->option_name); ?></label>
						<div class="col-lg-7">
							<div class="input-group">
								<textarea class="form-control <?php echo $option->option_table ? 'hide' : ''; ?>" id="option_value" name="option_value" value="<?php echo $option->option_value; ?>" <?php echo $option->option_table ? 'readonly' : ''; ?>><?php echo $option->option_value; ?></textarea>

								<?php
									$addClass = 'hide';
									if($option->option_table) {
										$addClass = '';
										$optVal = getOptionValueToByTableLookup((get_option($option->option_name) ? : 0), $option->option_table);
									}
								?>
								<div class="form-control-static <?php echo $addClass; ?>" id="label-option_value"><?php echo ($optVal ? $optVal->nama : ''); ?></div>
								<span class="input-group-btn <?php echo $addClass; ?>">
									<button type="button" id="btn-table_lookup" class="btn btn-primary">Cari</button>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<!-- Input Hidden -->
			<input type="hidden" name="option_id" value="<?php echo $option->option_id; ?>" />
			<input type="hidden" name="option_name" value="<?php echo $option->option_name; ?>" />
			<input type="hidden" name="option_type" value="text" />
			<input type="hidden" id="option_table" name="option_table" value="<?php echo $option->option_table; ?>" />
			<input type="hidden" name="autoload" value="yes" />
			<input type="hidden" name="user_visibility" value="1" />
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">
                    Batal
                </button>
			</div>
		</div>
	</div>
</form>

<!-- Modal Lookup -->
<div id="modal_lookup" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
            <div class="modal-header bg-slate">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Cari Data</h6>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover" id="table_lookup">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>