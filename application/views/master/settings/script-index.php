<script>
var url = {
    loadData: "<?php echo site_url('api/master/options/load_data?visibility=:VISIBILITY'); ?>",
    form: "<?php echo site_url('master/settings/form?id=:OPTION_ID'); ?>"
};

var table = $("#table"),
    tableDt,
    tableTimer;

tableDt = table.dataTable({
    "sPaginationType": "full_numbers",
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": url.loadData.replace(':VISIBILITY', btoa(1)),
    "columns": [
        {
            "data": "option_name",
            "render": function (data, type, row, meta) {
                var link = url.form.replace(':OPTION_ID', btoa(row.option_id));
                return '<a href="' + link + '">' + data + '</a>';
            },
            "className": "",
        },
        {
            "data": "option_value",
            "render": function (data, type, row, meta) {
                return data;
            },
            "className": ""
        },
        {
            "data": "option_type",
            "visible": false,
        },
        {
            "data": "autoload",
            "visible": false,
        }
    ],
    "order": [ [0, "asc"] ],
    "stateSave": true,
    "stateSaveParams": function (settings, data) {
        data.search.search = "";
        // remove filters
        for (var i = 0; i < data.columns.length; i++) {
            switch (i) {
                default:
                    data.columns[i].search.search = "";
                    break;
            }
        }
    },
    "fnServerData": function ( sSource, aoData, fnCallback ) {
        if (tableTimer) clearTimeout(tableTimer);
        tableTimer = setTimeout(function () {
            blockElement(table.selector);
            $.getJSON( sSource, aoData, function (json) {
                fnCallback(json);
            });
        }, 500);
    },
    "fnDrawCallback": function (oSettings) {
      var n = oSettings._iRecordsTotal;
      
      table.unblock();

      table.find('[data-popup=tooltip]').tooltip();
    }
});

$("#btn-refresh").click(function (e) {
    tableDt.fnDraw(false);
});
</script>