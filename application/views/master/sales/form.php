<?php echo messages(); ?>
<form class="form-horizontal" method="POST" id="myForm">
    <input type="hidden" name="uid" value="<?php echo isset($sales) ? $sales->uid : ''; ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-labeled">
                            <b><i class="icon-floppy-disk"></i></b>
                            Simpan
                        </button>
                        <a href="javascript:window.history.go(-1);" class="btn btn-default cancel-button">
                            Batal
                        </a>
                    </div>
                </div>
                <div class="line"></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-users position-left"></i>
                                        FORM SALES
                                    </h6>
                                </div>
                                
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Nama
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="nama" value="<?php echo isset($sales) ? $sales->nama : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tempat Lahir
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="tempat_lahir" value="<?php echo isset($sales) ? $sales->tempat_lahir : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tanggal Lahir
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control date_mask" name="tgl_lahir" value="<?php echo isset($sales) ? date_to_view($sales->tgl_lahir) : '' ?>" onkeyup="global.method.checkDate(this)">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label input-required" for="jenis_kelamin">
                                            Jenis Kelamin
                                        </label>
                                        <div class="col-lg-6 parent_styled">
                                            <div class="col-lg-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="jenis_kelamin" class="styled" value="1" <?php if(isset($sales) && $sales->jenis_kelamin == 1) echo 'checked'; ?>> 
                                                        Laki-Laki 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="jenis_kelamin" class="styled" value="0" <?php if(isset($sales) && $sales->jenis_kelamin == 0) echo 'checked'; ?>> 
                                                        Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Hp
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_hp" value="<?php echo isset($sales) ? $sales->no_hp : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Kode Promo
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="kode_promo" value="<?php echo isset($sales) ? $sales->kode_promo : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Alamat
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <textarea name="alamat" class="form-control" rows="3"><?php echo isset($sales) ? $sales->alamat : '' ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Ktp
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_ktp" value="<?php echo isset($sales) ? $sales->no_ktp : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tgl.Bergabung
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control date_mask" name="tgl_bergabung" value="<?php echo isset($sales) ? date_to_view($sales->tgl_bergabung) : '' ?>" onkeyup="global.method.checkDate(this)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-user position-left"></i>
                                        AKUN USER
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Username
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="hidden" class="form-control" name="username_default" value="<?php echo isset($akun_user) ? $akun_user->username : ''; ?>">
                                            <input type="text" class="form-control" name="username" value="<?php echo isset($akun_user) ? $akun_user->username : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Email
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="hidden" class="form-control" name="email_default" value="<?php echo isset($akun_user) ? $akun_user->email : ''; ?>">
                                            <input type="text" class="form-control" name="email" value="<?php echo isset($akun_user) ? $akun_user->email : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Password
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Konfirmasi Password
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($method == "store") { ?>
                        <div class="col-md-12">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-calendar position-left"></i>
                                        TARGET SALES <?php echo $tahun ?>
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php foreach($array_bulan as $num_bulan => $name_bulan) { ?>
                                        <div class="col-md-6">
                                            <div class="target_wrapper target_sales <?php echo strtolower($name_bulan); ?>">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <tr class="bg-slate">
                                                            <td colspan="2"><?php echo $name_bulan ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>TARGET</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">Rp.</span>
                                                                    <input type="hidden" class="form-control" name="bulan[]" value="<?php echo strtolower($num_bulan); ?>">
                                                                    <input type="text" class="form-control input-rupiah" name="target_sales[<?php echo strtolower($name_bulan); ?>]" value="0">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }else if($method == "update") { ?>
                        <?php if($new_target_sales) { ?>
                        <div class="col-md-12">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-calendar position-left"></i>
                                        TARGET SALES <?php echo $tahun ?>
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php foreach($array_bulan as $num_bulan => $name_bulan) { ?>
                                        <div class="col-md-6">
                                            <div class="target_wrapper target_sales_add <?php echo strtolower($name_bulan); ?>">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <tr class="bg-slate">
                                                            <td colspan="2"><?php echo $name_bulan ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>TARGET</td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">Rp.</span>
                                                                    <input type="hidden" class="form-control" name="bulan[]" value="<?php echo strtolower($num_bulan); ?>">
                                                                    <input type="text" class="form-control input-rupiah" name="target_sales_add[<?php echo strtolower($name_bulan); ?>]">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                        <div class="col-md-12">
                            <div class="panel panel-default no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-calendar position-left"></i>
                                        TARGET SALES
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs top-divided nav-justified">
                                            <?php foreach($target_sales as $key => $value) { ?>
                                            <li class="<?php if($key == $tahun) echo "active"; ?>"><a href="#<?php echo 'tab_'.$key ?>" data-toggle="tab"><?php echo $key; ?></a></li>
                                            <?php } ?>
                                        </ul>
        
                                        <div class="tab-content">
                                            <?php foreach($target_sales as $key => $value) { ?>
                                            <div class="tab-pane <?php if($key == $tahun) echo "active"; ?>" id="<?php echo 'tab_'.$key ?>">
                                                <?php foreach($value as $data) { ?>
                                                <div class="col-md-6">
                                                    <div class="target_wrapper target_sales_edit <?php echo strtolower($data['tahun']);?>_<?php echo strtolower($data['bulan']);?>">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-slate">
                                                                    <td colspan="2"><?php echo $data['bulan_label'] ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>TARGET</td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">Rp.</span>
                                                                            <input type="hidden" class="form-control" name="target_sales_edit_id[<?php echo strtolower($data['tahun']); ?>_<?php echo strtolower($data['bulan']) ?>]" value="<?php echo strtolower($data['id']); ?>">
                                                                            <input type="text" class="form-control input-rupiah" name="target_sales_edit[<?php echo strtolower($data['tahun']); ?>_<?php echo strtolower($data['bulan']) ?>]" value="<?php echo $data['target']; ?>">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-labeled">
                            <b><i class="icon-floppy-disk"></i></b>
                            Simpan
                        </button>
                        <a href="javascript:window.history.go(-1);" class="btn btn-default cancel-button">
                            Batal
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


