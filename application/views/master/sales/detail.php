<?php echo messages(); ?>
<div class="row">
<div class="panel panel-flat">
    <div class="panel-heading">
        <ul class="nav nav-tabs nav-tabs-component nav-justified">
            <li class="active"><a href="#basic-rounded-justified-tab1" data-toggle="tab"><i class="icon-calendar"></i> HISTORY TRANSAKASI</a></li>
            <li><a href="#basic-rounded-justified-tab2" data-toggle="tab"><i class="icon-credit-card"></i> INSENTIF</a></li>
            <li><a href="#basic-rounded-justified-tab3" data-toggle="tab"><i class="icon-users"></i> DAFTAR MEMBER</a></li>
        </ul>
    </div>
    <!-- <h6 class="panel-title">Justified rounded</h6> -->
    <div class="panel-body">
        <div class="tabbable">
            <div class="tab-content">
                <div class="tab-pane active" id="basic-rounded-justified-tab1">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="detail">
                            <thead>
                                <tr class="bg-slate">
                                    <th>No FBP</th>
                                    <th>Tanggal</th>
                                    <th>Nama Dokter</th>
                                    <th>Nama Barang</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                    <th>Jumlah</th>
                                    <th>Disc</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th colspan="9" class="text-center">TIDAK ADA DATA</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="basic-rounded-justified-tab2">
                    <div class="col-md-12">
                        <div class="tabbable">
                            <ul class="nav nav-tabs top-divided nav-justified">
                                <?php foreach($target_sales as $key => $value) { ?>
                                <li class="<?php if($key == $tahun) echo "active"; ?>"><a href="#<?php echo 'tab_'.$key ?>" data-toggle="tab"><?php echo $key; ?></a></li>
                                <?php } ?>
                            </ul>

                            <div class="tab-content">
                                <?php foreach($target_sales as $key => $value) { ?>
                                <div class="tab-pane <?php if($key == $tahun) echo "active"; ?>" id="<?php echo 'tab_'.$key ?>">
                                    <?php foreach($value as $data) { ?>
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <div class="target_wrapper target_sales_edit <?php echo strtolower($data['tahun']);?>_<?php echo strtolower($data['bulan']);?>">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tr class="bg-slate">
                                                        <td colspan="7"><?php echo $data['bulan_label'] ?></td>
                                                    </tr>
                                                    <tr class="bg-slate">
                                                        <td>Target</td>
                                                        <td>Pencapaian</td>
                                                        <td>Persentase</td>
                                                        <td>Insentif Pendapatan</td>
                                                        <td>Insentif Member Baru</td>
                                                        <td>Insentif Khusus</td>
                                                        <td>Total</td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo 'Rp.'.toRupiah($data['data']['target'])?></td>
                                                        <td><?php echo 'Rp.'.toRupiah($data['data']['pencapaian'])?></td>
                                                        <td><?php echo $data['data']['persentase']?> %</td>
                                                        <td><?php echo 'Rp.'.toRupiah($data['data']['insentif_dari_pendapatan'])?> (<?php echo $data['data']['insentif_pendapatan']?> %)</td>
                                                        <td><?php echo 'Rp.'.toRupiah($data['data']['insentif_dari_pendapatan'])?> (<?php echo $data['data']['insentif_member']?> %)</td>
                                                        <td><?php echo 'Rp.'.toRupiah($data['data']['insentif_khusus'])?></td>
                                                        <td><?php echo 'Rp.'.toRupiah($data['data']['total_insentif_didapat '])?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="basic-rounded-justified-tab3">
                    <div class="table table-responsive">
                        <table class="table table-bordered table-striped" id="member">
                            <thead>
                                <tr class="bg-slate">
                                    <th class="text-center" style="width: 25%">NAMA</th>
                                    <th class="text-center" style="width: 25%">KONTAK</th>
                                    <!-- <th class="text-center" style="width: 20%">NO.TLP</th> -->
                                    <th class="text-center">ALAMAT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(empty($member_sales)) {?>
                                <tr>
                                    <td colspan="3" class="text-uppercase text-center">BELUM ADA SALES</td>
                                </tr>   
                                <?php } else { foreach($member_sales as $data) { ?>
                                <tr>
                                    <td><div class="column_nama"><img src="<?php echo assets_url('img/icon/dokter.png') ?>" alt=""><!-- <a href="<?php echo base_url('master/dokter/form?uid='.$data['uid']) ?>"> --> <?php echo $data['nama']; ?><!-- </a> --></div></td>
                                    <td>
                                        <ul class="column_kontak">
                                            <li><i class="icon-envelop5"></i> <?php echo $data['email']; ?></li>
                                            <li><i class="icon-phone2"></i> <?php echo $data['no_hp']; ?></li>
                                        </ul>
                                    </td>
                                    <td><span class="column_alamat"><i class="icon-map5"></i> <?php echo $data['alamat']; ?></span></td>
                                </tr>
                                <?php } } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    let sales_id = "<?php echo isset($sales) ? $sales->id : 0; ?>";
</script>