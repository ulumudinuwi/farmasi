<?php echo messages(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <ul class="nav nav-tabs nav-tabs nav-justified">
                <li class="active">
                    <a href="#sales_active_tab" table="#sales_active" data-toggle="tab" aria-expanded="true">
                        <i class="icon-menu7 position-left"></i> Sales Active
                    </a>
                </li>
                <li>
                    <a href="#sales_inactive_tab" table="#sales_inactive" data-toggle="tab" aria-expanded="true">
                        <i class="icon-menu7 position-left"></i> Sales Inactive
                    </a>
                </li>
            </ul>
            <div class="panel-body">
                <div class="tabbable">
                    <div class="tab-content">
                        <div class="tab-pane active" id="sales_active_tab" >
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped dataTable no-footer" id="myTable">
                                    <thead class="bg-slate">
                                        <tr>
                                            <th class="text-center" style="width: 25%">NAMA</th>
                                            <th class="text-center" style="width: 25%">KONTAK</th>
                                            <!-- <th class="text-center" style="width: 20%">NO.TLP</th> -->
                                            <th class="text-center">ALAMAT</th>
                                            <th class="text-center" style="width: 15%">STATUS</th>
                                            <th class="text-center" style="width: 2%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="sales_inactive_tab" >
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped dataTable no-footer" id="myTable_inactive">
                                    <thead class="bg-slate">
                                        <tr>
                                            <th class="text-center" style="width: 25%">NAMA</th>
                                            <th class="text-center" style="width: 25%">KONTAK</th>
                                            <!-- <th class="text-center" style="width: 20%">NO.TLP</th> -->
                                            <th class="text-center">ALAMAT</th>
                                            <th class="text-center" style="width: 15%">STATUS</th>
                                            <th class="text-center" style="width: 2%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>