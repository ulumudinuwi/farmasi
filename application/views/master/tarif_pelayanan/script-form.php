<script>
/*
var uid = "<?php echo $uid; ?>",
form = '#form';
var url = {
	index: "<?php echo site_url('master/tarif_pelayanan'); ?>",
	save: "<?php echo site_url('api/master/tarif_pelayanan/save'); ?>",
	getData: "<?php echo site_url('api/master/tarif_pelayanan/get_data/:UID'); ?>",
}

function fillForm(uid) {
	blockElement(form);
	$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
		if (status === 'success') {
			data = data.data;

			$("#uid").val(data.uid);
			$("#kode").val(data.kode);
			$("#nama").val(data.nama);
			$("#jenis_barang_id").val(data.jenis_barang_id).trigger('change');

			$('.el-hidden').show('slow');
			$(form).unblock();
		}
	});
}

$(form).validate({
  rules: {
    nama: { required: true },
    jenis_barang_id: { required: true },
  },
  messages: {
    nama: "Nama Diperlukan",
    jenis_barang_id: "Jenis Diperlukan",
  },
  errorPlacement: function(error, element) {
    var placement = $(element).closest('.input-group');
    if (placement.length > 0) {
        error.insertAfter(placement);
    } else {
        error.insertAfter($(element));
    }
  },
  focusInvalid: true,
  submitHandler: function (form) {
    $('input, textarea, select').prop('disabled', false);
    
    blockElement($(form));
    var formData = $(form).serialize();
    $.ajax({
      data: formData,
      type: 'POST',
      dataType: 'JSON', 
      url: url.save,
      success: function(data){
          $(form).unblock();
          successMessage('Berhasil', "Kategori barang berhasil disimpan.");
          window.location.assign(url.index);
      },
      error: function(data){
          $(form).unblock();
          errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
      }
    });
    return false;
  }
});

$(".cancel-button").click(function () {
	window.location.assign(url.index);
});

if(uid !== "") fillForm(uid);
*/
</script>
<script type="text/javascript">
	var url_load_lookup_tarif_pelayanan = '<?php echo site_url("api/master/tarif_pelayanan/load_lookup_tarif_pelayanan"); ?>';
	var url_index = '<?php echo site_url("master/tarif_pelayanan"); ?>';
	var url_simpan = '<?php echo site_url("api/master/tarif_pelayanan/save"); ?>';
	
	
	var url_load_data_pasien = '<?php echo site_url("kasir/kasir/load_data_pasien"); ?>';
	var url_daftar_pasien = '<?php echo site_url("kasir/kasir"); ?>';
	var url_daftar_piutang = '<?php echo site_url("kasir/kasir/daftar_piutang"); ?>';
	var url_daftar_bayar = '<?php echo site_url("kasir/daftar_bayar"); ?>';
	var url_get_perkiraan_bank = '<?php echo site_url("kasir/kasir_ajax_call/get_perkiraan_bank"); ?>';
	var url_get_data = '<?php echo site_url("kasir/kasir_ajax_call/get_data"); ?>';
	
	var url_hapus = '<?php echo site_url("kasir/kasir_ajax_call/delete"); ?>';
	var uid = '<?php echo $uid; ?>';
	var pelayanan_uid = '<?php echo $uid; ?>';
</script>
<script type="text/javascript" src="<?php echo base_url('assets\js\pages\scripts\master\tarif_pelayanan\edit.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		Edit.init();
	});
</script>