<style>
	.heading-elements {
		right: 20px;
	}
</style>
<form class="form-horizontal" method="post" id="tarif_pelayanan_form">
	<div class="panel panel-white">
		<div class="panel-heading">
			<h5 class="panel-title">Tarif Pelayanan</h5>
			<div class="heading-elements">
				<div class="heading-btn">
					<button class="btn btn-success btn-labeled" type="submit" id="simpan_1_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
					<button class="btn btn-default" type="button" id="batal_1_button"> Kembali </button>
				</div>
			</div>
			<a class="heading-elements-toggle"><i class="icon-more"></i></a>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-3 control-label input-required">Kode</label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="kode" name="kode" maxlength="20" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label input-required">Nama</label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="nama" name="nama" maxlength="60" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label input-required">Kelas</label>
						<div class="col-md-9">
							<div class="checkbox">
								<label>
									<input class="styled" type="checkbox" id="disp_kelas" value="1" />
								</label>
							</div>
						</div>
					</div>
					<div class="form-group" id="bedah_section">
						<label class="col-md-3 control-label input-required">Bedah</label>
						<div class="col-md-9">
							<div class="checkbox">
								<label>
									<input class="styled" type="checkbox" id="disp_bedah" value="1" />
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-3 control-label input-required">Unit</label>
						<div class="col-md-9">
							<select class="select" multiple="multiple" >
								<?php
									foreach ($layanan_list as $layanan) {
										echo '<option value="'.$layanan->id.'">'.$layanan->nama.'</option>';
									}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" id="kelas_section" style="display:none;">
						<label class="col-md-4 control-label input-required">Kelas</label>
						<div class="col-md-6">
							<select class="form-control" id="disp_kelas_id">
								<option>VVIP</option>
								<option>VIP</option>
								<option>I</option>
								<option>II</option>
								<option>III</option>
							</select>
						</div>
						<div class="col-md-2">
							<button class="btn btn-info" type="submit" id="simpan_1_button"> ... </button>
						</div>
					</div>
					<div class="form-group" id="golongan_operasi_section" style="display:none;">
						<label class="col-md-4 control-label input-required">Golongan Operasi</label>
						<div class="col-md-6">
							<select class="form-control" id="disp_golongan_operasi">
								<option>Kecil</option>
								<option>Sedang</option>
								<option>Besar</option>
								<option>Khusus</option>
							</select>
						</div>
						<div class="col-md-2">
							<button class="btn btn-info" type="submit" id="simpan_1_button"> ... </button>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<button class="btn btn-info" type="button" id="komponen_button"> Komponen </button>
					<div class="form-group" id="kelas_section">
						<label class="col-md-4 control-label input-required">Cara Pembayaran</label>
						<div class="col-md-8">
							<select class="form-control" id="cara_bayar_id">
							<?php
								$i = 0;
								foreach ($cara_bayar_list as $cara_bayar) {
									if ($i === 0) {
										echo '<option value="'.$cara_bayar->id.'" selected="selected">'.$cara_bayar->nama.'</option>';
									}
									else {
										echo '<option value="'.$cara_bayar->id.'">'.$cara_bayar->nama.'</option>';
									}
									$i++;
								}
							?>
							</select>
						</div>
					</div>
					<table class="table">
						<thead>
							<tr>
								<th style="padding-left:0px;">Komponen</th>
								<th style="padding-left:0px;">Tarif</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach ($komponen_tarif_list as $komponenTarif) {
							?>
							<tr style="border:none;" data-save="0">
								<td style="padding:2px;border:none;"><?php echo $komponenTarif->nama; ?></td>
								<td style="padding:2px;border:none;"><input class="form-control komponen-tarif" type="text" id="disp_komponen_tarif_<?php echo $komponenTarif->id; ?>" value="0" data-id="<?php echo $komponenTarif->id; ?>" style="text-align:right;" /></td>
							</tr>
							<?php
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="heading-elements">
				<div class="heading-btn pull-right">
					<button class="btn-success btn-labeled btn" type="submit" id="simpan_2_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
					<button class="btn btn-default" type="button" id="batal_2_button"> Kembali </button>
				</div>
			</div>
			<a class="heading-elements-toggle"><i class="icon-more"></i></a>
		</div>
		<div>
			<table id="komponen_tarif_table">
				<tbody></tbody>
			</table>
			<input type="hidden" id="id" name="id" value="<?php echo isset($tarifPelayanan->id) ? $tarifPelayanan->id : 0; ?>" />
			<input type="hidden" id="uid" name="uid" value="<?php echo isset($tarifPelayanan->uid) ? $tarifPelayanan->uid : ""; ?>" />
			<input type="hidden" id="kelas" name="kelas" value="<?php echo isset($tarifPelayanan->kelas) ? $tarifPelayanan->kelas : 0; ?>" />
			<input type="hidden" id="bedah" name="bedah" value="<?php echo isset($tarifPelayanan->bedah) ? $tarifPelayanan->bedah : 0; ?>" />
		</div>
	</div>
</form>
<!--
Nama:  Tindakan 1
Kelas: Y
Bedah: Y
Pelayanan ?:  Poli X
			  IGD
			  Rawat Inap
			
Kelas: kelas 1 ?				 Komponen Tarif:
Operasi: Besar ?				 Jasa Sarana:		...
								 Dokter:			...
								 Intensif Karyawan:	...
								 Rumah Sakit:		...
								 Rekam Medis:		...
-->