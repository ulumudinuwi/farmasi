<style>

</style>
<form class="form-horizontal" method="post" id="form">
  <div class="panel panel-flat">
    <div class="panel-body">
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
      <legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

      <div class="row">
        <!-- Data -->
        <div class="col-sm-12">
          <fieldset>
            <legend class="text-bold">
              <i class="fa fa-list-alt position-left"></i>
              <strong>Data Promo</strong>
            </legend>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Nama Barang</label>
                  <div class="col-lg-8">
                    <input type="hidden" class="input-barang_id" name="barang_id" value="" id="barang_id">
                    <div class="input-group">
                      <input class="form-control disp_kode_barang valid" readonly="readonly" placeholder="Cari Barang" value="" aria-invalid="false">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-primary cari-barang" data-id="row-barang-1"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Quantity Barang</label>
                  <div class="col-lg-8">
                    <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                        <input type="checkbox" name="cek_promo" id="cek_promo" class="styled" value="1">
                    </span>
                      <input type="text" name="qty_promo" id="qty_promo" class="form-control input-number" disabled="true">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Harga Jual </label>
                  <div class="col-lg-8">
                    <div class="input-group">
                      <input type="text" name="harga_jual" id="harga_jual" class="form-control input-decimal">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Deskripsi</label>
                  <div class="col-lg-8">
                    <div class="input-group">
                     <textarea class="form-control" name="deskripsi" id="deskripsi"></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label input-required">Tanggal Expired</label>
                    <div class="col-lg-8">
                        <div class="input-group">
                            <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                                <i class="icon-calendar22"></i>
                            </span>
                            <input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4 input-required">Type</label>
                  <div class="col-md-8">
                    <div class="input-group">
                      <select class="form-control" id="type" name="type">
                        <option value="">- Pilih Type -</option>
                        <option value="2">News</option>
                        <option value="1">Promo</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-lg-4 control-label">Banner Promo</label>
                  <div class="col-lg-8">
                    <input type="file" class="form-control" name="banner_id" id="banner_id" accept="image/*">
                    <input type="hidden" class="form-control" name="old_banner_id" id="old_banner_id" accept="image/*">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-12">
                    <img src="" width="100%" id="priviewImg">
                  </div>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">
                  <label class="col-lg-12 control-label">Deskripsi Promo</label>
                  <div class="col-lg-12">
                    <textarea class="form-control wysihtml5-min" name="long_description" id="long_description"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
  
      <!-- Input Hidden -->
      <input type="hidden" name="uid" id="uid" value="" />

      <legend class="text-semibold"> &nbsp;</legend>
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
    </div>
  </div>
</form>
<?php $this->load->view('master/promo/modal-barang'); ?>