<style>
input[type=text].form-control {
    text-transform: none;
}
.operasi-list {
    font-size: 80%;
    white-space: nowrap;
}
</style>
<form class="form-horizontal" method="post" id="form">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                <!-- Data -->
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong>
                                <?= lang('section_data'); ?>
                                &nbsp;
                                <label class="label label-danger" id="lbl-deleted">DELETED</label>
                            </strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="kode">
                                        <?= lang('field_kode'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="kode" id="kode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="nama">
                                        <?= lang('field_nama'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="nama" id="nama" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="nama">
                                        <?= lang('field_jenis'); ?>
                                    </label>
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="jenis" value="kelompok"> 
                                                <?= lang('field_jenis_kelompok'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="jenis" value="rincian"> 
                                                <?= lang('field_jenis_rincian'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="parent_id">
                                        <?= lang('field_parent_id'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="hidden" id="old_parent_id" name="old_parent_id" value="0" />
                                        <select class="form-control" id="parent_id" name="parent_id">
                                            <?php foreach ($parents as $parent): ?>
                                                <?php if ($parent->jenis == 'Root'): ?>
                                                    <option value="<?= $parent->id; ?>" selected="true">Root</option>
                                                <?php else: ?>
                                                    <option value="<?= $parent->id; ?>">
                                                        <?= $parent->kode.' - '.$parent->nama; ?>
                                                    </option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong><?= lang('field_deskripsi'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="summernote" id="disp_deskripsi"></div>
                                        <textarea id="deskripsi" name="deskripsi" class="hide"></textarea>
                                        <!-- <textarea class="form-control wysihtml5 wysihtml5-min" id="deskripsi" name="deskripsi"></textarea> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <!-- Rincian Pemeriksaan -->
            <div class="row mt-10" id="section-ukuran_foto">
                <div class="col-sm-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-certificate position-left"></i>
                            <strong><?= lang('section_ukuran_foto'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="table_ukuran_foto" class="table table-bordered">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center" style="width: 10%;">
                                                    <?= lang('field_select'); ?>
                                                </th>
                                                <th class="text-center">
                                                    <?= lang('field_kode'); ?>
                                                </th>
                                                <th class="text-center">
                                                    <?= lang('field_ukuran'); ?>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <!-- ./Rincian Pemeriksaan -->

            <!-- Penggunaan BMHP -->
            <div class="row mt-10" id="section-obat">
                <div class="col-sm-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-droplet position-left"></i>
                            <strong><?= lang('section_obat'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="table_obat" class="table table-bordered">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center">
                                                    <?= lang('field_kode'); ?>
                                                </th>
                                                <th class="text-center">
                                                    <?= lang('field_obat'); ?>
                                                </th>
                                                <th class="text-center" style="width: 15%;">
                                                    <?= lang('field_quantity'); ?>
                                                </th>
                                                <th class="text-center" style="width: 10%;">
                                                    <?= lang('field_status'); ?>
                                                </th>
                                                <th class="text-center" style="width: 15%;">
                                                    <?= lang('field_action'); ?>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5">
                                                    <button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah-obat">
                                                        <b><i class="icon-plus-circle2"></i></b>
                                                        <?= lang('btn_add'); ?>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <!-- ./Penggunaan BMHP -->

            <!-- Input Hidden -->
            <input type="hidden" name="id" id="id" value="0" />
            <input type="hidden" name="uid" id="uid" value="0" />

            <legend class="text-semibold"> &nbsp;</legend>
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    <?= lang('btn_save'); ?>
                </button>
                <button type="button" id="btn-restore" class="btn btn-info btn-labeled">
                    <b><i class="icon-reset"></i></b>
                    <?= lang('btn_restore'); ?>
                </button>
                <button type="button" id="btn-delete" class="btn btn-danger btn-labeled">
                    <b><i class="icon-trash"></i></b>
                    <?= lang('btn_delete'); ?>
                </button>
                <button type="button" id="btn-cancel" class="btn btn-default cancel-button">
                    <?= lang('btn_cancel'); ?>
                </button>
            </div>
        </div>
    </div>
</form>

<!-- Modal Obat -->
<div id="modal_lookup_obat" class="modal fade" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        
            <div class="modal-header bg-slate">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title"><?= lang('modal_lookup_obat_title'); ?></h6>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover" id="table_lookup_obat">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 5%;"><?= lang('field_pilih'); ?></th>
                            <th class="text-center"><?= lang('field_kode'); ?></th>
                            <th class="text-center"><?= lang('field_nama'); ?></th>
                            <th class="text-center"><?= lang('field_jenis'); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-labeled" id="btn-tambah_lookup_obat_simpan">
                    <b><i class="icon-plus-circle2"></i></b>
                    <?= lang('btn_add'); ?>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- ./Modal Obat -->

<script>
var UID = "<?= $uid; ?>";

/**
 * URLS
 */
var URL = {
    index: "<?= site_url('master/radiologi/pemeriksaan'); ?>",
    fetchUkuranFoto: "<?= site_url('api/master/radiologi/pemeriksaan/fetch_ukuran_foto'); ?>",
    fetchParent: "<?= site_url('api/master/radiologi/pemeriksaan/fetch_parent'); ?>",
    fetchLookupObat: "<?= site_url('api/master/radiologi/pemeriksaan/fetch_lookup_obat'); ?>",
    delete: "<?= site_url('api/master/radiologi/pemeriksaan/delete'); ?>",
    restore: "<?= site_url('api/master/radiologi/pemeriksaan/restore'); ?>",
    getData: "<?= site_url('api/master/radiologi/pemeriksaan/get_data/:UID'); ?>",
    save: "<?= site_url('api/master/radiologi/pemeriksaan/save'); ?>",
};

var FORM = $("#form");

let BTN_RESTORE = $("#btn-restore");
let BTN_DELETE = $("#btn-delete");
let BTN_CANCEL = $("#btn-cancel");

let LABEL_DELETED = $("#lbl-deleted");

let TABLE_UKURAN_FOTO = $("#table_ukuran_foto");
let TABLE_OBAT = $("#table_obat");

// DATATABLE
let TABLE_LOOKUP_OBAT = $("#table_lookup_obat"),
    TABLE_LOOKUP_OBAT_DT,
    TABLE_LOOKUP_OBAT_TIMER;

let MODAL_LOOKUP_OBAT = $("#modal_lookup_obat");

    

let BTN_TAMBAH_OBAT = $("#btn-tambah-obat");

let BTN_TAMBAH_LOOKUP_OBAT_SIMPAN = $("#btn-tambah_lookup_obat_simpan");

let LABEL_STATUS_ACTIVE = "<?= lang('status_active'); ?>",
    LABEL_STATUS_INACTIVE = "<?= lang('status_inactive'); ?>";


/**
 * COLUMNS OPTIONS
 */
let COLUMNS_OPT_LOOKUP_OBAT = [
    {
        "orderable": false,
        "render": (data, type, row, meta) => {
            let dataAttr = `data-id="${row.id}" data-uid="${row.uid}" data-kode="${row.kode}" data-nama="${row.nama}"`;

            let checkbox = `<div class="checkbox"><label><input type="checkbox" class="check" ${dataAttr} /></label></div>`;

            return checkbox;
        },
        "className": "text-center"
    },
    {
        "orderable": true,
        "data": "kode",
        "render": (data, type, row, meta) => {
            return data;
        },
        "className": "text-center"
    },
    {
        "orderable": true,
        "data": "nama",
        "render": (data, type, row, meta) => {
            return data;
        },
        "className": "text-left"
    },
    {
        "orderable": true,
        "data": "jenis",
        "render": (data, type, row, meta) => {
            return data;
        },
        "className": "text-left"
    }
];

$(function() {
    $('#disp_deskripsi').summernote();
    $('select').select2();
    $('[data-popup=tooltip]').tooltip();
});
</script>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/radiologi/pemeriksaan/form.js'); ?>"></script>