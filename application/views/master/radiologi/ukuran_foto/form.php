<style>
input[type=text].form-control {
    text-transform: none;
}
</style>
<form class="form-horizontal" method="post" id="form">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                <!-- Data -->
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong><?= lang('section_data'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="kode">
                                        <?= lang('field_kode'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="kode" id="kode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label" for="nama">
                                        <?= lang('field_nama'); ?>
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="nama" id="nama" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <fieldset>
                        <legend class="text-bold">
                            <i class="icon-clipboard3 position-left"></i>
                            <strong><?= lang('field_deskripsi'); ?></strong>
                        </legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="summernote" id="disp_deskripsi"></div>
                                        <textarea id="deskripsi" name="deskripsi" class="hide"></textarea>
                                        <!-- <textarea class="form-control wysihtml5 wysihtml5-min" id="deskripsi" name="deskripsi"></textarea> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <!-- Input Hidden -->
            <input type="hidden" name="id" id="id" value="0" />
            <input type="hidden" name="uid" id="uid" value="0" />

            <legend class="text-semibold"> &nbsp;</legend>
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-labeled">
                    <b><i class="icon-floppy-disk"></i></b>
                    <?= lang('btn_save'); ?>
                </button>
                <button type="button" id="btn-restore" class="btn btn-info btn-labeled">
                    <b><i class="icon-reset"></i></b>
                    <?= lang('btn_restore'); ?>
                </button>
                <button type="button" id="btn-delete" class="btn btn-danger btn-labeled">
                    <b><i class="icon-trash"></i></b>
                    <?= lang('btn_delete'); ?>
                </button>
                <button type="button" id="btn-cancel" class="btn btn-default cancel-button">
                    <?= lang('btn_cancel'); ?>
                </button>
            </div>
        </div>
    </div>
</form>

<script>
let UID = "<?= $uid; ?>";
let URL = {
    index: "<?= site_url('master/radiologi/ukuran_foto'); ?>",
    getData: "<?= site_url('api/master/radiologi/ukuran_foto/get_data/:UID'); ?>",
    save: "<?= site_url('api/master/radiologi/ukuran_foto/save'); ?>",
    delete: "<?= site_url('api/master/radiologi/ukuran_foto/delete'); ?>",
    restore: "<?= site_url('api/master/radiologi/ukuran_foto/restore'); ?>",
};

let FORM = $("#form");

let BTN_RESTORE = $("#btn-restore");
let BTN_DELETE = $("#btn-delete");
let BTN_CANCEL = $("#btn-cancel");

$(function() {
    $('#disp_deskripsi').summernote();
    $('select').select2();
    $('[data-popup=tooltip]').tooltip();
});
</script>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/radiologi/ukuran_foto/form.js'); ?>"></script>