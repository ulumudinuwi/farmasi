<?php echo messages(); ?>
<style type="text/css">
    #btn-refresh {
        position: fixed;
        bottom: 20px;
        right: 20px;
        z-index: 10000;
    }
    a.disabled {
        pointer-events: none;
    }
    .text-white {
        color: white !important;
    }
    table small {
        font-size: 75%;
    }
    td.alamat {
        font-size: 75%;
        vertical-align: top;
    }
    .heading-btn-group{
        display: none;
    }
</style>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body uppercase">
            <button type="button" id="btn-refresh" class="btn bg-slate btn-float btn-rounded">
                <b>
                    <i class="icon-database-refresh"></i>
                </b>
            </button>
            <div class="no-padding-top">
                <div class="table-responsive">
                    <table id="table" class="table table-bordered table-striped small">
                        <thead>
                            <tr class="bg-slate">
                                <th class="text-center">
                                    Nama Pemilik
                                </th>
                                <th class="text-center">
                                    Nama Bank
                                </th>
                                <th class="text-center">
                                    Nomor Rekening
                                </th><!-- 
                                <th class="text-center" style="width: 15%;">
                                    &nbsp;
                                </th> -->
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
let URL = {
    form: "<?= site_url('master/no_rekening/form/:UID'); ?>",
    loadData: "<?= site_url('api/master/no_rekening/load_data'); ?>",
    delete: "<?= site_url('api/master/no_rekening/delete'); ?>",
    restore: "<?= site_url('api/master/no_rekening/restore'); ?>",
};

let TABLE = $("#table"),
    TABLEDT,
    TABLE_TIMER;

let BTN_REFRESH = $("#btn-refresh");
</script>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/no_rekening/index.js'); ?>"></script>