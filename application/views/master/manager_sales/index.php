<?php echo messages(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped dataTable no-footer" id="myTable">
                        <thead class="bg-slate">
                            <tr>
                                <th class="text-center" style="width: 25%">NAMA</th>
                                <th class="text-center" style="width: 25%">KONTAK</th>
                                <!-- <th class="text-center" style="width: 20%">NO.TLP</th> -->
                                <th class="text-center">ALAMAT</th>
                                <th class="text-center" style="width: 15%">STATUS</th>
                                <th class="text-center" style="width: 2%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>