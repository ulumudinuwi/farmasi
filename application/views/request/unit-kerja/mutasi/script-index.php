<script type="text/javascript">
  var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
  var table,
      detailModal = $('#detail-modal');
  
  var url = {
    loadData : "<?php echo site_url('api/request/unit_kerja/mutasi/load_data'); ?>",
    getData : "<?php echo site_url('api/request/unit_kerja/mutasi/get_data/:UID'); ?>",
    cetak : "<?php echo site_url('api/request/unit_kerja/mutasi/cetak/:UID'); ?>",
    listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID&modul=request'); ?>",
    edit : "<?php echo site_url('request/mutasi_unit/edit/:UID'); ?>",
  };

  function subsDate(range, tipe) {
    let date = range.substr(0, 10);
    if(tipe === "sampai") date = range.substr(13, 10);
    return getDate(date);
  }

  function listen(q) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('request-' + q, function(e) {
      table.draw(false);
    }, false);
  }

  function showDetail(uid) {
    $.getJSON(url.getData.replace(':UID', uid), function (data, status) {
      if (status === 'success') {
        data = data.data;

        detailModal.find(".detail_kode").html(data.kode);
        detailModal.find(".detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
        detailModal.find(".detail_pemohon").html(data.label_pemohon);
        detailModal.find(".detail_status").html(data.status_desc);
        detailModal.find(".detail_keterangan").html(data.keterangan);
        
        var isDTable = $.fn.dataTable.isDataTable(detailModal.find('.table-detail'));
        if(isDTable === true) detailModal.find('.table-detail').DataTable().destroy();

        detailModal.find('.table-detail').DataTable({
            "ordering": false,
            "processing": true,
            "aaData": data.details,
            "columns": [
              { 
                "data": "barang",
                "render": function (data, type, row, meta) {
                    let tmp = data;
                    tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
                    return tmp;
                },
              },
              { "data": "satuan" },
              { 
                  "data": "qty",
                  "render": function (data, type, row, meta) {
                      return `<span class="label-qty">${numeral(data).format('0.0,')}</span>`;
                  },
                  "className": "text-right"
              },
            ],
        });
        detailModal.modal('show');
      }
    });
  }

  function handleLoadTable() {
    table = $("#table").DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": url.loadData,
          "type": "POST",
          "data": function(p) {
              p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
              p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
              p.unit_id = $('#search_unit').val();
              p.status = $('#search_status').val();
          }
      },
      "order": [1, "desc"],
      "columns": [
        { 
          "data": "kode",
          "render": function (data, type, row, meta) {
              let tmp = '<a class="show-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + data + '</a>';
              tmp += `<br/><span class="text-size-mini text-info"><b>Unit:</b><br/> ${row.unit_kerja}</span>`;
              return tmp;
          },
        },
        {
          "data": "tanggal",
          "render": function (data, type, row, meta) {
            let tmp = moment(data).format('DD-MM-YYYY HH:mm');
            tmp += `<br/><span class="text-size-mini text-info"><b>Mutasi Oleh:</b><br/> ${row.pemohon}</span>`;
            return tmp;
          },
          "searchable": false,
        },
        { 
          "data": "status_desc",
          "orderable": false,
          "searchable": false,
        },
        { 
          "data": "status",
          "orderable": false,
          "searchable": false,
          "render": function (data, type, row, meta) {
            let tmp = '';
            switch(parseInt(data)) {
              case 2:
                tmp += `&mdash;`;
                break;
              default:
                tmp += '<a href="' + url.edit.replace(':UID', row.uid) + '" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" data-placement="top"><i class="fa fa-edit"></i></a>';
                break;
            }
            return tmp;
          },
          "className": "text-center",
        },
      ],
      "fnDrawCallback": function (oSettings) {
        $('[data-toggle=tooltip]').tooltip();
      },
    });
  }

  $(window).ready(function() {

    $(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(tanggalDari),
        endDate: moment(tanggalSampai),
    });
    handleLoadTable();

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-1':
            table.draw(false);
            break;
        }
    });

    $("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn_search_tanggal").click(function () {
        $("#search_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_range_tanggal, #search_unit, #search_status").on('change', function() {
      table.draw();
    });

    $("#table").on("click", ".show-row", function () {
      let uid = $(this).data('uid');
      showDetail(uid);
    });

    listen("<?php echo $tabUid; ?>");
  });
</script>