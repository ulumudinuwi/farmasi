<style type="text/css">
.table > thead > tr > th, .table > tbody > tr > td {
    white-space: nowrap;
    vertical-align: middle;
}

#btn-group_float {
	position: fixed;
	bottom: 10%;
	right: 20px;
	z-index: 10000;
	text-align: right;
}
</style>
<div id="btn-group_float" style="display: none;">
	<p>
		<button type="button" id="btn-print-excel" class="btn bg-success btn-float btn-rounded" data-popup="popover" title="Export to Excel" data-trigger="hover" data-content="Klik tombol ini untuk mengexport laporan dalam bentuk Excel." data-placement="left" data-delay="600">
			<b><i class="icon-file-excel"></i></b>
		</button>
	</p>
	<p>
		<button type="button" id="btn-print-pdf" class="btn bg-orange btn-float btn-rounded" data-popup="popover" title="Print PDF" data-trigger="hover" data-content="Klik tombol ini untuk mencetak laporan dalam bentuk PDF." data-placement="left" data-delay="600">
			<b><i class="icon-file-pdf"></i></b>
		</button>
	</p>
</div>
<div class="panel panel-flat">
	<div class="panel-body form-horizontal"> 
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3" for="laporan">Laporan</label>
					<div class="col-md-9">
						<div class="input-group" style="width: 100%";>
							<select class="form-control select" id="laporan">
								<option value="">- Pilih Laporan -</option>
								<option value="laporan_001">1. Stock Barang</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="section-laporan" class="panel panel-flat" style="display: none;">
	<div class="panel-body form-horizontal">
		<div class="row">
			<div class="col-md-12">
				<div id="div-laporan"></div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-print" tabindex="-1" role="dialog" aria-labelledby="modal-printLabel">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<div class="close">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
				<h4 class="modal-title">Print Preview</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>