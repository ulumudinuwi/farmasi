<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
.text-center {
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center"><?= get_option('rs_nama') ? strtoupper(get_option('rs_nama')) : "IMEDIS"; ?></h3>
<h4 class="text-center"><?php echo $title; ?></h4>
<h4 class="text-center"><?php echo strtoupper(lang('pabrik_label')); ?>: <?php echo strtoupper($pabrik); ?></h4>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($current_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th>NO.</th>
			<th>KODE</th>
			<th>NAMA BARANG</th>
			<th>SATUAN</th>
			<th>STOCK</th>
			<th>NILAI RP</th>
			<th>SUB TOTAL</th>
			<th>EXP. DATE</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row):
					$row->harga_penjualan = (int) $row->harga_penjualan != 0 ? $row->harga_penjualan : getHargaDasar($row->id, 'gudang_farmasi');
		            $row->sub_total = $row->harga_penjualan * $row->stock;
		            $row->expired_date = str_replace(';', '<br/>', $row->expired_date);
		?>
		<tr>
			<td class="text-center"><?php echo $no; ?></td>
			<td class="text-center"><?php echo $row->kode; ?></td>
			<td><?php echo $row->nama; ?></td>
			<td class="text-center"><?php echo $row->satuan; ?></td>
			<td style="text-align: right;"><?php echo number_format($row->stock, 2, ",", "."); ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->harga_penjualan, 2, ",", "."); ?></td>
			<td style="text-align: right;">Rp. <?php echo number_format($row->sub_total, 2, ",", "."); ?></td>
			<td class="text-center"><?php echo $row->expired_date ? : '&mdash;'; ?></td>
		</tr>
		<?php 
			$no++;
			endforeach; 
		?>
		<?php else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="8">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo get_option('rs_kota') ? : '&mdash;' ?>, <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>