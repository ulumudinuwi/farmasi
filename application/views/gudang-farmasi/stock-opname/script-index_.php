<script type="text/javascript">
  var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
  var table,
      tableDouble,
      tableHistory,
      detailModal = $('#detail-modal');
  
  var url = {
    loadData: "<?php echo site_url('api/gudang_farmasi/so/load_data?browse=:BROWSE'); ?>",
    loadDataBarang: "<?php echo site_url('api/gudang_farmasi/so/load_data_barang'); ?>",
    getData: "<?php echo site_url('api/gudang_farmasi/so/get_data/:UID'); ?>",
    save: "<?php echo site_url('api/gudang_farmasi/so/save'); ?>",
    doubleCheck: "<?php echo site_url('gudang_farmasi/so/double_check/:UID'); ?>",
    listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID&modul=gudang_farmasi'); ?>",
  };

  function subsDate(range, tipe) {
    let date = range.substr(0, 10);
    if(tipe === "sampai") date = range.substr(13, 10);
    return getDate(date);
  }

  function listen(q, browse) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('gudang_farmasi-' + q, function(e) {
      switch(browse) {
        default:
          tableDouble.draw(false);
          tableHistory.draw(false);
          break;
      }
    }, false);
  }

  function showDetail(uid) {
    $.getJSON(url.getData.replace(':UID', uid), function (data, status) {
      if (status === 'success') {
        data = data.data;

        detailModal.find(".detail_kode").html(data.kode);
        detailModal.find(".detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
        detailModal.find(".detail_pengecekan1_by").html(data.pengecekan1_by);
        detailModal.find(".detail_pengecekan2_by").html(data.pengecekan2_by);
        
        var isDTable = $.fn.dataTable.isDataTable(detailModal.find('.table-detail'));
        if(isDTable === true) detailModal.find('.table-detail').DataTable().destroy();

        detailModal.find('.table-detail').DataTable({
          "ordering": false,
          "processing": true,
          "aaData": data.details,
          "columns": [
            { 
              "data": "barang",
              "render": function (data, type, row, meta) {
                  let tmp = data;
                  tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
                  return tmp;
              },
            },
            { "data": "satuan" },
            { 
                "data": "stock_sistem",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
            { 
                "data": "pengecekan1_stock_fisik",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
            { 
                "data": "pengecekan1_selisih",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
            { 
                "data": "pengecekan2_stock_fisik",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
            { 
                "data": "pengecekan2_selisih",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
          ],
        });
        detailModal.modal('show');
      }
    });
  }

  function handleLoadTable(browse) {
    switch(browse) {
      case 'table_barang':
        table = $("#table").DataTable({
          ordering: false,
          scrollX: true,
          scrollCollapse: true,
          fixedColumns: {
            leftColumns: 3,
          },
          fnDrawCallback: function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
            $('.input-stock_fisik').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2', vMin: 0});
            $('.disp-check').uniform();
            $('.checkbox-inline')
                    .css('top', '-15px')
                    .css('left', '8px');
          },
        });

        $.getJSON(url.loadDataBarang, function(data, status) {
          if (status === 'success') {
            data = data.data;
            for (var i = 0; i < data.length; i++) {
              getColumnTable(0, data[i]);
            }
          }
        });

        var isDTable = $.fn.dataTable.isDataTable($('#table-double'));
        if(isDTable === false) handleLoadTable('table_double');
        break;
      case 'table_double':
        tableDouble = $("#table-double").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadData.replace(':BROWSE', 1),
              "type": "POST",
          },
          "order": [1, "asc"],
          "columns": getColumnTable(1, ""),
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });

        var isDTable = $.fn.dataTable.isDataTable($('#table-history'));
        if(isDTable === false) handleLoadTable('table_history');
        break;
      default:
        tableHistory = $("#table-history").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadData.replace(':BROWSE', 2),
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate($("#search_history_range_tanggal").val(), 'dari');
                  p.tanggal_sampai = subsDate($("#search_history_range_tanggal").val(), 'sampai');
              }
          },
          "order": [1, "desc"],
          "columns": getColumnTable(2, ""),
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });
        break;
    }
  }

  function getColumnTable(browse, data) {
    if(browse == 0) {
      table.row.add([
        // Col 1
        `<label class="checkbox-inline">` +
        `<input type="checkbox" class="disp-check" disabled>` +
        `</label>`,
        // Col 2
        `<span class="label-barang">${data.barang}</span>` +
        `<br/><span class="text-slate-300 text-bold text-size-mini label-kode-barang mt-10">${data.kode_barang}</span>`,
        // Col 3
        `<label class="label-satuan">${data.satuan}</label>`,
        // Col 4
        `<span class="label-stock_sistem">${numeral(data.stock).format('0.0,')}</span>`,
        // Col 5
        `<input type="hidden" class="input-stock_id" name="stock_id[]" value="${data.stock_id}">` +
        `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
        `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
        `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="1">` +
        `<input type="hidden" class="input-stock_sistem" name="stock_sistem[]" value="${data.stock}">` +
        `<input type="hidden" class="input-selisih" name="selisih[]" value="0">` +
        `<input type="hidden" class="input-stock_fisik" name="stock_fisik[]" value="${data.stock}">` +
        `<input type="hidden" class="input-check" value="0">` +
        `<span class="label-stock_fisik">${numeral(data.stock).format('0.0,')}</span>`,
        // Col 6
        `<span class="label-selisih">${numeral(0).format('0.0,')}</span>`,
        // Col 7
        `<span class="label-selisih">${numeral(0).format('0.0,')}</span>`,
        // Col 8
        `<span class="label-selisih">${numeral(0).format('0.0,')}</span>`,
        // Col 9
        `<span class="label-selisih">${numeral(0).format('0.0,')}</span>`,
        // Col 10
        `<button class="btn btn-link btn-xs edit-row"><i class="fa fa-edit"></i></button>`,
      ]).draw(false);
    } else {
      let column = [
          { 
            "data": "kode",
            "render": function (data, type, row, meta) {
              let title = "Lihat Detail";
              if(browse == 1) title = "Pemeriksaan Kedua";

              let tmp = `<a class="show-row" data-uid="${row.uid}" data-toggle="tooltip" data-title="${title}" data-placement="top">${data}</a>`;
              return tmp;
            },
          },
          {
            "data": "tanggal",
            "searchable": false,
            "render": function (data, type, row, meta) {
              return moment(data).format('DD-MM-YYYY HH:mm');
            },
          },
        ];

      switch(parseInt(browse)) {
        case 2:
          column.push(
            { 
              "data": "pengecekan1_by",
              "orderable": false,
              "searchable": false,
              "render": function (data, type, row, meta) {
                let tmp = `${row.pengecekan1_by}<br/><span class="text-size-mini text-info">${moment(row.pengecekan1_at).format('DD-MM-YYYY HH:mm')}</span>`;
                return tmp;
              },
            },
            { 
              "data": "pengecekan2_by",
              "orderable": false,
              "searchable": false,
              "render": function (data, type, row, meta) {
                let tmp = `${row.pengecekan2_by}<br/><span class="text-size-mini text-info">${moment(row.pengecekan2_at).format('DD-MM-YYYY HH:mm')}</span>`;
                return tmp;
              },
            }
          ); 
          break;
        default:
          column.push({ 
            "data": "pengecekan1_by",
            "orderable": false,
            "searchable": false,
            "render": function (data, type, row, meta) {
              let tmp = `<b>Pemeriksaan pertama oleh:</b><br/><span class="text-size-mini text-info">${row.pengecekan1_by}</span><br/><span class="text-size-mini text-danger">${moment(row.pengecekan1_at).format('DD-MM-YYYY HH:mm')}</span>`;
              return tmp;
            },
          }); 
          break;
      }
      return column;
    }
  }

  function genStockDetail(data, key) {
    let tmp = [];
    for (var i = 0; i < data.length; i++) {
      let detail = data[i];
      tmp += `<input type="hidden" class="input-stock_id" name="stock_id[]" value="${detail[key]}">`;
    }
  }

  $(window).ready(function() {

    $(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(tanggalDari),
        endDate: moment(tanggalSampai),
    });
    handleLoadTable('table_barang');

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-1':
            table.draw(false);
            break;
          case '#tab-2':
            tableDouble.draw(false)
            break;
          default:
            tableHistory.draw(false);
            break;
        }
    });

    // TAB 1
    $(".disp_tanggal").daterangepicker({
      singleDatePicker: true,
      startDate: moment("<?php echo date('Y-m-d'); ?>"),
      endDate: moment("<?php echo date('Y-m-d'); ?>"),
      applyClass: "bg-slate-600",
      cancelClass: "btn-default",
      opens: "center",
      autoApply: true,
      locale: {
        format: "DD/MM/YYYY"
      }
    });

    /*$("#btn_tanggal").click(function () {
      let parent = $(this).parent();
      parent.find('input').data("daterangepicker").toggle();
    });*/

    $('#table').on('click', '.edit-row', function (e) {
        let tr = $(this).closest('tr');
        let data = table.row(tr).data();

        data._DT_RowIndex = tr[0]._DT_RowIndex;
        console.log(data);
    });

    $('#table').on('keyup', '.input-stock_fisik', function() {
      let tr = $(this).parents('tr');
      let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? "" : parseFloat($(this).autoNumeric('get'));
      let selisih = 0;
      let check = 0;
      if(val !== "") {
        let stockSistem = parseFloat(tr.find('.input-stock_sistem').val());
        selisih = stockSistem - val;
        check = 1;
      }
      tr.find('.input-selisih').val(selisih);
      tr.find('.label-selisih').html(numeral(selisih).format('0.0,'));
      tr.find('.input-check').val(check);

      let parent = tr.find('.disp-check').parent();
      parent.removeClass('checked');
      if(check === 1) parent.addClass('checked');
    });

    $('#form').validate({
      rules: {
        tanggal: { required: true },
      },
      focusInvalid: true,
      errorPlacement: function(error, element) {
          var placement = $(element).closest('.input-group');
          if (placement.length > 0) {
              error.insertAfter(placement);
          } else {
              error.insertAfter($(element));
          }
      },
      submitHandler: function (form) {
        var formData = new FormData($('#form'));
        formData.append('tanggal', $('#tanggal').val());

        table.rows().nodes().each(function (i, dt_index) {
          let check = parseInt(table.row(dt_index).nodes().to$().find('.input-check').val());
          if(check == 1) {
            let stock_id = table.row(dt_index).nodes().to$().find('.input-stock_id').val();
            let barang_id = table.row(dt_index).nodes().to$().find('.input-barang_id').val();
            let satuan_id = table.row(dt_index).nodes().to$().find('.input-satuan_id').val();
            let isi_satuan = table.row(dt_index).nodes().to$().find('.input-isi_satuan').val();
            let stock_sistem = table.row(dt_index).nodes().to$().find('.input-stock_sistem').val();
            let selisih = table.row(dt_index).nodes().to$().find('.input-selisih').val();
            let stock_fisik = table.row(dt_index).nodes().to$().find('.input-stock_fisik').autoNumeric('get');
            
            formData.append('stock_id[]', stock_id);
            formData.append('barang_id[]', barang_id);
            formData.append('satuan_id[]', satuan_id);
            formData.append('isi_satuan[]', isi_satuan);
            formData.append('stock_sistem[]', stock_sistem);
            formData.append('selisih[]', selisih);
            formData.append('stock_fisik[]', stock_fisik);
          }
        });

        swal({
          title: "Konfirmasi?",
          type: "warning",
          text: "Apakah pemeriksaan pertama yang dimasukan benar ??",
          showCancelButton: true,
          confirmButtonText: "Ya",
          confirmButtonColor: "#2196F3",
          cancelButtonText: "Batal",
          cancelButtonColor: "#FAFAFA",
          closeOnConfirm: true,
          showLoaderOnConfirm: true,
        },
        function() {
          blockPage('Sedang diproses ...');
          $.ajax({
            data: formData,
            type: 'POST',
            dataType: 'JSON', 
            processData: false,
            contentType: false,
            url: url.save,
            success: function(data){
                $.unblockUI();
                swal('Berhasil', 'Pemeriksaan pertama berhasil disimpan.', 'info');
                setTimeout(function() { 
                  window.location.reload();
                }, 1500);
            },
            error: function(data){
                $.unblockUI();
                errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
            }
          });
          return false;
        });
      }
    });

    // TAB 2
    $("#table-double").on("click", ".show-row", function() {
      let uid = $(this).data('uid');
      blockPage('Form pemeriksaan kedua sedang diproses ...');
      setTimeout(function() { 
        window.location.assign(url.doubleCheck.replace(':UID', uid));
      }, 1000);
    });

    // TAB 3
    $("#search_history_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
      tableHistory.draw();
    });

    $("#btn_search_history_tanggal").click(function () {
      $("#search_history_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_history_range_tanggal").on('change', function() {
      tableHistory.draw();
    });

    $("#table-history").on("click", ".show-row", function () {
      let uid = $(this).data('uid');
      showDetail(uid);
    });

    listen("<?php echo $tabUid; ?>", 1);
  });
</script>