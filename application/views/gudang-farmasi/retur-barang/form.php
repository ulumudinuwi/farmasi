<style>
	.table > thead > tr > th,
	.table > tbody > tr > td, 
	.table > tfoot > tr > td {
        vertical-align: top;
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<fieldset>
							<div class="col-md-12">
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Retur</legend>
							</div>
							<div class="col-md-6">
								<div class="form-group el-hidden">
									<label class="control-label col-md-4 input-required">Nomor</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="kode" name="kode" placeholder="Nomor ..." disabled="true">
									</div>
								</div>
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Tanggal</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                                                <i class="icon-calendar22"></i>
                                            </span>
                                            <input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal" disabled="true">
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3 input-required"><?php echo $this->lang->line('pabrik_label'); ?></label>
									<div class="col-md-8">
										<div class="input-group">
											<select class="form-control" id="pabrik_id" name="pabrik_id">
												<option value="" selected="selected">- Pilih -</option>
												<?php 
												foreach($pabrik as $row) {
													echo "<option value='{$row->id}'>{$row->kode} - {$row->nama}</option>";
												}
												?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
		            <div class="row mb-20">
		                <div class="col-sm-12">
		                    <fieldset>
		                        <legend class="text-bold">
		                            <i class="icon-list"></i> <strong>Daftar Barang&nbsp;&nbsp;<span class="text-info text-size-mini"><i class="icon-info22"></i> Qty tidak dapat melebihi sisa stock yang tersedia.</span></strong>
		                        </legend>
		                        <div class="row">
		                            <div class="col-sm-12">
		                                <div class="table-responsive">
		                                    <table id="table_detail" class="table table-bordered">
		                                        <thead>
		                                            <tr class="bg-slate">
		                                                <th>Nama</th>
		                                                <th>Satuan</th>
		                                                <th>Qty</th>
		                                                <th>Exp. Date</th>
		                                                <th class="text-center" style="width: 5%;"></th>
		                                            </tr>
		                                        </thead>
		                                        <tbody></tbody>
		                                        <tfoot>
		                                            <tr>
		                                                <td colspan="5">
		                                                    <button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah">
		                                                        <b><i class="icon-plus-circle2"></i></b>
		                                                        Tambah
		                                                    </button>
		                                                </td>
		                                            </tr>
		                                        </tfoot>
		                                    </table>
		                                </div>
		                                <div class="row mt-10">
											<div class="col-md-8">
												<label>Keterangan</label>
												<div>
													<textarea class="form-control wysihtml5-min" id="keterangan" name="keterangan"></textarea>
												</div>
											</div>
										</div>
		                            </div>
		                        </div>
		                    </fieldset>
		                </div>
		            </div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php $this->load->view('logistik/list-barang-modal'); ?>
<?php $this->load->view('logistik/history-barang-modal'); ?>
<?php $this->load->view('gudang-farmasi/stock/detail-modal'); ?>