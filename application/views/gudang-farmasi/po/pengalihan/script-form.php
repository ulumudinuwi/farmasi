<script>
var UID = "<?php echo $uid; ?>",
    form = '#form';

var url = {
  index: "<?php echo site_url('gudang_farmasi/po'); ?>",
  save: "<?php echo site_url('api/gudang_farmasi/pengalihan/save'); ?>",
  getData: "<?php echo site_url('api/gudang_farmasi/pengalihan/get_data/:UID'); ?>",
  getDistributor: "<?php echo site_url('api/master/pabrik/get_distributor?q=:ID'); ?>",
};

var tableDetail = $('#table_detail'),
    tableDetailDt;

function fillElement(obj, element) {
  let parent = element.parent();
  parent.find('.loading-select').show();
  $.getJSON(obj.url, function(data, status) {
    if (status === 'success') {
      var option = '';
      option += '<option value="" selected="selected">- Pilih -</option>';
      for (var i = 0; i < data.list.length; i++) {
        if(parseInt(obj.value) !== parseInt(data.list[i].id)) {
          let selected = "",
              label = data.list[i][obj.key];
          if (parseInt(data.list[i].is_default) === 1) {
            selected = 'selected="selected"';
            label += ` (Default)`;
          }
          option += `<option value="${data.list[i].id}" ${selected}>${label}</option>`;
        }
      }
      element.html(option).trigger("change");
    }
    parent.find('.loading-select').hide();
  });
}

function fillForm(uid) {
  tableDetailDt = tableDetail.DataTable({
    "info": false,
    "ordering": false,
    "paginate": false,
  });

  blockElement($(form));
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;

      $("#detail_kode").html(data.kode);
      $("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
      $("#detail_sifat").html(data.sifat_desc);
      $("#detail_pabrik").html(data.pabrik);
      $("#detail_status").html(data.status_desc);

      var isDTable = $.fn.dataTable.isDataTable($('#table_detail_po'));
      if(isDTable === true) $('#table_detail_po').DataTable().destroy();

      $('#table_detail_po').DataTable({
          "ordering": false,
          "processing": true,
          "aaData": data.details,
          "columns": [
            { 
              "data": "barang",
              "render": function (data, type, row, meta) {
                  let tmp = data;
                  tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
                  return tmp;
              },
            },
            { "data": "satuan_po" },
            { 
                "data": "qty",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
            {
                "data": "harga",
                "render": function (data, type, row, meta) {
                    return 'Rp. ' + numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
            {
                "data": "total",
                "render": function (data, type, row, meta) {
                    return 'Rp.' + numeral(row.total).format('0.0,');
                },
                "className": "text-right"
            },
            { 
                "data": "diterima",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
            },
          ]
      });

      $('#detail_total').html('Rp. ' + numeral(data.total).format('0.0,'));
      $('#detail_total_ppn').html('Rp. ' + numeral(data.total_ppn).format('0.0,'));
      $('#detail_grand_total').html('Rp. ' + numeral(data.grand_total).format('0.0,'));

      $("#po_id").val(data.id);
      $("#po_uid").val(data.uid);
      $("#po_kode").val(data.kode);
      $("#tanggal").data("daterangepicker").setStartDate(moment(data.tanggal_new).isValid() ? moment(data.tanggal_new) : moment())
      $("#tanggal").data("daterangepicker").setEndDate(moment(data.tanggal_new).isValid() ? moment(data.tanggal_new) : moment());
      $("#sifat").val(data.sifat).trigger("change");

      $("#pabrik_id").find(`option[value="${data.pabrik_id}"]`).remove();
      $("#pabrik_id").val("").trigger("change");

      for (var i = 0; i < data.details.length; i++) {
        columnBarang(data.details[i]);
      }
      $(form).unblock();
    }
  });
}

function columnBarang(data) {
  tableDetailDt.row.add([
    // Col 1
    `<input type="hidden" class="input-detail_id" name="detail_id[]" value="${data.id}">` +
    `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
    `${data.barang}` +
    `<br/><span class="text-slate-300 text-bold text-size-mini disp_barang mt-10">${data.kode_barang}</span>`,
    // Col 2
    `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
    `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
    `<label class="label-satuan">${data.satuan_po}</label>`,
    // Col 3
    `<input type="hidden" class="input-minimum" name="minimum[]" value="${data.minimum}">` +
    `<label class="label-minimum">${numeral(data.minimum).format('0.0,')}</label>`,
    // Col 4
    `<input type="hidden" class="input-stock" name="stock[]" value="${data.stock}">` +
    `<label class="label-stock">${numeral(data.stock).format('0.0,')}</label>`,
    // Col 5
    `<input type="hidden" class="input-maximum" name="maximum[]" value="${data.maximum}">` +
    `<label class="label-maximum">${numeral(data.maximum).format('0.0,')}</label>`,
    // Col 6
    `<input type="hidden" class="input-qty" name="qty[]" value="${data.qty}">` +
    `<label class="label-qty">${numeral(data.qty).format('0.0,')}</label>`,
    // Col 7
    `<input type="hidden" class="input-harga" name="harga[]" value="${data.harga}">` +
    `<label class="label-harga">Rp. ${numeral(data.harga).format('0.0,')}</label>`,
    // Col 8
    `<label class="label-subtotal">Rp. ${numeral(data.total).format('0.0,')}</label>`,
  ]).draw(false);
  updateDetail();
}

function updateDetail() {
    let total = 0,
        total_ppn = 0;
    tableDetail.find('tbody .label-subtotal').each(function (i, el) {
        total += numeral($(el).html())._value;
    });
    $("#disp_total").html('Rp.' + numeral(total).format('0.0,'));
    $("#total").val(total);

    total_ppn = total * 0.1;
    $("#disp_total_ppn").html('Rp.' + numeral(total_ppn).format('0.0,'));
    $('#total_ppn').val(total_ppn);

    total += total_ppn;
    $("#disp_grand_total").html('Rp.' + numeral(total).format('0.0,'));
}

$(document).ready(function() {
  $(".wysihtml5-min").wysihtml5({
    parserRules:  wysihtml5ParserRules
  });
  $(".wysihtml5-toolbar").remove();
  $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
  $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

  $('.btn-save').on('click', function(e) {
    e.preventDefault();
    $(form).submit();
  });

  $(".btn-batal").click(function() {
    window.location.assign(url.index);
  });

  $(".disp_tanggal").daterangepicker({
    singleDatePicker: true,
    startDate: moment("<?php echo date('Y-m-d'); ?>"),
    endDate: moment("<?php echo date('Y-m-d'); ?>"),
    applyClass: "bg-slate-600",
    cancelClass: "btn-default",
    opens: "center",
    autoApply: true,
    locale: {
      format: "DD/MM/YYYY"
    }
  });

  $("#btn_tanggal").click(function () {
    let parent = $(this).parent();
    parent.find('input').data("daterangepicker").toggle();
  });

  $(form).validate({
    rules: {
      kode: { required: true },
      tanggal: { required: true },
      sifat: { required: true },
      pabrik_id: { required: true },
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var placement = $(element).closest('.input-group');
        if (placement.length > 0) {
            error.insertAfter(placement);
        } else {
            error.insertAfter($(element));
        }
    },
    submitHandler: function (form) {
      tableDetailDt.search('').draw(false);
      swal({
        title: "Konfirmasi?",
        type: "warning",
        text: "Apakah data yang dimasukan benar??",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#2196F3",
        cancelButtonText: "Batal",
        cancelButtonColor: "#FAFAFA",
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
      },
      function() {
        $('.input-decimal').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('.input-bulat').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('input, textarea, select').prop('disabled', false);

        blockPage('Sedang diproses ...');
        var formData = $(form).serialize();
        $.ajax({
          data: formData,
          type: 'POST',
          dataType: 'JSON', 
          url: url.save,
          success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "Pengalihan PO berhasil disimpan.");
              window.location.assign(url.index);
          },
          error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
          }
        });
        return false;
      });
    }
  });

  fillForm(UID);
});
</script>