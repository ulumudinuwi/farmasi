<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Detail Purchase Order</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="row mb-20">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Data PO</legend>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-3 control-label">Nomor</label>
								<div class="col-md-6">
									<div class="form-control-static text-bold" id="detail_kode"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Tanggal</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_tanggal"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Sifat</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_sifat"></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-3 control-label"><?php echo lang('pabrik_label'); ?></label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_pabrik"></div>
								</div>
							</div>
							<div class="form-group money-detail">
								<label class="col-lg-3 control-label"><?php echo lang('label_kurs'); ?></label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_kurs">&mdash;</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Status</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_status"></div>
								</div>
							</div>
							<div class="form-group">
								<span id="detail_keterangan" class="text-info text-size-mini col-md-12"></span>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="row mb-20">
					<div class="col-sm-12">
						<fieldset>
							<legend class="text-bold">
								<i class="icon-list"></i> <strong>Daftar Barang</strong>
							</legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table id="table_detail" class="table table-bordered">
											<thead>
												<tr class="bg-slate">
													<th>Nama</th>
													<th>Satuan</th>
													<th>Min</th>
													<th>Stock</th>
													<th>Max</th>
													<th>Qty</th>
													<th>Harga</th>
													<th>Sub Total</th>
													<th>Diterima</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot class="money-detail">
												<tr>
													<td colspan="7" class="text-right text-bold">TOTAL</td>
													<td class="text-bold text-right" id="detail_total"></td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td colspan="7" class="text-right text-bold">PPN</td>
													<td class="text-bold text-right" id="detail_total_ppn"></td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td colspan="7" class="text-right text-bold">TOTAL DAFTAR BARANG</td>
													<td class="text-bold text-right" id="detail_grand_total_barang"></td>
													<td>&nbsp;</td>
												</tr>
											</tfoot>
										</table>
									</div>
									<span class="text-info"><i class="icon-info22"></i> Kolom merah menandakan qty yang di pesan melebihi <b>maximum</b> point.</span>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="row money-detail">
					<div class="col-sm-12">
						<fieldset>
							<legend class="text-bold">
	                            <i class="icon-list"></i> <strong>Biaya Lain</strong>
	                        </legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table id="table-biaya_lain" class="table table-bordered">
											<thead>
												<tr class="bg-slate">
													<th>Deskripsi</th>
		                                            <th>Total Harga</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot>
												<tr>
													<td class="text-right text-bold">TOTAL</td>
													<td class="text-bold text-right" id="detail_total_biaya_lain"></td>
												</tr>
												<tr>
													<td class="text-right text-bold">TOTAL DAFTAR BARANG + TOTAL BIAYA LAIN</td>
													<td class="text-bold text-right" id="detail_grand_total"></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>