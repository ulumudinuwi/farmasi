<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var tableConfirm, table;
	var tableDetail = $('#table_detail');
	var tableBiayaLain = $('#table-biaya_lain');
	var btnRefresh = $('#btn-refresh');
	var detailModal = $('#detail-modal');
	var url = {
		loadData : "<?php echo site_url('api/gudang_farmasi/po/load_data'); ?>",
		getData : "<?php echo site_url('api/gudang_farmasi/po/get_data/:UID'); ?>",
		actExe: "<?php echo site_url('api/gudang_farmasi/approval/act_exe'); ?>",
		edit : "<?php echo site_url('gudang_farmasi/po_direktur/form/:UID'); ?>",
		cetakPO : "<?php echo site_url('api/gudang_farmasi/po/cetak/:UID'); ?>",
		listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID&modul=gudang_farmasi'); ?>",
	};

	function listen(q) {
		eventSource = new EventSource(url.listen.replace(':UID', q));
		eventSource.addEventListener('gudang_farmasi-' + q, function(e) {
			table.draw(false);
			tableConfirm.draw(false);
		}, false);
	}

	function showDetail(uid) {
		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data.data;
				let visibleMoneyDetail = true;
				detailModal.find('.money-detail').show(); 
				if(data.status < 2) {
					detailModal.find('.money-detail').hide(); 
					visibleMoneyDetail = false;
				} 
				$("#detail_kode").html(data.kode);
				$("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
				$("#detail_sifat").html(data.sifat_desc);
				$("#detail_pabrik").html(data.pabrik);
				$("#detail_status").html(data.status_desc);

				let kurs = '&mdash;';
				if(data.kurs_id) {
					kurs = `${data.kurs_code}`;
					kurs += `<p class="text-size-mini text-muted">1 ${data.kurs_name} = Rp. ${numeral(data.kurs).format('0.0,')}</p>`;
				}
				$("#detail_kurs").html(kurs);

				$("#detail_keterangan").html("");
				if(parseInt(data.status) === 6 || parseInt(data.status) === 7) { // Ditolak, Dialihkan
					let keterangan = data.keterangan ? `<b>Alasan</b>:<br/>${data.keterangan}` : "";
					$("#detail_keterangan").html(keterangan);
				}

				var isDTable = $.fn.dataTable.isDataTable(tableDetail);
				if(isDTable === true) tableDetail.DataTable().destroy();

				tableDetail.DataTable({
					"ordering": false,
					"processing": true,
					"aaData": data.details,
					"columns": [
						{ 
							"data": "barang",
							"render": function (data, type, row, meta) {
								let tmp = data;
								tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
								return tmp;
							},
						},
						{ "data": "satuan_po" },
						{ 
							"data": "minimum",
							"render": function (data, type, row, meta) {
								return numeral(data).format('0.0,');
							},
							"className": "text-right"
						},
						{ 
							"data": "stock",
							"render": function (data, type, row, meta) {
								return numeral(data).format('0.0,');
							},
							"className": "text-right"
						},
						{ 
							"data": "maximum",
							"render": function (data, type, row, meta) {
									return `<span class="label-maximum">${numeral(data).format('0.0,')}</span>`;
							},
							"className": "text-right"
						},
						{ 
							"data": "qty",
							"render": function (data, type, row, meta) {
									return `<span class="label-qty">${numeral(data).format('0.0,')}</span>`;
							},
							"className": "text-right"
						},
						{
							"data": "harga",
							"visible": visibleMoneyDetail,
							"render": function (data, type, row, meta) {
								return numeral(data).format('0.0,');
							},
							"className": "text-right"
						},
						{
							"data": "total",
							"visible": visibleMoneyDetail,
							"render": function (data, type, row, meta) {
								return numeral(row.total).format('0.0,');
							},
							"className": "text-right"
						},
						{ 
							"data": "diterima",
							"render": function (data, type, row, meta) {
								return numeral(data).format('0.0,');
							},
							"className": "text-right"
						},
					],
					"drawCallback": function (settings) {
						let tr = tableDetail.find("tbody tr");
						tr.each(function() {
							let qty = isNaN(parseFloat(numeral($(this).find('.label-qty').html())._value)) ? 0 : parseFloat(numeral($(this).find('.label-qty').html())._value);
							let maximum = parseFloat(numeral($(this).find('.label-maximum').html())._value);
							let td = $(this).children().eq(5);
							td.removeClass('bg-danger');
							if(qty > maximum) {
								td.addClass('bg-danger');
							}
						});
					},
				});

				var isDTable = $.fn.dataTable.isDataTable(tableBiayaLain);
				if(isDTable === true) tableBiayaLain.DataTable().destroy();

				tableBiayaLain.DataTable({
					"ordering": false,
					"processing": true,
					"aaData": data.biaya_lains,
					"columns": [
						{ "data": "deskripsi" },
						{
							"data": "total",
							"render": function (data, type, row, meta) {
								return numeral(data).format('0.0,');
							},
							"className": "text-right"
						},
					],
				});

				$('#detail_total').html(numeral(data.total).format('0.0,'));
				$('#detail_total_ppn').html(numeral(data.total_ppn).format('0.0,'));
				$('#detail_grand_total_barang').html(numeral(parseFloat(data.total) + parseFloat(data.total_ppn)).format('0.0,'));
				$('#detail_total_biaya_lain').html(numeral(data.total_biaya_lain).format('0.0,'));
				$('#detail_grand_total').html(numeral(data.grand_total).format('0.0,'));
				detailModal.modal('show');
			}
		});
	}

	function handleLoadTable(browse) {
		switch(browse) {
			case 'table_confirm':
				tableConfirm = $("#table-confirm").DataTable({
					"processing": true,
					"serverSide": true,
					"ajax": {
						"url": url.loadData,
						"type": "POST",
						"data": function(p) {
							p.sifat = $('#search_confirm_sifat').val();
							p.pabrik_id = $('#search_confirm_pabrik').val();
							p.status = 1;
						}
					},
					"order": [1, "asc"],
					"columns": [
						{ "data": "kode" },
						{
							"data": "tanggal",
							"render": function (data, type, row, meta) {
								let tmp = moment(data).format('DD-MM-YYYY HH:mm');
								tmp += `<br/><span class="text-size-mini text-info"><b>Pengajuan Oleh:</b><br/> ${row.pengajuan_by}</span>`;
								return tmp;
							},
							"searchable": false,
						},
						{ 
							"data": "pabrik",
							"orderable": false,
							"searchable": false,
						},
						{ 
							"data": "sifat",
							"orderable": false,
							"searchable": false,
						},
						{ 
							"data": "status_desc",
							"orderable": false,
							"searchable": false,
							"visible": false,
						},
						{
							"data": "status",
							"orderable": false,
							"searchable": false,
							"render": function (data, type, row, meta) {
								let tmp = '<a href="' + url.edit.replace(':UID', row.uid) + '" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Konfirmasi" data-placement="top"><i class="icon-checkmark-circle"></i></a>';
								return tmp;
							},
							"className": "text-center"
						},
					],
					"fnDrawCallback": function (oSettings) {
						$('[data-toggle=tooltip]').tooltip();
					},
				});

				var isDTable = $.fn.dataTable.isDataTable($('#table'));
				if(isDTable === false) handleLoadTable('table');
				break;
			default:
				table = $("#table").DataTable({
					"processing": true,
					"serverSide": true,
					"ajax": {
						"url": url.loadData,
						"type": "POST",
						"data": function(p) {
								p.tanggal_dari = tanggalDari;
								p.tanggal_sampai = tanggalSampai;
								p.sifat = $('#search_sifat').val();
								p.pabrik_id = $('#search_pabrik').val();
								p.status = $('#search_status').val();
						}
					},
					"order": [1, "desc"],
					"columns": [
						{ 
							"data": "kode",
							"render": function (data, type, row, meta) {
									let tmp = '<a class="show-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + data + '</a>';
									return tmp;
							},
						},
						{
							"data": "tanggal",
							"render": function (data, type, row, meta) {
								let tmp = moment(data).format('DD-MM-YYYY HH:mm');
								tmp += `<br/><span class="text-size-mini text-info"><b>Pengajuan Oleh:</b><br/> ${row.pengajuan_by}</span>`;
								return tmp;
							},
							"searchable": false,
						},
						{ 
							"data": "pabrik",
							"orderable": false,
							"searchable": false,
						},
						{ 
							"data": "sifat",
							"orderable": false,
							"searchable": false,
						},
						{ 
							"data": "status_desc",
							"orderable": false,
							"searchable": false,
							"render": function (data, type, row, meta) {
								let tmp = data;
								tmp += `<br/><span class="text-size-mini text-info"><b>Diproses Oleh:</b><br/> ${row.diubah_by}</span>`;
								if(parseInt(row.status) === 6 || parseInt(row.status) === 0) { // Ditolak dan Dibatalkan
									tmp += `<br/><span class="text-size-mini text-info"><b>Alasan: </b><br/> ${row.keterangan}</span>`;
								}
								return tmp;
							}
						},
						{
							"data": "status",
							"orderable": false,
							"searchable": false,
							"render": function (data, type, row, meta) {
								let tmp = `<a href="${url.cetakPO.replace(':UID', btoa(JSON.stringify({ uid: row.uid, browse: (row.status < 2 ? 'po_admin' : 'po_direktur')  })))}" target="_blank" class="btn btn-info btn-xs" data-toggle="tooltip" title="Cetak"><i class="fa fa-print"></i></a>`;
								switch(parseInt(data)) {
									case 2:
										//tmp += `&nbsp;<a target="_blank" class="btn btn-warning btn-xs batal-row" data-toggle="tooltip" data-title="Batalkan" data-uid="${row.uid}"><i class="fa fa-times"></i></a>`;
										break;
									case 3:
										tmp += '&nbsp;<a href="' + url.edit.replace(':UID', row.uid) + '" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" data-placement="top"><i class="fa fa-edit"></i></a>';
										break;
								}
								return tmp;
							},
							"className": "text-center"
						},
					],
					"fnDrawCallback": function (oSettings) {
						$('[data-toggle=tooltip]').tooltip();
					},
				});
				break;
			}
		}

	function actexe(obj) {
		$.post(url.actExe, {uid: obj.uid, alasan: obj.alasan, mode: obj.mode}, function (data, status) {
			if (status === "success") {
				swal.close();
				successMessage('Berhasil !', obj.success_message);
			}
		})
		.fail(function (error) {
			swal.close();
			errorMessage('Gagal !', obj.error_message);
		});
	}

	$(window).ready(function() {
		handleLoadTable('table_confirm');

		$('a[data-toggle="tab"]').click(function (e) {
			switch($(this).attr('href')) {
				case '#tab-1':
					tableConfirm.draw(false);
					break;
				default:
					table.draw(false);
					break;
			}
		});

		$("#search_confirm_sifat, #search_confirm_pabrik").on('change', function() {
			tableConfirm.draw();
		});

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			tanggalDari = picker.startDate.format('YYYY-MM-DD');
			tanggalSampai = picker.endDate.format('YYYY-MM-DD');       

			table.draw();
		});

		$("#search_range_tanggal").on('change', function () {
			range = $("#search_range_tanggal").val();
			var date = range.substr(0, 10);
			tanggalDari = getDate(date); 

			date = range.substr(13, 10);
			tanggalSampai = getDate(date); 

			table.draw();
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_sifat, #search_pabrik, #search_status").on('change', function() {
			table.draw();
		});

		$("#table").on("click", ".show-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid);
		});

		$("#table").on("click", ".batal-row", function () {
			let uid = $(this).data('uid');
			swal({
				title: "Batalkan PO ?",
				text: "Apakah Anda yakin tidak menyetujui PO ini ?<br/><textarea class='form-control' id='swal-text' style='resize: vertical;'></textarea>",
				html: true,
				showCancelButton: true,
				confirmButtonColor: "#F44336",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
				animation: "slide-from-top",
			},
			function() {
				var value = $("#swal-text").val();
				if (value === "") {
					swal.showInputError("Silahkan isi alasan dibatalkannya PO.");
					$("#swal-text").focus();
					return;
				}

				actexe({
					uid: uid,
					alasan: value,
					mode: "batal",
					success_message: 'PO berhasil dibatalkan.',
					error_message: 'Terjadi kesalahan saat membatalkan transaksi.',
				});
			});
		});

		listen("<?php echo $tabUid; ?>");
	});
</script>