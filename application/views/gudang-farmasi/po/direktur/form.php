<style>
	.table > thead > tr > th,
	.table > tbody > tr > td, 
	.table > tfoot > tr > td {
        vertical-align: top;
		white-space: nowrap;
	}

	#table-biaya_lain_wrapper .datatable-header {
		display: none;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<fieldset>
							<div class="col-md-12">
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data PO</legend>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4 input-required">Nomor</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="kode" name="kode" placeholder="Nomor ..." disabled="true">
									</div>
								</div>
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Tanggal</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                                                <i class="icon-calendar22"></i>
                                            </span>
                                            <input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
									<label class="control-label col-md-4 input-required">Sifat</label>
									<div class="col-md-6">
										<div class="input-group">
											<select class="form-control" id="sifat" name="sifat">
							                    <option value="" selected="selected">- Pilih -</option>
							                    <?php 
							                      foreach($sifat as $key => $val) {
							                        echo "<option value='{$key}'>{$val}</option>";
							                      }
							                    ?>
							                </select>
							            </div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 input-required">No. Invoice</label>
									<div class="col-md-6">
										<div class="input-group">
											<input type="text" id="no_invoice" name="no_invoice" class="form-control" placeholder="No. Invoice ...">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3 input-required"><?php echo lang('pabrik_label'); ?></label>
									<div class="col-md-6">
										<div class="input-group section-add section-select_pabrik">
											<select class="form-control" id="pabrik_id" name="pabrik_id">
												<option value="" data-row="" selected="selected">- Pilih -</option>
												<?php 
													foreach($pabrik as $row) {
														echo "<option value='{$row->id}' data-row='".json_encode($row)."'>{$row->kode} - {$row->nama}</option>";
													}
												?>
											</select>
										</div>
										<div class="form-control-static section-edit" id="div-label_pabrik" style="display: none;"></div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3"><?php echo lang('label_kurs'); ?></label>
									<div class="col-md-6">
										<div class="form-control-static" id="div-label_kurs">&mdash;</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
		            <div class="row mb-20">
		                <div class="col-sm-12">
		                    <fieldset>
		                        <legend class="text-bold">
		                            <i class="icon-list"></i> <strong>Daftar Barang</strong>
		                        </legend>
		                        <div class="row">
		                            <div class="col-sm-12">
		                                <div class="table-responsive">
		                                    <table id="table_detail" class="table table-bordered">
		                                        <thead>
		                                            <tr class="bg-slate">
		                                                <th>Nama</th>
		                                                <th>Satuan</th>
		                                                <th>Min</th>
		                                                <th>Stock</th>
		                                                <th>Max</th>
		                                                <th>Qty</th>
		                                                <th>Harga <span class="th-kurs text-info text-bold"></span></th>
		                                                <th>Sub Total <span class="th-kurs text-info text-bold"></span></th>
		                                                <th class="text-center" style="width: 5%;"></th>
		                                            </tr>
		                                        </thead>
		                                        <tbody></tbody>
		                                        <tfoot>
		                                            <tr>
		                                                <td colspan="5">
		                                                    <button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah">
		                                                        <b><i class="icon-plus-circle2"></i></b>
		                                                        Tambah
		                                                    </button>
		                                                </td>
		                                                <td colspan="2" class="text-right text-bold">
		                                                	TOTAL
		                                                </td>
		                                                <td class="text-right text-bold">
		                                                	<label id="disp_total">0</label>
		                                                	<p class="text-size-mini text-info" id="disp_total_kurs"></p>
		                                                </td>
		                                                <td>
		                                                	<input type="checkbox" class="styled" value="1" id="disp_is_ppn"> PPN
		                                                </td>
		                                            </tr>
		                                            <tr class="section-is_ppn" style="display: none;">
		                                                <td colspan="7" class="text-right text-bold">TOTAL PPN</td>
		                                                <td class="text-right text-bold">
		                                                	<label id="disp_total_ppn">0</label>
		                                                	<p class="text-size-mini text-info" id="disp_total_ppn_kurs"></p>
		                                                </td>
		                                                <td>&nbsp;</td>
		                                            </tr>
		                                            <tr class="section-is_ppn" style="display: none;">
		                                                <td colspan="7" class="text-right text-bold">TOTAL DAFTAR BARANG</td>
		                                                <td class="text-right text-bold">
		                                                	<label id="disp_grand_total_barang">0</label>
		                                                	<p class="text-size-mini text-info" id="disp_grand_total_kurs_barang"></p>
		                                                </td>
		                                                <td>&nbsp;</td>
		                                            </tr>
		                                        </tfoot>
		                                    </table>
		                                    <span class="text-info"><i class="icon-info22"></i> Kolom merah menandakan qty yang di pesan melebihi <b>maximum</b> point.</span>
		                                </div>
		                            </div>
		                        </div>
		                    </fieldset>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-sm-12 div-direktur">
		                    <fieldset>
		                        <legend class="text-bold">
		                            <i class="icon-list"></i> <strong>Biaya Lain</strong>
		                        </legend>
		                        <div class="row">
		                            <div class="col-sm-12">
		                                <div class="table-responsive">
		                                    <table id="table-biaya_lain" class="table table-bordered">
		                                        <thead>
		                                            <tr class="bg-slate">
		                                                <th class="text-center" style="width: 5%;"></th>
		                                                <th>Deskripsi</th>
		                                                <th>Total Harga <span class="th-kurs text-info text-bold"></span></th>
		                                            </tr>
		                                        </thead>
		                                        <tbody></tbody>
		                                        <tfoot>
		                                            <tr>
		                                            	<td>
		                                            		<button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah_biaya_lain">
		                                                        <b><i class="icon-plus-circle2"></i></b>
		                                                        Tambah
		                                                    </button>
		                                            	</td>
		                                                <td class="text-right text-bold">
		                                                	TOTAL
		                                                </td>
		                                                <td class="text-right text-bold">
		                                                	<label id="disp_total_biaya_lain">0</label>
		                                                	<p class="text-size-mini text-info" id="disp_total_biaya_lain_kurs"></p>
		                                                </td>
		                                            </tr>
		                                            <tr>
		                                                <td>&nbsp;</td>
		                                                <td class="text-right text-bold">TOTAL DAFTAR BARANG + TOTAL BIAYA LAIN</td>
		                                                <td class="text-right text-bold">
		                                                	<label id="disp_grand_total">0</label>
		                                                	<p class="text-size-mini text-info" id="disp_grand_total_kurs"></p>
		                                                </td>
		                                            </tr>
		                                        </tfoot>
		                                    </table>
		                                </div>
		                            </div>
		                        </div>
		                    </fieldset>
		                </div>
		            </div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="">
						<input type="hidden" id="kurs_id" name="kurs_id" value="">
						<input type="hidden" id="kurs" name="kurs" value="1">
						<input type="hidden" id="total" name="total" value="0">
						<input type="hidden" id="total_kurs" name="total_kurs" value="0">
						<input type="hidden" id="is_ppn" name="is_ppn" value="0">
						<input type="hidden" id="total_ppn" name="total_ppn" value="0">
						<input type="hidden" id="total_ppn_kurs" name="total_ppn_kurs" value="0">
						<input type="hidden" id="total_biaya_lain" name="total_biaya_lain" value="0">
						<input type="hidden" id="total_biaya_lain_kurs" name="total_biaya_lain_kurs" value="0">
						<input type="hidden" id="status" name="status" value="<?php echo $this->config->item('status_po_waiting_for_delivery'); ?>">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php $this->load->view('logistik/list-barang-modal'); ?>
<?php $this->load->view('logistik/history-barang-modal'); ?>