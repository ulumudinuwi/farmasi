<html>
<body>
<style>
body {
    line-height: 1.2em;
    font-size: 12px;
    font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
    margin-top: 0;
    margin-bottom: 5px;
}
h1 {
    font-size: 26px;
}
h2 {
    font-size: 20px;
}
h3 {
    font-size: 18px;
}
h4 {
    font-size: 16px;
}
h5 {
    font-size: 14px;
}
h6 {
    font-size: 12px;
}
table {
    border-collapse: collapse;
    font-size: 12px;
}
table th,
table td {
    vertical-align: top;
    padding: 3px 10px;
    line-height: 1.5384616;
}
.table thead th {
    color: #fff;
    background-color: #607D8B;
    font-weight: bold;
    text-align: center;
}
.text-left {
    text-align: left;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.text-bold {
  font-weight: bold;
}
.uppercase {
    text-transform: uppercase !important;
}
.text-size-mini {
    font-size: 8px;
}
.text-muted {
    color: #999999;
}
.footer_current_date_user {
    color: #d10404;
    vertical-align: top;
    margin-top: 10px;
}
</style>
<?php
    function genLabelKurs($harga = 0, $hargaKurs = 0, $kursId = null) {
        $tmp = number_format($harga, 2, ",", ".");
        if($kursId) {
            $tmp .= "<p class='text-size-mini text-muted'>Rp. ".number_format($hargaKurs, 2, ",", ".")."</p>";
        }
        return $tmp;
    }

    $labelKurs = "<span class='text-size-mini text-muted'>(IDR)</span>";
    if($obj->kurs_id) $labelKurs = "<span class='text-size-mini text-muted'>({$obj->kurs_code})</span>";
?>

<h4 class="text-center text-bold no-margin no-padding">PURCHASE ORDER</h4>
<h6 class="text-center text-bold no-margin no-padding">Nomor <?php echo $obj->kode; ?></h6>
<table style="width: 100%;" style="font-size: 11px;">
    <tr>
        <td style="width: 50%;">
            <table>
                <tr>
                    <td class="text-bold">TANGGAL</td>
                    <td>:</td>
                    <td><?php echo strtoupper($obj->indo_tanggal); ?></td>
                </tr>
                <tr>
                    <td class="text-bold"><?php echo strtoupper($this->lang->line('pabrik_label')); ?></td>
                    <td>:</td>
                    <td><?php echo $obj->pabrik; ?></td>
                </tr>
            </table>
        </td>
        <td style="width: 50%;">
            <table>
                <tr>
                    <td class="text-bold">SIFAT</td>
                    <td>:</td>
                    <td><?php echo $obj->sifat_desc; ?></td>
                </tr>
                <tr>
                    <td class="text-bold"><?php echo lang('label_kurs'); ?></td>
                    <td>:</td>
                    <td>
                        <?php 
                            if($obj->kurs_id) {
                                echo $obj->kurs_code."<p class='text-size-mini text-muted'>1 {$obj->kurs_name} = Rp. ".number_format($obj->kurs, 2, ",", ".").'</p>'; 
                            } else echo "&mdash;";
                        ?> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<h4 class="text-bold">Daftar Barang</h4>
<table class="table table-bordered table-striped" border="1" style="font-size: 11px; width: 100%;">
    <thead>
        <tr class="bg-slate">
            <th style="width: 8%;">NO.</th>
            <th style="width: 25%;">BARANG</th>
            <th style="width: 15%;">SATUAN</th>
            <th style="width: 12%;">QTY</th>
            <th style="width: 20%;">HARGA <?php echo $labelKurs; ?></th>
            <th style="width: 20%;">SUBTOTAL <?php echo $labelKurs; ?></th>
        </tr>
    </thead>
    <?php 
        if(count($obj->details) > 0):
            $no = 1;
            foreach ($obj->details as $i => $row): 
    ?>
    <tbody>
        <tr>
            <td class="text-center"><?php echo $no; ?></td>
            <td><?php echo $row->barang; ?></td>
            <td><?php echo $row->satuan_po; ?></td>
            <td class="text-right"><?php echo number_format($row->qty, 2, ",", "."); ?></td>
            <td class="text-right">
                <?php echo genLabelKurs($row->harga, $row->harga_kurs, $obj->kurs_id); ?>        
            </td>
            <td class="text-right">
                <?php echo genLabelKurs($row->total, $row->total_kurs, $obj->kurs_id); ?>        
            </td>
        </tr>
    <?php 
            $no++;
        endforeach; 
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="text-bold">TOTAL</td>
            <td class="text-right">
                <?php echo genLabelKurs($obj->total, $obj->total_kurs, $obj->kurs_id); ?>                
            </td>
        </tr>
        <tr>
            <td colspan="5" class="text-bold">TOTAL PPN</td>
            <td class="text-right">
                <?php echo genLabelKurs($obj->total_ppn, $obj->total_ppn_kurs, $obj->kurs_id); ?>                        
            </td>
        </tr>
        <tr>
            <td colspan="5" class="text-bold">TOTAL DAFTAR BARANG</td>
            <td class="text-right">
                <?php echo genLabelKurs($obj->total + $obj->total_ppn, $obj->total_kurs + $obj->total_ppn_kurs, $obj->kurs_id); ?>                        
            </td>
        </tr>
    </tfoot>
    <?php else: ?>
    <tbody>
        <tr>
            <td style="font-weight: bold;text-align: center;" colspan="6">TIDAK ADA DATA</td>
        </tr>
    </tbody>
    <?php endif; ?>
</table>
<h4 style="margin-top: 10px;" class="text-bold">Biaya Lain</h4>
<table class="table table-bordered table-striped" border="1" style="font-size: 11px; width: 100%;">
    <thead>
        <tr class="bg-slate">
            <th style="width: 8%;">NO.</th>
            <th>DESKRIPSI</th>
            <th style="width: 20%;">TOTAL <?php echo $labelKurs; ?></th>
        </tr>
    </thead>
    <?php 
        if(count($obj->biaya_lains) > 0):
            $no = 1;
            foreach ($obj->biaya_lains as $i => $row): 
    ?>
    <tbody>
        <tr>
            <td class="text-center"><?php echo $no; ?></td>
            <td><?php echo $row->deskripsi; ?></td>
            <td class="text-right">
                <?php echo genLabelKurs($row->total, $row->total_kurs, $obj->kurs_id); ?>        
            </td>
        </tr>
    <?php 
            $no++;
        endforeach; 
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2" class="text-bold">TOTAL</td>
            <td class="text-right">
                <?php echo genLabelKurs($obj->total, $obj->total_kurs, $obj->kurs_id); ?>                
            </td>
        </tr>
        <tr>
            <td colspan="2" class="text-bold">TOTAL DAFTAR BARANG + TOTAL BIAYA LAIN</td>
            <td class="text-right">
                <?php echo genLabelKurs($obj->grand_total, $obj->grand_total_kurs, $obj->kurs_id); ?>                        
            </td>
        </tr>
    </tfoot>
    <?php else: ?>
    <tbody>
        <tr>
            <td style="font-weight: bold;text-align: center;" colspan="3">TIDAK ADA DATA</td>
        </tr>
    </tbody>
    <?php endif; ?>
</table>
<table style="width: 100%; font-size: 8px;">
    <tr>
        <td style="width: 10%;" class="text-bold">TERBILANG</td>
        <td style="width: 1%;">:</td>
        <td style="width: 86%;" class="uppercase"><?php echo $obj->terbilang; ?> <?php echo $labelKurs; ?></td>
    </tr>
</table>
<br />
<table style="width: 100%; font-size: 10px;">
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 40%;"></td>
        <td style="width: 30%;"></td>
    </tr>
    <tr>
        <td class="text-center">PENANGGUNG JAWAB</td>
        <td>&nbsp;</td>
        <td class="text-center"><?php echo strtoupper($this->lang->line('pabrik_label')); ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="text-bold text-center">( <?php echo $obj->pengajuan_by; ?> )</td>
        <td>&nbsp;</td>
        <td class="text-bold text-center">( <?php echo $obj->pabrik; ?> )</td>
    </tr>
</table>
<div class="footer_current_date_user text-size-mini">
    <span><?php echo $current_date.", ".$current_user; ?></span>
</div>
</body>
</html>