<script type="text/javascript">
	var UID = "<?php echo $uid; ?>",
		VIEW_MODE = 1,
		ADD_MODE = 0;
		USE_PPN = 1;
		form = '#form';

	var tablePo = $('#table-detail-po'),
			tableDetail = $('#table-penerimaan'),
			tableDetailDt,
			modalDataBarang = $('#data-barang-modal'),
			tableDataBarang = $('#table-data-barang'),
			tableDataBarangDt;

	var url = {
		getData: "<?php echo site_url('api/gudang_farmasi/penerimaan/get_data/:UID'); ?>",
		save: "<?php echo site_url('api/gudang_farmasi/penerimaan/save'); ?>",
		index: "<?php echo site_url('gudang_farmasi/penerimaan'); ?>",
	};

	function fillForm(uid) {
		blockElement($(form));
		$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
			if (status === 'success') {
				data = data.data;
				$("#detail_kode").html(data.kode);
				$("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
				$("#detail_sifat").html(data.sifat_desc);
				$("#detail_no_invoice").html(data.no_invoice ? data.no_invoice : '&mdash;');
				$("#detail_pabrik").html(data.pabrik);
				$("#detail_status").html(data.status_desc);
				USE_PPN = data.is_ppn;

				switch(parseInt(data.status)) {
					case 5: // full receive
					case 7: // dialihkan
						$("#section_input_penerimaan, .btn-save").hide();
						$("a[href='#accordion-group1']").click();
						$("#detail_keterangan").html("");
						if(parseInt(data.status) === 7) { // Ditolak, Dialihkan
							let keterangan = data.keterangan ? `<b>Alasan</b>:<br/>${data.keterangan}` : "";
							$("#detail_keterangan").html(keterangan);
						}
						break;
					 default:
						$("#status_penerimaan").text("-");
						break;
				}

				var isDTable = $.fn.dataTable.isDataTable($('#table_detail_po'));
				if(isDTable === true) $('#table_detail_po').DataTable().destroy();

				$('#table_detail_po').DataTable({
						"ordering": false,
						"processing": true,
						"aaData": data.details,
						"columns": [
							{ 
								"data": "barang",
								"render": function (data, type, row, meta) {
										let tmp = data;
										tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
										return tmp;
								},
							},
							{ "data": "satuan_po" },
							{ 
									"data": "qty",
									"render": function (data, type, row, meta) {
											return numeral(data).format('0.0,');
									},
									"className": "text-right"
							},
							/*{
									"data": "harga",
									"render": function (data, type, row, meta) {
											return numeral(data).format('0.0,');
									},
									"className": "text-right"
							},
							{
									"data": "total",
									"render": function (data, type, row, meta) {
											return numeral(row.total).format('0.0,');
									},
									"className": "text-right"
							},*/
							{ 
									"data": "diterima",
									"render": function (data, type, row, meta) {
											return numeral(data).format('0.0,');
									},
									"className": "text-right"
							},
						]
				});

				let labelTotalKurs = "";
				let labelTotalPpnKurs = "";
				let labelGrandTotalKurs = "";
				if(data.kurs_id) {
					labelTotalKurs = 'Rp. ' + numeral(data.total_kurs).format('0.0,');
					labelTotalPpnKurs = 'Rp. ' + numeral(data.total_ppn_kurs).format('0.0,');
					labelGrandTotalKurs = 'Rp. ' + numeral(data.grand_total_kurs).format('0.0,');
				}
				$('#detail_total').html(numeral(data.total).format('0.0,'));
				$('#detail_total_kurs').html(labelTotalKurs);
				$('#detail_total_ppn').html(numeral(data.total_ppn).format('0.0,'));
				$('#detail_total_ppn_kurs').html(labelTotalPpnKurs);
				$('#detail_grand_total').html(numeral(data.grand_total).format('0.0,'));
				$('#detail_grand_total_kurs').html(labelGrandTotalKurs);

				// START input penerimaan
				tableDetailDt = tableDetail.DataTable({
					"info": false,
					"ordering": false,
					"paginate": false,
					"drawCallback": function (settings) {
						$('.input-qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
						/*$('.input-disc').autoNumeric('init', {aSep: '.', aDec: ',', vMin: '0.00', vMax: '100.00'});
						$('.input-harga').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});*/
						$('.styled').uniform();
						$('.checkbox-inline')
										.css('top', '-15px')
										//.css('left', '-6px')
										.css('padding-left', '0')
										.css('padding-right', '0');

						let tr = tableDetail.find("tbody tr");
						tr.each(function() {
							$(this).find('td:eq(6)').addClass('text-center');
							$(this).find('.input-qty').trigger('change');
							$(this).find('input').prop('readonly', true);

							let id = $(this).find('.input-barang_id').val();
							$(this).prop('id', "row-barang-" + id);
						});
					},
				});

				for (var i = 0; i < data.details.length; i++) {
					let qty = parseFloat(data.details[i].qty);
					let diterima = parseFloat(data.details[i].diterima);
					if(qty > diterima) columnBarang(data.details[i]);
				}
				// END input penerimaan

				$("#pabrik_id").val(data.pabrik_id);
				$("#sifat").val(data.sifat);
				$("#po_id").val(data.id);
				$("#kurs_id").val(data.kurs_id);
				$("#kurs").val(data.kurs);

				$(".th-kurs").html('');
				let kurs = `&mdash;`;
				if(data.kurs_id) {
					$(".th-kurs").html(`(${data.kurs_code})`);
					kurs = `${data.kurs_code}`;
					kurs += `<p class="text-size-mini text-muted">1 ${data.kurs_name} = Rp. ${numeral(data.kurs).format('0.0,')}</p>`;
				}
				$("#detail_label_kurs").html(kurs);
				
				// START history penerimaan
				$("#section_history_penerimaan").hide('slow');
				if (typeof data.history !== 'undefined') {
					$("#section-data-history-penerimaan").append('<div class="panel-group content-group-lg mb-10" id="history_accordion"></div>');
					for (var i = 0; i < data.history.length; i++) {
						var history = data.history[i];
						var tanggal = moment(history.tanggal).format('DD-MM-YYYY HH:mm');
						var status_history = "<span class='pull-right text-info'><i class='glyphicon glyphicon-asterisk'></i> Diterima Sebagian</span>";
						switch(parseInt(history.status)) {
							case 2:
								var status_history = "<span class='pull-right text-success'><i class='icon-check'></i> Diterima Semua</span>";
								break;
						}
						var keterangan = history.keterangan ? history.keterangan : "-";

						var rows = '<div class="panel panel-white">';
						rows += ' <div class="panel-heading">';
						rows += '   <h6 class="panel-title">';
						rows += '     <a class="collapsed" data-toggle="collapse" data-parent="#history_accordion" href="#history_' + ( i + 1 ) + '" aria-expanded="false">';
						rows += '       Penerimaan ' + tanggal;
						rows +=         status_history;
						rows += '     </a>';
						rows += '   </h6>';
						rows += ' </div>';
						rows += ' <div id="history_' + ( i + 1 ) + '" class="panel-collapse collapse" aria-expanded="false">';
						rows += '   <div class="panel-body form-inline" style="padding-left: 30px;">';
						rows += '     <div class="form-group mr-10">';
						rows += '       <label>Tanggal</label>';
						rows += '       <p class="form-control-static text-bold">' + tanggal + '</p>';
						rows += '     </div>';
						rows += '     <div class="form-group mr-10">';
						rows += '       <label>No. Penerimaan</label>';
						rows += '       <p class="form-control-static text-bold">' + history.kode + '</p>';
						rows += '     </div>';
						rows += '     <div class="form-group mr-10">';
						rows += '       <label>No. Faktur</label>';
						rows += '       <p class="form-control-static text-bold">' + history.no_faktur + '</p>';
						rows += '     </div>';
						rows += '   </div>';
						rows += '   <div class="table-responsive">';
						rows += '     <table id="history_table_' + ( i + 1 ) + '" class="table table-bordered table-striped">';
						rows += '       <thead>';
						rows += '         <tr class="bg-slate">';
						rows += '           <th rowspan="2" style="width: 20%;">Kode</th>';
						rows += '           <th rowspan="2">Nama</th>';
						rows += '           <th rowspan="2" style="width: 15%;">Satuan</th>';
						/*rows += '           <th colspan="2" class="text-center" style="width: 18%;">Harga ' + (history.kurs_id ? `<span class="text-size-mini text-info">(${history.kurs_code})</span>` : '') + '</th>';*/
						rows += '           <th colspan="3" class="text-center" style="width: 18%;">Jumlah</th>';
						rows += '           <th rowspan="2" style="width: 15%;">Data Barang</th>';
						rows += '         </tr>';
						rows += '         <tr class="bg-slate">';
						/*rows += '           <th class="text-center">Order</th>';
						rows += '           <th class="text-center">Terima</th>';*/
						rows += '           <th class="text-center">Order PO</th>';
						rows += '           <th class="text-center">Terima Kantor</th>';
						rows += '           <th class="text-center">Sisa Gudang Utama</th>';
						rows += '         </tr>';
						rows += '       </thead>';
						rows += '       <tbody>';
						rows += '      <table>';
						rows += '     <div class="col-sm-12 mt-10">';
						rows += '       <div class="col-md-8">';
						rows += '         <label>Keterangan</label>';
						rows += `         <div class="form-control-static" id="keterangan_po">${keterangan}</div>`;
						rows += '       </div>';
						rows += '     </div>';
						rows += '   </div>';
						rows += ' </div>';
						rows += '</div>';

						$("#history_accordion").append(rows);
						
						if (typeof history.details !== 'undefined') {
							let curHisTable = $('#history_table_' + ( i + 1 ));
							let isDTable = $.fn.dataTable.isDataTable(curHisTable);
							if(isDTable === true) curHisTable.DataTable().destroy();

							curHisTable.DataTable({
								"ordering": false,
								"bProcessing": true,
								"aaData": history.details,
								"columns": [
									{ "data": "kode_barang"},
									{ "data": "barang" },
									{ "data": "satuan" },
									/*{ 
										"data": "harga_po",
										"render": function (data, type, row, meta) {
											return `<span class="label-history-harga_po">${numeral(data).format('0,0.')}</span>`;
										},
										"className": "text-right"
									},
									{ 
										"data": "harga",
										"render": function (data, type, row, meta) {
											return `<span class="label-history-harga">${numeral(data).format('0,0.')}</span>`;
										},
										"className": "text-right"
									},*/
									{ 
										"data": "qty_po",
										"render": function (data, type, row, meta) {
											return numeral(data).format('0,0.');
										},
										"className": "text-right"
									},
									{ 
										"data": "qty",
										"render": function (data, type, row, meta) {
											return numeral(data).format('0,0.');
										},
										"className": "text-right"
									},
									{ 
										"data": "sisa",
										"render": function (data, type, row, meta) {
											return numeral(data).format('0,0.');
										},
										"className": "text-right"
									},
									{ 
										"data": "expired_date",
										"render": function (data, type, row, meta) {
											let tmp = `<span class="text-size-mini text-info"><b>Exp. Date:</b><br/>${data}</span>`;
											tmp += `<br/><span class="text-size-mini text-info"><b>No. Batch:</b><br/>${row.no_batch}</span>`;
											return tmp;
										},
									},
								],
								"drawCallback": function (settings) {
									let tr = curHisTable.find("tbody tr");
									tr.each(function() {
										/*let harga_po = parseFloat(numeral($(this).find('.label-history-harga_po').html())._value);
										let harga = parseFloat(numeral($(this).find('.label-history-harga').html())._value);
										let td = $(this).children().eq(4);
										td.removeClass('bg-danger');
										if(harga > harga_po) {
											td.addClass('bg-danger');
										}*/
									});
								},
							});
						}
					}
				}
				if(data.history.length > 0) $("#section_history_penerimaan").show('slow');

				$(form).unblock();
			}
		})
	}

	function columnBarang(data) {
		tableDetailDt.row.add([
			// Col
			`<input type="hidden" class="input-detail_id" name="detail_id[]" value="0">` +
			`<input type="hidden" class="input-po_detail_id" name="po_detail_id[]" value="${data.id}">` +
			`<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
			`<input type="hidden" class="input-harga_po" name="harga_po[]" value="${data.harga}">` +
			`<input type="hidden" class="input-harga" name="harga[]" value="${data.harga_diterima}">` +
			`<input type="hidden" class="input-disc" name="diskon_item[]" value="${data.diskon}">` +
			 `<label class="checkbox-inline">` +
			 `   <input type="checkbox" class="styled disp-checkbox">` +
			 `   <input  type="hidden" name="check_barang[]" class="input-checkbox" value="0">` +
			 `</label>`,
			// Col
			`<label class="label_barang">${data.barang}</label>` +
			`<br/><span class="text-slate-300 text-bold text-size-mini disp_barang mt-10">${data.kode_barang}</span>`,
			// Col
			`<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
			`<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
			`<label class="label-satuan">${data.satuan_po}</label>`,
			// Col
			/*`<input type="hidden" class="input-harga_po" name="harga_po[]" value="${data.harga}">` +
			`<label class="label-harga_po">${numeral(data.harga).format('0.0,')}</label>`,*/
			// Col
			/*`<input type="text" class="input-harga form-control text-right input-decimal" name="harga[]" value="${data.harga_diterima}">`,*/
			// Col
			`<input type="hidden" class="input-qty_po" name="qty_po[]" value="${data.qty}">` +
			`<label class="label-qty_po">${numeral(data.qty).format('0.0,')}</label>`,
			// Col
			`<input type="hidden" class="tmp-sisa" value="${data.sisa}">` +
			`<input type="hidden" class="input-sisa" name="sisa[]" value="${data.sisa}">` +
			`<label class="label-sisa">${numeral(data.sisa).format('0.0,')}</label>`,
			// Col
			`<input type="text" class="input-qty form-control text-right input-decimal" name="qty[]" value="${data.sisa}">`,
			// Col
			/*`<input type="text" class="form-control text-right input-disc" name="diskon_item[]" value="${data.diskon}">`,*/
			// Col
			`<button type="button" class="btn btn-warning btn-xs edit-data_barang" disabled>Edit</button>` +
			`<div class="div_data_barang" style="display: none;"></div>`,
		]).draw(false);
	}

	function columnDataBarang(data) {
		tableDataBarangDt.row.add([
			// Col 1
			`<input type="text" class="input-no_batch form-control" value="${data.no_batch}" placeholder="No. Batch" autofocus>`,
			// Col 2
			`<input type="text" class="input-exp_date form-control" value="${data.expired_date}" placeholder="DD/MM/YYYY">`,
			// Col 3
			`<input type="text" class="input-qty form-control text-right" value="${data.qty}">`,
			// Col 4
			`<button type="button" class="btn btn-danger btn-xs remove-row"><i class="fa fa-trash"></i></button>`,
		]).draw(false);
	}

	function updateGrandTotal() {
		let total = 0,
			total_ppn = 0,
			kursId = $('#kurs_id').val(),
			kurs = parseFloat($('#kurs').val());

		tableDetail.find('tbody tr').each(function (i, el) {
			let isCheck = $(el).find('.input-checkbox').val();
			if(isCheck == 1) {
				/*let harga  = isNaN(parseFloat($(el).find('.input-harga').autoNumeric('get'))) ? 0 : parseFloat($(el).find('.input-harga').autoNumeric('get'));
				let qty  = isNaN(parseFloat($(el).find('.input-qty').autoNumeric('get'))) ? 0 : parseFloat($(el).find('.input-qty').autoNumeric('get'));
				let diskon_item  = isNaN(parseFloat($(el).find('.input-disc').autoNumeric('get'))) ? 0 : parseFloat($(el).find('.input-disc').autoNumeric('get'));*/
				let harga  = isNaN(parseFloat($(el).find('.input-harga').val())) ? 0 : parseFloat($(el).find('.input-harga').val());
				let qty  = isNaN(parseFloat($(el).find('.input-qty').val())) ? 0 : parseFloat($(el).find('.input-qty').val());
				let diskon_item  = isNaN(parseFloat($(el).find('.input-disc').val())) ? 0 : parseFloat($(el).find('.input-disc').val());
				let sub_total = harga * qty;
				let total_diskon_item = sub_total * (diskon_item / 100);
				total += (sub_total - total_diskon_item);
			}
		});

		$("#label-total").html(numeral(total).format('0.0,'));
		$("#total").val(total);
		$("#total_kurs").val(total * kurs);

		let diskon = isNaN(parseFloat($('#diskon').autoNumeric('get'))) ? 0 : parseFloat($('#diskon').autoNumeric('get'));
		let total_diskon = total * (diskon / 100);
		$("#label-total_diskon").html(numeral(total_diskon).format('0.0,'));
		$('#total_diskon').val(total_diskon);
		$("#total_diskon_kurs").val(total_diskon * kurs);

		total -= total_diskon;
		$("#label-total_after_diskon").html(numeral(total).format('0.0,'));

		if(parseInt(USE_PPN) === 1) {
			total_ppn = total * 0.1;
			$("#label-total_ppn").html(numeral(total_ppn).format('0.0,'));
			$('#total_ppn').val(total_ppn);
			$('#total_ppn_kurs').val(total_ppn * kurs);
		}

		total += total_ppn;
		$("#label-grand_total").html(numeral(total).format('0.0,'));
		$('#grand_total').val(total);
		$("#label-grand_total_kurs").html('Rp.' + numeral(total * kurs).format('0.0,'));
		$('#grand_total_kurs').val(total * kurs);
	}

	$(document).ready(function() {
		$('.input-disc').autoNumeric('init', {aSep: '.', aDec: ',', vMin: '0.00', vMax: '100.00'});
		$(".wysihtml5-min").wysihtml5({
			parserRules:  wysihtml5ParserRules
		});
		$(".wysihtml5-toolbar").remove();

		$(".btn-batal").click(function() {
			window.location.assign(url.index);
		});

		$(".disp_tanggal").daterangepicker({
			singleDatePicker: true,
			startDate: moment("<?php echo date('Y-m-d'); ?>"),
			endDate: moment("<?php echo date('Y-m-d'); ?>"),
			applyClass: "bg-slate-600",
			cancelClass: "btn-default",
			opens: "center",
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY"
			}
		});

		$("#btn_tanggal").click(function () {
			let parent = $(this).parent();
			parent.find('input').data("daterangepicker").toggle();
		});

		// EVENT TABLE INPUT PENERIMAAN
		tableDetail.on('click', '.disp-checkbox', function() {
			let tr = $(this).parents('tr');

			tr.find('input:not(".disp-checkbox")').prop('readonly', true);
			tr.find('button').prop('disabled', true);
			tr.find('.input-checkbox').val(0);
			if($(this).prop('checked') === true) {
				tr.find('input:not(".disp-checkbox")').prop('readonly', false);
				tr.find('button').prop('disabled', false);
				tr.find('.input-checkbox').val(1);
			} 
			updateGrandTotal();
		});

		/*tableDetail.on('change blur keyup', '.input-harga', function() {
			let tr = $(this).parent().parent();
			let td = $(this).parent();
			let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
			let harga_po = parseFloat(tr.find('.input-harga_po').val());
			
			td.removeClass('bg-danger');
			if(val > harga_po) {
				td.addClass('bg-danger');
			}
			updateGrandTotal();
		});

		tableDetail.on('change blur keyup', '.input-qty', function() {
			let tr = $(this).parent().parent();
			let td = $(this).parent();
			let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
			let sisa = parseFloat(tr.find('.tmp-sisa').val());
			sisa = sisa - val;
			if(sisa < 0) sisa = 0;

			tr.find('.input-sisa').val(sisa);
			tr.find('.label-sisa').html(numeral(sisa).format('0.0,'));

			updateGrandTotal();
		});

		tableDetail.on('change blur keyup', '.input-disc', function() {
			updateGrandTotal();
		});

		$("#diskon").on('change keyup', function() {
			updateGrandTotal();
		});*/

		$(form).validate({
			rules: {
				tanggal: { required: true },
				no_faktur: { required: true },
			},
			submitHandler: function (form) {
				tableDetailDt.search('').draw(false);
				let checked = $('.disp-checkbox:checked');
				if (checked.length <= 0) {
					swal({
							title: "Peringatan!",
							text: "Pilih Barang terlebih dahulu.",
							html: true,
							type: "warning",
							confirmButtonColor: "#2196F3"
					});
					return;
				}

				swal({
					title: "Konfirmasi?",
					type: "warning",
					text: "Apakah data yang dimasukan telah benar??",
					showCancelButton: true,
					confirmButtonText: "Ya",
					confirmButtonColor: "#2196F3",
					cancelButtonText: "Batal",
					cancelButtonColor: "#FAFAFA",
					closeOnConfirm: true,
					showLoaderOnConfirm: true,
				},
				function() {
					$('.input-decimal').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					$('.input-bulat').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					/*$('.input-disc').each(function() {
						$(this).val(isNaN($(this).autoNumeric('get')) ? 0 : $(this).autoNumeric('get'));
					});*/

					$('input, textarea, select').prop('disabled', false);
					
					blockPage('Sedang diproses ...');
					var formData = $(form).serialize();
					$.ajax({
						data: formData,
						type: 'POST',
						dataType: 'JSON', 
						url: url.save,
						success: function(data){
							$.unblockUI();
							successMessage('Berhasil', "Penerimaan berhasil disimpan.");
							window.location.assign(url.index);
						},
						error: function(data){
							$.unblockUI();
							errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
						}
					});
					return false;
				});
			}
		});

		// START INPUT DATA BARANG
		tableDetail.on('click', '.edit-data_barang', function() {
			let tr = $(this).parents('tr');
			let dataBarang = tr.find('.div_data_barang');
			let barang_id = tr.find('.input-barang_id').val();
			let barang = tr.find('.label_barang').html();
			let qty = isNaN(parseFloat(tr.find('.input-qty').autoNumeric('get'))) ? 0 : parseFloat(tr.find('.input-qty').autoNumeric('get'));

			modalDataBarang.find('#data_barang_id').val(barang_id);
			modalDataBarang.find('#label-data-barang-nama').html(barang);
			modalDataBarang.find('#label-data-barang-qty').html(numeral(qty).format('0.0,'));

			var isDTable = $.fn.dataTable.isDataTable(tableDataBarang);
			if(isDTable === true) {
				tableDataBarangDt.clear().draw();
			} else {
				tableDataBarangDt = tableDataBarang.DataTable({
					"info": false,
					"ordering": false,
					"searching": false,
					"paginate": false,
					"drawCallback": function (settings) {
						$('.input-qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
						$('.input-exp_date').formatter({ pattern: '{{99}}/{{99}}/{{9999}}' });
						$('.input-no_batch').focus().select();
					},
				});
			}

			if(dataBarang.html() !== "") {
				let trDataBarang = dataBarang.find('div');
				trDataBarang.each(function() {
					let no_batch = $(this).find('.input-hidden-no_batch').val();
					let exp_date = $(this).find('.input-hidden-exp_date').val();
					let qty = $(this).find('.input-hidden-qty').val();

					columnDataBarang({
						barang_id: barang_id,
						no_batch: no_batch,
						expired_date: exp_date,
						qty: qty,
					});
				});
			}

			modalDataBarang.modal('show');
		});

		$('#btn-tambah-data-barang').click(function(e) {
			e.preventDefault();

			let barang_id = modalDataBarang.find('#data_barang_id').val();
			let qtyMaks = parseFloat(numeral(modalDataBarang.find('#label-data-barang-qty').html())._value);
			let qtySum = 0;
			tableDataBarang.find('tbody .input-qty').each(function (i, el) {
					let qty = isNaN(parseFloat($(el).autoNumeric('get'))) ? 0 : parseFloat($(el).autoNumeric('get'));
					qtySum += qty;
			});

			if((qtyMaks - qtySum) <= 0) {
				warningMessage('Peringatan!', 'Qty Terima telah terpenuhi.'); 
				return;
			}

			let data = {
				barang_id: barang_id,
				no_batch: '',
				expired_date: '',
				qty: qtyMaks - qtySum,
			}
			columnDataBarang(data);
		});

		tableDataBarang.on('change keyup', '.input-qty', function() {
			let qtyMaks = parseFloat(numeral(modalDataBarang.find('#label-data-barang-qty').html())._value);
			let qtySum = 0;
			tableDataBarang.find('tbody .input-qty').each(function (i, el) {
				let qty = isNaN(parseFloat($(el).autoNumeric('get'))) ? 0 : parseFloat($(el).autoNumeric('get'));
				qtySum += qty;
			});

			if((qtyMaks - qtySum) < 0) {
				warningMessage('Peringatan!', `Qty Terima tidak boleh melebihi ${qtyMaks}.`); 
				$(this).val(0).focus().select();
				return;
			}
		});

		tableDataBarang.on('keyup', '.input-exp_date', function() {
			let td = $(this).parent();
			
			let value = $(this).val();
			let day = value.substr(0, 2);
			day = isNaN(day) ? 0 : day;
			let month = value.substr(3, 2);
			month = isNaN(month) ? 0 : month;
			let year = value.substr(6, 4);
			year = isNaN(year) ? 0 : year;

			$("#btn-simpan-data-barang").prop('disabled', false);
			td.find('.span-exdate-error').remove();
			if(value != "") {
				if(parseInt(day) > 31 || parseInt(month) > 12 || year.length < 4) {
					$("#btn-simpan-data-barang").prop('disabled', true);
					if(td.find('.span-exdate-error').length <= 0) 
						td.append('<span class="label label-block label-danger span-exdate-error">Format Tanggal Salah</span>');
				}
			}
		});

		tableDataBarang.on('click', '.remove-row', function() {
			tableDataBarangDt
					.row($(this).parents('tr'))
					.remove()
					.draw();
		});

		$('#btn-simpan-data-barang').click(function(e) {
			e.preventDefault();

			let barang_id = modalDataBarang.find('#data_barang_id').val();
			let qtyMaks = parseFloat(numeral(modalDataBarang.find('#label-data-barang-qty').html())._value);
			let qtySum = 0;
			tableDataBarang.find('tbody .input-qty').each(function (i, el) {
					let qty = isNaN(parseFloat($(el).autoNumeric('get'))) ? 0 : parseFloat($(el).autoNumeric('get'));
					qtySum += qty;
			});

			if((qtyMaks - qtySum) > 0) {
				warningMessage('Peringatan!', 'Qty Terima belum terpenuhi.'); 
				return;
			}

			let tmp = '';
			tableDataBarang.find('tbody tr').each(function() {
				tmp += `<div>` +
								`<input type="hidden" class="input-hidden-no_batch" name="data_barang_no_batch_${barang_id}[]" value="${$(this).find('.input-no_batch').val()}">` +
								`<input type="hidden" class="input-hidden-exp_date" name="data_barang_expired_date_${barang_id}[]" value="${$(this).find('.input-exp_date').val()}">` +
								`<input type="hidden" class="input-hidden-qty" name="data_barang_qty_${barang_id}[]" value="` + $(this).find('.input-qty').autoNumeric('get') + `">` +
							 `</div>`;
			});

			let dataBarang = tableDataBarang.find('tbody');
			let tr = tableDetail.find(`#row-barang-${barang_id}`);
			tr.find('.div_data_barang').html(tmp);

			modalDataBarang.modal('hide');
		});
		// END INPUT DATA BARANG

		fillForm(UID);
		$('.sidebar-control').click();
	});
</script>