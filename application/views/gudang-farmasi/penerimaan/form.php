<style type="text/css">
	.table thead tr th {
		white-space: nowrap;
		vertical-align: middle;
	}

	.table tbody tr td {
		white-space: nowrap;
		vertical-align: top;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel-group panel-group-control panel-group-control-right mb-10" id="accordion1">
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title text-bold">
							<a data-toggle="collapse" data-parent="#accordion1" class="collapsed" href="#accordion-group1">
								Data PO 
							</a>
						</h6>
					</div>
					<div id="accordion-group1" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row mb-20">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Nomor</label>
										<div class="col-md-6">
											<div class="form-control-static text-bold" id="detail_kode"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Tanggal</label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_tanggal"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Sifat</label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_sifat"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Status</label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_status"></div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-lg-3 control-label">No. Invoice</label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_no_invoice"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label"><?php echo $this->lang->line('pabrik_label'); ?></label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_pabrik"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3"><?php echo lang('label_kurs'); ?></label>
										<div class="col-md-6">
											<div class="form-control-static" id="detail_label_kurs">&mdash;</div>
										</div>
									</div>
									<div class="form-group">
					                	<span id="detail_keterangan" class="text-info text-size-mini col-md-12"></span>
					              	</div>
								</div>
							</div>
							<div class="row">
				                <div class="col-sm-12">
				                    <fieldset>
				                        <legend class="text-bold">
				                            <i class="icon-list position-left"></i> <strong>Daftar Barang</strong>
				                        </legend>
				                        <div class="row">
				                            <div class="col-sm-12">
				                                <div class="table-responsive">
				                                    <table id="table_detail_po" class="table table-bordered">
				                                        <thead>
				                                            <tr class="bg-slate">
				                                                <th>Nama</th>
				                                                <th>Satuan</th>
				                                                <th>Qty</th>
				                                                <!-- <th>Harga <span class="th-kurs text-info text-bold"></span></th>
				                                                <th>Sub Total <span class="th-kurs text-info text-bold"></span></th> -->
				                                                <th>Diterima</th>
				                                            </tr>
				                                        </thead>
				                                        <tbody></tbody>
				                                        <!-- <tfoot>
				                                            <tr>
				                                                <td colspan="4" class="text-right text-bold">TOTAL</td>
				                                                <td class="text-right text-bold">
				                                                	<label id="detail_total">0</label>
		                                                			<p class="text-size-mini text-info" id="detail_total_kurs"></p>
				                                                </td>
				                                                <td></td>
				                                            </tr>
				                                            <tr>
				                                                <td colspan="4" class="text-right text-bold">TOTAL PPN</td>
				                                                <td class="text-right text-bold">
				                                                	<label id="detail_total_ppn">0</label>
		                                                			<p class="text-size-mini text-info" id="detail_total_ppn_kurs"></p>
				                                                </td>
				                                                <td></td>
				                                            </tr>
				                                            <tr>
				                                                <td colspan="4" class="text-right text-bold">GRAND TOTAL</td>
				                                                <td class="text-right text-bold">
				                                                	<label id="detail_grand_total">0</label>
		                                                			<p class="text-size-mini text-info" id="detail_grand_total_kurs"></p>
				                                                </td>
				                                                <td></td>
				                                            </tr>
				                                        </tfoot> -->
				                                    </table>
				                                </div>
				                            </div>
				                        </div>
				                    </fieldset>
				                </div>
				            </div>
						</div>
					</div>
				</div>	
			</div>
			<div id="section_history_penerimaan">
				<div class="h4 mt-20">History Penerimaan</div>
				<div id="section-data-history-penerimaan"></div>
			</div>
			<div class="panel panel-white">
				<div id="section_input_penerimaan">
					<div class="panel-heading">
						<h6 class="panel-title text-bold">Penerimaan</h6>
					</div>
					<div class="panel-body form-horizontal" style="padding-left: 30px;">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-3 input-required">Tanggal</label>
								<div class="col-md-9">
									<div class="input-group">
										<span class="input-group-addon cursor-pointer" id="btn_tanggal">
											<i class="icon-calendar22"></i>
										</span>
										<input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-3 input-required">No. Faktur</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="no_faktur" name="no_faktur" class="form-control" placeholder="No. Faktur ...">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table id="table-penerimaan" class="table table-bordered table-striped">
							<thead>
								<tr class="bg-slate">
									<th rowspan="2" style="width: 5%;" class="text-center"></th>
									<th rowspan="2">Nama</th>
	                                <th rowspan="2" style="width: 15%;">Satuan</th>
	                                <!-- <th colspan="2" class="text-center" style="width: 25%;">Harga <span class="th-kurs text-info text-bold"></span></th> -->
	                                <th colspan="3" class="text-center" style="width: 30%;">Qty</th>
	                                <!-- <th rowspan="2" style="width: 10%;">Disc (%)</th> -->
	                                <th rowspan="2" style="width: 12%;">Data Barang</th>
								</tr>
								<tr class="bg-slate">
									<!-- <th class="text-center">Order</th>
									<th class="text-center">Terima</th> -->
									<th class="text-center">Order PO</th>
									<th class="text-center">Sisa Gudang Utama</th>
									<th class="text-center">Terima Kantor</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot class="hide">
	                            <tr>
	                                <td colspan="6" class="text-right text-bold">
	                            		SUB TOTAL
	                                </td>
	                                <td class="text-right text-bold" id="label-total">0</td>
	                            </tr>
	                            <tr>
	                                <td colspan="6" class="text-right text-bold">DISCOUNT(%)</td>
	                                <td class="text-right text-bold">
	                                	<input type="text" class="form-control text-right input-disc" name="diskon" id="diskon" value="0">
	                                	<span class="text-info text-size-mini" id="label-total_diskon">0</span>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td colspan="6" class="text-right text-bold">
	                            		SUB TOTAL <span class="text-size-mini text-info">(Setelah Diskon)</span>
	                                </td>
	                                <td class="text-right text-bold" id="label-total_after_diskon">0</td>
	                            </tr>
	                            <tr>
	                                <td colspan="6" class="text-right text-bold">
	                                	TOTAL PPN
	                                </td>
	                                <td class="text-right text-bold" id="label-total_ppn">0</td>
	                            </tr>
	                            <tr>
	                                <td colspan="6" class="text-right text-bold">
	                                	GRAND TOTAL
	                                </td>
	                                <td class="text-right text-bold">
	                                	<label id="label-grand_total">0</label>
			                            <p class="text-size-mini text-info" id="label-grand_total_kurs"></p>
	                                </td>
	                            </tr>
	                        </tfoot>
						</table>
						<div class="col-md-12">
							<p class="text-info"><i class="icon-info22"></i> Untuk melakukan penerimaan, silahkan pilih barang dengan mengchecklist terlebih dahulu checkbox.</p>
							<p class="text-info"><i class="icon-info22"></i> Untuk memasukan exp. date dan no. batch, silahkan tekan tombol edit pada kolom data barang.</p>
							<p class="text-info"><i class="icon-info22"></i> Kolom merah menandakan harga pada saat penerimaan melebihi harga PO.</p>
						</div>
						<div class="panel-body form-horizontal mt-10">
							<div class="col-md-8">
								<label>Keterangan</label>
								<div>
									<textarea class="form-control wysihtml5-min" id="keterangan" name="keterangan"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right mt-10 mb-10">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
				<div>
					<input type="hidden" id="uid" name="uid" value="0">
					<input type="hidden" id="pabrik_id" name="pabrik_id" value="0">
					<input type="hidden" id="sifat" name="sifat" value="">
					<input type="hidden" id="po_id" name="po_id" value="0">
					<input type="hidden" id="kurs_id" name="kurs_id" value="">
					<input type="hidden" id="kurs" name="kurs" value="1">
					<input type="hidden" name="total" id="total" value="0">
					<input type="hidden" name="total_kurs" id="total_kurs" value="0">
					<input type="hidden" name="total_diskon" id="total_diskon" value="0">
					<input type="hidden" name="total_diskon_kurs" id="total_diskon_kurs" value="0">
					<input type="hidden" name="total_ppn" id="total_ppn" value="0">
					<input type="hidden" name="total_ppn_kurs" id="total_ppn_kurs" value="0">
					<input type="hidden" name="grand_total" id="grand_total" value="0">
					<input type="hidden" name="grand_total_kurs" id="grand_total_kurs" value="0">
				</div>
			</div>
		</form>
	</div>
</div>
<?php $this->load->view('gudang-farmasi/penerimaan/data-barang-modal'); ?>