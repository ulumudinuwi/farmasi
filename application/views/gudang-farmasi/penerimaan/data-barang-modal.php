<div id="data-barang-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal">
				<div class="modal-header bg-slate">
					<h4>Data Barang</h4>
				</div>
				<div class="modal-body bg-light lt">
					<div class="alert alert-info alert-styled-left text-blue-800 content-group">
		                Untuk memasukan <span class="text-semibold">No. Batch</span> dan <span class="text-semibold">Exp. Date</span> Barang.
		                <button type="button" class="close" data-dismiss="alert">×</button>
		            </div>
					<div class="row m-t-md">
						<div class="col-sm-12">
							<div class="row form-horizontal">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Barang</label>
										<div class="col-md-8">
											<div class="form-control-static text-bold" id="label-data-barang-nama"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4">Qty. Terima</label>
										<div class="col-md-8">
											<div class="form-control-static text-bold" id="label-data-barang-qty"></div>
										</div>
									</div>
								</div>
							</div>
							<hr class="no-margin-top">
							<div class="table-responsive">
								<table id="table-data-barang" class="table table-striped table-bordered">
									<thead>
										<tr class="bg-slate">
											<th>No. Batch</th>
											<th>Exp. Date</th>
											<th>Qty</th>
											<th style="width: 10%;"></th>
										</tr>
									</thead>
									<tbody></tbody>
									<tfoot>
										<tr>
											<td colspan="4">
												<button type="button" id="btn-tambah-data-barang" class="btn btn-xs btn-primary btn-labeled">
													<b><i class="icon-plus-circle2"></i></b>
													Tambah
												</button>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="btn-simpan-data-barang" class="btn btn-primary">Simpan</button>
					<button type="button" id="btn-batal-data-barang" class="btn btn-default" data-dismiss="modal">Batal</button>
				</div>
				<div>
					<input type="hidden" id="data_barang_id" value="0" />
				</div>
			</form>
		</div>
	</div>
</div>