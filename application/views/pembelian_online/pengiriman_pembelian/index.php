<?php echo messages(); ?>
<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
		vertical-align: top;
	}
</style>
<div class="panel panel flat">
    <div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon cursor-pointer" id="btn_search_tanggal">
								<i class="icon-calendar22"></i>
							</span>
							<input type="text" id="search_range_tanggal" class="form-control rangetanggal-form">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="panel-heading">
        <ul class="nav nav-tabs nav-tabs nav-justified">
            <li class="active">
                <a href="#verifikasi_pos_manager_tab" data-toggle="tab" aria-expanded="true" id="tab-1">
                    <i class="icon-menu7 position-left"></i> Daftar Pengiriman Online 
                </a>
            </li>
            <li>
                <a href="#verifikasi_diskon_manager_tab" data-toggle="tab" aria-expanded="true" id="tab-2">
                    <i class="icon-menu7 position-left"></i> History Pengiriman Online
                </a>
            </li>
        </ul>
    </div>

    <div class="panel-body">
        <div class="tabbable">
            <div class="tab-content">
                <div class="tab-pane active" id="verifikasi_pos_manager_tab">
					<div class="table-responsive">
						<table id="table" class="table table-bordered table-striped">
							<thead>
                                <tr class="bg-slate">
                                    <th class="text-center" style="width: 10%">TANGGAL</th>
                                    <th class="text-center" style="width: 10%">NO INVOICE</th>
                                    <th class="text-center" style="width: 18%">NO DOKTER</th>
                                    <th class="text-center" style="width: 10%">DOKTER</th>
                                    <th class="text-center" style="width: 10%">STATUS</th>
                                    <th class="text-center" style="width: 10%">AKSI</th>
                                </tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
                </div>

                <div class="tab-pane" id="verifikasi_diskon_manager_tab">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="tableHistory">
                            <thead>
                                <tr class="bg-slate">
                                    <th class="text-center" style="width: 10%">TANGGAL</th>
                                    <th class="text-center" style="width: 10%">NO INVOICE</th>
                                    <th class="text-center" style="width: 18%">NO DOKTER</th>
                                    <th class="text-center" style="width: 10%">DOKTER</th>
                                    <th class="text-center" style="width: 10%">STATUS</th>
                                    <th class="text-center" style="width: 10%">AKSI</th>
                                </tr>
                            </thead>
							<tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('pembelian_online/pengiriman_pembelian/modal'); ?>