<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Pembelian Online</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="row mb-20">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pembelian</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">No Invoice</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="no_invoice">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Tanggal Tansaksi</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="tanggal_transaksi">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Status</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="status_pembayaran">-</label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Ekspedisi</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="ekspedisi">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Ongkos Kirim</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="ongkos_kirim">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Total Harga (Rp)</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="total_harga">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Total Pembayaran (Rp)</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="total_pembayaran">-</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Detail Pembelian</legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table id="table-detail" class="table table-bordered">
											<thead>
												<tr class="bg-slate">
													<th>Product</th>
													<th class="text-center" style="width: 20%;">Quantity</th>
													<th class="text-center" style="width: 20%;">Price (Rp)</th>
													<th class="text-center" style="width: 20%;">Total Price (Rp.)</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>

				<div class="row penolakanData">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i>Pengembalian Pesanan</legend>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label for="noInvoice" class="col-md-2 col-form-label">Alasan Penolakan</label>
										<div class="col-md-6">
											<textarea id="penolakan_desc" name="penolakan_desc" class="form-control"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<input type="hidden" name="uid" id="uid">
				<button type="button" class="btn btn-danger btn-delete" >Tolak</button>
				<button type="button" class="btn btn-success btn-confirm" >Konfirmasi</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<div id="detail-modal-lihat" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Pembelian Online</h6>
			</div>
			<div class="modal-body form-horizontal">
			</div>
			<div class="modal-footer m-t-none">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="PrintModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="PrintModalLabel">Cetak Faktur</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<iframe src="" height="450" width="100%" id="modalIframe"></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>