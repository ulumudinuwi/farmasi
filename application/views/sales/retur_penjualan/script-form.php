<script type="text/javascript">
	let UID = "<?php echo $uid; ?>",
	loadData = "<?php echo site_url('api/sales/retur_penjualan/getData/:UID'); ?>",
	loadDataBarang = "<?php echo site_url('api/gudang_farmasi/stock/load_data_modal'); ?>",
	save = "<?php echo site_url('api/sales/retur_penjualan/save'); ?>";

	/* variabel di modal */
	var tableBarang = $('#table-barang'),
	tableBarangDt,
	curRowBarang = 0,
	form = $('#form'),
	modalBarang = $('#list-barang-modal');

	$(document).ready(function () {
		tableBarangDt = tableBarang.DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": loadDataBarang,
				"type": "POST",
				"data": function(p) {
					p.pabrik_id = $('#pabrik_id').val();
				}
			},
			"columns": [
			{ 
				"data": "kode",
				"render": function (data, type, row, meta) {
					var template = `<span class="text-slate-300 text-bold text-size-mini mt-10"><?php echo $this->lang->line('pabrik_label'); ?>: ${row.pabrik}</span><br/>` + 
					'<a href="#" class="add-barang" data-id="{{ID}}" data-kode="{{KODE}}" data-nama="{{NAMA}}" data-satuan_id="{{SATUAN_ID}}" data-satuan_po="{{SATUAN}}" data-isi_satuan="{{ISI}}" data-maximum="{{MAX}}" data-stock="{{STOCK}}" data-minimum="{{MIN}}" data-harga="{{HARGA}}">{{KODE}}</a>';
					return template
					.replace(/\{\{ID\}\}/g, row.id)
					.replace(/\{\{KODE\}\}/g, row.kode)
					.replace(/\{\{NAMA\}\}/g, row.nama)
					.replace(/\{\{SATUAN_ID\}\}/g, row.satuan_id)
					.replace(/\{\{SATUAN\}\}/g, row.satuan)
					.replace(/\{\{ISI\}\}/g, row.isi_satuan)
					.replace(/\{\{MAX\}\}/g, row.maximum)
					.replace(/\{\{STOCK\}\}/g, row.stock)
					.replace(/\{\{MIN\}\}/g, row.minimum)
					.replace(/\{\{HARGA\}\}/g, row.harga_penjualan);
				},
			},
			{ "data": "nama" },
			{ 
				"data": "stock",
				"render": function (data, type, row, meta) {
					return data
				},
			},
			]
		});

		tableBarang.on('click', '.add-barang', function (e) {
			e.preventDefault();

			let id = $(this).data('id'),
			nama = $(this).data('nama'),
			harga = $(this).data('harga');
			stock = $(this).data('stock');

			let tr = tbody.find('tr');
			tr.each(function() {
				if(curRowBarang[0].sectionRowIndex !== $(this)[0].sectionRowIndex) {
					val = $(this).find('[name="barang_id[]"]').val();
					if(parseInt(id) === parseInt(val)) {
						curRowBarang.val("").trigger("change");
						errorMessage('Peringatan !', 'Barang telah dipilih sebelumnya. Silahkan pilih kembali !');
						return false;
					}
				}else{
					curRowBarang.find('[name="new_barang_id[]"]').val(id);
					curRowBarang.find('[name="new_detail_id[]"]').val(id);
					curRowBarang.find('[name="nama_barang[]"]').val(nama);
					curRowBarang.find('[name="harga[]"]').val(harga);
					curRowBarang.find('[name="qty[]"]').val('0');
					curRowBarang.find('[name="jumlah[]"]').val('0');
					curRowBarang.find('[name="retur[]"]').val('1');
					modalBarang.modal('hide');
				}
			});

		});

		if (UID) {
			$.getJSON(loadData.replace(':UID', btoa(UID)), function(res, status) {
				if(status === 'success') {
					let data = res.data;

					if (data) {
	                	//detail member
	                	$('#pos_id').val(data.id);
	                	$('#pos_uid').val(data.uid);
	                	$('#no_member').html(data.no_member_dokter);
	                	$('#member_id').val(data.id_dokter);
	                	$('#total_pembayaran').val(data.total_pembayaran);
	                	$('#nama_member').html(data.nama_dokter);
	                	$('#email_member').html(data.email_dokter);
	                	$('#no_hp').html(data.no_hp_dokter);
	                	$('#point').html(data.point_dokter);
	                	$('#plafon').html('Rp. '+data.plafon_dokter);
	                	$('#alamat').html(data.alamat_dokter);
	                	$('#nama_klinik').html(data.nama_klinik_dokter);
	                	$('#no_tlp_klinik').html(data.no_tlp_klinik_dokter);
	                	$('#alamat_klinik').html(data.alamat_klinik_dokter);

						//detail_transaksi
						$('#disp_tanggal_transaksi').html(data.tanggal_transaksi);
						$('#tanggal_transaksi').val(data.tanggal_transaksi);
						$('#no_invoice').val(data.no_invoice);
						$('#disp_tgl_jatuh_tempo').html(data.tgl_jatuh_tempo);
						$('#tgl_jatuh_tempo').val(data.tgl_jatuh_tempo);
						$('#nama_sales').html(data.nama_sales);
						$('#sales_id').val(data.id_sales);

						if (data.status_lunas == 1) {
							$('#status_lunas_cek_1').removeClass('hide');
							$('#status_lunas_cek_2').addClass('hide');
							$('#status_lunas').html('LUNAS');
							$('.style_media').addClass('border-info text-info');
						}else{
							$('#status_lunas_cek_1').addClass('hide');
							$('#status_lunas_cek_2').removeClass('hide');
							$('#status_lunas').html('BELUM');
							$('.style_media').addClass('border-danger text-danger');
						}

						$('#table_transaksi').find('tbody').empty();
						if(data.detail_transaksi.length <= 0) {
							addDetail({
								id: 0,
								barang_id: '',
								harga: 0.00,
								qty : 0,
								diskon : 0,
								jumlah : 0.00,
							});
						} else {
							for (var i = 0, n = data.detail_transaksi.length; i < n; i++) {
								addDetail(data.detail_transaksi[i]);
							}
						}
						$('#disp_ongkos_kirim').html(data.ongkir)
						$('#ongkos_kirim').val(data.ongkir)
						getGrandTotal();
					}
				}
			});
		}

		$(table_transaksi).find('.btn-tambah').on('click', function(){
			var tr = $(table_transaksi).find('tbody').find("tr").last();
			if (tr.find('[name="new_barang_id[]"]').val() == '') {
				errorMessage('Peringatan', "Barang tidak boleh kosong.");
				return;
			}else{
				addDetail({
					id: 0,
					barang_id: '',
					harga: 0.00,
					qty : 0,
					diskon : 0,
					grand_total : 0.00,
				});
			}
			$(table_transaksi).find('tbody').find("tr").last().find('.edit-row').click();
		});

		function addDetail(data) {

			var tbody = $('#table_transaksi').find('tbody');

			var count = $('[name="barang_id[]"]').length;

			var ID = (count + 1);

			var tr = $("<tr/>")
			.prop('id', 'row-cb-' + ID)
			.appendTo(tbody);

			//barang
			var tdBarang = $("<td/>")
			.appendTo(tr);
			//new barang
			var inputNewId = $("<input/>")
			.addClass('cb-input_new_id')
			.prop('type', 'hidden')
			.prop('name', 'new_detail_id[]')
			.val('0')
			.appendTo(tdBarang);
			var inputnewBarang = $("<input/>")
			.prop('name', 'new_barang_id[]')
			.prop('type', 'hidden')
			.val('0')
			.appendTo(tdBarang);

			//oldBarang
			var inputId = $("<input/>")
			.addClass('cb-input_id')
			.prop('type', 'hidden')
			.prop('name', 'detail_id[]')
			.val(data.id)
			.appendTo(tdBarang);
			var inputUid = $("<input/>")
			.addClass('cb-input_id')
			.prop('type', 'hidden')
			.prop('name', 'detail_uid[]')
			.val(data.uid)
			.appendTo(tdBarang);
			var inputBarang = $("<input/>")
			.prop('name', 'barang_id[]')
			.prop('type', 'hidden')
			.val(data.barang_id)
			.appendTo(tdBarang);
			var inputRetur = $("<input/>")
			.prop('name', 'retur[]')
			.prop('type', 'hidden')
			.val('0')
			.appendTo(tdBarang);
			var divBarang = $("<div/>")
			.addClass('input-group')
			.appendTo(tdBarang);
			var inputBarang = $("<input/>")
			.prop('name', 'nama_barang[]')
			.prop('type', 'text')
			.prop('readonly', true)
			.addClass('form-control')
			.val(data.nama_barang)
			.appendTo(divBarang);
			var spanBarang = $("<span/>")
			.addClass("input-group-btn")
			.appendTo(divBarang);
			var btncariBarang = $("<button/>")
			.addClass('btn btn-info searchBarang')
			.prop('disabled', true)
			.prop('type', 'button')
			.data('id', data.id)
			.html('<b><i class=" icon-search4"></i></b>')
			.appendTo(spanBarang);

			//harga
			var tdHarga = $("<td/>")
			.addClass('text-center')
			.appendTo(tr);
			var inputHarga = $("<input/>")
			.prop('name', 'harga[]')
			.addClass('form-control harga')
			.prop('readonly', true)
			.prop('type', 'text')
			.val(data.harga_penjualan)
			.appendTo(tdHarga);

			//qty
			var tdQty = $("<td/>")
			.addClass('text-center')
			.appendTo(tr);
			var inputOldQty = $("<input/>")
			.prop('name', 'old_qty[]')
			.addClass('form-control old_qty')
			.prop('readonly', true)
			.prop('type', 'hidden')
			.prop('min', '1')
			.val(data.qty)
			.appendTo(tdQty);
			var inputQty = $("<input/>")
			.prop('name', 'qty[]')
			.addClass('form-control qty')
			.prop('readonly', true)
			.prop('type', 'number')
			.prop('min', '1')
			.val(data.qty)
			.appendTo(tdQty);

			//disc
			var tdDisc = $("<td/>")
			.addClass('text-center')
			.appendTo(tr);
			var inputDisc = $("<input/>")
			.prop('name', 'diskon[]')
			.addClass('form-control diskon')
			.prop('readonly', true)
			.prop('min', '0')
			.prop('max', '100')
			.prop('type', 'number')
			.val(data.diskon)
			.appendTo(tdDisc);

			//jumlah
			var tdJumlah = $("<td/>")
			.addClass('text-center')
			.appendTo(tr);
			var inputJumlah = $("<input/>")
			.prop('name', 'jumlah[]')
			.addClass('form-control jumlah')
			.prop('readonly', true)
			.prop('type', 'text')
			.val(data.grand_total)
			.appendTo(tdJumlah);

			//action
			var tdAction = $("<td/>")
			.addClass('text-center')
			.appendTo(tr);
			var btnEdit = $("<a/>")
			.addClass('edit-row')
			.html('<b><i class="icon icon-pencil7 text-info"></i></b>')
			.appendTo(tdAction);
			var btnSave = $("<a/>")
			.addClass('save-row hide')
			.html('<b><i class="icon icon-floppy-disk text-info"></i></b>')
			.appendTo(tdAction);
			var btnDelete = $("<a/>")
			.addClass('delete-row hide')
			.html('<b><i class="icon icon-trash text-danger"></i></b>')
			.appendTo(tdAction);

			btnEdit.on('click', function () {
				//tr.remove();
				tr.find('.searchBarang').attr('disabled', false);
				tr.find('.searchBarang').attr('disabled', false);
				tr.find('.qty').prop('readonly', false);
				tr.find('.diskon').prop('readonly', false);
				tr.find(btnSave).removeClass('hide');
				tr.find(btnDelete).removeClass('hide');
				tr.find(btnEdit).addClass('hide');
				$('.btn-tambah, .simpan').attr('disabled', true);
			});
			btnSave.on('click', function () {
				//tr.remove();
				if (tr.find('.jumlah').val() == '0') {
					errorMessage('Peringatan', "Quantity tidak boleh kosong.");
					return;
				}else{
					tr.find('.searchBarang').attr('disabled', true);
					tr.find('.searchBarang').attr('disabled', true);
					tr.find('.qty').prop('readonly', true);
					tr.find('.diskon').prop('readonly', true);
					tr.find(btnSave).addClass('hide');
					tr.find(btnEdit).removeClass('hide');
					tr.find(btnDelete).addClass('hide');
					$('.btn-tambah, .simpan').attr('disabled', false);
				}
			});
			btnDelete.on('click', function () {
				tr.remove();
				getTotalharga();
				getGrandTotal();
				$('.btn-tambah, .simpan').attr('disabled', false);
			});

			tr.find('.qty').on('change', function(){
				let thisVal = $(this).val(),
				thisHarga = tr.find('.harga').val(),
				thisDiskon = tr.find('.diskon').val();
				thisJumlah = tr.find('.jumlah');

				thisTotal = thisHarga * thisVal;
				if (thisDiskon != 0) {
					countDiskon = thisTotal * thisDiskon / 100;
					thisTotal = thisTotal - countDiskon;
				}
				thisJumlah.val(thisTotal);
				getTotalharga();
				getGrandTotal();
			});

			tr.find('.diskon').on('change', function(){
				let thisVal = $(this).val(),
				thisHarga = tr.find('.harga').val(),
				thisQty = tr.find('.qty').val();
				thisJumlah = tr.find('.jumlah');

				thisTotal = thisHarga * thisQty;
				if (thisVal != 0) {
					countDiskon = thisTotal * thisVal / 100;
					thisTotal = thisTotal - countDiskon;
				}
				thisJumlah.val(thisTotal);
				getTotalharga();
				getGrandTotal();
			});

			tr.find('.searchBarang').on('click', function(){
				curRowBarang = tr;
				tableBarangDt.draw(false);
				modalBarang.modal('show');
			});

			getTotalharga();
		}

		function getTotalharga(){
			tbody = $('#table_transaksi').find('tbody');
			TotalHarga = 0;
			thisJumlah = tbody.find('.jumlah');
			thisJumlah.each(function(i){
				TotalHarga += parseFloat($(this).val());
			});
			$('#disp_total_harga_barang').html(TotalHarga);
			$('#total_harga_barang').val(TotalHarga);
		}

		function getGrandTotal(){
			let totalOngkir = $('#ongkos_kirim').val();
			tbody = $('#table_transaksi').find('tbody');
			TotalHarga = 0;
			thisJumlah = tbody.find('.jumlah');
			thisJumlah.each(function(i){
				TotalHarga += parseFloat($(this).val());
			});

			grandTotal = parseFloat(TotalHarga) + parseFloat(totalOngkir);
			$('#disp_grand_total').html(grandTotal);
			$('#grand_total').val(grandTotal);
		}
	});

	$(form).validate({
		rules: {
		},
		messages: {
		},
		errorPlacement: function(error, element) {
			var placement = $(element).closest('.input-group');
			if (placement.length > 0) {
				error.insertAfter(placement);
			} else {
				error.insertAfter($(element));
			}
		},
		focusInvalid: true,
		submitHandler: function (form) {
			$('input, textarea, select').prop('disabled', false);

			$('.input-decimal').each(function() {
				$(this).val($(this).autoNumeric('get'));
			});

			blockElement($(form));
			var formData = $(form).serialize();
			$.ajax({
				data: formData,
				type: 'POST',
				dataType: 'JSON', 
				url: save,
				success: function(data){
					$(form).unblock();
					successMessage('Berhasil', "Promo barang berhasil disimpan.");
					window.location.reload();
				},
				error: function(data){
					$(form).unblock();
					errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
				}
			});
			return false;
		}
	});

</script>