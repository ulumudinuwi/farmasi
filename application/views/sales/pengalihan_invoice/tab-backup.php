<div class="col-md-12">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <ul class="nav nav-tabs nav-tabs nav-justified">
                <li class="active">
                    <a href="#verifikasi_pos_manager_tab" data-toggle="tab" aria-expanded="true">
                        <i class="icon-menu7 position-left"></i> Approval Pos Manager
                    </a>
                </li>
                <li>
                    <a href="#verifikasi_diskon_manager_tab" data-toggle="tab" aria-expanded="true">
                        <i class="icon-menu7 position-left"></i> Approval Diskon Manager
                    </a>
                </li>
                <li>
                    <a href="#verifikasi_diskon_direktur_tab" data-toggle="tab" aria-expanded="true">
                        <i class="icon-menu7 position-left"></i> Approval Diskon Direktur
                    </a>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane active" id="verifikasi_pos_manager_tab">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="approval_pos_manager">
                                <thead>
                                    <tr class="bg-slate">
                                        <th class="text-center" style="width: 10%">NO INVOICE</th>
                                        <th class="text-center" style="width: 10%">NO MEMBER</th>
                                        <th class="text-center" style="width: 25%">MEMBER</th>
                                        <!-- <th class="text-center" style="width: 8%">STATUS LUNAS</th> -->
                                        <th class="text-center" style="width: 10%">STATUS</th>
                                        <th class="text-center" style="width: 15%">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr></tr>
                                        <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane" id="verifikasi_diskon_manager_tab">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="approval_diskon_manager">
                                <thead>
                                    <tr class="bg-slate">
                                        <th class="text-center" style="width: 10%">NO INVOICE</th>
                                        <th class="text-center" style="width: 10%">NO MEMBER</th>
                                        <th class="text-center" style="width: 25%">MEMBER</th>
                                        <!-- <th class="text-center" style="width: 8%">STATUS LUNAS</th> -->
                                        <th class="text-center" style="width: 10%">STATUS</th>
                                        <th class="text-center" style="width: 15%">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane" id="verifikasi_diskon_direktur_tab">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="approval_diskon_direktur">
                                <thead>
                                    <tr class="bg-slate">
                                        <th class="text-center" style="width: 10%">NO INVOICE</th>
                                        <th class="text-center" style="width: 10%">NO MEMBER</th>
                                        <th class="text-center" style="width: 25%">MEMBER</th>
                                        <!-- <th class="text-center" style="width: 8%">STATUS LUNAS</th> -->
                                        <th class="text-center" style="width: 10%">STATUS</th>
                                        <th class="text-center" style="width: 15%">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>