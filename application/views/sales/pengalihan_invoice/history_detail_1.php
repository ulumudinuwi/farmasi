<?php echo messages(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-user position-left"></i>Biodata Member
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <table class="table borderless t_custom">
                            <tr>
                                <th>No.Member</th>
                                <td>JK-0005270</td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td>Lili Tatamas</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>Perempuan</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>Villa Artha Gading F/36</td>
                            </tr>
                            <tr>
                                <th>Nomor HP</th>
                                <td>6281381766919</td>
                            </tr>
                            <tr>
                                <th>Point</th>
                                <td>200</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class=" icon-magazine position-left"></i>Data Transaksi
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <table class="table borderless t_custom">
                            <tr>
                                <th>Tanggal</th>
                                <td><input class="form-control" value="<?php echo Carbon\Carbon::now()->format('d/m/Y'); ?>"></td>
                            </tr>
                            <tr>
                                <th>No.Kwitansi</th>
                                <td>KW00839389</td>
                            </tr>
                            <tr>
                                <td>Sales</td>
                                <td>Boby</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-cart position-left"></i>Katalog
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="input-group input-group-sm" style="margin-bottom: 20px;">
                            <span class="input-group-addon"><i class=" icon-search4"></i></span>
                            <input type="text" class="form-control" placeholder="Pencarian">
                        </div>
                        <div class="container-catalog scrollbar-inner">
                            <div class="wrapper-catalog">
                                <div class="item-catalog panel panel-flat">
                                    <div class="panel-body">
                                        <div class="img-container">
                                            <img src="<?php echo  assets_url('img/sample.jpg'); ?>" alt="" class="product-cover"> 
                                        </div>
                                        <h5 class="panel-title">Whitening Pixy</h5>
                                        <p>Rp. <span class="pull-right">50.000,00</span></p>
                                        <div class="input-group product-input" data-stock="20">
                                            <div class="input-group-btn btn-min">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-minus3"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="form-control count-product input-sm" readonly value="0">
                                            <div class="input-group-btn btn-plus">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-plus3"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="btn-cart">
                                            <button class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-cart2"></i></b> Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-catalog panel panel-flat">
                                    <div class="panel-body">
                                        <div class="img-container">
                                            <img src="<?php echo  assets_url('img/sample.jpg'); ?>" alt="" class="product-cover"> 
                                        </div>
                                        <h5 class="panel-title">Whitening Pixy</h5>
                                        <p>Rp. <span class="pull-right">50.000,00</span></p>
                                        <div class="input-group product-input" data-stock="20">
                                            <div class="input-group-btn btn-min">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-minus3"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="form-control count-product input-sm" readonly value="0">
                                            <div class="input-group-btn btn-plus">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-plus3"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="btn-cart">
                                            <button class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-cart2"></i></b> Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-catalog panel panel-flat">
                                    <div class="panel-body">
                                        <div class="img-container">
                                            <img src="<?php echo  assets_url('img/sample.jpg'); ?>" alt="" class="product-cover"> 
                                        </div>
                                        <h5 class="panel-title">Whitening Pixy</h5>
                                        <p>Rp. <span class="pull-right">50.000,00</span></p>
                                        <div class="input-group product-input" data-stock="20">
                                            <div class="input-group-btn btn-min">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-minus3"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="form-control count-product input-sm" readonly value="0">
                                            <div class="input-group-btn btn-plus">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-plus3"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="btn-cart">
                                            <button class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-cart2"></i></b> Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-catalog panel panel-flat">
                                    <div class="panel-body">
                                        <div class="img-container">
                                            <img src="<?php echo  assets_url('img/sample.jpg'); ?>" alt="" class="product-cover"> 
                                        </div>
                                        <h5 class="panel-title">Whitening Pixy</h5>
                                        <p>Rp. <span class="pull-right">50.000,00</span></p>
                                        <div class="input-group product-input" data-stock="20">
                                            <div class="input-group-btn btn-min">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-minus3"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="form-control count-product input-sm" readonly value="0">
                                            <div class="input-group-btn btn-plus">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-plus3"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="btn-cart">
                                            <button class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-cart2"></i></b> Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-catalog panel panel-flat">
                                    <div class="panel-body">
                                        <div class="img-container">
                                            <img src="<?php echo  assets_url('img/sample.jpg'); ?>" alt="" class="product-cover"> 
                                        </div>
                                        <h5 class="panel-title">Whitening Pixy</h5>
                                        <p>Rp. <span class="pull-right">50.000,00</span></p>
                                        <div class="input-group product-input" data-stock="20">
                                            <div class="input-group-btn btn-min">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-minus3"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="form-control count-product input-sm" readonly value="0">
                                            <div class="input-group-btn btn-plus">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-plus3"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="btn-cart">
                                            <button class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-cart2"></i></b> Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-catalog panel panel-flat">
                                    <div class="panel-body">
                                        <div class="img-container">
                                            <img src="<?php echo  assets_url('img/sample.jpg'); ?>" alt="" class="product-cover"> 
                                        </div>
                                        <h5 class="panel-title">Whitening Pixy</h5>
                                        <p>Rp. <span class="pull-right">50.000,00</span></p>
                                        <div class="input-group product-input" data-stock="20">
                                            <div class="input-group-btn btn-min">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-minus3"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="form-control count-product input-sm" readonly value="0">
                                            <div class="input-group-btn btn-plus">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-plus3"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="btn-cart">
                                            <button class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-cart2"></i></b> Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-price-tag position-left"></i>Rincian Biaya
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Uraian</th>
                                        <th>Harga</th>
                                        <th>Qty</th>
                                        <th>(%)</th>
                                        <th>Jumlah</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Whitening Pixy</td>
                                        <td>Rp.50.000,00</td>
                                        <td>4</td>
                                        <td><input type="text" value="0%" class="form-control input-sm text-center"  style="width:60px;"></td>
                                        <td>Rp. 200.000,00</td>
                                        <td><button class="btn btn-danger"><i class="icon-trash"></i></button></td>
                                    </tr>
                                    <?php
                                        foreach([0,0,0,0,] as $i){
                                            $html = '<tr>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td><button class="btn btn-danger"><i class="icon-trash"></i></button></td>'.
                                            '</tr>';

                                            echo $html;
                                        }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4">TOTAL</td>
                                        <td colspan="2"><span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3">Redeem Point</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <!-- <div class="checkbox"> -->
                                            <input type="checkbox" class="control-default" name="use_point">
                                            <!-- </div> -->
                                        </span>
                                        <input type="text" class="form-control" placeholder="point">
                                    </div>
                                    <span class="pull-right text-muted point">Total point 30 (Rp.3000)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Voucher</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default">Redeem</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <div class="col-md-3">
                                    <div class="checkbox checkbox-right uppercase">
                                        <label class="bold">
                                            <input class="control-default" type="checkbox" name="piutang">
                                            Piutang
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3 uppercase bold">Total</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 uppercase bold">Discount</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 uppercase bold">Grand Total</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-credit-card position-left"></i>Metode Pembayaran
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Cara Bayar</th>
                                        <th>Receipt No</th>
                                        <th>Bank</th>
                                        <th>Mesin EDC</th>
                                        <th>Nominal</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select class="form-control select">
                                                <option value="" selected disabled>Pilih Cara Bayar</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <td>
                                            <select class="form-control select">
                                                <option value="" selected disabled>Pilih Bank</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control select">
                                                <option value="" selected disabled>Pilih Mesin EDC</option>
                                            </select>
                                        </td>
                                        <td id="check-form">
                                            
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-success"><i class="icon-check"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tunai</td>
                                        <td>0002</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><span>Rp.10.710.880,00</span></td>
                                        <td class="text-center">
                                            <button class="btn btn-danger"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="uppercase bold">TOTAL</td>
                                        <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                       <td colspan="6">
                                           <button class="btn btn-labeled btn-primary btn-block"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                       </td> 
                                       <!-- <td></td> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3 bold">Bayar Tunai</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 bold">Uang Diterima</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 bold">Kembalian</label>
                                <div class="col-md-8">
                                    <span class="pull-left">Rp.</span><span class="pull-right">0,00</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-8 col-md-offset-3">
                                    <span class="text-muted pull-right point">Total Point yang didapatkan 108</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-3">
                                    <div class="pull-right">
                                        <button class="btn btn-default btn-labeled" style="margin-right:10px;"><b><i class="icon-printer"></i></b> Kwitansi</button>
                                        <button class="btn btn-default btn-labeled"><b><i class="icon-printer"></i></b> Rincian Kasir</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>