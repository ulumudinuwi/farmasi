<div class="panel-body">
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-user-tie"></i> Detail Member   
                    </h6>
                </div>
                <div class="panel-body" style="display: block;">
                    <!-- <pre>{{ member_bio }}</pre> -->
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">No Member</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.no_member}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">Nama Member</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.nama_dokter}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">Email Member</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.email_dokter}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">No.Tlp Member</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.no_hp_dokter}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">Point</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.point_dokter}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">Plafon</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold"> Rp. {{ parseFloat(member_bio.plafon_dokter) | to_rupiah}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">Alamat</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.alamat_dokter }}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">Nama Klinik</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.nama_klinik_dokter}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">No.Tlp Klinik</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.no_tlp_klinik_dokter}}</span>
                        </div>
                    </div>
                    <div class="detail-group">
                        <div class="detail-label">
                            <label for="" class="text-bold">Alamat Klinik</label>
                        </div>
                        <div class="detail-info">
                            <span class="text-semibold">{{ member_bio.alamat_klinik_dokter}}</span>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
        <!-- <div class="col-md-8">
            <div class="panel panel-flat" v-if="member_bio">
                <div class="panel-body">
                    <div class="biodata_header">
                        <img src="http://gudang-farmasi.apk/assets/img/polygon.png" alt="" style="height:28px;">
                        <span class="header_title">Biodata Member</span>
                    </div>
                    <h4 class="biodata_nama">{{ member_bio.nama_dokter }}</h4>
                    <span class="biodata_title">Dokter</span>
                    <hr style="margin: 20px 0px 10px 0px">
                    <div class="info-container">
                        <table class="info-wrapper">
                            <tbody>
                                <tr>
                                    <td class="info-title" style="width: 20%">Alamat</td>
                                    <td></td>
                                    <td class="info-content">{{ member_bio.alamat_dokter }}</td>
                                </tr>
                                <tr>
                                    <td class="info-title" style="width: 20%">Nama Klinik</td>
                                    <td></td>
                                    <td class="info-content">{{ member_bio.nama_klinik_dokter }}</td>
                                </tr>
                                <tr>
                                    <td class="info-title" style="width: 20%">Alamat Klinik</td>
                                    <td></td>
                                    <td class="info-content">{{ member_bio.alamat_klinik_dokter }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-md-5">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-file-text2"></i> Data Transaksi
                    </h6>
                </div>
                <div class="panel-body">
                    <ul class="media-list">
                        <li class="media">
                            <div class="media-left">
                                <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                    <i class="icon-calendar2"></i>
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="media-heading text-semibold text-uppercase bold">Tanggal Transakasi</div>

                                <div class="media-annotation">{{ cart.tgl_transaksi }}</div>
                                <!-- <div class="media-annotation">
                                    <vue-datepicker-local v-model="cart.tgl_transaksi" :local="setupDefault.datepicker.lang"></vue-datepicker-local>
                                </div> -->
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                    <i class="icon-menu6"></i>
                                </a>
                            </div>

                            <div class="media-body">
                                <div class="media-heading text-semibold text-uppercase bold">No. Kuitansi</div>

                                <div class="media-annotation">_____</div>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                    <i class="icon-user-check"></i>
                                </a>
                            </div>

                            <div class="media-body">
                                <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                <div class="media-annotation">{{ member_bio.nama_sales }}</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-truck position-left"></i>
                        Kurir <span class="badge badge-success" v-if="parseFloat(cart.total_harga) >= 30000000" v-cloak>Free Ongkir</span>
                    </h6>
                    <div class="heading-elements">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <input type="checkbox" v-model="pembayaran.is_kurir" @click="setKurir()" class="checkbox-custom">
                                <label class="label label-info">Gunakan Kurir</label>
                            </div>
                        </form>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body" v-if="pembayaran.is_kurir">
                    <form class="form-horizontal">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Ekspedisi</label>
                                <div class="col-md-12">
                                    {{ pembayaran.ekspedisi }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" v-if="parseFloat(cart.total_harga) < 30000000">
                            <div class="form-group">
                                <label for="">Ongkos Kirim </label>
                                <span>{{ pembayaran.ongkir | to_rupiah }}</span>
                            </div>    
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-megaphone position-left"></i>
                        Voucher
                    </h6>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Masukan Kode Voucher">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Gunakan Voucher</button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-box position-left"></i>
                        Detail Transaksi
                        
                    </h6>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead class="align-center">
                                <tr class="bg-slate">
                                    <th style="width: 32%">Nama Barang</th>
                                    <th style="width: 20%">Harga</th>
                                    <th>Qty</th>
                                    <th>(%)</th>
                                    <th style="width: 25%">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(barang, i) of cart.list_barang">
                                    <td>{{barang.nama}}</td>
                                    <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(barang.harga_persatu) | to_rupiah }}</span></td>
                                    <td>{{barang.qty}}</td>
                                    <td>{{barang.disc | to_rupiah}} %</td>
                                    <td><span class="pull-left">Rp.</span><span class="pull-right">{{ total_per_barang(i) | to_rupiah }}</span></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2" class="uppercase bold">Total Harga Barang</td>
                                    <td colspan="2" class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_harga_barang | to_rupiah }}</span></td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2" class="uppercase bold">Diskon</td>
                                    <td colspan="2" class="uppercase bold">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn btn-info" @click="changeTypeDiskon"><i class="icon-gear"></i></button>
                                            </div>
                                            <vue-autonumeric :placeholder="'Diskon'" :options="type_diskon" v-model="pembayaran.disc"></vue-autonumeric>
                                        </div>
                                    </td>
                                </tr>
                                <tr v-if="pembayaran.ongkir != 0">
                                    <td colspan="2"></td>
                                    <td colspan="2" class="uppercase bold">Ongkos Kirim</td>
                                    <td colspan="2" class="uppercase bold">
                                        <span class="pull-left">Rp.</span><span class="pull-right">{{ pembayaran.ongkir | to_rupiah}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2" class="uppercase bold">Grand Total</td>
                                    <td colspan="2" class="uppercase bold">
                                        <span class="pull-left">Rp.</span><span class="pull-right">{{ grand_total | to_rupiah }}</span>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-credit-card position-left"></i>Metode Pembayaran
                    </h6>
                    <div class="heading-elements">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <input type="checkbox" v-model="pembayaran.piutang" @click="setPiutang()" class="checkbox-custom">
                                <label class="label label-info">Piutang</label>
                            </div>
                        </form>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <div class="table table-responsive" v-if="pembayaran.piutang == false">
                        <table class="table table-striped table-bordered">
                            <thead class="align-center">
                                <tr class="bg-slate ">
                                    <th>Cara Bayar</th>
                                    <th>Receipt No</th>
                                    <th>Bank</th>
                                    <th>Mesin EDC</th>
                                    <th>Nominal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <tr v-for="(metode_pembayaran, i) of pembayaran.metode_pembayarans">
                                    <td style="width: 18%">
                                        <span> {{ pembayaran.metode_pembayarans[i].cara_bayar }}</span>
                                    </td>
                                    <td style="width: 18%">
                                        <span v-if="metode_pembayaran.cara_bayar != 'tunai'"> {{ pembayaran.metode_pembayarans[i].receipt_no }} </span>
                                    </td>
                                    <td style="width: 20%">
                                        <span> {{ pembayaran.metode_pembayarans[i].bank_id }} </span>
                                    </td>
                                    <td style="width: 18%">
                                        <span> {{ pembayaran.metode_pembayarans[i].mesin_edc_id }} </span>
                                    </td>
                                    <td style="width: 28%">
                                        <vue-autonumeric :placeholder="'Nominal'" :options="batasPembayaran" v-model="pembayaran.metode_pembayarans[i].nominal"></vue-autonumeric>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-danger" @click="removePembayaran(i)"><i class="icon-trash"></i></button>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td></td>
                                    <td>0002</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td><span>Rp.10.710.880,00</span></td>
                                    <td class="text-center">
                                        <button class="btn btn-danger"><i class="icon-trash"></i></button>
                                    </td>
                                </tr> -->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3"></td>
                                    <td class="uppercase bold">TOTAL</td>
                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah}}</span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <button class="btn btn-labeled btn-primary btn-block" @click="addPembayaran"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                    </td> 
                                <!-- <td></td> -->
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div v-else>
                        PIUTANG AKTIF
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-clipboard"></i> Rincian Pembayaran
                    </h6>
                </div>
                <div class="panel-body">
                    <table class="table table-borderless text-bold">
                        <tr>
                            <td>Total Harga Barang</td>
                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ pembayaran.grand_total | to_rupiah}}</span></td>
                        </tr>
                        <tr v-if="!pembayaran.piutang">
                            <td>Total Pembayaran</td>
                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ pembayaran.total_pembayaran | to_rupiah }}</span></td>
                        </tr>
                        <tr v-if="!pembayaran.piutang">
                            <td>Sisa Pembayaran</td>
                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ sisapembayaran | to_rupiah }}</span></td>
                        </tr>
                        <tr v-if="pembayaran.piutang">
                            <td>Piutang</td>
                            <td><span class="pull-left">Aktif</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <i class="icon-cash4"></i> Pembayaran tunai
                    </h6>
                </div>
                <div class="panel-body">
                    <table class="table table-borderless text-bold">
                        <tr v-if="!pembayaran.piutang">
                            <td>Bayar Tunai</td>
                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ bayar_tunai_only | to_rupiah}}</span></td>
                        </tr>
                        <tr v-if="!pembayaran.piutang">
                            <td>Uang diterima</td>
                            <td><vue-autonumeric :placeholder="'Rupiah'" :options="setupDefault.rupiah" v-model="uang_diterima"></vue-autonumeric></td>
                        </tr>
                        <tr v-if="!pembayaran.piutang">
                            <td>Kembalian</td>
                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ kembalian | to_rupiah}}</span></td>
                        </tr>
                        <tr v-if="pembayaran.piutang">
                            <td>Piutang</td>
                            <td><span class="pull-left">Aktif</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-right text-bold small text-muted">Total Point yg Didapat <span>{{ pointDidapat }}</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-12">
            <pre v-if="pembayaran">{{ pembayaran }}</pre>
            <pre>{{ cart }}</pre>
        </div> -->
    </div>
</div>