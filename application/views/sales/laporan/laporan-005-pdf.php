<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
</style>

<style>
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center">LAPORAN RETUR PENJUALAN</h3>
<h4 class="text-center">TANGGAL TRANSAKSI: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<thead>
		<tr class="bg-slate">
			<th width="20%">NO. INVOICE</th>
			<th width="20%">TANGGAL TRANSAKSI</th>
			<th width="25%">NAMA DOKTER</th>
			<th width="25%">MARKETING</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$grand_total = 0;
			$grand_sisa_pembayaran = 0;
			if($total_rows > 0):
				$no = 1;
				foreach ($rows as $i => $row):
					
		?>
		<tr>
			<td><?php echo $row->no_invoice; ?></td>
			<td><?php echo $row->tanggal_transaksi; ?></td>
			<td><?php echo $row->dokter; ?></td>
			<td><?php echo $row->marketing; ?></td>

		</tr>
		<?php 
			$no++;
			endforeach; 
			else: ?>
		<tr>
			<td style="font-weight: bold;text-align: center;" colspan="4">TIDAK ADA DATA</td>
		</tr>
	<?php endif; ?> 
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Tangerang , <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>