<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Nama Sales</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-sales">
                	<?php if (!is_sales()): ?>
                		<option value="" selected="selected"><?php echo lang('field_all'); ?></option>
                	<?php endif ?>
                    <?php 
                      	foreach($sales as $row) {
                        	echo "<option value='{$row->id}'>{$row->nama}</option>";
                      	}
                    ?>
                </select>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Nama Dokter</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-dokter">
                	<option value="" selected="selected"><?php echo lang('field_all'); ?></option>
                	<?php 
                      	foreach($dokter as $row) {
                        	echo "<option value='{$row->id}'>{$row->nama}</option>";
                      	}
                    ?>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO. INVOICE</th>
				<th>TANGGAL TRANSAKSI</th>
				<th>NAMA DOKTER</th>
				<th>MARKETING</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="5">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>
<?php $this->load->view('sales/laporan/detail-modal-retur'); ?>
<script>
(function () {

	var table;
	var btnRefresh = $("#btn-refresh"),
	    modalDetail = "#detail-modal",
	    tableDetail = $("#table-detail");
	
	var url = {
	  getData: "<?php echo site_url('api/sales/laporan/get_dataRetur'); ?>?id=:id",
	}
	$("select").select2();
	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: '01/MM/YYYY',
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
		"aLengthMenu": [[10, 20, 50, 100, 150,200], [10, 20, 50, 100, 150,200]],
		"ajax": {
			"url": "<?php echo site_url('api/sales/laporan/laporan_005'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.sales_id = $('#search-sales').val();
              	p.member_id = $('#search-dokter').val();
            }
		},
		 "columns": [

		    {
		      "data": "no_invoice",
		      "render": function (data, type, row, meta) {
		        return `<a class="detail-row" data-id="${row.id}">${data}</a>`;
		      }
		    },
	      	{ "data": "tanggal_transaksi" },
	      	{ "data": "dokter" },
	      	{ 
	      		"data": "marketing",
	      		"searchable": false,
	      	},
	    ]
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let sales_id = $('#search-sales').val();
      	let member_id = $('#search-dokter').val();
    	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&sales_id=${sales_id}&member_id=${member_id}`;
		window.location.assign(`<?php echo site_url('api/sales/laporan/print_005'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let sales_id = $('#search-sales').val();
      	let member_id = $('#search-dokter').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&sales_id=${sales_id}&member_id=${member_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/sales/laporan/print_005'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});

	function fillDetail(id) {
	  $(modalDetail).modal('show');
	  blockElement(modalDetail + ' .modal-dialog');
	  $.getJSON(url.getData.replace(":id", id), function(res, status) {
	    if(status === "success") {
	      	let data = res.data;
	      	$('#detail_no_invoice').html(data.no_invoice);
			$('#detail_tanggal').html(data.tanggal_transaksi);
			$('#detail_nama_dokter').html(data.dokter);
			$('#detail_marketing').html(data.marketing);
      		if (data.detail.length > 0) {
      			console.log(data.detail);
                tableDetail.find('tbody').html('');
                for (var i = 0; i < data.detail.length; i++) {
                    addDetail(data.detail[i]);
                }
            }else{
                tableDetail.find('tbody').html('<tr><td colspan="5" align="center">Tidak ada Data</td></tr>');
            }
	    }
	    $(modalDetail + ' .modal-dialog').unblock();

	    $('[data-toggle=tooltip]').tooltip();
	  });
	}

	let addDetail = (obj) => {
        let tbody = tableDetail.find('tbody');

        let tr = $("<tr/>")
            .data('detail_id', obj.id)
            .appendTo(tbody);

        let tdBarang = $("<td/>")
            .appendTo(tr);
            let inputDetailId = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'detail_id[]')
            .val(obj.id)
            .appendTo(tdBarang);
            let labelBarang = $("<div/>")
                .html(obj.barang_old ? obj.barang_old : '<i class="icon-arrow-right7"></i>')
                .addClass('text-center')
                .appendTo(tdBarang)

        let tdQty = $("<td/>")
            .appendTo(tr);
            let labelQty = $("<div/>")
                .html(obj.old_qty > 0 ? obj.old_qty : '<i class="icon-arrow-right7"></i>')
                .addClass('text-center')
                .appendTo(tdQty)

        let valStatus = '';
        if (obj.status == 1) {
        	valStatus = '<i class="icon-arrow-right7" style="color:black;" title="Menjadi Barang"></i>';
        } else if(obj.status == 2){
        	valStatus = '<i class="icon-arrow-right7" style="color:black;" title="Menjadi Barang"></i>';
        } else if(obj.status == 3){
        	valStatus = '<i class="icon-sync" style="color:red;" title="di Retur menjadi Barang"></i>';
        }else{
        	valStatus = '<i class="icon-sync" style="color:blue" title="di Retur"></i>';
        }

        let tdStatus = $("<td/>")
            .appendTo(tr);
            let labelStatus = $("<div/>")
                .html(valStatus)
                .addClass('text-center')
                .appendTo(tdStatus)

        let tdBNewarang = $("<td/>")
            .appendTo(tr);
            let labelNewBarang = $("<div/>")
                .html(obj.barang_new ? obj.barang_new : '<i class="icon-arrow-right7"></i>')
                .addClass('text-center')
                .appendTo(tdBNewarang)

        let tdNewQty = $("<td/>")
            .appendTo(tr);
            let labelNewQty = $("<div/>")
                .html(obj.qty > 0 ? obj.qty : '<i class="icon-arrow-right7"></i>')
                .addClass('text-center')
                .appendTo(tdNewQty)
    }

	$('#table').on("click", ".detail-row", function() {
	    var id = $(this).data("id");
	    fillDetail(id);
	});

})();
</script>