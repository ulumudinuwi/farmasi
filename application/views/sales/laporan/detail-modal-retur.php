<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h6 class="modal-title">Detail Barang</h6>
      </div>
      <div class="modal-body form-horizontal">
        <div class="row mb-20">
          <div class="col-md-12">
            <fieldset>
              <legend class="text-bold"><i class="icon-magazine position-left"></i> Data Barang</legend>
            </fieldset>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-3">Nomor Invoice</label>
                  <div class="col-md-7">
                    <div class="form-control-static" id="detail_no_invoice"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Nama</label>
                  <div class="col-md-7">
                    <div class="form-control-static" id="detail_tanggal"></div>
                  </div>
                </div>
              </div>  
            </div>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-3">Nama Dokter</label>
                  <div class="col-md-7">
                    <div class="form-control-static" id="detail_nama_dokter"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Marketing</label>
                  <div class="col-md-7">
                    <div class="form-control-static" id="detail_marketing"></div>
                  </div>
                </div>
              </div>  
            </div>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
              <table id="table-detail" class="table table-bordered table-striped">
                <thead class="bg-slate">
                  <tr>
                    <th>BARANG</th>
                    <th>QTY</th>
                    <th></th>
                    <th>BARANG BARU</th>
                    <th>QTY</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="text-center" colspan="5">Tidak Ada Data</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer m-t-none">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
    </div>
  </div>
</div>