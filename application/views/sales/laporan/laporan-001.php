<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4">Nama Sales</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-sales">
                	<?php if (!is_sales()): ?>
                		<option value="" selected="selected"><?php echo lang('field_all'); ?></option>
                	<?php endif ?>
                    <?php 
                      	foreach($sales as $row) {
                        	echo "<option value='{$row->id}'>{$row->nama}</option>";
                      	}
                    ?>
                </select>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Nama Dokter</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-dokter">
                	<option value="" selected="selected"><?php echo lang('field_all'); ?></option>
                	<?php 
                      	foreach($dokter as $row) {
                        	echo "<option value='{$row->id}'>{$row->nama}</option>";
                      	}
                    ?>
                </select>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Barang</label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-barang">
                	<option value="" selected="selected"><?php echo lang('field_all'); ?></option>
                	<?php 
                      	foreach($barang as $row) {
                        	echo "<option value='{$row->id}'>{$row->nama}</option>";
                      	}
                    ?>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn-search_tanggal">
                        <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search-tanggal" class="form-control rangetanggal-form input-search">
                </div>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>NO. FPB</th>
				<th>NO. INVOICE</th>
				<th>TANGGAL</th>
				<th>NAMA DOKTER</th>
				<th>MARKETING</th>
				<th>NAMA BARANG</th>
				<th>QTY</th>
				<th>HARGA (Rp.)</th>
				<th>JUMLAH (Rp.)</th>
				<th>DISKON (%)</th>
				<th>TOTAL(Rp.)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="11">Tidak Ada Data</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td class="text-right" colspan="10">Total</td>
				<td class="text-right"></td>
			</tr>
		</tfoot>
	</table>
</div>
<script>
(function () {
	$("select").select2();
	$(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: '01/MM/YYYY',
        endDate: moment(),
    });

	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
		"aLengthMenu": [[10, 20, 50, 100, 150,200], [10, 20, 50, 100, 150,200]],
		"ajax": {
			"url": "<?php echo site_url('api/sales/laporan/laporan_001'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
                p.tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
              	p.sales_id = $('#search-sales').val();
              	p.member_id = $('#search-dokter').val();
              	p.barang_id = $('#search-barang').val();
            }
		},
		 "columns": [
	      	{ "data": "no_fbp" },
	      	{ "data": "no_invoice" },
	      	{ "data": "tanggal_transaksi" },
	      	{ "data": "dokter" },
	      	{ 
	      		"data": "marketing",
	      		"searchable": false,
	      	},
	      	{ 
	      		"data": "barang",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			return data ? data : "&mdash;";
		        }
	      	},
	      	{ 
	      		"data": "qty",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			return numeral(data).format('0.0,');
		        }
	      	},
	      	{ 
	      		"data": "harga",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			let val = '';
	      			if (row.is_bonus == 1) {
	      				val += 'FREE';
	      			}else {
	      				val += numeral(data).format('0.0,');
	      			} 
	      			return val;
		        }
	      	},
	      	{ 
	      		"data": "jumlah",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			let val = '';
	      			if (row.is_bonus == 1) {
	      				val += 'FREE';
	      			}else {
	      				val += numeral(data).format('0.0,');
	      			} 
	      			return val;
		        }
	      	},
	      	{ 
	      		"data": "diskon",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			let val = '';
	      			if (row.is_bonus == 1) {
	      				val += '';
	      			}else {
	      				val += numeral(data).format('0.0,');
	      				val_plus = row.diskon_plus ? ' + '+numeral(row.diskon_plus).format('0.0,') : '';
	      			} 
	      			return val + val_plus;
		        }
	      	},
	      	{ 
	      		"data": "total",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			let val = '';
	      			if (row.is_bonus == 1) {
	      				val += 'FREE';
	      			}else {
	      				val += numeral(data).format('0.0,');
	      			} 
	      			return val;
		        }
	      	},
	    ],
	     "fnDrawCallback": function (oSettings) {
	        var n = oSettings._iRecordsTotal;
	        
	        var total = oSettings.json.total;
	        var total_sd = oSettings.json.total_sd;
	        $("#table").find('tfoot').html(`
	            <tr>
	                <td colspan="8" class="text-right text-bold">TOTAL Sebelum Diskon</td>
	                <td class="text-right text-bold">${numeral(total_sd).format('0.0,')}</td>
	                <td class="text-right text-bold">TOTAL</td>
	                <td class="text-right text-bold">${numeral(total).format('0.0,')}</td>
	            </tr>
	        `);
	    }
	});

    $("#search-tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $(".input-search").on('change', function() {
      table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let sales_id = $('#search-sales').val();
      	let member_id = $('#search-dokter').val();
      	let barang_id = $('#search-barang').val();
    	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&sales_id=${sales_id}&member_id=${member_id}&barang_id=${barang_id}`;
		window.location.assign(`<?php echo site_url('api/sales/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let tanggal_dari = subsDate($("#search-tanggal").val(), 'dari');
        let tanggal_sampai = subsDate($("#search-tanggal").val(), 'sampai');
      	let sales_id = $('#search-sales').val();
      	let member_id = $('#search-dokter').val();
      	let barang_id = $('#search-barang').val();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}&sales_id=${sales_id}&member_id=${member_id}&barang_id=${barang_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/sales/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>