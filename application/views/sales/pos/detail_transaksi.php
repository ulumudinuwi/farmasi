<div id="detail_transaksi" class="content content-custom">
    <div class="col-md-12">
        <div class="panel panel-white">
            <!-- <div class="panel-heading">
                <div class="text-right">
                    <a :href="invoice_url" type="button" class="btn btn-default btn-labeled" target="_blank">
                        <b><i class="icon-printer"></i></b>
                        Invoice
                    </a>
                    <button type="button" class="btn btn-info btn-labeled" v-if="!disabled_button">
                        <b><i class="fa fa-refresh"></i></b>
                        Return
                    </button>
                    <button type="button" class="btn btn-danger btn-labeled" v-if="!disabled_button">
                        <b><i class="icon-close2"></i></b>
                        Cancel
                    </button>
                </div>
            </div> -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-user-tie"></i> Detail Member   
                                </h6>
                            </div>
                            <div class="panel-body" style="display: block;" v-if="detail_member.member">
                                <!-- <pre>{{ member_bio }}</pre> -->
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_member}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.nama}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Email Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.email}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_hp}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Point</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.point}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Plafon</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold"> Rp. {{ parseFloat(detail_member.member.plafon) | to_rupiah}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.alamat }}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.nama_klinik}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_tlp_klinik}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.alamat_klinik}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-file-text2"></i> Data Transaksi
                                </h6>
                            </div>
                            <div class="panel-body">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-calendar2"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal Transakasi</div>

                                            <div class="media-annotation">{{ detail_transaksi.tanggal_transaksi | d_m_Y_H_m_s }}</div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-warning text-warning btn-flat btn-icon btn-rounded btn-sm change-jatuh-tempo">
                                                <i class="icon-calendar2"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal Jatuh Tempo</div>

                                            <!-- <div class="media-annotation">{{ detail_transaksi.tgl_jatuh_tempo | d_m_Y }}</div> -->
                                            <input type="date" name="tgl_jatuh_tempo" id="tgl_jatuh_tempo" class="form-control" :value="detail_transaksi.tgl_jatuh_tempo">
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-user-check"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                            <div class="media-annotation" v-if="detail_member.sales">{{ detail_member.sales.nama }}</div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-left">
                                            <a :class="`btn btn-flat btn-icon btn-rounded btn-sm ${detail_transaksi.status_lunas ? 'border-success text-success' : 'border-danger text-danger'}`">
                                                <i class="fa fa-check" v-if="detail_transaksi.status_lunas"></i>
                                                <i class="icon-close2" v-if="!detail_transaksi.status_lunas"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Status Lunas</div>

                                            <div class="media-annotation">{{ detail_transaksi.status_lunas ? 'LUNAS' : 'BELUM LUNAS' }}</div>
                                        </div>

                                        <div class="media-body">
                                            <!-- <div class="media-heading text-semibold text-uppercase bold">Cicilan Ke</div> -->

                                            <div class="media-annotation"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- 
                    <div class="col-md-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-truck position-left"></i>
                                    Kurir <span class="badge badge-success" v-if="parseFloat(detail_transaksi.total_harga) >= 30000000" v-cloak>Free Ongkir</span>
                                </h6>
                                <div class="heading-elements">
                                    <form action="#" class="form-inline">
                                        <label class="label label-info" v-if="detail_transaksi.is_kurir">Menggunakan Kurir</label>
                                        <label class="label label-danger" v-if="!detail_transaksi.is_kurir">Tidak Menggunakan Kurir</label>
                                    </form>
                                </div>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>
                            <div class="panel-body" v-if="detail_transaksi.is_kurir">
                                <form class="form-horizontal">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-12">Ekspedisi</label>
                                            <div class="col-md-12">
                                                {{detail_transaksi.ekspedisi}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" v-if="parseFloat(detail_transaksi.total_harga) < 30000000">
                                        <div class="form-group">
                                            <label for="">Ongkos Kirim </label>
                                            <div class="col-md-12">
                                                <span>Rp. {{ parseFloat(detail_transaksi.ongkir) | to_rupiah }}</span>
                                            </div>
                                        </div>    
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-6 belum_lunas">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-truck position-left"></i>
                                    Kurir <span class="badge badge-success" v-if="parseFloat(grand_total) >= 3000000" v-cloak>Free Ongkir</span>
                                </h6>
                                <div class="heading-elements">
                                    <form action="#" class="form-inline">
                                        <div class="form-group" v-if="parseFloat(grand_total) <= 3000000" v-cloak>
                                            <input type="checkbox" v-model="pembayaran.is_kurir" @click="setKurir()" class="checkbox-custom">
                                            <label class="label label-info">Gunakan Kurir</label>
                                        </div>
                                    </form>
                                </div>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>
                            <div class="panel-body" v-if="pembayaran.is_kurir">
                                <form class="form-horizontal">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-12">Ekspedisi</label>
                                            <div class="col-md-12">
                                                <select2 :classname="'form-control select2-cs'" :options="master_ekspedisi" v-model="pembayaran.ekspedisi" :lengthselected="0">
                                                    <option disabled value="0">Pilih ekspedisi</option>
                                                </select2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" v-if="parseFloat(grand_total) < 30000000">
                                        <div class="form-group">
                                            <label for="">Ongkos Kirim </label>
                                            <vue-autonumeric :placeholder="'Nominal'" :options="batas_ongkir" v-model="pembayaran.ongkir"></vue-autonumeric>
                                        </div>    
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 lunas">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-truck position-left"></i>
                                    Kurir <span class="badge badge-success" v-if="parseFloat(detail_transaksi.total_harga) >= 3000000" v-cloak>Free Ongkir</span>
                                </h6>
                                <div class="heading-elements">
                                    <form action="#" class="form-inline">
                                        <div class="form-group">
                                            <label class="label label-info"  v-if="parseFloat(grand_total) <= 3000000" v-cloak>Gunakan Kurir</label>
                                        </div>
                                    </form>
                                </div>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>
                            <div class="panel-body" v-if="detail_transaksi.is_kurir">
                                <form class="form-horizontal">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-12">Ekspedisi</label>
                                            <div class="col-md-12">
                                                <select2 :classname="'form-control select2-cs'" :options="master_ekspedisi" v-model="detail_transaksi.ekspedisi" :lengthselected="0" disabled="disabled">
                                                    <option disabled value="0">Pilih ekspedisi</option>
                                                </select2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" v-if="parseFloat(grand_total) < 30000000">
                                        <div class="form-group">
                                            <label for="">Ongkos Kirim </label>
                                            <vue-autonumeric :placeholder="'Nominal'" :options="batas_ongkir" v-model="detail_transaksi.ongkir" disabled="disabled"></vue-autonumeric>
                                        </div>    
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-megaphone position-left"></i>
                                    Voucher
                                </h6>
                            </div>
                            <div class="panel-body">
                                <!-- <form class="form-horizontal">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Masukan Kode Voucher">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Gunakan Voucher</button>
                                            </span>
                                        </div>
                                    </div>
                                </form> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-box position-left"></i>
                                    Detail Transaksi
                                </h6>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead class="align-center">
                                            <tr class="bg-slate">
                                                <th style="width: 32%">Nama Barang</th>
                                                <th style="width: 20%">Harga</th>
                                                <th>Qty</th>
                                                <th>(%)</th>
                                                <th style="width: 25%">Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(barang, i) of detail_pembelian">
                                                <td>{{barang.nama}}</td>
                                                <td>
                                                    <span class="pull-left" v-if="barang.is_bonus == 0">Rp.</span>
                                                    <span class="pull-right" v-if="barang.is_bonus == 0">
                                                        {{ parseFloat(barang.harga) | to_rupiah }}
                                                    </span>
                                                    <span class="pull-left text-size-mini text-danger" v-if="barang.is_bonus == 1">
                                                        Bonus dari Barang {{ JSON.parse(barang.is_bonus_detail).nama }}
                                                    </span>
                                                </td>
                                                <td>{{barang.qty}}</td>
                                                <td>{{barang.diskon | to_rupiah}} % + {{barang.diskon_plus | to_rupiah}} %</td>
                                                <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(barang.grand_total) | to_rupiah }}</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Total Harga Barang</td>
                                                <td colspan="2" class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_harga_barang | to_rupiah }}</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Diskon</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">{{ detail_transaksi.jenis_diskon != 'persen' ? 'Rp.' : ''}} </span><span class="pull-right">{{ detail_transaksi.diskon }} {{ detail_transaksi.jenis_diskon == 'persen' ? '%' : ''}}</span>
                                                </td>
                                            </tr>
                                            <tr v-if="detail_transaksi.ongkir != 0">
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Ongkos Kirim</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.ongkir) | to_rupiah}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Grand Total</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ grand_total | to_rupiah }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" class="uppercase bold">
                                                    <span class="pull-right text-size-mini text-danger">
                                                        Belum Termasuk PPN
                                                    </span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="belum_lunas">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-credit-card position-left"></i>Metode Pembayaran
                                    </h6>
                                    <div class="heading-elements" v-if="!pembayaran.is_free">
                                        <form action="#" class="form-inline">
                                            <div class="form-group">
                                                <input type="checkbox" v-model="pembayaran.piutang" @click="setPiutang()" class="checkbox-custom">
                                                <label class="label label-info">Piutang</label>
                                            </div>
                                        </form>
                                    </div>
                                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                                </div>
                                <div class="panel-body">
                                    <div class="table table-responsive" v-if="pembayaran.piutang == false && pembayaran.is_free == false">
                                        <table class="table table-striped table-bordered">
                                            <thead class="align-center">
                                                <tr class="bg-slate ">
                                                    <th>Cara Bayar</th>
                                                    <th>Receipt No</th>
                                                    <th>Bank</th>
                                                    <th>Mesin EDC</th>
                                                    <th>Nominal</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                <tr v-for="(metode_pembayaran, i) of pembayaran.metode_pembayarans">
                                                    <td style="width: 18%">
                                                        <select2 :classname="'form-control select2-cs'" :options="master_cara_bayar" v-model="pembayaran.metode_pembayarans[i].cara_bayar">
                                                            <option disabled value="0">Pilih cara bayar</option>
                                                        </select2>
                                                        <!-- <select2 :classname="'form-control select2-cs'" v-model="pembayaran.metode_pembayarans[i].cara_bayar" v-else>
                                                            <option value="tunai" selected>Tunai</option>
                                                        </select2> -->
                                                    </td>
                                                    <td style="width: 18%">
                                                        <input type="text" class="form-control" v-if="metode_pembayaran.cara_bayar != 'tunai'" v-model="pembayaran.metode_pembayarans[i].receipt_no">
                                                    </td>
                                                    <td style="width: 20%">
                                                        <select2 :classname="'form-control select2-cs'" :options="master_bank" v-model="pembayaran.metode_pembayarans[i].bank_id" v-if="master_bank.length > 0 && metode_pembayaran.cara_bayar != 'tunai'">
                                                            <option disabled value="0">Pilih bank</option>
                                                        </select2>
                                                        <!-- <select class="form-control" v-model="pembayaran.metode_pembayarans[i].bank_id">
                                                            <option value="">Pilih Bank</option>
                                                            <option v-for="bank of master_bank" v-bind:value="bank.id">{{ bank.nama }}</option>
                                                        </select> -->
                                                    </td>
                                                    <td style="width: 18%">
                                                        <select2 :classname="'form-control select2-cs'" :options="master_edc" v-model="pembayaran.metode_pembayarans[i].mesin_edc_id" v-if="master_edc.length > 0 && metode_pembayaran.cara_bayar != 'tunai'">
                                                            <option disabled value="0">Pilih mesin EDC</option>
                                                        </select2>
                                                        <!-- <select class="form-control" v-model="pembayaran.metode_pembayarans[i].mesin_edc_id">
                                                            <option value="">Pilih Mesin Edc</option>
                                                            <option v-for="edc of master_edc" v-bind:value="edc.id">{{ edc.nama }}</option>
                                                        </select> -->
                                                    </td>
                                                    <td style="width: 28%">
                                                        <vue-autonumeric :placeholder="'Nominal'" :options="batas_pembayaran[i]" v-model="pembayaran.metode_pembayarans[i].nominal"></vue-autonumeric>
                                                    </td>
                                                    <td class="text-center">
                                                        <button class="btn btn-danger" @click="removePembayaran(i)"><i class="icon-trash"></i></button>
                                                    </td>
                                                </tr>
                                                <!-- <tr>
                                                    <td></td>
                                                    <td>0002</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td><span>Rp.10.710.880,00</span></td>
                                                    <td class="text-center">
                                                        <button class="btn btn-danger"><i class="icon-trash"></i></button>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td class="uppercase bold">TOTAL</td>
                                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah}}</span></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6">
                                                        <button class="btn btn-labeled btn-primary btn-block" @click="addPembayaran"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                                    </td> 
                                                <!-- <td></td> -->
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div v-if="pembayaran.is_free == true">
                                        GRATIS 
                                    </div>
                                    <div v-if="pembayaran.piutang == true">
                                        PIUTANG AKTIF
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default" v-if="!pembayaran.is_free">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-clipboard"></i> Rincian Pembayaran
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-borderless text-bold">
                                        <tr>
                                            <td>Total Harga</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ grand_total | to_rupiah}}</span></td>
                                        </tr>
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Total Pembayaran</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah }}</span></td>
                                        </tr>
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Sisa Pembayaran</td>
                                            <td>
                                                <span v-if="sisapembayaran >= 0">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ sisapembayaran | to_rupiah }}</span>
                                                </span>
                                                <span v-else>
                                                    <span class="pull-left">Rp.</span><span class="pull-right text-danger">( {{ sisapembayaran | to_rupiah }} )</span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr v-if="pembayaran.piutang">
                                            <td>Piutang</td>
                                            <td><span class="pull-left">Aktif</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-white" v-if="!pembayaran.is_free">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-cash4"></i> Pembayaran tunai
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-borderless text-bold">
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Bayar Tunai</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ bayar_tunai_only | to_rupiah}}</span></td>
                                        </tr>
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Uang diterima</td>
                                            <td><vue-autonumeric :placeholder="'Rupiah'" :options="setupDefault.rupiah" v-model="uang_diterima"></vue-autonumeric></td>
                                        </tr>
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Kembalian</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ kembalian | to_rupiah}}</span></td>
                                        </tr>
                                        <tr v-if="pembayaran.piutang">
                                            <td>Piutang</td>
                                            <td><span class="pull-left">Aktif</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="lunas">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-credit-card position-left"></i>Detail Pembayaran
                                    </h6>
                                    <div class="heading-elements">
                                        <label class="label label-info" v-if="detail_transaksi.piutang">Piutang</label>
                                        <a v-if="!detail_transaksi.piutang && !detail_transaksi.is_cicilan" :href="kuitansi_proc(total_pembayaran_cicilan,getKetTotal(),no_kuitansi)" type="button" class="btn btn-default btn-labeled" target="_blank"><b><i class="icon-printer"></i></b>Kwitansi</a>
                                    </div>
                                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                                </div>
                                <div class="panel-body">
                                    <div class="panel-group panel-group-control panel-group-control-right content-group-lg" v-if="detail_transaksi.is_cicilan">
                                        <div class="panel panel-white" v-for="(cicilan, i) of detail_cicilan">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a data-toggle="collapse" :href="'#'+i" aria-expanded="false" class="collapsed">{{ cicilanKe(i) }}</a>
                                                </h6>
                                            </div>
                                            <div :id="i" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <div class="table table-responsive">
                                                        <table class="table table-striped table-bordered">
                                                            <thead class="align-center">
                                                                <tr class="bg-slate ">
                                                                    <th>Cara Bayar</th>
                                                                    <th>Receipt No</th>
                                                                    <th>Tanggal</th>
                                                                    <th>Bank</th>
                                                                    <th>Mesin EDC</th>
                                                                    <th>Nominal</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <!-- <pre>{{ pembayaran.metode_pembayarans }}</pre> -->
                                                                <tr v-for="(metode_pembayaran) of cicilan">
                                                                    
                                                                    <td style="width: 18%">
                                                                        {{ find_cara_bayar(metode_pembayaran.jenis_pembayaran).text }}
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        {{ metode_pembayaran.no_receipt ? metode_pembayaran.no_receipt : '-' }}
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        {{ metode_pembayaran.created_at ? metode_pembayaran.created_at : '-' }}
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        {{ metode_pembayaran.bank_id != 0 ? find_bank(metode_pembayaran.bank_id) : '-' }}
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        {{ metode_pembayaran.mesin_edc_id != 0 ? find_edc(metode_pembayaran.mesin_edc_id) : '-' }}
                                                                    </td>
                                                                    <td style="width: 28%">
                                                                        <div v-if="metode_pembayaran.nominal_pembayaran">
                                                                            <span class="pull-left">Rp.</span><span class="pull-right">{{  parseFloat(metode_pembayaran.nominal_pembayaran) | to_rupiah}}</span>
                                                                        </div>
                                                                        <div v-else>
                                                                            <span>-</span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                            <!-- <pre>{{i}}</pre> -->
                                                                <tr>
                                                                    <td colspan="4"></td>
                                                                    <td class="uppercase bold">TOTAL</td>
                                                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran_per_cicilan(i) | to_rupiah}}</span></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table table-responsive" v-if="!detail_transaksi.piutang && detail_transaksi.is_cicilan">
                                        <table class="table table-striped table-bordered">
                                            <thead class="align-center">
                                                <tr class="bg-slate ">
                                                    <th>Cicilan Ke</th>
                                                    <th>No Kwitansi</th>
                                                    <th>Tanggal</th>
                                                    <th>Nominal</th>
                                                    <th style="width: 10%;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(cicilan, i) of detail_cicilan">
                                                    <td style="text-transform: capitalize">{{ text_cicilan(i) }}</td>     
                                                    <td>{{ no_kuitansi[i].no_kuitansi }}</td>
                                                    <td>{{ detail_cicilan[i][0].created_at }}</td>
                                                    <td>
                                                        <span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran_per_cicilan(i) | to_rupiah}}</span>
                                                    </td>
                                                    <td>
                                                        <a :href="kuitansi_proc(total_pembayaran_per_cicilan(i),getKetCicilan(i),no_kuitansi[i].no_kuitansi, detail_cicilan[i][0].sisa_pembayaran)" type="button" class="btn btn-default btn-labeled" target="_blank"><b><i class="icon-printer"></i></b>Kwitansi</a>
                                                    </td>     
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td class="uppercase bold">TOTAL</td>
                                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran_cicilan | to_rupiah}}</span></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="table table-responsive" v-if="!detail_transaksi.piutang && !detail_transaksi.is_cicilan">
                                        <table class="table table-striped table-bordered">
                                            <thead class="align-center">
                                                <tr class="bg-slate ">
                                                    <th>Cara Bayar</th>
                                                    <th>Receipt No</th>
                                                    <th>Tanggal</th>
                                                    <th>Bank</th>
                                                    <th>Mesin EDC</th>
                                                    <th>Nominal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                <!-- <pre>{{ pembayaran.metode_pembayarans }}</pre> -->
                                                <tr v-for="(metode_pembayaran, i) of detail_pembayaran">
                                                    <td style="width: 18%">
                                                        {{ find_cara_bayar(metode_pembayaran.jenis_pembayaran).text }}
                                                    </td>
                                                    <td style="width: 18%">
                                                        {{ metode_pembayaran.no_receipt ? metode_pembayaran.no_receipt : '-' }}
                                                    </td>
                                                    <td style="width: 18%">
                                                        {{ metode_pembayaran.created_at ? metode_pembayaran.created_at : '-' }}
                                                    </td>
                                                    <td style="width: 20%">
                                                        {{ metode_pembayaran.bank_id != 0 ? find_bank(metode_pembayaran.bank_id) : '-' }}
                                                    </td>
                                                    <td style="width: 18%">
                                                        {{ metode_pembayaran.mesin_edc_id != 0 ? find_edc(metode_pembayaran.mesin_edc_id) : '-' }}
                                                    </td>
                                                    <td style="width: 28%">
                                                        <div v-if="metode_pembayaran.nominal_pembayaran">
                                                            <span class="pull-left">Rp.</span><span class="pull-right">{{  parseFloat(metode_pembayaran.nominal_pembayaran) | to_rupiah}}</span>
                                                        </div>
                                                        <div v-else>
                                                            <span>-</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="4"></td>
                                                    <td class="uppercase bold">TOTAL</td>
                                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran_cicilan | to_rupiah}}</span></td>
                                                 
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div v-if="detail_transaksi.piutang">
                                        BELUM ADA TRANSAKSI PEMBAYARAN
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-clipboard"></i> Rincian Pembayaran
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-borderless text-bold">
                                        <tr>
                                            <td>Total Harga</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.grand_total) | to_rupiah}}</span></td>
                                        </tr>
                                        <tr v-if="!detail_transaksi.piutang">
                                            <td>Total Pembayaran</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(total_pembayaran_cicilan) | to_rupiah }}</span></td>
                                        </tr>
                                        <tr v-if="!detail_transaksi.piutang">
                                            <td>Sisa Pembayaran</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.sisa_pembayaran) | to_rupiah }}</span></td>
                                        </tr>
                                        <tr v-if="detail_transaksi.piutang">
                                            <!-- <td>Piutang</td>
                                            <td><span class="pull-left">Aktif</td> -->
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-cash4"></i> Pembayaran tunai
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-borderless text-bold">
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Bayar Tunai</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ bayar_tunai_only | to_rupiah}}</span></td>
                                        </tr>
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Uang diterima</td>
                                            <td><vue-autonumeric :placeholder="'Rupiah'" :options="setupDefault.rupiah" v-model="uang_diterima"></vue-autonumeric></td>
                                        </tr>
                                        <tr v-if="!pembayaran.piutang">
                                            <td>Kembalian</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ kembalian | to_rupiah}}</span></td>
                                        </tr>
                                        <tr v-if="pembayaran.piutang">
                                            <td>Piutang</td>
                                            <td><span class="pull-left">Aktif</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="text-right text-bold small text-muted">Total Point yg Didapat <span>{{ pointDidapat }}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-md-12">
                            <pre v-if="pembayaran">{{ pembayaran }}</pre>
                            <pre>{{ cart }}</pre>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="text-right belum_lunas">
                    <button type="submit" class="btn btn-success btn-labeled" @click="onProcess()">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                </div>
                <div class="text-right lunas">
                    <a :href="invoice_url" type="button" class="btn btn-default btn-labeled" target="_blank">
                        <b><i class="icon-printer"></i></b>
                        Invoice
                    </a>
                    <a :href="tanda_terima_url" type="button" class="btn btn-default btn-labeled" target="_blank">
                        <b><i class="icon-printer"></i></b>
                        Tanda Terima
                    </a><!-- 
                    <button type="button" class="btn btn-info btn-labeled" v-if="!disabled_button">
                        <b><i class="fa fa-refresh"></i></b>
                        Return
                    </button>
                    <button type="button" class="btn btn-danger btn-labeled" v-if="!disabled_button">
                        <b><i class="fa fa-close"></i></b>
                        Cancel
                    </button> -->
                </div>
            </div>
        </div>
    </div>
</div>