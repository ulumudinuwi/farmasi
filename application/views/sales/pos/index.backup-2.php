<?php echo messages(); ?>
<div class="row" id="index_pos">
    <div class="col-md-7 col-cs" style="height: inherit;" v-if="page == 1">
        <div class="panel panel-cs" style="height: inherit;">
            <div class="panel-heading cs-heading">
                <h4 class="panel-title">
                    <i class="icon-dropbox"></i> Barang
                </h4>
                <div class="heading-elements">
                    <div class="search-box">
                        <input type="text" class="search-input" placeholder="Cari Barang">
                        <i class="icon-search4"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body cs-body barang-container">
            <div class="barang-wrapper scrollbar-inner">
                <div class="list-wrapper">
                    <div class="item-barang" v-for="(barang, i) of barang.list_barang" @click="addTocart(i)" v-bind:class="barang.active != undefined && barang.active ? 'active' : ''">
                        <div class="item-thumb">
                            <img v-bind:src="barang.thumb" alt="">
                        </div>
                        <div class="item-body">
                            <span class="nama_barang">{{ barang.nama }}</span>
                            <span class="harga_barang">Rp.{{ barang.harga | to_rupiah}}</span>
                        </div>
                        <div class="stock_barang">QTY : {{ barang.stock | bil_bulat}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 col-cs flex-cart" style="height: inherit;" v-if="page == 1">
        <div class="panel panel-cs panel-cart" style="height: inherit;">
            <div class="panel-heading cs-heading cart-heading">
                <h4 class="panel-title">
                    <i class="icon-cart2"></i> Cart
                </h4>
                <div class="heading-elements">
                    <div class="search-box">
                        <autocomplete v-bind:search="person_search" @setresult="getSearch($event)" :items="list_person" place-holder="Cari Member"/>
                        <!-- <input type="text" class="search-input" placeholder="Cari Member"> -->
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="panel-body cs-body">
                <!-- Bagian member -->
                <div class="member-wrapper">
                    <div class="member-label">
                        <img class="member-label-icon" v-bind:src="assetsUrl+'img/icon/dokter.png'" alt="">
                        <span class="member-label-title">{{ selected_person.nama_member}}</span>
                    </div>
                    <div class="member-label">
                        <img class="member-label-icon" v-bind:src="assetsUrl+'img/icon/sales.png'" alt="">
                        <span class="member-label-title">{{ selected_person.nama_sales }}</span>
                    </div>
                </div>
                <!-- Bagian order -->
                <div class="order-wrapper">
                    <div class="order-heading">
                        <span class="__title">Daftar Order</span>
                        <span class="__tanggal">{{ tgl_transaksi_label }}</span>
                    </div>
                    <div class="line" style="width: 100%;"></div>
                    <div class="order-list scrollbar-inner">
                        <div class="item-order" v-for="(barang, i) of cart.list_barang">
                            <div class="__left">
                                <div class="__first">
                                    <span class="__title">{{ barang.nama }}</span>
                                </div>
                                <div class="__second">
                                    <span class="__discount">
                                        <img class="__discount_image" v-bind:src="assetsUrl+'img/icon/discount.png'" alt="">
                                        <span class="__discount_title">Disc</span>
                                    </span>
                                    <span class="__price">
                                        <img class="__price_image" v-bind:src="assetsUrl+'img/icon/price.png'" alt="">
                                        <span class="__price_title">Rp. {{total_per_barang(i) | to_rupiah}}</span>
                                    </span>
                                </div>
                            </div>
                            <div class="__right">
                                <div class="__qty_group">
                                    <button class="__qty-action" @click="qtyDecrease(i)"><i class="icon-minus2"></i></button>
                                    <input type="text" class="__qty_item" readonly v-model="cart.list_barang[i].qty">
                                    <button class="__qty-action" @click="qtyIncrease(i)"><i class="icon-plus2"></i></button>
                                </div>
                                <button class="btn btn-danger clear-btn" @click="removeCart(i)"><i class="icon-trash"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Bagian total -->
                <div class="total-wrapper">
                    <div  class="__first_column">
                        <div class="label_total">TOTAL</div>
                        <div class="price_total">Rp. {{total_belanja | to_rupiah}}</div>
                    </div>
                    <div  class="__second_column">
                        <button class="btn btn-default" @click="saveToDraf"><i class="icon-floppy-disk"></i></button>
                        <button class="btn btn-primary btn-block" @click="nextStep">BAYAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" v-bind:class="page == 1 ? 'hide-class': ''">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="biodata_header">
                                    <img src="http://gudang-farmasi.apk/assets/img/polygon.png" alt="" style="height:28px;">
                                    <span class="header_title">Biodata Member</span>
                                </div>
                                <h4 class="biodata_nama">{{ member_bio.nama_dokter }}</h4>
                                <span class="biodata_title">Dokter</span>
                                <hr style="margin: 20px 0px 10px 0px">
                                <div class="info-container">
                                    <table class="info-wrapper">
                                        <tbody>
                                            <tr>
                                                <td class="info-title" style="width: 20%">Alamat</td>
                                                <td></td>
                                                <td class="info-content">{{ member_bio.alamat_dokter }}</td>
                                            </tr>
                                            <tr>
                                                <td class="info-title" style="width: 20%">Nama Klinik</td>
                                                <td></td>
                                                <td class="info-content">{{ member_bio.nama_klinik_dokter }}</td>
                                            </tr>
                                            <tr>
                                                <td class="info-title" style="width: 20%">Alamat Klinik</td>
                                                <td></td>
                                                <td class="info-content">{{ member_bio.alamat_klinik_dokter }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel-heading">
                            <span class="title-heading">
                                <i class="icon-file-text2"></i> Data Transaksi
                            </span>
                        </div>
                        <div class="panel-body">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                            <i class="icon-calendar2"></i>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <div class="media-heading text-semibold text-uppercase bold">Tanggal Transakasi</div>

                                        <div class="media-annotation">{{ cart.tgl_transaksi }}</div>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                            <i class="icon-menu6"></i>
                                        </a>
                                    </div>

                                    <div class="media-body">
                                        <div class="media-heading text-semibold text-uppercase bold">No. Kwitansi</div>

                                        <div class="media-annotation">_____</div>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                            <i class="icon-user-check"></i>
                                        </a>
                                    </div>

                                    <div class="media-body">
                                        <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                        <div class="media-annotation">{{ member_bio.nama_sales }}</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default no-border">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-box position-left"></i>
                                    DETAIL TRANSAKSI
                                </h6>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead class="bg-slate align-center">
                                            <tr>
                                                <th>Nama Barang</th>
                                                <th>Harga</th>
                                                <th>Qty</th>
                                                <th>(%)</th>
                                                <th>Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(barang, i) of cart.list_barang">
                                                <td>{{barang.nama}}</td>
                                                <td><span class="pull-left">Rp.</span><span class="pull-right">{{ barang.harga_persatu | to_rupiah}}</span></td>
                                                <td>{{barang.qty}}</td>
                                                <td>0%</td>
                                                <td><span class="pull-left">Rp.</span><span class="pull-right">{{total_per_barang(i) | to_rupiah}}</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">TOTAL</td>
                                                <td colspan="2"><span class="pull-left">Rp.</span><span class="pull-right">{{total_belanja | to_rupiah}}</span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default no-border">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-credit-card position-left"></i>Metode Pembayaran
                                </h6>
                            </div>
                            <div class="panel-body">
                                <div class="table table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead class="bg-slate align-center">
                                            <tr>
                                                <th>Cara Bayar</th>
                                                <th>Receipt No</th>
                                                <th>Bank</th>
                                                <th>Mesin EDC</th>
                                                <th>Nominal</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(metode_pembayaran, i) of metode_pembayarans">
                                                <td>
                                                    <select class="form-control" v-model="metode_pembayarans[i].cara_bayar">
                                                        <option value="">Pilih Cara Bayar</option>
                                                        <option value="kartu_kredit">KARTU KREDIT</option>
                                                        <option value="kartu_debit">KARTU DEBIT</option>
                                                        <option value="transfer">TRANSFER</option>
                                                        <option value="tunai">tunai</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control">
                                                </td>
                                                <td>
                                                    <select class="form-control" v-model="metode_pembayarans[i].bank_id">
                                                        <option value="">Pilih Bank</option>
                                                        <option v-for="bank of master_bank" v-bind:value="bank.id">{{ bank.nama }}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control" v-model="metode_pembayarans[i].mesin_edc_id">
                                                        <option value="">Pilih Mesin Edc</option>
                                                        <option v-for="edc of master_edc" v-bind:value="edc.id">{{ edc.nama }}</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" v-model="metode_pembayarans[i].nominal">   
                                                </td>
                                                <td class="text-center">
                                                    <button class="btn btn-danger" @click="removePembayaran(i)"><i class="icon-trash"></i></button>
                                                </td>
                                            </tr>
                                            <!-- <tr>
                                                <td></td>
                                                <td>0002</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td><span>Rp.10.710.880,00</span></td>
                                                <td class="text-center">
                                                    <button class="btn btn-danger"><i class="icon-trash"></i></button>
                                                </td>
                                            </tr> -->
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3"></td>
                                                <td class="uppercase bold">TOTAL</td>
                                                <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah}}</span></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                            <td colspan="6">
                                                <button class="btn btn-labeled btn-primary btn-block" @click="addPembayaran"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                            </td> 
                                            <!-- <td></td> -->
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                SISA PEMBAYARAN : Rp. {{sisa_pembayaran | to_rupiah}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success btn-labeled">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                    <button @click="backStep()" class="btn btn-default cancel-button">
                        Batal
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/x-template" id="autocomplete">
  <div class="autocomplete">
    <input type="text" @input="onChange" v-model="search" @keyup.down="onArrowDown" @keyup.up="onArrowUp" @keyup.enter="onEnter" class="search-input" v-bind:placeholder="placeHolder">
    <i class="icon-search4"></i>
    <ul id="autocomplete-results" v-show="isOpen" class="autocomplete-results">
      <li class="loading" v-if="isLoading">
        Loading results...
      </li>
      <li v-else v-for="(result, i) in results" :key="i" @click="setResult(result)" class="autocomplete-result" :class="{ 'is-active': i === arrowCounter }">
        {{ result.nama_dokter.toUpperCase() }}
      </li>
    </ul>
  </div>
</script>

