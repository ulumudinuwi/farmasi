<!-- Content area -->
<div class="content content-custom" id="list_transaksi">
    <!-- Row Filter -->
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Filter Tanggal         
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                       <vue-datepicker-local v-model="filter_range" :local="setupDefault.datepicker.lang" range-separator="/" @confirm="drawFilter()" show-buttons></vue-datepicker-local>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-3">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Status         
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <div class="btn-group">
                            <button v-for="item of filter_status.all_status" type="button" 
                                @click="selected_status(item.status)"
                                :class="`btn btn-sm ${filter_status.selected_status == item.status ? item.btn_class : 'btn-default'}`" v-cloak>
                                {{item.status}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Status Lunas      
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <div class="btn-group btn-gtoup-sm">
                            <button v-for="item of filter_status_lunas.all_status" type="button" 
                                @click="selected_status_lunas(item.status)"
                                :class="`btn btn-sm ${filter_status_lunas.selected_status == item.status ? item.btn_class : 'btn-default'}`" v-cloak>
                                {{item.status}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Reset Filter 
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <button type="button" class="btn btn-sm btn-info" @click="resetFilter()"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row Datatable -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <ul class="nav nav-tabs nav-tabs nav-justified">
                        <li class="<?php echo is_manager() || is_admin() ? 'active' : ''; ?>" style="display: <?php echo is_manager() || is_admin() ? '' : 'none'; ?>">
                            <a href="#verifikasi_pos_manager_tab" table="#approval_pos_manager" data-toggle="tab" aria-expanded="true">
                                <i class="icon-menu7 position-left"></i> Approval Pos Manager
                            </a>
                        </li>
                        <li style="display: <?php echo is_manager() || is_admin() ? '' : 'none'; ?>">
                            <a href="#verifikasi_diskon_manager_tab" table="#approval_diskon_manager" data-toggle="tab" aria-expanded="true">
                                <i class="icon-menu7 position-left"></i> Approval Diskon Manager
                            </a>
                        </li>
                        <li class="<?php echo is_direktur() ? 'active' : ''; ?>" style="display: <?php echo is_direktur() || is_admin() ? '' : 'none'; ?>">
                            <a href="#verifikasi_diskon_direktur_tab" table="#approval_diskon_direktur" data-toggle="tab" aria-expanded="true">
                                <i class="icon-menu7 position-left"></i> Approval Diskon Direktur
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tabbable">
                        <div class="tab-content">
                            <div class="tab-pane active" id="verifikasi_pos_manager_tab" style="display: <?php echo is_manager() || is_admin() ? '' : 'none'; ?>">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="approval_pos_manager">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center" style="width: 10%">TANGGAL</th>
                                                <th class="text-center" style="width: 10%">NO INVOICE</th>
                                                <!-- <th class="text-center" style="width: 10%">NO MEMBER</th> -->
                                                <th class="text-center" style="width: 18%">MEMBER</th>
                                                <th class="text-center" style="width: 10%">STATUS LUNAS</th>
                                                <th class="text-center" style="width: 10%">STATUS</th>
                                                <th class="text-center" style="width: 10%">AKSI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane" id="verifikasi_diskon_manager_tab" style="display: <?php echo is_manager() || is_admin() ? '' : 'none'; ?>">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="approval_diskon_manager">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center" style="width: 10%">TANGGAL</th>
                                                <th class="text-center" style="width: 10%">NO INVOICE</th>
                                                <!-- <th class="text-center" style="width: 10%">NO MEMBER</th> -->
                                                <th class="text-center" style="width: 18%">MEMBER</th>
                                                <th class="text-center" style="width: 10%">STATUS LUNAS</th>
                                                <th class="text-center" style="width: 10%">STATUS</th>
                                                <th class="text-center" style="width: 10%">AKSI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane <?php echo is_direktur() ? 'active' : ''; ?>" id="verifikasi_diskon_direktur_tab" style="display: <?php echo is_direktur() || is_admin() ? '' : 'none'; ?>">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="approval_diskon_direktur">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center" style="width: 10%">TANGGAL</th>
                                                <th class="text-center" style="width: 10%">NO INVOICE</th>
                                                <!-- <th class="text-center" style="width: 10%">NO MEMBER</th> -->
                                                <th class="text-center" style="width: 18%">MEMBER</th>
                                                <th class="text-center" style="width: 10%">STATUS LUNAS</th>
                                                <th class="text-center" style="width: 10%">STATUS</th>
                                                <th class="text-center" style="width: 10%">AKSI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_reject" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Keterangan Reject</h5>
            </div>

            <div class="modal-body">
                <input type="hidden" id="uid">
                <input type="hidden" id="type">
                <textarea class="form-control" id="desc_reject"></textarea>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="VM_LIST_APPROVAL.reject_pos()">Save</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_status" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">INFO APPROVAL</h5>
            </div>

            <div class="modal-body">
                <hr>
                <h6 class="text-semibold">Diskon Per Barang</h6>
                <table style="width: 100%" class="table table-bordered">
                    <thead>
                        <tr class="bg-slate">
                            <td>Nama Barang</td>
                            <td>Qty</td>
                            <td>Disc</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody id="detail-per_item">
                        <tr class="empty_state_item">
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h6 class="text-semibold">Diskon Keseluruhan</h6>
                <table style="width: 100%" class="table table-bordered">
                    <thead>
                        <tr class="bg-slate">
                            <td colspan="2">Keterangan</td>
                            <td>Disc</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody id="detail-semua">
                        <tr class="empty_state_all">
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h6 class="text-semibold">Status Approval</h6>
                <table style="width: 100%" id="main-approval">
                    
                </table>
                <!-- <h6 class="text-semibold">Another paragraph</h6>
                <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
            </div>

            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>

