<!-- Content area -->
<div class="content content-custom" id="list_transaksi">
    <!-- Row Filter -->
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Filter Tanggal         
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                       <vue-datepicker-local v-model="filter_range" :local="setupDefault.datepicker.lang" range-separator="/" @confirm="drawFilter()" show-buttons></vue-datepicker-local>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Status         
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <div class="btn-group">
                            <button v-for="item of filter_status.all_status" type="button" 
                                @click="selected_status(item.status)"
                                :class="`btn btn-sm ${filter_status.selected_status == item.status ? item.btn_class : 'btn-default'}`" v-cloak>
                                {{item.status}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Status Lunas      
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <div class="btn-group btn-gtoup-sm">
                            <button v-for="item of filter_status_lunas.all_status" type="button" 
                                @click="selected_status_lunas(item.status)"
                                :class="`btn btn-sm ${filter_status_lunas.selected_status == item.status ? item.btn_class : 'btn-default'}`" v-cloak>
                                {{item.status}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Reset Filter 
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <button type="button" class="btn btn-sm btn-info" @click="resetFilter()"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row Datatable -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading no-padding-bottom uppercase">
                  
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="myTable">
                            <thead>
                                <tr class="bg-slate">
                                    <th class="text-center" style="width: 10%">TANGGAL</th>
                                    <th class="text-center" style="width: 10%">NO INVOICE</th>
                                    <th class="text-center" style="width: 10%">NO DOKTER</th>
                                    <th class="text-center" style="width: 20%">DOKTER</th>
                                    <th class="text-center" style="width: 8%">STATUS LUNAS</th>
                                    <th class="text-center" style="width: 10%">STATUS PEMBAYARAN</th>
                                    <th class="text-center" style="width: 10%">STATUS APPROVAL</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <!-- <thead>
                                <tr class="bg-white">
                                    <td><input class="form-control"></td>
                                    <td><input class="form-control"></td>
                                    <td><input class="form-control"></td>
                                    <td>Status Lunas</td>
                                    <td>Status</td>
                                </tr>
                            </thead> -->
                            <tbody>
                                <tr></tr>
                                    <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_rejected" class="modal fade"  tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Rejected Description</h5>
            </div>
            <div class="modal-body">
               <div id="rejected_desc_wrapper">
               </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_reject" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Keterangan Reject</h5>
            </div>

            <div class="modal-body">
                <input type="hidden" id="uid">
                <input type="hidden" id="type">
                <textarea class="form-control" id="desc_reject"></textarea>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="VM_LIST_TRANSAKSI.reject_pos()">Save</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_status" class="modal fade" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">INFO APPROVAL</h5>
            </div>

            <div class="modal-body">
                <hr>
                <h6 class="text-semibold">Diskon Per Barang</h6>
                <table style="width: 100%" class="table table-bordered">
                    <thead>
                        <tr class="bg-slate">
                            <td>Nama Barang</td>
                            <td>Qty</td>
                            <td>Disc</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody id="detail-per_item">
                        <tr class="empty_state_item">
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h6 class="text-semibold">Diskon Keseluruhan</h6>
                <table style="width: 100%" class="table table-bordered">
                    <thead>
                        <tr class="bg-slate">
                            <td colspan="2">Keterangan</td>
                            <td>Disc</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody id="detail-semua">
                        <tr class="empty_state_all">
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h6 class="text-semibold">Status Approval</h6>
                <table style="width: 100%" id="main-approval">
                    
                </table>
            </div>
        </div>
    </div>
</div>