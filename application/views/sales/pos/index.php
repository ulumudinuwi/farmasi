<!-- Content area -->
<div id="pos_menu" class="content content-custom" v-if="page == 1">

    <!-- Detached content -->
    <!-- <div class="container-detached">
        <div class="content-detached"> -->
        <div class="col-md-12">
            <div class="row">
                <div class="panel">
                    <div class="panel-body">
                        <!-- add Appen -->
                        <ul id="content-slider" class="content-slider">
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            
            <!-- Item options -->
            <div class="navbar navbar-default navbar-xs navbar-component label_bonus_barang hide">
                <h1 class="text-center">Pilih Bonus untuk Barang <b id="nama_barang"></b></h1>
                <!--<ul class="nav navbar-nav no-border visible-xs-block">
                    <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                </ul>

                 <div class="navbar-collapse collapse" id="navbar-filter">
                    <p class="navbar-text">Filter:</p>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-time-asc position-left"></i> By date <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Show all</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Today</a></li>
                                <li><a href="#">Yesterday</a></li>
                                <li><a href="#">This week</a></li>
                                <li><a href="#">This month</a></li>
                                <li><a href="#">This year</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-amount-desc position-left"></i> By status <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Show all</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Open</a></li>
                                <li><a href="#">On hold</a></li>
                                <li><a href="#">Resolved</a></li>
                                <li><a href="#">Closed</a></li>
                                <li><a href="#">Dublicate</a></li>
                                <li><a href="#">Invalid</a></li>
                                <li><a href="#">Wontfix</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-numeric-asc position-left"></i> By priority <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Show all</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Highest</a></li>
                                <li><a href="#">High</a></li>
                                <li><a href="#">Normal</a></li>
                                <li><a href="#">Low</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> -->
            </div>
            <!-- /Item options -->

            <!-- Item grid -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-dropbox"></i> Daftar Barang</h6>
                    <div class="heading-elements">
                        <form class="heading-form" action="#">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Cari Barang..." v-model="barang.q">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-icon" @click="onSearchBarang()"><i class="icon-search4"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
                <div class="panel-body">
                    <div class="list-wrapper" v-if="barang.isLoading">
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                        <div class="item-barang">
                            <content-loader :height="230" :width="166">
                                <rect x="0" y="0" rx="3" ry="3" width="166" height="166"></rect> 
                                <rect x="8" y="178" rx="3" ry="3" width="150" height="15"></rect>
                                <rect x="15" y="202" rx="3" ry="3" width="136" height="13"></rect>
                            </content-loader>
                        </div>
                    </div>
                    <div class="list-wrapper" v-else>
                        <div class="item-barang" v-for="(barang, i) of barang.list_barang" @click="addTocart(i)" v-bind:class="is_selectedItem(barang.uid_barang) ? 'active' : ''" v-cloak>
                            <div class="item-thumb">
                                <img v-bind:src="barang.thumb" alt="">
                            </div>
                            <div class="item-body">
                                <span class="nama_barang" v-cloak style="margin-bottom: 7px;">{{ barang.nama }}</span>
                                <span class="harga_barang" v-cloak>Rp.{{ parseFloat(barang.harga) | to_rupiah}}</span>
                                <span class="qty" v-cloak>QTY : {{ stockTersedia(barang.uid_barang) | bil_bulat }}</span>
                                <span class="expdate" v-cloak>Exp Date : {{barang.expired_date}}</span>
                            </div>
                            <!-- <div class="stock_barang" v-cloak>QTY : {{ stockTersedia(barang.uid_barang) | bil_bulat }}</div> -->
                        </div>
                        <div class="empty-item" v-if="barang.list_barang.length == 0" v-cloak>
                            <div class="icon-thumb">
                                <i class="icon-box"></i>
                            </div>
                            <br>
                            <span class="empty-label-item">TIDAK ADA BARANG</span>
                        </div>
                    </div>
                    <div class="btn-load-wrapper" v-cloak>
                        <button class="btn btn-primary" v-if="!barang.isLoading && barang.more_page" @click="get_list_barang(true)" v-bind:disabled="barang.isLoading" v-cloak>LOAD MORE</button>
                    </div>
                </div>
            </div>
            <!-- /Item grid -->
        </div>

        <!-- </div>
    </div> -->
    <!-- /detached content -->


    <!-- Detached sidebar -->
    <!-- <div class="sidebar-detached">
        <div class="sidebar sidebar-default sidebar-cs"> -->
        <div class="col-md-4">
            <div class="panel panel-white">
                
                <div class="sidebar-content">
                    <!-- Search task -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span><i class="icon-search4"></i> Cari Dokter</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <div class="row">
                                <div class="col-xs-12" style="margin-bottom: 10px;">
                                    <select2-ajax :url="global_url.api.sales.pos.list_member_select2" :placeholder="'Pilih Dokter ...'" @onselect="getSearch($event)"></select2-ajax>
                                    <!-- <div class="search-box">
                                        <carimember v-bind:search="person_search" @setresult="getSearch($event)" :items="list_person" place-holder="Pilih Member"/>
                                    </div> -->
                                </div>

                                <div class="col-xs-12">
                                    <div class="member-wrapper">
                                        <div class="member-label" @click="openDetail()">
                                            <img class="member-label-icon" v-bind:src="assetsUrl+'img/icon/dokter.png'" alt="">
                                            <span class="member-label-title" v-cloak>{{ selected_person.nama_member}}</span>
                                        </div>
                                        <div class="member-label">
                                            <img class="member-label-icon" v-bind:src="assetsUrl+'img/icon/sales.png'" alt="">
                                            <span class="member-label-title" v-cloak>{{ selected_person.nama_sales }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12" style="margin-top: 10px" v-if="detail_member == true">
                                    <div class="panel panel-flat" v-cloak>
                                        <div class="panel-body">
                                            <!-- <ul class="list">
                                                <li>Nama member : <span class="text-semibold">{{ member_bio.nama_dokter }}</span></li>
                                                <li>Nama sales : <span class="text-semibold">{{ member_bio.nama_sales }}</span></li>
                                                <li>Plafon : <span class="text-semibold">Rp. {{ member_bio.plafon_dokter | to_rupiah }}</span></li>
                                                <li>Sisa Plafon : <span class="text-semibold">-</span></li>
                                                <li>Order Terakhir : <a href="/a"></a> </li>
                                                <li>Jatuh Tempo :  <span class="text-semibold"></span></li>
                                            </ul> -->
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">No Dokter</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.no_member}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">Nama Dokter</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.nama_dokter}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">Email Dokter</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.email_dokter}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">No.Tlp Dokter</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.no_hp_dokter}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">Point</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.point_dokter}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">Plafon</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold"> Rp. {{ parseFloat(member_bio.plafon_dokter) | to_rupiah}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">Alamat</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.alamat_dokter }}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">Nama Klinik</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.nama_klinik_dokter}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">No.Tlp Klinik</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.no_tlp_klinik_dokter}}</span>
                                                </div>
                                            </div>
                                            <div class="detail-group-sm">
                                                <div class="detail-label">
                                                    <label for="" class="text-bold">Alamat Klinik</label>
                                                </div>
                                                <div class="detail-info">
                                                    <span class="text-semibold">{{ member_bio.alamat_klinik_dokter}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /search task -->

                    <!-- Task navigation -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span><i class="icon-cart2"></i>  Cart <span class="badge badge-danger" v-if="cart.list_barang.length > 0" v-cloak>{{cart.list_barang.length}}</span></span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content no-padding">
                            <div class="order-list" v-if="cart.list_barang.length > 0" v-cloak>
                                <div class="item-order" v-for="(barang, i) of cart.list_barang">
                                    <div class="__left">
                                        <div class="__first">
                                            <span class="__title">{{ barang.nama }}</span>
                                        </div>
                                        <div class="__second">
                                            <span class="__discount" @click="changeDiskonPerItem(i)" v-if="barang.is_bonus == 0">
                                                <!-- <img class="__discount_image" v-bind:src="assetsUrl+'img/icon/discount.png'" alt=""> -->
                                                <span class="__discount_title"> {{ barang.disc | to_rupiah }} %</span>
                                            </span>
                                            <span class="__price" v-if="barang.is_bonus == 0">
                                                <!-- <img class="__price_image" v-bind:src="assetsUrl+'img/icon/price.png'" alt=""> -->
                                                <span class="__price_title">Rp. {{total_per_barang(i) | to_rupiah}}</span>
                                            </span>
                                            <span v-if="barang.is_bonus == 1">
                                                <span class="text-danger text-size-mini">Bonus dari barang {{ barang.bonus_from_barang.nama }}</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="__right">
                                        <div class="__qty_group">
                                            <button class="__qty-action" @click="qtyDecrease(i)"><i class="icon-minus2"></i></button>
                                            <input type="text" class="__qty_item"  v-on:keydown="isNumber($event)" v-on:keyup="updateQty($event,i)" v-model="cart.list_barang[i].qty">
                                            <button class="__qty-action" @click="qtyIncrease(i)"><i class="icon-plus2"></i></button>
                                        </div>
                                        <button class="btn btn-danger clear-btn" :id="`clear-btn_${i}`" @click="removeMulti(i)">
                                            <i class="icon-trash"></i>
                                        </button>
                                        <button class="btn btn-info bonus-btn" :id="`bonus-btn_${i}`" @click="addBonusCart(i)" v-if="barang.is_bonus == 0">
                                            <i class="icon-plus-circle2"></i>
                                        </button>
                                        <button class="btn btn-info hide done-bonus-btn" :id="`done-bonus-btn_${i}`" @click="doneBonusCart(i)" v-if="barang.is_bonus == 0">
                                            <i class="icon-stack-check"></i>
                                        </button>
                                        <div class="__qty_group">
                                            <button class="__qty-action">
                                                <img class="__price_image" v-bind:src="assetsUrl+'img/icon/price.png'" alt="">
                                            </button>
                                            <input class="__qty_item"  v-on:keydown="isNumber($event)" v-on:keyup="updateDiskonPlus($event,i)" v-model="cart.list_barang[i].disc_plus" :options="setupDefault.percent" value="0">
                                            <button class="__qty-action">%</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-else class="empty-cart">
                                <div class="icon-thumb">
                                    <i class="icon-cart4"></i>
                                </div>
                                <br>
                                <span class="empty-label">BELUM ADA BARANG YANG DIPILIH</span>
                            </div>
                        </div>
                    </div>
                    <!-- /task navigation -->


                    <!-- Completeness stats -->
                    <div class="sidebar-category">
                        <!-- <div class="category-title">
                            <span>Total Harga</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div> -->

                        <div class="category-content">
                            <div class="total-wrapper">
                                <div class="__first_column">
                                    <div class="label_total">Plafon</div>
                                    <div class="price_total" v-cloak>Rp. {{ parseFloat(member_bio.plafon_dokter) | to_rupiah}}</div>
                                </div>
                                <div  class="__first_column">
                                    <div class="label_total">TOTAL</div>
                                    <div class="price_total" v-cloak>Rp. {{ total_belanja | to_rupiah }}</div>
                                </div>
                                <div  class="__second_column">
                                    <button class="btn btn-default" @click="saveToDraf"><i class="icon-floppy-disk"></i></button>
                                    <button class="btn btn-primary btn-block text-bold" @click="onPay">BAYAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /completeness stats -->
                </div>
            </div>
        </div>
        <!-- </div>
    </div> -->
    <!-- /detached sidebar -->
    <!-- Footer -->
    <div class="footer text-muted">
        &copy; <?php echo date('Y'); ?> VMT Software
    </div>
    <!-- /footer -->
    <!-- modal registered -->
    <modaldiskon v-if="modal.diskon.open" @close="modal.diskon.open = false" @save="updateDiskonPerItem($event)" v-bind:param="modal.diskon.param">
</div>

<div id="payment_menu" class="content content-custom" v-if="page == 2">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <div class="text-right">
                    <button type="submit" class="btn btn-success btn-labeled" @click="onProcess()">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                    <button @click="onBatal()" class="btn btn-default cancel-button">
                        Batal
                    </button>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-user-tie"></i> Detail Dokter   
                                </h6>
                            </div>
                            <div class="panel-body" style="display: block;">
                                <!-- <pre>{{ member_bio }}</pre> -->
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No Dokter</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.no_member}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Dokter</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.nama_dokter}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Email Dokter</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.email_dokter}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Dokter</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.no_hp_dokter}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Point</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.point_dokter}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Plafon</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold"> Rp. {{ parseFloat(member_bio.plafon_dokter) | to_rupiah}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.alamat_dokter }}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.nama_klinik_dokter}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.no_tlp_klinik_dokter}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ member_bio.alamat_klinik_dokter}}</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>   
                    </div>
                    <!-- <div class="col-md-8">
                        <div class="panel panel-flat" v-if="member_bio">
                            <div class="panel-body">
                                <div class="biodata_header">
                                    <img src="http://gudang-farmasi.apk/assets/img/polygon.png" alt="" style="height:28px;">
                                    <span class="header_title">Biodata Member</span>
                                </div>
                                <h4 class="biodata_nama">{{ member_bio.nama_dokter }}</h4>
                                <span class="biodata_title">Dokter</span>
                                <hr style="margin: 20px 0px 10px 0px">
                                <div class="info-container">
                                    <table class="info-wrapper">
                                        <tbody>
                                            <tr>
                                                <td class="info-title" style="width: 20%">Alamat</td>
                                                <td></td>
                                                <td class="info-content">{{ member_bio.alamat_dokter }}</td>
                                            </tr>
                                            <tr>
                                                <td class="info-title" style="width: 20%">Nama Klinik</td>
                                                <td></td>
                                                <td class="info-content">{{ member_bio.nama_klinik_dokter }}</td>
                                            </tr>
                                            <tr>
                                                <td class="info-title" style="width: 20%">Alamat Klinik</td>
                                                <td></td>
                                                <td class="info-content">{{ member_bio.alamat_klinik_dokter }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-5">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-file-text2"></i> Data Transaksi
                                </h6>
                            </div>
                            <div class="panel-body">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-calendar2"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal Transakasi</div>

                                            <!-- <div class="media-annotation">{{ cart.tgl_transaksi }}</div> -->
                                            <div class="media-annotation">
                                                <vue-datepicker-local v-model="cart.tgl_transaksi" :local="setupDefault.datepicker.lang" format="YYYY-MM-DD HH:mm:ss"></vue-datepicker-local>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-menu6"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">No. Kwitansi</div>

                                            <div class="media-annotation">_____</div>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-user-check"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                            <div class="media-annotation">{{ member_bio.nama_sales }}</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-truck position-left"></i>
                                    Kurir <span class="badge badge-success" v-if="parseFloat(cart.total_harga) >= 3000000" v-cloak>Free Ongkir</span>
                                </h6>
                                <div class="heading-elements">
                                    <form action="#" class="form-inline">
                                        <div class="form-group">
                                            <input type="checkbox" v-model="pembayaran.is_kurir" @click="setKurir()" class="checkbox-custom">
                                            <label class="label label-info">Gunakan Kurir</label>
                                        </div>
                                    </form>
                                </div>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>
                            <div class="panel-body" v-if="pembayaran.is_kurir">
                                <form class="form-horizontal">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-12">Ekspedisi</label>
                                            <div class="col-md-12">
                                                <select2 :classname="'form-control select2-cs'" :options="master_ekspedisi" v-model="pembayaran.ekspedisi" :lengthselected="0">
                                                    <option disabled value="0">Pilih ekspedisi</option>
                                                </select2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" v-if="parseFloat(cart.total_harga) < 30000000">
                                        <div class="form-group">
                                            <label for="">Ongkos Kirim </label>
                                            <vue-autonumeric :placeholder="'Nominal'" :options="batas_ongkir" v-model="pembayaran.ongkir"></vue-autonumeric>
                                        </div>    
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-5">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-megaphone position-left"></i>
                                    Voucher
                                </h6>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Masukan Kode Voucher">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Gunakan Voucher</button>
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-box position-left"></i>
                                    Detail Transaksi
                                   
                                </h6>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead class="align-center">
                                            <tr class="bg-slate">
                                                <th style="width: 32%">Nama Barang</th>
                                                <th style="width: 20%">Harga</th>
                                                <th>Qty</th>
                                                <th>(%)</th>
                                                <th style="width: 25%">Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(barang, i) of cart.list_barang">
                                                <td>{{barang.nama}}</td>
                                                <td>
                                                    <span class="pull-left" v-if="barang.is_bonus == 0">Rp.</span>
                                                    <span class="pull-right" v-if="barang.is_bonus == 0">{{ parseFloat(barang.harga_persatu) | to_rupiah }}</span>
                                                    <span class="pull-left text-size-mini text-danger" v-if="barang.is_bonus == 1">
                                                        Bonus dari Barang {{barang.bonus_from_barang.nama}}
                                                    </span>
                                                </td>
                                                <td>{{barang.qty}}</td>
                                                <td>{{barang.disc | to_rupiah}} % + {{barang.disc_plus | to_rupiah}} %</td>
                                                <td>
                                                    <span class="pull-left" v-if="barang.is_bonus == 0">Rp.</span>
                                                    <span class="pull-right" v-if="barang.is_bonus == 0">{{ total_per_barang(i) | to_rupiah }}</span>
                                                    <span class="pull-right" v-if="barang.is_bonus == 1">0</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Total Harga Barang</td>
                                                <td colspan="2" class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_harga_barang | to_rupiah }}</span></td>
                                            </tr>
                                            <tr class="hide">
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Diskon</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-info" @click="changeTypeDiskon"><i class="icon-gear"></i></button>
                                                        </div>
                                                        <vue-autonumeric :placeholder="'Diskon'" :options="type_diskon" v-model="pembayaran.disc"></vue-autonumeric>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr v-if="pembayaran.ongkir != 0">
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Ongkos Kirim</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ pembayaran.ongkir | to_rupiah}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Grand Total</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ grand_total | to_rupiah }}</span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 hide">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-credit-card position-left"></i>Metode Pembayaran
                                </h6>
                                <div class="heading-elements" v-if="!pembayaran.is_free">
                                    <form action="#" class="form-inline">
                                        <div class="form-group">
                                            <input type="checkbox" v-model="pembayaran.piutang" @click="setPiutang()" class="checkbox-custom">
                                            <label class="label label-info">Piutang</label>
                                        </div>
                                    </form>
                                </div>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>
                            <div class="panel-body">
                                <div class="table table-responsive" v-if="pembayaran.piutang == false && pembayaran.is_free == false">
                                    <table class="table table-striped table-bordered">
                                        <thead class="align-center">
                                            <tr class="bg-slate ">
                                                <th>Cara Bayar</th>
                                                <th>Receipt No</th>
                                                <th>Bank</th>
                                                <th>Mesin EDC</th>
                                                <th>Nominal</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            <tr v-for="(metode_pembayaran, i) of pembayaran.metode_pembayarans">
                                                <td style="width: 18%">
                                                    <select2 :classname="'form-control select2-cs'" :options="master_cara_bayar" v-model="pembayaran.metode_pembayarans[i].cara_bayar">
                                                        <option disabled value="0">Pilih cara bayar</option>
                                                    </select2>
                                                    <!-- <select2 :classname="'form-control select2-cs'" v-model="pembayaran.metode_pembayarans[i].cara_bayar" v-else>
                                                        <option value="tunai" selected>Tunai</option>
                                                    </select2> -->
                                                </td>
                                                <td style="width: 18%">
                                                    <input type="text" class="form-control" v-if="metode_pembayaran.cara_bayar != 'tunai'" v-model="pembayaran.metode_pembayarans[i].receipt_no">
                                                </td>
                                                <td style="width: 20%">
                                                    <select2 :classname="'form-control select2-cs'" :options="master_bank" v-model="pembayaran.metode_pembayarans[i].bank_id" v-if="master_bank.length > 0 && metode_pembayaran.cara_bayar != 'tunai'">
                                                        <option disabled value="0">Pilih bank</option>
                                                    </select2>
                                                    <!-- <select class="form-control" v-model="pembayaran.metode_pembayarans[i].bank_id">
                                                        <option value="">Pilih Bank</option>
                                                        <option v-for="bank of master_bank" v-bind:value="bank.id">{{ bank.nama }}</option>
                                                    </select> -->
                                                </td>
                                                <td style="width: 18%">
                                                    <select2 :classname="'form-control select2-cs'" :options="master_edc" v-model="pembayaran.metode_pembayarans[i].mesin_edc_id" v-if="master_edc.length > 0 && metode_pembayaran.cara_bayar != 'tunai'">
                                                        <option disabled value="0">Pilih mesin EDC</option>
                                                    </select2>
                                                    <!-- <select class="form-control" v-model="pembayaran.metode_pembayarans[i].mesin_edc_id">
                                                        <option value="">Pilih Mesin Edc</option>
                                                        <option v-for="edc of master_edc" v-bind:value="edc.id">{{ edc.nama }}</option>
                                                    </select> -->
                                                </td>
                                                <td style="width: 28%">
                                                    <vue-autonumeric :placeholder="'Nominal'" :options="batas_pembayaran[i]" v-model="pembayaran.metode_pembayarans[i].nominal"></vue-autonumeric>
                                                </td>
                                                <td class="text-center">
                                                    <button class="btn btn-danger" @click="removePembayaran(i)"><i class="icon-trash"></i></button>
                                                </td>
                                            </tr>
                                            <!-- <tr>
                                                <td></td>
                                                <td>0002</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td><span>Rp.10.710.880,00</span></td>
                                                <td class="text-center">
                                                    <button class="btn btn-danger"><i class="icon-trash"></i></button>
                                                </td>
                                            </tr> -->
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3"></td>
                                                <td class="uppercase bold">TOTAL</td>
                                                <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah}}</span></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <button class="btn btn-labeled btn-primary btn-block" @click="addPembayaran"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                                </td> 
                                            <!-- <td></td> -->
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div v-if="pembayaran.is_free == true">
                                    GRATIS 
                                </div>
                                <div v-if="pembayaran.piutang == true">
                                    PIUTANG AKTIF
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 hide">
                        <div class="panel panel-default" v-if="!pembayaran.is_free">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-clipboard"></i> Rincian Pembayaran
                                </h6>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless text-bold">
                                    <tr>
                                        <td>Total Harga</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ pembayaran.grand_total | to_rupiah}}</span></td>
                                    </tr>
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Total Pembayaran</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ pembayaran.total_pembayaran | to_rupiah }}</span></td>
                                    </tr>
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Sisa Pembayaran</td>
                                        <td>
                                            <span v-if="sisapembayaran >= 0">
                                                <span class="pull-left">Rp.</span><span class="pull-right">{{ sisapembayaran | to_rupiah }}</span>
                                            </span>
                                            <span v-else>
                                                <span class="pull-left">Rp.</span><span class="pull-right text-danger">( {{ sisapembayaran | to_rupiah }} )</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr v-if="pembayaran.piutang">
                                        <td>Piutang</td>
                                        <td><span class="pull-left">Aktif</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 hide">
                        <div class="panel panel-white" v-if="!pembayaran.is_free">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-cash4"></i> Pembayaran tunai
                                </h6>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless text-bold">
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Bayar Tunai</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ bayar_tunai_only | to_rupiah}}</span></td>
                                    </tr>
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Uang diterima</td>
                                        <td><vue-autonumeric :placeholder="'Rupiah'" :options="setupDefault.rupiah" v-model="uang_diterima"></vue-autonumeric></td>
                                    </tr>
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Kembalian</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ kembalian | to_rupiah}}</span></td>
                                    </tr>
                                    <tr v-if="pembayaran.piutang">
                                        <td>Piutang</td>
                                        <td><span class="pull-left">Aktif</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="text-right text-bold small text-muted">Total Point yg Didapat <span>{{ pointDidapat }}</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="status_pembayaran" value="1">
                    <!-- <div class="col-md-12">
                        <pre v-if="pembayaran">{{ pembayaran }}</pre>
                        <pre>{{ cart }}</pre>
                    </div> -->
                </div>
            </div>
            <div class="panel-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success btn-labeled" @click="onProcess()">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                    <button @click="onBatal()" class="btn btn-default cancel-button">
                        Batal
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /content area -->
<script type="text/javascript">
    let base_url = '<?php echo base_url(); ?>';
</script>