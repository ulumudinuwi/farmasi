<div id="pembayaran_baru" class="content content-custom">
    <!-- Row Filter -->
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-flat">
                <!-- <div class="panel-heading uppercase text-center">
                    Filter Tanggal         
                </div> -->
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <select2-ajax2 
                            :url="global_url.api.sales.cicilan.list_invoice_select2" 
                            :template_result="template_select2.template_result"
                            placeholder="Cari Invoice ..." 
                            @onselect="getSearch($event)"
                            @onselect="">
                        </select2-ajax2> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-flat">
                <!-- <div class="panel-heading uppercase text-center">
                    Filter Tanggal         
                </div> -->
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <button class="btn btn-block btn-primary" :disabled="!f_title" @click="onProcess()">BAYAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div class="row">
        <div class="col-md-12" v-cloak v-if="show_detail">
            <div class="panel panel-white" id="detail_invoice">
                <div class="panel-heading">
                    <h6 class="panel-title">Detail Transaksi
                        <span class="pull-right" v-html="f_title"></span>
                    </h6>    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-user-tie"></i> Detail Member   
                                    </h6>
                                </div>
                                <div class="panel-body" style="display: block;" v-if="detail_member.member">
                                    <!-- <pre>{{ member_bio }}</pre> -->
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">No Member</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.no_member}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">Nama Member</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.nama}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">Email Member</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.email}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">No.Tlp Member</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.no_hp}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">Point</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.point}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">Plafon</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold"> Rp. {{ parseFloat(detail_member.member.plafon) | to_rupiah}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">Alamat</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.alamat }}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">Nama Klinik</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.nama_klinik}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">No.Tlp Klinik</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.no_tlp_klinik}}</span>
                                        </div>
                                    </div>
                                    <div class="detail-group">
                                        <div class="detail-label">
                                            <label for="" class="text-bold">Alamat Klinik</label>
                                        </div>
                                        <div class="detail-info">
                                            <span class="text-semibold">{{ detail_member.member.alamat_klinik}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <div class="col-md-5">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-file-text2"></i> Data Transaksi
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <ul class="media-list">
                                        <li class="media">
                                            <div class="media-left">
                                                <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                    <i class="icon-calendar2"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <div class="media-heading text-semibold text-uppercase bold">Tanggal Transakasi</div>

                                                <div class="media-annotation">{{ detail_transaksi.tanggal_transaksi | d_m_Y_H_m_s }}</div>
                                            </div>
                                            <div class="media-body">
                                                <div class="media-heading text-semibold text-uppercase bold">Tanggal Jatuh Tempo</div>

                                                <div class="media-annotation">{{ detail_transaksi.tgl_jatuh_tempo | d_m_Y }}</div>
                                            </div>
                                        </li>

                                        <li class="media">
                                            <div class="media-left">
                                                <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                                    <i class="icon-user-check"></i>
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                                <div class="media-annotation" v-if="detail_member.sales">{{ detail_member.sales.nama }}</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left">
                                                <a :class="`btn btn-flat btn-icon btn-rounded btn-sm ${detail_transaksi.status_lunas ? 'border-success text-success' : 'border-danger text-danger'}`">
                                                    <i class="fa fa-check" v-if="detail_transaksi.status_lunas"></i>
                                                    <i class="icon-close2" v-if="!detail_transaksi.status_lunas"></i>
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <div class="media-heading text-semibold text-uppercase bold">Status Lunas</div>

                                                <div class="media-annotation">{{ detail_transaksi.status_lunas ? 'LUNAS' : 'BELUM LUNAS' }}</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left">
                                                <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                                    <i class="icon-credit-card2"></i>
                                                </a>
                                            </div>

                                            <div class="media-body" v-if="cicilan_ke > 0 && cicilan_ke != 1">
                                                <div class="media-heading text-semibold text-uppercase bold">CICILAN</div>

                                                <div class="media-annotation">CICILAN KE - <span v-cloak>{{cicilan_ke}}</span></div>
                                            </div>
                                            <div class="media-body" v-if="cicilan_ke == 1">
                                                <div class="media-heading text-semibold text-uppercase bold">PEMBAYARAN</div>

                                                <div class="media-annotation">DILUNASI / DICICIL</span></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-truck position-left"></i>
                                        Kurir <span class="badge badge-success" v-if="parseFloat(detail_transaksi.total_harga) >= 30000000" v-cloak>Free Ongkir</span>
                                    </h6>
                                    <div class="heading-elements">
                                        <form action="#" class="form-inline">
                                            <label class="label label-info" v-if="detail_transaksi.is_kurir">Menggunakan Kurir</label>
                                            <label class="label label-danger" v-if="!detail_transaksi.is_kurir">Tidak Menggunakan Kurir</label>
                                        </form>
                                    </div>
                                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                                </div>
                                <div class="panel-body" v-if="detail_transaksi.is_kurir">
                                    <form class="form-horizontal">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-12">Ekspedisi</label>
                                                <div class="col-md-12">
                                                    {{detail_transaksi.ekspedisi}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" v-if="parseFloat(detail_transaksi.total_harga) < 30000000">
                                            <div class="form-group">
                                                <label for="">Ongkos Kirim </label>
                                                <div class="col-md-12">
                                                    <span>Rp. {{ parseFloat(detail_transaksi.ongkir) | to_rupiah }}</span>
                                                </div>
                                            </div>    
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-megaphone position-left"></i>
                                        Voucher
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <!-- <form class="form-horizontal">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Masukan Kode Voucher">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button">Gunakan Voucher</button>
                                                </span>
                                            </div>
                                        </div>
                                    </form> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-box position-left"></i>
                                        Detail Transaksi
                                    
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead class="align-center">
                                                <tr class="bg-slate">
                                                    <th style="width: 32%">Nama Barang</th>
                                                    <th style="width: 20%">Harga</th>
                                                    <th>Qty</th>
                                                    <th>(%)</th>
                                                    <th style="width: 25%">Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(barang, i) of detail_pembelian">
                                                    <td>{{barang.nama}}</td>
                                                    <td>
                                                        <span class="pull-left" v-if="barang.is_bonus == 0">Rp.</span>
                                                        <span class="pull-right" v-if="barang.is_bonus == 0">
                                                            {{ parseFloat(barang.harga) | to_rupiah }}
                                                        </span>
                                                        <span class="pull-left text-size-mini text-danger" v-if="barang.is_bonus == 1">
                                                            Bonus dari Barang {{ JSON.parse(barang.is_bonus_detail).nama }}
                                                        </span>
                                                    </td>
                                                    <td>{{barang.qty}}</td>
                                                    <td>{{barang.diskon | to_rupiah}} % + {{barang.diskon_plus | to_rupiah}} %</td>
                                                    <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(barang.grand_total) | to_rupiah }}</span></td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="uppercase bold">Total Harga Barang</td>
                                                    <td colspan="2" class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_harga_barang | to_rupiah }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="uppercase bold">Diskon</td>
                                                    <td colspan="2" class="uppercase bold">
                                                        <span class="pull-left">{{ detail_transaksi.jenis_diskon != 'persen' ? 'Rp.' : ''}} </span><span class="pull-right">{{ detail_transaksi.diskon }} {{ detail_transaksi.jenis_diskon == 'persen' ? '%' : ''}}</span>
                                                    </td>
                                                </tr>
                                                <tr v-if="detail_transaksi.ongkir != 0">
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="uppercase bold">Ongkos Kirim</td>
                                                    <td colspan="2" class="uppercase bold">
                                                        <span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.ongkir) | to_rupiah}}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="uppercase bold">Grand Total</td>
                                                    <td colspan="2" class="uppercase bold">
                                                        <span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.grand_total) | to_rupiah }}</span>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-credit-card position-left"></i>Detail Pembayaran
                                    </h6>
                                    <div class="heading-elements">
                                        <label class="label label-info" v-if="detail_transaksi.piutang">Piutang</label>
                                        <a v-if="!detail_transaksi.piutang && !detail_transaksi.is_cicilan" :href="kuitansi_proc(total_pembayaran,getKetTotal(),no_kuitansi)" type="button" class="btn btn-default btn-labeled" target="_blank"><b><i class="icon-printer"></i></b>Kuitansi</a>
                                    </div>
                                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                                </div>
                                <div class="panel-body">
                                    <div class="panel-group panel-group-control panel-group-control-right content-group-lg" v-if="detail_transaksi.is_cicilan">
                                        <div class="panel panel-white" v-for="(cicilan, i) of detail_cicilan">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a data-toggle="collapse" :href="'#'+i" aria-expanded="false" class="collapsed">{{ cicilanKe(i) }}</a>
                                                </h6>
                                            </div>
                                            <div :id="i" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <div class="table table-responsive">
                                                        <table class="table table-striped table-bordered">
                                                            <thead class="align-center">
                                                                <tr class="bg-slate ">
                                                                    <th>Cara Bayar</th>
                                                                    <th>Receipt No</th>
                                                                    <th>Bank</th>
                                                                    <th>Mesin EDC</th>
                                                                    <th>Nominal</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <!-- <pre>{{ pembayaran.metode_pembayaran }}</pre> -->
                                                                <tr v-for="(metode_pembayaran) of cicilan">
                                                                    
                                                                    <td style="width: 18%">
                                                                        {{ find_cara_bayar(metode_pembayaran.jenis_pembayaran).text }}
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        {{ metode_pembayaran.no_receipt ? metode_pembayaran.no_receipt : '-' }}
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        {{ metode_pembayaran.bank_id != 0 ? find_bank(metode_pembayaran.bank_id) : '-' }}
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        {{ metode_pembayaran.mesin_edc_id != 0 ? find_edc(metode_pembayaran.mesin_edc_id) : '-' }}
                                                                    </td>
                                                                    <td style="width: 28%">
                                                                        <div v-if="metode_pembayaran.nominal_pembayaran">
                                                                            <span class="pull-left">Rp.</span><span class="pull-right">{{  parseFloat(metode_pembayaran.nominal_pembayaran) | to_rupiah}}</span>
                                                                        </div>
                                                                        <div v-else>
                                                                            <span>-</span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                            <!-- <pre>{{i}}</pre> -->
                                                                <tr>
                                                                    <td colspan="3"></td>
                                                                    <td class="uppercase bold">TOTAL</td>
                                                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran_per_cicilan(i) | to_rupiah}}</span></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table table-responsive" v-if="!detail_transaksi.piutang && detail_transaksi.is_cicilan">
                                        <table class="table table-striped table-bordered">
                                            <thead class="align-center">
                                                <tr class="bg-slate ">
                                                    <th>Cicilan Ke</th>
                                                    <th>Tanggal Pembayaran</th>
                                                    <th>No Kuitansi</th>
                                                    <th>Nominal</th>
                                                    <th style="width: 10%;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(cicilan, i) of detail_cicilan">
                                                    <td style="text-transform: capitalize">{{ text_cicilan(i) }}</td>     
                                                    <td>{{ (detail_cicilan[i][0].tanggal_bayar ? detail_cicilan[i][0].tanggal_bayar : detail_cicilan[i][0].created_at) | d_m_Y}}</td>
                                                    <td>{{ no_kuitansi[i].no_kuitansi }}</td>
                                                    <td>
                                                        <span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran_per_cicilan(i) | to_rupiah}}</span>
                                                    </td>
                                                    <td>
                                                        <a :href="kuitansi_proc(total_pembayaran_per_cicilan(i),getKetCicilan(i),no_kuitansi[i].no_kuitansi,detail_cicilan[i][0].tanggal_bayar)" type="button" class="btn btn-default btn-labeled" target="_blank"><b><i class="icon-printer"></i></b>Kuitansi</a>
                                                    </td>     
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="uppercase bold">TOTAL</td>
                                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah}}</span></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="table table-responsive" v-if="!detail_transaksi.piutang && !detail_transaksi.is_cicilan">
                                        <table class="table table-striped table-bordered">
                                            <thead class="align-center">
                                                <tr class="bg-slate ">
                                                    <th>Cara Bayar</th>
                                                    <th>Receipt No</th>
                                                    <th>Bank</th>
                                                    <th>Mesin EDC</th>
                                                    <th>Nominal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            
                                                <!-- <pre>{{ pembayaran.metode_pembayaran }}</pre> -->
                                                <tr v-for="(metode_pembayaran, i) of detail_pembayaran">
                                                    <td style="width: 18%">
                                                        {{ find_cara_bayar(metode_pembayaran.jenis_pembayaran).text }}
                                                    </td>
                                                    <td style="width: 18%">
                                                        {{ metode_pembayaran.no_receipt ? metode_pembayaran.no_receipt : '-' }}
                                                    </td>
                                                    <td style="width: 20%">
                                                        {{ metode_pembayaran.bank_id != 0 ? find_bank(metode_pembayaran.bank_id) : '-' }}
                                                    </td>
                                                    <td style="width: 18%">
                                                        {{ metode_pembayaran.mesin_edc_id != 0 ? find_edc(metode_pembayaran.mesin_edc_id) : '-' }}
                                                    </td>
                                                    <td style="width: 28%">
                                                        <div v-if="metode_pembayaran.nominal_pembayaran">
                                                            <span class="pull-left">Rp.</span><span class="pull-right">{{  parseFloat(metode_pembayaran.nominal_pembayaran) | to_rupiah}}</span>
                                                        </div>
                                                        <div v-else>
                                                            <span>-</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td class="uppercase bold">TOTAL</td>
                                                    <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah}}</span></td>
                                                
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div v-if="detail_transaksi.piutang">
                                        BELUM ADA TRANSAKSI PEMBAYARAN
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-credit-card position-left"></i>Metode Pembayaran
                                    </h6>
                                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                                </div>
                                <div class="panel-body">
                                    <div class="table table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <thead class="align-center">
                                                <tr class="bg-slate ">
                                                    <th>Cara Bayar</th>
                                                    <th>Receipt No</th>
                                                    <th>Bank</th>
                                                    <th>Mesin EDC</th>
                                                    <th>Tanggal Pembayaran</th>
                                                    <th>Nominal</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(item, i) of pembayaran.metode_pembayaran">
                                                    <td style="padding:10px 10px; width: 150px">
                                                        <select2 :classname="'form-control select2-cs'" :options="master_cara_bayar" v-model="pembayaran.metode_pembayaran[i].cara_bayar">
                                                            <option disabled value="0">Pilih cara bayar</option>
                                                        </select2>
                                                    </td>
                                                    <td style="padding:10px 10px; width: 150px">
                                                        <input type="text" class="form-control" v-if="item.cara_bayar != 'tunai'" v-model="pembayaran.metode_pembayaran[i].receipt_no">
                                                    </td>
                                                    <td style="padding:10px 10px; width: 150px">
                                                        <select2 :classname="'form-control select2-cs'" :options="master_bank" v-model="pembayaran.metode_pembayaran[i].bank_id" v-if="master_bank.length > 0 && item.cara_bayar != 'tunai'">
                                                            <option disabled value="0">Pilih bank</option>
                                                        </select2>
                                                    </td>
                                                    <td style="padding:10px 10px; width: 150px">
                                                        <select2 :classname="'form-control select2-cs'" :options="master_edc" v-model="pembayaran.metode_pembayaran[i].mesin_edc_id" v-if="master_edc.length > 0 && item.cara_bayar != 'tunai'">
                                                            <option disabled value="0">Pilih mesin EDC</option>
                                                        </select2>
                                                    </td>
                                                    <td style="padding:10px 10px; ">
                                                        <input type="date" class="form-control" v-model="pembayaran.metode_pembayaran[i].tanggal_bayar">
                                                    </td>
                                                    <td style="padding:10px 10px; width: 200px">
                                                        <vue-autonumeric :placeholder="'Nominal'" :options="batas_pembayaran[i]" v-model="pembayaran.metode_pembayaran[i].nominal"></vue-autonumeric>
                                                    </td>
                                                    <td class="text-center">
                                                        <button class="btn btn-danger" @click="removePembayaran(i)"><i class="icon-trash"></i></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td class="uppercase bold">TOTAL</td>
                                                    <td class="uppercase bold" colspan="3">
                                                        <span class="pull-left">Rp.</span><span class="pull-right">{{ new_total_pembayaran | to_rupiah}}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7">
                                                        <button class="btn btn-labeled btn-primary btn-block" @click="addPembayaran"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                                    </td> 
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-clipboard"></i> Rincian Pembayaran
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-borderless text-bold">
                                        <tr>
                                            <td>Total Harga</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.grand_total) | to_rupiah}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Total Pembayaran Sebelumnya</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah }}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Sisa Pembayaran Sebelumnya</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.sisa_pembayaran) | to_rupiah }}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Total Pembayaran Baru</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ new_total_pembayaran | to_rupiah }}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Sisa Pembayaran</td>
                                            <td><span class="pull-left">Rp.</span><span class="pull-right">{{ sisapembayaran | to_rupiah }}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="text-right">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" v-else>
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center uppercase">
                        Silahkan Masukan No.INVOICE terlebih dahulu.
                    </div>
                </div>
            </div>    
        </div>
    </div>

</div>