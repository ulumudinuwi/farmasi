<div id="detail_transaksi" class="content content-custom">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <!-- <div class="text-right">
                    <a :href="invoice_url" type="button" class="btn btn-default btn-labeled" target="_blank">
                        <b><i class="icon-printer"></i></b>
                        Invoice
                    </a>
                    <button type="button" class="btn btn-info btn-labeled">
                        <b><i class="fa fa-refresh"></i></b>
                        Return
                    </button>
                    <button type="button" class="btn btn-danger btn-labeled">
                        <b><i class="fa fa-close"></i></b>
                        Cancel
                    </button>
                </div> -->
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-user-tie"></i> Detail Member   
                                </h6>
                            </div>
                            <div class="panel-body" style="display: block;" v-if="detail_member.member">
                                <!-- <pre>{{ member_bio }}</pre> -->
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_member}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.nama}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Email Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.email}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_hp}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Point</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.point}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Plafon</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold"> Rp. {{ parseFloat(detail_member.member.plafon) | to_rupiah}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.alamat }}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.nama_klinik}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_tlp_klinik}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.alamat_klinik}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-file-text2"></i> Data Transaksi
                                </h6>
                            </div>
                            <div class="panel-body">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-calendar2"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal Transakasi</div>

                                            <div class="media-annotation">{{ detail_transaksi.tanggal_transaksi | d_m_Y }}</div>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal Jatuh Tempo</div>

                                            <div class="media-annotation">{{ detail_transaksi.tgl_jatuh_tempo | d_m_Y }}</div>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-user-check"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                            <div class="media-annotation" v-if="detail_member.sales">{{ detail_member.sales.nama }}</div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-warning text-warning btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Status Lunas</div>

                                            <div class="media-annotation" v-if="detail_member.sales">{{ detail_transaksi.status_lunas ? 'LUNAS' : 'BELUM LUNAS' }}</div>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Cicilan Ke</div>

                                            <div class="media-annotation"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-truck position-left"></i>
                                    Kurir <span class="badge badge-success" v-if="parseFloat(detail_transaksi.total_harga) >= 30000000" v-cloak>Free Ongkir</span>
                                </h6>
                                <div class="heading-elements">
                                    <form action="#" class="form-inline">
                                        <label class="label label-info" v-if="detail_transaksi.is_kurir">Menggunakan Kurir</label>
                                        <label class="label label-danger" v-if="!detail_transaksi.is_kurir">Tidak Menggunakan Kurir</label>
                                    </form>
                                </div>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>
                            <div class="panel-body" v-if="detail_transaksi.is_kurir">
                                <form class="form-horizontal">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-12">Ekspedisi</label>
                                            <div class="col-md-12">
                                                {{detail_transaksi.ekspedisi}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" v-if="parseFloat(detail_transaksi.total_harga) < 30000000">
                                        <div class="form-group">
                                            <label for="">Ongkos Kirim </label>
                                            <span>Rp. {{ detail_transaksi.ongkir | to_rupiah }}</span>
                                        </div>    
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-megaphone position-left"></i>
                                    Voucher
                                </h6>
                            </div>
                            <div class="panel-body">
                                <!-- <form class="form-horizontal">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Masukan Kode Voucher">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Gunakan Voucher</button>
                                            </span>
                                        </div>
                                    </div>
                                </form> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-box position-left"></i>
                                    Detail Transaksi
                                   
                                </h6>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead class="align-center">
                                            <tr class="bg-slate">
                                                <th style="width: 32%">Nama Barang</th>
                                                <th style="width: 20%">Harga</th>
                                                <th>Qty</th>
                                                <th>(%)</th>
                                                <th style="width: 25%">Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(barang, i) of detail_pembelian">
                                                <td>{{barang.nama}}</td>
                                                <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(barang.harga) | to_rupiah }}</span></td>
                                                <td>{{barang.qty}}</td>
                                                <td>{{barang.diskon | to_rupiah}} %</td>
                                                <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(barang.grand_total) | to_rupiah }}</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Total Harga Barang</td>
                                                <td colspan="2" class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_harga_barang | to_rupiah }}</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Diskon</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">{{ detail_transaksi.jenis_diskon != 'persen' ? 'Rp.' : ''}} </span><span class="pull-right">{{ detail_transaksi.diskon }} {{ detail_transaksi.jenis_diskon == 'persen' ? '%' : ''}}</span>
                                                </td>
                                            </tr>
                                            <tr v-if="detail_transaksi.ongkir != 0">
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Ongkos Kirim</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.ongkir) | to_rupiah}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Grand Total</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.grand_total) | to_rupiah }}</span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-credit-card position-left"></i>Metode Pembayaran
                                </h6>
                                <div class="heading-elements">
                                    <label class="label label-info" v-if="detail_transaksi.pitang">Piutang</label>
                                </div>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>
                            <div class="panel-body">
                                <div class="table table-responsive" v-if="!detail_transaksi.piutang">
                                    <table class="table table-striped table-bordered">
                                        <thead class="align-center">
                                            <tr class="bg-slate ">
                                                <th>Cara Bayar</th>
                                                <th>Receipt No</th>
                                                <th>Bank</th>
                                                <th>Mesin EDC</th>
                                                <th>Nominal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            <!-- <pre>{{ pembayaran.metode_pembayarans }}</pre> -->
                                            <tr v-for="(metode_pembayaran, i) of detail_pembayaran">
                                                <td style="width: 18%">
                                                    {{ find_cara_bayar(metode_pembayaran.jenis_pembayaran).text }}
                                                </td>
                                                <td style="width: 18%">
                                                    {{ metode_pembayaran.no_receipt ? metode_pembayaran.no_receipt : '-' }}
                                                </td>
                                                <td style="width: 20%">
                                                    {{ metode_pembayaran.bank_id != 0 ? find_bank(metode_pembayaran.bank_id) : '-' }}
                                                </td>
                                                <td style="width: 18%">
                                                    {{ metode_pembayaran.mesin_edc_id != 0 ? find_edc(metode_pembayaran.mesin_edc_id) : '-' }}
                                                </td>
                                                <td style="width: 28%">
                                                    <div v-if="metode_pembayaran.nominal_pembayaran">
                                                        <span class="pull-left">Rp.</span><span class="pull-right">{{  parseFloat(metode_pembayaran.nominal_pembayaran) | to_rupiah}}</span>
                                                    </div>
                                                    <div v-else>
                                                        <span>-</span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3"></td>
                                                <td class="uppercase bold">TOTAL</td>
                                                <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah}}</span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div v-else>
                                    PIUTANG
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-clipboard"></i> Rincian Pembayaran
                                </h6>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless text-bold">
                                    <tr>
                                        <td>Total Harga</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.grand_total) | to_rupiah}}</span></td>
                                    </tr>
                                    <tr v-if="!detail_transaksi.piutang">
                                        <td>Total Pembayaran</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ total_pembayaran | to_rupiah }}</span></td>
                                    </tr>
                                    <tr v-if="!detail_transaksi.piutang">
                                        <td>Sisa Pembayaran</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.sisa_pembayaran) | to_rupiah }}</span></td>
                                    </tr>
                                    <tr v-if="detail_transaksi.piutang">
                                        <td>Piutang</td>
                                        <td><span class="pull-left">Aktif</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-cash4"></i> Pembayaran tunai
                                </h6>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless text-bold">
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Bayar Tunai</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ bayar_tunai_only | to_rupiah}}</span></td>
                                    </tr>
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Uang diterima</td>
                                        <td><vue-autonumeric :placeholder="'Rupiah'" :options="setupDefault.rupiah" v-model="uang_diterima"></vue-autonumeric></td>
                                    </tr>
                                    <tr v-if="!pembayaran.piutang">
                                        <td>Kembalian</td>
                                        <td><span class="pull-left">Rp.</span><span class="pull-right">{{ kembalian | to_rupiah}}</span></td>
                                    </tr>
                                    <tr v-if="pembayaran.piutang">
                                        <td>Piutang</td>
                                        <td><span class="pull-left">Aktif</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="text-right text-bold small text-muted">Total Point yg Didapat <span>{{ pointDidapat }}</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-md-12">
                        <pre v-if="pembayaran">{{ pembayaran }}</pre>
                        <pre>{{ cart }}</pre>
                    </div> -->
                </div>
            </div>
            <div class="panel-footer">
                <div class="text-right">
                    -
                </div>
            </div>
        </div>
    </div>
</div>