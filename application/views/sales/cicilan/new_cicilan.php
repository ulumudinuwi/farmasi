<?php echo messages(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading no-padding-bottom ">
                <div class="row">
                    <div class="col-md-6">
                        <input class="form-control" placeholder="Pencarian Invoice">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-user position-left"></i>Biodata Member
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <table class="table borderless t_custom">
                            <tr>
                                <th>No.Member</th>
                                <td>JK-0005270</td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td>Lili Tatamas</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>Perempuan</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>Villa Artha Gading F/36</td>
                            </tr>
                            <tr>
                                <th>Nomor HP</th>
                                <td>6281381766919</td>
                            </tr>
                            <tr>
                                <th>Point</th>
                                <td>200</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class=" icon-magazine position-left"></i>Data Transaksi
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <table class="table borderless t_custom">
                            <tr>
                                <th>Tanggal</th>
                                <td><input class="form-control" value="<?php echo Carbon\Carbon::now()->format('d/m/Y'); ?>"></td>
                            </tr>
                            <tr>
                                <th>Invoice No</th>
                                <td>KW-01.190227.000002</td>
                            </tr>
                            <tr>
                                <td>Sales</td>
                                <td>Boby</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="bold uppercase">Pembayaran Cicilan Ke-3</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-price-tag position-left"></i>Rincian Biaya
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Uraian</th>
                                        <th>Harga</th>
                                        <th>Qty</th>
                                        <th>(%)</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Grow factor serum /CC</td>
                                        <td>1.000.000</td>
                                        <td>3</td>
                                        <td>0%</td>
                                        <td>3.000.000</td>  
                                    </tr>
                                    <tr>
                                        <td>Punyfing Cream</td>
                                        <td>1.500.000</td>
                                        <td>1</td>
                                        <td>0%</td>
                                        <td>382.500</td>  
                                    </tr>
                                    <tr>
                                        <td>Injection - Botox(Unit) (II)</td>
                                        <td>145.000</td>
                                        <td>100</td>
                                        <td>0%</td>
                                        <td>14.500.000</td>  
                                    </tr>
                                    <tr>
                                        <td>Nanode 33G</td>
                                        <td>21.600</td>
                                        <td>2</td>
                                        <td>0%</td>
                                        <td>43.200</td>  
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3">TOTAL</td>
                                        <td colspan="2">
                                            <span class="pull-left">Rp.</span><span class="pull-right">24.500.000</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="bold">Sisa Cicilan</td>
                                        <td colspan="2">
                                            <span class="pull-left">Rp.</span><span class="pull-right">1.500.000</span>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-magazine position-left"></i> Pembayaran Sebelumnya <i class="icon-plus2 pull-right"></i> 
                                </h6>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-credit-card position-left"></i>Metode Pembayaran
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Cara Bayar</th>
                                        <th>Receipt No</th>
                                        <th>Bank</th>
                                        <th>Mesin EDC</th>
                                        <th>Nominal</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Kartu Debit</td>
                                        <td>001</td>
                                        <td>BCA</td>
                                        <td>EDC BCA</td>
                                        <td><span>Rp.1000.000</span></td>
                                        <td class="text-center">
                                            <button class="btn btn-danger"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tunai</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><span>Rp.500.000</span></td>
                                        <td class="text-center">
                                            <!-- <button class="btn btn-danger"><i class="icon-trash"></i></button> -->
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="uppercase bold">TOTAL</td>
                                        <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">1500.000,00</span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                       <td colspan="6">
                                           <button class="btn btn-labeled btn-primary btn-block"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                       </td> 
                                       <!-- <td></td> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3 bold">Uang Diterima</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 bold">Kembalian</label>
                                <div class="col-md-8">
                                    <span class="pull-left">Rp.</span><span class="pull-right">0,00</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-3">
                                    <div class="pull-right">
                                        <button class="btn btn-success btn-labeled" style="margin-right:10px;"><b><i class="icon-floppy-disk"></i></b> Save</button>
                                        <a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>