<form class="form-inline" role="form" method="post" target="_blank" action="site_url('adm_umum/surat_masuk/cetak_list')">
  <div class="form-group">
    <label class="sr-only" for="tgl-surat">Tanggal Surat</label>
    <?php echo form_input('tgl','','class="form-control" id="tgl-surat"'); ?>
  </div>
	<button type="submit" class="btn btn-primary btn-labeled"><b><i class="fa fa-print"></i></b>Cetak</button>
</form>
<div id="print-list-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="print-list-form" class="form-horizontal" method="post" target="_blank">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3 class="modal-title">Cetak</h3>
			</div>

			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label" for="cari">Cari</label>
					<div class="col-md-8">
						<?php echo form_input('tgl','','class="form-control" id="tgl-surat"'); ?>
					</div>
				</div>

			</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-labeled"><b><i class="fa fa-print"></i></b>Cetak</button>
				<button id="print-list-modal-cancel" type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
			</div>
			</form>
		</div>
	</div>
</div>
<script>
$('body').on('click', '[data-button=print-list]', function(e) {
	$('#tgl-surat').daterangepicker({
		locale:{
			format: 'DD/MM/YYYY'
		}
	});
	$('#print-list-form').attr('action', $(this).attr('href'));
	$('#print-list-modal').modal('show');
	e.preventDefault();
});
</script>
