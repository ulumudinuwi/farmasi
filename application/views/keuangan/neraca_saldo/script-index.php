<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var table;
	var tableDetail = $('#table-detail');
	var detailModal = $('#detail-modal');
	$("#detail_total").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	var url = {
        fetchData: "<?php echo site_url('api/keuangan/neraca_saldo/load_data?tanggal_dari=:TANGGAL_DARI&tanggal_sampai=:TANGGAL_SAMPAI'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	function fillForm(link){
		let tbody = $('#table').find('tbody'),
			totalPendapatan = 0,
			totalbeban = 0;
        $.getJSON(link, (res, status) => {
        	tbody.empty();
        	if (res.data) {
        		recursiveParent(res.data.harta, 'Harta');
        		recursiveParent(res.data.kewajiban, 'Kewajiban');
        		recursiveParent(res.data.modal, 'Modal');
        		recursiveParent(res.data.pendapatan, 'Pendapatan');
        		recursiveParent(res.data.beban_atas_pendapatan, 'Beban Atas Pendapatan');
        		recursiveParent(res.data.bebanoperasional, 'Beban Operasional');
        		recursiveParent(res.data.bebannonoperasional, 'Beban Non Operasional');
        		recursiveParent(res.data.pendapatanlainlain, 'Pendapatan Lain - Lain');
        		recursiveParent(res.data.bebanlainlain, 'Beban Lain - Lain');
        	}else{
				tbody.append(`
							<tr>
								<td colspan="8" class="text-bold text-center">Tidak Ada Data</td>
							</tr>
							`);  
        	}
		});
	}

	function recursiveParent(resData, nameParent){
		let tbody = $('#table').find('tbody');
		if (resData.length > 0) {
			tbody.append(`
					<tr>
						<td class="text-center">-</td>
						<td colspan="7" class="text-bold">${nameParent}</td>
					</tr>
					`);  
			recursiveData(resData, tbody, '', '0');
		}
	}
	function recursiveData(data, tbody, parent, kode_sub){
		for (let d of data) {
			if(parent){
	            tbody.append(`
	                <tr>
	                    <td>${kode_sub+' - '+d.kode}</td>
	                    <td><strong><span class="gi">|—</span></strong><strong><span class="gi">|—</span></strong>${d.nama}</td>
	                    <td class="text-right">${formatNumber(d.saldo_awal_debit)}</td>
	                    <td class="text-right">${formatNumber(d.saldo_awal_kredit)}</td>
	                    <td class="text-right">${formatNumber(d.saldo_pergerakan_debit)}</td>
	                    <td class="text-right">${formatNumber(d.saldo_pergerakan_kredit)}</td>
	                    <td class="text-right">${formatNumber(d.saldo_akhir_debit)}</td>
	                    <td class="text-right">${formatNumber(d.saldo_akhir_kredit)}</td>
	                </tr>
	            `);
			}else{
				tbody.append(`
	                <tr>
	                    <td>${d.kode}</td>
	                    <td colspan="7" class="text-bold"><strong><span class="gi">|—</span></strong>${d.nama}</td>
	                </tr>
	            `);
			}
			if(d.sub_parent){
				recursiveData(d.sub_parent, tbody, d.parent_id, d.kode);
			}
        }
	}

    function formatNumber(value) {
        value = numeral(value)._value;

        if (value == 0) return '0';

        if (value < 0) {
            return '(' + numeral(Math.abs(value)).format(',.##') + ')';
        }

        return numeral(value).format(',.##');
    }

	$(window).ready(function() {
		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
					format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_range_tanggal").on('change', function() {
			// Initial
		    var tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
		    var tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
		    var link = url.fetchData.replace(':TANGGAL_DARI', tanggal_dari).replace(':TANGGAL_SAMPAI', tanggal_sampai);

		    fillForm(link);
		});

		// Initial
	    var tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
	    var tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
	    var link = url.fetchData.replace(':TANGGAL_DARI', tanggal_dari).replace(':TANGGAL_SAMPAI', tanggal_sampai);

	    fillForm(link);
	});
</script>