<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Detail Kas Bank</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="row mb-20">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Kas Bank</legend>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-3 control-label">Tanggal</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_tanggal"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Dibayar Oleh</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_dibayar_oleh"></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Nomor</label>
								<div class="col-md-6">
									<div class="form-control-static text-bold" id="detail_kode"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Diterima Oleh</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_diterima_oleh"></div>
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Detail Kas Bank</legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table id="table-detail" class="table table-bordered">
											<thead>
												<tr class="bg-slate">
													<th>Keterangan</th>
													<th class="text-center" style="width: 8%;">Jumlah</th>
												</tr>
											</thead>
											<tbody></tbody>
											<tfoot>
												<tr>
													<td colspan="2">
														<span class=" pull-right">
															Total : 
															<label id="detail_total" class="text-bold "></label> 
														</span>
													</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>