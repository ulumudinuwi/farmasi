<div class="modal fade" id="lookup_tarif_pelayanan_modal" data-counter="0">
    <div class="modal-dialog">
        <div class="modal-content">
		
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Tarif Pelayanan</h6>
			</div>

            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover" id="lookup_tarif_pelayanan_table">
                    <thead>
                        <tr>
							<th>ID</th>
							<th>UID</th>
							<th>Kode</th>
                            <th>Nama</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>