<?php $this->load->view('keuangan/kas_bank/kasir/modal'); ?>
<script type="text/javascript">
	var url_load_lookup_tarif_pelayanan = '<?php echo site_url("api/master/tarif_pelayanan/load_lookup_tarif_pelayanan"); ?>';
	
	var url_view = '<?php echo site_url("kasir/kasir/view"); ?>';
	var url_load_data_pasien = '<?php echo site_url("kasir/kasir/load_data_pasien"); ?>';
	var url_daftar_pasien = '<?php echo site_url("kasir/kasir"); ?>';
	var url_daftar_piutang = '<?php echo site_url("kasir/kasir/daftar_piutang"); ?>';
	var url_daftar_bayar = '<?php echo site_url("kasir/daftar_bayar"); ?>';
	var url_get_perkiraan_bank = '<?php echo site_url("kasir/kasir_ajax_call/get_perkiraan_bank"); ?>';
	var url_get_data = '<?php echo site_url("kasir/kasir_ajax_call/get_data"); ?>';
	var url_simpan = '<?php echo site_url("kasir/kasir_ajax_call/simpan"); ?>';
	var url_hapus = '<?php echo site_url("kasir/kasir_ajax_call/delete"); ?>';
	var uid = '<?php echo $uid; ?>';
	var pelayanan_uid = '<?php echo $uid; ?>';
</script>
<script type="text/javascript" src="<?php echo base_url('assets\js\pages\scripts\keuangan\kas_bank\kasir\edit.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		Kasir.init();
	});
</script>