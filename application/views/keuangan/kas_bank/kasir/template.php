<script type="text/template" id="kasir-detail-template">
<tr>
	<td style="padding:2px;">
		<input type="hidden" id="kasir_detail_id_<%line_no%>" name="kasir_detail_id[]" value="new_kasir_detail_id_<%line_no%>" />
		<input type="hidden" id="tindakan_id_<%line_no%>" name="tindakan_id[]" value="0" />
		<label id="label_kode_<%line_no%>" style="margin-bottom:0;display:none;"></label>
		<div class="input-group" id="kode_section_<%line_no%>">
			<div class="input-cont">
				<input class="form-control" type="text" id="disp_kode_{{line_no}}" value="" />
			</div>
			<span class="input-group-btn">
				<button class="btn btn-info tarif-pelayanan-button" type="button">
					<i class="icon-menu"></i>
				</button>
			</span>
		</div>
	</td>
	<td style="padding:2px;">
		<label id="label_uraian_<%line_no%>" style="margin-bottom:0;display:none;"></label>
		<input class="form-control uraian-row" type="text" id="disp_uraian_<%line_no%>" maxlength="60" autocomplete="off" />
	</td>
	<td style="padding:2px;text-align:right;">
		<input type="hidden" id="harga_satuan_<%line_no%>" name="harga_satuan[]" value="0" />
		<label id="label_harga_satuan_<%line_no%>" style="margin-bottom:0;display:none;"></label>
		<input class="form-control harga-satuan-row" type="text" id="disp_harga_satuan_<%line_no%>" value="0" maxlength="20" autocomplete="off" style="text-align:right;" />
	</td>
	<td style="padding:2px;text-align:right;">
		<input type="hidden" id="quantity_<%line_no%>" name="quantity[]" value="0" />
		<label id="label_quantity_<%line_no%>" style="margin-bottom:0;display:none;">0</label>
		<input class="form-control quantity-row" type="text" id="disp_quantity_<%line_no%>" value="0" maxlength="20" autocomplete="off" style="text-align:right;" />
	</td>
	<td style="padding:2px;text-align:right;">
		<input type="hidden" id="discount_<%line_no%>" name="discount[]" value="0" />
		<label id="label_discount_<%line_no%>" style="margin-bottom:0;display:none;">0</label>
		<input class="form-control discount-row" type="text" id="disp_discount_<%line_no%>" value="0" maxlength="20" autocomplete="off" style="text-align:right;" />
	</td>
	<td style="padding:2px;text-align:right;">
		<input type="hidden" id="jumlah_<%line_no%>" name="jumlah[]" value="0" />
		<label id="label_jumlah_<%line_no%>" style="margin-bottom:0;display:none;">0</label>
		<input class="form-control jumlah-row" type="text" id="disp_jumlah_<%line_no%>" value="0" maxlength="20" autocomplete="off" style="text-align:right;" />
	</td>
	<td style="padding: 2px; text-align: center;">
		<button class="btn btn-link text-primary-600 tambah-simpan-button" type="button" style="padding: 0;"><i class="icon-floppy-disk"></i></button>
		<button class="btn btn-link text-danger-600 tambah-batal-button" type="button" style="padding: 0;"><i class="icon-undo"></i></button>
	</td>
</tr>
</script>