<style>
	.table > thead > tr > th,
	.table > tbody > tr > td, 
	.table > tfoot > tr > td {
        vertical-align: top;
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<fieldset>
							<div class="col-md-12">
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Kas Bank</legend>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Tanggal</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                                                <i class="icon-calendar22"></i>
                                            </span>
                                            <input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal" disabled="true">
                                        </div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Dibayar Oleh</label>
                                    <div class="col-lg-6">
                                            <input type="text" id="dibayar_oleh" name="dibayar_oleh" class="form-control">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Diterima Oleh</label>
                                    <div class="col-lg-6">
                                        <input type="text" id="diterima_oleh" name="diterima_oleh" class="form-control">
                                    </div>
                                </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-4 input-required">Nomor</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="no_pengeluaran" name="no_pengeluaran" placeholder="[Otomatis]" readonly="true">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 input-required">Kas / Bank</label>
									<div class="col-md-6">
										<select name="perkiraan_id" id="perkiraan_id" class="form-control">
											<option value="">[ Pilih Perkiraan Kas/Bank ]</option>
											<?php foreach ($perkiraan_kas_bank_list as $row): ?>
												<option value="<?php echo $row->id ?>"><?php echo $row->nama ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
						<legend class="text-bold"><i class="icon-magazine position-left"></i> Detail Kas Bank</legend>
						<div class="row">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table id="table-detail" class="table table-bordered">
										<thead>
											<tr class="bg-slate">
												<th>Keterangan</th>
												<th>Perkiraan</th>
												<th class="text-center" style="width: 20%;">Jumlah</th>
												<th class="text-center" style="width: 5%;"></th>
											</tr>
										</thead>
										<tbody></tbody>
										<tfoot>
											<tr>
												<td colspan="4">
													<button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah_detail">
														<b><i class="icon-plus-circle2"></i></b>
														Tambah
													</button>
												</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>