<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
			<div class="col-md-6">
                <div class="input-group hide" style="width: 100%";>
                    <input class="form-control" id="disp_tanggal" value="<?php echo date('Y-m-d') ?>">
                    <input type="hidden" id="tanggal_dari" value="<?php echo date('Y-m-01') ?>">
                    <input type="hidden" id="tanggal_sampai" value="<?php echo date('Y-m-t') ?>">
                </div>
                <div class="input-group" style="width: 100%";>
                    <input class="form-control" id="disp_month" value="<?php echo date('M Y') ?>">
                </div>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<tbody>
		</tbody>
	</table>
</div>
<script>
(function () {
	$("select").select2();
	
	var url = {
        fetchData: "<?php echo site_url('api/keuangan/laporan/laporan_001/fetch_data?tanggal_dari=:TANGGAL_DARI&tanggal_sampai=:TANGGAL_SAMPAI'); ?>",
        excel: "<?php echo site_url('api/keuangan/laporan/laporan_001/print_001?tanggal_dari=:TANGGAL_DARI&tanggal_sampai=:TANGGAL_SAMPAI'); ?>",
        pdf: "<?php echo site_url('api/keuangan/laporan/laporan_001/print_001?tanggal_dari=:TANGGAL_DARI&tanggal_sampai=:TANGGAL_SAMPAI'); ?>"
    };

    $("#disp_month").datepicker( {
        format: " MM yyyy", // Notice the Extra space at the beginning
        viewMode: "months", 
        minViewMode: "months",
        autoclose: true
    }).on('changeDate', function(e) {
        var tanggal = moment(e.date);

        $("#tanggal_dari").val(tanggal.startOf('month').format('YYYY-MM-DD'));
        $("#tanggal_sampai").val(tanggal.endOf('month').format('YYYY-MM-DD'));

        var tanggal_dari = $("#tanggal_dari").val().trim();
        var tanggal_sampai = $("#tanggal_sampai").val().trim();
        var link = url.fetchData.replace(':TANGGAL_DARI', tanggal_dari).replace(':TANGGAL_SAMPAI', tanggal_sampai);

        fillForm(link);
    });

    $("#disp_tanggal").daterangepicker({
        startDate: moment("<?php echo date('Y-m-01'); ?>"),
        endDate: moment("<?php echo date('Y-m-t'); ?>"),
        applyClass: "bg-slate-600",
        cancelClass: "btn-default",
        opens: "center",
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY"
        }
    });

    $("#disp_tanggal").on('apply.daterangepicker', function (ev, picker) {
        $("#tanggal_dari").val(picker.startDate.format('YYYY-MM-DD'));
        $("#tanggal_sampai").val(picker.endDate.format('YYYY-MM-DD'));

        var tanggal_dari = $("#tanggal_dari").val().trim();
        var tanggal_sampai = $("#tanggal_sampai").val().trim();
        var link = url.fetchData.replace(':TANGGAL_DARI', tanggal_dari).replace(':TANGGAL_SAMPAI', tanggal_sampai);

        fillForm(link);
    });

    // Initial
    var tanggal_dari = $("#tanggal_dari").val().trim();
    var tanggal_sampai = $("#tanggal_sampai").val().trim();
    var link = url.fetchData.replace(':TANGGAL_DARI', tanggal_dari).replace(':TANGGAL_SAMPAI', tanggal_sampai);

    fillForm(link);

	function fillForm(link){
		let tbody = $('#table').find('tbody'),
			totalPendapatan = 0,
			totalbeban = 0;
        $.getJSON(link, (res, status) => {
        	tbody.empty();
        	if (res.data.length > 0) {
        		if (res.data_pendapatan.length > 0) {
					tbody.append(`
								<tr>
									<td colspan="2" class="text-bold">PENDAPATAN</td>
								</tr>
								`);  

					for (let d of res.data_pendapatan) {
		                tbody.append(`
		                    <tr>
		                        <td>${d.nama}</td>
		                        <td class="text-right">${formatNumber(d.total)}</td>
		                    </tr>
		                `);
		                totalPendapatan += parseFloat(d.total);
		            }  			
	                tbody.append(`
	                    <tr>
	                        <td class="text-right text-bold">TOTAL PENDAPATAN</td>
	                        <td class="text-right text-bold">${formatNumber(totalPendapatan)}</td>
	                    </tr>
	                `);
        		}

        		if (res.data_beban.length > 0) {
					tbody.append(`
								<tr>
									<td colspan="2" class="text-bold">BEBAN</td>
								</tr>
								`);  

					for (let d of res.data_beban) {
		                tbody.append(`
		                    <tr>
		                        <td>${d.nama}</td>
		                        <td class="text-right">${formatNumber(d.total)}</td>
		                    </tr>
		                `);
		                totalbeban += parseFloat(d.total);
		            }  			
	                tbody.append(`
	                    <tr>
	                        <td class="text-right text-bold">TOTAL BEBAN</td>
	                        <td class="text-right text-bold">${formatNumber(totalbeban)}</td>
	                    </tr>
	                `);
        		}
        		totalLabaRugi = totalPendapatan - totalbeban;
                tbody.append(`
                    <tr>
                        <td class="text-right text-bold">TOTAL LABA / RUGI</td>
                        <td class="text-right text-bold">${formatNumber(totalLabaRugi)}</td>
                    </tr>
                `);
        	}else{
				tbody.append(`
							<tr>
								<td colspan="2" class="text-bold text-center">Tidak Ada Data</td>
							</tr>
							`);  
        	}
		});
	}

    function formatNumber(value) {
        value = numeral(value)._value;

        if (value == 0) return '';

        if (value < 0) {
            return '(' + numeral(Math.abs(value)).format(',.##') + ')';
        }

        return numeral(value).format(',.##');
    }

    $("#btn-search_tanggal").click(function () {
        $("#search-tanggal").data('daterangepicker').toggle();
    });

    $("#btn-print-excel").click(function () {
        var tanggal_dari = $("#tanggal_dari").val().trim();
        var tanggal_sampai = $("#tanggal_sampai").val().trim();
    	let param = `?d=excel&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}`;
		window.location.assign(`<?php echo site_url('api/keuangan/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
        var tanggal_dari = $("#tanggal_dari").val().trim();
        var tanggal_sampai = $("#tanggal_sampai").val().trim();
		let param = `?d=pdf&tanggal_dari=${tanggal_dari}&tanggal_sampai=${tanggal_sampai}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/keuangan/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>