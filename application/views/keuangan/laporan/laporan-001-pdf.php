<html>
<body>

<style type="text/css" media="print">
body {
	line-height: 1.2em;
	font-size: 8px;
	font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
	font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
	margin-top: 0;
	margin-bottom: 5px;
	text-align: center;
}
h1 {
	font-size: 24px;
}
h2 {
	font-size: 16px;
}
h3 {
	font-size: 14px;
}
h4 {
	font-size: 12px;
}
h5 {
	font-size: 10px;
}
h6 {
	font-size: 8px;
}
table {
	border-collapse: collapse;
	font-size: 8px;
}
.table {
    border-spacing: 0;
	width: 100%;
	border: 1px solid #555;
	font-size: 10px;
}
.table thead th,
.table tbody td {
	border: 1px solid #555;
	vertical-align: middle;
	padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
	color: #fff;
	background-color: #607D8B;
	font-weight: bold;
	text-align: center;
}
.text-bold{
	font-weight: bold;
}
.text-right{
	text-align:right;
}
.text-center{
	text-align:center;
}
.footer_current_date_user {
	text-align: right;
	color: #d10404;
	font-size: 8px;
	vertical-align: top;
	margin-top: 10px;
}
</style>
<h3 class="text-center">LAPORAN LABA / RUGI</h3>
<h4 class="text-center">TANGGAL: <?php echo strtoupper($periode_date); ?></h4>
<br>
<table class="table table-bordered table-striped">
	<tbody>
		<?php  
		$totalPendapatan = 0;
		$totalBeban = 0;
		$totalLabaRugi = 0;

		if($total_rows > 0):
		?>
		<?php if(count($rows['data_pendapatan']) > 0): ?> 
			<tr>
				<td colspan="2" class="text-bold">PENDAPATAN</td>
			</tr>
			<?php foreach ($rows['data_pendapatan'] as $row): ?>
				<tr>
					<td><?php echo $row->nama ?></td>
					<td class="text-right">Rp. <?php echo number_format($row->total,'2',',','.') ?></td>
				</tr>
			<?php 
				$totalPendapatan += $row->total;
				endforeach 
			?>
			<tr>
				<td class="text-bold text-right">TOTAL PENDAPATAN</td>
				<td class="text-bold text-right">Rp. <?php echo number_format($totalPendapatan,'2',',','.') ?></td>
			</tr>
		<?php endif; ?>

		<?php if(count($rows['data_beban']) > 0): ?> 
			<tr>
				<td colspan="2" class="text-bold">BEBAN</td>
			</tr>
			<?php foreach ($rows['data_beban'] as $row): ?>
				<tr>
					<td><?php echo $row->nama ?></td>
					<td class="text-right">Rp. <?php echo number_format($row->total,'2',',','.') ?></td>
				</tr>
			<?php 
				$totalBeban += $row->total;
				$totalLabaRugi = $totalPendapatan - $totalBeban;
				endforeach 
			?>
			<tr>
				<td class="text-bold text-right">TOTAL BEBAN</td>
				<td class="text-bold text-right">Rp. <?php echo number_format($totalBeban,'2',',','.') ?></td>
			</tr>
			<tr>
				<td class="text-bold text-right">TOTAL LABA RUGI</td>
				<td class="text-bold text-right">Rp. <?php echo number_format($totalLabaRugi,'2',',','.') ?></td>
			</tr>
		<?php endif; ?>
		<?php 
		else: 
		?>	
			<tr>
				<td colspan="2" class="text-bold text-center">Tidak Ada Data</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">Tangerang , <?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo strtoupper($current_user); ?></td>
    </tr>
</table>
</body>
</html>