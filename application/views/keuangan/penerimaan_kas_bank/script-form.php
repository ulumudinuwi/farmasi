<script>
	$('select').select2();
var UID = "<?php echo $uid; ?>",
	form = '#form';

var url = {
	index: "<?php echo site_url('keuangan/kas_bank/penerimaan_kas_bank'); ?>",
	save: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/save'); ?>",
	getData: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/get_data/:UID'); ?>",
	loadDataBarang: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/load_data_modal'); ?>",
	getHistoryBarang: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/load_data?uid=:UID'); ?>",
	loadDetailStock: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/load_detail_stock?uid=:UID'); ?>",
	getPerkiraan: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/get_perkiraan'); ?>",
	getDibayarOleh: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/get_dibayar_oleh'); ?>",
	getDiterimaOleh: "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/get_diterima_oleh'); ?>",
};

let tableDetail = $('#table-detail'),
	tableDetailDt,
	curRowBarang = null;

/* variabel di modal */
var tableBarang = $('#table-barang'),
	tableBarangDt,
	modalBarang = $('#list-barang-modal');

function fillElement(obj, element) {
	let parent = element.parent();
	parent.find('.loading-select').show();
	$.getJSON(obj.url, function(data, status) {
		if (status === 'success') {
			var option = '';
			option += '<option value="" selected="selected">- Pilih -</option>';
			for (var i = 0; i < data.list.length; i++) {
				let selected = ""
				if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';

				let value_name = data.list[i].nama; 
				option += '<option value="' + data.list[i].id + '" ' + selected + '>' + value_name + '</option>';
			}
			element.html(option).trigger("change");
		}
		parent.find('.loading-select').hide();
	});
	$('select').select2();
}

function fillForm(uid, data) {
	if(parseInt(UID) === 0) return;

	blockElement($(form));
	$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
		if (status === 'success') {
			data = data.data;
			$("#id").val(data.id);
			$("#uid").val(data.uid);
			$("#tanggal").val(data.tanggal);
			$("#dibayar_oleh").val(data.dibayar_oleh);
			$("#diterima_oleh").val(data.diterima_oleh);
			$("#no_pengeluaran").val(data.no_pengeluaran);
			$("#perkiraan_id").val(data.perkiraan_id).trigger('change');

			tableDetail.find('tbody').empty();
			if(data.details.length <= 0) {
				addDetail({
					id: 0,
					keterangan: '',
					perkiraan: '',
					jumlah: 1,
					delete: 1,
				});
			} else {
				for (var i = 0, n = data.details.length; i < n; i++) {
					addDetail(data.details[i]);
				}
			}
			$(form).unblock();
		}
	});
}

function addDetail(data) {
	data = data || {};

	var tbody = tableDetail.find('tbody');

	var count = $('[name="detail_id[]"]').length;

	var ID = (count + 1);

	var tr = $("<tr/>")
	.prop('id', 'row-cb-' + ID)
	.appendTo(tbody);

	var tdKeterangan = $("<td/>")
	.appendTo(tr);
	var inputId = $("<input/>")
	.addClass('cb-input_id')
	.prop('type', 'hidden')
	.prop('name', 'detail_id[]')
	.val(data.id)
	.appendTo(tdKeterangan);
	var inputKeterangan = $("<textarea/>")
	.prop('rows', '2')
	.prop('name', 'keterangan[]')
	.prop('type', 'text')
	.addClass('form-control')
	.val(data.keterangan)
	.appendTo(tdKeterangan);

	var tdPerkiraan = $("<td/>")
	.appendTo(tr);
	var inputPerkiraan = $("<select/>")
	.prop('name', 'perkiraan[]')
	.addClass('form-control')
	.appendTo(tdPerkiraan);

	var obj = {
		value: data.perkiraan,
		url: url.getPerkiraan
	};
	fillElement(obj, inputPerkiraan);

	var tdJumlah = $("<td/>")
	.appendTo(tr);
	var inputJumlah = $("<input/>")
	.prop('name', 'jumlah[]')
	.prop('type', 'text')
	.addClass('form-control input-bulat')
	.appendTo(tdJumlah);

	inputJumlah.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	inputJumlah.autoNumeric('set', data.jumlah);

	var tdAction = $("<td/>")
	.addClass('text-center')
	.appendTo(tr);
	var btnDel = $("<a/>")
	.addClass('delete-row')
	.html('<b><i class="fa fa-trash-o"></i></b>')
	.appendTo(tdAction);
	
	btnDel.on('click', function () {
		tr.remove();
		$('#btn-tambah_detail, .btn-save').attr('disabled', false);
	});
}

$(document).ready(function() {
	$("#btn-tambah_detail").click(function () {
		addDetail({
			id: 0,
			keterangan: '',
			perkiraan: '',
			jumlah: 0,
		});
	});

	$(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
	$(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	$(".wysihtml5-min").wysihtml5({
		parserRules:  wysihtml5ParserRules
	});
	$(".wysihtml5-toolbar").remove();

	$('.btn-save').on('click', function(e) {
		e.preventDefault();
		$(form).submit();
	});

	$(".btn-batal").click(function() {
		window.location.assign(url.index);
	});

	$(".disp_tanggal").daterangepicker({
		singleDatePicker: true,
		startDate: moment("<?php echo date('Y-m-d'); ?>"),
		endDate: moment("<?php echo date('Y-m-d'); ?>"),
		applyClass: "bg-slate-600",
		cancelClass: "btn-default",
		opens: "center",
		autoApply: true,
		locale: {
			format: "DD/MM/YYYY"
		}
	});

	$("#btn_tanggal").click(function () {
		let parent = $(this).parent();
		parent.find('input').data("daterangepicker").toggle();
	});

	$("#btn-tambah").click(function (e) {
		curRowBarang = null;
		tableBarangDt.draw(false);
		modalBarang.modal('show');
	});

	$(form).validate({
		rules: {
			diterima_oleh: { required: true },
			tanggal: { required: true },
			uraian: { required: true },
		},
		focusInvalid: true,
		errorPlacement: function(error, element) {
				var placement = $(element).closest('.input-group');
				if (placement.length > 0) {
						error.insertAfter(placement);
				} else {
						error.insertAfter($(element));
				}
		},
		submitHandler: function (form) {
			swal({
				title: "Konfirmasi?",
				type: "warning",
				text: "Apakah data yang dimasukan benar??",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#2196F3",
				cancelButtonText: "Batal",
				cancelButtonColor: "#FAFAFA",
				closeOnConfirm: true,
				showLoaderOnConfirm: true,
			},
			function() {
				$('.input-decimal').each(function() {
					$(this).val($(this).autoNumeric('get'));
				});

				$('.input-bulat').each(function() {
					$(this).val($(this).autoNumeric('get'));
				});

				$('input, textarea, select').prop('disabled', false);

				blockPage('Sedang diproses ...');
				var formData = $(form).serialize();
				$.ajax({
					data: formData,
					type: 'POST',
					dataType: 'JSON', 
					url: url.save,
					success: function(data){
						$.unblockUI();
						successMessage('Berhasil', "Kas Keluar berhasil disimpan.");
						window.location.assign(url.index);
					},
					error: function(data){
						$.unblockUI();
						errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
					}
				});
				return false;
			});
		}
	});

    $( "#dibayar_oleh" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: url.getDibayarOleh,
          dataType: "json",
          data: {
            q: request.term
          },
          success: function( data ) {
            response($.map(data.list, function (item) {
	            return {
	                label: item.dibayar_oleh,
	                value: item.dibayar_oleh
	            };
	        }));
          }
        });
      },
      minLength: 1,
    });

    $( "#diterima_oleh" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: url.getDiterimaOleh,
          dataType: "json",
          data: {
            q: request.term
          },
          success: function( data ) {
            response($.map(data.list, function (item) {
	            return {
	                label: item.diterima_oleh,
	                value: item.diterima_oleh
	            };
	        }));
          }
        });
      },
      minLength: 1,
    });

	fillForm(UID);
});
</script>