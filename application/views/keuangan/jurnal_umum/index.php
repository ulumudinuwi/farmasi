<?php echo messages(); ?>
<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
		vertical-align: top;
	}
</style>
<div class="panel panel flat">
	<div class="panel-body form-horizontal">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon cursor-pointer" id="btn_search_tanggal">
								<i class="icon-calendar22"></i>
							</span>
							<input type="text" id="search_range_tanggal" class="form-control rangetanggal-form">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr class="no-margin-bottom no-margin-top">
	<div class="panel-body no-padding-top">
		<div class="table-responsive">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr class="bg-slate">
						<th>Tanggal</th>
						<th>Keterangan</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->load->view('keuangan/jurnal_umum/detail-modal'); ?>