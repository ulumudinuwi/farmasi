<script>
var UID = "<?php echo $uid; ?>",
    MODE = "<?php echo $mode; ?>",
    form = '#form';

var url = {
  index: "<?php echo site_url('logistik/pengeluaran'); ?>",
  save: "<?php echo site_url('api/logistik/pengeluaran/save'); ?>",
  getDataPermintaan: "<?php echo site_url('api/request/unit_kerja/permintaan/get_data/:UID?mode=sent'); ?>",
  getData: "<?php echo site_url('api/logistik/pengeluaran/get_data/:UID'); ?>",
  getHistoryBarang: "<?php echo site_url('api/logistik/history/load_data?uid=:UID'); ?>",
};

var tableDetail = $('#table_detail'),
    tableDetailDt;

function fillForm(uid) {
  tableDetailDt = tableDetail.DataTable({
    "info": false,
    "ordering": false,
    "paginate": false,
    "drawCallback": function (settings) {
      let tr = tableDetail.find("tbody tr");
      tr.each(function() {
        let stock = parseFloat(numeral($(this).find('.label-disp_stock').html())._value);
        let td = $(this).children().eq(3);
        td.removeClass('bg-danger');
        if(stock <= 0) td.addClass('bg-danger');
        $(this).find('.disp_qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2', vMax: stock, vMin: 0});
        $(this).find('.disp_qty').trigger('change');
      });
    },
  });

  let urlForm = url.getData;
  if(MODE === "add") {
    urlForm = url.getDataPermintaan;
  }

  blockElement($(form));
  $.getJSON(urlForm.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;
      
      if(MODE === "add") {
        $("#permintaan_id").val(data.id);
        $("#label_kode_permintaan").html(data.kode);
        $("#keterangan_permintaan").data("wysihtml5").editor.setValue(data.keterangan);
      } else {
        $("#id").val(data.id);
        $("#uid").val(data.uid);
        $("#permintaan_id").val(data.permintaan_id);
        $("#kode").val(data.kode);
        $("#label_kode_permintaan").html(data.kode_permintaan);
        $("#keterangan_permintaan").data("wysihtml5").editor.setValue(data.keterangan_permintaan);
        $("#keterangan").data("wysihtml5").editor.setValue(data.keterangan);
      }

      $("#pemohon").val(data.pemohon);
      $("#label_pemohon").html(data.pemohon);
      $("#unitkerja_id").val(data.unitkerja_id).trigger('change');
      $("#sifat").val(data.sifat).trigger("change");
      
      for (var i = 0; i < data.details.length; i++) {
        columnBarang(data.details[i]);
      }
      $(form).unblock();
    }
  });
}

function columnBarang(data) {
  tableDetailDt.row.add([
    // Col 1
    `<input type="hidden" class="input-detail_id" name="detail_id[]" value="${data.id}">` +
    `<input type="hidden" class="input-permintaan_detail_id" name="permintaan_detail_id[]" value="${data.permintaan_detail_id}">` +
    `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
    `${data.kode_barang}` +
    `<br/><span class="text-slate-300 text-bold text-size-mini label-disp_barang mt-10">${data.barang}</span>` +
    `<br/><a class="label label-info history-row" data-barang_id="${btoa(JSON.stringify(data.barang_id))}">History</a>`,
    // Col 2
    `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
    `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
    `<label class="label-satuan">` +
    `${data.satuan}` +
    `<br/><span class="text-info-300 text-size-mini mt-10">Sisa Stock: <span class="text-bold label-disp_stock">${numeral(data.stock_gudang).format('0.0,')}</span></span>` +
    `</label>`,
    // Col 3
    `<input type="hidden" class="input-qty_order" name="qty_order[]" value="${data.qty_order}">` +
    `<label class="label-qty_order">${numeral(data.qty_order).format('0.0,')}</label>`,
    // Col 4
    `<input type="hidden" class="tmp-sisa" value="${data.sisa}">` +
    `<input type="hidden" class="input-sisa" name="sisa[]" value="${data.sisa}">` +
    `<label class="label-sisa">${numeral(data.sisa).format('0.0,')}</label>`,
    // Col 5
    `<input type="hidden" class="input-old_qty" name="old_qty[]" value="${data.qty}">` +
    `<input type="hidden" class="input-qty" name="qty[]" value="${data.qty}">` +
    `<input class="form-control disp_qty text-right" value="${data.qty}">`,
  ]).draw(false);
}

$(document).ready(function() {
  $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
  $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

  $(".wysihtml5-min").wysihtml5({
    parserRules:  wysihtml5ParserRules
  });
  $(".wysihtml5-toolbar").remove();

  $('.btn-save').on('click', function(e) {
    e.preventDefault();
    $(form).submit();
  });

  $(".btn-batal").click(function() {
    window.location.assign(url.index);
  });

  $(".disp_tanggal").daterangepicker({
    singleDatePicker: true,
    startDate: moment("<?php echo date('Y-m-d'); ?>"),
    endDate: moment("<?php echo date('Y-m-d'); ?>"),
    applyClass: "bg-slate-600",
    cancelClass: "btn-default",
    opens: "center",
    autoApply: true,
    locale: {
      format: "DD/MM/YYYY"
    }
  });

  $("#btn_tanggal").click(function () {
    let parent = $(this).parent();
    parent.find('input').data("daterangepicker").toggle();
  });
  
  /* EVENTS TABLE DETAIL */
  tableDetail.on('click', '.history-row', function() {
    let barang_id = $(this).data('barang_id');
    showHistoryBarang(url.getHistoryBarang.replace(':UID', barang_id), null);
  });
  
  tableDetail.on('change blur keyup', '.disp_qty', function() {
    let tr = $(this).parent().parent();
    let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));

    let sisa = parseFloat(tr.find('.tmp-sisa').val());
    sisa = sisa - val;
    if(sisa < 0) sisa = 0;

    tr.find('.label-sisa').html(numeral(sisa).format('0.0,'));
    tr.find('.input-sisa').val(sisa);
    tr.find('.input-qty').val(val);
  });

  $(form).validate({
    rules: {
      tanggal: { required: true },
      sifat: { required: true },
      unitkerja_id: { required: true },
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var placement = $(element).closest('.input-group');
        if (placement.length > 0) {
            error.insertAfter(placement);
        } else {
            error.insertAfter($(element));
        }
    },
    submitHandler: function (form) {
      tableDetailDt.search('').draw(false);
      swal({
        title: "Konfirmasi?",
        type: "warning",
        text: "Apakah data yang dimasukan benar??",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#2196F3",
        cancelButtonText: "Batal",
        cancelButtonColor: "#FAFAFA",
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
      },
      function() {
        $('.input-decimal').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('.input-bulat').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('input, textarea, select').prop('disabled', false);

        blockPage('Sedang diproses ...');
        var formData = $(form).serialize();
        $.ajax({
          data: formData,
          type: 'POST',
          dataType: 'JSON', 
          url: url.save,
          success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "Pengeluaran berhasil disimpan.");
              window.location.assign(url.index);
          },
          error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
          }
        });
        return false;
      });
    }
  });

  fillForm(UID);
});
</script>