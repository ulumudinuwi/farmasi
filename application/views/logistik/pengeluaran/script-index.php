<script type="text/javascript">
  var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
  var tableHistory, table;
  var detailModal = $('#detail-modal');
  
  var url = {
    loadDataHistory : "<?php echo site_url('api/logistik/pengeluaran/load_data'); ?>",
    loadData : "<?php echo site_url('api/logistik/pengeluaran/load_data_permintaan'); ?>",
    getData : "<?php echo site_url('api/logistik/pengeluaran/get_data/:UID'); ?>",
    cetak : "<?php echo site_url('api/logistik/pengeluaran/cetak/:UID'); ?>",
    form : "<?php echo site_url('logistik/pengeluaran/form/:UID'); ?>",
    listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID&modul=logistik'); ?>",
  };

  function subsDate(range, tipe) {
    let date = range.substr(0, 10);
    if(tipe === "sampai") date = range.substr(13, 10);
    return getDate(date);
  }

  function listen(q, browse) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('logistik-' + q, function(e) {
        switch(browse) {
          case 2:
            tableHistory.draw(false);
            break;
          default:
            table.draw(false);
            break;
        }
    }, false);
  }

  function showDetail(uid) {
    $.getJSON(url.getData.replace(':UID', uid), function (data, status) {
      if (status === 'success') {
        data = data.data;

        detailModal.find(".detail_kode").html(data.kode);
        detailModal.find(".detail_kode_permintaan").html(data.kode_permintaan);
        detailModal.find(".detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
        detailModal.find(".detail_pemohon").html(data.label_pemohon);
        detailModal.find(".detail_sifat").html(data.sifat_desc);
        detailModal.find(".detail_status").html(data.status_desc);
        detailModal.find(".detail_keterangan_permintaan").html(data.keterangan_permintaan);
        detailModal.find(".detail_keterangan_pengeluaran").html(data.keterangan);
        
        var isDTable = $.fn.dataTable.isDataTable(detailModal.find('.table-detail'));
        if(isDTable === true) detailModal.find('.table-detail').DataTable().destroy();

        detailModal.find('.table-detail').DataTable({
            "ordering": false,
            "processing": true,
            "aaData": data.details,
            "columns": [
              { 
                "data": "barang",
                "render": function (data, type, row, meta) {
                    let tmp = data;
                    tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
                    return tmp;
                },
              },
              { "data": "satuan" },
              { 
                  "data": "qty_order",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "qty",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
            ],
        });
        detailModal.modal('show');
      }
    });
  }

  function handleLoadTable(browse) {
    switch(browse) {
      case 'table_history':
        tableHistory = $("#table-history").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadDataHistory,
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate($("#search_history_range_tanggal").val(), 'dari');
                  p.tanggal_sampai = subsDate($("#search_history_range_tanggal").val(), 'sampai');
                  p.unit_id = $('#search_history_unit').val();
                  p.sifat = $('#search_history_sifat').val();
                  p.status = $('#search_history_status').val();
              }
          },
          "order": [2, "desc"],
          "columns": [
            { 
              "data": "kode",
              "render": function (data, type, row, meta) {
                  let tmp = '<a class="show-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + data + '</a>';
                  tmp += `<br/><a href="${url.cetak.replace(':UID', row.uid)}" target="_blank" class="label label-info"><i class="fa fa-print"></i> Cetak</a>`;
                  return tmp;
              },
            },
            { 
              "data": "kode_permintaan",
              "orderable": false,
            },
            {
              "data": "tanggal",
              "searchable": false,
              "render": function (data, type, row, meta) {
                let tmp = moment(data).format('DD-MM-YYYY HH:mm');
                tmp += `<br/><span class="text-size-mini text-info"><b>Dikirim Oleh:</b><br/> ${row.diproses_by}</span>`;;
                return tmp;
              },
            },
            { 
              "data": "sifat",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "status_desc",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "pemohon",
              "orderable": false,
              "render": function (data, type, row, meta) {
                  let tmp = data;
                  tmp += `<br/><span class="text-size-mini text-info"><b>Unit:</b><br/> ${row.unit_kerja}</span>`;
                  return tmp;
              },
            },
          ],
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });
        break;
      default:
        table = $("#table").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadData,
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
                  p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
                  p.sifat = $('#search_sifat').val();
                  p.unit_id = $('#search_unit').val();
                  p.status = $('#search_status').val();
              }
          },
          "order": [1, "asc"],
          "columns": [
            { 
              "data": "kode",
              "render": function (data, type, row, meta) {
                  let tmp = '<a class="kirim-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Proses Pengiriman" data-placement="top">' + data + '</a>';
                  return tmp;
              },
            },
            {
              "data": "tanggal",
              "render": function (data, type, row, meta) {
                let tmp = moment(data).format('DD-MM-YYYY HH:mm');
                return tmp;
              },
              "searchable": false,
            },
            { 
              "data": "sifat",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "status_desc",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "pemohon",
              "orderable": false,
              "render": function (data, type, row, meta) {
                  let tmp = data;
                  tmp += `<br/><span class="text-size-mini text-info"><b>Unit:</b><br/> ${row.unit_kerja}</span>`;
                  return tmp;
              },
            },
          ],
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });

        var isDTable = $.fn.dataTable.isDataTable($('#table-history'));
        if(isDTable === false) handleLoadTable('table_history');
        break;
      }
    }

  $(window).ready(function() {

    $(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(tanggalDari),
        endDate: moment(tanggalSampai),
    });
    handleLoadTable('table');

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-1':
            table.draw(false);
            break;
          default:
            tableHistory.draw(false);
            break;
        }
    });

    $("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn_search_tanggal").click(function () {
        $("#search_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_range_tanggal, #search_sifat, #search_unit, #search_status").on('change', function() {
      table.draw();
    });

    $('#table').on('click', '.kirim-row', function() {
      let uid = $(this).data('uid');
      blockPage('Form Pengiriman sedang diproses ...');
      setTimeout(function() { 
        window.location.assign(url.form.replace(':UID', uid));         
      }, 1000);
    });

    $("#search_history_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
        tableHistory.draw();
    });

    $("#btn_search_history_tanggal").click(function () {
        $("#search_history_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_history_range_tanggal, #search_history_sifat, #search_history_unit, #search_history_status").on('change', function() {
      tableHistory.draw();
    });

    $("#table-history").on("click", ".show-row", function () {
      let uid = $(this).data('uid');
      showDetail(uid);
    });

    listen("<?php echo $tab1Uid; ?>", 1);
    listen("<?php echo $tab2Uid; ?>", 2);
  });
</script>