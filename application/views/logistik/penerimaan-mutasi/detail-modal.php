<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h6 class="modal-title">Detail Penerimaan Mutasi</h6>
      </div>
      <div class="modal-body form-horizontal">
        <div class="row mb-20">
          <fieldset>
            <div class="col-md-12">
              <legend class="text-bold"><i class="icon-magazine position-left"></i> Data Penerimaan Mutasi</legend>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-lg-4 control-label">Nomor</label>
                <div class="col-md-6">
                  <div class="form-control-static text-bold detail_kode"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">No. Mutasi</label>
                <div class="col-md-6">
                  <div class="form-control-static text-bold detail_kode_mutasi"></div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-lg-4 control-label">Tanggal</label>
                <div class="col-lg-6">
                  <div class="form-control-static text-bold detail_tanggal"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">Pemohon</label>
                <div class="col-lg-6">
                  <div class="form-control-static text-bold detail_pemohon"></div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="row mb-20">
          <div class="col-sm-12">
            <fieldset>
              <legend class="text-bold">
                <i class="icon-list"></i> <strong>Daftar Barang</strong>
              </legend>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-detail">
                      <thead>
                        <tr class="bg-slate">
                          <th>Nama</th>
                          <th>Satuan</th>
                          <th>Qty</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                  <div class="row mt-10">
                    <div class="col-md-8">
                      <label>Keterangan</label>
                      <div>
                        <div class="form-control-static text-bold detail_keterangan">&mdash;</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
      <div class="modal-footer m-t-none">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>