<script type="text/javascript">
  var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
  var tableDraf, table;
  var tableDetail = $('#table_detail');
  var btnRefresh = $('#btn-refresh');
  var detailModal = $('#detail-modal');
  var url = {
    loadDataDraf : "<?php echo site_url('api/logistik/po/load_data_draft'); ?>",
    loadData : "<?php echo site_url('api/logistik/po/load_data'); ?>",
    getData : "<?php echo site_url('api/logistik/po/get_data/:UID'); ?>",
    edit : "<?php echo site_url('logistik/po/edit/:UID'); ?>",
    pengalihan : "<?php echo site_url('logistik/po/pengalihan/:UID'); ?>",
    actExe: "<?php echo site_url('api/logistik/approval/act_exe'); ?>",
    cetakPO : "<?php echo site_url('api/logistik/po/cetak/:UID'); ?>",
    listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID'); ?>",
  };

  function listen(q, browse) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('logistik-' + q, function(e) {
        switch(browse) {
          case 2:
            table.draw(false);
            break;
          default:
            tableDraf.draw(false);
            break;
        }
    }, false);
  }

  function showDetail(uid) {
    $.getJSON(url.getData.replace(':UID', uid), function (data, status) {
      if (status === 'success') {
        data = data.data;

        $("#detail_kode").html(data.kode);
        $("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
        $("#detail_sifat").html(data.sifat_desc);
        $("#detail_pabrik").html(data.pabrik);
        $("#detail_vendor").html(data.vendor);
        $("#detail_status").html(data.status_desc);

        $("#detail_keterangan").html("");
        if(parseInt(data.status) === 6 || parseInt(data.status) === 7) { // Ditolak, Dialihkan
          let keterangan = data.keterangan ? `<b>Alasan</b>:<br/>${data.keterangan}` : "";
          $("#detail_keterangan").html(keterangan);
        }

        var isDTable = $.fn.dataTable.isDataTable(tableDetail);
        if(isDTable === true) tableDetail.DataTable().destroy();

        tableDetail.DataTable({
            "ordering": false,
            "processing": true,
            "aaData": data.details,
            "columns": [
              { 
                "data": "barang",
                "render": function (data, type, row, meta) {
                    let tmp = data;
                    tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
                    return tmp;
                },
              },
              { "data": "satuan_po" },
              { 
                  "data": "minimum",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "stock",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "maximum",
                  "render": function (data, type, row, meta) {
                      return `<span class="label-maximum">${numeral(data).format('0.0,')}</span>`;
                  },
                  "className": "text-right"
              },
              { 
                  "data": "qty",
                  "render": function (data, type, row, meta) {
                      return `<span class="label-qty">${numeral(data).format('0.0,')}</span>`;
                  },
                  "className": "text-right"
              },
              {
                  "data": "harga",
                  "render": function (data, type, row, meta) {
                      return 'Rp. ' + numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              {
                  "data": "total",
                  "render": function (data, type, row, meta) {
                      return 'Rp.' + numeral(row.total).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "diterima",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
            ],
            "drawCallback": function (settings) {
              let tr = tableDetail.find("tbody tr");
              tr.each(function() {
                let qty = isNaN(parseFloat(numeral($(this).find('.label-qty').html())._value)) ? 0 : parseFloat(numeral($(this).find('.label-qty').html())._value);
                let maximum = parseFloat(numeral($(this).find('.label-maximum').html())._value);
                let td = $(this).children().eq(5);
                td.removeClass('bg-danger');
                if(qty > maximum) {
                  td.addClass('bg-danger');
                }
              });
            },
        });

        $('#detail_total').html('Rp. ' + numeral(data.total).format('0.0,'));
        $('#detail_total_ppn').html('Rp. ' + numeral(data.total_ppn).format('0.0,'));
        $('#detail_grand_total').html('Rp. ' + numeral(data.grand_total).format('0.0,'));
        detailModal.modal('show');
      }
    });
  }

  function handleLoadTable(browse) {
    switch(browse) {
      case 'table_draft':
        tableDraf = $("#table-draft").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadDataDraf,
              "type": "POST",
              "data": function(p) {
                  p.status = $('#search_draft_status').val();
              }
          },
          "columns": [
            { "data": "pabrik" },
            { "data": "vendor" },
            {
              "data": "pabrik_id",
              "orderable": false,
              "searchable": false,
              "render": function (data, type, row, meta) {
                let tmp = `<a class="btn btn-primary btn-labeled draft-row" data-pabrik_id="${data}" data-vendor_id="${row.vendor_id}" data-toggle="tooltip" title="Buat Draft" data-placement="top"><b><i class="icon-plus-circle2"></i></b>Buat Draft</a>`;
                return tmp;
              },
              "className": "text-center"
            },
          ],
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });

        var isDTable = $.fn.dataTable.isDataTable($('#table'));
        if(isDTable === false) handleLoadTable('table');
        break;
      default:
        table = $("#table").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadData,
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = tanggalDari;
                  p.tanggal_sampai = tanggalSampai;
                  p.sifat = $('#search_sifat').val();
                  p.pabrik_id = $('#search_pabrik').val();
                  p.status = $('#search_status').val();
              }
          },
          "order": [1, "desc"],
          "columns": [
            { 
              "data": "kode",
              "render": function (data, type, row, meta) {
                  let tmp = '<a class="show-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + data + '</a>';
                  return tmp;
              },
            },
            {
              "data": "tanggal",
              "render": function (data, type, row, meta) {
                let tmp = moment(data).format('DD-MM-YYYY HH:mm');
                tmp += `<br/><span class="text-size-mini text-info"><b>Pengajuan Oleh:</b><br/> ${row.pengajuan_by}</span>`;
                return tmp;
              },
              "searchable": false,
            },
            { 
              "data": "pabrik",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "vendor",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "sifat",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "status_desc",
              "orderable": false,
              "searchable": false,
              "render": function (data, type, row, meta) {
                let tmp = data;
                tmp += `<br/><span class="text-size-mini text-info"><b>Diproses Oleh:</b><br/> ${row.diubah_by}</span>`;
                if(parseInt(row.status) === 6) { // Ditolak
                  tmp += `<br/><span class="text-size-mini text-info"><b>Alasan: </b><br/> ${row.keterangan}</span>`;
                }
                return tmp;
              }
            },
            {
              "data": "status",
              "orderable": false,
              "searchable": false,
              "render": function (data, type, row, meta) {
                let tmp = `<a href="${url.cetakPO.replace(':UID', row.uid)}" target="_blank" class="btn btn-info btn-xs" data-toggle="tooltip" title="Cetak"><i class="fa fa-print"></i></a>`;
                switch(parseInt(data)) {
                  case 1:
                    tmp += '&nbsp;<a href="' + url.edit.replace(':UID', row.uid) + '" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" data-placement="top"><i class="fa fa-edit"></i></a>';
                    break;
                  case 2:
                    tmp += `&nbsp;<a target="_blank" class="btn btn-warning btn-xs sanggup-row" data-toggle="tooltip" data-title="Kesanggupan Distributor" data-uid="${row.uid}"><i class="icon-checkmark-circle"></i></a>`;
                    break;
                  case 3:
                  case 4:
                    tmp += `&nbsp;<a target="_blank" class="btn bg-brown btn-xs alihkan-row" data-toggle="tooltip" data-title="Alihkan" data-uid="${row.uid}"><i class="icon-direction"></i></a>`;
                    break;
                }
                return tmp;
              },
              "className": "text-center"
            },
          ],
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });
        break;
      }
    }

  $(window).ready(function() {
    handleLoadTable('table_draft');

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-1':
            tableDraf.draw(false);
            break;
          default:
            table.draw(false);
            break;
        }
    });

    $("#search_draft_status").on('change', function() {
      tableDraf.draw();
    });

    $('#table-draft').on('click', '.draft-row', function() {
      let obj = {
          pabrik_id: $(this).data('pabrik_id'),
          vendor_id: $(this).data('vendor_id'),
      };
      let data = btoa(JSON.stringify(obj));
      blockPage('Form Draft PO sedang diproses ...');
      setTimeout(function() { 
        window.location.assign("<?php echo site_url('logistik/po/add'); ?>?data=" + data);         
      }, 1000);
    })
    $(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(tanggalDari),
        endDate: moment(tanggalSampai),
    });

    $("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
        tanggalDari = picker.startDate.format('YYYY-MM-DD');
        tanggalSampai = picker.endDate.format('YYYY-MM-DD');       

        table.draw();
    });

    $("#search_range_tanggal").on('change', function () {
        range = $("#search_range_tanggal").val();
        var date = range.substr(0, 10);
        tanggalDari = getDate(date); 

        date = range.substr(13, 10);
        tanggalSampai = getDate(date); 

        table.draw();
    });

    $("#btn_search_tanggal").click(function () {
        $("#search_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_sifat, #search_pabrik, #search_status").on('change', function() {
      table.draw();
    });

    $("#table").on("click", ".edit-row", function () {
      let uid = $(this).data('uid');
    });

    $("#table").on("click", ".show-row", function () {
      let uid = $(this).data('uid');
      showDetail(uid);
    });

    $("#table").on("click", ".sanggup-row", function () {
      let uid = $(this).data('uid');
      swal({
        title: "Kesanggupan <?php echo $this->lang->line("vendor_label"); ?> ?",
        text: "Jika tidak sanggup, maka PO akan dialihkan ke <?php echo $this->lang->line("vendor_label"); ?> lainnya.",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        animation: "slide-from-top",
      },
      function(inputValue) {
        if (inputValue === false) {
          blockPage('Form Pengalihan PO sedang diproses ...');
          setTimeout(function() { 
            window.location.assign(url.pengalihan.replace(':UID', uid));
          }, 1000);
          return;
        } 
        
        $.post(url.actExe, {uid: uid, alasan: "", mode: "sanggup"}, function (data, status) {
          if (status === "success") {
            swal.close();
            successMessage('Berhasil !', 'PO berhasil disanggupi oleh <?php echo $this->lang->line("vendor_label"); ?>.');
          }
        })
        .fail(function (error) {
            swal.close();
            errorMessage('Gagal !', 'Terjadi kesalahan saat menyetujui transaksi.');
        });
      });
    });

    $("#table").on("click", ".alihkan-row", function() {
      let uid = $(this).data('uid');
      blockPage('Form Pengalihan PO sedang diproses ...');
      setTimeout(function() { 
        window.location.assign(url.pengalihan.replace(':UID', uid));
      }, 1000);
    });

    listen("<?php echo $tab1Uid; ?>", 1);
    listen("<?php echo $tab2Uid; ?>", 2);
  });
</script>