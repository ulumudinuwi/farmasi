<script>
var UID = "<?php echo $uid; ?>",
    form = '#form';

var url = {
  index: "<?php echo site_url('logistik/po'); ?>",
  save: "<?php echo site_url('api/logistik/po/save'); ?>",
  getDistributor: "<?php echo site_url('api/master/pabrik/get_distributor?q=:ID'); ?>",
  getData: "<?php echo site_url('api/logistik/po/get_data/:UID'); ?>",
  loadDataBarang: "<?php echo site_url('api/logistik/stock/load_data_modal?mode=po'); ?>",
  getHistoryBarang: "<?php echo site_url('api/logistik/history/load_data?uid=:UID'); ?>",
};

var tableDetail = $('#table_detail'),
    tableDetailDt,
    curRowBarang = null;

/* variabel di modal */
var tableBarang = $('#table-barang'),
    tableBarangDt,
    modalBarang = $('#list-barang-modal');

function fillElement(obj, element) {
  let parent = element.parent();
  parent.find('.loading-select').show();
  $.getJSON(obj.url, function(data, status) {
    if (status === 'success') {
      var option = '';
      option += '<option value="" selected="selected">- Pilih -</option>';
      for (var i = 0; i < data.list.length; i++) {
        let selected = "",
            label = data.list[i][obj.key];
        if (parseInt(data.list[i].is_default) === 1) {
          selected = 'selected="selected"';
          label += ` (Default)`;
        }

        if(parseInt(obj.value) !== 0) {
          if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';
        }
        option += `<option value="${data.list[i].id}" ${selected}>${label}</option>`;
      }
      element.html(option).trigger("change");
    }
    parent.find('.loading-select').hide();
  });
}

function fillForm(uid, data) {
  tableDetailDt = tableDetail.DataTable({
    "info": false,
    "ordering": false,
    "paginate": false,
    "drawCallback": function (settings) {
      $('.disp_qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});

      let tr = tableDetail.find("tbody tr");
      tr.each(function() {
        let id = $(this).find('.input-barang_id').val();
        let qty = isNaN(parseFloat($(this).find('.input-qty').val())) ? 0 : parseFloat($(this).find('.input-qty').val());
        let maximum = parseFloat($(this).find('.input-maximum').val());
        let td = $(this).children().eq(5);
        td.removeClass('bg-danger');
        if(qty > maximum) {
          td.addClass('bg-danger');
        }
        $(this).prop('id', "row-barang-" + id);
      });
    },
  });

  if(parseInt(UID) === 0) {
    if(data !== "") {
      blockElement($(form));
      $.getJSON(url.getData.replace(':UID', 0) + "?data=" + data, function(data, status) {
        data = data.data;
        $("#tmp_vendor_id").val(data.vendor_id);
        $("#pabrik_id").val(data.pabrik_id).trigger("change");
        for (var i = 0; i < data.details.length; i++) {
          columnBarang(data.details[i]);
        }
        $(form).unblock();
      });
    } else {
      $("#pabrik_id").trigger("change");
    }
    return;
  }

  blockElement($(form));
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;
      $("#id").val(data.id);
      $("#uid").val(data.uid);
      $("#kode").val(data.kode);
      $("#tanggal").data("daterangepicker").setStartDate(moment(data.tanggal).isValid() ? moment(data.tanggal) : moment())
      $("#tanggal").data("daterangepicker").setEndDate(moment(data.tanggal).isValid() ? moment(data.tanggal) : moment());
      $("#sifat").val(data.sifat).trigger("change");
      $("#tmp_vendor_id").val(data.vendor_id);
      $("#pabrik_id").val(data.pabrik_id).trigger("change");

      for (var i = 0; i < data.details.length; i++) {
        columnBarang(data.details[i]);
      }
      $(form).unblock();
    }
  });
}

function columnBarang(data) {
  tableDetailDt.row.add([
    // Col 1
    `<input type="hidden" class="input-detail_id" name="detail_id[]" value="${data.id}">` +
    `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
    `<div class="input-group">` +
      `<input class="form-control disp_kode_barang" readonly="readonly" placeholder="Cari Barang" value="${data.kode_barang}">` +
      `<div class="input-group-btn">` +
        `<button type="button" class="btn btn-primary cari-barang" data-id="row-barang-${data.barang_id}"><i class="fa fa-search"></i></button>` +
      `</div>` +
    `</div>` +
    `<span class="text-slate-300 text-bold text-size-mini disp_barang mt-10">${data.barang}</span>` +
    `<br/><a class="label label-info history-row" data-barang_id="${btoa(JSON.stringify(data.barang_id))}">History</a>`,
    // Col 2
    `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
    `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
    `<label class="label-satuan">${data.satuan_po}</label>`,
    // Col 3
    `<input type="hidden" class="input-minimum" name="minimum[]" value="${data.minimum}">` +
    `<label class="label-minimum">${numeral(data.minimum).format('0.0,')}</label>`,
    // Col 4
    `<input type="hidden" class="input-stock" name="stock[]" value="${data.stock}">` +
    `<label class="label-stock">${numeral(data.stock).format('0.0,')}</label>`,
    // Col 5
    `<input type="hidden" class="input-maximum" name="maximum[]" value="${data.maximum}">` +
    `<label class="label-maximum">${numeral(data.maximum).format('0.0,')}</label>`,
    // Col 6
    `<input type="hidden" class="input-qty" name="qty[]" value="${data.qty}">` +
    `<input class="form-control disp_qty text-right" value="${data.qty}">`,
    // Col 7
    `<input type="hidden" class="input-harga" name="harga[]" value="${data.harga}">` +
    `<label class="label-harga">Rp. ${numeral(data.harga).format('0.0,')}</label>`,
    // Col 8
    `<label class="label-subtotal">Rp. ${numeral(data.total).format('0.0,')}</label>`,
    // Col 9
    `<button type="button" class="btn btn-xs btn-danger remove-row"><i class="fa fa-trash"></i></button>`,
  ]).draw(false);
  updateDetail();
}

function updateDetail() {
    let total = 0,
        total_ppn = 0;
    tableDetail.find('tbody .label-subtotal').each(function (i, el) {
        total += numeral($(el).html())._value;
    });
    $("#disp_total").html('Rp.' + numeral(total).format('0.0,'));
    $("#total").val(total);

    total_ppn = total * 0.1;
    $("#disp_total_ppn").html('Rp.' + numeral(total_ppn).format('0.0,'));
    $('#total_ppn').val(total_ppn);

    total += total_ppn;
    $("#disp_grand_total").html('Rp.' + numeral(total).format('0.0,'));
}

$(document).ready(function() {
  $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
  $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

  $('.btn-save').on('click', function(e) {
    e.preventDefault();
    $(form).submit();
  });

  $(".btn-batal").click(function() {
    window.location.assign(url.index);
  });

  $(".disp_tanggal").daterangepicker({
    singleDatePicker: true,
    startDate: moment("<?php echo date('Y-m-d'); ?>"),
    endDate: moment("<?php echo date('Y-m-d'); ?>"),
    applyClass: "bg-slate-600",
    cancelClass: "btn-default",
    opens: "center",
    autoApply: true,
    locale: {
      format: "DD/MM/YYYY"
    }
  });

  $("#btn_tanggal").click(function () {
    let parent = $(this).parent();
    parent.find('input').data("daterangepicker").toggle();
  });

  $('#pabrik_id').change(function() {
    let obj = {
      key: "vendor",
      value: $('#tmp_vendor_id').val(),
      url: url.getDistributor.replace(":ID", $(this).val())
    };
    fillElement(obj, $('#vendor_id'));
  });

  tableBarangDt = tableBarang.DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": url.loadDataBarang,
        "type": "POST"
    },
    "columns": [
        { 
            "data": "kode",
            "render": function (data, type, row, meta) {
                var template = '<a href="#" class="add-barang" data-id="{{ID}}" data-kode="{{KODE}}" data-nama="{{NAMA}}" data-satuan_id="{{SATUAN_ID}}" data-satuan_po="{{SATUAN}}" data-isi_satuan="{{ISI}}" data-maximum="{{MAX}}" data-stock="{{STOCK}}" data-minimum="{{MIN}}" data-harga="{{HARGA}}">{{KODE}}</a>';
                return template
                    .replace(/\{\{ID\}\}/g, row.id)
                    .replace(/\{\{KODE\}\}/g, row.kode)
                    .replace(/\{\{NAMA\}\}/g, row.nama)
                    .replace(/\{\{SATUAN_ID\}\}/g, row.satuan_id)
                    .replace(/\{\{SATUAN\}\}/g, row.satuan)
                    .replace(/\{\{ISI\}\}/g, row.isi_satuan)
                    .replace(/\{\{MAX\}\}/g, row.maximum)
                    .replace(/\{\{STOCK\}\}/g, row.stock)
                    .replace(/\{\{MIN\}\}/g, row.minimum)
                    .replace(/\{\{HARGA\}\}/g, row.harga);
            },
            "className": "text-center"
        },
        { "data": "nama" },
        { 
            "data": "satuan",
            "orderable": false,
            "searchable": false
        },
        { 
            "data": "stock",
            "render": function (data, type, row, meta) {
                return numeral(data).format('0.0,');
            },
            "orderable": false,
            "searchable": false
        }
    ]
  });

  tableBarang.on('click', '.add-barang', function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    let kode = $(this).data('kode');
    let nama = $(this).data('nama');
    let satuan_id = $(this).data('satuan_id');
    let satuan_po = $(this).data('satuan_po');
    let isi_satuan = parseFloat($(this).data('isi_satuan'));
    let maximum = parseFloat($(this).data('maximum'));
    let stock = parseFloat($(this).data('stock'));
    let minimum = parseFloat($(this).data('minimum'));
    let harga = parseFloat($(this).data('harga'));

    let notif = false;
    $('[name="barang_id[]"').each(function () {
      if(parseInt(id) === parseInt($(this).val())) notif = true;
    });

    if(notif) {
      errorMessage('Peringatan !', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali.');
      return;
    }

    let qty = maximum - stock;
    if(qty < 0) qty = 1;

    if (curRowBarang != null) { // Row Exists
      let tr = $("#" + curRowBarang);
      let qty = tr.find('.input-qty').val();

      tr.prop('id', "row-barang-" + id);
      tr.find('.cari-barang').data('id', "row-barang-" + id).attr('data-id', "row-barang-" + id);
      tr.find('.history-row').data('barang_id', btoa(JSON.stringify(id))).attr('data-barang_id', btoa(JSON.stringify(id)));
      tr.find('.input-barang_id').val(id);
      tr.find('.input-satuan_id').val(satuan_id);
      tr.find('.input-isi_satuan').val(isi_satuan);
      tr.find('.input-maximum').val(maximum);
      tr.find('.input-stock').val(stock);
      tr.find('.input-minimum').val(minimum);
      tr.find('.input-harga').val(harga);
      tr.find('.disp_kode_barang').val(kode);
      tr.find('.disp_barang').html(nama);
      tr.find('.label-satuan').html(satuan_po);
      tr.find('.label-maximum').html(numeral(maximum).format('0.0,'));
      tr.find('.label-stock').html(numeral(stock).format('0.0,'));
      tr.find('.label-minimum').html(numeral(minimum).format('0.0,'));
      tr.find('.label-harga').html('Rp.' + numeral(harga).format('0.0,'));
      tr.find('.label-subtotal').html('Rp.' + numeral(harga * qty).format('0.0,'));
      tr.find('.disp_qty').focus().select();
    } else {
      columnBarang({
          id: 0,
          barang_id: id,
          kode_barang: kode,
          barang: nama,
          satuan_id: satuan_id,
          isi_satuan: isi_satuan,
          satuan_po: satuan_po,
          maximum: maximum,
          stock: stock,
          minimum: minimum,
          harga: harga,
          qty: qty,
          total: harga * qty,
      });
    }
    updateDetail();
    modalBarang.modal('hide');
  });

  $("#btn-tambah").click(function (e) {
      curRowBarang = null;
      tableBarangDt.draw(false);
      modalBarang.modal('show');
  });

  /* EVENTS TABLE DETAIL */
  tableDetail.on('click', '.cari-barang', function() {
    curRowBarang = $(this).data('id');
    tableBarangDt.draw(false);
    modalBarang.modal('show');
  });

  tableDetail.on('click', '.history-row', function() {
    let barang_id = $(this).data('barang_id');
    showHistoryBarang(url.getHistoryBarang.replace(':UID', barang_id), null);
  });

  tableDetail.on('change blur keyup', '.disp_qty', function() {
    let tr = $(this).parent().parent();
    let td = $(this).parent();
    let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
    let maximum = parseFloat(tr.find('.input-maximum').val());

    let harga = parseFloat(tr.find('.input-harga').val());
    tr.find('.input-qty').val(val);
    tr.find('.label-subtotal').html('Rp. ' + numeral(harga * val).format('0.0,'))
    updateDetail();

    td.removeClass('bg-danger');
    if(val > maximum) {
      td.addClass('bg-danger');
    }
  });

  tableDetail.on('click', '.remove-row', function() {
    tableDetailDt
        .row($(this).parents('tr'))
        .remove()
        .draw();

    updateDetail();
  });

  $(form).validate({
    rules: {
      kode: { required: true },
      tanggal: { required: true },
      sifat: { required: true },
      pabrik_id: { required: true },
      vendor_id: { required: true },
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var placement = $(element).closest('.input-group');
        if (placement.length > 0) {
            error.insertAfter(placement);
        } else {
            error.insertAfter($(element));
        }
    },
    submitHandler: function (form) {
      tableDetailDt.search('').draw(false);

      let input = $('input[name="barang_id[]"]');
      if (input.length <= 0) {
        swal({
            title: "Peringatan!",
            text: "Pilih Barang terlebih dahulu.",
            html: true,
            type: "warning",
            confirmButtonColor: "#2196F3"
        });
        return;
      }

      swal({
        title: "Konfirmasi?",
        type: "warning",
        text: "Apakah data yang dimasukan benar??",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#2196F3",
        cancelButtonText: "Batal",
        cancelButtonColor: "#FAFAFA",
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
      },
      function() {
        $('.input-decimal').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('.input-bulat').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('input, textarea, select').prop('disabled', false);

        blockPage('Sedang diproses ...');
        var formData = $(form).serialize();
        $.ajax({
          data: formData,
          type: 'POST',
          dataType: 'JSON', 
          url: url.save,
          success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "PO berhasil disimpan.");
              window.location.assign(url.index);
          },
          error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
          }
        });
        return false;
      });
    }
  });

  fillForm(UID, "<?php echo $data; ?>");
});
</script>