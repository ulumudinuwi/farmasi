<script type="text/javascript">
  var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
  var table, tableHistory;
  var tableDetail = $('#table_detail');
  var btnRefresh = $('#btn-refresh');
  var detailModal = $('#detail-modal');
  var url = {
    loadData: "<?php echo site_url('api/logistik/approval/load_data?browse=:BROWSE'); ?>",
    getData: "<?php echo site_url('api/logistik/po/get_data/:UID'); ?>",
    actExe: "<?php echo site_url('api/logistik/approval/act_exe'); ?>",
    cetakPO: "<?php echo site_url('api/logistik/po/cetak/:UID'); ?>",
    listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID'); ?>",
  };

  function subsDate(range, tipe) {
    let date = range.substr(0, 10);
    if(tipe === "sampai") date = range.substr(13, 10);
    return getDate(date);
  }

  function listen(q) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('logistik-' + q, function(e) {
        table.draw(false);
        tableHistory.draw(false);
    }, false);
  }

  function showDetail(uid) {
    $.getJSON(url.getData.replace(':UID', uid), function (data, status) {
      if (status === 'success') {
        data = data.data;

        $("#detail_kode").html(data.kode);
        $("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
        $("#detail_sifat").html(data.sifat_desc);
        $("#detail_pabrik").html(data.pabrik);
        $("#detail_vendor").html(data.vendor);
        $("#detail_status").html(data.status_desc);

        $("#detail_keterangan").html("");
        if(parseInt(data.status) === 6 || parseInt(data.status) === 7) { // Ditolak, Dialihkan
          let keterangan = data.keterangan ? `<b>Alasan</b>:<br/>${data.keterangan}` : "";
          $("#detail_keterangan").html(keterangan);
        }

        var isDTable = $.fn.dataTable.isDataTable(tableDetail);
        if(isDTable === true) tableDetail.DataTable().destroy();

        tableDetail.DataTable({
            "ordering": false,
            "processing": true,
            "aaData": data.details,
            "columns": [
              { 
                "data": "barang",
                "render": function (data, type, row, meta) {
                    let tmp = data;
                    tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
                    return tmp;
                },
              },
              { "data": "satuan_po" },
              { 
                  "data": "minimum",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "stock",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "maximum",
                  "render": function (data, type, row, meta) {
                      return `<span class="label-maximum">${numeral(data).format('0.0,')}</span>`;
                  },
                  "className": "text-right"
              },
              { 
                  "data": "qty",
                  "render": function (data, type, row, meta) {
                      return `<span class="label-qty">${numeral(data).format('0.0,')}</span>`;
                  },
                  "className": "text-right"
              },
              {
                  "data": "harga",
                  "render": function (data, type, row, meta) {
                      return 'Rp. ' + numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              {
                  "data": "total",
                  "render": function (data, type, row, meta) {
                      return 'Rp.' + numeral(row.total).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "diterima",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
            ],
            "drawCallback": function (settings) {
              let tr = tableDetail.find("tbody tr");
              tr.each(function() {
                let qty = isNaN(parseFloat(numeral($(this).find('.label-qty').html())._value)) ? 0 : parseFloat(numeral($(this).find('.label-qty').html())._value);
                let maximum = parseFloat(numeral($(this).find('.label-maximum').html())._value);
                let td = $(this).children().eq(5);
                td.removeClass('bg-danger');
                if(qty > maximum) {
                  td.addClass('bg-danger');
                }
              });
            },
        });

        $('#detail_total').html('Rp. ' + numeral(data.total).format('0.0,'));
        $('#detail_total_ppn').html('Rp. ' + numeral(data.total_ppn).format('0.0,'));
        $('#detail_grand_total').html('Rp. ' + numeral(data.grand_total).format('0.0,'));
        detailModal.modal('show');
      }
    });
  }

  function getColumnTable(browse) {
    let column = [
        { 
          "data": "kode",
          "render": function (data, type, row, meta) {
              let tmp = '<a class="show-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + data + '</a>';
              return tmp;
          },
        },
        {
          "data": "tanggal",
          "render": function (data, type, row, meta) {
            let tmp = moment(data).format('DD-MM-YYYY HH:mm');
            tmp += `<br/><span class="text-size-mini text-info"><b>Pengajuan Oleh:</b><br/> ${row.pengajuan_by}</span>`;
            return tmp;
          },
          "searchable": false,
        },
        { 
          "data": "pabrik",
          "orderable": false,
          "searchable": false,
        },
        { 
          "data": "vendor",
          "orderable": false,
          "searchable": false,
        },
        { 
          "data": "sifat",
          "orderable": false,
          "searchable": false,
        },
      ];

    switch(parseInt(browse)) {
      case 2:
        column.push(
          { 
            "data": "status_desc",
            "orderable": false,
            "searchable": false,
            "render": function (data, type, row, meta) {
              let tmp = data;
              return tmp;
            }
          },
          {
            "data": "persetujuan_by",
            "searchable": false,
            "orderable": false,
            "render": function (data, type, row, meta) {
              let tmp = data;
              tmp += `<br/><span class="text-size-mini text-info"><b>Tanggal:</b><br/> ${moment(row.persetujuan_at).format('DD-MM-YYYY HH:mm')}</span>`;
              return tmp;
            },
          }
        ); 
        break;
      default:
        column.push({
          "data": "uid",
          "orderable": false,
          "searchable": false,
          "render": function (data, type, row, meta) {
            let tmp = '&mdash;';
            if(parseInt(row.status) === 1) {
              tmp = '<a class="acc-row btn btn-success btn-xs" data-uid="' + row.uid + '" data-popup="tooltip" data-title="Setujui" data-placement="top"><i class="fa fa-check"></i></a>';
              tmp += '&nbsp;&nbsp;<a class="tolak-row btn btn-danger btn-xs" data-uid="' + row.uid + '" data-popup="tooltip" data-title="Tolak" data-placement="top"><i class="fa fa-times"></i></a>';
            }
            return tmp;
          },
          "className": "text-center"
        }); 
        break;
    }
    return column;
  }

  function handleLoadTable(browse, element) {
    switch(parseInt(browse)) {
      case 2:
        tableHistory = element.DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadData.replace(':BROWSE', browse),
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate($("#search_history_range_tanggal").val(), 'dari');
                  p.tanggal_sampai = subsDate($("#search_history_range_tanggal").val(), 'sampai');
                  p.sifat = $('#search_history_sifat').val();
                  p.pabrik_id = $('#search_history_pabrik').val();
                  p.status = $('#search_history_status').val();
              }
          },
          "order": [1, "desc"],
          "columns": getColumnTable(browse),
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });
        break;
      default:
        table = element.DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadData.replace(':BROWSE', browse),
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
                  p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
                  p.sifat = $('#search_sifat').val();
                  p.pabrik_id = $('#search_pabrik').val();
                  p.status = "";
              }
          },
          "order": [1, "asc"],
          "columns": getColumnTable(browse),
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });

        var isDTable = $.fn.dataTable.isDataTable($('#table-history'));
        if(isDTable === false) handleLoadTable(2, $('#table-history'));
        break;
    }
  }

  $(window).ready(function() {
    $(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(tanggalDari),
        endDate: moment(tanggalSampai),
    });
    handleLoadTable(1, $('#table'));

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-1':
            table.draw(false);
            break;
          default:
            tableHistory.draw(false);
            break;
        }
    });

    $("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
        table.draw();
    });

    $("#btn_search_tanggal").click(function () {
        $("#search_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_range_tanggal, #search_sifat, #search_pabrik").on('change', function() {
      table.draw();
    });

    $("#table").on("click", ".show-row", function () {
      var uid = $(this).data('uid');
      showDetail(uid);
    });

    $("#table").on("click", ".acc-row", function () {
      var uid = $(this).data('uid');
      swal({
        title: "Setujui PO ?",
        text: "Apakah Anda yakin menyetujui PO ini ?",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Ya",
        cancelButtonText: "Batal",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        animation: "slide-from-top",
      },
      function() {
        $.post(url.actExe, {uid: uid, alasan: "", mode: "acc"}, function (data, status) {
          if (status === "success") {
            table.draw(false);
            swal.close();
            successMessage('Berhasil !', 'PO berhasil disetujui.');
          }
        })
        .fail(function (error) {
            swal.close();
            errorMessage('Gagal !', 'Terjadi kesalahan saat menyetujui transaksi.');
        });
      });
    });

    $("#table").on("click", ".tolak-row", function () {
      $("#swal-text").focus();

      var uid = $(this).data('uid');
      swal({
        title: "Tolak PO ?",
        text: "Apakah Anda yakin tidak menyetujui PO ini ?<br/><textarea class='form-control' id='swal-text' style='resize: vertical;'></textarea>",
        html: true,
        showCancelButton: true,
        confirmButtonText: "Tolak",
        cancelButtonText: "Batal",
        confirmButtonColor: "#F44336",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        animation: "slide-from-top",
      },
      function() {
        var value = $("#swal-text").val();
        if (value === "") {
          swal.showInputError("Silahkan isi alasan ditolaknya PO.");
          $("#swal-text").focus();
          return;
        }
      
        $.post(url.actExe, {uid: uid, alasan: value, mode: "tolak"}, function (data, status) {
          if (status === "success") {
            table.draw(false);
            swal.close();
            successMessage('Berhasil !', 'PO berhasil ditolak.');
          }
        })
        .fail(function (error) {
            swal.close();
            errorMessage('Gagal !', 'Terjadi kesalahan saat menolak transaksi.');
        });
      });
    });

    $("#search_history_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
        tableHistory.draw();
    });

    $("#btn_history_search_tanggal").click(function () {
        $("#search_history_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_history_range_tanggal, #search_history_sifat, #search_history_pabrik, #search_history_status").on('change', function() {
      tableHistory.draw();
    });

    $("#table-history").on("click", ".show-row", function () {
      var uid = $(this).data('uid');
      showDetail(uid);
    });

    listen("<?php echo $pUid; ?>");
  });
</script>