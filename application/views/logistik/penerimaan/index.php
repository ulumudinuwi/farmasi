<?php echo messages(); ?>
<style type="text/css">
  .table thead tr th, .table tbody tr td {
    white-space: nowrap;
    vertical-align: top;
  }

  .detail-td-footer {
    border-top: none !important;
  }
</style>
<div class="panel panel flat">
  <div class="tabbale">
    <ul class="nav nav-tabs nav-tabs-highlight">
      <li class="active">
        <a href="#tab-1" data-toggle="tab">Daftar Penerimaan</a>
      </li>
      <li>
        <a href="#tab-2" data-toggle="tab">History Penerimaan</a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab-1">
        <div class="panel-body form-horizontal">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('pabrik_label'); ?></label>
                <div class="col-md-8">
                  <select class="form-control" id="search_pabrik">
                    <option value="" selected="selected">- Pilih -</option>
                    <?php 
                      foreach($pabrik as $row) {
                        echo "<option value='".base64_encode($row->id)."'>{$row->kode} - {$row->nama}</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3">Sifat</label>
                <div class="col-md-8">
                  <select class="form-control" id="search_sifat">
                    <option value="" selected="selected">- Pilih -</option>
                    <?php 
                      foreach($sifat as $key => $val) {
                        echo "<option value='{$key}'>{$val}</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3">Status</label>
                <div class="col-md-8">
                  <select class="form-control" id="search_status">
                    <option value="" selected="selected">- Pilih -</option>
                    <?php 
                      foreach($status_po as $key => $val) {
                        echo "<option value='{$key}'>{$val}</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr class="no-margin-bottom no-margin-top">
        <div class="panel-body no-padding-top">
          <div class="table-responsive">
            <table id="table" class="table table-bordered table-striped">
              <thead>
                <tr class="bg-slate">
                  <th>No. PO</th>
                  <th>Tanggal</th>
                  <th><?php echo $this->lang->line('pabrik_label'); ?></th>
                  <th><?php echo $this->lang->line('vendor_label'); ?></th>
                  <th>Sifat</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab-2">
        <div class="panel-body form-horizontal">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3" for="search_history_tanggal">Tanggal</label>
                <div class="col-md-8">
                  <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn_search_history_tanggal">
                      <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search_history_range_tanggal" class="form-control rangetanggal-form">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3">Sifat</label>
                <div class="col-md-8">
                  <select class="form-control" id="search_history_sifat">
                    <option value="" selected="selected">- Pilih -</option>
                    <?php 
                      foreach($sifat as $key => $val) {
                        echo "<option value='{$key}'>{$val}</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('pabrik_label'); ?></label>
                <div class="col-md-8">
                  <select class="form-control" id="search_history_pabrik">
                    <option value="" selected="selected">- Pilih -</option>
                    <?php 
                      foreach($pabrik as $row) {
                        echo "<option value='".base64_encode($row->id)."'>{$row->kode} - {$row->nama}</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3">Status</label>
                <div class="col-md-8">
                  <select class="form-control" id="search_history_status">
                    <option value="" selected="selected">- Pilih -</option>
                    <?php 
                      foreach($status as $key => $val) {
                        echo "<option value='{$key}'>{$val}</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr class="no-margin-bottom no-margin-top">
        <div class="panel-body no-padding-top">
          <div class="table-responsive">
            <table id="table-history" class="table table-bordered table-striped">
              <thead>
                <tr class="bg-slate">
                  <th>Nomor</th>
                  <th>No. PO</th>
                  <th>Tanggal</th>
                  <th><?php echo $this->lang->line('pabrik_label'); ?></th>
                  <th><?php echo $this->lang->line('vendor_label'); ?></th>
                  <th>Sifat</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>