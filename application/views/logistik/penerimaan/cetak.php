<html>
<body>
<style>
body {
    line-height: 1.2em;
    font-size: 12px;
    font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
    margin-top: 0;
    margin-bottom: 5px;
}
h1 {
    font-size: 26px;
}
h2 {
    font-size: 20px;
}
h3 {
    font-size: 18px;
}
h4 {
    font-size: 16px;
}
h5 {
    font-size: 14px;
}
h6 {
    font-size: 12px;
}
table {
    border-collapse: collapse;
    font-size: 12px;
}
table th,
table td {
    vertical-align: middle;
    padding: 3px 10px;
    line-height: 1.5384616;
}
.table thead th {
    color: #fff;
    background-color: #607D8B;
    font-weight: bold;
    text-align: center;
}
.text-left {
    text-align: left;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.text-bold {
  font-weight: bold;
}
.uppercase {
    text-transform: uppercase !important;
}
.footer_current_date_user {
    color: #d10404;
    font-size: 8px;
    vertical-align: top;
    margin-top: 10px;
}
</style>
<h4 class="text-center text-bold no-margin no-padding">LAPORAN PENERIMAAN BARANG</h4>
<h6 class="text-center text-bold no-margin no-padding">Nomor <?php echo $obj->kode; ?></h6>
<table style="width: 100%;" style="font-size: 11px;">
   <tr>
        <td style="width: 50%;">
            <table>
                <tr>
                    <td class="text-bold">TANGGAL</td>
                    <td>:</td>
                    <td><?php echo strtoupper($obj->indo_tanggal); ?></td>
                </tr>
                <tr>
                    <td class="text-bold"><?php echo strtoupper($this->lang->line('pabrik_label')); ?></td>
                    <td>:</td>
                    <td><?php echo $obj->pabrik; ?></td>
                </tr>
                <tr>
                    <td class="text-bold"><?php echo strtoupper($this->lang->line('vendor_label')); ?></td>
                    <td>:</td>
                    <td><?php echo $obj->vendor; ?></td>
                </tr>
            </table>
        </td>
        <td style="width: 50%;">
            <table>
                <tr>
                    <td class="text-bold">SIFAT</td>
                    <td>:</td>
                    <td><?php echo $obj->sifat_desc; ?></td>
                </tr>
                <tr>
                    <td class="text-bold">STATUS</td>
                    <td>:</td>
                    <td><?php echo $obj->status_desc; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br/>
<table class="table table-bordered table-striped" border="1" style="font-size: 11px; width: 100%;">
    <thead>
        <tr class="bg-slate">
            <th rowspan="2" style="width: 8%;">NO.</th>
            <th rowspan="2" style="width: 20%;">BARANG</th>
            <th rowspan="2" style="width: 10%;">SATUAN</th>
            <th colspan="2" style="width: 20%;">HARGA</th>
            <th colspan="3" style="width: 22%;">QTY</th>
            <th rowspan="2" style="width: 15%;">SUBTOTAL</th>
        </tr>
        <tr class="bg-slate">
            <th>ORDER</th>
            <th>TERIMA</th>
            <th>ORDER</th>
            <th>TERIMA</th>
            <th>SISA</th>
        </tr>
    </thead>
    <?php 
        if(count($obj->details) > 0):
            $no = 1;
            foreach ($obj->details as $i => $row): 
    ?>
    <tbody>
        <tr>
            <td class="text-center"><?php echo $no; ?></td>
            <td><?php echo $row->barang; ?></td>
            <td><?php echo $row->satuan_penerimaan; ?></td>
            <td class="text-right">Rp. <?php echo number_format($row->harga_po, 2, ",", "."); ?></td>
            <td class="text-right">Rp. <?php echo number_format($row->harga, 2, ",", "."); ?></td>
            <td class="text-right"><?php echo number_format($row->qty_po, 2, ",", "."); ?></td>
            <td class="text-right"><?php echo number_format($row->qty, 2, ",", "."); ?></td>
            <td class="text-right"><?php echo number_format($row->sisa, 2, ",", "."); ?></td>
            <td class="text-right">Rp. <?php echo number_format($row->harga * $row->qty, 2, ",", "."); ?></td>
        </tr>
    <?php 
            $no++;
        endforeach; 
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6">&nbsp;</td>
            <td colspan="2" class="text-bold">TOTAL</td>
            <td class="text-right">Rp. <?php echo number_format($obj->total, 2, ",", "."); ?></td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
            <td colspan="2" class="text-bold">TOTAL PPN</td>
            <td class="text-right">Rp. <?php echo number_format($obj->total_ppn, 2, ",", "."); ?></td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
            <td colspan="2" class="text-bold">GRAND TOTAL</td>
            <td class="text-right">Rp. <?php echo number_format($obj->grand_total, 2, ",", "."); ?></td>
        </tr>
    </tfoot>
    <?php else: ?>
    <tbody>
        <tr>
            <td style="font-weight: bold;text-align: center;" colspan="9">TIDAK ADA DATA</td>
        </tr>
    </tbody>
    <?php endif; ?>
</table>
<table style="width: 100%; font-size: 8px;">
    <tr>
        <td style="width: 12%;">KETERANGAN</td>
        <td style="width: 2%;">:</td>
        <td style="width: 86%;"><?php echo $obj->keterangan ? $obj->keterangan : "-"; ?></td>
    </tr>
</table>
<br />
<table style="width: 100%; font-size: 10px;">
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 40%;"></td>
        <td style="width: 30%;"></td>
    </tr>
    <tr>
        <td class="text-center">PENERIMA / PENANGGUNG JAWAB</td>
        <td>&nbsp;</td>
        <td class="text-center"><?php echo strtoupper($this->lang->line('vendor_label')); ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="text-bold text-center">( <?php echo $obj->diproses_by; ?> )</td>
        <td>&nbsp;</td>
        <td class="text-bold text-center">( <?php echo $obj->vendor; ?> )</td>
    </tr>
</table>
<div class="footer_current_date_user">
    <span><?php echo $current_date.", ".$current_user; ?></span>
</div>
</body>
</html>