<script type="text/javascript">
  var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
  var tableHistory, table;
  var url = {
    loadDataPo : "<?php echo site_url('api/logistik/penerimaan/load_data_po'); ?>",
    loadData : "<?php echo site_url('api/logistik/penerimaan/load_data'); ?>",
    cetak : "<?php echo site_url('api/logistik/penerimaan/cetak/:UID'); ?>",
    form : "<?php echo site_url('logistik/penerimaan/form/:UID'); ?>",
    listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID'); ?>",
  };

  function subsDate(range, tipe) {
    let date = range.substr(0, 10);
    if(tipe === "sampai") date = range.substr(13, 10);
    return getDate(date);
  }

  function listen(q, browse) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('logistik-' + q, function(e) {
        switch(browse) {
          case 2:
            tableHistory.draw(false);
            break;
          default:
            table.draw(false);
            break;
        }
    }, false);
  }

  function handleLoadTable(browse) {
    switch(browse) {
      case 'table_history':
        tableHistory = $("#table-history").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadData,
              "type": "POST",
              "data": function(p) {
                  p.tanggal_dari = subsDate($("#search_history_range_tanggal").val(), 'dari');
                  p.tanggal_sampai = subsDate($("#search_history_range_tanggal").val(), 'sampai');
                  p.sifat = $('#search_history_sifat').val();
                  p.pabrik_id = $('#search_history_pabrik').val();
                  p.status = $('#search_history_status').val();
              }
          },
          "order": [2, "desc"],
          "columns": [
            { 
              "data": "kode",
              "render": function (data, type, row, meta) {
                  let label = `<a href="${url.form.replace(':UID', row.po_uid)}" data-toggle="tooltip" data-title="Lihat Penerimaan" data-placement="top">${data}</a>`;
                  label += `<br/><a href="${url.cetak.replace(':UID', row.uid)}" target="_blank" class="label label-info"><i class="fa fa-print"></i> Cetak</a>`;
                  return label;
              },
            },
            { 
              "data": "kode_po",
              "orderable": false,
            },
            {
              "data": "tanggal",
              "render": function (data, type, row, meta) {
                let tmp = moment(data).format('DD-MM-YYYY HH:mm');
                tmp += `<br/><span class="text-size-mini text-info"><b>Diproses Oleh:</b><br/> ${row.diproses_by}</span>`;
                return tmp;
              },
              "searchable": false,
            },
            { 
              "data": "pabrik",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "vendor",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "sifat",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "status_desc",
              "orderable": false,
              "searchable": false
            },
          ],
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();

            let tr = $("#table-history").find("tbody tr");
            tr.each(function() {
              var status = $(this).find('td').eq(6).text();
              if(status == "Diterima Sebagian") {
                $(this).find('td').eq(6).addClass('bg-info-300');
              } else {
                $(this).find('td').eq(6).addClass('bg-success-300');
              }
            });
          },
        });
        break;
      default:
        table = $("#table").DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
              "url": url.loadDataPo,
              "type": "POST",
              "data": function(p) {
                  p.sifat = $('#search_sifat').val();
                  p.pabrik_id = $('#search_pabrik').val();
                  p.status = $('#search_status').val();
              }
          },
          "order": [1, "asc"],
          "columns": [
            { 
              "data": "kode",
              "render": function (data, type, row, meta) {
                  let tmp = '<a class="terima-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Proses Penerimaan" data-placement="top">' + data + '</a>';
                  return tmp;
              },
            },
            {
              "data": "tanggal",
              "render": function (data, type, row, meta) {
                let tmp = moment(data).format('DD-MM-YYYY HH:mm');
                tmp += `<br/><span class="text-size-mini text-info"><b>Pengajuan Oleh:</b><br/> ${row.pengajuan_by}</span>`;
                return tmp;
              },
              "searchable": false,
            },
            { 
              "data": "pabrik",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "vendor",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "sifat",
              "orderable": false,
              "searchable": false,
            },
            { 
              "data": "status_desc",
              "orderable": false,
              "searchable": false
            },
          ],
          "fnDrawCallback": function (oSettings) {
            $('[data-toggle=tooltip]').tooltip();
          },
        });

        var isDTable = $.fn.dataTable.isDataTable($('#table-history'));
        if(isDTable === false) handleLoadTable('table_history');
        break;
      }
    }

  $(window).ready(function() {
    handleLoadTable('table');

    $('a[data-toggle="tab"]').click(function (e) {
        switch($(this).attr('href')) {
          case '#tab-1':
            table.draw(false);
            break;
          default:
            tableHistory.draw(false);
            break;
        }
    });

    $("#search_sifat, #search_pabrik, #search_status").on('change', function() {
      table.draw();
    });

    $('#table').on('click', '.terima-row', function() {
      let uid = $(this).data('uid');
      blockPage('Form Penerimaan PO sedang diproses ...');
      setTimeout(function() { 
        window.location.assign(url.form.replace(':UID', uid));         
      }, 1000);
    });

    $(".rangetanggal-form").daterangepicker({
        autoApply: true,
        locale: {
            format: "DD/MM/YYYY",
        },
        startDate: moment(tanggalDari),
        endDate: moment(tanggalSampai),
    });

    $("#search_history_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
        tanggalDari = picker.startDate.format('YYYY-MM-DD');
        tanggalSampai = picker.endDate.format('YYYY-MM-DD');       

        tableHistory.draw();
    });

    $("#search_history_range_tanggal").on('change', function () {
        range = $("#search_history_range_tanggal").val();
        var date = range.substr(0, 10);
        tanggalDari = getDate(date); 

        date = range.substr(13, 10);
        tanggalSampai = getDate(date); 

        tableHistory.draw();
    });

    $("#btn_search_history_tanggal").click(function () {
        $("#search_history_range_tanggal").data('daterangepicker').toggle();
    });

    $("#search_history_sifat, #search_history_pabrik, #search_history_status").on('change', function() {
      tableHistory.draw();
    });

    /*$("#table").on("click", ".edit-row", function () {
      let uid = $(this).data('uid');
    });*/

    listen("<?php echo $tab1Uid; ?>", 1);
    listen("<?php echo $tab2Uid; ?>", 2);
  });
</script>