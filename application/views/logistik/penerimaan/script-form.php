<script type="text/javascript">
  var UID = "<?php echo $uid; ?>",
    VIEW_MODE = 1,
    ADD_MODE = 0;
    form = '#form';

  var tablePo = $('#table-detail-po'),
      tableDetail = $('#table-penerimaan'),
      tableDetailDt;

  var url = {
    getData: "<?php echo site_url('api/logistik/penerimaan/get_data/:UID'); ?>",
    save: "<?php echo site_url('api/logistik/penerimaan/save'); ?>",
    index: "<?php echo site_url('logistik/penerimaan'); ?>",
  };

  function fillForm(uid) {
    blockElement($(form));
    $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
      if (status === 'success') {
        data = data.data;
        $("#detail_kode").html(data.kode);
        $("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
        $("#detail_sifat").html(data.sifat_desc);
        $("#detail_pabrik").html(data.pabrik);
        $("#detail_vendor").html(data.vendor);
        $("#detail_status").html(data.status_desc);

        switch(parseInt(data.status)) {
          case 5: // full receive
          case 7: // dialihkan
            $("#section_input_penerimaan, .btn-save").hide();
            $("a[href='#accordion-group1']").click();
            $("#detail_keterangan").html("");
            if(parseInt(data.status) === 7) { // Ditolak, Dialihkan
              let keterangan = data.keterangan ? `<b>Alasan</b>:<br/>${data.keterangan}` : "";
              $("#detail_keterangan").html(keterangan);
            }
            break;
          default:
            $("#status_penerimaan").text("-");
            break;
        }

        var isDTable = $.fn.dataTable.isDataTable($('#table_detail_po'));
        if(isDTable === true) $('#table_detail_po').DataTable().destroy();

        $('#table_detail_po').DataTable({
            "ordering": false,
            "processing": true,
            "aaData": data.details,
            "columns": [
              { 
                "data": "barang",
                "render": function (data, type, row, meta) {
                    let tmp = data;
                    tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
                    return tmp;
                },
              },
              { "data": "satuan_po" },
              { 
                  "data": "qty",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              {
                  "data": "harga",
                  "render": function (data, type, row, meta) {
                      return 'Rp. ' + numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
              {
                  "data": "total",
                  "render": function (data, type, row, meta) {
                      return 'Rp.' + numeral(row.total).format('0.0,');
                  },
                  "className": "text-right"
              },
              { 
                  "data": "diterima",
                  "render": function (data, type, row, meta) {
                      return numeral(data).format('0.0,');
                  },
                  "className": "text-right"
              },
            ]
        });

        $('#detail_total').html('Rp. ' + numeral(data.total).format('0.0,'));
        $('#detail_total_ppn').html('Rp. ' + numeral(data.total_ppn).format('0.0,'));
        $('#detail_grand_total').html('Rp. ' + numeral(data.grand_total).format('0.0,'));

        // START input penerimaan
        tableDetailDt = tableDetail.DataTable({
          "info": false,
          "ordering": false,
          "paginate": false,
          "drawCallback": function (settings) {
            $('.input-qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
            $('.input-harga').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
            $('.styled').uniform();
            $('.checkbox-inline')
                    .css('top', '-15px')
                    .css('left', '-6px')
                    .css('padding-left', '0')
                    .css('padding-right', '0');

            let tr = tableDetail.find("tbody tr");
            tr.each(function() {
              $(this).find('.input-qty').trigger('change');
              $(this).find('input').prop('readonly', true);
            });
          },
        });

        for (var i = 0; i < data.details.length; i++) {
          let qty = parseFloat(data.details[i].qty);
          let diterima = parseFloat(data.details[i].diterima);
          if(qty > diterima) columnBarang(data.details[i]);
        }
        // END input penerimaan

        $("#pabrik_id").val(data.pabrik_id);
        $("#vendor_id").val(data.vendor_id);
        $("#sifat").val(data.sifat);
        $("#po_id").val(data.id);
        
        // START history penerimaan
        $("#section_history_penerimaan").hide('slow');
        if (typeof data.history !== 'undefined') {
          $("#section-data-history-penerimaan").append('<div class="panel-group content-group-lg mb-10" id="history_accordion"></div>');
          for (var i = 0; i < data.history.length; i++) {
            var history = data.history[i];
            var tanggal = moment(history.tanggal).format('DD-MM-YYYY HH:mm');
            var status_history = "<span class='pull-right text-info'><i class='glyphicon glyphicon-asterisk'></i> Diterima Sebagian</span>";
            switch(parseInt(history.status)) {
              case 2:
                var status_history = "<span class='pull-right text-success'><i class='icon-check'></i> Diterima Semua</span>";
                break;
            }
            var keterangan = history.keterangan ? history.keterangan : "-";

            var rows = '<div class="panel panel-white">';
            rows += ' <div class="panel-heading">';
            rows += '   <h6 class="panel-title">';
            rows += '     <a class="collapsed" data-toggle="collapse" data-parent="#history_accordion" href="#history_' + ( i + 1 ) + '" aria-expanded="false">';
            rows += '       Penerimaan ' + tanggal;
            rows +=         status_history;
            rows += '     </a>';
            rows += '   </h6>';
            rows += ' </div>';
            rows += ' <div id="history_' + ( i + 1 ) + '" class="panel-collapse collapse" aria-expanded="false">';
            rows += '   <div class="panel-body form-inline" style="padding-left: 30px;">';
            rows += '     <div class="form-group mr-10">';
            rows += '       <label>Tanggal</label>';
            rows += '       <p class="form-control-static text-bold">' + tanggal + '</p>';
            rows += '     </div>';
            rows += '     <div class="form-group mr-10">';
            rows += '       <label>No. Penerimaan</label>';
            rows += '       <p class="form-control-static text-bold">' + history.kode + '</p>';
            rows += '     </div>';
            rows += '   </div>';
            rows += '   <div class="table-responsive">';
            rows += '     <table id="history_table_' + ( i + 1 ) + '" class="table table-bordered table-striped">';
            rows += '       <thead>';
            rows += '         <tr class="bg-slate">';
            rows += '           <th rowspan="2" style="width: 20%;">Kode</th>';
            rows += '           <th rowspan="2">Nama</th>';
            rows += '           <th rowspan="2" style="width: 15%;">Satuan</th>';
            rows += '           <th colspan="2" class="text-center" style="width: 18%;">Harga</th>';
            rows += '           <th colspan="3" class="text-center" style="width: 18%;">Jumlah</th>';
            rows += '         </tr>';
            rows += '         <tr class="bg-slate">';
            rows += '           <th class="text-center">Order</th>';
            rows += '           <th class="text-center">Terima</th>';
            rows += '           <th class="text-center">Order</th>';
            rows += '           <th class="text-center">Terima</th>';
            rows += '           <th class="text-center">Sisa</th>';
            rows += '         </tr>';
            rows += '       </thead>';
            rows += '       <tbody>';
            rows += '      <table>';
            rows += '     <div class="col-sm-12 mt-10">';
            rows += '       <div class="col-md-8">';
            rows += '         <label>Keterangan</label>';
            rows += `         <div class="form-control-static" id="keterangan_po">${keterangan}</div>`;
            rows += '       </div>';
            rows += '     </div>';
            rows += '   </div>';
            rows += ' </div>';
            rows += '</div>';

            $("#history_accordion").append(rows);
            
            if (typeof history.details !== 'undefined') {
              let curHisTable = $('#history_table_' + ( i + 1 ));
              let isDTable = $.fn.dataTable.isDataTable(curHisTable);
              if(isDTable === true) curHisTable.DataTable().destroy();

              curHisTable.DataTable({
                "ordering": false,
                "bProcessing": true,
                "aaData": history.details,
                "columns": [
                  { "data": "kode_barang"},
                  { "data": "barang" },
                  { "data": "satuan_penerimaan" },
                  { 
                    "data": "harga_po",
                    "render": function (data, type, row, meta) {
                      return `<span class="label-history-harga_po">Rp. ${numeral(data).format('0,0')}</span>`;
                    },
                    "className": "text-right"
                  },
                  { 
                    "data": "harga",
                    "render": function (data, type, row, meta) {
                      return `<span class="label-history-harga">Rp. ${numeral(data).format('0,0')}</span>`;
                    },
                    "className": "text-right"
                  },
                  { 
                    "data": "qty_po",
                    "render": function (data, type, row, meta) {
                      return numeral(data).format('0,0');
                    },
                    "className": "text-right"
                  },
                  { 
                    "data": "qty",
                    "render": function (data, type, row, meta) {
                      return numeral(data).format('0,0');
                    },
                    "className": "text-right"
                  },
                  { 
                    "data": "sisa",
                    "render": function (data, type, row, meta) {
                      return numeral(data).format('0,0');
                    },
                    "className": "text-right"
                  },
                ],
                "drawCallback": function (settings) {
                  let tr = curHisTable.find("tbody tr");
                  tr.each(function() {
                    let harga_po = parseFloat(numeral($(this).find('.label-history-harga_po').html())._value);
                    let harga = parseFloat(numeral($(this).find('.label-history-harga').html())._value);
                    let td = $(this).children().eq(4);
                    td.removeClass('bg-danger');
                    if(harga > harga_po) {
                      td.addClass('bg-danger');
                    }
                  });
                },
              });
            }
          }
        }
        if(data.history.length > 0) $("#section_history_penerimaan").show('slow');

        $(form).unblock();
      }
    })
  }

  function columnBarang(data) {
    tableDetailDt.row.add([
      // Col 1
      `<input type="hidden" class="input-detail_id" name="detail_id[]" value="0">` +
      `<input type="hidden" class="input-po_detail_id" name="po_detail_id[]" value="${data.id}">` +
      `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
       `<label class="checkbox-inline">` +
       `   <input type="checkbox" class="styled disp-checkbox">` +
       `   <input  type="hidden" name="check_barang[]" class="input-checkbox" value="0">` +
       `</label>`,
      // Col 2
      `${data.barang}` +
      `<br/><span class="text-slate-300 text-bold text-size-mini disp_barang mt-10">${data.kode_barang}</span>`,
      // Col 3
      `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
      `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
      `<label class="label-satuan">${data.satuan_po}</label>`,
      // Col 4
      `<input type="hidden" class="input-harga_po" name="harga_po[]" value="${data.harga}">` +
      `<label class="label-harga_po">${numeral(data.harga).format('0.0,')}</label>`,
      // Col 5
      `<input type="text" class="input-harga form-control text-right input-decimal" name="harga[]" value="${data.harga_diterima}">`,
      // Col 6
      `<input type="hidden" class="input-qty_po" name="qty_po[]" value="${data.qty}">` +
      `<label class="label-qty_po">${numeral(data.qty).format('0.0,')}</label>`,
      // Col 7
      `<input type="hidden" class="tmp-sisa" value="${data.sisa}">` +
      `<input type="hidden" class="input-sisa" name="sisa[]" value="${data.sisa}">` +
      `<label class="label-sisa">${numeral(data.sisa).format('0.0,')}</label>`,
      // Col 8
      `<input type="text" class="input-qty form-control text-right input-decimal" name="qty[]" value="${data.sisa}">`,
    ]).draw(false);
  }

  $(document).ready(function() {
    $(".wysihtml5-min").wysihtml5({
      parserRules:  wysihtml5ParserRules
    });
    $(".wysihtml5-toolbar").remove();

    $(".btn-batal").click(function() {
      window.location.assign(url.index);
    });

    $(".disp_tanggal").daterangepicker({
      singleDatePicker: true,
      startDate: moment("<?php echo date('Y-m-d'); ?>"),
      endDate: moment("<?php echo date('Y-m-d'); ?>"),
      applyClass: "bg-slate-600",
      cancelClass: "btn-default",
      opens: "center",
      autoApply: true,
      locale: {
        format: "DD/MM/YYYY"
      }
    });

    $("#btn_tanggal").click(function () {
      let parent = $(this).parent();
      parent.find('input').data("daterangepicker").toggle();
    });

    // EVENT TABLE INPUT PENERIMAAN
    tableDetail.on('click', '.disp-checkbox', function() {
      let tr = $(this).parents('tr');

      tr.find('input:not(".disp-checkbox")').prop('readonly', true);
      tr.find('.input-checkbox').val(0);
      if($(this).prop('checked') === true) {
        tr.find('input:not(".disp-checkbox")').prop('readonly', false);
        tr.find('.input-checkbox').val(1);
      } 
    });

    tableDetail.on('change blur keyup', '.input-harga', function() {
      let tr = $(this).parent().parent();
      let td = $(this).parent();
      let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
      let harga_po = parseFloat(tr.find('.input-harga_po').val());
      
      td.removeClass('bg-danger');
      if(val > harga_po) {
        td.addClass('bg-danger');
      }
    });

    tableDetail.on('change blur keyup', '.input-qty', function() {
      let tr = $(this).parent().parent();
      let td = $(this).parent();
      let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
      let sisa = parseFloat(tr.find('.tmp-sisa').val());
      sisa = sisa - val;
      if(sisa < 0) sisa = 0;

      tr.find('.input-sisa').val(sisa);
      tr.find('.label-sisa').html(numeral(sisa).format('0.0,'));
    });

    $(form).validate({
      rules: {
        tanggal: { required: true },
      },
      submitHandler: function (form) {
        tableDetailDt.search('').draw(false);
        let checked = $('.disp-checkbox:checked');
        if (checked.length <= 0) {
          swal({
              title: "Peringatan!",
              text: "Pilih Barang terlebih dahulu.",
              html: true,
              type: "warning",
              confirmButtonColor: "#2196F3"
          });
          return;
        }

        swal({
          title: "Konfirmasi?",
          type: "warning",
          text: "Apakah data yang dimasukan telah benar??",
          showCancelButton: true,
          confirmButtonText: "Ya",
          confirmButtonColor: "#2196F3",
          cancelButtonText: "Batal",
          cancelButtonColor: "#FAFAFA",
          closeOnConfirm: true,
          showLoaderOnConfirm: true,
        },
        function() {
          $('.input-decimal').each(function() {
            $(this).val($(this).autoNumeric('get'));
          });

          $('.input-bulat').each(function() {
            $(this).val($(this).autoNumeric('get'));
          });

          $('input, textarea, select').prop('disabled', false);
          
          blockPage('Sedang diproses ...');
          var formData = $(form).serialize();
          $.ajax({
            data: formData,
            type: 'POST',
            dataType: 'JSON', 
            url: url.save,
            success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "Penerimaan berhasil disimpan.");
              window.location.assign(url.index);
            },
            error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
            }
          });
          return false;
        });
      }
    });

    fillForm(UID);
  });
</script>