<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
    	vertical-align: top;
	}
</style>
<div class="panel panel-flat">
	<div class="row panel-body form-horizontal">
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-6">
					<select class="form-control" id="search_status">
						<option value="" selected="selected">- Pilih -</option>
						<option value="1">Ready</option>
						<option value="2">Reorder Point</option>
						<option value="3">Minimum</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<hr class="no-margin-bottom no-margin-top">
	<div class="panel-body no-padding-top">
		<div class="table-responsive">
			<table id="table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Kode</th>
						<th>Nama</th>
						<th>Satuan</th>
						<th>Maximum</th>
						<th>Reorder</th>
						<th>Minimum</th>
						<th>Stock</th>
						<th></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<?php $this->load->view('setting-mrm-modal'); ?>

<script type="text/javascript">
var table,
	tableId = '#table',
	modalSetting = '#setting_modal';

var url = {
	loadData: "<?php echo site_url('api/logistik/stock/load_data'); ?>",
	settingMRM: "<?php echo site_url('api/logistik/stock/setting'); ?>",
	kartuStock: "<?php echo site_url('logistik/stock/kartu_stock?uid=:UID'); ?>",
	listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID'); ?>",
};

function listen(q) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('logistik-' + q, function(e) {
        table.draw(false);
    }, false);
}

$(document).ready(function() {
	$(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	
	table = $(tableId).DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": url.loadData,
			"type": "POST",
			"data": function(p) {
				p.status = $('#search_status').val();
			}
		},
		"columns": [
		{ 
			"data": "kode",
			"render": function (data, type, row, meta) {
				let tmp = `<a title="Setting MRM Point" class="setting-row" data-id="${row.id}" data-maximum="${row.maximum}" data-reorder="${row.reorder}" data-minimum="${row.minimum}" data-satuan="${row.satuan}">${data}</a>`;
				if(parseInt(row.dipesan) === 1) 
					tmp += '<br/><label class="label label-flat label-rounded border-success text-success-600">Dipesan</label>';
				return tmp;
			},
		},
		{ "data": "nama" },
		{ 
			"data": "satuan", 
			"orderable": false,
			"searchable": false,
		},
		{ 
			"data": "maximum",
			"orderable": false,
			"searchable": false,
			"render": function (data, type, row, meta) {
				return numeral(data).format('0.0,');
			},
			"className": "text-right"
		},
		{ 
			"data": "reorder",
			"orderable": false,
			"searchable": false,
			"render": function (data, type, row, meta) {
				return numeral(data).format('0.0,');
			},
			"className": "text-right"
		},
		{ 
			"data": "minimum",
			"orderable": false,
			"searchable": false,
			"render": function (data, type, row, meta) {
				return numeral(data).format('0.0,');
			},
			"className": "text-right"
		},
		{ 
			"data": "qty",
			"orderable": false,
			"searchable": false,
			"render": function (data, type, row, meta) {
				return numeral(data).format('0.0,');
			},
			"className": "text-right"
		},
		{
			"data": "id",
			"orderable": false,
			"searchable": false,
			"render": function (data, type, row, meta) {
				let tmp = "&mdash;";
				tmp = '<a data-title="Lihat Kartu Stock" data-toggle="tooltip" class="btn btn-xs btn-primary" href="' + url.kartuStock.replace(':UID', btoa(JSON.stringify(data))) + '"><i class="fa fa-language"></i></a>';
				return tmp;
			},
			"className": "text-center"
		},
		],
		"drawCallback": function (oSettings) {
			$('[data-toggle=tooltip]').tooltip();
		},
    });

	$("#search_status").change(function() {
		table.draw(false);
	});

	$(tableId).on('click', '.setting-row', function() {
		$tr = $(this).parent().parent();
		let id = $(this).data('id');
		let maximum = $(this).data('maximum');
		let reorder = $(this).data('reorder');
		let minimum = $(this).data('minimum');
		let satuan = $(this).data('satuan');

		$("#setting_id").val(id);
		$("#setting_barang").html($tr.children("td").eq(1).text());
		$("#setting_satuan").html(satuan);
		$("#setting_maximum").autoNumeric("set", maximum).focus().select();
		$("#setting_reorder").autoNumeric("set", reorder);
		$("#setting_minimum").autoNumeric("set", minimum);

		$('#setting_simpan').attr('disabled', false);
		$('#setting_batal').attr('disabled', false);
		$(modalSetting).modal('show');
	});

	$('#setting_reorder').on('keyup', function() {
	    let setting_reorder = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
	    let setting_maximum = isNaN(parseFloat($('#setting_maximum').autoNumeric('get'))) ? 0 : parseFloat($('#setting_maximum').autoNumeric('get'));

	    if(setting_reorder >= setting_maximum) {
	    	warningMessage('Peringatan !', 'Reorder tidak boleh melebihi Maximum.');
	    	$(this).autoNumeric('set', 0);
	    }
	});

	$('#setting_minimum').on('keyup', function() {
	    let setting_reorder = isNaN(parseFloat($('#setting_reorder').autoNumeric('get'))) ? 0 : parseFloat($('#setting_reorder').autoNumeric('get'));
	    let setting_maximum = isNaN(parseFloat($('#setting_maximum').autoNumeric('get'))) ? 0 : parseFloat($('#setting_maximum').autoNumeric('get'));
	    let setting_minimum = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));

	    if(setting_minimum >= setting_maximum || setting_minimum >= setting_reorder) {
	    	warningMessage('Peringatan !', '<b>Minimum</b> tidak boleh sama atau melebihi <b>Maximum</b> dan <b>Reorder</b>.');
	    	$(this).autoNumeric('set', 0);
	    }
	});

	var settingApp = {
		initSettingForm: function () {
			$("#setting_form").validate({
				rules: {
					setting_maximum : { required: true },
					setting_reorder : { required: true },
					setting_minimum : { required: true },
				},
				messages: {
					setting_maximum: "Maximum diperlukan",
					setting_reorder: "Reorder diperlukan",
					setting_minimum: "Minimum diperlukan",
				},
				submitHandler: function (form) {
					settingApp.addSetting($(form));
					return false;
				}

			});
		},
		addSetting: function (form) {
			$('.input-decimal').each(function() {
	            $(this).val($(this).autoNumeric('get'));
	        });

	        $('.input-bulat').each(function() {
	            $(this).val($(this).autoNumeric('get'));
	        });

			$('#setting_simpan').attr('disabled', true);
			$('#setting_batal').attr('disabled', true);

			var postData = form.serialize();
			$.post(url.settingMRM, postData, function (data, status) {
				if (status === "success") {
					$(modalSetting).modal("hide");
					table.draw();
					successMessage('Berhasil !', 'Setting Berhasil Dilakukan.');
					return;
				}
			});
		}
	};
	
	settingApp.initSettingForm();

	listen("<?php echo $pUid; ?>");
});

</script>