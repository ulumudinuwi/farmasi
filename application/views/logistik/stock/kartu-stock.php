<style type="text/css">
  .table > tbody > tr > td {
    vertical-align: top;
  }
</style>
<?php echo messages(); ?>
<div class="content">
  <div class="panel panel-flat">
    <div class="panel-body uppercase">
      <div class="no-padding-top">
        <div class="table-responsive">
          <table id="table" class="table table-bordered table-striped">
            <thead>
              <tr class="bg-slate">
                <th class="text-center">Tanggal</th>
                <th class="text-center">Kode</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center" style="width: 15%;">Masuk</th>
                <th class="text-center" style="width: 15%;">Keluar</th>
                <th class="text-center" style="width: 15%;">Saldo</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
var table,
    btnRefresh;

var url = {
  loadData: "<?php echo site_url('api/logistik/stock/load_data_kartu_stock?uid=:UID'); ?>",
  listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID'); ?>",
}

function listen(q) {
    eventSource = new EventSource(url.listen.replace(':UID', q));
    eventSource.addEventListener('logistik-' + q, function(e) {
        table.draw(false);
    }, false);
}

$(window).ready(function () {
  table = $('#table').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": url.loadData.replace(':UID', '<?php echo $param; ?>'),
      "type": "POST",
      "data": function(p) {
        p.status = $('#search_status').val();
      }
    },
    "columns": [
      { 
        "data": "created_at",
        "render": function (data, type, row, meta) {
          return `<label data-tipe="${row.tipe}">${moment(data).format('DD-MM-YYYY HH:mm')}</label>`;
        },
      },
      { 
        "data": "kode",
        "orderable": false,
      },
      { 
        "data": "keterangan",
        "orderable": false,
        "searchable": false,
      },
      { 
        "data": "masuk",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          return numeral(data).format('0.0,');
        },
        "className": "text-right"
      },
      { 
        "data": "keluar",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          return numeral(data).format('0.0,');
        },
        "className": "text-right"
      },
      { 
        "data": "sisa_sum",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          return numeral(data).format('0.0,');
        },
        "className": "text-right"
      },
    ],
    "drawCallback": function (oSettings) {
      $('[data-toggle=tooltip]').tooltip();

      let tr = $('#table > tbody > tr');
      tr.each(function() {
        let tipe = parseInt($(this).find('label').data('tipe'));
        if(tipe === 7) {
          $(this).find('td:eq(4)').remove();
          $(this).find('td:eq(3)').attr('colspan', 2).addClass('text-center');
        }
      })
    },
  });
  listen("<?php echo $pUid; ?>");
});

</script>