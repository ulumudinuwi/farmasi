<style>
	.table > thead > tr > th {
		white-space: nowrap;
	    vertical-align: middle;
		text-align: center;
	}

	.table > tbody > tr > td {
		white-space: nowrap;
	    vertical-align: top;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
                                <label class="col-lg-4 control-label">Nomor</label>
                                <div class="col-lg-6">
                                    <div class="form-control-static text-bold" id="detail_kode"></div>
                                </div>
                            </div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
                                <label class="col-lg-4 control-label">Tanggal</label>
                                <div class="col-lg-6">
                                    <div class="form-control-static text-bold" id="detail_tanggal"></div>
                                </div>
                            </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
                                <label class="col-lg-4 control-label">Pemeriksa Pertama</label>
                                <div class="col-lg-6">
                                    <div class="form-control-static text-bold" id="detail_pengecekan1_by"></div>
                                </div>
                            </div>
						</div>
					</div>
		            <div class="row mt-20">
		                <div class="col-sm-12">
		                    <fieldset>
		                        <legend class="text-bold">
		                            <i class="icon-list position-left"></i> <strong>Daftar Barang</strong>
		                        </legend>
		                        <div class="row">
		                            <div class="col-sm-12">
		                                <div class="table-responsive">
		                                    <table id="table_detail" class="table table-bordered">
		                                        <thead>
		                                        	<tr class="bg-slate">
		                                                <th colspan="4">Pemeriksaan Pertama</th>
		                                                <th colspan="4">Pemeriksaan Kedua</th>
		                                            </tr>
		                                            <tr class="bg-slate">
		                                                <th rowspan="2">Nama</th>
		                                                <th rowspan="2">Satuan</th>
		                                                <th colspan="3">Stock</th>
		                                                <th colspan="2" width="30%;">Stock</th>
		                                                <th rowspan="2" width="8%;">Check</th>
		                                            </tr>
		                                            <tr class="bg-slate">
		                                            	<th>Sistem</th>
									                    <th>Fisik</th>
									                    <th>Selisih</th>
									                    <th>Fisik</th>
									                    <th>Selisih</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody></tbody>
		                                    </table>
		                                </div>
		                            </div>
		                        </div>
		                    </fieldset>
		                </div>
		            </div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="">
						<input type="hidden" id="kode" name="kode" value="">
						<input type="hidden" id="tanggal" name="tanggal" value="">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>