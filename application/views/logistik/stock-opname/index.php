<?php echo messages(); ?>
<style type="text/css">
  .table > thead > tr > th {
    white-space: nowrap;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
    white-space: nowrap;
    vertical-align: top;
  }
</style>
<div class="panel panel flat">
  <div class="tabbale">
    <ul class="nav nav-tabs nav-tabs-highlight">
      <li class="active">
        <a href="#tab-1" data-toggle="tab">Stock Opname</a>
      </li>
      <li>
        <a href="#tab-2" data-toggle="tab">Double Check</a>
      </li>
      <li>
        <a href="#tab-3" data-toggle="tab">History</a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab-1">
        <form class="form-horizontal" id="form">
          <div class="panel-body">  
            <div class="row">
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label class="control-label col-md-3">Tanggal</label>
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                        <i class="icon-calendar22"></i>
                      </span>
                      <input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal" disabled="true">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr class="no-margin-bottom no-margin-top">
          <div class="panel-body">
            <div class="table-responsive">
              <p class="text-info"><i class="icon-info22"></i> Kolom fisik jangan diisi jika tidak dilakukan stock opname.</p>
              <table id="table" class="table table-bordered table-striped">
                <thead>
                  <tr class="bg-slate">
                    <th rowspan="2">Nama Barang</th>
                    <th rowspan="2" width="15%;">Satuan</th>
                    <th class="text-center" colspan="3" width="32%;">Stock</th>
                    <th class="text-center" rowspan="2" width="8%;">Check</th>
                  </tr>
                  <tr class="bg-slate">
                    <th class="text-center">Sistem</th>
                    <th class="text-center">Fisik</th>
                    <th class="text-center">Selisih</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
          <div class="panel-footer">
            <div class="heading-elements">
              <div class="heading-btn pull-right mt-10 mb-10">
                <button type="submit" class="btn btn-success btn-labeled btn-save">
                  <b><i class="icon-floppy-disk"></i></b>
                  Simpan
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="tab-pane" id="tab-2">
        <div class="panel-body no-padding-top">
          <div class="table-responsive">
            <table id="table-double" class="table table-bordered table-striped">
              <thead>
                <tr class="bg-slate">
                  <th width="25%">Nomor</th>
                  <th>Tanggal</th>
                  <th width="25%"></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab-3">
        <div class="panel-body form-horizontal">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <div class="form-group">
                <label class="control-label col-md-3" for="search_history_tanggal">Tanggal</label>
                <div class="col-md-8">
                  <div class="input-group">
                    <span class="input-group-addon cursor-pointer" id="btn_search_history_tanggal">
                      <i class="icon-calendar22"></i>
                    </span>
                    <input type="text" id="search_history_range_tanggal" class="form-control rangetanggal-form">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr class="no-margin-bottom no-margin-top">
        <div class="panel-body no-padding-top">
          <div class="table-responsive">
            <table id="table-history" class="table table-bordered table-striped">
              <thead>
                <tr class="bg-slate">
                  <th width="25%">Nomor</th>
                  <th>Tanggal</th>
                  <th width="25%">Pemeriksa Pertama</th>
                  <th width="25%">Pemeriksa Kedua</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('logistik/stock-opname/detail-modal'); ?>