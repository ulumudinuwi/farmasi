<style type="text/css">
  .table-detail > thead > tr > th {
    white-space: nowrap;
    vertical-align: middle;
    text-align: center;
  }
</style>
<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h6 class="modal-title">Detail Stock Opname</h6>
      </div>
      <div class="modal-body form-horizontal">
        <div class="row mb-20">
          <fieldset>
            <div class="col-md-12">
              <legend class="text-bold"><i class="icon-magazine position-left"></i> Data Stock Opname</legend>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-lg-4 control-label">Nomor</label>
                <div class="col-md-6">
                  <div class="form-control-static text-bold detail_kode"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">Tanggal</label>
                <div class="col-lg-6">
                  <div class="form-control-static text-bold detail_tanggal"></div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-lg-4 control-label">Pemeriksa Pertama</label>
                <div class="col-lg-6">
                  <div class="form-control-static text-bold detail_pengecekan1_by"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-4 control-label">Pemeriksa Kedua</label>
                <div class="col-lg-6">
                  <div class="form-control-static text-bold detail_pengecekan2_by"></div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="row mb-20">
          <div class="col-sm-12">
            <fieldset>
              <legend class="text-bold">
                <i class="icon-list"></i> <strong>Daftar Barang</strong>
              </legend>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-detail">
                      <thead>
                        <tr class="bg-slate">
                          <th colspan="5">Pemeriksaan Pertama</th>
                          <th colspan="2">Pemeriksaan Kedua</th>
                        </tr>
                        <tr class="bg-slate">
                          <th rowspan="2">Nama</th>
                          <th rowspan="2">Satuan</th>
                          <th colspan="3">Stock</th>
                          <th colspan="2" width="30%;">Stock</th>
                        </tr>
                        <tr class="bg-slate">
                          <th>Sistem</th>
                          <th>Fisik</th>
                          <th>Selisih</th>
                          <th>Fisik</th>
                          <th>Selisih</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
      <div class="modal-footer m-t-none">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>