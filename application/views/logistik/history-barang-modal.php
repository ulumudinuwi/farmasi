<div id="history-barang-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">5 History Barang Terakhir</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row form-horizontal">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Barang</label>
                                    <div class="col-md-8">
                                        <div class="form-control-static text-bold" id="detail-history-barang"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Satuan</label>
                                    <div class="col-md-8">
                                        <div class="form-control-static text-bold" id="detail-history-satuan"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="table-history-barang" class="table table-bordered table-striped">
                                <thead>
                                    <tr class="bg-slate">
                                        <th>Transaksi</th>
                                        <th>Tanggal</th>
                                        <th>Kode</th>
                                        <th>Qty</th>
                                        <th>Sisa</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><b>Tutup</b></button>
            </div>
        </div>
    </div>
</div>