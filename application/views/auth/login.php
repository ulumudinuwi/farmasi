<form role="form" method="post">
	<div class="panel panel-body login-form">
		<div class="text-center">
			<img src="<?= base_url(get_option('rs_logo')) ?>" style="width: 100%;" />
			<h5 class="content-group">Verifikasi Keamanan<small class="display-block">Silahkan login terlebih dahulu</small></h5>
		</div>
		
		<?php echo messages(); ?>

		<div class="form-group has-feedback has-feedback-left">
			<input type="text" class="form-control" style="text-transform: lowercase !important;" placeholder="<?php echo lang('username'); ?>" name="username" autofocus value="<?php echo (isset($username)) ? $username : ''; ?>">
			<div class="form-control-feedback">
				<i class="icon-user text-muted"></i>
			</div>
		</div>
		
		<div class="form-group has-feedback has-feedback-left">
			<input type="password" class="form-control" placeholder="<?php echo lang('password'); ?>" name="password"  value="" />
			<div class="form-control-feedback">
				<i class="icon-lock2 text-muted"></i>
			</div>
		</div>

		<div class="form-group">
			<button  name="login-button" type="submit" class="btn bg-primary-800 btn-block"><i class="icon-key position-left"></i><?php echo lang('login'); ?> </button>
		</div>

		<!-- <div class="form-group text-center">
			<a href="<?php echo site_url('layar/layar'); ?>" target="_blank" class="btn btn-link"><i class="icon-screen3"></i></a>
		</div> -->

	</div>
</form>
