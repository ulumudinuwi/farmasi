<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Controller for authenticate controllers.
 *
 * @package CI-Beam
 * @category Controller
 * @author Ardi Soebrata
 */
class Registration_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->template->set_layout('self_registration');
    }
}
