<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Controller for authenticate controllers.
 * 
 * @package CI-Beam
 * @category Controller
 * @author Ardi Soebrata
 */
class Admin_Angular_Controller extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->template->set_layout('admin_angular');
	}
}
