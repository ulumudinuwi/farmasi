<?php 
    function insertLabarugi($data) {
        $CI =& get_instance();

        $dataInsert = array(
            'transaksi_id'  => isset($data->transaksi_id) ? $data->transaksi_id : null,
            'transaksi_detail_id'  => isset($data->transaksi_detail_id) ? $data->transaksi_detail_id : null,
            'perkiraan_id'  => isset($data->perkiraan_id) ? $data->perkiraan_id : null,
            'modul'         => isset($data->modul) ? $data->modul : null,
            'total'         => isset($data->total) ? $data->total : null,
            'keterangan'    => isset($data->keterangan) ? $data->keterangan : null,
            'bpom'          => isset($data->bpom) ? $data->bpom : null,
            'tipe'          => isset($data->tipe) ? $data->tipe : null,
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $CI->auth->userid(),
            'update_at'    => date('Y-m-d H:i:s'),
            'update_by'    => $CI->auth->userid(),
        );
        $CI->db->set('uid', 'UUID()', FALSE);
        $insert = $CI->db->insert('t_buku_besar', $dataInsert);

        return true;
    }

    function updateLabarugi($data) {
        $CI =& get_instance();

        $dataUpdate = array(
            'transaksi_id'  => isset($data->transaksi_id) ? $data->transaksi_id : null,
            'transaksi_detail_id'  => isset($data->transaksi_detail_id) ? $data->transaksi_detail_id : null,
            'perkiraan_id'  => isset($data->perkiraan_id) ? $data->perkiraan_id : null,
            'modul'         => isset($data->modul) ? $data->modul : null,
            'total'         => isset($data->total) ? $data->total : null,
            'bpom'          => isset($data->bpom) ? $data->bpom : null,
            'keterangan'    => isset($data->keterangan) ? $data->keterangan : null,
            'tipe'          => isset($data->tipe) ? $data->tipe : null,
            'update_at'    => date('Y-m-d H:i:s'),
            'update_by'    => $CI->auth->userid(),
        );
        if (isset($data->transaksi_detail_id)) {
            $CI->db->where(['transaksi_id' => $data->transaksi_id, 'transaksi_detail_id' => $data->transaksi_detail_id, 'modul'=> $data->modul]);
        }else{
            $CI->db->where(['transaksi_id' => $data->transaksi_id, 'modul'=> $data->modul]);
            $CI->db->where('transaksi_detail_id IS NULL', '', FALSE);
        }
        $CI->db->update('t_buku_besar', $dataUpdate);

        return true;
    }

    function deleteLabarugi($data) {
        $CI =& get_instance();

        if (isset($data->transaksi_detail_id)) {
            $CI->db->where(['transaksi_id' => $data->transaksi_id, 'transaksi_detail_id' => $data->transaksi_detail_id, 'modul'=> $data->modul]);
        }else{
            $CI->db->where(['transaksi_id' => $data->transaksi_id, 'modul'=> $data->modul]);
        }
        $CI->db->delete('t_buku_besar');

        return true;
    }

    function updateLabarugibyId($data) {
        $CI =& get_instance();

        $dataUpdate = array(
            'transaksi_id'  => isset($data->transaksi_id) ? $data->transaksi_id : null,
            'transaksi_detail_id'  => isset($data->transaksi_detail_id) ? $data->transaksi_detail_id : null,
            'perkiraan_id'  => isset($data->perkiraan_id) ? $data->perkiraan_id : null,
            'modul'         => isset($data->modul) ? $data->modul : null,
            'total'         => isset($data->total) ? $data->total : null,
            'keterangan'    => isset($data->keterangan) ? $data->keterangan : null,
            'bpom'          => isset($data->bpom) ? $data->bpom : null,
            'tipe'          => isset($data->tipe) ? $data->tipe : null,
            'update_at'    => date('Y-m-d H:i:s'),
            'update_by'    => $CI->auth->userid(),
        );
        $CI->db->where('transaksi_id IS NULL', '', FALSE);
        $CI->db->where('transaksi_detail_id IS NULL', '', FALSE);
        $CI->db->where(['id' => $data->id, 'modul'=> $data->modul]);

        $CI->db->update('t_buku_besar', $dataUpdate);

        return true;
    }
?>