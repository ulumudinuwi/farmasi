<?php

function to_rupiah($price){
    return number_format($price, 0, ",",".");
}

if ( ! function_exists('date_descr')) {
    function date_descr($date, $callbackFormat = null) {
        $hari = date('D', strtotime($date));
        $tanggal = date('j', strtotime($date));
        $bulan = date('M', strtotime($date));
        $tahun = date('Y', strtotime($date));

        $format = '%s Tanggal %s bulan %s tahun %s';

        $hari_str = konversi_to_id($hari);
        $tanggal_str = numbers_to_words($tanggal);
        $bulan_str = konversi_to_id($bulan);
        $tahun_str = numbers_to_words($tahun);

        if ($callbackFormat && is_callable($callbackFormat)) {
            $hari_str = $callbackFormat($hari_str);
            $tanggal_str = $callbackFormat($tanggal_str);
            $bulan_str = $callbackFormat($bulan_str);
            $tahun_str = $callbackFormat($tahun_str);
        }

        return sprintf($format, $hari_str, $tanggal_str, $bulan_str, $tahun_str);
    }
}

if ( ! function_exists('hari_descr')) {
	function hari_descr($index) {
		$CI =& get_instance();
        if ($index < $CI->config->item('ID_HARI_MINGGU') || $index > $CI->config->item('ID_HARI_SABTU'))
			return '';
		$hari = $CI->config->item('hari');
		return $hari[$index];
	}
}

if ( ! function_exists('konversi_to_id')) {
	function konversi_to_id($tanggal) {
		$format = array("Sun"	=> "Minggu",
						"Mon"	=> "Senin",
						"Tue"	=> "Selasa",
						"Wed"	=> "Rabu",
						"Thu"	=> "Kamis",
						"Fri"	=> "Jumat",
						"Sat"	=> "Sabtu",
						"Jan"	=> "Januari",
						"Feb"	=> "Februari",
						"Mar"	=> "Maret",
						"Apr"	=> "April",
						"May"	=> "Mei",
						"Jun"	=> "Juni",
						"Jul"	=> "Juli",
						"Aug"	=> "Agustus",
						"Sep"	=> "September",
						"Oct"	=> "Oktober",
						"Nov"	=> "November",
						"Dec"	=> "Desember"
					);
		return strtr($tanggal, $format);
	}
}

/*
|--------------------------------------------------------------------------
| Concat Tempat Lahir dan Tanggal Lahir Helper
|--------------------------------------------------------------------------
*/

if ( ! function_exists('concat_tempat_tanggal_lahir')) {
	function concat_tempat_tanggal_lahir($tempat_lahir, $tanggal_lahir) {
		if ((strlen($tempat_lahir) > 0) && (strlen($tanggal_lahir) > 0)) {
			return $tempat_lahir.", ".$tanggal_lahir;
		}
		else {
			return $tempat_lahir.$tanggal_lahir;
		}
	}
}

/*
|--------------------------------------------------------------------------
| Concat Umur Helper
|--------------------------------------------------------------------------
*/

if ( ! function_exists('concat_umur')) {
	function concat_umur($umur_tahun, $umur_bulan, $umur_hari) {
		$umur = array();
		$umur[] = strlen(strval($umur_tahun)) > 0 ? strval($umur_tahun)." th" : "";
		$umur[] = strlen(strval($umur_bulan)) > 0 ? strval($umur_bulan)." bl" : "";
		$umur[] = strlen(strval($umur_hari)) > 0 ? strval($umur_hari)." hr" : "";
		return implode(", ", $umur);
	}
}

/*
|--------------------------------------------------------------------------
| Hitung Umur Helper
|--------------------------------------------------------------------------
*/

if ( ! function_exists('get_age_difference')) {
	function get_age_difference($start_date, $end_date) {
		list($start_year, $start_month, $start_date) = explode('-', $start_date);
		list($current_year, $current_month, $current_date) = explode('-', $end_date);

		$result = array(
			'year'	=> 0,
			'month'	=> 0,
			'day'	=> 0
		);

		/** days of each month **/
		for ($x = 1 ; $x <= 12 ; $x++) {
			 $dim[$x] = date('t', mktime(0, 0, 0, $x, 1, date('Y')));
		}

		/** calculate differences **/
		$m = $current_month - $start_month;
		$d = $current_date - $start_date;
		$y = $current_year - $start_year;

		/** if the start day is ahead of the end day **/
		if ($d < 0) {
			$today_day = $current_date + $dim[intval($current_month)];
			$today_month = $current_month - 1;
			$d = $today_day - $start_date;
			$m = $today_month - $start_month;
			if (($today_month - $start_month) < 0) {
				$today_month += 12;
				$today_year = $current_year - 1;
				$m = $today_month - $start_month;
				$y = $today_year - $start_year;
			}
		}

		/** if start month is ahead of the end month **/
		if($m < 0) {
			$today_month = $current_month + 12;
			$today_year = $current_year - 1;
			$m = $today_month - $start_month;
			$y = $today_year - $start_year;
		}

		/** Calculate dates **/
		if ($y < 0) {
			$result['year'] = 0;
			$result['month'] = 0;
			$result['day'] = 0;
		} else {
			switch ($y) {
				case 0:
					$result['year'] = 0;
					break;
				case 1:
					$result['year'] = $y;
					break;
				default:
					$result['year'] = $y;
			}
			switch($m) {
				case 0:
					$result['month'] = 0;
					break;
				case 1:
					$result['month'] = $m;
					break;
				default:
					$result['month'] = $m;
			}
			switch($d) {
				case 0:
					$result['day'] = 0;
					break;
				case 1:
					$result['day'] = $d;
					break;
				default:
					$result['day'] = $d;
			}
		}
		return $result;
	}
}

if ( ! function_exists('numbers_to_words')) {
	function numbers_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'minus ';
		$decimal     = ' koma ';
		$dictionary  = array(
			0				=> 'nol',
			1				=> 'satu',
			2				=> 'dua',
			3				=> 'tiga',
			4				=> 'empat',
			5				=> 'lima',
			6				=> 'enam',
			7				=> 'tujuh',
			8				=> 'delapan',
			9				=> 'sembilan',
			10				=> 'sepuluh',
			11				=> 'sebelas',
			19				=> 'belas',
			90				=> 'puluh',
			100				=> 'seratus',
			999				=> 'ratus',
			1000			=> 'seribu',
			999999			=> 'ribu',
			999999999		=> 'juta',
			999999999999	=> 'milyar',
			999999999999999	=> 'trilyun'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
			);
			return false;
		}

		if ($number < 0) {
			return $negative . numbers_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
            if (intval($fraction) === 0) {
                $fraction = null;
            }
		}

		switch (true) {
			case $number < 12:
				$string = $dictionary[$number];
				break;
			case $number < 20:
				$tens   = 19;
				$units  = $number % 10;
				$string = $dictionary[$units] . ' ' . $dictionary[$tens];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10));
				$units  = $number % 10;
				$string = $dictionary[$tens] . ' ' . $dictionary[90];
				if ($units) {
					$string .= ' '.$dictionary[$units];
				}
				break;
			case $number == 100:
				$string = $dictionary[$number];
				break;
			case $number < 200:
				$remainder = $number % 100;
				$string = $dictionary[100];
				if ($remainder) {
					$string .= ' ' . numbers_to_words($remainder);
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[999];
				if ($remainder) {
					$string .= ' ' . numbers_to_words($remainder);
				}
				break;
			case $number == 1000:
				$string = $dictionary[$number];
				break;
			case $number < 2000:
				$remainder = $number % 1000;
				$string = $dictionary[1000];
				if ($remainder) {
					$string .= ' ' . numbers_to_words($remainder);
				}
				break;
			case $number < 1000000:
				$thousands  = (int) ($number / 1000);
				$remainder = $number % ($thousands * 1000);
				$string = numbers_to_words($thousands) . ' ' . $dictionary[999999];
				if ($remainder) {
					$string .= ' ' . numbers_to_words($remainder);
				}
				break;
			case $number < 1000000000:
				$millions  = (int) ($number / 1000000);
				$remainder = $number % ($millions * 1000000);
				$string = numbers_to_words($millions) . ' ' . $dictionary[999999999];
				if ($remainder) {
					$string .= ' ' . numbers_to_words($remainder);
				}
				break;
			case $number < 1000000000000:
				$billions  = (int) ($number / 1000000000);
				$remainder = $number % ($billions * 1000000000);
				$string = numbers_to_words($billions) . ' ' . $dictionary[999999999999];
				if ($remainder) {
					$string .= ' ' . numbers_to_words($remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = numbers_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= numbers_to_words($remainder);
				}
				break;
		}

		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}

		return $string;
	}
}

function words_to_numbers($data) {
    // Replace all number words with an equivalent numeric value
    $data = strtr(
        $data,
        array(
            'nol'      => '0',
            'satu'       => '1',
            'dua'       => '2',
            'tiga'     => '3',
            'empat'      => '4',
            'lima'      => '5',
            'enam'       => '6',
            'tujuh'     => '7',
            'delapan'     => '8',
            'sembilan'      => '9',
            'sepuluh'       => '10',
            'sebelas'    => '11',
            'dua belas'    => '12',
            'tiga belas'  => '13',
            'empat belas'  => '14',
            'lima belas'   => '15',
            'enam belas'   => '16',
            'tujuh belas' => '17',
            'delapan belas'  => '18',
            'sembilan belas'	=> '19',
            'dua puluh'			=> '20',
            'tiga puluh'    => '30',
            'empat puluh'     => '40',
            'lima puluh'     => '50',
            'enam puluh'     => '60',
            'tujuh puluh'   => '70',
            'delapan puluh'    => '80',
            'sembilan puluh'    => '90',
            'seratus'   => '100',
            'seribu'  => '1000',
            'satu juta'   => '1000000',
            'satu milyar'   => '1000000000',
            'and'       => '',
        )
    );

    // Coerce all tokens to numbers
    $parts = array_map(
        function ($val) {
            return floatval($val);
        },
        preg_split('/[\s-]+/', $data)
    );

    $stack = new SplStack; // Current work stack
    $sum   = 0; // Running total
    $last  = null;

    foreach ($parts as $part) {
        if (!$stack->isEmpty()) {
            // We're part way through a phrase
            if ($stack->top() > $part) {
                // Decreasing step, e.g. from hundreds to ones
                if ($last >= 1000) {
                    // If we drop from more than 1000 then we've finished the phrase
                    $sum += $stack->pop();
                    // This is the first element of a new phrase
                    $stack->push($part);
                } else {
                    // Drop down from less than 1000, just addition
                    // e.g. "seventy one" -> "70 1" -> "70 + 1"
                    $stack->push($stack->pop() + $part);
                }
            } else {
                // Increasing step, e.g ones to hundreds
                $stack->push($stack->pop() * $part);
            }
        } else {
            // This is the first element of a new phrase
            $stack->push($part);
        }

        // Store the last processed part
        $last = $part;
    }

    return $sum + $stack->pop();
}

if ( ! function_exists('convert_format_time')) {
  function convert_format_time($time,$format='24') {
    $result = FALSE;
    if($format == '24')
      $result  = date('H:i:s', strtotime($time));
    elseif($format == '12')
      $result  = date('h:i a');
    return $result;
  }
}

if ( ! function_exists('delete_file')) {
  function delete_file($uri_file) {
    if(file_exists('.'.$uri_file)){
      unlink('.'.$uri_file);
      return true;
    }
    else
      return false;

  }
}

if ( ! function_exists('restruct_date')) {
  function restruct_date($tgl){
    if($tgl != '')
    {
      $t = explode('-', $tgl);
      if(count($t)>2)
      {
        $temp = $t[2];
        $t[2] = $t[0];
        $t[0] = $temp;
      }
      return implode('-', $t);
    }
    else
      return '';
  }
}

if ( ! function_exists('long_date')) {
  function long_date($tgl){
    if($tgl)
    {
      $t = explode('-', $tgl);
      return $t[2].' '.nama_bulan($t[1]).' '.$t[0];
    }
    else
      return '';
  }
}

if ( ! function_exists('nama_bulan')) {
  function nama_bulan($bln){
      $bulan = [];
      $bulan['01'] = 'Januari';
      $bulan['02'] = 'Februari';
      $bulan['03'] = 'Maret';
      $bulan['04'] = 'April';
      $bulan['05'] = 'Mei';
      $bulan['06'] = 'Juni';
      $bulan['07'] = 'Juli';
      $bulan['08'] = 'Agustus';
      $bulan['09'] = 'September';
      $bulan['10'] = 'Oktober';
      $bulan['11'] = 'November';
      $bulan['12'] = 'Desember';
      return $bulan[$bln];
  }
}

if ( ! function_exists('nama_bulan_int')) {
  function nama_bulan_int($bln){
      $bulan = [];
      $bulan['1'] = 'Januari';
      $bulan['2'] = 'Februari';
      $bulan['3'] = 'Maret';
      $bulan['4'] = 'April';
      $bulan['5'] = 'Mei';
      $bulan['6'] = 'Juni';
      $bulan['7'] = 'Juli';
      $bulan['8'] = 'Agustus';
      $bulan['9'] = 'September';
      $bulan['10'] = 'Oktober';
      $bulan['11'] = 'November';
      $bulan['12'] = 'Desember';
      return $bulan[$bln];
  }
}

if ( ! function_exists('next_bulan')) {
  function next_bulan($bln){
      $bln++;
      if($bln>12)
        $bln = 1;
      return $bln;
  }
}

if ( ! function_exists('list_bulan')) {
  function list_bulan(){
      $bulan = [];
      $bulan['']   = '(PILIH)';
      $bulan['01'] = 'Januari';
      $bulan['02'] = 'Februari';
      $bulan['03'] = 'Maret';
      $bulan['04'] = 'April';
      $bulan['05'] = 'Mei';
      $bulan['06'] = 'Juni';
      $bulan['07'] = 'Juli';
      $bulan['08'] = 'Agustus';
      $bulan['09'] = 'September';
      $bulan['10'] = 'Oktober';
      $bulan['11'] = 'November';
      $bulan['12'] = 'Desember';
      return $bulan;
  }
}

if ( ! function_exists('list_tahun')) {
  function list_tahun($current_tahun){
      $tahun = [];
      $tahun['']   = '(PILIH)';
      for ($thn = $current_tahun-2; $thn <= $current_tahun+5 ; $thn++) {
        $tahun[$thn] = $thn;
      }
      return $tahun;
  }
}



if ( ! function_exists('format_time')) {
  function format_time($time,$format='H:i:s') {
    $result  = date($format, strtotime($time));
    return $result;
  }
}

if ( ! function_exists('get_penggunaan_bmhp')) {
    function get_penggunaan_bmhp($layanan_id, $tarif_pelayanan_id, $layanan = null) {
        $CI =& get_instance();
        $isBmhpActive = get_setting('opt_penggunaan_bmhp');

        if (! $layanan) {
            $layanan = $CI->db->where('id', $layanan_id)
                            ->get('m_layanan')
                            ->row();
        }

        // if ($isBmhpActive == 0) return [];
        if ($isBmhpActive == 0 && $layanan->jenis != 4) return [];

        $CI->load->model('master/Tindakan_obat_model');

        $tarif = $CI->db->where('id', $tarif_pelayanan_id)
                        ->get('m_tarif_pelayanan')
                        ->row();

        $result = $CI->Tindakan_obat_model->get_bmhp($layanan_id, $tarif_pelayanan_id);

        if ($tarif->parent_id != $layanan->tarip_layanan_id && $layanan->tarip_layanan_id != 0) {
            return array_merge($result, get_penggunaan_bmhp($layanan_id, $tarif->parent_id, $layanan));
        }

        return $result;
    }
}

if ( ! function_exists('get_database_connection')) {
    function get_database_connection($unit_usaha_kode) {
        $CI =& get_instance();
        //
        return $CI->db;
    }
}

if ( ! function_exists('get_database_instance')) {
    function get_database_instance($setting) {
        $CI =& get_instance();
        $instance = null;
        //

        return $instance;
    }
}

if ( ! function_exists('get_database_setting')) {
    function get_database_setting($obj) {
        $setting = new stdClass();
        $setting->driver = $obj->value->driver;
        $setting->hostname = $obj->value->hostname;
        $setting->port = $obj->value->port;
        $setting->username = $obj->value->username;
        $setting->password = $obj->value->password;
        $setting->database = $obj->value->database;

        return $setting;
    }
}

if ( ! function_exists('check_database_connection')) {
    function check_database_connection($setting, $timeout = 10) {
        $connected = false;
        try {
            $setting->driver = $setting->driver == 'mysqli' ? 'mysql' : $setting->driver;
            $host = sprintf('%s:host=%s;port=%s;dbname=%s', $setting->driver, $setting->hostname, $setting->port, $setting->database);
            $pdoSetting = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_TIMEOUT => $timeout
            );
            $dbh = new pdo($host, $setting->username, $setting->password, $pdoSetting);
            $connected = true;
        } catch(PDOException $ex){
            $connected = false;
        } catch (Exception $ex) {
            $connected = false;
        }

        $dbh = null;
        return $connected;
    }
}

if ( ! function_exists('check_database_connection_by_kode')) {
    function check_database_connection_by_kode($kode_unit) {
        $CI =& get_instance();
    }
}

if ( ! function_exists('get_setting')) {
    function get_setting($key) {
        $CI =& get_instance();

        $row = $CI->db->where('key', $key)
                    ->get('m_settings')
                    ->row();

        return $row ? $row->value : "";
    }
}

if ( ! function_exists('array_json')) {
    function array_json($data) {
      if($data)
      {
        $d = explode(',', $data);
        return json_encode($d);
      }
      else
        return '';
    }
}

if ( ! function_exists('date_time_indo')) {
  function date_time_indo($datetime){
    $datetime = explode(' ', $datetime);

    $tgl = $datetime[0];
    $pos = strpos($tgl, '/');
    if ($pos !== false) {
      $tgl = str_replace("/","-",$tgl);
    }

    if($tgl)
    {
      $t = explode('-', $tgl);

      $temp = $t[2];
      $t[2] = $t[0];
      $t[0] = $temp;
      return implode('-', $t).' '.$datetime[1];
    }
    else
      return '';
  }
}

if ( ! function_exists('date_indo')) {
  function date_indo($tgl){
    $datetime = explode(' ', $tgl);
    if(count($datetime)>1)
      $tgl = $datetime[0];

    $pos = strpos($tgl, '/');
    if ($pos !== false) {
      $tgl = str_replace("/","-",$tgl);
    }

    if($tgl)
    {
      $t = explode('-', $tgl);

      $temp = $t[2];
      $t[2] = $t[0];
      $t[0] = $temp;
      return implode('-', $t);
    }
    else
      return '';
  }
}

if (! function_exists('get_kamar_obat')) {
    function get_kamar_obat($layanan_id, $ruang_id = 0, $kelas_id = 0) {
        $CI =& get_instance();
        $CI->db->where('layanan_id', $layanan_id);

        if ($ruang_id) {
            $CI->db->where('ruang_id', $ruang_id);
        }

        if ($kelas_id) {
            $CI->db->where('kelas_id', $kelas_id);
        }

        $row = $CI->db->get('m_kamar_obat_layanan')->row();

        if ($row) {
            $kamar_obat = $CI->db->where('id', $row->kamar_obat_id)
                ->get('m_kamar_obat')->row();

            if ($kamar_obat) {
                $kamar_obat->layanan_id = $row->layanan_id;
                $kamar_obat->ruang_id = $row->ruang_id;

                return $kamar_obat;
            }
        }

        return null;
    }
}

if (! function_exists('dropdown_array')) {
    function dropdown_array($array_data, $caption='nama', $value='id') {
        $result = array();
        foreach ($array_data as $arr) {
        	$result[$arr->{$value}] = $arr->{$caption};
        }
        return $result;
    }
}

if (! function_exists('get_unit_usaha_kota')) {
    function get_unit_usaha_kota() {
        $CI =& get_instance();

        return ucwords($CI->config->item('unit_usaha_kota'));
    }
}

if (! function_exists('get_unit_usaha_format_alamat')) {
    function get_unit_usaha_format_alamat() {
        $CI =& get_instance();

        $alamat = '';
        switch ($CI->config->item('unit_usaha_kode')) {
            case '02': // RSBT PKP
                $alamat = 'Jl. Bukit Baru No. 1 Pangkalpinang (0717) 421091';
                break;
            case '03': // RSBT Karimun
                $alamat = 'Jl. Canggai Putri, Kec. Tebing, Kab. Karimun (0777) 7367085';
                break;
            case '04': // Medikastannia
                $alamat = 'Jl. Jendral Sudirman No. 3 Sungailiat (0717) 95837';
                break;
            case '05': // RSBT Muntok
                $alamat = 'Jl. Raya Timah, Komp. Unit Metalurgi Muntok (0716) 21008';
                break;
            case '06': // RSBT Belinyu
                $alamat = 'Jl. Depati Amir No. 1E, Belinyu (0715) 321365';
                break;
            case '07': // RSBT Parittiga
                $alamat = 'Jl. Raya Parit Tiga, Parit Tiga (0715) 351143';
                break;
            case '08': // RSBT Toboali
                $alamat = 'Jl. Jend. Sudirman No. 227, Toboali (0718) 41771';
                break;
            case '09': // RSBT TJPandan
                $alamat = 'Jl. Melati, Tanjung Pandan (0719) 21865';
                break;
        }

        return $alamat;
    }
}

if (! function_exists('get_unit_usaha_format_rs')) {
    function get_unit_usaha_format_rs($abbr = true) {
        $CI =& get_instance();

        $alamat = '';
        if ($abbr) {
            switch ($CI->config->item('unit_usaha_kode')) {
                case '02': // RSBT PKP
                    $alamat = 'RS. Bakti Timah Pangkalpinang';
                    break;
                case '03': // RSBT Karimun
                    $alamat = 'RS. Bakti Timah Karimun';
                    break;
                case '04': // Medikastannia
                    $alamat = 'RS. Medika Stannia Sungailiat';
                    break;
                case '05': // RSBT Muntok
                    $alamat = 'RS. Bakti Timah Muntok';
                    break;
                case '06': // RSBT Belinyu
                    $alamat = 'Klinik Medika Stannia Belinyu';
                    break;
                case '07': // RSBT Parittiga
                    $alamat = 'Klinik Bakti Timah Parit Tiga';
                    break;
                case '08': // RSBT Toboali
                    $alamat = 'Klinik Bakti Timah Toboali';
                    break;
                case '09': // RSBT TJPandan
                    $alamat = 'Klinik Bakti Timah Tanjung Pandan';
                    break;
            }
        } else {
            switch ($CI->config->item('unit_usaha_kode')) {
                case '02': // RSBT PKP
                    $alamat = 'Rumah Sakit Bakti Timah Pangkalpinang';
                    break;
                case '03': // RSBT Karimun
                    $alamat = 'Rumah Sakit Bakti Timah Karimun';
                    break;
                case '04': // Medikastannia
                    $alamat = 'Rumah Sakit Medika Stannia Sungailiat';
                    break;
                case '05': // RSBT Muntok
                    $alamat = 'Rumah Sakit Bakti Timah Muntok';
                    break;
                case '06': // RSBT Belinyu
                    $alamat = 'Klinik Medika Stannia Belinyu';
                    break;
                case '07': // RSBT Parittiga
                    $alamat = 'Klinik Bakti Timah Parit Tiga';
                    break;
                case '08': // RSBT Toboali
                    $alamat = 'Klinik Bakti Timah Toboali';
                    break;
                case '09': // RSBT TJPandan
                    $alamat = 'Klinik Bakti Timah Tanjung Pandan';
                    break;
            }
        }

        return $alamat;
    }
}

if (! function_exists('get_db_unit')) {
    function get_db_unit($id) {
        $CI =& get_instance();
        $unit_usaha = $CI->db->where('id', $id)
            ->get('m_unitusaha')->row();

        if (! $unit_usaha) return null;

        $db_instance = null;
        if ($unit_usaha->id != $CI->session->userdata('unit_usaha_id')) {
            $setting = $CI->db->where("`key` LIKE 'db_{$unit_usaha->kode}'", null, FALSE)
                ->get('m_settings')->row();
            if (! $setting) return null;

            $db = json_decode($setting->value);

            $config = array(
                'hostname' => $db->hostname,
                'username' => $db->username,
                'password' => $db->password,
                'database' => $db->database,
                'dbdriver' => $db->driver,
                'dbprefix' => '',
                'pconnect' => FALSE,
                'db_debug' => TRUE,
                'cache_on' => FALSE,
                'cachedir' => '',
                'char_set' => 'utf8',
                'dbcollat' => 'utf8_general_ci',
            );

            $db_instance = $CI->load->database($config, TRUE);
        } else {
            $db_instance = $CI->db;
        }

        return $db_instance;
    }
}

if(!function_exists('default_directory')) {
  	function default_directory($folder_name = "") {
  		if($folder_name != "") {
  			$uploads_dir = "./uploads";
  			$dir = "./uploads/{$folder_name}";
  			$ori_dir = "./uploads/{$folder_name}/original";
            $thumb_dir = "./uploads/{$folder_name}/thumbnail";

            make_directory($uploads_dir);
            make_directory($dir);
            make_directory($ori_dir);
            make_directory($thumb_dir);
  		}
  	}
}

if(!function_exists('make_directory')) {
  	function make_directory($dir) {
    	if (!file_exists($dir)) {
      		mkdir($dir, 0777, true);
      		copy('./assets/index.html', $dir.'/index.html');
    	}
  	}
}

if(! function_exists('proc_do_upload')) {
	function proc_do_upload($input_name, $folder = '', $config = [], $encrypt = FALSE) {
		$CI =& get_instance();
        if (!file_exists($folder)) make_directory($folder);

        $config['upload_path'] = $folder;
        $config['file_ext_tolower'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['max_size'] = 0;
        if($encrypt) 
            $config['file_name'] = uniqid().'-'.time();

        $CI->load->library('upload');
        $CI->upload->initialize($config, TRUE);

        if (!$CI->upload->do_upload($input_name)) {
            $message = array('error' => $CI->upload->display_errors());
            return [
                'message' => $message,
            ];
        } else {
            $file = $CI->upload->data();
            $message = array('upload_data' => $file);

            return [
                'config' => $config,
                'message' => $message,
                'file' => $file
            ];
        }
    }
}

if(! function_exists('get_date_accepted_db')) {
	function get_date_accepted_db($date = "12/12/2020") {
		$cDate = str_replace('/', '-', $date);
		return date('Y-m-d', strtotime($cDate));
	}
}

if ( ! function_exists('get_jenis_racikan')) {
  function get_jenis_racikan($i){
      $array = [];
      $array['1'] = 'Kapsul';
      $array['2'] = 'Sirup';
      $array['3'] = 'Salep';
      $array['99'] = 'Lainnya';
      return $array[$i];
  }
}

if (! function_exists('footer_imedis')) {
    function footer_imedis($html=false) {
        $text = 'Powered by www.imedis.co.id';
        if ($html)
            return '<p style="position: static;top: 0;font-size: 6px;color: #999;font-weight: bold;text-align: center;">'.$text.'</p>';
        return $text;
    }
}

if (! function_exists('get_status')) {
	function get_status($valArr, $configArr) {		
		$aData = array();
	    foreach ($valArr as $key) {
	        $aData[$key] = $configArr[$key];
	    }
	    return $aData;
	}
}

if(!function_exists('assets_url_version')) {
    function assets_url_version($filename) {
        $file = base_url($filename);
        $version = '?version='.md5_file(FCPATH.$filename);

        return $file.$version;
    }
}

if (! function_exists('script_url')) {
    function script_url($filename) {
        $file = base_url($filename);
        $version = '?version='.md5_file(FCPATH.$filename);

        return $file.$version;
    }
}

if (! function_exists('checkBase64Value')) {
	function checkBase64Value($value = 0) {
		$result = $value;
		if(base64_encode(base64_decode($value, true)) == $value) $result = base64_decode($value);
		return $result;
	}
}