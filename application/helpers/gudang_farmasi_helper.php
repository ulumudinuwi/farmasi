<?php
if(! function_exists('procStockPenerimaan')) {
	function procStockPenerimaan($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_gudang_farmasi_stock';
		$table_def_stock_detail = 't_gudang_farmasi_stock_detail';
		$table_def_kartu_stock = 't_gudang_farmasi_kartu_stock';
		$table_def_barang = 'm_barang';
		
		$checkExists = $CI->db->where('barang_id', $detail->barang_id)
								->get($table_def_stock);

		$stock = 0;
		$stockDetail = 0;
		if($checkExists->num_rows() > 0) {
			$stock_id = $checkExists->row()->id;
			$stock = $checkExists->row()->qty;

			$CI->db->set('qty', 'qty + '.$detail->qty, FALSE)
					->set('dipesan', $detail->dipesan)
					->set('update_at', date('Y-m-d H:i:s'))
					->set('update_by', $CI->auth->userid())
					->where('id', $stock_id)
					->update($table_def_stock);
		} else {
			$objNew = array(
				'barang_id' => $detail->barang_id,
				'qty' => $detail->qty,
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $CI->auth->userid(),
			);
			$CI->db->insert($table_def_stock, $objNew);
			$stock_id = $CI->db->insert_id();
		}

		# GET AND UPDATE STOCK DETAIL
		if(count($detail->data_barang) > 0) {
			foreach ($detail->data_barang as $oBarang) {
				$checkExists = $CI->db->where('stock_id', $stock_id)
					->where('harga', $detail->harga)
					->where('expired_date', $oBarang->expired_date)
			 		->get($table_def_stock_detail);	

			 	if($checkExists->num_rows() > 0) {
					$stock_detail_id = $checkExists->row()->id;
					$CI->db->set('qty_awal', 'qty_awal + '.($oBarang->qty * $detail->isi_satuan), FALSE)
							->set('qty', 'qty + '.($oBarang->qty * $detail->isi_satuan), FALSE)
							->set('update_at', date('Y-m-d H:i:s'))
							->set('update_by', $CI->auth->userid())
							->where('id', $stock_detail_id)
							->update($table_def_stock_detail);

					$stockDetail = $checkExists->row()->qty + ($oBarang->qty * $detail->isi_satuan);
				} else {
					$objNew = get_object_vars($oBarang);
					$objNew['stock_id'] = $stock_id;
					$objNew['harga'] = $detail->harga;
					$objNew['qty_awal'] = ($oBarang->qty * $detail->isi_satuan);
					$objNew['qty'] = ($oBarang->qty * $detail->isi_satuan);
					$objNew['created_at'] = date('Y-m-d H:i:s');
					$objNew['created_by'] = $CI->auth->userid();
					$CI->db->insert($table_def_stock_detail, $objNew);
					$stock_detail_id = $CI->db->insert_id();

					$stockDetail = ($oBarang->qty * $detail->isi_satuan);
				}

				$stock += $detail->qty;

				# KARTU STOCK
				$kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $detail->barang_id;
				$kartu->stock_detail_id = $stock_detail_id;
				$kartu->masuk 	  = ($oBarang->qty * $detail->isi_satuan);
				$kartu->harga = $detail->harga;
				$kartu->no_batch = $oBarang->no_batch;
				$kartu->expired_date = $oBarang->expired_date;
				$kartu->sisa 	  = $stockDetail;
				$kartu->sisa_sum  = $stock;
				$kartu->keterangan = isset($obj->keterangan) ? $obj->keterangan : '';
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);
			}
		} else {
			$checkExists = $CI->db->where('stock_id', $stock_id)
				->where('harga', $detail->harga)
				->where('expired_date IS NULL')
		 		->get($table_def_stock_detail);

			if($checkExists->num_rows() > 0) {
				$stock_detail_id = $checkExists->row()->id;
				$CI->db->set('qty_awal', 'qty_awal + '.$detail->qty, FALSE)
						->set('qty', 'qty + '.$detail->qty, FALSE)
						->set('update_at', date('Y-m-d H:i:s'))
						->set('update_by', $CI->auth->userid())
						->where('id', $stock_detail_id)
						->update($table_def_stock_detail);
			} else {
				$objNew = array(
					'stock_id' => $stock_id,
					'harga' => $detail->harga,
					'no_batch' => '',
					'expired_date' => null,
					'qty' => $detail->qty,
					'qty_awal' => $detail->qty,
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => $CI->auth->userid(),
				);
				$CI->db->insert($table_def_stock_detail, $objNew);
				$stock_detail_id = $CI->db->insert_id();
			}

			$stock += $detail->qty;

			# KARTU STOCK
			$kartu = new stdClass();
			$kartu->tipe    = $tipe;
			$kartu->tanggal = $obj->tanggal;
			$kartu->kode = $obj->kode;
			$kartu->transaksi_id = $obj->id;
			$kartu->transaksi_detail_id = $detail->id;
			$kartu->barang_id = $detail->barang_id;
			$kartu->stock_detail_id = $stock_detail_id;
			$kartu->masuk 	  = $detail->qty;
			$kartu->harga 	  = $detail->harga;
			$kartu->no_batch = '';
			$kartu->expired_date = null;
			$kartu->sisa 	  = $detail->qty;
			$kartu->sisa_sum  = $stock;
			$kartu->keterangan = isset($obj->keterangan) ? $obj->keterangan : '';
			$kartu->created_by = $CI->auth->userid();
	        $kartu->created_at = date('Y-m-d H:i:s');
	        $CI->db->insert($table_def_kartu_stock, $kartu);
		}
	}
}

if(! function_exists('procStockKeluar')) {
	function procStockKeluar($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_gudang_farmasi_stock';
		$table_def_stock_detail = 't_gudang_farmasi_stock_detail';
		$table_def_kartu_stock = 't_gudang_farmasi_kartu_stock';

		$objStock = $CI->db->where('barang_id', $detail->barang_id)
							->get($table_def_stock)->row();


		$aDetailStock = array();
		$tmpQty = $detail->qty;
		do {
			$objStockDetail = $CI->db->select('id, qty, qty_awal, no_batch, expired_date, harga')
									->where('stock_id', $objStock->id)
									->where('qty >', 0)
									->order_by('expired_date', 'ASC')
									->limit(1)
									->get($table_def_stock_detail)->row();

            if($objStockDetail) {
                $tmpStockSisa = $tmpQty - $objStockDetail->qty;
                $stock_sisa = $tmpQty - $objStockDetail->qty;
                if($stock_sisa > 0) {
                    $stock_sisa = 0;
                    $sendJumlah = $objStockDetail->qty;
                } else {
                    $stock_sisa = $objStockDetail->qty - $tmpQty;
                    $sendJumlah = $tmpQty;
                }
                $tmpQty = $tmpStockSisa;
                $CI->db->set('qty', $stock_sisa)
                		->where('id', $objStockDetail->id)
                		->update($table_def_stock_detail);

                $CI->db->set('qty', 'qty - '.$sendJumlah, FALSE)
                		->set('update_at', date('Y-m-d H:i:s'))
						->set('update_by', $CI->auth->userid())
						->where('id', $objStock->id)
						->update($table_def_stock);

				$stock = $CI->db->select('qty')
								->where('barang_id', $detail->barang_id)
								->get($table_def_stock)->row()->qty;

                # KARTU STOCK
                $kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $detail->barang_id;
				$kartu->stock_detail_id = $objStockDetail->id;
				$kartu->keluar 	  = $sendJumlah;
				$kartu->harga = $objStockDetail->harga;
				$kartu->no_batch = $objStockDetail->no_batch;
				$kartu->expired_date = $objStockDetail->expired_date;
				$kartu->sisa 	  = $stock_sisa;
				$kartu->sisa_sum  = $stock;
				$kartu->keterangan = $obj->keterangan;
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);

		        $objStockDetail->qty_keluar = $sendJumlah; 
		        $aDetailStock[] = $objStockDetail;
            } else break;
		} while ($tmpQty > 0);					

		return $aDetailStock;
	}
}

if(! function_exists('procStockKeluarByExpiredDate')) {
	function procStockKeluarByExpiredDate($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_gudang_farmasi_stock';
		$table_def_stock_detail = 't_gudang_farmasi_stock_detail';
		$table_def_kartu_stock = 't_gudang_farmasi_kartu_stock';

		$objStock = $CI->db->where('barang_id', $detail->barang_id)
							->get($table_def_stock)->row();

		$stock = $objStock->qty;
		$tmpQty = $detail->qty;
		do {
			$objStockDetail = $CI->db->where('stock_id', $objStock->id)
									->where('expired_date', $detail->expired_date)
									->where('qty > ', 0)
									->order_by('id', 'ASC')
									->limit(1)
									->get($table_def_stock_detail)->row();

            if($objStockDetail) {
                $tmpStockSisa = $tmpQty - $objStockDetail->qty;
                $stock_sisa = $tmpQty - $objStockDetail->qty;
                if($stock_sisa > 0) {
                    $stock_sisa = 0;
                    $sendJumlah = $objStockDetail->qty;
                } else {
                    $stock_sisa = $objStockDetail->qty - $tmpQty;
                    $sendJumlah = $tmpQty;
                }
                $tmpQty = $tmpStockSisa;
                $CI->db->set('qty', $stock_sisa)
                		->where('id', $objStockDetail->id)
                		->update($table_def_stock_detail);

                $CI->db->set('qty', 'qty - '.$sendJumlah, FALSE)
						->set('update_at', date('Y-m-d H:i:s'))
						->set('update_by', $CI->auth->userid())
						->where('id', $objStock->id)
						->update($table_def_stock);

				$stock -= $sendJumlah; 

                # KARTU STOCK
                $kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $detail->barang_id;
				$kartu->stock_detail_id = $objStockDetail->id;
				$kartu->keluar 	  = $sendJumlah;
				$kartu->harga = $objStockDetail->harga;
				$kartu->no_batch = $objStockDetail->no_batch;
				$kartu->expired_date = $objStockDetail->expired_date;
				$kartu->sisa 	  = $stock_sisa;
				$kartu->sisa_sum  = $stock;
				$kartu->keterangan = $obj->keterangan;
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);
            } else break;
		} while ($tmpQty > 0);					
	}
}

if(! function_exists('procStockMasukByTransaksi')) {
	function procStockMasukByTransaksi($obj, $detail, $tipe = "") {
		$CI =& get_instance();
		if($tipe == "") $tipe = $CI->config->item('tipe_kartu_stock_penerimaan');
		$table_def_stock = 't_gudang_farmasi_stock';
		$table_def_stock_detail = 't_gudang_farmasi_stock_detail';
		$table_def_kartu_stock = 't_gudang_farmasi_kartu_stock';

		$objStock = $CI->db->select('id, qty')
							->where('barang_id', $detail->barang_id)
							->get($table_def_stock)->row();
		
		$stock = 0;
		$stockDetail = 0;
		if($objStock) {
			$stock = $objStock->qty;

			$CI->db->set('qty', 'qty + '.$detail->qty, FALSE)
					->set('update_at', date('Y-m-d H:i:s'))
					->set('update_by', $CI->auth->userid())
					->where('id', $objStock->id)
					->update($table_def_stock);
		} 

		# GET AND UPDATE STOCK DETAIL
		if(count($detail->data_barang) > 0) {
			foreach ($detail->data_barang as $oBarang) {
				$checkExists = $CI->db->where('id', $oBarang->id)
					->get($table_def_stock_detail);	

			 	if($checkExists->num_rows() > 0) {
					$CI->db->set('qty', 'qty + '.$oBarang->qty_keluar, FALSE)
							->set('update_at', date('Y-m-d H:i:s'))
							->set('update_by', $CI->auth->userid())
							->where('id', $oBarang->id)
							->update($table_def_stock_detail);

					$stockDetail = $checkExists->row()->qty + $oBarang->qty_keluar;
					$stock += $oBarang->qty_keluar;

					# KARTU STOCK
					$kartu = new stdClass();
					$kartu->tipe    = $tipe;
					$kartu->tanggal = $obj->tanggal;
					$kartu->kode = $obj->kode;
					$kartu->transaksi_id = $obj->id;
					$kartu->transaksi_detail_id = $detail->id;
					$kartu->barang_id = $detail->barang_id;
					$kartu->stock_detail_id = $oBarang->id;
					$kartu->masuk 	  = $oBarang->qty_keluar;
					$kartu->harga = $oBarang->harga;
					$kartu->no_batch = $oBarang->no_batch;
					$kartu->expired_date = $oBarang->expired_date;
					$kartu->sisa 	  = $stockDetail;
					$kartu->sisa_sum  = $stock;
					$kartu->keterangan = isset($obj->keterangan) ? $obj->keterangan : '';
					$kartu->created_by = $CI->auth->userid();
			        $kartu->created_at = date('Y-m-d H:i:s');
			        $CI->db->insert($table_def_kartu_stock, $kartu);
				}
			}
		} 
	}
}

if(! function_exists('procStockSO')) {
	function procStockSO($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_gudang_farmasi_stock';
		$table_def_stock_detail = 't_gudang_farmasi_stock_detail';
		$table_def_kartu_stock = 't_gudang_farmasi_kartu_stock';
		$table_def_barang = 'm_barang';

		$stock_id = $detail->stock_id;
		$objStock = $CI->db->select('id, barang_id, qty')
						->where('id', $stock_id)
						->get($table_def_stock)->row();


		// UPDATE STOCK BARANG
		$fisik = $detail->pengecekan2_stock_fisik;		
		$CI->db->set('qty', $fisik, FALSE)
					->set('status', 1)
					->set('update_at', date('Y-m-d H:i:s'))
					->set('update_by', $CI->auth->userid())
					->where('id', $stock_id)
					->update($table_def_stock);
		
		// UPDATE STOCK DETAIL BARANG
		$selisih = $detail->pengecekan2_selisih;		
		if($selisih <= 0) {
			$stock = 0;
			if(is_array($detail->detail_barang)) {
				foreach ($detail->detail_barang as $oDb) {
					$dataStockDetail = $CI->db->select('id, qty, qty_awal, harga, no_batch, expired_date')
												// ->where('stock_id', $stock_id, FALSE)
												// ->where('no_batch', $oDb->no_batch)
												// ->where('expired_date', $oDb->expired_date)
												// ->order_by('id', 'desc')
												->where('id', $oDb->id, FALSE)
												->get($table_def_stock_detail)->result();

					$totQtyMasuk = $oDb->qty;
					if(count($dataStockDetail) > 0) {
						foreach ($dataStockDetail as $i => $row) {
							$nextRow = isset($dataStockDetail[$i + 1]) ? $dataStockDetail[$i + 1] : null;
							$stock_detail_id = $row->id;

							$jumlahMasuk = $row->qty;
							$jumlahStock = $row->qty;
							if(!is_null($nextRow)) {
								if($row->qty < $row->qty_awal) {
									$jumlahMasuk = $totQtyMasuk;
									$tmpSelisih = $row->qty_awal - $row->qty;
									$jumlahStock = $row->qty + $jumlahMasuk;
									if($jumlahMasuk > $row->qty_awal) {
										$jumlahMasuk = $row->qty_awal;
										$jumlahStock = $row->qty_awal;
										$totQtyMasuk -= $tmpSelisih; 
									}
								}
							} else {
								$jumlahStock = $totQtyMasuk;
							}

							$stock += $jumlahStock;

							$CI->db->set('qty_awal', $jumlahStock, FALSE)
		                		->set('qty', $jumlahStock, FALSE)
		                		->where('id', $stock_detail_id)
		                		->update($table_def_stock_detail);

		                	# KARTU STOCK
				            $kartu = new stdClass();
							$kartu->tipe    = $tipe;
							$kartu->tanggal = $obj->tanggal;
							$kartu->kode = $obj->kode;
							$kartu->transaksi_id = $obj->id;
							$kartu->transaksi_detail_id = $detail->id;
							$kartu->barang_id = $objStock->barang_id;
							$kartu->stock_detail_id = $stock_detail_id;
							$kartu->masuk 	  = $jumlahStock;
							$kartu->harga 	  = $row->harga;
							$kartu->no_batch = $row->no_batch;
							$kartu->expired_date = $row->expired_date;
							$kartu->sisa 	  = $jumlahStock;
							$kartu->sisa_sum  = $stock;
							$kartu->keterangan = 'Stock Opname Barang dengan Detail';
							$kartu->created_by = $CI->auth->userid();
					        $kartu->created_at = date('Y-m-d H:i:s');
					        $CI->db->insert($table_def_kartu_stock, $kartu);

					        if($totQtyMasuk <= 0) break;
						}
					} else {
						$harga = 0;
						$objBarang = $CI->db->query("SELECT harga_pembelian, isi_satuan_penggunaan FROM {$table_def_barang} WHERE id = {$objStock->barang_id}")->row();
						if($objBarang) $harga = $objBarang->harga_pembelian / $objBarang->isi_satuan_penggunaan;

						$objNew = array(
							'stock_id' => $stock_id,
							'expired_date' => $oDb->expired_date,
							'no_batch' => $oDb->no_batch,
							'harga' => $harga,
							'qty' => $oDb->qty,
							'qty_awal' => $oDb->qty,
							'created_at' => date('Y-m-d H:i:s'),
							'created_by' => $CI->auth->userid(),
						);
						$CI->db->insert($table_def_stock_detail, $objNew);

						$stock_detail_id = $CI->db->insert_id();
						$jumlahStock = $oDb->qty;
						$stock += $oDb->qty;

						# KARTU STOCK
			            $kartu = new stdClass();
						$kartu->tipe    = $tipe;
						$kartu->tanggal = $obj->tanggal;
						$kartu->kode = $obj->kode;
						$kartu->transaksi_id = $obj->id;
						$kartu->transaksi_detail_id = $detail->id;
						$kartu->barang_id = $objStock->barang_id;
						$kartu->stock_detail_id = $stock_detail_id;
						$kartu->masuk 	  = $jumlahStock;
						$kartu->harga 	  = $harga;
						$kartu->expired_date = $oDb->expired_date;
						$kartu->no_batch = $oDb->no_batch;
						$kartu->sisa 	  = $jumlahStock;
						$kartu->sisa_sum  = $stock;
						$kartu->keterangan = 'Stock Opname Barang tanpa Detail';
						$kartu->created_by = $CI->auth->userid();
				        $kartu->created_at = date('Y-m-d H:i:s');
				        $CI->db->insert($table_def_kartu_stock, $kartu);
					}	
				}
			}
		} else {
			$stock = 0;
			if(is_array($detail->detail_barang)) {
				foreach ($detail->detail_barang as $oDb) {
					$dataStockDetail = $CI->db->select('id, qty, qty_awal, harga, no_batch, expired_date')
												// ->where('stock_id', $stock_id, FALSE)
												// ->where('no_batch', $oDb->no_batch)
												// ->where('expired_date', $oDb->expired_date)
												// ->where('qty >', 0)
												// ->order_by('id', 'asc')
												->where('id', $oDb->id, FALSE)
												->get($table_def_stock_detail)->result();

					$totQtyMasuk = $oDb->qty;
					if(count($dataStockDetail) > 0) {
						foreach ($dataStockDetail as $i => $row) {
							$nextRow = isset($dataStockDetail[$i + 1]) ? $dataStockDetail[$i + 1] : null;
							$stock_detail_id = $row->id;

							$jumlahMasuk = $row->qty;
							$jumlahStock = $row->qty;
							if(!is_null($nextRow)) {
								if($row->qty < $row->qty_awal) {
									$jumlahMasuk = $totQtyMasuk;
									$tmpSelisih = $row->qty_awal - $row->qty;
									$jumlahStock = $row->qty + $jumlahMasuk;
									if($jumlahMasuk > $row->qty_awal) {
										$jumlahMasuk = $row->qty_awal;
										$jumlahStock = $row->qty_awal;
										$totQtyMasuk -= $tmpSelisih; 
									}
								}
							} else {
								$jumlahStock = $totQtyMasuk;
							}

							$stock += $jumlahStock;

							$CI->db->set('qty_awal', $jumlahStock, FALSE)
		                		->set('qty', $jumlahStock, FALSE)
		                		->where('id', $stock_detail_id)
		                		->update($table_def_stock_detail);

		                	# KARTU STOCK
				            $kartu = new stdClass();
							$kartu->tipe    = $tipe;
							$kartu->tanggal = $obj->tanggal;
							$kartu->kode = $obj->kode;
							$kartu->transaksi_id = $obj->id;
							$kartu->transaksi_detail_id = $detail->id;
							$kartu->barang_id = $objStock->barang_id;
							$kartu->stock_detail_id = $stock_detail_id;
							$kartu->masuk 	  = $jumlahStock;
							$kartu->harga 	  = $row->harga;
							$kartu->no_batch = $row->no_batch;
							$kartu->expired_date = $row->expired_date;
							$kartu->sisa 	  = $jumlahStock;
							$kartu->sisa_sum  = $stock;
							$kartu->keterangan = 'Stock Opname Barang Selisih lebih besar';
							$kartu->created_by = $CI->auth->userid();
					        $kartu->created_at = date('Y-m-d H:i:s');
					        $CI->db->insert($table_def_kartu_stock, $kartu);

					        if($totQtyMasuk <= 0) break;
						}
					}
				}
			}
		}
	}
}