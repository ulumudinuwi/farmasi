<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promo_model extends CI_Model {

	protected $table_def = "m_promo";
	protected $table_def_barang = "m_barang";

	public function __construct() {
        parent::__construct();
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else  {
			$select = array(
				"{$this->table_def}.* ",
				"barang.id as barang_id ",
				"barang.nama as nama_barang ",
				"barang.kode as kode_barang ",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM ".$this->table_def;
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN ".$this->table_def_barang." AS barang ON (".$this->table_def.".barang_id = barang.id) ";
		return $join;
	}
	
	public function get_by($sWhere = "") {
		$sql = $this->_get_select()." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function save() {
		if (isset($_POST['uid']) && $_POST['uid'] == '') {
			$id = $this->create();
		}
		else {
			$id = $this->update();
		}
		return $id;
	}

	public function create() {
		$this->db->trans_start();
		
		$data = $this->_getRequest();
		$this->db->set('uid', 'UUID()', FALSE);
		$this->db->insert($this->table_def, $data);
		$id =  $this->db->insert_id();

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();
			return $id;
		} else {
			$this->db->trans_rollback();
			return false;
		}
	}

	public function update() {
		$this->db->trans_start();

		$data = $this->_getRequest(false);
		$this->db->where('uid', $_POST['uid']);
		$this->db->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();
			return $_POST['uid'];
		} else {
			$this->db->trans_rollback();
			return false;
		}
	}
	
	private function _getRequest($add = true) {
		if (!empty($_FILES['banner_id']['name'])) {
	      if ($this->input->post('old_banner_id')) {
	        if (file_exists('./uploads/promo/'.$_POST['old_banner_id'])) {
	          unlink('./uploads/promo/'.$_POST['old_banner_id']);
	        }
	      }
	      $banner_id = proc_do_upload('banner_id', './uploads/promo', ['allowed_types' => 'png|jpg|jpeg'], TRUE);
	      $dataBanner_id = $banner_id['file']['file_name'];
	    }else{
	      $dataBanner_id = $this->input->post('old_banner_id');
	    }
	    
		$data = array(
			'barang_id'				=> $_POST['barang_id'],
			'cek_promo'				=> isset($_POST['cek_promo']) ? 1 : 0,
			'qty_promo'				=> $_POST['qty_promo'],
			'harga_jual'			=> $_POST['harga_jual'],
			'type'					=> $_POST['type'],
			'expired_at'			=> get_date_accepted_db($_POST['tanggal']),
			'description'			=> $_POST['deskripsi'],
			'long_description'			=> $_POST['long_description'],
			'banner_id'				=> $dataBanner_id,
        );
		if ($add) {
			$data['created_by']	= $this->session->userdata('auth_user');
			$data['created_at']	= date('Y-m-d H:i:s');
        }
		else {
			$data['update_by']	= $this->session->userdata('auth_user');
			$data['update_at']	= date('Y-m-d H:i:s');
		}
		return $data;
	}
	
	public function delete_promo($uid) {
		$this->db->set('deleted', 1);
		$this->db->set('delete_at', $this->session->userdata('auth_user'));
		$this->db->set('delete_by', date('Y-m-d H:i:s'));
		$this->db->where('uid', $uid);
		$this->db->update($this->table_def);
	}

	public function update_status($uid, $status) {
		$this->db->trans_start();

		$data = array();
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');
		$data['status'] = $status;

		$this->db->where('uid', $uid);
		$this->db->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}
	
}

?>