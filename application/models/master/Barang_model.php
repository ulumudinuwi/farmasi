<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Barang_model extends CI_Model {

	protected $table_def = "m_barang";
	protected $table_def_foto = "m_barang_foto";
	protected $table_def_barang_kelompok = "m_barang_kelompok";
	/*protected $table_def_detail_distributor = "m_barang_distributor";*/
	protected $table_def_jenis = "m_jenisbarang";
	protected $table_def_golongan = "m_golonganbarang";
	protected $table_def_kategori = "m_kategoribarang";
	protected $table_def_pabrik = "m_pabrik";
	protected $table_def_satuan = "m_satuan";
	//protected $table_def_kelompok = "m_kelompokbarang";

	public function __construct() {
        parent::__construct();

        $this->load->library('image_lib');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"CONCAT(jn.kode, ' - ', jn.nama) jenis",
				"CONCAT(gl.kode, ' - ', gl.nama) golongan",
				"CONCAT(kt.kode, ' - ', kt.nama) kategori",
				"CONCAT(pb.kode, ' - ', pb.nama) pabrik",
				//"kp.nama kelompok",
				"CONCAT(spb.nama, ' (', spb.singkatan, ')') satuan_pembelian",
				"CONCAT(spg.nama, ' (', spg.singkatan, ')') satuan_penggunaan",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_satuan} spb ON {$this->table_def}.satuan_pembelian_id = spb.id ";
		$join .= "LEFT JOIN {$this->table_def_satuan} spg ON {$this->table_def}.satuan_penggunaan_id = spg.id ";
		$join .= "LEFT JOIN {$this->table_def_pabrik} pb ON {$this->table_def}.pabrik_id = pb.id ";
		//$join .= "LEFT JOIN {$this->table_def_kelompok} kp ON {$this->table_def}.kelompok_id = kp.id ";
		$join .= "LEFT JOIN {$this->table_def_kategori} kt ON {$this->table_def}.kategori_id = kt.id ";
		$join .= "LEFT JOIN {$this->table_def_golongan} gl ON {$this->table_def}.golongan_id = gl.id ";
		$join .= "LEFT JOIN {$this->table_def_jenis} jn ON {$this->table_def}.jenis_id = jn.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['kode']);
		unset($data['prefix_kode']);
		unset($data['fotos']);
		unset($data['kelompoks']);
		$this->db->set('uid', 'UUID()', FALSE);
		$this->db->set('kode', "func_get_kode_barang('{$obj->prefix_kode}%')", FALSE);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();

		$this->_saveFotos($obj);
		$this->_saveKelompoks($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->id;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['kode']);
		unset($data['prefix_kode']);
		unset($data['fotos']);
		unset($data['kelompoks']);
		if(isset($obj->kode)) {
			if(substr($obj->kode, 0, -3) != $obj->prefix_kode) $this->db->set('kode', "func_get_kode_barang('{$obj->prefix_kode}%')", FALSE);
		}
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

		if(isset($obj->fotos))
			$this->_saveFotos($obj);

		$this->_saveKelompoks($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->id;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function delete($uid) {
		$this->db->trans_start();

		$this->db->where('uid', $uid);
		$this->db->delete($this->table_def);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update_status($uid, $status) {
		$this->db->trans_start();

		$data = array();
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');
		$data['status'] = $status;

		$this->db->where('uid', $uid);
		$this->db->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	private function _saveFotos($obj) {
		if(count($obj->fotos) > 0) {
            foreach ($obj->fotos as $key => $value) {
                default_directory('barang');
                $ori_dir = './uploads/barang/original';
                $thumb_dir = './uploads/barang/thumbnail';
                $file_name = $obj->uid.'-img-'.($key+1).'-'.time().'.png';

                // remove the "data:image/png;base64,"
                $tmp_foto = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $value));
                if(!file_put_contents($ori_dir.'/'.$file_name, $tmp_foto)) {
                	$this->db->trans_rollback();
		  			return false;
                }

                // make thumbnail
                $config['image_library'] 	= 'gd2';
				$config['source_image'] 	= $ori_dir.'/'.$file_name;
				$config['new_image'] 		= $thumb_dir.'/'.$file_name;
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['thumb_marker'] 	= '';
				$config['width']         	= 150;
				$config['height']       	= 120;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();

                $dataFoto = array(
                    'barang_id' => $obj->id,
                    'nama' => $file_name,
                    'upload_at' => date('Y-m-d H:i:s'),
                    'upload_by' => $this->auth->userid()
                );
                $this->db->set('uid', 'UUID()', FALSE)
                	->insert($this->table_def_foto, $dataFoto);
            }
        }
	}

	private function _saveKelompoks($obj) {
		if (! property_exists($obj, 'kelompoks')) return;
		foreach ($obj->kelompoks as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			switch ($detail->data_mode) {
                case Data_mode_model::DATA_MODE_ADD:
                	$data['barang_id'] = $obj->id;
                	$data['kelompok_id'] = $detail->kelompok_id;
					$this->db->insert($this->table_def_barang_kelompok, $data);
                    break;
                case Data_mode_model::DATA_MODE_EDIT:
                	//
                    break;
                case Data_mode_model::DATA_MODE_DELETE:
                    $this->db->where(['kelompok_id' => $detail->kelompok_id, 'barang_id' => $obj->id])
                    		->delete($this->table_def_barang_kelompok); 
                    break;
            }
        }		
	}
}

?>