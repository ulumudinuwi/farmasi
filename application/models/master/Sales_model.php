<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Model_Eloquent;
use Illuminate\Database\Capsule\Manager as DB;
class Sales_model extends Model_Eloquent
{
    protected $table = 'm_sales';
    public $timestamps = true;

    public static function findByUID($uid){
        return self::where('uid', $uid)->first();
    }

    public static function list_dt_transaksi($field = [], $sWhere = []){
        if(empty($field)) return false;
        /*Query*/
        $query = DB::table('t_pos_detail');
        $query->select([
            't_pos_detail.no_fbp as no_fbp',
            't_pos.tanggal_transaksi as tgl_transaksi',
            'm_dokter_member.nama as nama_dokter',
            'm_barang.nama as nama_barang',
            't_pos_detail.qty as qty',
            't_pos_detail.harga as harga',
            't_pos_detail.grand_total as grand_total',
            't_pos_detail.diskon as diskon',
            't_pos_detail.total as total',
        ]);

        $query->join('t_pos', 't_pos_detail.pos_id', '=', 't_pos.id');
        $query->join('m_dokter_member', 't_pos_detail.member_id', '=', 'm_dokter_member.id');
        $query->join('m_barang', 't_pos_detail.barang_id', '=', 'm_barang.id');
        //Filter Filter Disini
        $filter_range = isset($_REQUEST['filter_range']) ? $_REQUEST['filter_range']: null;
        
        //Tanggal
        if(isset($filter_range)){
            $query->whereDate('t_pos_detail.tanggal_transaksi','>=',$filter_range[0]);
            $query->whereDate('t_pos_detail.tanggal_transaksi','<=',$filter_range[1]);
        }

        if(!empty($sWhere)){
            foreach($sWhere as $_field => $_search){
                if(is_manager() || is_admin()) {
                    if($_field == 't_pos_detail.sales_id') {
                        $query->whereRaw("{$_field} IN (SELECT id FROM m_sales WHERE manager_sales_id = {$_search} OR id = {$_search})");
                    } else $query->where($_field, $_search); 
                } else $query->where($_field, $_search);
            }
        }
        // dd($query->get());
        

        // dd($field);
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw= $_REQUEST ? $_REQUEST['draw'] : 0;

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length= $_REQUEST ? $_REQUEST['length'] : 10;

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start= $_REQUEST ? $_REQUEST['start'] : 0;

		/*Keyword yang diketikan oleh user pada field pencarian*/
        $search = $_REQUEST ? $_REQUEST['search']["value"] : '';
        
        /*Order Column*/
		$order = $_REQUEST && isset($_REQUEST['order']) ? $_REQUEST['order'][0] : null;
        

		/*Menghitung total data didalam database*/
		$total= $query;
		$total= $total->count();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output = [];

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw'] = $draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal'] = $output['recordsFiltered'] = $total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data'] = [];

		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
        */
        // dd(count($_REQUEST['columns']));
		if($search != ""){
            $sNewColumn = "";
            $new_column = [];
            for($i = 0; $i < count($_REQUEST['columns']); $i++){
                if($_REQUEST){
                    $is_searchable = $_REQUEST['columns'][$i]['searchable'];
                    if($is_searchable == "true"){
                        $new_column[] = $field[$i].' LIKE "%'.$search.'%"';
                    }
                }
            }
            
            if(count($new_column) > 0) $sNewColumn = implode(' OR ', $new_column);

            if($sNewColumn) $query->whereRaw('('.$sNewColumn.')');

            $countSearch = $query;
            $countSearch = $countSearch->count(); 
            $output['recordsTotal'] = $output['recordsFiltered'] = $countSearch;
        }
        
        // dd($_REQUEST['columns'], $field);
        // dd($query->toSql());

		/*Limit dan Offset*/
        $query->skip($start);
        $query->limit($length);

        // dd($field);
        /*Ordering*/
		for($i = 0; $i < count($field); $i++){
            if($_REQUEST){
                $is_orderable = $_REQUEST['columns'][$i]['orderable'];
                if($is_orderable == 'true'){
                    if($order['column'] == $i){
                        $query->orderBy($field[$i],$order['dir']);
                    }
                }
            }
        }
		$results = $query->get();

        /**
         * Hasil dari Generate datatable
        */
        $output['data'] = $results;

		return $output;
    }

    public static function redraw_sales(){
        $request = request_handler();
        $id = sales_id();
        if(isset($request->uid)){
            $id = self::where('uid', $request->uid)->first()->id;
        }
        // dd($id);
        $data = DB::table('t_target_sales')->where('sales_id',$id)->get();
        foreach($data as $item){
            $jml_pendapatan = self::get_pendapatan($item['bulan'], $item['tahun'], $id);
            
            $pencapaian_persen = ($item['target'] > 0 ? ($jml_pendapatan / $item['target']) * 100 : 0);
            
            $insentif_pendapatan = self::get_insentif_pendapatan($pencapaian_persen);
   
            // echo '('.$insentif_pendapatan.')<br>';
            $jml_member = self::get_jumlah_member($item['bulan'], $item['tahun'], $id);
            $insentif_member = self::get_insentif_member($jml_member);

            // $total_insentif_persen = $insentif_pendapatan + $insentif_member;
            // $total_insentif = $jml_pendapatan*($total_insentif_persen/100);

            $insentif_dari_pendapatan = $jml_pendapatan * ($insentif_pendapatan/100);
            $insentif_dari_member_baru = $jml_pendapatan * ($insentif_member/100);

            $total = $insentif_dari_member_baru + $insentif_dari_member_baru + $item['insentif_khusus'];
            // var_dump($jml_member);
            $update = DB::table('t_target_sales')->where('id', $item['id'])->update([
                'pencapaian' => $jml_pendapatan,
                'persentase' => $pencapaian_persen,
                'insentif_pendapatan' => $insentif_pendapatan,
                'insentif_dari_pendapatan' => $insentif_dari_pendapatan,
                'jumlah_member' => $jml_member,
                'insentif_dari_member_baru' => $insentif_dari_member_baru,
                'insentif_member' => $insentif_member,
                'total_insentif_didapat' => $total,
                'updated_at' => timestamp(),
                'updated_by' => auth_id(),
            ]);
        }
    }

    public static function get_pendapatan($bulan, $tahun, $sales_id){
        $bulan = (Int) $bulan;
        $tahun = (Int) $tahun;

        $query = DB::table('t_pos')
            ->where('is_approved', 1)
            //->where('sales_id', $sales_id)
            ->whereMonth('tanggal_transaksi','=', $bulan)
            ->whereYear('tanggal_transaksi','=', $tahun);
            if (is_manager() || is_admin()) {
                $query->whereRaw("sales_id IN (SELECT id FROM m_sales WHERE manager_sales_id ={$sales_id} OR id = {$sales_id})");
            } else $query->where('sales_id', $sales_id);

        return $query->sum(DB::raw('(total_harga_before_diskon - sisa_pembayaran)'));
        //return $query->sum(DB::raw('(grand_total - sisa_pembayaran)'));
    }

    public static function get_jumlah_member($bulan, $tahun, $sales_id){
        $bulan = (Int) $bulan;
        $tahun = (Int) $tahun;

        $query = DB::table('m_dokter_member')
              ->whereMonth('created_at','=', $bulan)
              ->whereYear('created_at','=', $tahun);
        // var_dump($query);
        // dd(is_manager());
        if(is_manager()){
            $get_id_with_sales_bawahan = get_bawahan_sales_id();
            $get_id_with_sales_bawahan[] = $sales_id;
            // var_dump($get_id_with_sales_bawahan);
            if(count($get_id_with_sales_bawahan) > 0) {
                $query->whereIn('sales_id',$get_id_with_sales_bawahan);
            }
        }else if(is_sales()){
            $query->where('sales_id', $sales_id);
        }
        // dd($query->toSql());
        return $query->count();
    }

    public static function get_insentif_pendapatan($pencapaian){
        // echo $pencapaian.'<br>';
        $get_rules = DB::table('m_insentif_sales')->get();
        $insentif = 0;
        foreach($get_rules as $item){
            $con = 'return ' . str_replace('%d',$pencapaian,$item['pencapaian']).';';
            if(eval($con)){
                $insentif = $item['insentif'];
            }
        }
        return $insentif;
    }

    public static function get_insentif_member($jml_member){
        $get_rules = DB::table('m_insentif_jumlah_member_sales')->get();
        $insentif = 0;
        foreach($get_rules as $item){
            $con = 'return ' . str_replace('%d',$jml_member,$item['pencapaian_member']).';';
            if(eval($con)){
                $insentif = $item['insentif'];
            }
        }
        return $insentif;
    }
}
