<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Options_model extends CI_Model {

    protected $table_def = "sys_options";

    public function __construct() {
        parent::__construct();
    }

    private function _get_select() {
        $select = array(
            $this->table_def.".option_id",
            $this->table_def.".option_name",
            $this->table_def.".option_value",
            $this->table_def.".option_table",
            $this->table_def.".option_type",
            $this->table_def.".autoload",
            $this->table_def.".user_visibility",
        );

        return "SELECT ".implode(",", $select)." ";
    }

    private function _get_from() {
        $from = "FROM ".$this->table_def." ";
        return $from;
    }

    private function _get_join() {
        $join = "";

        return $join;
    }

    public function get_by($sWhere = "") {
        $sql = $this->_get_select()." ";
        $sql .= $this->_get_from()." ";
        $sql .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql .= " ".$sWhere;
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
        $data = array();
        $sql_count = "SELECT COUNT({$this->table_def}.option_id) AS numrows ";
        $sql_count .= $this->_get_from()." ";
        $sql_count .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql_count .= " ".$sWhere." ";
        }
        $query = $this->db->query($sql_count);
        if ($query->num_rows() == 0) {
            $data['total_rows'] = 0;
        } else {
            $row = $query->row();
            $data['total_rows'] = (int) $row->numrows;
        }

        $select = $this->_get_select();
        $from = $this->_get_from();
        $join = $this->_get_join();
        $sql = $select." ".$from." ".$join." ";
        if (!empty($sWhere)) {
            $sql .= $sWhere." ";
        }
        if (!empty($sOrder)) {
            $sql .= $sOrder." ";
        }
        if ($iLimit > 0) {
            $sql .= "LIMIT ".$iOffset.", ".$iLimit;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result();
        } else {
            $data['data'] = array();
        }
        return $data;
    }

    public function create($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['option_id']);

        $this->db->insert($this->table_def, $data);
        // get insert_id()
        $option_id =  $this->db->insert_id();

        $result = $this->db->where('option_id', $option_id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['option_id']);

        $this->db->where('option_id', $obj->option_id);
        $this->db->update($this->table_def, $data);

        $result = $this->db->where('option_id', $obj->option_id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function delete($option_id) {
        $this->db->trans_start();

        $this->db->where('option_id', $option_id);
        $this->db->delete($this->table_def);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $option_id;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }


    // LOOKUP TABLE
    private function _get_select_lookup($fields) {
        $select = array();
        foreach ($fields as $as => $field) {
            $select[] = $field.' as '.$as;
        }

        return "SELECT ".implode(",", $select)." ";
    }

    private function _get_from_lookup($table) {
        $from = "FROM ".$table." ";
        return $from;
    }

    private function _get_join_lookup() {
        $join = "";

        return $join;
    }

    public function get_all_lookup($table, $fields, $iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
        $data = array();
        $sql_count = "SELECT COUNT({$table}.id) AS numrows ";
        $sql_count .= $this->_get_from_lookup($table)." ";
        $sql_count .= $this->_get_join_lookup();
        if (!empty($sWhere)) {
            $sql_count .= " ".$sWhere." ";
        }
        $query = $this->db->query($sql_count);
        if ($query->num_rows() == 0) {
            $data['total_rows'] = 0;
        } else {
            $row = $query->row();
            $data['total_rows'] = (int) $row->numrows;
        }

        $select = $this->_get_select_lookup($fields);
        $from = $this->_get_from_lookup($table);
        $join = $this->_get_join_lookup();
        $sql = $select." ".$from." ".$join." ";
        if (!empty($sWhere)) {
            $sql .= $sWhere." ";
        }
        if (!empty($sOrder)) {
            $sql .= $sOrder." ";
        }
        if ($iLimit > 0) {
            $sql .= "LIMIT ".$iOffset.", ".$iLimit;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result();
        } else {
            $data['data'] = array();
        }
        return $data;
    }
}