<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pemeriksaan_obat_model extends CI_Model {

    protected $table_def = 'm_rad_pemeriksaan_obat';
    protected $table_def_obat = 'm_barang';

    /**
     * Constants
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function __construct() {
        parent::__construct();
    }

    private function _get_select() {
        $select = array(
            $this->table_def.".*",
            $this->table_def_obat.".kode as obat_kode",
            $this->table_def_obat.".nama as obat",
        );

        return "SELECT ".implode(",", $select)." ";
    }

    private function _get_from() {
        $from = "FROM ".$this->table_def." ";
        return $from;
    }

    private function _get_join() {
        $aJoin = array(
            "LEFT JOIN {$this->table_def_obat} ON {$this->table_def}.obat_id = {$this->table_def_obat}.id",
        );

        return implode(" ", $aJoin);
    }

    public function get_by($sWhere = "") {
        $sql = $this->_get_select()." ";
        $sql .= $this->_get_from()." ";
        $sql .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql .= " ".$sWhere;
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
        $data = array();
        $sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
        $sql_count .= $this->_get_from()." ";
        $sql_count .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql_count .= " ".$sWhere." ";
        }
        $query = $this->db->query($sql_count);
        if ($query->num_rows() == 0) {
            $data['total_rows'] = 0;
        } else {
            $row = $query->row();
            $data['total_rows'] = (int) $row->numrows;
        }

        $select = $this->_get_select();
        $from = $this->_get_from();
        $join = $this->_get_join();
        $sql = $select." ".$from." ".$join." ";
        if (!empty($sWhere)) {
            $sql .= $sWhere." ";
        }
        if (!empty($sOrder)) {
            $sql .= $sOrder." ";
        }
        if ($iLimit > 0) {
            $sql .= "LIMIT ".$iOffset.", ".$iLimit;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result();
        } else {
            $data['data'] = array();
        }
        return $data;
    }
}