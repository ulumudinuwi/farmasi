<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pemeriksaan_model extends CI_Model {

    protected $table_def = 'm_rad_pemeriksaan';
    protected $table_def_obat = 'm_rad_pemeriksaan_obat';

    /**
     * Constants
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const JENIS_ROOT = 'root';
    const JENIS_KELOMPOK = 'kelompok';
    const JENIS_RINCIAN = 'rincian';

    public function __construct() {
        parent::__construct();

        $this->load->library('Nested_set');

        $this->load->model('Data_mode_model');
    }

    private function _get_select() {
        $select = array(
            $this->table_def.".*",
        );

        return "SELECT ".implode(",", $select)." ";
    }

    private function _get_from() {
        $from = "FROM ".$this->table_def." ";
        return $from;
    }

    private function _get_join() {
        $aJoin = array(
            //
        );

        return implode(" ", $aJoin);
    }

    public function get_by($sWhere = "") {
        $sql = $this->_get_select()." ";
        $sql .= $this->_get_from()." ";
        $sql .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql .= " ".$sWhere;
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
        $data = array();
        $sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
        $sql_count .= $this->_get_from()." ";
        $sql_count .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql_count .= " ".$sWhere." ";
        }
        $query = $this->db->query($sql_count);
        if ($query->num_rows() == 0) {
            $data['total_rows'] = 0;
        } else {
            $row = $query->row();
            $data['total_rows'] = (int) $row->numrows;
        }

        $select = $this->_get_select();
        $from = $this->_get_from();
        $join = $this->_get_join();
        $sql = $select." ".$from." ".$join." ";
        if (!empty($sWhere)) {
            $sql .= $sWhere." ";
        }
        if (!empty($sOrder)) {
            $sql .= $sOrder." ";
        }
        if ($iLimit > 0) {
            $sql .= "LIMIT ".$iOffset.", ".$iLimit;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result();
        } else {
            $data['data'] = array();
        }
        return $data;
    }

    public function create($obj) {
        $this->nested_set->setControlParams($this->table_def, 'id');

        $this->db->trans_start();

        // CREATE ROOT
        if (!$root_id = $this->nested_set->getRootId())
            $root_id = $this->_create_root();

        if ($obj->parent_id == 0) {
            $obj->parent_id = $root_id;
        }

        if ($obj->old_parent_id != $obj->parent_id || $obj->id == 0) {
            $this->nested_set->setLocation($obj->parent_id, 'last-child');
        }

        $data = get_object_vars($obj);
        // unset($data['id']);
        unset($data['uid']);
        unset($data['old_parent_id']);
        unset($data['items']);
        unset($data['obats']);

        $this->db->set('uid', 'UUID()', FALSE);
        $data['status'] = self::STATUS_ACTIVE;
        $data['created_by'] = $this->session->userdata('auth_user');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['update_by'] = $this->session->userdata('auth_user');
        $data['update_at'] = date('Y-m-d H:i:s');

        // $this->db->insert($this->table_def, $data);
        // get insert_id()
        // $id =  $this->db->insert_id();

        $id = $this->nested_set->store($data);

        $result = $this->db->where('id', $id)->get($this->table_def)->row();

        $this->saveObats($result, $obj->obats);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($obj) {
        $this->nested_set->setControlParams($this->table_def, 'id');

        $this->db->trans_start();

        // CREATE ROOT
        if (!$root_id = $this->nested_set->getRootId())
            $root_id = $this->_create_root();

        if ($obj->parent_id == 0) {
            $obj->parent_id = $root_id;
        }

        if ($obj->old_parent_id != $obj->parent_id || $obj->id == 0) {
            $this->nested_set->setLocation($obj->parent_id, 'last-child');
        }

        $data = get_object_vars($obj);
        // unset($data['id']);
        unset($data['uid']);
        unset($data['old_parent_id']);
        unset($data['items']);
        unset($data['obats']);

        $data['update_by'] = $this->session->userdata('auth_user');
        $data['update_at'] = date('Y-m-d H:i:s');

        // $this->db->where('id', $obj->id);
        // $this->db->update($this->table_def, $data);
        $id = $this->nested_set->store($data);

        $result = $this->db->where('id', $obj->id)->get($this->table_def)->row();

        $this->saveObats($result, $obj->obats);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update_status($uid, $status) {
        $this->db->trans_start();

        $data = array();
        $data['update_by'] = $this->session->userdata('auth_user');
        $data['update_at'] = date('Y-m-d H:i:s');
        $data['status'] = $status;

        $this->db->where('uid', $uid);
        $this->db->update($this->table_def, $data);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $uid;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function delete($uid) {
        $this->db->trans_start();

        $data = array();
        $data['update_by'] = $this->session->userdata('auth_user');
        $data['update_at'] = date('Y-m-d H:i:s');
        $data['status'] = self::STATUS_INACTIVE;
        $data['deleted_flag'] = 1;

        $this->db->where('uid', $uid);
        $this->db->update($this->table_def, $data);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $uid;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function restore($uid) {
        $this->db->trans_start();

        $data = array();
        $data['update_by'] = $this->session->userdata('auth_user');
        $data['update_at'] = date('Y-m-d H:i:s');
        $data['status'] = self::STATUS_ACTIVE;
        $data['deleted_flag'] = 0;

        $this->db->where('uid', $uid);
        $this->db->update($this->table_def, $data);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $uid;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    /**
     * SAVE OBATS
     */
    private function saveObats($obj, $obats) {
        foreach ($obats as $obat) {
            $data = get_object_vars($obat);
            unset($data['id']);
            unset($data['uid']);
            unset($data['data_mode']);

            switch ($obat->data_mode) {
                case $this->Data_mode_model::DATA_MODE_ADD:
                    $data['pemeriksaan_id'] = $obj->id;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['created_by'] = $this->session->userdata('auth_user');
                    $data['update_at'] = date('Y-m-d H:i:s');
                    $data['update_by'] = $this->session->userdata('auth_user');

                    $this->db->insert($this->table_def_obat, $data);
                    break;
                case $this->Data_mode_model::DATA_MODE_EDIT:
                    $data['update_at'] = date('Y-m-d H:i:s');
                    $data['update_by'] = $this->session->userdata('auth_user');

                    $this->db->where('id', $obat->id);
                    $this->db->update($this->table_def_obat, $data);
                    break;
                case $this->Data_mode_model::DATA_MODE_DELETE:
                    $this->db->where('id', $obat->id);
                    $this->db->delete($this->table_def_obat);
                    break;
            }
        }
    }

    private function _create_root() {
        $data = array(
            'tarif_pelayanan_id' => null,
            'kode' => '',
            'nama' => 'ROOT',
            'deskripsi' => '',
            'status' => 0,
            'jenis' => self::JENIS_ROOT,
            'parent_id' => 0,
            'lvl' => 0,
            'lft' => 1,
            'rgt' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => -1,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => -1,
            'deleted_flag' => 0,
        );
        $this->db->set('uid', 'UUID()', FALSE);
        $this->db->insert($this->table_def, $data);
        return $this->db->insert_id();
    }
}