<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Penerimaan_mutasi_model extends CI_Model {
	protected $table_def = "t_logistik_penerimaan_mutasi";
  	protected $table_def_detail = "t_logistik_penerimaan_mutasi_detail";
	protected $table_def_mutasi = "t_request_mutasi_unit";
	protected $table_def_unitkerja = "m_unitkerja";
	protected $table_def_user = "auth_users";

	public function __construct() {
        parent::__construct();
        $this->load->helper('logistik');
        $this->load->helper('gudang');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"{$this->table_def_mutasi}.kode kode_mutasi",
				"{$this->table_def_mutasi}.keterangan",
				"uk.nama unit_kerja",
				"CONCAT(diproses_by.first_name, ' ',diproses_by.last_name) diproses_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_mutasi} ON {$this->table_def}.mutasi_id = {$this->table_def_mutasi}.id ";
		$join .= "LEFT JOIN {$this->table_def_unitkerja} uk ON {$this->table_def}.unitkerja_id = uk.id ";
		$join .= "LEFT JOIN {$this->table_def_user} diproses_by ON {$this->table_def}.created_by = diproses_by.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['kode']);
		unset($data['details']);
		$this->db->set('uid', 'UUID()', FALSE);
		$data['kode'] = generateKode("TMU", $this->table_def);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();
		$obj->kode = $this->db->query("SELECT kode FROM {$this->table_def} WHERE id = {$obj->id}")->row()->kode;

		$this->_saveDetail($obj);

		$this->db->set('status', $this->config->item('status_mutasi_processed'))
				->where('id', $obj->mutasi_id)
				->update($this->table_def_mutasi);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			$data['update_by'] = $this->session->userdata('auth_user');
        	$data['update_at'] = date('Y-m-d H:i:s');
        	$data['penerimaan_id'] = $obj->id;
			$data['created_by'] = $this->session->userdata('auth_user');
    		$data['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert($this->table_def_detail, $data);

			// KARTU STOCK
			$detail->qty = ($detail->qty * $detail->isi_satuan);
			procStockMasuk($obj, $detail, $this->config->item('tipe_kartu_stock_penerimaan_mutasi'));
        }		
	}
}

?>