<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pengeluaran_detail_model extends CI_Model {

  	protected $table_def = "t_logistik_pengeluaran_detail";
  	protected $table_def_pengeluaran = "t_logistik_pengeluaran";
	protected $table_def_barang = "m_barang";
	protected $table_def_satuan = "m_satuan";

	public function __construct() {
        parent::__construct();
    }
	
	private function _get_select() {
		$select = array(
			"{$this->table_def}.*",
			"br.kode kode_barang",
			"br.nama barang",
			"CONCAT(st.nama, ' (', st.singkatan, ')') satuan",
		);
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_pengeluaran} a ON {$this->table_def}.pengeluaran_id = a.id ";
		$join .= "LEFT JOIN {$this->table_def_barang} br ON {$this->table_def}.barang_id = br.id ";
		$join .= "LEFT JOIN {$this->table_def_satuan} st ON {$this->table_def}.satuan_id = st.id ";
		return $join;
	}
	
	public function get_by($sWhere = "") {
		$sql = $this->_get_select()." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select();
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}
}

?>