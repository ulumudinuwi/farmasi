<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Po_model extends CI_Model {
	protected $table_def = "t_logistik_po";
  	protected $table_def_detail = "t_logistik_po_detail";
  	protected $table_def_stock = "t_logistik_stock";
	protected $table_def_pabrik = "m_pabrik";
	protected $table_def_vendor = "m_vendor";
	protected $table_def_user = "auth_users";

	public function __construct() {
        parent::__construct();
        $this->load->helper('gudang');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"CONCAT(pb.kode, ' - ', pb.nama) pabrik",
				"CONCAT(vd.kode, ' - ', vd.nama) vendor",
				"CONCAT(pengajuan_by.first_name, ' ',pengajuan_by.last_name) pengajuan_by",
				"CONCAT(persetujuan_by.first_name, ' ',persetujuan_by.last_name) persetujuan_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_pabrik} pb ON {$this->table_def}.pabrik_id = pb.id ";
		$join .= "LEFT JOIN {$this->table_def_vendor} vd ON {$this->table_def}.vendor_id = vd.id ";
		$join .= "LEFT JOIN {$this->table_def_user} pengajuan_by ON {$this->table_def}.created_by = pengajuan_by.id ";
		$join .= "LEFT JOIN {$this->table_def_user} diubah_by ON {$this->table_def}.update_by = diubah_by.id ";
		$join .= "LEFT JOIN {$this->table_def_user} persetujuan_by ON {$this->table_def}.persetujuan_by = persetujuan_by.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['kode']);
		unset($data['details']);
		$this->db->set('uid', 'UUID()', FALSE);
		//$this->db->set('kode', 'func_get_logistik_kode_po("PO")', FALSE);
		$data['kode'] = generateKode("GLO", $this->table_def);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['details']);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function delete($uid) {
		$this->db->trans_start();

		$this->db->where('uid', $uid);
		$this->db->delete($this->table_def);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function act_exe($uid, $alasan, $mode = "batal") {
		$this->db->trans_start();

		$data = array();
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');		
		switch ($mode) {
			case 'acc':
			case 'tolak':
				$data['persetujuan_by'] = $this->session->userdata('auth_user');
				$data['persetujuan_at'] = date('Y-m-d H:i:s');
				$data['status'] = $this->config->item('status_po_purchasing');
				if($mode == 'tolak') {
					$data['status'] = $this->config->item('status_po_rejected');
					$data['keterangan'] = $alasan;
				}
				break;
			case 'sanggup':
			default:
				if($mode === 'sanggup') {
					$data['status'] = $this->config->item('status_po_waiting_for_delivery');
				} else {
					$data['status'] = 0; // BATAL
					$data['keterangan'] = $alasan;
				}
				break;
		}

		$this->db->where('uid', $uid)
				->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function pengalihan($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['po_id']);
		unset($data['keterangan']);
		unset($data['details']);
		$this->db->set('uid', 'UUID()', FALSE);
		$data['kode'] = generateKode("GLO", $this->table_def);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();

		$this->_saveDetail($obj);

		$kode = $this->db->query("SELECT kode FROM {$this->table_def} WHERE id = {$obj->id}")->row()->kode;
		// Update PO yang dialihkan
		$keterangan = $obj->keterangan ? $obj->keterangan."<br/>" : "";
		$keterangan .= "Dialihkan ke PO dengan nomor {$kode}.";

		$dataAlih = array(
			'status' => $this->config->item('status_po_diverted'),
			'keterangan' => $keterangan,
			'update_by' => $this->session->userdata('auth_user'),
			'update_at' => date('Y-m-d H:i:s'),
		);
		$this->db->where('id', $obj->po_id)
				->update($this->table_def, $dataAlih);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			$data['update_by'] = $this->session->userdata('auth_user');
        	$data['update_at'] = date('Y-m-d H:i:s');
            switch ($detail->data_mode) {
                case Data_mode_model::DATA_MODE_ADD:
                	$data['po_id'] = $obj->id;
					$data['created_by'] = $this->session->userdata('auth_user');
	        		$data['created_at'] = date('Y-m-d H:i:s');
					$this->db->insert($this->table_def_detail, $data);

					$this->db->set('dipesan', 1)
							->where('barang_id', $detail->barang_id)
							->update($this->table_def_stock);
                    break;
                case Data_mode_model::DATA_MODE_EDIT:
                    $this->db->where('id', $detail->id)
                    		->update($this->table_def_detail, $data);
                    break;
                case Data_mode_model::DATA_MODE_DELETE:
                    $this->db->where('id', $detail->id)
                    		->delete($this->table_def_detail); 

                    $this->db->set('dipesan', 0)
							->where('barang_id', $detail->barang_id)
							->update($this->table_def_stock);
                    break;
            }
        }		
	}
}

?>