<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pengeluaran_model extends CI_Model {
	protected $table_def = "t_logistik_pengeluaran";
  	protected $table_def_detail = "t_logistik_pengeluaran_detail";
  	protected $table_def_permintaan = "t_request_permintaan_unit";
  	protected $table_def_permintaan_detail = "t_request_permintaan_unit_detail";
	protected $table_def_unitkerja = "m_unitkerja";
	protected $table_def_user = "auth_users";

	public function __construct() {
        parent::__construct();
        $this->load->helper('logistik');
        $this->load->helper('gudang');
        $this->load->model('request/unit_kerja/Permintaan_detail_model', 'permintaan_detail');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"{$this->table_def_permintaan}.kode kode_permintaan",
				"{$this->table_def_permintaan}.keterangan keterangan_permintaan",
				"uk.nama unit_kerja",
				"CONCAT(diproses_by.first_name, ' ',diproses_by.last_name) diproses_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_permintaan} ON {$this->table_def}.permintaan_id = {$this->table_def_permintaan}.id ";
		$join .= "LEFT JOIN {$this->table_def_unitkerja} uk ON {$this->table_def}.unitkerja_id = uk.id ";
		$join .= "LEFT JOIN {$this->table_def_user} diproses_by ON {$this->table_def}.created_by = diproses_by.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$obj->kode = generateKode("GLD", $this->table_def);
		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['details']);
		$this->db->set('uid', 'UUID()', FALSE);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();
		
		$this->_saveDetail($obj);

		$this->_updateStatusPermintaan($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['details']);
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function delete($uid) {
		$this->db->trans_start();

		$this->db->where('uid', $uid);
		$this->db->delete($this->table_def);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			$data['update_by'] = $this->session->userdata('auth_user');
        	$data['update_at'] = date('Y-m-d H:i:s');
            switch ($detail->data_mode) {
                case Data_mode_model::DATA_MODE_ADD:
                	$data['pengeluaran_id'] = $obj->id;
					$data['created_by'] = $this->session->userdata('auth_user');
	        		$data['created_at'] = date('Y-m-d H:i:s');
					$this->db->insert($this->table_def_detail, $data);
					$detail->id =  $this->db->insert_id();

					$this->db->set('dikirim', 'dikirim + '.$detail->qty, FALSE)
						->where('id', $detail->permintaan_detail_id)
						->update($this->table_def_permintaan_detail);

					// KARTU STOCK
					$detail->qty = ($detail->qty * $detail->isi_satuan);
					procStockKeluar($obj, $detail, $this->config->item('tipe_kartu_stock_pengeluaran'));
                    break;
                case Data_mode_model::DATA_MODE_EDIT:
                    //
                    break;
                case Data_mode_model::DATA_MODE_DELETE:
                    //
                    break;
            }
        }		
	}

	private function _updateStatusPermintaan($obj) {
		$sWhere = "";
	    $aWheres = array();
	    $aWheres[] = "{$this->table_def_permintaan_detail}.permintaan_id = {$obj->permintaan_id}";
	    $aWheres[] = "{$this->table_def_permintaan_detail}.qty > {$this->table_def_permintaan_detail}.dikirim";
	    if (count($aWheres) > 0) {
	      $sWhere = implode(' AND ', $aWheres);
	    }
	    if (!empty($sWhere)) {
	      $sWhere = "WHERE ".$sWhere;
	    }
		$isExist = $this->permintaan_detail->get_all(1, 0, $sWhere)['total_rows'];

		$dataPermintaan = array();
		$status_permintaan = $this->config->item('status_request_closed');
	    $status_pengeluaran = $this->config->item('status_closed');
		if($isExist > 0) {
			$status_permintaan = $this->config->item('status_request_partial');
	    	$status_pengeluaran = $this->config->item('status_partial');
		}

		$this->db->set('status', $status_pengeluaran)
				 ->where('id', $obj->id)
				 ->update($this->table_def);

		$dataPermintaan['status'] = $status_permintaan;
		$dataPermintaan['update_at'] = date('Y-m-d H:i:s');
		$dataPermintaan['update_by'] = $this->auth->userid();
		$this->db->where('id', $obj->permintaan_id)
				 ->update($this->table_def_permintaan, $dataPermintaan);
	}
}

?>