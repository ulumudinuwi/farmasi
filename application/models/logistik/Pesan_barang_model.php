<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pesan_barang_model extends CI_Model {
	protected $table_def_stock = "t_logistik_stock";
	protected $table_def_permintaan_detail = "t_request_permintaan_unit_detail";
	protected $table_def_barang = "m_barang";
	protected $table_def_satuan = "m_satuan";
	protected $table_def_vendor = "m_vendor";
	protected $table_def_pabrik = "m_pabrik";
	protected $table_def_pabrik_distributor = "m_pabrik_distributor";

	public function __construct() {
        parent::__construct();
    }

    private function _getSelectPermintaanDatatable() {
    	$sql = "(SELECT {$this->table_def_barang}.pabrik_id FROM ";
    	$sql .= "{$this->table_def_permintaan_detail} ";
    	$sql .= "LEFT JOIN {$this->table_def_barang} ON {$this->table_def_permintaan_detail}.barang_id = {$this->table_def_barang}.id ";
    	$sql .= "LEFT JOIN {$this->table_def_stock} ON {$this->table_def_stock}.barang_id = {$this->table_def_barang}.id ";
    	$sql .= "WHERE ";
    	$sql .= "{$this->table_def_barang}.status = 1 ";
    	$sql .= "AND {$this->table_def_stock}.dipesan = 0 ";
    	$sql .= "AND {$this->table_def_permintaan_detail}.qty > {$this->table_def_permintaan_detail}.dikirim ";
    	$sql .= "AND (ISNULL($this->table_def_stock.qty) OR {$this->table_def_stock}.qty < ({$this->table_def_permintaan_detail}.qty * {$this->table_def_permintaan_detail}.isi_satuan)) ";
    	$sql .= "GROUP BY {$this->table_def_barang}.pabrik_id) ";
    	return $sql;
    }

    private function _getSelectStockDatatable() {
    	$sql = "(SELECT {$this->table_def_barang}.pabrik_id FROM ";
    	$sql .= "{$this->table_def_stock} ";
    	$sql .= "LEFT JOIN {$this->table_def_barang} ON {$this->table_def_stock}.barang_id = {$this->table_def_barang}.id ";
    	$sql .= "WHERE ";
    	$sql .= "{$this->table_def_barang}.status = 1 ";
    	$sql .= "AND {$this->table_def_stock}.dipesan = 0 ";
    	$sql .= "AND {$this->table_def_stock}.qty <= {$this->table_def_stock}.reorder ";
    	$sql .= "GROUP BY {$this->table_def_barang}.pabrik_id) ";
    	return $sql;
    }

    public function datatable($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $sStatus = "") {
		$this->db->query("SET SESSION sql_mode='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';");
    	$sql = "SELECT {$this->table_def_pabrik_distributor}.pabrik_id, ";
    	$sql .= "CONCAT({$this->table_def_pabrik}.kode, ' - ', {$this->table_def_pabrik}.nama) pabrik, ";
    	$sql .= "{$this->table_def_pabrik_distributor}.vendor_id, ";
    	$sql .= "CONCAT({$this->table_def_vendor}.kode, ' - ', {$this->table_def_vendor}.nama) vendor ";
    	$sql .= "FROM {$this->table_def_pabrik_distributor} ";
    	$sql .= "LEFT JOIN {$this->table_def_pabrik} ON {$this->table_def_pabrik_distributor}.pabrik_id = {$this->table_def_pabrik}.id ";
    	$sql .= "LEFT JOIN {$this->table_def_vendor} ON {$this->table_def_pabrik_distributor}.vendor_id = {$this->table_def_vendor}.id ";
    	$sql .= "WHERE {$this->table_def_pabrik}.id IN (";
    	switch ($sStatus) {
    		case 1:
    			$sql .= $this->_getSelectStockDatatable();
    			break;
    		case 2:
    			$sql .= $this->_getSelectPermintaanDatatable();
    			break;
    		default:
    			$sql .= $this->_getSelectStockDatatable();
    			break;
    	}
    	$sql .= ") ";
    	if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
			$data['total_rows'] = $query->num_rows();
		}
		else {
			$data['data'] = array();
			$data['total_rows'] = 0;
		}
		return $data;
    }
	
	private function _get_select() {
		$select = array(
			"0 id",
			"{$this->table_def_stock}.barang_id",
			"({$this->table_def_stock}.maximum / {$this->table_def_barang}.isi_satuan_penggunaan) maximum",
    		"({$this->table_def_stock}.minimum / {$this->table_def_barang}.isi_satuan_penggunaan) minimum",
    		"({$this->table_def_stock}.qty / {$this->table_def_barang}.isi_satuan_penggunaan) stock",
			"{$this->table_def_barang}.kode kode_barang",
			"{$this->table_def_barang}.nama barang",
			"{$this->table_def_barang}.satuan_pembelian_id satuan_id",
			"{$this->table_def_satuan}.nama satuan_po",
			"{$this->table_def_barang}.isi_satuan_penggunaan isi_satuan",
			"{$this->table_def_barang}.harga_pembelian harga",
		);
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def_stock}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_barang} ON {$this->table_def_stock}.barang_id = {$this->table_def_barang}.id ";
		$join .= "LEFT JOIN {$this->table_def_satuan} ON {$this->table_def_barang}.satuan_pembelian_id = {$this->table_def_satuan}.id ";
		return $join;
	}
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
		$this->db->query("SET SESSION sql_mode='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';");
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def_stock}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select();
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}
}

?>