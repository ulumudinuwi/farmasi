<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model as Model_Eloquent;
use Carbon\Carbon;
use Ryuna\Auth;

class Pos_model extends Model_Eloquent
{
    public static $table_barang = 'm_barang';
    public static $table_stock_barang = 't_gudang_farmasi_stock';
    public static $table_stock_barang_detail = 't_gudang_farmasi_stock_detail';
    public static $table_promo_barang = 'm_promo';
    public static $table_dokter = 'm_dokter_member';
    public static $table_sales = 'm_sales';
    public static $table_pos = 't_pos';
    public static $table_pos_detail = 't_pos_detail';
    public static $table_pos_pembayaran = 't_pos_pembayaran';
    public static $table_pos_riwayat_approval = 't_pos_riwayat_approval';
    public static $table_pos_batal = 't_pos_batal';
    public static $table_pos_riwayat = 't_pos_riwayat';

    protected $table = 't_pos';
    public $timestamps = true;

    public static function getID($key,$field = 'uid'){
        return DB::table(self::$table_pos)->where($field,$key)->first()['id'];
    }
    public static function getUID($key,$field = 'id'){
        return DB::table(self::$table_pos)->where($field,$key)->first()['uid'];
    }
    public static function findByUID($uid){
        $data = self::getTransaksiDetail($uid);
        return $data;
    }
    public static function get_list_pos(){
        // Get Request
        $request = $this->input->post();

        // Select Columns
        $select = ['uid', 'nama', 'no_hp', 'email', 'alamat', 'status'];

        $this->datatables->select(implode(', ', $select));
        $this->datatables->from($this->table_def);
        return Response::datatables($this->datatables->generate());
    }
    public static function get_list_barang(){
        // dd(self::$table_barang);
        $request = $_POST;

        $page   = $_GET['page'] ? $_GET['page'] : 1;
        $limit  = 8;
        $offset = ($page - 1) * $limit;
        
        $q = $_GET['q'];
                
        // Select Columns
        $select = [
            self::$table_barang.'.uid as uid_barang',
            self::$table_barang.'.id as id_barang',
            self::$table_barang.'.nama',
            self::$table_barang.'.harga_penjualan as harga',
            self::$table_stock_barang.'.qty',
            self::$table_stock_barang.'.id as gudang_id',
            self::$table_stock_barang.'.qty',
            self::$table_stock_barang_detail.'.expired_date as expired_date',
        ];

        // Where Conditions_
        $filter = [];
        // foreach($select as $column){
        //     if(isset($request[$column]) && !empty($request[$column])) $filter[$column] = $request[$column];
        // }
        // Join
        $join = "INNER JOIN ".self::$table_barang." on ".self::$table_stock_barang.".barang_id = ".self::$table_barang.".id";
        $join .= " LEFT JOIN ".self::$table_stock_barang_detail." on "."(t_gudang_farmasi_stock_detail.stock_id = t_gudang_farmasi_stock.id AND t_gudang_farmasi_stock_detail.expired_date IS NOT NULL)";
        // Where
        $where  = " ";
        // $total_filter = count($filter);
        //$where .= "WHERE (".self::$table_barang.".status = 1 AND ".self::$table_stock_barang.".status = 1 AND ".self::$table_stock_barang.".qty > 0 AND ".self::$table_stock_barang.".dipesan = 0 )";
        $where .= "WHERE (".self::$table_barang.".status = 1 AND ".self::$table_stock_barang.".qty > 0)";
        if (Auth::user()->bpom == 1) {
            $where .= " AND ".self::$table_barang.".bpom = ".Auth::user()->bpom;
        }
        if(!empty($q) && $q != null){
            $where .= " AND (".self::$table_barang.".nama LIKE '%".$q."%' OR ".self::$table_barang.".harga_penjualan LIKE '%".$q."%')";
        }
       
        // Query
        $select = count($select) == 0 ? " * " : implode(',', $select);
        $query = "SELECT {$select} FROM ".self::$table_stock_barang." {$join}";
        if($where != " "){
            $query .= $where;
        }
        //Count
        $count = count(DB::select($query));
        //Order
        $query .= "";
        //LIMIT AND OFFSET
        $query .= " LIMIT {$limit} OFFSET {$offset}";
        // dd($query);
        // cek_dulu($query);
        $results = DB::select($query); 
        $resource = [];
        $i = 0;
        //dd($results);
        foreach($results as $data){
            $get_image = DB::table('m_barang_foto')->where('barang_id', $data['id_barang'])->first();
            $foto = base_url('assets/img/no_image_available.png');
            if(!empty($get_image)) $foto = base_url('uploads/barang/original/'.$get_image['nama']);

            $resource[$i]['uid_barang'] = $data['uid_barang'];
            $resource[$i]['id_barang'] = $data['id_barang'];
            $resource[$i]['nama'] = $data['nama'];
            $resource[$i]['harga'] = $data['harga'];
            $resource[$i]['gudang_stock_id'] = $data['gudang_id'];
            $resource[$i]['stock'] = $data['qty'];
            $resource[$i]['thumb'] = $foto;
            $resource[$i]['expired_date'] = $data['expired_date'];
            $i++;
        } 
        $list_barang = $resource;

        $total_page = round($count/$limit);
        $endCount   = $offset + $limit;
        $morePage   = $endCount < $count; 

         
        // cek_dulu($query);

        $results = [
            'totalPage'   => $total_page,
            'currentPage' => $page,
            'results' => $list_barang,
            'morePage' => $morePage,
        ];
        return $results;
    }

    public static function get_list_member(){
        $query = DB::table(self::$table_dokter);
        $query->select([
            self::$table_dokter.'.uid as uid_dokter',
            self::$table_dokter.'.no_member as no_member',
            self::$table_dokter.'.nama as nama_dokter',
            self::$table_dokter.'.no_hp as no_hp_dokter',
            self::$table_dokter.'.alamat as alamat_dokter',
            self::$table_dokter.'.email as email_dokter',
            self::$table_dokter.'.no_ktp as ktp_dokter',
            self::$table_dokter.'.no_npwp as npwp_dokter',
            self::$table_dokter.'.nama_klinik as nama_klinik_dokter',
            self::$table_dokter.'.alamat_klinik as alamat_klinik_dokter',
            self::$table_dokter.'.no_tlp_klinik as no_tlp_klinik_dokter',
            self::$table_dokter.'.plafon as plafon_dokter',
            self::$table_sales.'.uid as uid_sales',
            self::$table_sales.'.nama as nama_sales',
            self::$table_sales.'.email as email_sales',
            self::$table_sales.'.no_ktp as no_ktp_sales',
            self::$table_sales.'.no_hp as no_hp_sales',
            self::$table_sales.'.alamat as alamat_sales',
            self::$table_sales.'.tgl_bergabung as tgl_bergabung_sales',
        ]);
        $query->join(self::$table_sales, self::$table_dokter.'.sales_id', '=',self::$table_sales.'.id');
        $query->where(self::$table_sales.'.status', 1);
        // if(Auth::user()->sales_id != null){
        //     $query->where(self::$table_sales.'.id', Auth::user()->sales_id);
        // }
        $results = $query->get();
        return $results;
    }

    public static function find_member(){
        $request = $this->getRequest();
        $query = DB::table(self::$table_dokter);
        $query->select([
            self::$table_dokter.'.uid as uid_dokter',
            self::$table_dokter.'.nama as nama_dokter',
            self::$table_dokter.'.no_hp as no_hp_dokter',
            self::$table_dokter.'.email as email_dokter',
            self::$table_dokter.'.no_ktp as ktp_dokter',
            self::$table_dokter.'.no_npwp as npwp_dokter',
            self::$table_dokter.'.nama_klinik as nama_klinik_dokter',
            self::$table_dokter.'.alamat_klinik as alamat_klinik_dokter',
            self::$table_dokter.'.no_tlp_klinik as no_tlp_klinik_dokter',
            self::$table_dokter.'.plafon as plafon_dokter',
            self::$table_sales.'.uid as uid_sales',
            self::$table_sales.'.nama as nama_sales',
            self::$table_sales.'.email as email_sales',
            self::$table_sales.'.no_ktp as no_ktp_sales',
            self::$table_sales.'.no_hp as no_hp_sales',
            self::$table_sales.'.alamat as alamat_sales',
            self::$table_sales.'.tgl_bergabung as tgl_bergabung_sales',
        ]);
        $query->join(self::$table_sales, self::$table_dokter.'.sales_id', '=',self::$table_sales.'.id');
        if($request->q){
            $query->where(self::$table_dokter.'.nama', 'LIKE' ,'%'.$request->q.'%');
        }
        $query->where(self::$table_sales.'.status', 1);
        // if(Auth::user()->sales_id != null){
        //     $query->where(self::$table_sales.'.id', Auth::user()->sales_id);
        // }
        $results = $query->get();

        return $results;
    } 

    //Untuk Select2 Ajax
    public static function list_member_page(){
        $request = get_request(); 
        $page = (!empty( (Array) $request) && $request->page) ? $request->page : 1;
        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $query = DB::table(self::$table_dokter);
        $query->select([
            self::$table_dokter.'.id as id',
            self::$table_dokter.'.nama as text',
            self::$table_dokter.'.no_member as no_member',
            self::$table_dokter.'.uid as uid_dokter',
            self::$table_dokter.'.nama as nama_dokter',
            self::$table_dokter.'.no_hp as no_hp_dokter',
            self::$table_dokter.'.alamat as alamat_dokter',
            self::$table_dokter.'.email as email_dokter',
            self::$table_dokter.'.no_ktp as ktp_dokter',
            self::$table_dokter.'.no_npwp as npwp_dokter',
            self::$table_dokter.'.nama_klinik as nama_klinik_dokter',
            self::$table_dokter.'.alamat_klinik as alamat_klinik_dokter',
            self::$table_dokter.'.no_tlp_klinik as no_tlp_klinik_dokter',
            self::$table_dokter.'.plafon as plafon_dokter',
            self::$table_dokter.'.point as point_dokter',
            self::$table_sales.'.uid as uid_sales',
            self::$table_sales.'.nama as nama_sales',
            self::$table_sales.'.email as email_sales',
            self::$table_sales.'.no_ktp as no_ktp_sales',
            self::$table_sales.'.no_hp as no_hp_sales',
            self::$table_sales.'.alamat as alamat_sales',
            self::$table_sales.'.tgl_bergabung as tgl_bergabung_sales',
        ]);
        if(is_manager()){
            $get_id_with_sales_bawahan = get_bawahan_sales_id();
            $get_id_with_sales_bawahan[] = sales_id();
            // var_dump($get_id_with_sales_bawahan);
            if(count($get_id_with_sales_bawahan) > 0) {
                $query->whereIn('sales_id',$get_id_with_sales_bawahan);
            }
        }else if(is_sales()){
            $query->where('sales_id', sales_id());
        }
        $query->join(self::$table_sales, self::$table_dokter.'.sales_id', '=',self::$table_sales.'.id');
        $query->where(self::$table_sales.'.status', 1);
        $query->where(self::$table_dokter.'.nama', 'LIKE','%'.$request->term.'%');
        $query->orderBy(self::$table_dokter.'.nama', 'ASC');

        $total_count = $query->count();

        $query->skip($offset);
        $query->take($resultCount);
        $member = $query->get();
                
        $count = $query->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
          "results" => $member,
          "pagination" => array(
            "more" => $morePages
		  ),
		  "total_count" => $total_count,
        );
        return $results;
    }

    public static function saveDraf($request){
        if(!$request) return false;
        $now = Carbon::now()->format('Y-m-d H:m:s');
        DB::table('t_temporary_pos')->insert([
            'uid' => uuid(),
            'save_belanjaan' => $request->json_data,
            'created_at' => $now,
            'created_by' => Auth::user()->id,
            'updated_at' => $now,
            'updated_by' => Auth::user()->id,
        ]);

        return true;
    }

    public static function processOrder($object){
        //Setup Data

        $CI =& get_instance();
        $CI->load->helper('gudang_farmasi');
        $config_tipe = $CI->config->item('tipe_kartu_stock_pengeluaran');

        $with_manager = array_filter((Array) $object->cart->list_barang,function($item){
            return $item->mustApprove == 'with_manager';
        });
        $with_direktur = array_filter((Array) $object->cart->list_barang,function($item){
            return $item->mustApprove == 'with_direktur';
        });
        
        $keterangan = '<ul>';
        $total_with_manager = count($with_manager);
        $total_with_direktur = count($with_direktur);
        
        $is_approved  = 1;
        $verifikasi_pos_manager  = 1;
        $verifikasi_diskon_manager  = 1;
        $verifikasi_diskon_direktur = 1;
        $json_keterangan = [];
        
        if($total_with_manager > 0){
            foreach($with_manager as $item){
                $_data = [
                    'type' => 'per_item',
                    'nama_barang' => $item->nama,
                    'qty' => $item->qty,
                    'type_disc' => 'persen',
                    'disc' => $item->disc,
                    'total' => $item->total_harga,
                    'must_approve' => 'with_manager',
                ];
                $_data_t = 'Nama: '.$item->nama.' - Qty:'.$item->qty.' - Disc:'.$item->disc.'% - Total: Rp.'.toRupiah($item->total_harga);
                $json_keterangan[] = $_data;
                $keterangan .= '<li>'.$_data_t.'</li>';
            }
        }
        if($total_with_direktur > 0){
            foreach($with_direktur as $item){
                $_data = [
                    'type' => 'per_item',
                    'nama_barang' => $item->nama,
                    'qty' => $item->qty,
                    'type_disc' => 'persen',
                    'disc' => $item->disc,
                    'total' => $item->total_harga,
                    'must_approve' => 'with_direktur',
                ];
                $_data_t = 'Nama: '.$item->nama.' - Qty:'.$item->qty.' - Disc:'.$item->disc.'% - Total: Rp.'.toRupiah($item->total_harga);
                $json_keterangan[] = $_data;
                $keterangan .= '<li>'.$_data_t.'</li>';
            }
        }
        if($object->transaksi->mustApprove == 'with_manager'){
            //$total_with_manager += 1;
            if($object->transaksi->type_disc == 'persen'){
                $_data = [
                    'type' => 'seluruh',
                    'nama_barang' => '',
                    'qty' => '',
                    'type_disc' => 'persen',
                    'disc' => $object->transaksi->disc,
                    'total' => $object->transaksi->grand_total,
                    'must_approve' => 'with_manager',
                ];
                $json_keterangan[] = $_data;
                $_data_t = 'Diskon keseluruhan '.$object->transaksi->disc.'%';
                $keterangan .= '<li>'.$_data_t.'</li>';
            }else{
                $_data = [
                    'type' => 'seluruh',
                    'nama_barang' => '',
                    'qty' => '',
                    'type_disc' => 'nominal',
                    'disc' => $object->transaksi->disc,
                    'total' => $object->transaksi->grand_total,
                    'must_approve' => 'with_manager',
                ];
                $json_keterangan[] = $_data;
                $_data_t = 'Diskon keseluruhan Rp.'.toRupiah($object->transaksi->disc);
                $keterangan .= '<li>'.$_data_t.'</li>';
            }
        }else if($object->transaksi->mustApprove == 'with_direktur'){
            //$total_with_direktur += 1;
            if($object->transaksi->type_disc == 'persen'){
                $_data = [
                    'type' => 'seluruh',
                    'nama_barang' => '',
                    'qty' => '',
                    'type_disc' => 'persen',
                    'disc' => $object->transaksi->disc,
                    'total' => $object->transaksi->grand_total,
                    'must_approve' => 'with_direktur',
                ];
                $json_keterangan[] = $_data;
                $_data_t = 'Diskon keseluruhan '.$object->transaksi->disc.'%';
                $keterangan .= '<li>'.$_data_t.'</li>';
            }else{
                $_data = [
                    'type' => 'seluruh',
                    'nama_barang' => '',
                    'qty' => '',
                    'type_disc' => 'persen',
                    'disc' => $object->transaksi->disc,
                    'total' => $object->transaksi->grand_total,
                    'must_approve' => 'with_direktur',
                ];
                $json_keterangan[] = $_data;
                $_data_t = 'Diskon keseluruhan Rp.'.toRupiah($object->transaksi->disc);
                $json_keterangan[] = $_data;
                $keterangan .= '<li>'.$_data_t.'</li>';
            }
        }else{
            if ($object->transaksi->disc > 0) {
                if($object->transaksi->type_disc == 'persen'){
                    $_data = [
                        'type' => 'seluruh',
                        'nama_barang' => '',
                        'qty' => '',
                        'type_disc' => 'persen',
                        'disc' => $object->transaksi->disc,
                        'total' => $object->transaksi->grand_total,
                        'must_approve' => 'without_approve',
                    ];
                    $json_keterangan[] = $_data;
                    $_data_t = 'Diskon keseluruhan '.$object->transaksi->disc.'%';
                    $keterangan .= '<li>'.$_data_t.'</li>';
                }else{
                    $_data = [
                        'type' => 'seluruh',
                        'nama_barang' => '',
                        'qty' => '',
                        'type_disc' => 'persen',
                        'disc' => $object->transaksi->disc,
                        'total' => $object->transaksi->grand_total,
                        'must_approve' => 'without_approve',
                    ];
                    $json_keterangan[] = $_data;
                    $_data_t = 'Diskon keseluruhan Rp.'.toRupiah($object->transaksi->disc);
                    $json_keterangan[] = $_data;
                    $keterangan .= '<li>'.$_data_t.'</li>';
                }
            }
        }
        $keterangan .= '</ul>';
        $json_keterangan = json_encode($json_keterangan);
        
        $now = Carbon::now()->format('Y-m-d H:m:s');
        $is_kurir = $object->transaksi->is_kurir ? 1 : 0;
        $is_ongkir_free = $object->transaksi->grand_total >= 3000000 ? 1 : 0;

        DB::beginTransaction();
        try {
            $pos = DB::table(self::$table_pos)->insertGetId([
                'uid' => uuid(),
                //'no_invoice' => $no_invoice,
                'member_id' => $object->dokter->id,
                'sales_id' => sales_id(),
                'piutang' => $object->transaksi->piutang ? 1 : 0,
                'tanggal_transaksi' => $object->cart->tgl_transaksi,
                'total_harga' => $object->transaksi->total,
                'jenis_diskon' => $object->transaksi->type_disc,
                'diskon' => $object->transaksi->disc,
                'grand_total' => $object->transaksi->grand_total,
                'sisa_pembayaran' => $object->transaksi->sisa_pembayaran,
                'total_pembayaran' => $object->transaksi->total_pembayaran,
                'status_lunas' => $object->transaksi->sisa_pembayaran > 0 ? 0 : 1,
                'status_pembayaran' => $object->status_pembayaran,
                'point_didapat' => $object->transaksi->point_didapat,
                'tgl_jatuh_tempo' => Carbon::parse($object->cart->tgl_transaksi)->addDays(30),
                'keterangan_verifikasi' => $keterangan,
                'json_keterangan' => $json_keterangan,
                'is_kurir' => 0,
                'ekspedisi' => $is_kurir ? $object->transaksi->ekspedisi : '',
                'ongkir_free' => $is_kurir ? $is_ongkir_free : 0,
                'ongkir' => $is_kurir ? $object->transaksi->ongkir : 0,
                'internal_used' => $object->cart->internal_used, 
                'created_at' => $now,
                'created_by' => Auth::user()->id,
                'updated_at' => $now,
                'updated_by' => Auth::user()->id,
                'json_before_process' => json_encode($object->json_original),
            ]);
            
            //Point Masuk
            $_object = new stdClass();
            $_object->member = $object->dokter;
            $_object->sales = $object->sales;

            $ket = 'Transkasi Baru';
            point_masuk($_object,$object->transaksi->point_didapat,$pos,$ket);

            $total_harga_before_diskon = 0;
            foreach($object->cart->list_barang as $item){
                //total_harga_before_diskon
                $total_harga_before_diskon += harga_sebelum_diskon($item->total_harga, $item->disc);

                $objDetail = DB::table(self::$table_pos_detail)->insertGetId([
                    'uid' => uuid(),
                    'member_id' => $object->dokter->id,
                    'sales_id' => sales_id(),
                    'pos_id' => $pos,
                    'no_fbp' => get_no_fbp(),
                    'barang_id' => $item->id,
                    'qty' => $item->qty,
                    'harga' => $item->harga_persatu,
                    'total' => harga_sebelum_diskon($item->total_harga, $item->disc),
                    'jenis_diskon' => 'persen',
                    'diskon' => $item->disc,
                    'diskon_plus' => $item->disc_plus,
                    'is_bonus' => $item->is_bonus,
                    'is_bonus_detail' => json_encode($item->bonus_from_barang),
                    'grand_total' => $item->total_harga,
                    'created_at' => $now,
                    'created_by' => Auth::user()->id,
                    'updated_at' => $now,
                    'updated_by' => Auth::user()->id,
                ]);
                diskon_mou($objDetail);
                $getBarang = DB::table('m_barang')->where('id', $item->id)->first();

                $bpom = $getBarang['bpom'];
                if ($bpom == 1) {
                    $ppn = $getBarang['ppn'];
                    $no_invoice = get_no_invoice('PER%');
                }else{
                    $ppn = '0';
                    $no_invoice = get_no_invoice('SI%');
                }

                $obj = new stdClass();
                $obj->tanggal = $object->cart->tgl_transaksi;
                $obj->kode = $no_invoice;
                $obj->id = $pos;
                $obj->keterangan = 'Pengeluaran POS Barang NO.INVOICE '.$no_invoice;

                $detail = new stdClass();
                $detail->barang_id = $item->id;
                $detail->qty = $item->qty;
                $detail->id = $objDetail;
                 
                $json_response = procStockKeluar($obj,$detail,$config_tipe);

                DB::table(self::$table_pos_detail)->where('id', $objDetail)->update([
                            'json_response' => json_encode($json_response),
                        ]);
            }

            if($total_with_manager > 0){
                $verifikasi_diskon_manager  = 0;
                $verifikasi_pos_manager  = 0;
                $is_approved  = 0;
                //add notif
                $dataNotif = new stdClass();
                $dataNotif->role_id = self::getRole('manager')['id'];
                $dataNotif->description = 'Data No Invoice '.$no_invoice.' harus di Approve dengan diskon 25%';
                $dataNotif->link = 'sales/pos/list_approval';
                //insert
                insertNotif($dataNotif);
            }
            
            if($total_with_direktur > 0){
                $verifikasi_diskon_direktur  = 0;
                $verifikasi_pos_manager  = 0;
                $is_approved  = 0;
                //add notif
                $dataNotif = new stdClass();
                $dataNotif->role_id = self::getRole('direktur')['id'];
                $dataNotif->description = 'Data harus No Invoice '.$no_invoice.' di Approve dengan diskon diatas 25%';
                $dataNotif->link = 'sales/pos/list_approval';
                //insert
                insertNotif($dataNotif);
            }
            
            DB::table(self::$table_pos)->where('id', $pos)->update([
                        'bpom' => $bpom,
                        'ppn' => $ppn,
                        'no_invoice' => $no_invoice,
                        'verifikasi_pos_manager' => $verifikasi_pos_manager,
                        'verifikasi_diskon_manager' => $verifikasi_diskon_manager,
                        'verifikasi_diskon_direktur' => $verifikasi_diskon_direktur,
                        'is_approved' => $is_approved,
                        'total_harga_before_diskon' => $total_harga_before_diskon,
                    ]);

            if ($object->status_pembayaran == 2) {
                $is_cicilan = $object->transaksi->sisa_pembayaran > 0 ? 1 : 0;
                $cicilan_ke = $is_cicilan ? cicilan_ke($pos)['cicilan_ke'] : 0;
            } $is_cicilan = 0; $cicilan_ke = 0;

            /*if(!$object->transaksi->piutang && !$object->cart->internal_used){
                $no_kuitansi = get_no_kuitansi();
                foreach($object->transaksi->metode_pembayarans as $item){
                    DB::table(self::$table_pos_pembayaran)->insert([
                        'uid' => uuid(),
                        'no_kuitansi' => $no_kuitansi,
                        'no_receipt' => $item->receipt_no,
                        'member_id' => $object->dokter->id,
                        'sales_id' => sales_id(),
                        'pos_id' => $pos,
                        'total_harus_dibayar' => $object->transaksi->grand_total,
                        'jenis_pembayaran' => $item->cara_bayar,
                        'nominal_pembayaran' => $item->nominal,
                        'bank_id' => $item->cara_bayar != 'tunai' ? $item->bank_id : 0,
                        'mesin_edc_id' => $item->cara_bayar != 'tunai' ? $item->mesin_edc_id : 0,
                        'is_cicilan' => $is_cicilan,
                        'cicilan_ke' => $cicilan_ke,
                        'created_at' => $now,
                        'created_by' => Auth::user()->id,
                        'updated_at' => $now,
                        'updated_by' => Auth::user()->id,
                    ]);
                }
            } */           
            //Record
            $pos_uid = self::getUID($pos,'id');
            self::procHistoryPos($pos_uid,'new');
            DB::commit();
            // all good
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            print_r($e->getMessage());
            return false;
            // something went wrong
        }
    }

    public static function processOrderUpdate($object){
        //Setup Data
        $CI =& get_instance();
        $CI->load->helper('laba_rugi_helper');
        $pos_id = self::getID($object->uid,'uid');
        $dTrnsaksi = self::findByUID($object->uid,'uid');
        $now = Carbon::now()->format('Y-m-d H:m:s');
        
        $is_kurir = $object->transaksi->is_kurir ? 1 : 0;
        $is_ongkir_free = $object->transaksi->grand_total >= 3000000 ? 1 : 0;
        $no_kuitansi = get_no_kuitansi();

        DB::beginTransaction();

        try {
            $pos = DB::table(self::$table_pos)->where('uid', $object->uid)->update([
                        'piutang' => $object->transaksi->piutang ? 1 : 0,
                        'is_kurir' => $is_kurir,
                        'ekspedisi' => $is_kurir ? $object->transaksi->ekspedisi : '',
                        'ongkir_free' => $is_kurir ? $is_ongkir_free : 0,
                        'ongkir' => $is_kurir ? $object->transaksi->ongkir : 0,
                        'grand_total' => $object->grand_total_harga,
                        'total_harga' => $object->grand_total_harga,
                        'sisa_pembayaran' => $object->transaksi->sisa_pembayaran,
                        'tgl_jatuh_tempo' => $object->tgl_jatuh_tempo,
                        'status_pembayaran' => $object->status_pembayaran,
                        'status_lunas' => $object->transaksi->sisa_pembayaran > 0 ? 0 : 1,
                        'updated_at' => $now,
                        'updated_by' => Auth::user()->id,
                    ]);

            if ($object->status_pembayaran == 2) {
                $is_cicilan = $object->transaksi->sisa_pembayaran > 0 ? 1 : 0;
                $cicilan_ke = $is_cicilan ? cicilan_ke($pos)['cicilan_ke'] : 0;
            } $is_cicilan = 0; $cicilan_ke = 0;


            $dataLabaRugi = new stdClass();
            $dataLabaRugi->transaksi_id = $pos_id;
            $dataLabaRugi->bpom = $CI->session->userdata('bpom');
            $dataLabaRugi->total = $object->grand_total_harga;
            $dataLabaRugi->keterangan = 'Pembayaran POS dengan metode pembayaran PIUTANG <b>No Kwitansi : '.$no_kuitansi.'</b>';
            $dataLabaRugi->tipe = JENIS_KREDIT;
            $dataLabaRugi->modul = JENIS_MODUL_POS;

            if(!$object->transaksi->piutang){
                foreach($object->transaksi->metode_pembayarans as $item){
                    $pembayaran = DB::table(self::$table_pos_pembayaran)->insertGetId([
                        'uid' => uuid(),
                        'no_kuitansi' => $no_kuitansi,
                        'no_receipt' => $item->receipt_no,
                        'tanggal_bayar' => $now,
                        'member_id' => $dTrnsaksi['detail_member']['member']['id'],
                        'sales_id' => $dTrnsaksi['detail_member']['sales']['id'],
                        'pos_id' => $pos_id,
                        'total_harus_dibayar' => $object->grand_total_harga,
                        'jenis_pembayaran' => $item->cara_bayar,
                        'nominal_pembayaran' => $item->nominal,
                        'bank_id' => $item->cara_bayar != 'tunai' ? $item->bank_id : 0,
                        'mesin_edc_id' => $item->cara_bayar != 'tunai' ? $item->mesin_edc_id : 0,
                        'is_cicilan' => $is_cicilan,
                        'cicilan_ke' => $cicilan_ke,
                        'created_at' => $now,
                        'created_by' => Auth::user()->id,
                        'updated_at' => $now,
                        'updated_by' => Auth::user()->id,
                    ]);
                    $dataLabaRugi->perkiraan_id = get_option('perkiraan_pos');
                    $dataLabaRugi->transaksi_id = $pembayaran;
                    $dataLabaRugi->bpom = $CI->session->userdata('bpom');
                    $dataLabaRugi->total = $item->nominal;
                    $dataLabaRugi->keterangan = 'Pembayaran POS dengan metode pembayaran '.$item->cara_bayar.' <b>No Kwitansi : '.$no_kuitansi.'</b>';
                    $dataLabaRugi->tipe = JENIS_KREDIT;
                    insertLabarugi($dataLabaRugi);
                    
                    $dataLabaRugi->perkiraan_id = get_option('perkiraan_kas');
                    $dataLabaRugi->tipe = JENIS_DEBIT;
                    insertLabarugi($dataLabaRugi);
                }
                if ($object->transaksi->sisa_pembayaran > 0) {
                    $dataLabaRugi->transaksi_id = $pos_id;
                    $dataLabaRugi->total = $object->transaksi->sisa_pembayaran;
                    $dataLabaRugi->keterangan = 'Pembayaran POS dengan metode pembayaran PIUTANG <b>No Kwitansi : '.$no_kuitansi.'</b>';
                    $dataLabaRugi->perkiraan_id = get_option('perkiraan_piutang');
                    $dataLabaRugi->tipe = JENIS_KREDIT;
                    insertLabarugi($dataLabaRugi);

                    $dataLabaRugi->perkiraan_id = get_option('perkiraan_kas');
                    $dataLabaRugi->tipe = JENIS_DEBIT;
                    insertLabarugi($dataLabaRugi);
                }
            }else{
                $dataLabaRugi->perkiraan_id = get_option('perkiraan_piutang');
                insertLabarugi($dataLabaRugi);
              
                $dataLabaRugi->perkiraan_id = get_option('perkiraan_kas');
                $dataLabaRugi->tipe = JENIS_DEBIT;
              
                insertLabarugi($dataLabaRugi);
            }
            
            //Record
            $pos_uid = self::getUID($pos,'id');
            DB::commit();
            // all good
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            print_r($e->getMessage());
            return false;
            // something went wrong
        }
    }

    private static function procHistoryPos($pos_uid,$type_action){
        $pos_id = self::getID($pos_uid,'uid');
        $history = DB::table(self::$table_pos_riwayat)->insert([
            'uid' => uuid(),
            'pos_id' => $pos_id,
            'type_action' => $type_action,
            'created_at' => timestamp(),
            'created_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);

        return true;
    }

    private static function getRole($name){
        $role = DB::table('acl_roles')->where('name','LIKE','%'.$name.'%')->first();
        if(!$role){
            $role_id = DB::table('acl_roles')->insertGetId([
                'name' => $name,
                'status' => 1,
                'created_at' => 1,
                'created_by' => Auth::user()->id,
                'updated_at' => 1,
                'updated_by' => Auth::user()->id,
            ]);

            $role = DB::table('acl_roles')->where('id', $role_id)->first();
        }

        // dd($role);
        return $role;
    }

    public static function pos_datatables_template($field = [], $sWhere = [], $transaksi_only = FALSE){
        if(empty($field)) return false;
        /*Query*/
        $query = DB::table(self::$table_pos);
        $query->select([
            self::$table_pos.'.id',
            self::$table_pos.'.uid',
            self::$table_pos.'.no_invoice',
            self::$table_pos.'.member_id',
            self::$table_pos.'.sales_id',
            self::$table_pos.'.status_lunas',
            self::$table_pos.'.diskon',
            self::$table_pos.'.tanggal_transaksi',
            self::$table_pos.'.json_keterangan',
            self::$table_pos.'.grand_total',
            self::$table_pos.'.verifikasi_pos_manager',
            self::$table_pos.'.verifikasi_diskon_direktur',
            self::$table_pos.'.verifikasi_diskon_manager',
            self::$table_pos.'.canceled_at',
            self::$table_pos.'.piutang',
            self::$table_pos.'.status_pembayaran',
            self::$table_pos.'.is_rejected',
            self::$table_pos.'.tanggal_transaksi as tanggal',
            self::$table_pos.'.flag',
            self::$table_dokter.'.nama as nama_dokter',
            self::$table_dokter.'.no_member as no_member',
            self::$table_dokter.'.uid as uid_dokter',
            self::$table_sales.'.nama as nama_sales',
        ]);

        $query->join(self::$table_dokter,self::$table_dokter.'.id','=',self::$table_pos.'.member_id');
        $query->join(self::$table_sales,self::$table_sales.'.id','=',self::$table_pos.'.sales_id', 'left');
        
        //Khusus
        if($transaksi_only){
            if(is_manager()){
                $get_id_with_sales_bawahan = get_bawahan_sales_id();
                $get_id_with_sales_bawahan[] = sales_id();
                // var_dump($get_id_with_sales_bawahan);
                if(count($get_id_with_sales_bawahan) > 0) {
                    $query->whereIn(self::$table_pos.'.sales_id',$get_id_with_sales_bawahan);
                }
            }else if(is_sales()){
                $query->where(self::$table_pos.'.sales_id', sales_id());
            }
            if (Auth::user()->bpom == 1) {
                $query->where(self::$table_pos.".bpom", Auth::user()->bpom);
            }
        }
        
        //Filter Filter Disini
        $filter_range = isset($_REQUEST['filter_range']) ? $_REQUEST['filter_range']: null;
        $filter_status = isset($_REQUEST['filter_status']) ? $_REQUEST['filter_status'] : null;
        $filter_status_lunas = isset($_REQUEST['filter_status_lunas']) ? $_REQUEST['filter_status_lunas'] : null;
        // dd($filter_range);
        
        //Tanggal
        if(isset($filter_range)){
            $query->whereDate(self::$table_pos.'.tanggal_transaksi','>=',$filter_range[0]);
            $query->whereDate(self::$table_pos.'.tanggal_transaksi','<=',$filter_range[1]);
        }

        //Status
        if(isset($filter_status)){
            switch ($filter_status) {
                case 'Approved':
                    $query->where('verifikasi_pos_manager', 1)
                    ->where('verifikasi_diskon_manager', 1)
                    ->where('verifikasi_diskon_direktur', 1);
                    $query->where('is_rejected', 0);
                break;
                
                case 'Waiting':
                    $col = ['verifikasi_pos_manager', 'verifikasi_diskon_manager', 'verifikasi_diskon_direktur'];
                    $sCol = [];
                    foreach($col as $sfield){
                        $sCol[] = $sfield.' = 0';
                    }
                    $sNewCol = implode(' OR ',$sCol);
                    $query->whereRaw('('.$sNewCol.')');
                    $query->where('is_rejected', 0);
                break;

                case 'Rejected':
                    $query->where('is_rejected', 1);
                break;
                
                default:
                    # code...
                break;
            }
        }

        // Status Lunas
        if(isset($filter_status_lunas)){
            switch ($filter_status_lunas) {
                case 'Sudah':
                    $query->where('status_lunas', 1);
                break;

                case 'Belum':
                    $query->where('status_lunas', 0);
                break;
                
                default:
                    # code...
                    break;
            }
        }

        if(!empty($sWhere)){
            foreach($sWhere as $_field => $_search){
                $query->where($_field, $_search);
            }
        }
        // dd($field);
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw= $_REQUEST ? $_REQUEST['draw'] : 0;

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length= $_REQUEST ? $_REQUEST['length'] : 10;

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start= $_REQUEST ? $_REQUEST['start'] : 0;

		/*Keyword yang diketikan oleh user pada field pencarian*/
        $search = $_REQUEST ? $_REQUEST['search']["value"] : '';
        
        /*Order Column*/
		$order = $_REQUEST && isset($_REQUEST['order']) ? $_REQUEST['order'][0] : null;
        

		/*Menghitung total data didalam database*/
		$total= $query;
		$total= $total->count();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output = [];

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw'] = $draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal'] = $output['recordsFiltered'] = $total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data'] = [];

		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
        */
        // dd(count($_REQUEST['columns']));
		if($search != ""){
            $sNewColumn = "";
            $new_column = [];
            for($i = 0; $i < count($_REQUEST['columns']); $i++){
                if($_REQUEST){
                    $is_searchable = $_REQUEST['columns'][$i]['searchable'];
                    if($is_searchable == "true"){
                        $new_column[] = $field[$i].' LIKE "%'.$search.'%"';
                    }
                }
            }
            
            if(count($new_column) > 0) $sNewColumn = implode(' OR ', $new_column);

            if($sNewColumn) $query->whereRaw('('.$sNewColumn.')');

            $countSearch = $query;
            $countSearch = $countSearch->count(); 
            $output['recordsTotal'] = $output['recordsFiltered'] = $countSearch;
        }
        
        // dd($_REQUEST['columns'], $field);
        // dd($query->toSql());

		/*Limit dan Offset*/
        $query->skip($start);
        $query->limit($length);

        // dd($field);
        /*Ordering*/
		for($i = 0; $i < count($field); $i++){
            if($_REQUEST){
                $is_orderable = $_REQUEST['columns'][$i]['orderable'];
                if($is_orderable == 'true'){
                    if($order['column'] == $i){
                        $query->orderBy($field[$i],$order['dir']);
                    }
                }
            }
        }
		$results = $query->get();

        /**
         * Hasil dari Generate datatable
        */
        $output['data'] = $results;

		return $output;
    }

    public static function approve_pos_manager($pos_uid){
        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'verifikasi_pos_manager' => 1,
            'verifikasi_pos_manager_at' => timestamp(),
            'verifikasi_pos_manager_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
        $type_approval = 'approval_pos_manager';
        $type_action = 'approve';
        self::procTransactionApprove($pos_uid);
        self::procHistoryApproval($pos_uid, $type_approval, $type_action);

        return true;
    }

    public static function reject_pos_manager($pos_uid, $rejected_description = ""){
        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'verifikasi_pos_manager' => 0,
            'verifikasi_pos_manager_at' => timestamp(),
            'verifikasi_pos_manager_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
        
        $type_approval = 'approval_pos_manager';
        $type_action = 'reject';
        self::procTransactionReject($pos_uid, $rejected_description);
        self::procHistoryApproval($pos_uid, $type_approval, $type_action, $rejected_description);
        return true;
    }

    public static function approve_diskon_manager($pos_uid){
        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'verifikasi_diskon_manager' => 1,
            'verifikasi_diskon_manager_at' => timestamp(),
            'verifikasi_diskon_manager_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
        $type_approval = 'approval_diskon_manager';
        $type_action = 'approve';
        self::procTransactionApprove($pos_uid);
        self::procHistoryApproval($pos_uid, $type_approval, $type_action);
        return true;
    }

    public static function reject_diskon_manager($pos_uid, $rejected_description = ""){
        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'verifikasi_diskon_manager' => 0,
            'verifikasi_diskon_manager_at' => timestamp(),
            'verifikasi_diskon_manager_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
        
        $type_approval = 'approval_diskon_manager';
        $type_action = 'reject';
        self::procTransactionReject($pos_uid, $rejected_description);
        self::procHistoryApproval($pos_uid, $type_approval, $type_action, $rejected_description);
        return true;
    }

    public static function approve_diskon_direktur($pos_uid){
        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'verifikasi_diskon_direktur' => 1,
            'verifikasi_diskon_direktur_at' => timestamp(),
            'verifikasi_diskon_direktur_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
        $type_approval = 'approval_diskon_direktur';
        $type_action = 'approve';
        self::procTransactionApprove($pos_uid);
        self::procHistoryApproval($pos_uid, $type_approval, $type_action);
        return true;
    }

    public static function reject_diskon_direktur($pos_uid,  $rejected_description = ""){
        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'verifikasi_diskon_direktur' => 0,
            'verifikasi_diskon_direktur_at' => timestamp(),
            'verifikasi_diskon_direktur_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
        
        $type_approval = 'approval_diskon_direktur';
        $type_action = 'reject';
        self::procTransactionReject($pos_uid, $rejected_description);
        self::procHistoryApproval($pos_uid, $type_approval, $type_action, $rejected_description);
        return true;
    }

    private static function procTransactionReject($pos_uid, $rejected_description){
        $pos_batal = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'is_rejected' => 1,
            'is_approved' => 0,
            'rejected_at' => timestamp(),
            'rejected_by' => auth_id(),
            'rejected_description' => $rejected_description,
            // 'created_at' => timestamp(),
            // 'created_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);

        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->first();

        $point_terdahulu = $pos['point_didapat'];
        $ket = 'Transaksi Ditolak';

        $_object = new stdClass();
        $_object->sales = (Object) DB::table(self::$table_sales)->where('id', $pos['sales_id'])->first();
        $_object->member = (Object) DB::table(self::$table_dokter)->where('id', $pos['member_id'])->first();
        // dd($pos, $point_terdahulu, $_object, $pos['id'], $ket);
        point_keluar($_object,$point_terdahulu,$pos['id'],$ket);
            
        //Pengembalian Stock
        //Get detail pembelian
        $pos = DB::table(self::$table_pos)->where('uid', $pos_uid)->first();
        $detail_barang = DB::table(self::$table_pos_detail)->where('pos_id', $pos['id'])->get();

        //Get Config
        $CI =& get_instance();
        $CI->load->helper('gudang_farmasi');
        $config_tipe =  $CI->config->item('tipe_kartu_stock_penerimaan');
            
        //Return Barang
        foreach($detail_barang as $item){
            $no_invoice = $pos['no_invoice'];

            $obj = new stdClass();
            $obj->tanggal = $pos['tanggal_transaksi'];
            $obj->kode = $no_invoice;
            $obj->id = $pos['id'];
            $obj->keterangan = 'Pengembailan POS Barang NO.INVOICE'.$no_invoice;
            
            $detail = new stdClass();
            $detail->barang_id = $item['barang_id'];
            $detail->qty = $item['qty'];
            $detail->id = $item['id'];
            $detail->data_barang = json_decode($item['json_response']);

            DB::table(self::$table_pos_detail)->where('id', $item['id'])->update([
                'rejected_at' => timestamp(),
                'rejected_by' => auth_id(),
            ]);
            procStockMasukByTransaksi($obj,$detail,$config_tipe);
        }
        //Ari lanjut
        
    }

    private static function returnBarang(){
        $CI =& get_instance();
        $CI->load->helper('gudang_farmasi');
        $config_tipe = $CI->config->item('tipe_kartu_stock_pengeluaran');

        procStockMasukByTransaksi();
    }
    private static function procTransactionApprove($pos_uid){
        $pos_berhasil = DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
            'approved_at' => timestamp(),
            'approved_by' => auth_id(),
            // 'created_at' => timestamp(),
            // 'created_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
        
        $pos = (Object) DB::table(self::$table_pos)->where('uid', $pos_uid)->first();
        // dd($pos);
        if($pos->verifikasi_pos_manager && $pos->verifikasi_diskon_direktur && $pos->verifikasi_diskon_manager){
            DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
                'is_rejected' => 0,
                'is_approved' => 1,
            ]);
        }
        
    }

    private static function procHistoryApproval($pos_uid, $type_approval, $type_action, $description = ''){
        $pos = (Object) DB::table(self::$table_pos)->where('uid', $pos_uid)->first();
        $pos_id = $pos->id;

        $history_approval = DB::table(self::$table_pos_riwayat_approval)->insert([
            'uid' => uuid(),
            'pos_id' => $pos_id,
            'type_approval' => $type_approval,
            'type_action' => $type_action,
            'description' => $description,
            'created_at' => timestamp(),
            'created_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);

        if($type_action == 'reject'){
            DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
                'rejected_at' => timestamp(),
                'rejected_by' => auth_id(),
            ]);
        }else if($type_action == 'approve'){
            DB::table(self::$table_pos)->where('uid', $pos_uid)->update([
                'approved_at' => timestamp(),
                'approved_by' => auth_id(),
            ]);
        }

    }

    public static function getHistoryApproval($type, $pos_id, $all = FALSE){
        $history = DB::table(self::$table_pos_riwayat_approval);
        if($type == 'approve'){
            $history->where('type_action','approve')->orderBy('created_at', 'DESC');
            if(!$all) $history->where('pos_id', $pos_id);
        }else if($type == 'reject'){
            $history->where('type_action','reject')->orderBy('created_at', 'DESC');
            if(!$all) $history->where('pos_id', $pos_id);
        }
        return $history->get();
    }

    public static function getTransaksiDetail($uid){
        $arr = [];

        $pos = DB::table(self::$table_pos)->where('uid', $uid)->first();
        $pos_pembayaran = DB::table(self::$table_pos_pembayaran)->where('pos_id', $pos['id'])->get();
        
        $get_cicilan = DB::table(self::$table_pos_pembayaran)->select('cicilan_ke')
            ->where('is_cicilan', 1)
            ->where('pos_id', $pos['id'])
            ->groupBy('cicilan_ke')
            ->get();
        $is_cicilan = count($get_cicilan) > 0 ? 1 : 0;
        // dd($get_cicilan);
        $pos_cicilan = [];
        $no_kuitansi = [];
        
        if($is_cicilan && !$pos['piutang']){
            foreach($get_cicilan as $data){
                $pos_cicilan["cicilan_ke_".$data['cicilan_ke']] = DB::table(self::$table_pos_pembayaran)
                    ->where('is_cicilan', 1)
                    ->where('cicilan_ke', $data['cicilan_ke'])
                    ->where('pos_id', $pos['id'])
                    ->get();
                $no_kuitansi["cicilan_ke_".$data['cicilan_ke']] = DB::table(self::$table_pos_pembayaran)
                    ->select('no_kuitansi')
                    ->where('is_cicilan', 1)
                    ->where('cicilan_ke', $data['cicilan_ke'])
                    ->where('pos_id', $pos['id'])
                    ->first();
            }
        }else if(!$pos['piutang']){
            $no_kuitansi = $pos_pembayaran ? $pos_pembayaran[0]['no_kuitansi'] : "";
        }

        
        // dd($pos_cicilan);
        $detail_member = [];
        $detail_member['member'] = DB::table(self::$table_dokter)->where('id', $pos['member_id'])->first();
        $detail_member['sales'] = DB::table(self::$table_sales)->where('id', $detail_member['member']['sales_id'])->first();

        $detail_pembelian = DB::table(self::$table_pos_detail)
                ->select([self::$table_barang.'.nama as nama',self::$table_pos_detail.'.*'])
                ->join(self::$table_barang,self::$table_barang.'.id','=',self::$table_pos_detail.'.barang_id')
                ->where('pos_id', $pos['id'])
                ->where('retur', 0)
                ->get();
        
        $arr['detail_member'] = $detail_member;
        $arr['detail_transaksi'] = $pos;
        $arr['detail_transaksi']['is_cicilan'] = $is_cicilan;
        $arr['detail_pembelian'] = $detail_pembelian;
        $arr['detail_pembayaran'] = $pos_pembayaran;
        $arr['detail_cicilan'] = $is_cicilan ? $pos_cicilan : [];
        $arr['no_kuitansi'] = $no_kuitansi ? $no_kuitansi : [];
        
        return $arr;
    }

    

}
