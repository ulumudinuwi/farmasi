<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Retur_penjualan_model extends CI_Model {

	protected $table_def = "t_retur";
	protected $table_def_pos = "t_pos";

	public function __construct() {
        parent::__construct();
        $this->load->helper('gudang_farmasi');

    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else  {
			$select = array(
				"{$this->table_def}.*"
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM ".$this->table_def;
		return $from;
	}

	private function _get_join() {
		$join = "";
		return $join;
	}
	
	public function get_by($sWhere = "") {
		$sql = $this->_get_select()." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere;
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}
	
	public function save() {
		if (isset($_POST['uid']) && $_POST['uid'] == '') {
			$id = $this->create();
		}
		else {
			$id = $this->update();
		}
		return $id;
	}

	public function create($obj) {
		$this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);
        unset($data['detail_list']);

        $this->db->set('uid', 'UUID()', FALSE);
        $data['created_by'] = $this->session->userdata('auth_user');
        $data['created_at'] = date('Y-m-d H:i:s');

        $this->db->insert($this->table_def_pos, $data);
        // get insert_id()
        $id =  $this->db->insert_id();

        $result = $this->db->where('id', $id)->get($this->table_def_pos)->row();

        $this->_save_Detail($result, $obj->detail_list);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
	}

	public function update($obj) {
		$this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);
        unset($data['detail_list']);

        $data['updated_by'] = $this->session->userdata('auth_user');
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->db->where('id', $obj->id);
        $this->db->update($this->table_def_pos, $data);

        $result = $this->db->where('id', $obj->id)->get($this->table_def_pos)->row();

        $this->_save_Detail($result, $obj->detail_list);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
	}

	public function update_status($uid, $status) {
		$this->db->trans_start();

		$data = array();
		$data['updated_by'] = $this->session->userdata('auth_user');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['status'] = $status;

		$this->db->where('uid', $uid);
		$this->db->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}
		
	private function _save_Detail($detail, $detail_list) {
		foreach ($detail_list as $oDetail) {
			$data = get_object_vars($oDetail);
			$data['id'] = $detail->id;
			unset($data['id']);
			unset($data['uid']);
			unset($data['data_mode']);
			unset($data['tanggal_transaksi']);
			unset($data['no_invoice']);
			unset($data['old_qty']);
			unset($data['old_barang_id']);
			unset($data['new_barang_id']);
			unset($data['data_barang']);

			switch ($oDetail->data_mode) {
				case 1:
					//procstockkeluar
        			$config_tipe = $this->config->item('tipe_kartu_stock_pengeluaran');
					$no_invoice = $oDetail->no_invoice;
					
					$obj = new stdClass();
	                $obj->tanggal = $oDetail->tanggal_transaksi;
	                $obj->kode = $no_invoice;
	                $obj->id = $oDetail->pos_id;
	                $obj->keterangan = 'Pengeluaran Retur Barang NO.INVOICE '.$no_invoice;

	                $detail = new stdClass();
	                $detail->barang_id = $oDetail->barang_id;
	                $detail->qty = $oDetail->qty;
	                $detail->id = $oDetail->pos_id;
	                 
	                $json_response = procStockKeluar($obj,$detail,$config_tipe);
	                $data['created_at'] = date('Y-m-d H:i:s');
					$data['created_by'] = $this->session->userdata('auth_user');
					$data['json_response'] = json_encode($json_response);
					$this->db->set('uid', 'UUID()', FALSE);
					$this->db->insert('t_pos_detail', $data);
					
					//save Retur
					$dRetur['created_at'] = date('Y-m-d H:i:s');
					$dRetur['created_by'] = $this->session->userdata('auth_user');
					$dRetur['pos_id'] = $oDetail->pos_id;
					$dRetur['barang_id'] = '';
					$dRetur['old_qty'] = '';
					$dRetur['new_barang_id'] = $oDetail->barang_id;
					$dRetur['qty'] = $oDetail->qty;
					$dRetur['status'] = 1;
					$this->db->set('uid', 'UUID()', FALSE);
					$this->db->insert('t_retur', $dRetur);
					break;
				case 2:
					$new_qty = intval($oDetail->old_qty) - intval($oDetail->qty);
					if($new_qty > 0) {
						//procstockMasuk
	        			$config_tipe =  $this->config->item('tipe_kartu_stock_penerimaan');
						$no_invoice = $oDetail->no_invoice;
		                
						$obj = new stdClass();
		                $obj->tanggal = $oDetail->tanggal_transaksi;
		                $obj->kode = $no_invoice;
		                $obj->id = $oDetail->pos_id;
		                $obj->keterangan = 'Penerimaan Quantity Retur Barang NO.INVOICE '.$no_invoice;

		                $detail = new stdClass();
		                $detail->barang_id = $oDetail->old_barang_id;
		                $detail->qty = intval($oDetail->old_qty);
		                $detail->id = $oDetail->pos_id;
		                $detail->data_barang = $oDetail->data_barang;
		                 
		                procStockMasukByTransaksi($obj,$detail,$config_tipe);

		                $config_tipe = $this->config->item('tipe_kartu_stock_pengeluaran');
						$no_invoice = $oDetail->no_invoice;
						
						$obj = new stdClass();
		                $obj->tanggal = $oDetail->tanggal_transaksi;
		                $obj->kode = $no_invoice;
		                $obj->id = $oDetail->pos_id;
		                $obj->keterangan = 'Pengeluaran Quantity Retur Barang NO.INVOICE '.$no_invoice;

		                $detail = new stdClass();
		                $detail->barang_id = $oDetail->old_barang_id;
		                $detail->qty = $oDetail->qty;
		                $detail->id = $oDetail->pos_id;
		                 
		                $json_response = procStockKeluar($obj,$detail,$config_tipe);

						$data['json_response'] = json_encode($json_response);

						if($oDetail->new_barang_id != 0){
							$config_tipe = $this->config->item('tipe_kartu_stock_pengeluaran');
							$no_invoice = $oDetail->no_invoice;
							
							$obj = new stdClass();
			                $obj->tanggal = $oDetail->tanggal_transaksi;
			                $obj->kode = $no_invoice;
			                $obj->id = $oDetail->pos_id;
			                $obj->keterangan = 'Pengeluaran Quantity Retur Barang NO.INVOICE '.$no_invoice;

			                $detail = new stdClass();
			                $detail->barang_id = $oDetail->barang_id;
			                $detail->qty = $oDetail->qty;
			                $detail->id = $oDetail->pos_id;
			                 
			                $json_response = procStockKeluar($obj,$detail,$config_tipe);

							$data['json_response'] = json_encode($json_response);
						} 																		
						$data['updated_at'] = date('Y-m-d H:i:s');
						$data['updated_by'] = $this->session->userdata('auth_user');
						$this->db->where('id', $oDetail->id);
						$this->db->update('t_pos_detail', $data);

						//save Retur
						$dRetur['created_at'] = date('Y-m-d H:i:s');
						$dRetur['created_by'] = $this->session->userdata('auth_user');
						$dRetur['pos_id'] = $oDetail->pos_id;
						$dRetur['barang_id'] = $oDetail->old_barang_id;
						$dRetur['old_qty'] = $oDetail->old_qty;
						$dRetur['new_barang_id'] = $oDetail->barang_id;
						$dRetur['qty'] = $oDetail->qty;
						$dRetur['status'] = 2;
						$this->db->set('uid', 'UUID()', FALSE);
						$this->db->insert('t_retur', $dRetur);	
					} else {
						//procstockMasuk
	        			$config_tipe =  $this->config->item('tipe_kartu_stock_penerimaan');
						$no_invoice = $oDetail->no_invoice;
		                
						$obj = new stdClass();
		                $obj->tanggal = $oDetail->tanggal_transaksi;
		                $obj->kode = $no_invoice;
		                $obj->id = $oDetail->pos_id;
		                $obj->keterangan = 'Penerimaan Quantity Retur Barang NO.INVOICE '.$no_invoice;

		                $detail = new stdClass();
		                $detail->barang_id = $oDetail->new_barang_id != 0 ? $oDetail->old_barang_id : $oDetail->barang_id;
		                $detail->qty = intval($oDetail->old_qty);
		                $detail->id = $oDetail->pos_id;
		                $detail->data_barang = $oDetail->data_barang;
		                 
		                procStockMasukByTransaksi($obj,$detail,$config_tipe);

						$config_tipe = $this->config->item('tipe_kartu_stock_pengeluaran');
						$no_invoice = $oDetail->no_invoice;
						
						$obj = new stdClass();
		                $obj->tanggal = $oDetail->tanggal_transaksi;
		                $obj->kode = $no_invoice;
		                $obj->id = $oDetail->pos_id;
		                $obj->keterangan = 'Pengeluaran Quantity Retur Barang NO.INVOICE '.$no_invoice;

		                $detail = new stdClass();
		                $detail->barang_id = $oDetail->barang_id;
		                $detail->qty = $oDetail->qty;
		                $detail->id = $oDetail->pos_id;
		                 
		                $json_response = procStockKeluar($obj,$detail,$config_tipe);

						$data['json_response'] = json_encode($json_response);
						$data['updated_at'] = date('Y-m-d H:i:s');
						$data['updated_by'] = $this->session->userdata('auth_user');
						$this->db->where('id', $oDetail->id);
						$this->db->update('t_pos_detail', $data);	

						//save Retur
						$dRetur['created_at'] = date('Y-m-d H:i:s');
						$dRetur['created_by'] = $this->session->userdata('auth_user');
						$dRetur['pos_id'] = $oDetail->pos_id;
						$dRetur['barang_id'] = $oDetail->old_barang_id;
						$dRetur['old_qty'] = $oDetail->old_qty;
						$dRetur['new_barang_id'] = $oDetail->barang_id;
						$dRetur['qty'] = $oDetail->qty;
						$dRetur['status'] = 3;
						$this->db->set('uid', 'UUID()', FALSE);
						$this->db->insert('t_retur', $dRetur);			
					}
					break;
				case 3:
					//procstockMasuk
        			$config_tipe =  $this->config->item('tipe_kartu_stock_penerimaan');
					$no_invoice = $oDetail->no_invoice;
					
					$obj = new stdClass();
	                $obj->tanggal = $oDetail->tanggal_transaksi;
	                $obj->kode = $no_invoice;
	                $obj->id = $oDetail->pos_id;
	                $obj->keterangan = 'Penerimaan Retur Barang NO.INVOICE '.$no_invoice;

	                $detail = new stdClass();
	                $detail->barang_id = $oDetail->barang_id;
	                $detail->qty = $oDetail->qty;
	                $detail->id = $oDetail->pos_id;
	                $detail->data_barang = $oDetail->data_barang;
	                 
	                procStockMasukByTransaksi($obj,$detail,$config_tipe);

					$data['updated_at'] = date('Y-m-d H:i:s');
					$data['updated_by'] = $this->session->userdata('auth_user');
					$this->db->where('id', $oDetail->id);
					$this->db->update('t_pos_detail', $data);

						//save Retur
						$dRetur['created_at'] = date('Y-m-d H:i:s');
						$dRetur['created_by'] = $this->session->userdata('auth_user');
						$dRetur['pos_id'] = $oDetail->pos_id;
						$dRetur['barang_id'] = $oDetail->barang_id;
						$dRetur['old_qty'] = $oDetail->qty;
						$dRetur['new_barang_id'] = '';
						$dRetur['qty'] = '';
						$dRetur['status'] = 4;
						$this->db->set('uid', 'UUID()', FALSE);
						$this->db->insert('t_retur', $dRetur);	
					break;
			}
		}
	}
	
}

?>