<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Model_Eloquent;
class Target_sales_model extends Model_Eloquent
{
    protected $table = 't_target_sales';
    public $timestamps = true;
}
