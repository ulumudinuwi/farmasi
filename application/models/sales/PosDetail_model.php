<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PosDetail_model extends CI_Model {

	protected $table_def = "t_pos_detail";

	public function __construct() {
        parent::__construct();
    }
	
	private function _get_select() {
		$select = array(
			"{$this->table_def}.*"
		);
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM ".$this->table_def;
		return $from;
	}

	private function _get_join() {
		$join = "";
		return $join;
	}
	
	public function get_by($sWhere = "") {
		$sql = $this->_get_select()." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere;
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select();
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere;
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}
	
	public function save($oData) {
		if (isset($oData->uid) && $oData->uid == '') {
			$id = $this->create($oData);
		}
		else {
			$id = $this->update($oData);
		}
		return $id;
	}
	
	public function create($oData) {
		$data = $this->_getRequest($oData);
		$this->db->set('uid', 'UUID()', FALSE);
		$this->db->insert($this->table_def, $data);
		$id =  $this->db->insert_id();
		return (int) $oData->id !== 0 ? $id : FALSE;
	}

	public function update($oData) {
		$data = $this->_getRequest($oData, FALSE);
		$this->db->where('uid', $oData->uid);
		$this->db->update($this->table_def, $data);
		return $oData->uid;
	}
	
	private function _getRequest($oData, $add = true) {
		$data = array(
			'kode'			=> $oData->kode,
			'nama'			=> $oData->nama,
			'ruang_id'		=> (int) $oData->ruang_id !== 0 ? $oData->ruang_id : null,
			'kelas_id'		=> (int) $oData->kelas_id !== 0 ? $oData->kelas_id : null,
			'status_bed'	=> $oData->status_bed,
        );
		if ($add) {
			$data['created_by']	= $this->session->userdata('auth_user');
			$data['created_at']	= date('Y-m-d H:i:s');
        }
		else {
			$data['update_by']	= $this->session->userdata('auth_user');
			$data['update_at']	= date('Y-m-d H:i:s');
		}
		return $data;
	}
	
	public function delete_detail($uid) {
		$this->db->where('uid', $uid);
		$this->db->delete($this->table_def);
	}
	
}

?>