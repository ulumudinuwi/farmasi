<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Add_tocart_model extends CI_Model {

    protected $table_def = 't_pos_pesanan';
    protected $table_def_barang = 'm_barang';
    protected $table_def_member = 'm_dokter_member';

    public function __construct() {
        parent::__construct();
    }

    private function _get_select($customSelect = "") {
        if($customSelect != "") {
            $select = $customSelect;
        } else {
            $select = array(
                "{$this->table_def}.*",
            );
        }

        return "SELECT ".implode(",", $select)." ";
    }

    private function _get_from() {
        $from = "FROM ".$this->table_def." ";
        return $from;
    }

    private function _get_join() {
        $join = "LEFT JOIN {$this->table_def_barang} ON {$this->table_def}.product_uid = {$this->table_def_barang}.uid ";
        $join .= "LEFT JOIN {$this->table_def_member} ON {$this->table_def}.member_id = {$this->table_def_member}.id ";

        return $join;
    }

    public function get_by($sWhere = "", $customSelect = "") {
        $sql = $this->_get_select($customSelect)." ";
        $sql .= $this->_get_from()." ";
        $sql .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql .= " ".$sWhere;
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
        $data = array();
        $sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
        $sql_count .= $this->_get_from()." ";
        $sql_count .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql_count .= " ".$sWhere." ";
        }
        $query = $this->db->query($sql_count);
        if ($query->num_rows() == 0) {
            $data['total_rows'] = 0;
        } else {
            $row = $query->row();
            $data['total_rows'] = (int) $row->numrows;
        }

        $select = $this->_get_select($customSelect);
        $from = $this->_get_from();
        $join = $this->_get_join();
        $sql = $select." ".$from." ".$join." ";
        if (!empty($sWhere)) {
            $sql .= $sWhere." ";
        }
        if (!empty($sOrder)) {
            $sql .= $sOrder." ";
        }
        if ($iLimit > 0) {
            $sql .= "LIMIT ".$iOffset.", ".$iLimit;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result();
        } else {
            $data['data'] = array();
        }
        return $data;
    }

    public function create($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);

        $this->db->set('uid', 'UUID()', FALSE);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['status'] = '1'; // Add Cart
        $data['update_at'] = date('Y-m-d H:i:s');

        $this->db->insert($this->table_def, $data);
        // get insert_id()
        $id =  $this->db->insert_id();

        $result = $this->db->where('id', $id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);

        $data['update_by'] = $this->session->userdata('auth_user');
        $data['update_at'] = date('Y-m-d H:i:s');

        $this->db->where('id', $obj->id);
        $this->db->update($this->table_def, $data);

        $result = $this->db->where('id', $obj->id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function delete($uid) {
        $this->db->trans_start();

        $this->db->where('uid', $uid);
        $this->db->delete($this->table_def);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $uid;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function restore($uid) {
        $this->db->trans_start();

        $data = array();
        $data['update_by'] = $this->session->userdata('auth_user');
        $data['update_at'] = date('Y-m-d H:i:s');
        $data['deleted'] = 0;

        $this->db->where('uid', $uid);
        $this->db->update($this->table_def, $data);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $uid;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
}