<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Penerimaan_model extends CI_Model {
	protected $table_def = "t_gudang_farmasi_penerimaan";
	protected $table_def_detail = "t_gudang_farmasi_penerimaan_detail";
	protected $table_def_po = "t_gudang_farmasi_po";
	protected $table_def_po_detail = "t_gudang_farmasi_po_detail";
	protected $table_def_barang = "m_barang";
	protected $table_def_pabrik = "m_pabrik";
	protected $table_def_kurs = "m_kurs";
	protected $table_def_user = "auth_users";

	public function __construct() {
        parent::__construct();
        $this->load->helper('gudang_farmasi');
        $this->load->helper('gudang');
        $this->load->model('gudang_farmasi/Po_detail_model', 'po_detail');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"{$this->table_def_po}.uid uid_po",
				"{$this->table_def_po}.kode kode_po",
				"kurs.country_id kurs_code",
				"kurs.currency_name kurs_name",
				"CONCAT(pb.kode, ' - ', pb.nama) pabrik",
				"CONCAT(diproses_by.first_name, ' ',diproses_by.last_name) diproses_by",
				//"CONCAT(diubah_by.first_name, ' ',diubah_by.last_name) diubah_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_po} ON {$this->table_def}.po_id = {$this->table_def_po}.id ";
		$join .= "LEFT JOIN {$this->table_def_pabrik} pb ON {$this->table_def}.pabrik_id = pb.id ";
		$join .= "LEFT JOIN {$this->table_def_kurs} kurs ON {$this->table_def}.kurs_id = kurs.id ";
		$join .= "LEFT JOIN {$this->table_def_user} diproses_by ON {$this->table_def}.created_by = diproses_by.id ";
		//$join .= "LEFT JOIN {$this->table_def_user} diubah_by ON {$this->table_def}.update_by = diubah_by.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$obj->kode = generateKode("GFT", $this->table_def);

		$data = get_object_vars($obj);
		unset($data['id']);
		unset($data['uid']);
		unset($data['details']);
		$this->db->set('uid', 'UUID()', FALSE);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();
		
		$this->_saveDetail($obj);

		$this->_updateStatusPO($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	private function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
        	$data['penerimaan_id'] = $obj->id;
        	$data['data_barang'] = json_encode($detail->data_barang);
			$data['created_by'] = $this->session->userdata('auth_user');
    		$data['created_at'] = date('Y-m-d H:i:s');
    		$data['update_by'] = $this->session->userdata('auth_user');
        	$data['update_at'] = date('Y-m-d H:i:s');
			$this->db->insert($this->table_def_detail, $data);
			$detail->id =  $this->db->insert_id();

			$this->db->set('harga_diterima', $detail->harga, FALSE)
					->set('harga_diterima_kurs', $detail->harga_kurs, FALSE)
					->set('diterima', 'diterima + '.$detail->qty, FALSE)
					->where('id', $detail->po_detail_id)
					->update($this->table_def_po_detail);

			$sWhere = "";
		    $aWheres = array();
		    $aWheres[] = "{$this->table_def_po_detail}.po_id = {$obj->po_id}";
		    $aWheres[] = "{$this->table_def_po_detail}.barang_id = {$detail->barang_id}";
		    $aWheres[] = "{$this->table_def_po_detail}.qty > {$this->table_def_po_detail}.diterima";
		    if (count($aWheres) > 0) {
		      $sWhere = implode(' AND ', $aWheres);
		    }
		    if (!empty($sWhere)) {
		      $sWhere = "WHERE ".$sWhere;
		    }
			$isExist = $this->po_detail->get_all(1, 0, $sWhere)['total_rows'];
			$detail->dipesan = 0;
			if($isExist > 0) $detail->dipesan = 1;

			$this->db->set('harga_pembelian', $detail->harga_kurs, FALSE)
					->where('id', $detail->barang_id)
					->update($this->table_def_barang);

			// KARTU STOCK
			$detail->qty = ($detail->qty * $detail->isi_satuan);
			$detail->harga = ($detail->harga_kurs / $detail->isi_satuan);
			procStockPenerimaan($obj, $detail, $this->config->item('tipe_kartu_stock_penerimaan'));
        }		
	}

	private function _updateStatusPO($obj) {
		$sWhere = "";
	    $aWheres = array();
	    $aWheres[] = "{$this->table_def_po_detail}.po_id = {$obj->po_id}";
	    $aWheres[] = "{$this->table_def_po_detail}.qty > {$this->table_def_po_detail}.diterima";
	    if (count($aWheres) > 0) {
	      $sWhere = implode(' AND ', $aWheres);
	    }
	    if (!empty($sWhere)) {
	      $sWhere = "WHERE ".$sWhere;
	    }
		$isExist = $this->po_detail->get_all(1, 0, $sWhere)['total_rows'];

		$data_po = array();
		$status_po = $this->config->item('status_po_closed');
	    $status_penerimaan = $this->config->item('status_closed');
		if($isExist > 0) {
			$status_po = $this->config->item('status_po_partial_received');
	    	$status_penerimaan = $this->config->item('status_partial');
		}

		$this->db->set('status', $status_penerimaan)
				 ->where('id', $obj->id)
				 ->update($this->table_def);

		$data_po['status'] = $status_po;
		$data_po['update_at'] = date('Y-m-d H:i:s');
		$data_po['update_by'] = $this->auth->userid();
		$this->db->where('id', $obj->po_id)
				 ->update($this->table_def_po, $data_po);
	}
}

?>