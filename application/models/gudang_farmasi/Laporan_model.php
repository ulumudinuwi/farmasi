<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_model extends CI_model {

    public function __construct()
  	{
    	parent::__construct();
  	}

  	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"*",
			);
		}
		return 'SELECT '.implode(', ', $select);
	}

  	public function data_laporan($browse = "001", $iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();

		$view = "view_gudang_farmasi_laporan_${browse}";
		$sqlData = $this->_get_select($customSelect)." FROM {$view}";
		$sqlCount = "SELECT COUNT(id) numrows FROM {$view}";

		if (!empty($sWhere)) {
			$sqlCount .= " ".$sWhere." ";
			$sqlData .= " ".$sWhere." ";
		}
		if (!empty($sOrder)) $sqlData .= " ".$sOrder." ";

		$query = $this->db->query($sqlCount);
		if ($query->num_rows() > 0) {
			switch ($browse) {
				default:
					$data['total_rows'] = (int) $query->row()->numrows;
					break;
			}
		} else $data['total_rows'] = 0;

		if ($iLimit > 0) $sqlData .= " LIMIT ".$iOffset.", ".$iLimit." ";
		
		$query = $this->db->query($sqlData);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		} else $data['data'] = array();

		return $data;
	}
}