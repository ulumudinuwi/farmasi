<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stock_model extends CI_Model {

	protected $table_def = "t_gudang_farmasi_stock";
	protected $table_def_pos_checkout = "t_pos_checkout";
	protected $table_def_pos_checkout_detail = "t_pos_pesanan";

	protected $table_def_barang = 'm_barang';
	protected $table_def_satuan = "m_satuan";
	protected $table_def_pabrik = "m_pabrik";
	protected $table_def_jenis = 'm_jenisbarang';
	protected $table_def_kategori = "m_kategoribarang";
	//protected $table_def_kelompok = "m_kelompokbarang";

	public function __construct() {
        parent::__construct();
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"pabrik.nama pabrik",
				//"kelompok.nama kelompok",
				"CONCAT(satuan_pembelian.nama, ' (', satuan_pembelian.singkatan, ')') satuan_pembelian",
				"CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan_penggunaan",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM ".$this->table_def;
		return $from;
	}

	private function _get_join() {
		$join = "INNER JOIN {$this->table_def_barang} ON {$this->table_def}.barang_id = {$this->table_def_barang}.id ";
		$join .= "LEFT JOIN {$this->table_def_pabrik} pabrik ON {$this->table_def_barang}.pabrik_id = pabrik.id ";
		//$join .= "LEFT JOIN {$this->table_def_kelompok} kelompok ON {$this->table_def_barang}.kelompok_id = kelompok.id ";
		$join .= "LEFT JOIN {$this->table_def_jenis} jenis ON {$this->table_def_barang}.jenis_id = jenis.id ";
		$join .= "LEFT JOIN {$this->table_def_kategori} kategori ON {$this->table_def_barang}.kategori_id = kategori.id ";
		$join .= "LEFT JOIN {$this->table_def_satuan} satuan_pembelian ON {$this->table_def_barang}.satuan_pembelian_id = satuan_pembelian.id ";
		$join .= "LEFT JOIN {$this->table_def_satuan} satuan_penggunaan ON {$this->table_def_barang}.satuan_penggunaan_id = satuan_penggunaan.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function checkAvailableStockByBarangUid($barang_uid = "")
	{
		$stock = 0;
		$oBarang = $this->get_by("WHERE {$this->table_def_barang}.uid = '{$barang_uid}'", array(
        	"{$this->table_def_barang}.id",
        	"{$this->table_def_barang}.uid",
        	"{$this->table_def_barang}.kode",
        	"{$this->table_def}.qty stock"
        ));
		if($oBarang) $oBarang->stock = $this->getTotalCheckoutActive($oBarang);
		return $oBarang->stock;
	}

	// GET TOTAL ACTIVE CHECKOUT BARANG
	public function getTotalCheckoutActive($oBarang) {
		$cnCheckout = $this->db->query("SELECT SUM(a.qty) qty
									FROM 
									{$this->table_def_pos_checkout_detail} a
									INNER JOIN {$this->table_def_pos_checkout} b ON a.`checkout_id` = b.`id`
									WHERE 
									a.product_uid = '{$oBarang->uid}'
									AND b.status = 1")->row();
		$currStock = $oBarang->stock - ($cnCheckout->qty ? : 0);
		if($currStock < 0) $currStock = 0;
		return $currStock;
	}
}

?>