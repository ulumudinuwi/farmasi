<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Data Mode Model
 * 
 * @package App
 * @category Model
 * @author Dadang Dana Suryana
 */
class Data_mode_model extends CI_Model {

	// DATA MODE
	const DATA_MODE_VIEW	= 0;
	const DATA_MODE_ADD		= 1;
    const DATA_MODE_EDIT	= 2;
    const DATA_MODE_DELETE	= 3;

	public function __construct()
	{
		parent::__construct();
	}
	
}
