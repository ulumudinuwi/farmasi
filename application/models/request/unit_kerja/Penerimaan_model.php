<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Penerimaan_model extends CI_Model {
	protected $table_def = "t_request_penerimaan_unit";
  	protected $table_def_detail = "t_request_penerimaan_unit_detail";
	protected $table_def_permintaan = "t_request_permintaan_unit";
	protected $table_def_pengeluaran = "t_logistik_pengeluaran";
	protected $table_def_unitkerja = "m_unitkerja";
	protected $table_def_user = "auth_users";

	public function __construct() {
        parent::__construct();
        $this->load->helper('gudang');
        $this->load->model('request/unit_kerja/Penerimaan_detail_model', 'main_detail');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"{$this->table_def_permintaan}.kode kode_permintaan",
				"{$this->table_def_permintaan}.keterangan keterangan_permintaan",
				"{$this->table_def_pengeluaran}.kode kode_pengeluaran",
				"{$this->table_def_permintaan}.status status_permintaan",
				"{$this->table_def_pengeluaran}.keterangan keterangan_pengeluaran",
				"uk.nama unit_kerja",
				"CONCAT(diproses_by.first_name, ' ',diproses_by.last_name) diproses_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_pengeluaran} ON {$this->table_def}.pengeluaran_id = {$this->table_def_pengeluaran}.id ";
		$join .= "LEFT JOIN {$this->table_def_permintaan} ON {$this->table_def_pengeluaran}.permintaan_id = {$this->table_def_permintaan}.id ";
		$join .= "LEFT JOIN {$this->table_def_unitkerja} uk ON {$this->table_def}.unitkerja_id = uk.id ";
		$join .= "LEFT JOIN {$this->table_def_user} diproses_by ON {$this->table_def}.created_by = diproses_by.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['kode']);
		unset($data['permintaan_id']);
		unset($data['details']);
		$this->db->set('uid', 'UUID()', FALSE);
		$data['kode'] = generateKode("TFG", $this->table_def);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();

		$this->_saveDetail($obj);
		$this->_updateStatus($obj);

		$this->db->set('status', $this->config->item('status_done'))
				->where('id', $obj->pengeluaran_id)
				->update($this->table_def_pengeluaran);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			$data['update_by'] = $this->session->userdata('auth_user');
        	$data['update_at'] = date('Y-m-d H:i:s');
            switch ($detail->data_mode) {
                case Data_mode_model::DATA_MODE_ADD:
                	$data['penerimaan_id'] = $obj->id;
					$data['created_by'] = $this->session->userdata('auth_user');
	        		$data['created_at'] = date('Y-m-d H:i:s');
					$this->db->insert($this->table_def_detail, $data);
                    break;
                case Data_mode_model::DATA_MODE_EDIT:
                    $this->db->where('id', $detail->id)
                    		->update($this->table_def_detail, $data);
                    break;
                case Data_mode_model::DATA_MODE_DELETE:
                    $this->db->where('id', $detail->id)
                    		->delete($this->table_def_detail); 
                    break;
            }
        }		
	}

	private function _updateStatus($obj) {
		$sWhere = "";
	    $aWheres = array();
	    $aWheres[] = "{$this->table_def_detail}.penerimaan_id = {$obj->id}";
	    $aWheres[] = "{$this->table_def_detail}.sisa > 0";
	    if (count($aWheres) > 0) {
	      $sWhere = implode(' AND ', $aWheres);
	    }
	    if (!empty($sWhere)) {
	      $sWhere = "WHERE ".$sWhere;
	    }
		$isExist = $this->main_detail->get_all(1, 0, $sWhere)['total_rows'];

		$status = $this->config->item('status_closed');
		if($isExist > 0) $status = $this->config->item('status_partial');

		$this->db->set('status', $status)
				 ->where('id', $obj->id)
				 ->update($this->table_def);
	}
}

?>