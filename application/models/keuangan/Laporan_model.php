<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_model extends CI_Model {
    protected $table_def = "t_buku_besar";
    protected $table_def_perkiraan = "m_perkiraan";
    protected $table_def_user = "auth_users";
    
    public function __construct() {
        parent::__construct();
    }
    
    private function _get_select($customSelect = "") {
        if($customSelect != "") {
            $select = $customSelect;
        } else {
            $select = array(
                "SUM(DISTINCT {$this->table_def}.total) as total",
                "{$this->table_def}.tipe",
                "{$this->table_def_perkiraan}.kode",
                "{$this->table_def_perkiraan}.nama",
                "{$this->table_def_perkiraan}.klasifikasi",
            );
        }
        return 'SELECT '.implode(', ', $select).' ';
    }
    
    private function _get_from() {
        $from = "FROM {$this->table_def}";
        return $from;
    }

    private function _get_join($customSelect = "") {
        $join = array(
            "LEFT JOIN {$this->table_def_perkiraan} ON {$this->table_def}.perkiraan_id = {$this->table_def_perkiraan}.id",
            "LEFT JOIN {$this->table_def_user} ON {$this->table_def}.update_by = {$this->table_def_user}.id",
        );

        return implode(' ', $join);
    }
    
    public function get_by($sWhere = "", $customSelect = "") {
        $sql = $this->_get_select($customSelect)." ";
        $sql .= $this->_get_from()." ";
        $sql .= $this->_get_join($customSelect);
        if (!empty($sWhere)) {
            $sql .= " ".$sWhere;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
        
        $data = array();
        $sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
        $sql_count .= $this->_get_from()." ";
        $sql_count .= $this->_get_join($customSelect);
        if (!empty($sWhere)) {
            $sql_count .= " ".$sWhere." ";
        }
        $query = $this->db->query($sql_count);
        if ($query->num_rows() == 0) {
            $data['total_rows'] = 0;
        }
        else {
            $row = $query->row();
            $data['total_rows'] = (int) $row->numrows;
        }
        
        $select = $this->_get_select($customSelect);
        $from = $this->_get_from();
        $join = $this->_get_join($customSelect);
        $sql = $select." ".$from." ".$join." ";
        if (!empty($sWhere)) {
            $sql .= $sWhere." ";
        }
        if (!empty($sOrder)) {
            $sql .= $sOrder." ";
        }
        if ($iLimit > 0) {
            $sql .= "LIMIT ".$iOffset.", ".$iLimit;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result();
        }
        else {
            $data['data'] = array();
        }
        return $data;
    }
}

?>