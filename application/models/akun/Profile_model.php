<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends CI_Model {

    protected $table_def = 'm_dokter_member';
    protected $table_def_auth = 'auth_users';
    private $ci;

    public function __construct() {
        parent::__construct();

        $this->ci = & get_instance();
        $this->ci->load->library('PasswordHash', array('iteration_count_log2' => 8, 'portable_hashes' => FALSE));
    }

    private function _get_select() {
        $select = array(
            $this->table_def.".*",
        );

        return "SELECT ".implode(",", $select)." ";
    }

    private function _get_from() {
        $from = "FROM ".$this->table_def." ";
        return $from;
    }

    private function _get_join() {
        $join = "";

        return $join;
    }

    public function get_by($sWhere = "") {
        $sql = $this->_get_select()." ";
        $sql .= $this->_get_from()." ";
        $sql .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql .= " ".$sWhere;
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
        $data = array();
        $sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
        $sql_count .= $this->_get_from()." ";
        $sql_count .= $this->_get_join();
        if (!empty($sWhere)) {
            $sql_count .= " ".$sWhere." ";
        }
        $query = $this->db->query($sql_count);
        if ($query->num_rows() == 0) {
            $data['total_rows'] = 0;
        } else {
            $row = $query->row();
            $data['total_rows'] = (int) $row->numrows;
        }

        $select = $this->_get_select();
        $from = $this->_get_from();
        $join = $this->_get_join();
        $sql = $select." ".$from." ".$join." ";
        if (!empty($sWhere)) {
            $sql .= $sWhere." ";
        }
        if (!empty($sOrder)) {
            $sql .= $sOrder." ";
        }
        if ($iLimit > 0) {
            $sql .= "LIMIT ".$iOffset.", ".$iLimit;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result();
        } else {
            $data['data'] = array();
        }
        return $data;
    }

    public function create($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);

        $this->db->set('uid', 'UUID()', FALSE);
		$this->db->set('no_member', "func_get_no_member()", FALSE);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->db->insert($this->table_def, $data);
        // get insert_id()
        $id =  $this->db->insert_id();

        $dataUpdate = new stdClass();
        $dataUpdate->created_by = $id;
        $dataUpdate->updated_by = $id;

        $this->db->where('id', $id);
        $this->db->update($this->table_def, $dataUpdate);

        $result = $this->db->where('id', $id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);

        $data['updated_by'] = $this->session->userdata('auth_user');
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->db->where('id', $obj->id);
        $this->db->update($this->table_def, $data);

        $result = $this->db->where('id', $obj->id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function change_profile($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);
        unset($data['image_profile']);

        if(isset($obj->image_profile))
            $data['foto_profile'] = $obj->image_profile;
        $data['updated_by'] = $obj->id;
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->db->where('id', $obj->id);
        $this->db->update($this->table_def, $data);
        $result = $this->db->where('id', $obj->id)->get($this->table_def)->row();

        $dataUser = new stdClass();
        $dataUser->first_name = $obj->nama;
        $dataUser->email = $obj->email;
        if(isset($obj->image_profile))
            $dataUser->image_profile = $obj->image_profile;
        $this->db->where('dokter_id', $obj->id);
        $this->db->update($this->table_def_auth, $dataUser);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function change_address($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);

        $data['updated_by'] = $obj->id;
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->db->where('id', $obj->id);
        $this->db->update($this->table_def, $data);
        $result = $this->db->where('id', $obj->id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function change_password($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);
        unset($data['password_baru']);
        unset($data['password_lama']);

        $data['updated_by'] = $obj->id;
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->db->where('id', $obj->id);
        $this->db->update($this->table_def, $data);
        $result = $this->db->where('id', $obj->id)->get($this->table_def)->row();
        
        $dataUser = new stdClass();
        $dataUser->password = $this->ci->passwordhash->HashPassword($obj->password_baru);
        $this->db->where('dokter_id', $obj->id);
        $this->db->update($this->table_def_auth, $dataUser);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function delete($uid) {
        $this->db->trans_start();

        $data = array();
        $data['updated_by'] = $this->session->userdata('auth_user');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['deleted_flag'] = 1;

        $this->db->where('uid', $uid);
        $this->db->update($this->table_def, $data);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $uid;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function restore($uid) {
        $this->db->trans_start();

        $data = array();
        $data['updated_by'] = $this->session->userdata('auth_user');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['deleted_flag'] = 0;

        $this->db->where('uid', $uid);
        $this->db->update($this->table_def, $data);

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $uid;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function createUser($obj) {
        $this->db->trans_start();

        $data = get_object_vars($obj);
        unset($data['id']);
        unset($data['uid']);

        $this->db->set('uid', 'UUID()', FALSE);
        
        $this->db->insert($this->table_def_auth, $data);
        // get insert_id()
        $id =  $this->db->insert_id();

        $result = $this->db->where('id', $id)->get($this->table_def)->row();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
}