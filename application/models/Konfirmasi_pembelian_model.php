<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Konfirmasi_pembelian_model extends CI_Model {
	protected $table_def = "t_pos_checkout";
	protected $table_def_pesanan = "t_pesanan";
	protected $table_def_member = "m_dokter_member";
  	protected $table_def_user = "auth_users";
  	protected $table_def_pos = "t_pos";
  	protected $table_def_pos_detail = "t_pos_detail";
  	protected $table_def_pos_pembayaran = "t_pos_pembayaran";

	public function __construct() {
        parent::__construct();
        $this->load->helper('laba_rugi_helper');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				//"CONCAT(created_by.first_name, ' ',created_by.last_name) created_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_member} dokter_member ON {$this->table_def}.member_id = dokter_member.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT {$this->table_def}.id AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $query->num_rows();
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		$this->db->set('uid', 'UUID()', FALSE);;
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def_pos, $data);
		$obj->id =  $this->db->insert_id();

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->id;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['details']);
		unset($data['uid']);
		unset($data['kode_p']);
		unset($data['jumlah']);
		unset($data['keterangan']);
		unset($data['modul']);
		unset($data['tipe']);
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');
		
		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

        $dataLabaRugi = new stdClass();
        $dataLabaRugi->transaksi_id = $obj->id;
        $dataLabaRugi->modul = $obj->modul; //JENIS_MODUL_PENGELUARAN_KAS_BANK;
        $dataLabaRugi->total = $obj->jumlah;
        $dataLabaRugi->keterangan = $obj->keterangan; //"Pengeluaran Kas Bank senilai : ".$obj->jumlah;
        $dataLabaRugi->perkiraan_id = $data['perkiraan_id'];
        $dataLabaRugi->tipe = $obj->tipe; //JENIS_KREDIT;
        //insert
        updateLabarugi($dataLabaRugi);

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function save_detail($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		$this->db->set('uid', 'UUID()', FALSE);;
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def_pos_detail, $data);
		$obj->id =  $this->db->insert_id();

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->id;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}
	public function cancel_data($uid, $desc) {
		$this->db->set('alasan_penolakan', $desc);
		$this->db->set('status', 6);
		$this->db->set('update_by', $this->session->userdata('auth_user'));
		$this->db->set('update_at', date('Y-m-d H:i:s'));
		$this->db->where('uid', $uid);
		$this->db->update($this->table_def);

		$data = $this->db->select('id')
	            ->where('uid', $uid)
	            ->get($this->table_def)
	            ->row();

		return true;
	}
}

?>