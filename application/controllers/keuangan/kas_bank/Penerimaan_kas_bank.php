<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerimaan_kas_bank extends Admin_Controller 
{
	protected $page_title = '<i class="icon-cash"></i> Penerimaan Kas Bank';
	protected $table_def = 't_kas_bank';

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
        $this->load->model('master/Perkiraan_model');
	}

	public function index()
	{
		$this->data['tabUid'] = base64_encode($this->table_def);
		$this->data['page_icons'] = '<a href="'. site_url("keuangan/kas_bank/penerimaan_kas_bank/add") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Buat Kas Bank</a>';
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_script('keuangan/penerimaan_kas_bank/script-index')
			->build('keuangan/penerimaan_kas_bank/index', $this->data);
	}

	function add($uid = 0)
	{
		$this->_updatedata($uid);
	}
	
	function _updatedata($uid = 0)
	{
		$this->data['uid'] = $uid;

		$perkiraanKasBankList = $this->Perkiraan_model->get_all(0, 0, "WHERE (m_perkiraan.parent_id != 0) ORDER BY m_perkiraan.kode ASC");
		$this->data['perkiraan_kas_bank_list'] = $perkiraanKasBankList['data'];
		
		$this->template
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
			->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
			->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
			->set_js('plugins/forms/inputs/formatter.min')
			->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
			->set_script('keuangan/penerimaan_kas_bank/script-form')
			->build('keuangan/penerimaan_kas_bank/form', $this->data);  
	}
}