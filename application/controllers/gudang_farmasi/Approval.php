<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approval extends Admin_Controller 
{
	protected $page_title = '<i class="icon-book"></i> Purchase Order (PO)';
	protected $table_def = 't_gudang_farmasi_po';
	protected $pabrik;

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('kurs');
		$this->lang->load('barang');
		$this->pabrik = $this->db->where('status', 1)
												->get('m_pabrik')->result();
	}

	public function index()
	{
		$status = $this->config->item('status_po');
		unset($status[$this->config->item('status_po_waiting_for_approval')]);
		
		$this->data['pUid'] = base64_encode($this->table_def);
		$this->data['pabrik'] = $this->pabrik;
		$this->data['sifat'] = $this->config->item('sifat_po');
		$this->data['status'] = $status;
		$this->template
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
					->set_script('gudang-farmasi/approval/script-index')
					->build('gudang-farmasi/approval/index', $this->data);
	}
}