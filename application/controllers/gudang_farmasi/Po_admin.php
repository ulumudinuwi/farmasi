<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Po_admin extends Admin_Controller 
{
	protected $page_title = '<i class="icon-book"></i> Purchase Order (PO) Admin';
	protected $table_def = 't_gudang_farmasi_po';
	protected $table_def_stock = 't_gudang_farmasi_stock';
	protected $def_uri = 'gudang_farmasi/po_admin';
	protected $pabrik;

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('kurs');
		$this->lang->load('barang');
		$this->load->model('master/Pabrik_model');
		$this->pabrik = $this->Pabrik_model->get_all(0, 0, 'WHERE m_pabrik.status = 1')['data'];
	}

	public function index()
	{
		$this->data['tab1Uid'] = base64_encode($this->table_def_stock);
		$this->data['tab2Uid'] = base64_encode($this->table_def);
		$this->data['pabrik'] = $this->pabrik;
		$this->data['sifat'] = $this->config->item('sifat_po');
		$this->data['status'] = $this->config->item('status_po');
		$this->data['page_icons'] = '<a href="'. site_url($this->def_uri."/add") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Buat Draft</a>';
		$this->template
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
					->set_script("gudang-farmasi/po/admin/script-index")
					->build("gudang-farmasi/po/admin/index", $this->data);
	}

	/**
	 * Edit
	 */
	function edit($uid)
	{
		$this->_updatedata($uid);
	}
	
	/**
	 * Tambah
	 */
	function add()
	{
		$data = $this->input->get('data') ? $this->input->get('data') : "";
		$this->_updatedata(0, $data);
	}
	
	function _updatedata($uid = 0, $data = "")
	{
		$this->data['uid'] = $uid;
		$this->data['data'] = $data;
		$this->data['pabrik'] = $this->pabrik;
		$this->data['sifat'] = $this->config->item('sifat_po');
		$this->template
					->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
					->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
					->set_js('core/libraries/jquery_ui/effects.min', TRUE)
					->set_js('plugins/forms/validation/validate.min.js', TRUE)
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
					->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
					->set_script("gudang-farmasi/po/admin/script-form")
					->build("gudang-farmasi/po/admin/form", $this->data);  
	}
}