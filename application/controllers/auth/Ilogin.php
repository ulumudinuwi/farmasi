<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ilogin extends MY_Controller {
    
    function index() {
        $data = json_decode(base64_decode($this->input->get('data')));
        
        $this->load->language('auth');
        $username = $data->username;
        $password = $data->password;
        $redirect = $this->input->get_post('redirect');
        
        // form submitted
        if ($username && $password) {
            // get user from database
            $user = $this->user_model->get_by_username($username);
            if ($user && $this->user_model->check_password($password, $user->password)) {
                
                if(ENVIRONMENT == 'development'){
                    // mark user as logged in
                    $this->auth->login($user->id, $remember);
                    
                    // Add session data
                    $this->session->set_userdata(array(
                        'lang' => $user->lang,
                        'role_id' => $user->role_id,
                        'role_name' => $user->role_name,
                        'unit_usaha_id' => $user->unit_usaha_id,
                        'unit_kerja_id' => $user->unit_kerja_id,
                        'pegawai_id' => $user->pegawai_id,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                    ));
                    
                    if ($redirect)
                        redirect($redirect);
                    else
                        redirect($this->config->item('dashboard_uri', 'template'));
                } else {
                    // cek dulu apakah ip nya cucok boo...
                    $myIP = $this->input->ip_address();;
                    $userIP = $user->ip;
                    $arrUserIP = explode(';', $userIP);
                    if((in_array($myIP, $arrUserIP)) or ($userIP == '*') ){
                        // mark user as logged in
                        $this->auth->login($user->id, $remember);
                        
                        // Add session data
                        $this->session->set_userdata(array(
                        'lang' => $user->lang,
                        'role_id' => $user->role_id,
                        'role_name' => $user->role_name,
                        'unit_usaha_id' => $user->unit_usaha_id,
                        'unit_kerja_id' => $user->unit_kerja_id,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        ));
                        
                        if ($redirect)
                            redirect($redirect);
                        else
                            redirect($this->config->item('dashboard_uri', 'template'));
                    }else{
                        $this->template->add_message('warning', 'IP anda tidak diperbolehkan');
                    }
                }
                
            } else
                $this->template->add_message('warning', lang('login_attempt_failed'));
        }
        else {
            if (($username === '') || ($password === ''))
                $this->template->add_message('warning', lang('username_or_password_empty'));
        }
        
        $data = array();
        if ($username)
            $data['username'] = $username;
        
        // show login form
        $this->load->helper('form');
        $this->template->set_layout('clean')
        ->build('auth/login', $data);
    }
    
}