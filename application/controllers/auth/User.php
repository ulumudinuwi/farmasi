<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* User management controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class User extends Admin_Controller
{
    protected $page_title = '<i class="fa fa-user"></i>User';
    
    /**
    * User form definition.
    *
    * @var array
    */
    protected $user_form = array(
        'first_name' => array(
            'label' => 'Nama',
            'rules' => 'trim|max_length[50]|required',
            'helper' => 'form_inputlabel'
        ),
        'last_name' => array(
            'label' => 'lang:last_name',
            'rules' => 'trim|max_length[50]',
            'helper' => 'form_inputlabel'
        ),
        'id' => array(
            'helper' => 'form_hidden'
        ),
        // 'pegawai_id' => array(
        //     'helper' => 'form_hidden'
        // ),
        'username' => array(
            'label' => 'lang:username',
            'rules' => 'trim|required|max_length[255]|callback_unique_username',
            'helper' => 'form_inputlabel'
        ),
        'email' => array(
            'label' => 'lang:email',
            'rules' => 'trim|required|max_length[255]|valid_email|callback_unique_email',
            'helper' => 'form_emaillabel'
        ),
        'password' => array(
            'label' => 'lang:password',
            'rules' => 'trim|required|matches[confirm-password]',
            'helper' => 'form_passwordlabel',
            'value' => ''
        ),
        'confirm-password' => array(
            'label' => 'lang:confirm_password',
            'rules' => 'trim|required',
            'helper' => 'form_passwordlabel',
            'value' => ''
        ),
        'ip' => array(
            'label' => 'IP Address',
            'rules' => 'trim|max_length[16]',
            'helper' => 'form_inputlabel'
        ),
        'unitkerja_id' => array(
            'label' => 'Unit Kerja',
            'rules' => 'trim',
            'helper' => 'form_hidden',
            'extra' => array(
                'class' => 'select2'
            )
        ),
        'farmasi_unit_id' => array(
            'label' => 'Farmasi Unit',
            'rules' => '',
            'helper' => 'form_multiselectlabel',
            'extra' => array(
                'class' => 'select2'
            )
        ),
        'satelite_unit_id' => array(
            'label' => 'Satelite Unit',
            'rules' => '',
            'helper' => 'form_multiselectlabel',
            'extra' => array(
                'class' => 'select2'
            )
        ),
        'roles_id' => array(
            'label' => 'lang:Roles',
            'rules' => '',
            'helper' => 'form_multiselectlabel',
            'extra' => array(
                'class' => 'select2',
                // 'multiple' => 'multiple',
            )
        ),
        'role_id' => array(
            'label' => 'lang:Role',
            'rules' => 'trim',
            'helper' => 'form_dropdownlabel',
            'extra' => array(
                'class' => 'select2'
            )
        ),
        'bpom' => array(
            'label' => 'lang:BPOM',
            'rules' => 'trim',
            'helper' => 'form_dropdownlabel',
            'extra' => array(
                'class' => 'select2'
            )
        ),
        'lang' => array(
            'label' => 'lang:language',
            'rules' => 'trim',
            'helper' => 'form_dropdownlabel',
            'extra' => array(
                'class' => 'select2'
            )
        )
    );
    
    /**
    * Redirect to index if cancel-button clicked.
    */
    function __construct()
    {
        parent::__construct();
        
        if ($this->input->post('cancel-button'))
            redirect ('auth/user/index');
        
        $this->load->model('master/Unit_kerja_model');
        $this->load->model('master/Farmasi_unit_model');
        $this->load->model('master/Satelite_unit_model');
        $this->load->language('auth');
    }
    
    /**
    * Display User list.
    */
    function index()
    {
        $this->data['page_icons'] = '<a href="' . site_url('auth/user/add') . '" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
        $this->template
        ->set_js('plugins/tables/datatables/datatables.min', FALSE)
        ->build('auth/index', $this->data);
    }
    
    /**
    * Edit User
    *
    * @param integer $id
    */
    function edit($id)
    {
        $this->_updatedata($id);
    }
    
    /**
    * Add a new User.
    */
    function add()
    {
        $this->_updatedata();
    }
    
    /**
    * Update profile.
    */
    function profile()
    {
        $this->data['redirect'] = 'auth/user/profile';
        $this->data['page_icons'] = '<a id="top-save-btn" href="" class="btn btn-success btn-labeled"><b><i class="icon-floppy-disk"></i></b>Simpan</a><a id="top-cancel-btn" href="' . site_url($this->data['redirect']) . '" class="btn btn-default">Batal</a>';
        $this->edit($this->auth->userid());
    }
    
    /**
    * Update user data
    *
    * @param int $id
    */
    function _updatedata($id = 0)
    {
     
        if (!isset($this->data['page_icons']))
            $this->data['page_icons'] = '<a id="top-save-btn" href="" class="btn btn-success btn-labeled"><b><i class="icon-floppy-disk"></i></b>Simpan</a><a id="top-cancel-btn" href="' . site_url('auth/user') . '" class="btn btn-default">Batal</a>';
        $this->load->library('form_validation');
        $user_form = $this->user_form;
        
        // Update rules for update data
        if ($id > 0)
        {
            $user_form['username']['rules'] = "trim|required|max_length[255]|callback_unique_username[$id]";
            $user_form['email']['rules']    = "trim|required|max_length[255]|valid_email|callback_unique_email[$id]";
            $user_form['password']['rules'] = "trim|matches[confirm-password]";
            $user_form['confirm-password']['rules'] = "trim";
            
            $user = $this->user_model->get_by_id($id);
        }
        
        // Add language options
        $languages = $this->config->item('languages', 'template');
        foreach($languages as $code => $language)
        $user_form['lang']['options'][$code] = $language['name'];
        
        // Add role options
        $role_tree = $this->role_model->get_tree();
        $availableRoles = array();
        if (isset($user)) {
            foreach ($user->roles as $r) {
                $availableRoles[$r->id] = $r->name;
            }
        }
        $user_form['role_id']['options'] = array(0 => '(' . lang('none') . ')') + $availableRoles;
        $user_form['roles_id']['options'] = array(0 => '(' . lang('none') . ')') + $this->role_model->generate_options($role_tree);
        
        // Add unit usaha options
        $user_form['unitkerja_id']['options'] = $this->Unit_kerja_model->dropdown_options(array('status' => 1));
        $user_form['bpom']['options'] = array('0' => 'Tidak', '1' => 'Ya');

        // Add farmasi unit options
        $fu_list = $this->Farmasi_unit_model->get_all(0, 0, "WHERE status = 1");
        foreach ($fu_list['data'] as $row) 
            $user_form['farmasi_unit_id']['options'][$row->id] = $row->nama;
        
        // Add Kamar Obat options
        $su_list = $this->Satelite_unit_model->get_all(0, 0, "WHERE status = 1");
        foreach ($su_list['data'] as $row)
            $user_form['satelite_unit_id']['options'][$row->id] = $row->nama;

        if ( ! $this->acl->is_allowed('auth/user')) {
            unset($user_form['ip']);
            // unset($user_form['unit_usaha_id']);
            unset($user_form['farmasi_unit_id']);
            unset($user_form['satelite_unit_id']);
            unset($user_form['roles_id']);
            unset($user_form['role_id']);
            unset($user_form['lang']);
        }
        
        $this->form_validation->init($user_form);
        // Set default value for update data
        if ($id > 0)
            $this->form_validation->set_default($this->user_model->get_by_id($id));
        if ($this->form_validation->run())
        {
            $data = $this->form_validation->get_values();
            // $data['pegawai_id'] = $this->input->post('pegawai_id');
            $data['farmasi_unit_id'] = !is_null($this->input->post('farmasi_unit_id')) ? $this->input->post('farmasi_unit_id') : array();
            $data['satelite_unit_id'] = !is_null($this->input->post('satelite_unit_id')) ? $this->input->post('satelite_unit_id') : array();
            $data['roles_id'] = $this->input->post('roles_id');
            if ($id > 0)
            {
                $this->user_model->update($id, $data);
                $this->template->set_flashdata('success', lang('user_updated'));
            }
            else
            {
                $this->user_model->insert($data);
                $this->template->set_flashdata('success', lang('user_added'));
            }
            
            if (isset($this->data['redirect']))
                redirect($this->data['redirect']);
            else
                redirect('auth/user');
        }
        
        $this->data['form'] = $this->form_validation;
        $this->template
        ->set_js('core/libraries/jquery_ui/interactions.min')
        ->set_js('core/libraries/jquery_ui/widgets.min')
        ->set_js('core/libraries/jquery_ui/effects.min')
        ->build('auth/user-form', $this->data);
    }
    
    /**
    * Delete a User
    *
    * @param integer $id
    */
    function delete($id)
    {
        $user = $this->user_model->get_by_id($id);
        if ($user) {
            $this->user_model->delete($id);
            $this->template->set_flashdata('success', lang('user_deleted'));
        }
        
        redirect('auth/user');
    }
    
    /**
    * Validation callback function to check whether the username is unique
    *
    * @param string $value Username to check
    * @param int $id Don't check if the username has this ID
    * @return boolean
    */
    function unique_username($value, $id = 0)
    {
        if ($this->user_model->is_username_unique($value, $id))
            return TRUE;
        else
        {
            $this->form_validation->set_message('unique_username', lang('already_taken'));
            return FALSE;
        }
    }
    
    /**
    * Validation callback function to check whether the email is unique
    *
    * @param string $value Email to check
    * @param int $id Don't check if the email has this ID
    * @return boolean
    */
    function unique_email($value, $id = 0)
    {
        if ($this->user_model->is_email_unique($value, $id))
            return TRUE;
        else
        {
            $this->form_validation->set_message('unique_email', lang('already_taken'));
            return FALSE;
        }
    }
    
    function pegawai_autocomplete_data(){
        $this->load->model('master/pegawai/pegawai_model');
        $data = $this->pegawai_model->autocomplete();
        echo json_encode($data);
    }
    
}

/* End of file user.php */
/* Location: ./application/modules/auth/controllers/user.php */