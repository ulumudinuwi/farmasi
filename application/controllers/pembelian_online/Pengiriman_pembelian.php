<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengiriman_pembelian extends Admin_Controller 
{
	protected $page_title = '<i class="icon-people"></i> Pengiriman Pembelian Online';
	protected $table_def = 't_pos_checkout';

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
	}

	public function index()
	{
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_script('pembelian_online/pengiriman_pembelian/script-index')
			->build('pembelian_online/pengiriman_pembelian/index');
	}

	public function getWaybill(){
		$courier = $this->input->post('courier');
		$no_resi = $this->input->post('no_resi');

		$curl = curl_init();	

		curl_setopt_array($curl, array(
		  	CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
		  	CURLOPT_RETURNTRANSFER => true,
		  	CURLOPT_ENCODING => "",
		  	CURLOPT_MAXREDIRS => 10,
		  	CURLOPT_TIMEOUT => 30,
		  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  	CURLOPT_CUSTOMREQUEST => "POST",
		  	CURLOPT_POSTFIELDS => "waybill=".$no_resi."&courier=".$courier."",
		  	CURLOPT_HTTPHEADER => array(
		    	"key: 2a2bd1917c36ad63ca43b88a0bf741e2"
		  	),
		));
	 
		$response = curl_exec($curl);
		$err = curl_error($curl);
	 
		if (!$this->input->is_ajax_request()){
			return $response;
		} else echo $response;
		
	}

}