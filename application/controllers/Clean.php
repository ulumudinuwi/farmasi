<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 12/6/16
 * Time: 07:02
 */
class Clean extends Clean_Controller
{

    public function status_lokalis()
    {
        $this->template->set_js(js_url('libs/smartzoom/e-smart-zoom-jquery'), TRUE);
        $this->template->set_js(js_url('config-smartzoom'), TRUE);
        $this->template->build('status_lokalis');
    }

}