<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permintaan_unit extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Permintaan dan Penerimaan Unit';
  protected $table_def_permintaan = 't_request_permintaan_unit';
  protected $table_def_penerimaan = 't_request_penerimaan_unit';
  protected $table_def_mutasi = 't_request_mutasi_unit';
  protected $unit;

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->unit = $this->db->where('status', 1)
                        ->get('m_unitkerja')->result();
  }

  public function index()
  {
    $this->data['tab1Uid'] = base64_encode($this->table_def_permintaan);
    $this->data['tab2Uid'] = base64_encode($this->table_def_penerimaan);
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->data['status'] = $this->config->item('status_request');
    $this->data['status_penerimaan'] = $this->config->item('status_penerimaan');
    $this->data['page_icons'] = '<a href="'. site_url("request/permintaan_unit/add") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Buat Permintaan</a>';
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script('request/unit-kerja/permintaan/script-index')
          ->build('request/unit-kerja/permintaan/index', $this->data);
  }

  /**
   * Edit
   */
  function edit($uid)
  {
    $this->_updatedata($uid);
  }
  
  /**
   * Tambah
   */
  function add()
  {
    $this->_updatedata(0);
  }
  
  function _updatedata($uid = 0)
  {
    $this->data['page_title'] = '<i class="icon-book"></i> Permintaan Barang ke Gudang Logistik';
    $this->data['uid'] = $uid;
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
          ->set_script('request/unit-kerja/permintaan/script-form')
          ->build('request/unit-kerja/permintaan/form', $this->data);  
  }

  /*
  * Penerimaan
  */
  function penerimaan($uid = 0)
  {
    if($uid === 0) {
      show_404(); exit();
    } 

    $this->data['page_title'] = '<i class="icon-book"></i> Penerimaan Barang dari Gudang Logistik';
    $this->data['uid'] = $uid;
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_script('request/unit-kerja/penerimaan/script-form')
          ->build('request/unit-kerja/penerimaan/form', $this->data);  
  }
}