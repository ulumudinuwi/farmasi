<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerimaan extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Penerimaan';
  protected $table_def = 't_logistik_penerimaan';
  protected $table_def_po = 't_logistik_po';
  protected $def_uri = 'logistik/penerimaan';
  protected $pabrik;

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->pabrik = $this->db->where('status', 1)
                        ->get('m_pabrik')->result();
  }

  public function index()
  {
    $status_po = array(
      $this->config->item('status_po_waiting_for_delivery'),
      $this->config->item('status_po_partial_received'),
    );

    $this->data['tab1Uid'] = base64_encode($this->table_def_po);
    $this->data['tab2Uid'] = base64_encode($this->table_def);
    $this->data['pabrik'] = $this->pabrik;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->data['status_po'] = get_status($status_po, $this->config->item('status_po'));
    $this->data['status'] = $this->config->item('status_penerimaan');
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script($this->def_uri.'/script-index')
          ->build($this->def_uri.'/index', $this->data);
  }

  /**
   * Edit
   */
  function edit($uid)
  {
    $data = new stdClass();
    $data->script_view = $this->def_uri.'/script-form-edit';
    $data->view = $this->def_uri.'/form-edit';
    $this->_updatedata($uid, $data);
  }
  
  /**
   * Tambah
   */
  function form($uid = 0)
  {
    $data = new stdClass();
    $data->script_view = $this->def_uri.'/script-form';
    $data->view = $this->def_uri.'/form';
    $this->_updatedata($uid, $data);
  }
  
  function _updatedata($uid = 0, $data = "")
  {
    $this->data['uid'] = $uid;
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_script($data->script_view)
          ->build($data->view, $this->data);  
  }
}