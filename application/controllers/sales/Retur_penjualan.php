<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;

class Retur_penjualan extends Admin_Controller 
{
    protected $page_title = '<i class="icon-sync"></i> Retur Penjualan';
	protected $def_uri = 'sales/retur_penjualan';
    
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 
	 */
	public function index()
	{
		$this->data['-'] = '<a href="'.base_url($this->def_uri.'/retur_penjualan').'" class="btn btn-primary btn-labeled"><b><i class=" icon-plus-circle2"></i></b>Tambah</a>';
        $this->template
				->set_css('global_a')
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
                ->build($this->def_uri . '/index', $this->data);
	}

	public function form($uid)
	{
		$this->data['uid'] = $uid;
	
		$this->load->view($this->def_uri . '/form', $this->data);
	}
}