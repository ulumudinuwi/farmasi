<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;

class Laporan extends Admin_Controller 
{
    protected $page_title = '<i class="icon-chart"></i> Laporan';
	protected $def_uri = 'sales/laporan';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 

		$this->dokter = $this->db->where('status', 1)->get('m_dokter_member')->result();
		$this->barang = $this->db->where('status', 1)->get('m_barang')->result();
	}

	/**
	 * 
	 */
	public function index()
	{
		$this->data['-'] = '<a href="'.base_url($this->def_uri.'/new_cicilan').'" class="btn btn-primary btn-labeled"><b><i class=" icon-plus-circle2"></i></b>Tambah</a>';
        $this->template
				->set_css('global_a')
				->set_css('page/pos')
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('app/page/pos', TRUE)
                ->build($this->def_uri . '/index', $this->data);
	}

	public function laporan_001() {
		if(is_manager()){
			$this->sales = $this->db->where('status', 1)->where('manager_sales_id', 1)->get('m_sales')->result();
		}else if (is_sales()) {
			$this->sales = $this->db->where('status', 1)->where('id', sales_id())->get('m_sales')->result();
		}else{
			$this->sales = $this->db->where('status', 1)->get('m_sales')->result();
		}
		
		$this->data['sales'] = $this->sales;
		$this->data['dokter'] = $this->dokter;
		$this->data['barang'] = $this->barang;
		$this->load->view('sales/laporan/laporan-001', $this->data);
	}

	public function laporan_002() {
		if(is_manager()){
			$this->sales = $this->db->where('status', 1)->where('manager_sales_id', 1)->get('m_sales')->result();
		}else if (is_sales()) {
			$this->sales = $this->db->where('status', 1)->where('id', sales_id())->get('m_sales')->result();
		}else{
			$this->sales = $this->db->where('status', 1)->get('m_sales')->result();
		}
		
		$this->data['sales'] = $this->sales;
		$this->data['dokter'] = $this->dokter;
		$this->data['barang'] = $this->barang;
		$this->load->view('sales/laporan/laporan-002', $this->data);
	}

	public function laporan_003() {
		if(is_manager()){
			$this->sales = $this->db->where('status', 1)->where('manager_sales_id', 1)->get('m_sales')->result();
		}else if (is_sales()) {
			$this->sales = $this->db->where('status', 1)->where('id', sales_id())->get('m_sales')->result();
		}else{
			$this->sales = $this->db->where('status', 1)->get('m_sales')->result();
		}
		
		$this->data['sales'] = $this->sales;
		$this->data['dokter'] = $this->dokter;
		$this->data['barang'] = $this->barang;
		$this->load->view('sales/laporan/laporan-003', $this->data);
	}

	public function laporan_004() {
		if(is_manager()){
			$this->sales = $this->db->where('status', 1)->where('manager_sales_id', 1)->get('m_sales')->result();
		}else if (is_sales()) {
			$this->sales = $this->db->where('status', 1)->where('id', sales_id())->get('m_sales')->result();
		}else{
			$this->sales = $this->db->where('status', 1)->get('m_sales')->result();
		}
		
		$this->data['sales'] = $this->sales;
		$this->data['dokter'] = $this->dokter;
		$this->data['barang'] = $this->barang;
		$this->load->view('sales/laporan/laporan-004', $this->data);
	}

	public function laporan_005() {
		if(is_manager()){
			$this->sales = $this->db->where('status', 1)->where('manager_sales_id', 1)->get('m_sales')->result();
		}else if (is_sales()) {
			$this->sales = $this->db->where('status', 1)->where('id', sales_id())->get('m_sales')->result();
		}else{
			$this->sales = $this->db->where('status', 1)->get('m_sales')->result();
		}
		
		$this->data['sales'] = $this->sales;
		$this->data['dokter'] = $this->dokter;
		$this->data['barang'] = $this->barang;
		$this->load->view('sales/laporan/laporan-005', $this->data);
	}
}