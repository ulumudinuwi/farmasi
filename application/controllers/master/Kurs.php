<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kurs extends Admin_Controller 
{
	protected $page_title;

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('kurs');
	}

	/**
	 * 
	 */
	public function index()
	{
		$this->data['page_title'] = '<i class="icon-store"></i> '.$this->lang->line('label_kurs');
		$this->data['page_icons'] = '<a href="'. site_url("master/kurs/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		$this->template
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/buttons/spin.min', FALSE)
					->set_js('plugins/buttons/ladda.min', FALSE)
					->build('master/kurs/index', $this->data);
	}

	public function form($uid = "")
	{
		$this->data['uid'] = $uid;

		if ($uid === "") 
			$this->data['page_title'] = '<i class="icon-store"></i> Tambah '.$this->lang->line('label_kurs');
		else 
			$this->data['page_title'] = '<i class="icon-store"></i> Edit '.$this->lang->line('label_kurs');
		$this->template
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
			->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
			->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
			->set_script('master/kurs/script-form')
			->build('master/kurs/form', $this->data);
	}

}