<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_barang extends Admin_Controller 
{
  protected $page_title = '<i class="icon-store"></i> Kategori Barang';
  protected $def_uri = 'master/kategori_barang';
  protected $jenis_barang;

  public function __construct()
  {
    parent::__construct();
    $this->jenis_barang = $this->db->select('id, nama')
                                    ->where('status', 1)
                                    ->get('m_jenisbarang')->result();
  }

  /**
   * 
   */
  public function index()
  {
    $this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/buttons/spin.min', FALSE)
          ->set_js('plugins/buttons/ladda.min', FALSE)
          ->build($this->def_uri . '/index', $this->data);
  }

  public function form($uid = "")
  {
    $this->data['uid'] = $uid;
    $this->data['jenis_barang'] = $this->jenis_barang;
    if ($uid === "") 
      $this->data['page_title'] = '<i class="icon-store"></i> Tambah Kategori Barang';
    else 
      $this->data['page_title'] = '<i class="icon-store"></i> Edit Kategori Barang';
    $this->template
      ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
      ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
      ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
      ->set_js('plugins/forms/validation/validate.min.js', TRUE)
      ->set_js('plugins/tables/datatables/datatables.min', FALSE)
      ->set_js('plugins/notifications/bootbox.min', FALSE)
      ->set_js('plugins/notifications/sweet_alert.min', FALSE)
      ->set_js('plugins/buttons/spin.min', FALSE)
      ->set_js('plugins/buttons/ladda.min', FALSE)
      ->set_js('plugins/ui/moment/moment.min', FALSE)
      ->set_script($this->def_uri . '/script-form')
      ->build($this->def_uri . '/form', $this->data);
  }

}