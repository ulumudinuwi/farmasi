<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang extends Admin_Controller 
{
	protected $page_title = '<i class="icon-store"></i> Barang';
	protected $def_uri = 'master/barang';

	protected $table_def_jenis = 'm_jenisbarang';
	protected $table_def_pabrik = 'm_pabrik';
	protected $table_def_vendor = 'm_vendor';

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
	}

	/**
	 * 
	 */
	public function index()
	{
		$this->data['jenis'] = $this->db->query("SELECT id, nama FROM {$this->table_def_jenis} WHERE status = 1")->result();
		$this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		$this->template
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/buttons/spin.min', FALSE)
					->set_js('plugins/buttons/ladda.min', FALSE)
					->build($this->def_uri . '/index', $this->data);
	}

	public function form($uid = "")
	{
		$this->data['uid'] = $uid;
		if ($uid === "") 
			$this->data['page_title'] = '<i class="icon-store"></i> Tambah Barang';
		else 
			$this->data['page_title'] = '<i class="icon-store"></i> Edit Barang';
		$this->template
          	->set_css('../bower_components/Croppie/croppie')
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          	->set_js('../bower_components/Croppie/croppie', TRUE)
			->set_script($this->def_uri . '/script-form')
			->build($this->def_uri . '/form', $this->data);
	}

	public function harga_jual($uid = "")
	{
		$this->data['uid'] = $uid;
		$this->data['jenis'] = $this->db->query("SELECT id, nama FROM {$this->table_def_jenis} WHERE status = 1")->result();
		$this->data['page_title'] = '<i class="icon-store"></i> Harga Jual Barang';
		$this->template
          	->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->build($this->def_uri . '/harga_jual/index', $this->data);
	}

	public function form_harga_jual($uid = "")
	{
		$this->data['uid'] = $uid;
		$this->data['page_title'] = '<i class="icon-store"></i> Edit Harga Jual Barang';
		$this->template
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
			->set_script($this->def_uri . '/harga_jual/script-form')
			->build($this->def_uri . '/harga_jual/form', $this->data);
	}

}