<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Ryuna\Auth;
use Ryuna\Response;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Dokter extends Admin_Controller 
{
    protected $page_title = '<i class="icon-user"></i> Dokter';
	protected $def_uri = 'master/dokter';
    
	public function __construct()
	{
		parent::__construct();
		// $this->lang->load('sales'); 
		$this->load->model('master/Sales_model');
		$this->load->model('master/Area_model');
		$this->load->model('master/Dokter_model');
	}

	/**
	 * 
	 */
	public function index()
	{
		if(is_sales() || is_manager() || is_admin()){
			$this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		}else{
			$this->data['page_icons'] = '';
		}
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/dokter/index')
				->set_js('app/pages/master/dokter/index', TRUE)
				//Build
                ->build($this->def_uri . '/index', $this->data);
	}

	/**
	 * 
	 */
	public function form()
	{	
		$tahun_now = Carbon::now()->format('Y');
		$uid = $this->input->get('uid');
		$this->data['method'] = 'store';
		if($uid){
			$this->data['method'] = 'update';
			$this->data['dokter'] = Dokter_model::where('uid', $uid)->first();
			$this->data['sales'] = Sales_model::where('id', $this->data['dokter']['sales_id'])->first();
			
			$akun_user = DB::table('auth_users')->where('dokter_id',$this->data['dokter']->id)->first();

        	if (!$akun_user) {
        		$akun_user = new stdClass();
        		$akun_user->username = '';
        		$akun_user->email = '';
        	}
        	$this->data['akun_user'] = (Object) $akun_user;
		}
		// dd($resource);
		$this->data['sales_all'] = Sales_model::where('status','=',1)->where('is_manager_sales','=',0)->lists('nama','id');
		$this->data['manager_sales_all'] = Sales_model::where('status','=',1)->where('is_manager_sales','=',1)->lists('nama','id');
		$area = $this->Area_model->get_all("WHERE m_area.status = 1");
		$this->data['area_all'] = $area['data'];
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
		// cek_dulu($this->data['sales_all']);
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('plugins/forms/styling/uniform.min', TRUE)
				->set_js('plugins/forms/styling/switchery.min', TRUE)
				->set_js('plugins/forms/styling/switch.min', TRUE)
				->set_js('jquery.mask.min', TRUE)
				->set_js('plugins/forms/selects/select2.min.js', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/dokter/form')
				->set_js('app/pages/master/dokter/form', TRUE)
				//Build
                ->build($this->def_uri . '/form', $this->data);
	}

}