<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pekerjaan extends Admin_Controller 
{
	protected $page_title = '<i class="icon-store"></i> Pekerjaan';
	protected $def_uri = 'master/pekerjaan';
	protected $table_def = "m_pekerjaan";
	protected $jenis_barang;

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		$this->template
			  ->set_js('plugins/tables/datatables/datatables.min', FALSE)
			  ->set_js('plugins/notifications/bootbox.min', FALSE)
			  ->set_js('plugins/notifications/sweet_alert.min', FALSE)
			  ->set_js('plugins/ui/moment/moment.min', FALSE)
			  ->set_js('plugins/buttons/spin.min', FALSE)
			  ->set_js('plugins/buttons/ladda.min', FALSE)
			  ->build($this->def_uri . '/index', $this->data);
	}
	
	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.status")
			 ->from($this->table_def.' a');
		echo $this->datatables->generate();
	}

	public function form($uid = "")
	{
	$this->data['uid'] = $uid;
	$this->data['jenis_barang'] = $this->jenis_barang;
	if ($uid === "") 
	  $this->data['page_title'] = '<i class="icon-store"></i> Tambah Kelompok Barang';
	else 
	  $this->data['page_title'] = '<i class="icon-store"></i> Edit Kelompok Barang';
	$this->template
	  ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
	  ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
	  ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
	  ->set_js('plugins/forms/validation/validate.min.js', TRUE)
	  ->set_js('plugins/tables/datatables/datatables.min', FALSE)
	  ->set_js('plugins/notifications/bootbox.min', FALSE)
	  ->set_js('plugins/notifications/sweet_alert.min', FALSE)
	  ->set_js('plugins/buttons/spin.min', FALSE)
	  ->set_js('plugins/buttons/ladda.min', FALSE)
	  ->set_js('plugins/ui/moment/moment.min', FALSE)
	  ->set_script($this->def_uri . '/script-form')
	  ->build($this->def_uri . '/form', $this->data);
	}

}