<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Ryuna\Auth;
use Ryuna\Response;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Manager_sales extends Admin_Controller 
{
    protected $page_title = '<i class="fa fa-user"></i> Manager Sales';
	protected $def_uri = 'master/manager_sales';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 
		$this->load->model('master/Sales_model');
		$this->load->model('sales/Target_sales_model');
	}

	/**
	 * 
	 */
	public function index()
	{
		$this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/manager_sales/index')
				->set_js('app/pages/master/manager_sales/index', TRUE)
				//Build
                ->build($this->def_uri . '/index', $this->data);
	}

	/**
	 * 
	 */
	public function form()
	{	
        $bulan = (Int) Carbon::now()->format('m');
		$tahun_now = Carbon::now()->format('Y');
		$array_bulan = [
			'1' => 'JANUARI',
			'2' => 'FEBRUARI',
			'3' => 'MARET',
			'4' => 'APRIL',
			'5' => 'MEI',
			'6' => 'JUNI',
			'7' => 'JULI',
			'8' => 'AGUSTUS',
			'9' => 'SEPTEMBER',
			'10' => 'OKTOBER',
			'11' => 'NOVEMBER',
			'12' => 'DESEMBER',
		];
		$uid = $this->input->get('uid');
		$this->data['method'] = 'store';
		if($uid){
			$this->data['method'] = 'update';
			$this->data['sales'] = (Object) Sales_model::where('uid', $uid)->first();
        	$akun_user = DB::table('auth_users')->where('sales_id',$this->data['sales']->id)->first();
        	if (!$akun_user) {
        		$akun_user = new stdClass();
        		$akun_user->username = '';
        		$akun_user->email = '';
        	}
        	$this->data['akun_user'] = (Object) $akun_user;
			$results = DB::table('t_target_sales')->whereRaw('sales_id in (select id from m_sales where manager_sales_id ='.$this->data['sales']->id.'  AND status = 1)')
				->select(Db::raw('sum(target) as target, bulan, tahun'))
				->groupBy(Db::raw('tahun, bulan'))
				->get();

			for($i = 0; $i < count($results); $i++){
				$tahun = $results[$i]['tahun'];
				$bulan = $results[$i]['bulan'];

				$gettargetID = DB::table('t_target_sales')->whereRaw('sales_id ='.$this->data['sales']->id.' AND tahun = '.$tahun.' AND bulan ='.$bulan)
				->select(Db::raw('id, uid'))
				->first();

				// if(!array_key_exists($tahun,$resources)){
				$resources[$tahun][$bulan]['id'] = $gettargetID['id'];
				$resources[$tahun][$bulan]['uid'] = $gettargetID['uid'];
				$resources[$tahun][$bulan]['bulan'] = $results[$i]['bulan'];
				$resources[$tahun][$bulan]['tahun'] = $results[$i]['tahun'];
				$resources[$tahun][$bulan]['bulan_label'] = $array_bulan[$results[$i]['bulan']];
				$resources[$tahun][$bulan]['target'] = $results[$i]['target'];
				// }
			}
			$this->data['target_sales'] = $resources;
			$new_target_sales = Target_sales_model::where('sales_id', $this->data['sales']->id)->where('tahun', $tahun_now)->count();
			// dd($new_target_sales);
			$this->data['new_target_sales'] = $new_target_sales == 0 ? TRUE : FALSE;
		}

		$this->data['array_bulan'] = $array_bulan;
		$this->data['tahun'] = $tahun_now;
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('plugins/forms/styling/uniform.min', TRUE)
				->set_js('plugins/forms/styling/switchery.min', TRUE)
				->set_js('plugins/forms/styling/switch.min', TRUE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/manager_sales/form')
				->set_js('app/pages/master/manager_sales/form', TRUE)
				//Build
                ->build($this->def_uri . '/form', $this->data);
	}

	public function detail()
	{
		Sales_model::redraw_sales();

		$array_bulan = [
			'1' => 'JANUARI',
			'2' => 'FEBRUARI',
			'3' => 'MARET',
			'4' => 'APRIL',
			'5' => 'MEI',
			'6' => 'JUNI',
			'7' => 'JULI',
			'8' => 'AGUSTUS',
			'9' => 'SEPTEMBER',
			'10' => 'OKTOBER',
			'11' => 'NOVEMBER',
			'12' => 'DESEMBER',
		];

		$this->data['tahun'] = Carbon::now()->format('Y');
		$uid = isset($_GET['uid']) ? $_GET['uid'] : '' ;
		if(!$uid){
			$uid = DB::table('m_sales')->where('id', Auth::user()->sales_id)->first()['uid'];
		}

		$sales = (Object) DB::table('m_sales')->where('uid', $uid)->first();
		
		$query = DB::table('m_dokter_member');
		if(is_manager()){
            $get_id_with_sales_bawahan = get_bawahan_sales_id();
            $get_id_with_sales_bawahan[] = sales_id();
            // var_dump($get_id_with_sales_bawahan);
            if(count($get_id_with_sales_bawahan) > 0) {
                $query->whereIn('sales_id',$get_id_with_sales_bawahan);
            }
        }else if(is_sales()){
            $query->where('sales_id', sales_id());
        }
		
		$member_sales = (Object) $query->get();
		
		// cek_dulu($member_sales);
		$this->data['member_sales'] = $member_sales;
		$results = Target_sales_model::where('sales_id', $sales->id)->orderBy('tahun','DESC')->orderBy('bulan','ASC')->get();
		/*$results = DB::table('t_target_sales')->whereRaw('sales_id in (select id from m_sales where manager_sales_id ='.$sales->id.')')
				->select(Db::raw('sum(target) as target, bulan, tahun, pencapaian, persentase, insentif_pendapatan, insentif_dari_pendapatan, insentif_member, insentif_khusus, total_insentif_didapat as total'))
				->groupBy(Db::raw('tahun, bulan'))
				->get();*/
		for($i = 0; $i < count($results); $i++){
			$id = $results[$i]['id'];
			$tahun = $results[$i]['tahun'];
			$bulan = $results[$i]['bulan'];

			/*$gettargetID = DB::table('t_target_sales')->whereRaw('sales_id ='.$sales->id.' AND tahun = '.$tahun.' AND bulan ='.$bulan)
			->select(Db::raw('id, uid'))
			->first();*/

			// if(!array_key_exists($tahun,$resource)){
			$resource[$tahun][$bulan]['id'] = $results[$i]['id'];
			$resource[$tahun][$bulan]['uid'] = $results[$i]['uid'];
			$resource[$tahun][$bulan]['bulan'] = $results[$i]['bulan'];
			$resource[$tahun][$bulan]['tahun'] = $results[$i]['tahun'];
			$resource[$tahun][$bulan]['bulan_label'] = $array_bulan[$results[$i]['bulan']];
			$resource[$tahun][$bulan]['data'] = $results[$i];
			// }
		}
		$this->data['target_sales'] = $resource;
		$this->data['sales'] = $sales;
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/sales/detail')
				->set_js('app/pages/master/manager_sales/detail', TRUE)
				//Build
                ->build($this->def_uri . '/detail', $this->data);
	}

}