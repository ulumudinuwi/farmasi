<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemeriksaan extends Admin_Controller 
{
    protected $page_title = '';
    protected $def_uri = 'master/radiologi/pemeriksaan';

    public function __construct()
    {
        parent::__construct();       

        $this->lang->load('common');
        $this->lang->load('radiologi');
        $icon = lang('master_pemeriksaan_icon');
        $title = lang('master_pemeriksaan_title');
        $this->page_title = '<i class="'.$icon.'"></i> '.$title;
    }

    /**
     * 
     */
    public function index()
    {
        $this->data['page_title'] = $this->page_title;
        $this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>'.lang('btn_add').'</a>';
        $this->template
                    ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                    ->set_js('plugins/notifications/bootbox.min', FALSE)
                    ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                    ->set_js('plugins/ui/moment/moment.min', FALSE)
                    ->set_js('plugins/buttons/spin.min', FALSE)
                    ->set_js('plugins/buttons/ladda.min', FALSE)
                    ->build($this->def_uri . '/index', $this->data);
    }

    public function form($uid = "")
    {
        $this->data['uid'] = $uid;
        if ($uid === "") 
            $this->data['page_title'] = '<i class="'.lang('master_pemeriksaan_icon').'"></i> '.lang('master_pemeriksaan_add');
        else 
            $this->data['page_title'] = '<i class="'.lang('master_pemeriksaan_icon').'"></i> '.lang('master_pemeriksaan_edit');
        $this->template
            ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
            ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
            ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
            ->set_js('plugins/forms/validation/validate.min.js', TRUE)
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/buttons/spin.min', FALSE)
            ->set_js('plugins/buttons/ladda.min', FALSE)
            ->set_js('plugins/editors/summernote/summernote.min', TRUE)
            ->set_js('plugins/editors/summernote/lang/summernote-id-ID', TRUE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
            ->build($this->def_uri . '/form', $this->data);
    }

}