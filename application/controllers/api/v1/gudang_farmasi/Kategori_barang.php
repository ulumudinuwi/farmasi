<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;

class Kategori_barang extends Rest {
	protected $limit = 10;
	protected $table_def = "m_kategoribarang";
	protected $auth_key, $continue;
	
	public function __construct() {
		parent::__construct();
		$this->load->model('master/Kategori_barang_model', 'main');

		$this->continue = false;
		$this->auth_key = $this->input->get_request_header('Authorization');
		if($this->auth_key != $this->secretKey) {
			if($this->checkToken()) $this->continue = true;
		} else $this->continue = true;
	}

	public function list_get() {
		$limit = $this->input->get('limit') ? $this->input->get('limit') : $this->limit;
		$offset = $this->input->get('offset') ? $this->input->get('offset') : 0;
		$term = $this->input->get('term') ? $this->input->get('term') : '';

		if($this->continue) {
			/*
	         * Order
	         */
			$sOrder = "";
	        $aOrders = array();
	        $aOrders[] = "{$this->table_def}.nama ASC";
	        if (count($aOrders) > 0) {
	            $sOrder = implode(', ', $aOrders);
	        }
	        if (!empty($sOrder)) {
	            $sOrder = "ORDER BY ".$sOrder;
	        }

	        /*
	         * Where
	         */
	        $sWhere = "";
	        $aWheres = array();
	        $aWheres[] = "{$this->table_def}.status = 1";
	        if (count($aWheres) > 0) {
	            $sWhere = implode(' AND ', $aWheres);
	        }
	        if (!empty($sWhere)) {
	            $sWhere = "WHERE ".$sWhere;
	        }

	        $aLikes = array();
	        if($term != "") {
	            $aLikes[] = "{$this->table_def}.kode = '{$term}'";
	            $aLikes[] = "{$this->table_def}.nama LIKE '%{$term}%'";
	        }

	        if (count($aLikes) > 0) {
	            $sLike = "(".implode(' OR ', $aLikes).")";
	            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
	        }

	        $aSelect = array(
	        	"{$this->table_def}.id",
	        	"{$this->table_def}.uid",
	        	"{$this->table_def}.kode",
	        	"{$this->table_def}.nama",
	        );
			$list = $this->main->get_all($limit, $offset * $limit, $sWhere, $sOrder, $aSelect);
			$output['total_data'] = $list['total_rows'];
			$output['total_pages'] = (ceil($list['total_rows'] / $limit)) - 1;
			$output['sisa_data'] = $list['total_rows'] - (($offset + 1) * $limit) < 0 ? 0 : $list['total_rows'] - (($offset + 1) * $this->limit);
			$output['list'] = $list['data'];

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($output));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}
}