<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;

class Add_tocart extends Rest {
	protected $limit = 0;
	protected $table_def = "t_pos_pesanan";
    protected $table_def_barang = 'm_barang';
	protected $table_def_foto = "m_barang_foto";
    protected $table_def_member = 'm_dokter_member';
	protected $ori_dir, $thumb_dir, $auth_key, $continue;

	public function __construct() {
		parent::__construct();
		$this->load->model('Add_tocart_model', 'main');
		$this->load->model('gudang_farmasi/Stock_model', 'stock');
		$this->ori_dir = './uploads/barang/original';
        $this->thumb_dir = './uploads/barang/thumbnail';

		$this->continue = false;
		$this->auth_key = $this->input->get_request_header('Authorization');
		if($this->auth_key != $this->secretKey) {
			if($this->checkToken()) $this->continue = true;
		} else $this->continue = true;
	}

	public function insert_post() {
		$member_id = $this->input->post('member_id') ? : '';
		$product_uid = $this->input->post('product_uid') ? : '';
		$qty = $this->input->post('qty') ? : '';

		if($this->continue) {
			if((int) $this->stock->checkAvailableStockByBarangUid($product_uid) <= 0) {
				$this->output->set_status_header(504)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product tidak tersedia."]));
		        return;
			}

			$obj = new stdClass();
            $obj->member_id = $member_id;
            $obj->product_uid = $product_uid;
            $obj->qty = $qty;
            $obj->created_by = $member_id;
            $obj->update_by = $member_id;

			$pExist = $this->main->get_by("WHERE {$this->table_def}.product_uid = '{$product_uid}' AND {$this->table_def}.member_id = '{$member_id}' AND {$this->table_def}.status = 1");

			if ($pExist) {
				$this->output->set_status_header(400);
				echo json_encode(["message" => "The product has been moved to the cart."]);
				exit();
			}

            $insert = $this->main->create($obj);
            
            if ($insert) {
				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Product has been added to Cart."]));
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}

	public function list_get() {
		$member_id = $this->input->get('member_id') ? : '';

		$limit = $this->input->get('limit') ? $this->input->get('limit') : $this->limit;
		$offset = $this->input->get('offset') ? $this->input->get('offset') : 0;
		$term = $this->input->get('term') ? $this->input->get('term') : '';

		if($this->continue) {
			/*
	         * Order
	         */
			$sOrder = "";
	        $aOrders = array();
	        $aOrders[] = "{$this->table_def_barang}.nama ASC";
	        if (count($aOrders) > 0) {
	            $sOrder = implode(', ', $aOrders);
	        }
	        if (!empty($sOrder)) {
	            $sOrder = "ORDER BY ".$sOrder;
	        }

	        /*
	         * Where
	         */
	        $sWhere = "";
	        $aWheres = array();
	        $aWheres[] = "{$this->table_def}.member_id = '{$member_id}'";
	        $aWheres[] = "{$this->table_def}.status = 1";
	        $aWheres[] = "{$this->table_def}.deleted = 0";
	        if (count($aWheres) > 0) {
	            $sWhere = implode(' AND ', $aWheres);
	        }
	        if (!empty($sWhere)) {
	            $sWhere = "WHERE ".$sWhere;
	        }

	        $aLikes = array();
	        if($term != "") {
	            $aLikes[] = "{$this->table_def_barang}.kode = '{$term}'";
	            $aLikes[] = "{$this->table_def_barang}.nama LIKE '%{$term}%'";
	        }

	        if (count($aLikes) > 0) {
	            $sLike = "(".implode(' OR ', $aLikes).")";
	            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
	        }

	        $aSelect = array(
	        	"{$this->table_def}.id",
	        	"{$this->table_def}.uid",
	        	"{$this->table_def}.qty",
	        	"{$this->table_def}.product_uid as uid_barang",
	        	"{$this->table_def_barang}.id as product_id",
	        	"{$this->table_def_barang}.uid as product_uid",
	        	"{$this->table_def_barang}.nama as product_nama",
	        	"{$this->table_def_barang}.kode as product_kode",
	        	"{$this->table_def_barang}.harga_penjualan as product_harga",
	        	"{$this->table_def_barang}.berat_barang as product_berat",
	        	"{$this->table_def_barang}.deskripsi as product_deskripsi",
	        	"{$this->table_def_barang}.diskon as product_diskon",
	        );

			$list = $this->main->get_all($limit, $offset * $limit, $sWhere, $sOrder, $aSelect);
			foreach ($list['data'] as $i => $obj) {
				//$obj->harga = getHargaDasar($obj->id, 'farmasi');
				$obj->harga = (int) $obj->product_harga != 0 ? $obj->product_harga : getHargaDasar($obj->product_id, 'farmasi');

				$fotos = $this->db->select('id, nama')
	                				->where('barang_id', $obj->product_id)
	                				->get($this->table_def_foto)->result();
	            $aFotos = array();
	            if(count($fotos) > 0) {
	            	foreach ($fotos as $row) {
	            		$fileori = base_url().'assets/img/no_image_available.png';
	            		$fileThumb = base_url().'assets/img/no_image_available.png';

	            		if (file_exists($this->ori_dir.'/'.$row->nama)) {
							$fileori 	= base_url($this->ori_dir).'/'.$row->nama;
	            			$fileThumb 	= base_url($this->thumb_dir).'/'.$row->nama;
	            		}

	            		$aFotos[] = [
	            			'original' => $fileori,
	            			'thumbnail' => $fileThumb,
	            		];
	            	}
	            }
	            $obj->foto = $aFotos;
	            unset($obj->id);
			}
			$output['total_data'] = $list['total_rows'];
			//$output['total_pages'] = (ceil($list['total_rows'] / $limit)) - 1;
			$output['sisa_data'] = $list['total_rows'] - (($offset + 1) * $limit) < 0 ? 0 : $list['total_rows'] - (($offset + 1) * $this->limit);
			$output['list'] = $list['data'];

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($output));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	public function delete_post() {
		$uid = $this->input->post('uid') ? : '';

		if($this->continue) {

            $deleted = $this->main->delete($uid);
            
            if ($deleted) {
				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Product has been deleted."]));
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "UID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}
}