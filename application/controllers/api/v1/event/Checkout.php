<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
class Checkout extends REST_Controller {
	function __construct()
    {
        parent::__construct();
    }
    
    public function listen_get() {
    	require_once(APPPATH.'events/ecommerce/CheckoutEvent.php');
    	
        session_write_close(); # !Important

		$uid = $this->input->get('uid');
		$sse = new CheckoutEvent($uid, 'checkout_event');
		$sse->start();
    }
}