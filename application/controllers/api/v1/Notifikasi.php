<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');

class Notifikasi extends Rest {
	protected $table_def = "t_notifikasi_ecommerce";
	protected $auth_key, $continue;
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('notifikasi_ecommerce');
		
		$this->continue = false;
		$this->auth_key = $this->input->get_request_header('Authorization');
		if($this->auth_key != $this->secretKey) {
			if($this->checkToken()) $this->continue = true;
		} else $this->continue = true;
	}

	public function list_get() {
		$member_id = $this->get('id') ? : '';
		$page = $this->get('page') ? : 1;
		$limit = $this->get('limit') ? : 5;
		
		if($this->continue) {
			$data = notif_msg_fetch($member_id, ($page == 1 ? 0 : ($page-1) * $limit), $limit);

	        $hash = '0';
	        foreach ($data as $d) {
	        	$d->created_at = date('d-m-Y H:i', strtotime($d->created_at));
	            $hash .= $d->id;
	        }
	        $hash = md5($hash);

	        // BADGE NOTIF
	        $badge = $this->db->where('member_id', $member_id)
                            ->where('read_flag', 0)
                            ->count_all_results($this->table_def);

			$output = ['data' => $data, 'badge' => $badge, 'hash' => $hash];

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($output));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	public function read_post() {
		$id = $this->post('id') ? : '';

		if($this->continue) {
			$result = notif_msg_read($id);

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($result));
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}

	public function read_all_post() {
		$member_id = $this->post('id') ? : '';

		if($this->continue) {
			$result = notif_msg_read_all_by_member_id($member_id);

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode(['message' => 'Notifikasi berhasil dibaca semua.']));
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}
}