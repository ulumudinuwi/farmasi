<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Mpdf\Mpdf;

class Laporan extends CI_Controller {

    protected $current_user = "Administrator";
    public function __construct()
  	{
    	parent::__construct();
        $this->lang->load('barang');
        $this->load->helper('harga_barang');
        $this->load->model('keuangan/Laporan_model', 'main');

        if($this->session->has_userdata('first_name')) 
            $this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
  	}

    // LAPORAN 001
  	public function laporan_001(){
        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_buku_besar.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(t_buku_besar.update_at) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(t_buku_besar.update_at) <= '{$tanggal_sampai}'";
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_buku_besar.perkiraan_id " : "GROUP BY t_buku_besar.perkiraan_id";

        $list = $this->main->get_all(0, 0, $sWhere, "ORDER BY t_buku_besar.tipe ASC");

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $dataPendapatan = array();
        $dataBeban = array();
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $rows[] = $data;
            if ($obj->klasifikasi == JENIS_PERKIRAAN_PENDAPATAN || $obj->klasifikasi == JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN) {
                $dataPendapatan[] = $obj;
            }else if ($obj->klasifikasi == JENIS_PERKIRAAN_BEBAN_ATAS_PENDAPATAN  || $obj->klasifikasi == JENIS_PERKIRAAN_BEBAN_OPERASIONAL || $obj->klasifikasi == JENIS_PERKIRAAN_BEBAN_NON_OPERASIONAL || $obj->klasifikasi == JENIS_PERKIRAAN_BEBAN_LAIN_LAIN ) {
                $dataBeban[] = $obj;
            }
        }
        $output['data'] = $rows;
        $output['data_pendapatan'] = $dataPendapatan;
        $output['data_beban'] = $dataBeban;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_buku_besar.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(t_buku_besar.update_at) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(t_buku_besar.update_at) <= '{$tanggal_sampai}'";
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;
        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_buku_besar.perkiraan_id " : "GROUP BY t_buku_besar.perkiraan_id";

        $list = $this->main->get_all(0, 0, $sWhere, "ORDER BY t_buku_besar.tipe ASC");

        $data = [];
        $dataPendapatan = [];
        $dataBeban = [];
        $pendapatan = 0;
        $beban = 0;
        foreach ($list['data'] as $row) {
            if ($row->klasifikasi == JENIS_PERKIRAAN_PENDAPATAN || $row->klasifikasi == JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN) {
                $dataPendapatan[] = $row;
                $pendapatan += $row->total;
            }else if ($row->klasifikasi == JENIS_PERKIRAAN_BEBAN_ATAS_PENDAPATAN  || $row->klasifikasi == JENIS_PERKIRAAN_BEBAN_OPERASIONAL || $row->klasifikasi == JENIS_PERKIRAAN_BEBAN_NON_OPERASIONAL || $row->klasifikasi == JENIS_PERKIRAAN_BEBAN_LAIN_LAIN ) {
                $dataBeban[] = $row;
                $beban += $row->total;
            }
        }
        $data['data_pendapatan'] = $dataPendapatan;
        $data['data_beban'] = $dataBeban;
        $LabaRugi = $pendapatan - $beban;

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'sales/laporan/laporan-001.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('B2', "LAPORAN LABA / RUGI");
                $sheet->setCellValue('B3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 5;
                $increment = 5;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $total = $row->total;
                        $grandTotal += $row->total;
                        
                        $obj = new stdClass();
                        //$obj->no = $no;
                        $obj->no_fbp = $row->no_fbp;
                        $obj->no_invoice = $row->no_invoice;
                        $obj->update_at = $row->update_at;
                        $obj->dokter = $row->dokter;
                        $obj->barang = $row->barang ? $row->barang : "-";
                        $obj->marketing = $row->marketing;
                        if ($row->status_lunas == 1) {
                            $obj->status_lunas = 'Lunas';
                        }else{
                            $obj->status_lunas = 'Belum Lunas';
                        }
                        $obj->qty = $row->qty;
                        if ($row->is_bonus == 1) {
                            # code...
                            $obj->harga = 'FREE';
                            $obj->jumlah = 'FREE';
                            $obj->diskon = 'FREE';
                            $obj->total = 'FREE';
                        }else{
                            $obj->harga = $row->harga;
                            $obj->jumlah = $row->jumlah;
                            $obj->diskon = $row->diskon.' '.($row->diskon_plus ? ' + '.$row->diskon_plus : '');
                            $obj->total = $row->total;
                        }
                        $aRow = get_object_vars($obj);
                        //$no++;


                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        $subTotalCells[] = "I{$increment}";
                        $increment++;
                    }
                    
                    # Row Grand Total
                    //$grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:K{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("L" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':L'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:L{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':L'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':L' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 9
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:L{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                ob_end_clean();
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Rekap Sales.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'rows' => $data,
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('keuangan/laporan/laporan-001-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4-P',
                    'margin_left' => '5',
                    'margin_right' => '5',
                    'margin_top' => '15',
                    'margin_bottom' => '5',]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('LAPORAN LABA / RUGI.PDF', "I");
                break;
        }
    }

    public function save(){
        $CI =& get_instance();
        $CI->load->helper('laba_rugi_helper');
        $labarugi = $this->input->post('labarugi');
        $tanggal_dari = $this->input->post('tanggal_dari');
        $tanggal_sampai = $this->input->post('tanggal_sampai');

        # code...
        $dataLabaRugi = new stdClass();
        $dataLabaRugi->total = $labarugi;
        $dataLabaRugi->modul = JENIS_MODUL_LAPORAN;
        $dataLabaRugi->keterangan = 'Laba / Rugi Tahun Berjalan periode '.date('Y-m-d H:i:s');
        $dataLabaRugi->bpom = $this->session->userdata('bpom');;

        if ($labarugi >= 0) {
            $dataLabaRugi->perkiraan_id = get_option('perkiraan_laba_rugi');
            $dataLabaRugi->tipe = JENIS_DEBIT;
        }else{
            $dataLabaRugi->perkiraan_id = get_option('perkiraan_laba_rugi');
            $dataLabaRugi->tipe = JENIS_KREDIT;
        }

        $cekLabarugi = $this->db->select('id')
                                ->where('DATE(created_at) >=', $tanggal_dari)
                                ->where('DATE(created_at) <=', $tanggal_sampai)
                                ->where('modul', JENIS_MODUL_LAPORAN)
                                ->get('t_buku_besar')->row();

        if ($cekLabarugi) {
            $dataLabaRugi->id = $cekLabarugi->id;
            if(updateLabarugibyId($dataLabaRugi)){
                echo true;
            }
        }else{
            //insert
            if(insertLabarugi($dataLabaRugi)){
                echo true;
            }
        }
    }
}