<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_kas_bank extends CI_Controller 
{
	protected $table_def = "t_kas_bank";
	protected $table_def_detail = "t_kas_bank_detail";
	
	function __construct() {
		parent::__construct();
		$this->lang->load('barang');

		$this->load->model('keuangan/kas_bank/Pengeluaran_kas_bank_model', 'main');
	}

	/**
	 * Load data
	 */
	public function load_data(){
		$tanggal_dari  = $_POST['tanggal_dari'];
		$tanggal_sampai  = $_POST['tanggal_sampai'];
		
		$aColumns = array('diterima_oleh', 'tanggal');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def}.status = 1";
		$aWheres[] = "{$this->table_def}.deleted = 0";
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.tanggal",
			"{$this->table_def}.diterima_oleh",
			"{$this->table_def}.no_pengeluaran",
			"{$this->table_def}.status",
			"CONCAT(created_by.first_name, ' ',created_by.last_name) created_by",
		);
		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function save() {
		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		if($obj->uid == ''){
			$result = $this->main->create($obj);
		}else{
			$result = $this->main->update($obj);
		}

		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200);
		echo json_encode($result);
	}

	public function get_data($uid = 0) {
		if (!$this->input->is_ajax_request())
			exit();

		if ($uid) {
			$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
			$obj->details = $this->db->select('id, kas_bank_id, keterangan, jumlah, perkiraan_id as perkiraan, status')
				            ->where('kas_bank_id', $obj->id)
				            ->get('t_kas_bank_detail')
				            ->result();
		}
		echo json_encode(['data' => $obj]);
	}

	/**
	 * Form Data Object
	 */
	private function _getDataObject() {
		$his = date('H:i:s');
		$obj = new stdClass();
		$obj->id = $this->input->post('id') ? $this->input->post('id') : '';
		$obj->uid = $this->input->post('uid') ? $this->input->post('uid') : '';
		$obj->tanggal = get_date_accepted_db($this->input->post('tanggal'));
		$obj->diterima_oleh = $this->input->post('diterima_oleh');
		$obj->dibayar_oleh = $this->input->post('dibayar_oleh');
		$obj->perkiraan_id = $this->input->post('perkiraan_id');
		$obj->status = 1;
		$obj->kode_p = "PM";
		$obj->jumlah = 0;
		$obj->modul = JENIS_MODUL_PENERIMAAN_KAS_BANK;
		$obj->tipe = JENIS_DEBIT;
		
		$query = $this->db->select('id, kas_bank_id, keterangan, jumlah, status')
	            ->where('kas_bank_id', $obj->id)
	            ->get('t_kas_bank_detail')
	            ->result();

	    $details['data'] = $query;

	    $aDetails = array();
	    array_map(function($each) use (&$aDetails) {
	      $each->data_mode = Data_mode_model::DATA_MODE_DELETE;
	      $aDetails[$each->id] = $each;
	      return $each;
	    }, $details['data']);

	    if (isset($_POST['detail_id'])) {
	      for ($i = 0; $i < count($_POST['detail_id']); $i++) {
	        $detail_id = $_POST['detail_id'][$i];
			$obj->jumlah += $_POST['jumlah'][$i];
			$obj->keterangan = "Penerimaan Kas Bank senilai : ".$obj->jumlah;;
	        if (!array_key_exists($detail_id, $aDetails)) {
	          $detail = new StdClass();
	          $detail->id = $detail_id;
	          $detail->keterangan = $_POST['keterangan'][$i];
	          $detail->jumlah = $_POST['jumlah'][$i];
	          $detail->perkiraan = $_POST['perkiraan'][$i];
			  $detail->keterangan_detail = "Penerimaan Kas Bank senilai : ".$_POST['jumlah'][$i];;
			  $detail->tipe = JENIS_KREDIT;
	          $detail->data_mode = Data_mode_model::DATA_MODE_ADD;
	          $aDetails[uniqid()] = $detail;
	        } else {
	          $aDetails[$detail_id]->keterangan = $_POST['keterangan'][$i];
	          $aDetails[$detail_id]->jumlah = $_POST['jumlah'][$i];
			  $aDetails[$detail_id]->tipe = JENIS_KREDIT;
			  $aDetails[$detail_id]->keterangan_detail = "Penerimaan Kas Bank senilai : ".$_POST['jumlah'][$i];;
	          $aDetails[$detail_id]->perkiraan = $_POST['perkiraan'][$i];
	          $aDetails[$detail_id]->data_mode = Data_mode_model::DATA_MODE_EDIT;
	        }    
	      }
	    }
	    $obj->details = $aDetails;
		return $obj;
	}

    public function delete() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $modul = JENIS_MODUL_PENERIMAAN_KAS_BANK;
        $result = $this->main->delete_pengeluaran_kas_bank($uid, $modul);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function get_perkiraan() {
	    $query = $this->db->select('id, nama')
	            ->where('parent_id !=', 0)
	            ->where('deleted_flag', 0)
	            ->order_by('nama', 'asc')
	            ->get('m_perkiraan')
	            ->result();

	    $output['list'] = $query;

	    echo json_encode($output);
	}

    public function get_dibayar_oleh() {
    	$q = $this->input->get('q');

	    $query = $this->db->distinct()
	    		->select('dibayar_oleh')
	            ->like('dibayar_oleh', $q)
	            ->order_by('dibayar_oleh', 'asc')
	            ->get('t_kas_bank')
	            ->result();

	    $output['list'] = $query;

	    echo json_encode($output);
	}

    public function get_diterima_oleh() {
    	$q = $this->input->get('q');

	    $query = $this->db->distinct()
	    		->select('diterima_oleh')
	            ->like('diterima_oleh', $q)
	            ->order_by('diterima_oleh', 'asc')
	            ->get('t_kas_bank')
	            ->result();

	    $output['list'] = $query;

	    echo json_encode($output);
	}
}