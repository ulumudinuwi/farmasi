<?php defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller 
{
  protected $table_def_barang = "m_barang";
  protected $table_def_detail = "t_request_permintaan_unit_detail";
  
  function __construct() {
    parent::__construct();
    $this->lang->load('barang');

    $this->load->model('logistik/Stock_model', 'stock_main');
    $this->load->model('request/unit_kerja/Permintaan_detail_model', 'detail_main');
  }

  /**
   * Load data
   */
  public function load_data(){
    $uid = json_decode(base64_decode($this->input->get('uid')));

    $aSelect = array(
      "CONCAT({$this->table_def_barang}.kode, '-', {$this->table_def_barang}.nama) barang",
      "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan",
    );
    $data = $this->stock_main->get_by("WHERE {$this->table_def_barang}.id = {$uid}", $aSelect);

    /*
     * Ordering
     */
    $sOrder = "";
    $aOrders = array();
    $aOrders[] = "{$this->table_def_detail}.created_at DESC";
    if (count($aOrders) > 0) {
      $sOrder = implode(', ', $aOrders);
    }
    if (!empty($sOrder)) {
      $sOrder = "ORDER BY ".$sOrder;
    }

    /*
     * Where
     */
    $sWhere = "";
    $aWheres = array();
    $aWheres[] = "{$this->table_def_detail}.barang_id = {$uid}";
    if (count($aWheres) > 0) {
      $sWhere = implode(' AND ', $aWheres);
    }
    if (!empty($sWhere)) {
      $sWhere = "WHERE ".$sWhere;
    }

    $aLikes = array();
    if (count($aLikes) > 0) {
      $sLike = "(".implode(' OR ', $aLikes).")";
      $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
    }

    $aSelect = array(
      "a.kode",
      "a.pemohon",
      "uk.nama unit_kerja",
      "{$this->table_def_detail}.qty qty_order",
      "{$this->table_def_detail}.dikirim qty_diproses",
      "{$this->table_def_detail}.created_at tanggal",
    );

    $list = $this->detail_main->get_all(5, 0, $sWhere, $sOrder, $aSelect);
    $data->details = $list['data'];

    echo json_encode(['data' => $data]);
  }  
}