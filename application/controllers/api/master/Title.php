<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Title extends CI_Controller 
{
	protected $table_def = "m_title";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Title_model');
	}

	/**
	* Load data
	*/
	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.status")
			->from($this->table_def.' a');
		echo $this->datatables->generate();
	}

  public function save() {

    if (!$this->input->is_ajax_request())
      exit();

    $obj = $this->_getDataObject();
    if (isset($obj->uid) && $obj->uid != "") {
      $result = $this->main->update($obj);
      $this->template->set_flashdata('success', "Data telah berhasil diperbarui.");
    }
    else {
      $result = $this->main->create($obj);
      $this->template->set_flashdata('success', "Data telah berhasil disimpan.");
    }
    echo json_encode($result);
  }

	public function update_status() {
		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->post('uid');
		$status = $this->input->post('status');

		$result = $this->main->update_status($uid, $status);
		echo json_encode($result);
	}

  public function get_all() {

    if (!$this->input->is_ajax_request())
      exit();

    $mode = $this->input->get('mode') ? $this->input->get('mode') : "";
    $jenis_id = $this->input->get('jenis_id') ? $this->input->get('jenis_id') : "0";
    if (base64_decode($jenis_id, true)) $jenis_id = base64_decode($jenis_id);

    $sWhere = "";
    $aWheres = array();
    $aWheres[] = "{$this->table_def}.status = 1";
    if($mode == "by") $aWheres[] = "{$this->table_def_jenis}.id = {$jenis_id}";
    if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
    if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;
    
    $list = $this->main->get_all(0, 0, $sWhere, "ORDER BY {$this->table_def}.kode ASC");
    $output['list'] = $list['data'];

    echo json_encode($output);
  }

	public function get_data($uid = "") {
		if (!$this->input->is_ajax_request())
			exit();

		$output = array();
		if ($uid) {
			$obj = $this->Title_model->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
			$output['data'] = $obj;
		}
		echo json_encode($output);
	}


  /**
   * Form Data Object
   * 
   * 
   */
  private function _getDataObject() {
    $obj = (object) $_POST;
    return $obj;
  }
}