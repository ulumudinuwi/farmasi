<?php
use Ryuna\Response;

class Perkiraan extends Admin_Controller
{
    protected $table_def = 'm_perkiraan';

    public function __construct() {
        parent::__construct();

        $this->load->model('master/Perkiraan_model');
    }

    public function load_data() {
        //array
        $aHarta = array();

        $parentList = $this->getParent(0);
        foreach ($parentList as $row) {
            switch ((int) $row->klasifikasi) {
                case JENIS_PERKIRAAN_HARTA:
                    $aHarta[] = $row;
                    break;
                case JENIS_PERKIRAAN_KEWAJIBAN:
                    $aKewajiban[] = $row;
                    break;
                case JENIS_PERKIRAAN_MODAL:
                    $aModal[] = $row;
                    break;
                case JENIS_PERKIRAAN_PENDAPATAN:
                    $aPendapatan[] = $row;
                    break;
                case JENIS_PERKIRAAN_BEBAN_ATAS_PENDAPATAN:
                    $aBebanataspendapatan[] = $row;
                    break;
                case JENIS_PERKIRAAN_BEBAN_OPERASIONAL:
                    $aBebanoperasional[] = $row;
                    break;
                case JENIS_PERKIRAAN_BEBAN_NON_OPERASIONAL:
                    $aBebannonoperasional[] = $row;
                    break;
                case JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN:
                    $aPendapatanlainlain[] = $row;
                    break;
                case JENIS_PERKIRAAN_BEBAN_LAIN_LAIN:
                    $aBebanlainlain[] = $row;
                    break;
            }
        }

        $rData['data'] = new stdClass();
        $rData['data']->harta                       = $aHarta;
        $rData['data']->kewajiban                   = $aKewajiban;
        $rData['data']->modal                       = $aModal;
        $rData['data']->pendapatan                  = $aPendapatan;
        $rData['data']->beban_atas_pendapatan       = $aBebanataspendapatan;
        $rData['data']->bebanoperasional            = $aBebanoperasional;
        $rData['data']->bebannonoperasional         = $aBebannonoperasional;
        $rData['data']->pendapatanlainlain          = $aPendapatanlainlain;
        $rData['data']->bebanlainlain               = $aBebanlainlain;
        
        echo json_encode($rData);
    }

    private function getParent($parent_id = null){
        $parentList  = null;
        if (!is_null($parent_id)) {
            $parentList = $this->Perkiraan_model->get_all(0, 0, "WHERE (m_perkiraan.parent_id = {$parent_id}) AND m_perkiraan.deleted_flag != 1", "ORDER BY m_perkiraan.kode ASC")['data'];
            if(count($parentList) > 0) {
                foreach ($parentList as $row) {
                    $row->sub_parent = $this->getParent($row->id ? : null);
                }
            }
        }
        return $parentList;
    }
    public function get_data($uid) {
        if (!$this->input->is_ajax_request())
            exit();

        $output = array();
        if ($uid) {
            $obj = $this->Perkiraan_model->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");

            $output['data'] = $obj;
        } else {
            $output['data'] = new stdClass();
        }

        echo json_encode($output);
    }

    public function save() {
        if (!$this->input->is_ajax_request())
            exit();

        $obj = $this->_getDataObject();
        $result = null;
    
        if (isset($obj->id) && $obj->id != "0") { // Update
            $result = $this->Perkiraan_model->update($obj);
        } else { // Create
            $result = $this->Perkiraan_model->create($obj);
        }

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function delete() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->Perkiraan_model->delete($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function restore() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->Perkiraan_model->restore($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function list_data() {

        // if (!$this->input->is_ajax_request())exit();
        
        $list = $this->Perkiraan_model->get_all(0, 0);
        $output['list'] = $list['data'];
        return Response::json($output,200);
        
    }

    private function _getDataObject() {
        $obj = new stdClass();
        $obj->id = $this->input->post('id') && ($this->input->post('id') != "0") ? $this->input->post('id') : 0;
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "0") ? $this->input->post('uid') : 0;
        $obj->klasifikasi = $this->input->post('klasifikasi') ? $this->input->post('klasifikasi') : '';
        $obj->kode = $this->input->post('kode');
        $obj->nama = $this->input->post('nama');
        $obj->rincian = $this->input->post('rincian');
        $obj->parent_id = $this->input->post('parent') ? $this->input->post('parent') : 0;
        $obj->nama_alias = $this->input->post('nama_alias') ? $this->input->post('nama_alias') : '';
        $obj->status = $this->input->post('status') ? $this->input->post('status') : 0;

        return $obj;
    }
}