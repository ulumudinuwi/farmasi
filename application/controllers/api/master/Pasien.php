<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

    protected $table_def = "m_pasien";
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('master/Pasien_model');
    }
	
	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.no_rekam_medis, a.nama")
			 ->from($this->table_def.' a');
		echo $this->datatables->generate();
	}
	
	public function get_data($uid = "") {
		if (!$this->input->is_ajax_request())
			exit();

		$output = array();
		if ($uid) {
			$obj = $this->Pasien_model->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
			$output['data'] = $obj;
		}
		echo json_encode($output);
	}

}
