<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan extends CI_Controller 
{
	protected $table_def = "m_pendidikan";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('master/Pendidikan');
	}
	
	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.nama, a.status")
			 ->from($this->table_def.' a');
		echo $this->datatables->generate();
	}
	
	public function get_all() {

		if (!$this->input->is_ajax_request())
			exit();
		
		$query = $this->db->query('SELECT * FROM m_pendidikan');
		$output['pendidikan_list'] = $query->result();
		echo json_encode($output);
	}

}