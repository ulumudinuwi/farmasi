<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends CI_Controller 
{
  protected $table_def = "m_area";

  function __construct()
  {
    parent::__construct();
    $this->load->model('master/Area_model');
}

  /**
   * Load data
   */
  public function load_data() {
    $this->datatables->select('id, uid, nama, status')
    ->from($this->table_def);
    echo $this->datatables->generate();
}

public function save() {

    if (!$this->input->is_ajax_request())
      exit();

  $obj = $this->_getDataObject();
  if (isset($obj->uid) && $obj->uid != "") {
      $result = $this->Area_model->update($obj);
      $this->template->set_flashdata('success', "Data telah berhasil diperbarui.");
  }
  else {
      $result = $this->Area_model->create($obj);
      $this->template->set_flashdata('success', "Data telah berhasil disimpan.");
  }
  echo json_encode($result);
}

public function update_status() {
    if (!$this->input->is_ajax_request())
      exit();

  $uid = $this->input->post('uid');
  $status = $this->input->post('status');

  $result = $this->Area_model->update_status($uid, $status);
  echo json_encode($result);
}

public function get_all() {

    if (!$this->input->is_ajax_request())
      exit();

  $list = $this->Area_model->get_all(0, 0);
  $output['list'] = $list['data'];

  echo json_encode($output);
}

public function get_data($uid = "") {
    if (!$this->input->is_ajax_request())
      exit();

  $output = array();
  if ($uid) {
      $obj = $this->Area_model->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
      $output['data'] = $obj;
  }
  echo json_encode($output);
}

  /**
   * Form Data Object
   * 
   * 
   */
  private function _getDataObject() {
    $obj = (object) $_POST;
    return $obj;
}
}