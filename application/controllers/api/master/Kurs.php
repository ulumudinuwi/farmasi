<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kurs extends CI_Controller 
{
	protected $table_def = "m_kurs";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Kurs_model', 'main');
	}

	/**
	 * Load data
	 */
	public function load_data() {
		$this->datatables->select('*')
			->from($this->table_def);
		echo $this->datatables->generate();
	}

	public function save() {

		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		if (isset($obj->uid) && $obj->uid != "") {
			$result = $this->main->update($obj);
			$this->template->set_flashdata('success', "Data telah berhasil diperbarui.");
		}
		else {
			$result = $this->main->create($obj);
			$this->template->set_flashdata('success', "Data telah berhasil disimpan.");
		}
		echo json_encode($result);
	}

	public function update_status() {
		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->post('uid');
		$status = $this->input->post('status');

		$result = $this->main->update_status($uid, $status);
		echo json_encode($result);
	}

	public function get_all() {

		if (!$this->input->is_ajax_request())
			exit();

		$list = $this->main->get_all(0, 0, "WHERE status = 1");
		$output['list'] = $list['data'];

		echo json_encode($output);
	}

	public function get_data($uid = "") {
		if (!$this->input->is_ajax_request())
			exit();

		$output = array();
		if ($uid) {
			$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
		} else {
			$obj = new stdClass();
			$obj->id = 0;
			$obj->uid = '';
			$obj->country_id = '';
			$obj->country = '';
			$obj->currency_name = '';
			$obj->currency = 0;
		}
		$output['data'] = $obj;
		echo json_encode($output);
	}

	private function _getDataObject() {
		$obj = new stdClass();
		$obj->id = $this->input->post('id');
		$obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
		$obj->country_id = $this->input->post('country_id');
		$obj->country = $this->input->post('country');
		$obj->currency_name = $this->input->post('currency_name');
		$obj->currency = $this->input->post('currency');
		return $obj;
	}
}