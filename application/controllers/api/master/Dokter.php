<?php
use Illuminate\Database\Capsule\Manager as DB;
use Ryuna\Datatables;
use Ryuna\Auth;
use Ryuna\Response;
use Ryuna\Validasi;
use Carbon\Carbon;

class Dokter extends CI_Controller
{
    protected $table_def = 'm_dokter_member';

    public function __construct() {
        parent::__construct();
        $this->load->model('master/Sales_model');
        $this->load->model('master/Dokter_model');
        
        // if(!$this->input->is_ajax_request()) exit();
    }

    public function data_dummy(){
        $faker = Faker\Factory::create();
        $total_dummy = 100;
        

        return Response::json([
            'message' => 'Insert '.$total_dummy.' dummy success'
        ],200);
    }
    
    public function list_dt(){
        // Get Request
        $request = $this->input->post();
        
        // Select Columns
        $select = ['uid', 'nama', 'no_hp', 'email', 'alamat', 'status', 'no_member'];

        $this->datatables->select(implode(', ', $select));
        if(is_manager()){
            $get_id_with_sales_bawahan = get_bawahan_sales_id();
            $get_id_with_sales_bawahan[] = sales_id();
            // var_dump($get_id_with_sales_bawahan);
            if(count($get_id_with_sales_bawahan) > 0) {
                $this->datatables->where_in('sales_id', $get_id_with_sales_bawahan);
            }
        }else if(is_sales()){
            $this->datatables->where('sales_id', sales_id());
        }
       
        $this->datatables->from($this->table_def);
        return Response::datatables($this->datatables->generate());
        // // Where Conditions_
        // $filter = [];
        // foreach($select as $column){
        //     if(isset($request[$column]) && !empty($request[$column])) $filter[$column] = $request[$column];
        // }
        // $where  = " ";
        // $total_filter = count($filter);
        // if($total_filter > 0){
        //     $where .= "WHERE ";
        //     $current = 1;
        //     $first = 1;
        //     $last  = $total_filter; 
        //     foreach($filter as $key => $val){
        //         $where .= "{$key} LIKE '%{$val}%'";
        //         if($current != $last) $where .= " AND ";
        //         $current++; 
        //     }
        // }
        // // Query
        // $select = count($select) == 0 ? " * " : implode(',', $select);
        // $query = "SELECT {$select} FROM {$this->table_def} ";
        // if($where != " "){
        //     $query .= $where;
        // }

        // //Order
        // $query .= "";
        // echo Datatables::of($query)->generate();
    }

    public function store(){
        $now = Carbon::now()->format('Y-m-d H:m:s');
        $request = $this->getRequest();
        $tgl_lahir = $request->tgl_lahir ? $request->tgl_lahir : '01/01/0001';
        $rules = [
            'nama' => 'required',
            //'tempat_lahir' => 'required',
            //'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            //'email' => 'required|email',
            //'no_ktp' => 'required',
            //'no_npwp' => 'required',
            //'sales_id' => 'required',
            //'nama_klinik' => 'required',
            //'no_tlp_klinik' => 'required',
            //'alamat_klinik' => 'required',
            'area_id' => 'required',
            'plafon' => 'numeric|min_uang:20000000,Rp.20.000.000',
        ];

        $rolesDokter = (Object) DB::table('acl_roles')->where('name','LIKE',"%dokter%")->first();
        $rolesDokter = $rolesDokter->id;
        //cek exist no hp & area
        $validasi = Validasi::buat($request,$rules);
        if ($validasi) {
            if(!$validasi->status) return Response::json(['message' => $validasi->message],422);
        }
        $exist = $this->db->select('*')
                          ->from('m_dokter_member')
                          ->where('nama', $request->nama)
                          ->where('no_hp', $request->no_hp)
                          ->where('area', $request->area_id)
                          ->get()->num_rows();
        if ($exist == 0) {
            DB::beginTransaction();
            try {
                $dokter_id = DB::table($this->table_def)->insertGetId([
                    'uid' => uuid(),
                    'nama' => $request->nama,
                    'no_member' => get_no_member(),
                    'tempat_lahir' => $request->tempat_lahir,
                    'tgl_lahir' => date_to_db($tgl_lahir),
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'no_hp' => $request->no_hp,
                    'alamat' => $request->alamat,
                    'email' => $request->email,
                    'no_ktp' => $request->no_ktp,
                    'no_npwp' => $request->no_npwp,
                    'sales_id' => $request->sales_id ? $request->sales_id : null,
                    //'sales_id' => sales_id(),
                    'st_diskon_mou' => isset($request->st_diskon_mou) ? $request->st_diskon_mou : 0,
                    'diskon_mou' => $request->diskon_mou,
                    'tgl_jatuh_tempo' => $request->tgl_jatuh_tempo,
                    'tgl_mulai_mou' => $request->tgl_mulai_mou,
                    'nama_klinik' => $request->nama_klinik,
                    'alamat_klinik' => $request->alamat_klinik,
                    'no_tlp_klinik' => $request->no_tlp_klinik,
                    'plafon' => $request->plafon ? $request->plafon : 0,
                    'area' => $request->area_id,
                    'status' => 1,
                    'created_at' => $now,
                    'created_by' => Auth::user()->id,
                    'updated_at' => $now,
                    'updated_by' => Auth::user()->id,
                ]);

                $user_id = DB::table('auth_users')->insertGetId([
                    'uid' => uuid(),
                    'first_name' => $request->nama,
                    'username' => $request->username,
                    'email' => $request->email,
                    'password' => $this->passwordhash->HashPassword($request->password),
                    'dokter_id' => $dokter_id,
                    'role_id' => $rolesDokter,
                    'registered' => timestamp(),
                    'created_at' => timestamp(),
                    'created_by' => auth_id(),
                    'update_at' => timestamp(),
                    'update_by' => auth_id(),
                ]);

                DB::table('auth_users_roles')->insert([
                    'user_id' => $user_id,
                    'role_id' => $rolesDokter,
                    'created_at' => timestamp(),
                    'created_by' => auth_id(),
                    'update_at'  => timestamp(),
                    'update_by'  => auth_id(),
                ]);
                DB::commit();
                // all good
                return Response::json([
                    'message' => 'Dokter baru ditambahkan.'
                ],200);
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json([
                    'message' => $e->getMessage(),
                ],400);
                // something went wrong
            }
        }else{
            return Response::json([
                'message' => 'Nama, Nomor Hp dan Area Dokter Sudah terdaftar.'
            ],400);
        }
    }

    public function update(){
        $now = Carbon::now()->format('Y-m-d H:m:s');
        $request = $this->getRequest();
        $rules = [
            'nama' => 'required',
            //'tempat_lahir' => 'required',
            //'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            //'email' => 'required|email',
            //'no_ktp' => 'required',
            //'no_npwp' => 'required',
            //'sales_id' => 'required',
            //'nama_klinik' => 'required',
            //'no_tlp_klinik' => 'required',
            //'alamat_klinik' => 'required',
            //'area' => 'required',
            'plafon' => 'numeric|min_uang:20000000,Rp.20.000.000',
        ];
        $validasi = Validasi::buat($request,$rules);

        $rolesDokter = (Object) DB::table('acl_roles')->where('name','LIKE',"%dokter%")->first();
        $rolesDokter = $rolesDokter->id;

        $dokter_id = DB::table('m_dokter_member')->where('uid', $request->uid)->first()['id'];

        if(!$validasi->status) return Response::json(['message' => $validasi->message],422);
        DB::beginTransaction();
        try {
            DB::table($this->table_def)->where('uid', $request->uid)->update([
                'nama' => $request->nama,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date_to_db($request->tgl_lahir),
                'jenis_kelamin' => $request->jenis_kelamin,
                'no_hp' => $request->no_hp,
                'alamat' => $request->alamat,
                'email' => $request->email,
                'no_ktp' => $request->no_ktp,
                'no_npwp' => $request->no_npwp,
                'sales_id' => $request->sales_id,
                //'sales_id' => sales_id(),
                'st_diskon_mou' => $request->st_diskon_mou,
                'diskon_mou' => $request->diskon_mou,
                'tgl_jatuh_tempo' => $request->tgl_jatuh_tempo,
                'tgl_mulai_mou' => $request->tgl_mulai_mou,
                'nama_klinik' => $request->nama_klinik,
                'alamat_klinik' => $request->alamat_klinik,
                'no_tlp_klinik' => $request->no_tlp_klinik,
                'plafon' => $request->plafon ? $request->plafon : 0,
                'area' => $request->area_id,
                'updated_at' => $now,
                'updated_by' => Auth::user()->id,
            ]);

            $isExist = DB::table('auth_users')->where('dokter_id', $dokter_id)->first()['id'];
            if ($isExist) {
                $update_user = [
                    'first_name' => $request->nama,
                    'username' => $request->username,
                    'email' => $request->email,
                    'password' => $this->passwordhash->HashPassword($request->password),
                    'update_at' => timestamp(),
                    'update_by' => auth_id(),
                ];

                if(!$request->password) unset($update_user['password']);
                DB::table('auth_users')->where('dokter_id', $dokter_id)->update($update_user);

                DB::table('auth_users_roles')->where('user_id',$isExist)->update([
                    'user_id' => $isExist,
                    'role_id' => $rolesDokter,
                    'created_at' => timestamp(),
                    'created_by' => auth_id(),
                    'update_at'  => timestamp(),
                    'update_by'  => auth_id(),
                ]);
            }else{
                $user_id = DB::table('auth_users')->insertGetId([
                    'uid' => uuid(),
                    'first_name' => $request->nama,
                    'username' => $request->username,
                    'email' => $request->email,
                    'password' => $this->passwordhash->HashPassword($request->password),
                    'dokter_id' => $dokter_id,
                    'role_id' => $rolesDokter,
                    'registered' => timestamp(),
                    'created_at' => timestamp(),
                    'created_by' => auth_id(),
                    'update_at' => timestamp(),
                    'update_by' => auth_id(),
                ]);

                DB::table('auth_users_roles')->insert([
                    'user_id' => $user_id,
                    'role_id' => $rolesDokter,
                    'created_at' => timestamp(),
                    'created_by' => auth_id(),
                    'update_at'  => timestamp(),
                    'update_by'  => auth_id(),
                ]);
            }
            
            DB::commit();
            // all good
            return Response::json([
                'message' => 'Dokter diperbaharui.'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json($e->getMessage(),400);
            // something went wrong
        }
    }

    public function show(){
        
    }

    public function switch_status(){
        $request = $this->getRequest();
        $uid = $request->uid;

        $sales = Dokter_model::where('uid', $uid)->first();
        if(!$sales) {
            return Response::json([
                'message' => 'User tidak ditemukan.',
            ],400);
        }
        $sales->status = $sales->status == 1 ? 0 : 1;
        if($sales->save()){
            $type = $sales->status == 1 ? 'aktifkan' : 'nonaktifkan';
            return Response::json([
                'message' => 'Sales Di'.$type,
            ],200);
        }
        return Response::json([
            'message' => 'Gagal mengubah status.',
        ],400);
    }
    
    public function getRequest()
    {
        $request = $_POST + $_FILES + $_GET;
        $request = (Object) $request;
        return $request;
    }
}