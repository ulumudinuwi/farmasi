<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pabrik extends CI_Controller 
{
	protected $table_def = "m_pabrik";
	protected $table_def_kurs = "m_kurs";
	protected $table_def_detail_distributor = "m_pabrik_distributor";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Pabrik_model', 'main');
		$this->load->model('master/Pabrik_distributor_model', 'main_distributor');
	}

	/**
	 * Load data
	 */
	public function load_data() {
		$this->datatables->select('a.id, a.uid, a.kode, a.nama, a.alamat, a.status, b.country_id, b.currency_name')
			->join($this->table_def_kurs.' b', 'a.kurs_id = b.id', 'left')
			->from($this->table_def.' a');
		echo $this->datatables->generate();
	}

	public function save() {

		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		if (isset($obj->uid) && $obj->uid != "") {
			$result = $this->main->update($obj);
			$this->template->set_flashdata('success', "Data telah berhasil diperbarui.");
		}
		else {
			$result = $this->main->create($obj);
			$this->template->set_flashdata('success', "Data telah berhasil disimpan.");
		}
		echo json_encode($result);
	}

	public function update_status() {
		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->post('uid');
		$status = $this->input->post('status');

		$result = $this->main->update_status($uid, $status);
		echo json_encode($result);
	}

	public function get_all() {

		if (!$this->input->is_ajax_request())
			exit();

		$list = $this->main->get_all(0, 0, "WHERE {$this->table_def}.status = 1");
		$output['list'] = $list['data'];

		echo json_encode($output);
	}

	public function get_data($uid = "") {
		if (!$this->input->is_ajax_request())
			exit();

		$output = array();
		if ($uid) {
			$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
			$obj->details = $this->main_distributor->get_all(0, 0, "WHERE {$this->table_def_detail_distributor}.pabrik_id = {$obj->id}", "ORDER BY is_default DESC")['data'];
			$output['data'] = $obj;
		}
		echo json_encode($output);
	}

	public function get_distributor() {
		if (!$this->input->is_ajax_request())
			exit();

		$pabrik_id = $this->input->get('q') ? $this->input->get('q') : 0;
		$list = $this->main_distributor->get_all(0, 0, "WHERE {$this->table_def_detail_distributor}.pabrik_id = {$pabrik_id}", "ORDER BY is_default DESC", "get");
		$output['list'] = $list['data'];

		echo json_encode($output);
	}

	/**
	 * Form Data Object
	 * 
	 * 
	 */
	private function _getDataObject() {
		$obj = new stdClass();
		$obj->id = $this->input->post('id');
		$obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
		$obj->kode = $this->input->post('kode');
		$obj->nama = $this->input->post('nama');
		$obj->kurs_id = $this->input->post('kurs_id') ? : null;
		$obj->alamat = $this->input->post('alamat');

		$details = $this->main_distributor->get_all(0, 0, "WHERE {$this->table_def_detail_distributor}.pabrik_id = {$obj->id}", "ORDER BY is_default DESC");
		$aDetails = array();
		array_map(function($each) use (&$aDetails) {
			unset($each->vendor);
			$each->data_mode = Data_mode_model::DATA_MODE_DELETE;
			$aDetails[$each->id] = $each;
			return $each;
		}, $details['data']);

		if (isset($_POST['detail_id'])) {
			for ($i = 0; $i < count($_POST['detail_id']); $i++) {
				$detail_id = $_POST['detail_id'][$i];
				if (!array_key_exists($detail_id, $aDetails)) {
					$detail = new StdClass();
					$detail->id = $detail_id;
					$detail->vendor_id = $_POST['vendor_id'][$i];
					$detail->is_default = $_POST['is_default'][$i];
					$detail->data_mode = Data_mode_model::DATA_MODE_ADD;
					$aDetails[uniqid()] = $detail;
				} else {
					$aDetails[$detail_id]->vendor_id = $_POST['vendor_id'][$i];
					$aDetails[$detail_id]->is_default = $_POST['is_default'][$i];
					$aDetails[$detail_id]->data_mode = Data_mode_model::DATA_MODE_EDIT;
				}    
			}
		}
		$obj->details = $aDetails;
		return $obj;
	}
}