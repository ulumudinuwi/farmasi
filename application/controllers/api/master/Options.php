<?php

class Options extends Admin_Controller
{
    protected $table_def = "sys_options";

    public function __construct() {
        parent::__construct();
        $this->load->model('master/Options_model');
    }

    public function load_data() {
        $userVisiblity = $this->input->get('visibility') ? checkBase64Value($this->input->get('visibility')) : 0;
        $aColumns = array('option_name', 'option_value', 'option_type', 'action');

        /* 
         * Paging
         */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $iLimit = intval( $_GET['iDisplayLength'] );
            $iOffset = intval( $_GET['iDisplayStart'] );
        }

        /*
         * Ordering
         */
        $sOrder = "";
        $aOrders = array();
        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i <intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true") {
                    switch ($aColumns[intval($_GET['iSortCol_'.$i])]) {
                        case 'option_name':
                            $aOrders[] = $this->table_def.".option_name ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'option_value':
                            $aOrders[] = $this->table_def.".option_value ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'option_type':
                            $aOrders[] = $this->table_def.".option_type ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'action':
                                break;
                    }
                }
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        if($userVisiblity) 
            $aWheres[] = "{$this->table_def}.user_visibility = {$userVisiblity}";
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
        if (isset($_GET['sSearch'])) {
            for ($i = 0; $i < count($aColumns); $i++) {
                switch ($aColumns[$i]) {
                    case 'option_name':
                        $aLikes[] = $this->table_def.".option_name LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'option_value':
                        $aLikes[] = $this->table_def.".option_value LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'option_type':
                        $aLikes[] = $this->table_def.".option_type LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'action':
                        break;
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->Options_model->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            if($userVisiblity) {
                if($obj->option_table == "hrd_karyawan") {
                    $optVal = getOptionValueToByTableLookup((get_option($obj->option_name) ? : 0), 'hrd_karyawan');
                    $obj->option_value = $optVal ? $optVal->nama : '';
                }
            }
            $rows[] = get_object_vars($obj);
        }
        $output['aaData'] = $rows;

        echo json_encode($output);
    }

    public function load_data_lookup() {
        $aColumns = array('kode', 'nama');

        $table = $_GET['sSearch_0'];

        if ($table == "") {
            $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            );
            echo json_encode($output);exit();
        }

        $fields = $this->_getFieldsLookup($table);

        /* 
         * Paging
         */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $iLimit = intval( $_GET['iDisplayLength'] );
            $iOffset = intval( $_GET['iDisplayStart'] );
        }

        /*
         * Ordering
         */
        $sOrder = "";
        $aOrders = array();
        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i <intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true") {
                    switch ($aColumns[intval($_GET['iSortCol_'.$i])]) {
                        case 'kode':
                            $aOrders[] = $fields['kode']." ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'nama':
                            $aOrders[] = $fields['nama']." ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                    }
                }
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();

        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
        if (isset($_GET['sSearch'])) {
            for ($i = 0; $i < count($aColumns); $i++) {
                switch ($aColumns[$i]) {
                    case 'kode':
                        $aLikes[] = $fields['kode']." LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'nama':
                        $aLikes[] = $fields['nama']." LIKE '%".$_GET['sSearch']."%'";
                        break;
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->Options_model->get_all_lookup($table, $fields, $iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $rows[] = get_object_vars($obj);
        }
        $output['aaData'] = $rows;

        echo json_encode($output);
    }

    public function get_data($option_id) {
        if (!$this->input->is_ajax_request())
            exit();

        $output = array();
        if ($option_id) {
            $obj = $this->Options_model->get_by("WHERE {$this->table_def}.option_id = \"{$option_id}\"");

            $output['data'] = $obj;
        } else {
            $output['data'] = new stdClass();
        }

        echo json_encode($output);
    }

    public function save() {
        if (!$this->input->is_ajax_request())
            exit();

        $obj = $this->_getDataObject();
        $result = null;
    
        if (isset($obj->option_id) && $obj->option_id != "0") { // Update
            $result = $this->Options_model->update($obj);
        } else { // Create
            $result = $this->Options_model->create($obj);
        }

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function delete() {
        if (! $this->input->is_ajax_request())
            exit();

        $option_id = $this->input->post('option_id');
        $result = $this->Options_model->delete($option_id);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    private function _getDataObject() {
        $obj = new stdClass();
        $obj->option_id = $this->input->post('option_id') && ($this->input->post('option_id') != "0") ? $this->input->post('option_id') : 0;
        $obj->option_name = $this->input->post('option_name');
        $obj->option_value = $this->input->post('option_value');
        $obj->option_table = $this->input->post('option_table');
        $obj->option_type = $this->input->post('option_type');
        $obj->autoload = $this->input->post('autoload');
        $obj->user_visibility = $this->input->post('user_visibility');

        return $obj;
    }

    private function _getFieldsLookup($table) {
        $result = $this->db->query('DESC '.$table)->result();

        $fieldLook = array(
            'id',
            'kode',
            'nama',
            str_ireplace('m_', '', $table),
        );
        $availAliases = array(
            'id',
            'kode',
            'nama',
        );

        $fields = array();
        $alias = '';
        $isUidExists = false;
        foreach ($result as $row) {
            if ($row->Field == 'uid') {
                $isUidExists = true;
            }
            if (in_array($row->Field, $fieldLook) !== FALSE) {
                if (in_array($row->Field, $availAliases)) {
                    $fields[$row->Field] = $row->Field;
                } else {
                    foreach ($availAliases as $as) {
                        if (! array_key_exists($as, $fields)) {
                            $fields[$as] = $row->Field;
                        }
                    }
                }
            }
        }

        foreach ($availAliases as $as) {
            if (! array_key_exists($as, $fields)) {
                if ($as == 'kode') {
                    $fields[$as] = $isUidExists ? 'uid' : "id";
                } else {
                    $fields[$as] = $isUidExists ? 'uid' : "id";
                }
            }
        }

        return $fields;
    }
}