<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

    protected $table_def = "m_barang";
    protected $table_def_foto = "m_barang_foto";
    protected $ori_dir, $thumb_dir;
    
    public function __construct()
    {
        parent::__construct();
        $this->ori_dir = './uploads/barang/original';
        $this->thumb_dir = './uploads/barang/thumbnail';

        $this->load->helper('harga_barang');
        $this->load->model('master/Barang_model', 'main');
        $this->load->model('master/Barang_kelompok_model', 'barang_kelompok');
    }

    public function load_data(){
        $jenis_id  = $this->input->post('jenis_id');
        $kategori_id  = $this->input->post('kategori_id');
        $browse  = $this->input->post('browse') ? : '';

        $aColumns = array('kode', 'nama', 'pabrik', 'satuan_pembelian', 'satuan_penggunaan', '');
        if($browse == "master-harga_jual") $aColumns = array('kode', 'nama', 'pabrik', 'harga_penjualan', '');
        /* 
         * Paging
         */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
         * Ordering
         */
        $sOrder = "";
        $aOrders = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if($_POST['columns'][$i]['orderable'] == "true") {
                if($i == $_POST['order'][0]['column']) {
                    switch ($aColumns[$i]) {
                        case 'pabrik':
                            $aLikes[] = 'pb.nama '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
                            break;
                        default:
                            $aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
                            break;
                    }
                }
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "{$this->table_def}.bpom = ".$this->session->userdata('bpom');
        }
        if($jenis_id != "") $aWheres[] = "{$this->table_def}.jenis_id = ".base64_decode($jenis_id);
        if($kategori_id != "") $aWheres[] = "{$this->table_def}.kategori_id = ".base64_decode($kategori_id);
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        case 'nama':
                            $aLikes[] = "{$this->table_def}.nama LIKE '%".$_POST['search']['value']."%' OR {$this->table_def}.alias LIKE '%".$_POST['search']['value']."%'";
                            break;
                        case 'pabrik':
                            $aLikes[] = "pb.nama LIKE '%".$_POST['search']['value']."%'";
                            break;
                        default:
                            $aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $i++;
        }
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function get_data() {
        if(!$this->input->is_ajax_request()) 
            exit();

        $uid = $this->input->get('uid');
        $output = array();
        if($uid) {
            $obj = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'");
            $obj->hna_by_setting = getHargaDasar($obj->id, 'farmasi');
            $obj->fotos = $this->db->select('id, CONCAT("'.base_url($this->ori_dir).'/", nama) foto, "Tersimpan" status')
                                ->where('barang_id', $obj->id)
                                ->get($this->table_def_foto)->result();

            $aKelompoks = [];
            $kelompoks = $this->barang_kelompok->get_all(0, 0, "WHERE barang_id = {$obj->id}")['data'];
            if(count($kelompoks) > 0) {
                foreach ($kelompoks as $kelompok) {
                    $aKelompoks[] = $kelompok->kelompok_id;
                }
            }
            $obj->kelompoks = $aKelompoks;
        } else {
            $obj = new stdClass();
            $obj->id = 0;
            $obj->uid = "";
            $obj->kode = "";
            $obj->nama = "";
            $obj->merk = "";
            $obj->jenis_id = "";
            $obj->kategori_id = "";
            $obj->pabrik_id = "";
            //$obj->kelompok_id = "";
            $obj->berat_barang = "";
            $obj->deskripsi = "";
            $obj->harga_pembelian = "";
            $obj->satuan_pembelian_id = "";
            $obj->diskon_mou = 0;
            $obj->satuan_penggunaan_id = "";
            $obj->isi_satuan_penggunaan = 1;
            $obj->fotos = array();
            $obj->kelompoks = array();
        }

        $obj->url_satuan_pembelian_id = site_url('api/master/satuan/get_all');
        $obj->url_satuan_penggunaan_id = site_url('api/master/satuan/get_all');
        $obj->url_jenis_id = site_url('api/master/jenis_barang/get_all');
        $obj->url_kategori_id = site_url('api/master/kategori_barang/get_all');
        $obj->url_pabrik_id = site_url('api/master/pabrik/get_all');
        $obj->url_kelompok_id = site_url('api/master/kelompok_barang/get_all');
        $output['data'] = $obj;
        echo json_encode($output);
    }  

    public function save() {
        if(!$this->input->is_ajax_request()) exit();

        $obj = $this->_getDataObject();
        if (isset($obj->uid) && $obj->uid != "") {
            $result = $this->main->update($obj);
        } else $result = $this->main->create($obj);

        if(!$result) {
            $this->output->set_status_header(500);
            echo json_encode(['message' => 'Terjadi kesalahan ketika memproses data!']); exit();
        }

        $this->output->set_status_header(200);
        echo json_encode(['message' => 'Berhasil disimpan.']);
    }

    public function update_status() {
        if (!$this->input->is_ajax_request())
          exit();

        $uid = $this->input->post('uid');
        $status = $this->input->post('status');

        $result = $this->main->update_status($uid, $status);
        echo json_encode($result);
    }

    public function delete_photo() {
    
        if (!$this->input->is_ajax_request())
          exit();
        
        if ($this->input->post('id')) {
            $id = $this->input->post('id');

            $obj = $this->db->select('id, nama')
                        ->where('id', $id)
                        ->get($this->table_def_foto)->row();

            $deleted = $this->db->where('id', $id)
                            ->delete($this->table_def_foto);
            if($deleted) {
                unlink($this->ori_dir.'/'.$obj->nama);
                unlink($this->thumb_dir.'/'.$obj->nama);
            }

            $this->output->set_status_header(200)
                ->set_output(json_encode(['data' => $deleted]));
        } else {
            $this->output->set_status_header(500)
                ->set_output(json_encode(['message' => 'error', 'status' => false]));
        }
    }

    public function delete() {
        if (!$this->input->is_ajax_request())
        exit();

        $uid = $this->input->post('uid');
        $result = $this->main->delete($uid);

        if(!$result) {
            $this->output->set_status_header(500);
            echo json_encode(['message' => 'Terjadi kesalahan ketika memproses data!']); exit();
        }

        $this->output->set_status_header(200);
        echo json_encode(['message' => 'Bahan Baku berhasil terhapus!']);
    }

    private function _getDataObject() {
        $browse = $this->input->post('browse') ? : '';
        
        $obj = new stdClass();
        if($browse == "master-harga_jual") {
            $obj->id = $this->input->post('id');
            $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";                
            $obj->harga_penjualan = $this->input->post('harga_penjualan');
            return $obj;
        }

        $obj->id = $this->input->post('id');
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
        $obj->kode = $this->input->post('kode');
        $obj->nama = $this->input->post('nama');
        $obj->bpom = $this->input->post('bpom');
        $obj->ppn = $this->input->post('ppn');
        $obj->merk = $this->input->post('merk');
        $obj->jenis_id = $this->input->post('jenis_id');
        $obj->kategori_id = $this->input->post('kategori_id');
        $obj->pabrik_id = $this->input->post('pabrik_id');
        //$obj->kelompok_id = $this->input->post('kelompok_id');
        $obj->berat_barang = $this->input->post('berat_barang');
        $obj->deskripsi = $this->input->post('deskripsi');
        $obj->deskripsi_information = $this->input->post('deskripsi_information');
        $obj->deskripsi_benefits = $this->input->post('deskripsi_benefits');
        $obj->deskripsi_ingredients = $this->input->post('deskripsi_ingredients');
        $obj->deskripsi_title = $this->input->post('deskripsi_title');
        $obj->harga_pembelian = $this->input->post('harga_pembelian');
        $obj->satuan_pembelian_id = $this->input->post('satuan_pembelian_id');
        $obj->diskon_mou = $this->input->post('diskon_mou');
        $obj->diskon = $this->input->post('diskon');
        $obj->satuan_penggunaan_id = $this->input->post('satuan_penggunaan_id');
        $obj->isi_satuan_penggunaan = $this->input->post('isi_satuan_penggunaan');
        $obj->prefix_kode = $this->input->post('prefix_kode');

        $aFotos = array();
        if(isset($_POST['foto_id'])) {
            $foto_id = $_POST['foto_id'];
            for($i = 0; $i < count($foto_id); $i++) {
                if($foto_id[$i] == 0) {
                    $aFotos[] = $_POST['foto'][$i];
                }
            }
        }
        $obj->fotos = $aFotos;

        $kelompoks = $kelompoks = $this->barang_kelompok->get_all(0, 0, "WHERE barang_id = {$obj->id}");
        
        $aKelompoks = array();
        array_map(function($each) use (&$aKelompoks) {
            $each->data_mode = Data_mode_model::DATA_MODE_DELETE;
            $aKelompoks[$each->kelompok_id] = $each;
            return $each;
        }, $kelompoks['data']);

        if(isset($_POST['kelompok_id'])) {
            $kelompok_ids = $_POST['kelompok_id'];
            for($i = 0; $i < count($kelompok_ids); $i++) {
                $kelompok = new StdClass();
                $kelompok->kelompok_id = $kelompok_ids[$i];
                if (!array_key_exists($kelompok_ids[$i], $aKelompoks)) {
                    $kelompok->data_mode = Data_mode_model::DATA_MODE_ADD;
                    $aKelompoks[uniqid()] = $kelompok;
                } else {
                    $kelompok->data_mode = Data_mode_model::DATA_MODE_EDIT;
                    $aKelompoks[$kelompok_ids[$i]] = $kelompok;
                }
            }
        }
        $obj->kelompoks = $aKelompoks;

        return $obj;
    }

    public function load_data_modal(){
        $aColumns = array('kode', 'nama');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Ordering
        */
        $sOrder = "";
        $aOrders = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if($_POST['columns'][$i]['orderable'] == "true") {
                if($i == $_POST['order'][0]['column']) 
                    $aOrders[] = $aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = "{$this->table_def}.status = 1";
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "{$this->table_def}.bpom = ".$this->session->userdata('bpom');
        }
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
            if($_POST['search']['value'] != "") {
                for ($i = 0; $i < count($aColumns); $i++) {
                    if($_POST['columns'][$i]['searchable'] == "true") {
                        switch ($aColumns[$i]) {
                            case 'kode':
                                $aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                                break;
                            case 'nama':
                                $aLikes[] = "({$this->table_def}.nama LIKE '%".$_POST['search']['value']."%' OR {$this->table_def}.alias LIKE '%".$_POST['search']['value']."%')";
                                break;
                            default:
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $aSelect = array(
            "{$this->table_def}.id",
            "{$this->table_def}.kode",
            "{$this->table_def}.nama",
            "1 isi_satuan",
            "{$this->table_def}.satuan_penggunaan_id satuan_id",
            "CONCAT(spg.nama, ' (', spg.singkatan, ')') satuan",
        );

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $i++;
        }
        $output['data'] = $rows;

        echo json_encode($output);
    }
}
