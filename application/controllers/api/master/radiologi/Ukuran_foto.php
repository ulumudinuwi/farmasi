<?php

class Ukuran_foto extends Admin_Controller
{
    protected $table_def = "m_rad_ukuran_foto";

    public function __construct() {
        parent::__construct();

        $this->load->model('master/radiologi/Ukuran_foto_model');
    }

    public function load_data() {
        $aColumns = array('kode', 'nama', 'action');

        /* 
         * Paging
         */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $iLimit = intval( $_GET['iDisplayLength'] );
            $iOffset = intval( $_GET['iDisplayStart'] );
        }

        /*
         * Ordering
         */
        $sOrder = "";
        $aOrders = array();
        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i <intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true") {
                    switch ($aColumns[intval($_GET['iSortCol_'.$i])]) {
                        case 'kode':
                            $aOrders[] = $this->table_def.".kode ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'nama':
                            $aOrders[] = $this->table_def.".nama ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'action':
                                break;
                    }
                }
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = $this->table_def.".id > 0";

        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
        if (isset($_GET['sSearch'])) {
            for ($i = 0; $i < count($aColumns); $i++) {
                switch ($aColumns[$i]) {
                    case 'kode':
                        $aLikes[] = $this->table_def.".kode LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'nama':
                        $aLikes[] = $this->table_def.".nama LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'action':
                        break;
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->Ukuran_foto_model->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $rows[] = get_object_vars($obj);
        }
        $output['aaData'] = $rows;

        echo json_encode($output);
    }

    public function get_data($uid) {
        if (!$this->input->is_ajax_request())
            exit();

        $output = array();
        if ($uid) {
            $obj = $this->Ukuran_foto_model->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");

            $output['data'] = $obj;
        } else {
            $output['data'] = new stdClass();
        }

        echo json_encode($output);
    }

    public function save() {
        if (!$this->input->is_ajax_request())
            exit();

        $obj = $this->_getDataObject();
        $result = null;
    
        if (isset($obj->id) && $obj->id != "0") { // Update
            $result = $this->Ukuran_foto_model->update($obj);
        } else { // Create
            $result = $this->Ukuran_foto_model->create($obj);
        }

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function delete() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->Ukuran_foto_model->delete($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function restore() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->Ukuran_foto_model->restore($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function update_status() {
        if (!$this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $status = $this->input->post('status');

        if (! $this->Ukuran_foto_model->update_status($uid, $status)) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $uid]));
    }

    private function _getDataObject() {
        $obj = new stdClass();
        $obj->id = $this->input->post('id') && ($this->input->post('id') != "0") ? $this->input->post('id') : 0;
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "0") ? $this->input->post('uid') : 0;
        $obj->kode = $this->input->post('kode');
        $obj->nama = $this->input->post('nama');
        $obj->deskripsi = $this->input->post('deskripsi');

        return $obj;
    }
}