<?php
use Illuminate\Database\Capsule\Manager as DB;
use Ryuna\Datatables;
use Ryuna\Auth;
use Ryuna\Response;
use Ryuna\Validasi;
use Carbon\Carbon;

class Sales extends CI_Controller
{
    protected $table_def = 'm_sales';

    public function __construct() {
        parent::__construct();
        $this->load->library('PasswordHash', array('iteration_count_log2' => 8, 'portable_hashes' => FALSE));
        $this->load->model('master/Sales_model');
        $this->load->model('sales/Target_sales_model');
        
        // if(!$this->input->is_ajax_request()) exit();
    }

    public function data_dummy(){
        $faker = Faker\Factory::create();
        $total_dummy = 100;
        for($i=0; $i < $total_dummy; $i++){
            $sales = new Sales_model();
            $sales->uid = uuid();
            $sales->nama = $faker->name;
            $sales->tgl_lahir = $faker->date;
            $sales->tempat_lahir = $faker->city;
            $sales->jenis_kelamin = 1;
            $sales->no_hp = $faker->phoneNumber;
            $sales->alamat = $faker->streetAddress;  
            $sales->email = $faker->email;
            $sales->no_ktp = "000000000000000";
            $sales->tgl_bergabung = $faker->date;
            $sales->status = "1";
            $sales->save();
        }

        return Response::json([
            'message' => 'Insert '.$total_dummy.' dummy success'
        ],200);
    }
    
    public function list_dt(){
        // Get Request
        $request = $this->input->post();
        
        // Select Columns
        $select = ['uid', 'nama', 'no_hp', 'email', 'alamat', 'status'];
        $where = [
            'is_manager_sales !=' => 1,
            'status =' => $request['status'],
        ];
        // dd($where);
        $this->datatables->select(implode(', ', $select));
        $this->datatables->where($where);
        if(is_manager()){
            $this->datatables->where('manager_sales_id', sales_id());    
        }
        $this->datatables->order_by('status', 'desc');    
        $this->datatables->from($this->table_def);
    
        return Response::datatables($this->datatables->generate());
        
        // // Where Conditions_
        // $filter = [];
        // foreach($select as $column){
        //     if(isset($request[$column]) && !empty($request[$column])) $filter[$column] = $request[$column];
        // }
        // $where  = " ";
        // $total_filter = count($filter);
        // if($total_filter > 0){
        //     $where .= "WHERE ";
        //     $current = 1;
        //     $first = 1;
        //     $last  = $total_filter; 
        //     foreach($filter as $key => $val){
        //         $where .= "{$key} LIKE '%{$val}%'";
        //         if($current != $last) $where .= " AND ";
        //         $current++; 
        //     }
        // }
        // // Query
        // $select = count($select) == 0 ? " * " : implode(',', $select);
        // $query = "SELECT {$select} FROM {$this->table_def} ";
        // if($where != " "){
        //     $query .= $where;
        // }

        // //Order
        // $query .= "";

        // dd(Datatables::of($query)->generate());
    }

    public function store(){
        // dd($this->getRequest());
        $request = $this->getRequest();
        // cek_hela($request);
        $rules = [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'no_ktp' =>'required',
            'tgl_bergabung' => 'required',
            'email' => 'required|email|unique:auth_users,email',
            'username' => 'required|unique:auth_users,username',
            'password'  => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'target_sales'   => 'required|array',
            'target_sales.*'   => 'required',
        ];
        $validasi = Validasi::buat($request,$rules);
        if(!$validasi->status) return Response::json(['message' => $validasi->message],422);
        $role_sales = (Object) DB::table('acl_roles')->where('name','LIKE',"%sales%")->first();
        $role_sales = $role_sales->id;
        
        $tahun = Carbon::now()->format('Y');
        DB::beginTransaction();
        try {
            $sales_id = DB::table('m_sales')->insertGetId([
                'uid' => uuid(),
                'nama' => $request->nama,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date_to_db($request->tgl_lahir),
                'jenis_kelamin' => $request->jenis_kelamin,
                'no_hp' => $request->no_hp,
                'kode_promo' => $request->kode_promo,
                'alamat' => $request->alamat,
                'email' => $request->email,
                'no_ktp' => $request->no_ktp,
                'tgl_bergabung' => date_to_db($request->tgl_bergabung),
                'status' => 1,
                'created_at' => timestamp(),
                'created_by' => auth_id(),
                'updated_at' => timestamp(),
                'updated_by' => auth_id(),
                'manager_sales_id' => sales_id(),
            ]);
            
            $sales_targets = $request->target_sales; 
            $i = 0;
            foreach($sales_targets as $key => $value){
                DB::table('t_target_sales')->insert([
                    'uid' => uuid(),
                    'sales_id' => $sales_id,
                    'target' => $value,
                    'tahun' => $tahun,
                    'bulan' => $request->bulan[$i++],
                    'created_at' => timestamp(),
                    'created_by' => auth_id(),
                    'updated_at' => timestamp(),
                    'updated_by' => auth_id(),
                ]);
            }

            $user_id = DB::table('auth_users')->insertGetId([
                'uid' => uuid(),
                'first_name' => $request->nama,
                'username' => $request->username,
                'email' => $request->email,
                'password' => $this->passwordhash->HashPassword($request->password),
                'sales_id' => $sales_id,
                'role_id' => $role_sales,
                'registered' => timestamp(),
                'created_at' => timestamp(),
                'created_by' => auth_id(),
                'update_at' => timestamp(),
                'update_by' => auth_id(),
            ]);

            DB::table('auth_users_roles')->insert([
                'user_id' => $user_id,
                'role_id' => $role_sales,
                'created_at' => timestamp(),
                'created_by' => auth_id(),
                'update_at'  => timestamp(),
                'update_by'  => auth_id(),
            ]);

            DB::commit();
            // all good
            return Response::json([
                'message' => 'Sales baru ditambahkan.'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'message' => $e->getMessage(),
            ],400);
            // something went wrong
        }
    }

    public function update(){
        $request = $this->getRequest();
        // return Response::json($request,400);
        $rules = [
			'nama' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'no_ktp' =>'required',
            'tgl_bergabung' => 'required',
            'email' => 'required|email|unique:auth_users,email,'.$request->email_default,
            'username' => 'required|unique:auth_users,username'.$request->username_default,
            'password'  => 'min:6',
            'password_confirmation' => 'same:password',
            'target_sales_edit'   => 'required|array',
            'target_sales_edit.*'   => 'required',
        ];
        if(isset($request->target_sales_add)){
            $new = [
                'target_sales_add'   => 'required|array',
                'target_sales_add.*'   => 'required',
            ];
            $rules = array_merge($rules, $new);            
        }
        // return Response::json($rules,400);
        $validasi = Validasi::buat($request,$rules);
        
        if(!$validasi->status) return Response::json(['message' => $validasi->message],422);
        $role_sales = (Object) DB::table('acl_roles')->where('name','LIKE',"%sales%")->first();
        $role_sales = $role_sales->id;

        $sales_id = DB::table('m_sales')->where('uid', $request->uid)->first()['id'];
        $user_id = DB::table('auth_users')->where('sales_id', $sales_id)->first()['id'];

        $tahun = Carbon::now()->format('Y');
        DB::beginTransaction();
        try {
            $sales = DB::table('m_sales')->where('uid', $request->uid)->update([
                'nama' => $request->nama,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date_to_db($request->tgl_lahir),
                'jenis_kelamin' => $request->jenis_kelamin,
                'no_hp' => $request->no_hp,
                'alamat' => $request->alamat,
                'email' => $request->email,
                'kode_promo' => $request->kode_promo,
                'no_ktp' => $request->no_ktp,
                'tgl_bergabung' => date_to_db($request->tgl_bergabung),
                'updated_at' => timestamp(),
                'updated_by' => auth_id(),
            ]);
            
            $sales_targets = $request->target_sales_edit; 
            foreach($sales_targets as $key => $value){
                $key = explode('_',$key);
                $t_s = Target_sales_model::where('tahun',$key[0])->where('bulan',$key[1])->where('sales_id',$sales_id)->update([
                    'target' => $value,
                    'updated_at' => timestamp(),
                    'updated_by' => auth_id(),
                ]);
                // if($t_s){
                //     $t_s->target = $value;
                //     $t_s->updated_at = timestamp();
                //     $t_s->updated_by = auth_id();
                //     $t_s->save();
                // }
            }
            if(isset($request->target_sales_add) && count($request->target_sales_add) > 0){
                $i = 0;
                // return Response::json($request['uid'],400);
                foreach($request->target_sales_add as $key => $value){
                    DB::table('t_target_sales')->insert([
                        'uid' => uuid(),
                        'sales_id' => $sales_id,
                        'target' => $value,
                        'tahun' => $tahun,
                        'bulan' => $request->bulan[$i++],
                        'created_at' => timestamp(),
                        'created_by' => auth_id(),
                        'updated_at' => timestamp(),
                        'updated_by' => auth_id(),
                    ]);
                }    
            }

            $update_user = [
                'first_name' => $request->nama,
                'username' => $request->username,
                'email' => $request->email,
                'password' => $this->passwordhash->HashPassword($request->password),
                'update_at' => timestamp(),
                'update_by' => auth_id(),
            ];
            if(!$request->password) unset($update_user['password']);
            DB::table('auth_users')->where('sales_id', $sales_id)->update($update_user);

            DB::table('auth_users_roles')->where('user_id',$user_id)->update([
                'user_id' => $user_id,
                'role_id' => $role_sales,
                'created_at' => timestamp(),
                'created_by' => auth_id(),
                'update_at'  => timestamp(),
                'update_by'  => auth_id(),
            ]);

            DB::commit();
            // all good
            return Response::json([
                'message' => 'Sales diperbaharui.'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json($e->getMessage(),400);
            // something went wrong
        }
    }

    public function show(){
        
    }

    public function switch_status(){
        $request = $this->getRequest();
        $uid = $request->uid;

        $sales = Sales_model::where('uid', $uid)->first();
        if(!$sales) {
            return Response::json([
                'message' => 'User tidak ditemukan.',
            ],400);
        }
        $sales->status = $sales->status == 1 ? 0 : 1;
        if($sales->save()){
            $type = $sales->status == 1 ? 'aktifkan' : 'nonaktifkan';
            return Response::json([
                'message' => 'Sales Di'.$type,
            ],200);
        }
        return Response::json([
            'message' => 'Gagal mengubah status.',
        ],400);
    }
    
    public function getRequest()
    {
        $request = $_POST + $_FILES + $_GET;
        $request = (Object) $request;
        return $request;
    }

    public function list_dt_transaksi()
    {
        // if (!$this->input->is_ajax_request()) {
        //     exit('No direct script access allowed');
        // }
        $sales_id = $this->input->post('sales_id') ? : sales_id();
        $field = [
            't_pos_detail.no_fbp',
            't_pos.tanggal_transaksi',
            'm_dokter_member.nama',
            'm_barang.nama',
            't_pos_detail.qty',
            't_pos_detail.harga',
            't_pos_detail.grand_total',
            't_pos_detail.diskon',
            't_pos_detail.total',
        ];

        $sWhere = [
            't_pos_detail.sales_id' => $sales_id,
            't_pos_detail.rejected_at' => null,
            't_pos.is_approved' => 1,
            't_pos.status_lunas' => 1,
        ];

        return Response::json(Sales_model::list_dt_transaksi($field, $sWhere), 200);
    }

    public function list_sales($status){
        // Get Request
        $select = ['id', 'uid', 'nama', 'no_hp', 'email', 'alamat', 'status'];
        $where = [
            'is_manager_sales !=' => 1,
            'status =' => $status,
        ];
        // dd($where);
        $this->datatables->select(implode(', ', $select));
        $this->datatables->where($where);
        if(is_manager()){
            $this->datatables->where('manager_sales_id', sales_id());    
        }
        $this->datatables->order_by('status', 'desc');    
        $this->datatables->from($this->table_def);
    
        return Response::datatables($this->datatables->generate());
        
    }
}