<?php
require_once(APPPATH.'events/EventProcess.php');
use Ryuna\Response;

class Subscribe extends CI_Controller{
    public function stream(){
        session_write_close(); # !Important
        $request = request_handler();
        $event_name = isset($request->event_name) ? $request->event_name : '';
        $uid  = isset($request->uid) ? $request->uid : '';

        if($event_name && $uid){
            // $sse = new Sse\SSE(); //create a libSSE instance
            // $sse->addEventListener($event_name, new EventProcess($uid));//register your event handler
            // $sse->start();//start the event loop
            $sse = new EventProcess($uid, $event_name);
            $sse->start();
        }

        // return Response::json([
        //     'status' => '400',
        //     'message' => 'Param Cannot Be Null',
        //     'param_key' => [
        //         'uid' => 'Base64encode from name table', 
        //         'event_name' => 'Event Name', 
        //     ],
        // ], 400);
    }
}