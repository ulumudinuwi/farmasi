<?php
use Illuminate\Database\Capsule\Manager as DB;
use Ryuna\Datatables;
use Ryuna\Auth;
use Ryuna\Response;
use Ryuna\Validasi;
use Carbon\Carbon;
use Mpdf\Mpdf;

class Cicilan extends CI_Controller
{
    protected $table_barang = 'm_barang';
    protected $table_stock_barang = 't_gudang_farmasi_stock';
    protected $table_dokter = 'm_dokter_member';
    protected $table_sales = 'm_sales';

    public function __construct() {
        parent::__construct();
        $this->load->model('master/Dokter_model');
        $this->load->model('master/Sales_model');
        $this->load->model('sales/Pos_model');
        $this->load->model('sales/Pos_detail_model');
        $this->load->model('sales/Pos_pembayaran_model');
        // if(!$this->input->is_ajax_request()) exit();
    }
    
    public function list_pembayaran_dt(){
        $field = [
            Pos_pembayaran_model::$table_pos_pembayaran.'.created_at',
            Pos_pembayaran_model::$table_dokter.'.no_member', 
            Pos_pembayaran_model::$table_pos_pembayaran.'.no_kuitansi',
            Pos_pembayaran_model::$table_dokter.'.nama',
            Pos_pembayaran_model::$table_pos.'.no_invoice',
        ];
        return Response::json(Pos_pembayaran_model::pembayaran_datatables_template($field), 200);
    }

    public function list_invoice_select2()
    {
        return Response::json(Pos_pembayaran_model::list_invoice_page(),200);
    }

    public function get_cicilan_ke(){
        $request = request_handler();
        $get_id_pos = Pos_model::getID($request->uid);
        $get_cicilan = cicilan_ke($get_id_pos);
        return Response::json($get_cicilan, 200);
    }

    public function pembayaran_cicilan(){
        $request = request_handler();

        $object = new stdClass;
        $object->pos = json_decode($request->pos);
        $object->pembayaran = json_decode($request->pembayaran);
        $object->dokter = json_decode($request->member);
        $object->sales = json_decode($request->sales);
        $proc = Pos_pembayaran_model::pembayaran_cicilan($object);

        if($proc){
            return Response::json([
                'message' => 'Pembayaran berhasil dilakukan',
            ],200);
        }

        return Response::json([
            'message' => 'Terjadi kesalahan',
        ],400);
    }
}