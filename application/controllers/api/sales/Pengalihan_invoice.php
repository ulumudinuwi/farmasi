<?php
use Illuminate\Database\Capsule\Manager as DB;
use Ryuna\Datatables;
use Ryuna\Auth;
use Ryuna\Response;
use Ryuna\Validasi;
use Carbon\Carbon;
use Mpdf\Mpdf;

class Pengalihan_invoice extends CI_Controller
{
    protected $table_barang = 'm_barang';
    protected $table_stock_barang = 't_gudang_farmasi_stock';
    protected $table_dokter = 'm_dokter_member';
    protected $table_sales = 'm_sales';

    public function __construct() {
        parent::__construct();
        $this->load->model('master/Dokter_model');
        $this->load->model('master/Sales_model');
        $this->load->model('sales/Pos_model');
        $this->load->model('sales/Pengalihan_invoice_model');
        $this->load->model('sales/Pos_detail_model');
        $this->load->model('sales/Pos_pembayaran_model');
        // if(!$this->input->is_ajax_request()) exit();
    }

    // Get Request
    public function get_all_barang()
    {
        $results = Pengalihan_invoice_model::get_list_barang();
        return Response::json($results,200);
    }

    public function get_member()
    {
        $results = Pengalihan_invoice_model::get_list_member();
        return Response::json($results,200);
    }

    public function search_member()
    {
        $results = Pengalihan_invoice_model::find_member();
        return Response::json($results,200);
    }

    public function save_temporary()
    {
        $request = $this->getRequest();
        
        $proc = Pengalihan_invoice_model::saveDraf($request);
        
        if($proc){
            return Response::json([
                'message' => 'Berhasil disimpan ke Draft',
            ],200);
        }
        return Response::json([
            'message' => 'Terjadi kesalahan',
        ],400);
    }

    public function proccess_order()
    {
        $request = $this->getRequest();
        
        $object = new stdClass;
        $object->status = $request->status;
        $object->json_original = $request;
        
        //return Response::json($request,400);
        $proc = Pengalihan_invoice_model::processOrder($request);
        if($proc){
            return Response::json([
                'message' => 'Transaksi berhasil dilakukan',
            ],200);
        }

        return Response::json([
            'message' => 'Terjadi kesalahan',
        ],400);
    }
    
    public function update_proccess_order()
    {
        $request = $this->getRequest();
        $transaksi = json_decode($request->transaksi);
        
        $object = new stdClass;
        $object->transaksi = $transaksi;
        $object->status_pembayaran = $request->status_pembayaran;
        $object->uid = $request->uid;
        $object->uid_detail = $request->uid_detail;
        $object->grand_total_harga = $request->grand_total_harga;
        $object->json_original = $request;
        
        //return Response::json($request,400);
        $proc = Pengalihan_invoice_model::processOrderUpdate($object);
        if($proc){
            return Response::json([
                'message' => 'Transaksi berhasil dilakukan',
            ],200);
        }

        return Response::json([
            'message' => 'Terjadi kesalahan',
        ],400);
    }
    
    public function list_member_select2()
    {
        return Response::json(Pengalihan_invoice_model::list_member_page(),200);
    }

    private function getRequest(){
        $request = $_POST + $_FILES + $_GET;
        return (Object) $request;
    }

    public function list_transaksi_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $sWhere = [];
        $field = [
            Pengalihan_invoice_model::$table_pos.'.tanggal_transaksi',
            Pengalihan_invoice_model::$table_pos.'.no_invoice',
            Pengalihan_invoice_model::$table_dokter.'.no_member', 
            Pengalihan_invoice_model::$table_dokter.'.nama',
            Pengalihan_invoice_model::$table_pos.'.status_lunas',
            Pengalihan_invoice_model::$table_pos.'.json_keterangan',
        ];
        $sWhere = [
            Pengalihan_invoice_model::$table_pos.'.status_pengalihan' => 1,
            Pengalihan_invoice_model::$table_sales.'.manager_sales_id' => sales_id(),
        ];
        if (Auth::user()->bpom == 1) {
            $sWhere[Pos_model::$table_pos.".bpom"]  = Auth::user()->bpom;
        }
        return Response::json(Pengalihan_invoice_model::pos_datatables($field,$sWhere,TRUE), 200);
    }

    public function list_approval_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $request = (Object) $_REQUEST;
        
        $sWhere = [];

        $field = [
            Pengalihan_invoice_model::$table_pos.'.no_invoice',
            Pengalihan_invoice_model::$table_dokter.'.no_member', 
            Pengalihan_invoice_model::$table_dokter.'.nama',
            Pengalihan_invoice_model::$table_pos.'.status_lunas',
            Pengalihan_invoice_model::$table_pos.'.verifikasi_transkasi_manager',
        ];

        if(isset($request) && $request->list){
            switch ($request->list) {
                case 'list':
                    $field = [
                        Pengalihan_invoice_model::$table_pos.'.no_invoice',
                        Pengalihan_invoice_model::$table_dokter.'.no_member', 
                        Pengalihan_invoice_model::$table_dokter.'.nama',
                        Pengalihan_invoice_model::$table_pos.'.is_approved',
                        Pengalihan_invoice_model::$table_pos.'.status_lunas',
                        Pengalihan_invoice_model::$table_pos.'.verifikasi_pos_manager',
                    ];

                    $sWhere = [
                        Pengalihan_invoice_model::$table_pengalihan_invoice.'.status' => 1,
                    ];
                    if (Auth::user()->bpom == 1) {
                        $sWhere[Pos_model::$table_pos.".bpom"]  = Auth::user()->bpom;
                    }
                break;
                
                case 'history':
                    $field = [
                        Pengalihan_invoice_model::$table_pos.'.no_invoice',
                        Pengalihan_invoice_model::$table_dokter.'.no_member', 
                        Pengalihan_invoice_model::$table_dokter.'.nama',
                        Pengalihan_invoice_model::$table_pos.'.is_approved',
                        Pengalihan_invoice_model::$table_pos.'.status_lunas',
                        Pengalihan_invoice_model::$table_pos.'.verifikasi_diskon_manager',
                    ];

                    $sWhere = [
                        Pengalihan_invoice_model::$table_pengalihan_invoice.'.status' => 2,
                    ];
                    if (Auth::user()->bpom == 1) {
                        $sWhere[Pos_model::$table_pos.".bpom"]  = Auth::user()->bpom;
                    }
                break;
                
                default:
                    $field = [
                        Pengalihan_invoice_model::$table_pos.'.no_invoice',
                        Pengalihan_invoice_model::$table_dokter.'.no_member', 
                        Pengalihan_invoice_model::$table_dokter.'.nama',
                        Pengalihan_invoice_model::$table_pos.'.status_lunas',
                    ];
                break;
            }
        }

        return Response::json(Pengalihan_invoice_model::pos_datatables_template($field,$sWhere), 200);
    }
    

    public function approve_pengalihan(){
        $request = request_handler();
        $proc = Pengalihan_invoice_model::approve_pengalihan($request->uid, $request->new_sales_id);

        if($proc) return Response::json(['message' => 'Approval Berhasil dilakukan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function reject_pengalihan(){
        $request = request_handler();
        $proc = Pengalihan_invoice_model::reject_pengalihan($request->uid, $request->new_sales_id);

        if($proc) return Response::json(['message' => 'Pembatalan Berhasil dilakukan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function reject_pos_manager(){
        $request = request_handler();
        $proc = Pengalihan_invoice_model::reject_pos_manager($request->uid, $request->desc);

        if($proc) return Response::json(['message' => 'Transkasi dibatalkan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function approve_diskon_manager(){
        $request = request_handler();
        $proc = Pengalihan_invoice_model::approve_diskon_manager($request->uid);

        if($proc) return Response::json(['message' => 'Approval Berhasil dilakukan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function reject_diskon_manager(){
        $request = request_handler();
        $proc = Pengalihan_invoice_model::reject_diskon_manager($request->uid, $request->desc);

        if($proc) return Response::json(['message' => 'Transkasi dibatalkan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function approve_diskon_direktur(){
        $request = request_handler();
        $proc = Pengalihan_invoice_model::approve_diskon_direktur($request->uid);

        if($proc) return Response::json(['message' => 'Approval Berhasil dilakukan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function reject_diskon_direktur(){
        $request = request_handler();
        $proc = Pengalihan_invoice_model::reject_diskon_direktur($request->uid, $request->desc);

        if($proc) return Response::json(['message' => 'Transkasi dibatalkan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function rejected_description(){
        $request = request_handler();
        if($request && isset($request->uid)){
            $pos_id = Pengalihan_invoice_model::getID($request->uid,'uid');
            $_get = Pengalihan_invoice_model::getHistoryApproval('reject', $pos_id);
            return Response::json($_get,200);
        }
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function get_detail_pos(){
        $request = request_handler();
        if($request && isset($request->uid)){
            $found = Pengalihan_invoice_model::where('uid',$request->uid)->first();
            if($found){
                $_get = Pengalihan_invoice_model::getTransaksiDetail($request->uid);
                return Response::json($_get,200);
            }
            return Response::json(['message' => 'Data tidak ditemukan'], 400);
        }
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function print_invoice(){
        $request = request_handler();
        $data = Pengalihan_invoice_model::findByUID($request->uid);
        $data['no_rek'] =  $this->db->query("SELECT * FROM m_no_rekening ORDER BY id")->row();;
        // dd($data);
        $html = $this->load->view('sales/print/pdf/invoice', $data, true);
        // Margins
        $ml = 10;
        $mr = 10;
        $mt = 10;
        $mb = 10;

        # Create PDF
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
			'format' => 'A4-L',
			'margin_left' => $ml,
			'margin_right' => $mr,
			'margin_top' => $mt,
			'margin_bottom' => $mb,
        ]);
        $mpdf->WriteHTML($html);
        // $mpdf->SetHTMLFooter($htmlFooter);
        $name = "Invoice-".$data['detail_transaksi']['no_invoice'].'.pdf';
        $mpdf->Output($name, 'I');
    }

    public function print_tanda_terima(){
        $request = request_handler();
        $data = Pengalihan_invoice_model::findByUID($request->uid);
        $data['no_rek'] =  $this->db->query("SELECT * FROM m_no_rekening ORDER BY id")->row();;
        // dd($data);
        $html = $this->load->view('sales/print/pdf/tanda_terima', $data, true);
        // Margins
        $ml = 5;
        $mr = 5;
        $mt = 10;
        $mb = 10;

        # Create PDF
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_left' => $ml,
            'margin_right' => $mr,
            'margin_top' => $mt,
            'margin_bottom' => $mb,
        ]);
        $mpdf->WriteHTML($html);
        // $mpdf->SetHTMLFooter($htmlFooter);
        $name = "Invoice-".$data['detail_transaksi']['no_invoice'].'.pdf';
        $mpdf->Output($name, 'I');
    }

    public function print_kwitansi(){
        $request = request_handler();
        $data = Pengalihan_invoice_model::findByUID($request->uid);
        $data['param']['nominal'] = $request->nominal; 
        $data['param']['ket'] = $request->ket;
        $data['param']['no_kuitansi'] = $request->no_kuitansi;
        $data['param']['tanggal_transaksi'] = $request->tanggal;
        $data['param']['sisa_pembayaran'] = $request->sisa_pembayaran;

        // dd($data);
        $html = $this->load->view('sales/print/pdf/kuitansi', $data, true);
        // Margins
        $ml = 5;
        $mr = 5;
        $mt = 10;
        $mb = 10;

        # Create PDF
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
			'format' => 'A4',
			'margin_left' => $ml,
			'margin_right' => $mr,
			'margin_top' => $mt,
			'margin_bottom' => $mb,
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->Output('Kwitansi.pdf', "I");
    }
}