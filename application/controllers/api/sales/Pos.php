<?php
use Illuminate\Database\Capsule\Manager as DB;
use Ryuna\Datatables;
use Ryuna\Auth;
use Ryuna\Response;
use Ryuna\Validasi;
use Carbon\Carbon;
use Mpdf\Mpdf;

class Pos extends CI_Controller
{
    protected $table_barang = 'm_barang';
    protected $table_stock_barang = 't_gudang_farmasi_stock';
    protected $table_dokter = 'm_dokter_member';
    protected $table_sales = 'm_sales';

    public function __construct() {
        parent::__construct();
        $this->load->model('master/Dokter_model');
        $this->load->model('master/Sales_model');
        $this->load->model('sales/Pos_model');
        $this->load->model('sales/Pos_detail_model');
        $this->load->model('sales/Pos_pembayaran_model');
        // if(!$this->input->is_ajax_request()) exit();
    }

    // Get Request
    public function get_all_barang()
    {
        $results = Pos_model::get_list_barang();
        return Response::json($results,200);
    }

    public function get_member()
    {
        $results = Pos_model::get_list_member();
        return Response::json($results,200);
    }

    public function search_member()
    {
        $results = Pos_model::find_member();
        return Response::json($results,200);
    }

    public function save_temporary()
    {
        $request = $this->getRequest();
        
        $proc = Pos_model::saveDraf($request);
        
        if($proc){
            return Response::json([
                'message' => 'Berhasil disimpan ke Draft',
            ],200);
        }
        return Response::json([
            'message' => 'Terjadi kesalahan',
        ],400);
    }

    public function proccess_order()
    {
        $request = $this->getRequest();
        $cart = json_decode($request->cart);
        $transaksi = json_decode($request->transaksi);
        $dokter = Dokter_model::findByUID($cart->member_uid);
        $sales = Sales_model::findByUID($cart->sales_uid);
        
        $object = new stdClass;
        $object->cart = $cart;
        $object->transaksi = $transaksi;
        $object->status_pembayaran = $request->status_pembayaran;
        $object->dokter = $dokter;
        $object->sales = $sales;
        $object->json_original = $request;
        
        //return Response::json($object,400);
        $proc = Pos_model::processOrder($object);
        if($proc){
            return Response::json([
                'message' => 'Transaksi berhasil dilakukan',
            ],200);
        }

        return Response::json([
            'message' => 'Terjadi kesalahan',
        ],400);
    }
    
    public function update_proccess_order()
    {
        $request = $this->getRequest();
        $transaksi = json_decode($request->transaksi);
        
        $object = new stdClass;
        $object->transaksi = $transaksi;
        $object->status_pembayaran = $request->status_pembayaran;
        $object->uid = $request->uid;
        //$object->uid_detail = $request->uid_detail;
        $object->grand_total_harga = $request->grand_total_harga;
        $object->tgl_jatuh_tempo = $request->tgl_jatuh_tempo;
        $object->json_original = $request;
        
        //return Response::json($request,400);
        $proc = Pos_model::processOrderUpdate($object);
        if($proc){
            return Response::json([
                'message' => 'Transaksi berhasil dilakukan',
            ],200);
        }

        return Response::json([
            'message' => 'Terjadi kesalahan',
        ],400);
    }
    
    public function list_member_select2()
    {
        $getData = Pos_model::list_member_page();
        foreach ($getData['results'] as $i => $row) {
            $getPlafon = $this->db->query("SELECT SUM(sisa_pembayaran) as utang_plafon FROM t_pos WHERE member_id = ".$row['id']." AND piutang = 1 ORDER BY id")->row();
            $getData['results'][$i]['plafon_dokter'] = $row['plafon_dokter'] - $getPlafon->utang_plafon;
        }
        return Response::json($getData,200);
    }

    private function getRequest(){
        $request = $_POST + $_FILES + $_GET;
        return (Object) $request;
    }

    public function list_transaksi_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $field = [
            Pos_model::$table_pos.'.tanggal_transaksi',
            Pos_model::$table_pos.'.no_invoice',
            Pos_model::$table_dokter.'.no_member', 
            Pos_model::$table_dokter.'.nama',
            Pos_model::$table_pos.'.status_lunas',
            Pos_model::$table_pos.'.json_keterangan',
        ];
        return Response::json(Pos_model::pos_datatables_template($field,[],TRUE), 200);
    }

    public function list_approval_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $request = (Object) $_REQUEST;
        
        $sWhere = [];

        $field = [
            Pos_model::$table_pos.'.no_invoice',
            Pos_model::$table_dokter.'.no_member', 
            Pos_model::$table_dokter.'.nama',
            Pos_model::$table_pos.'.status_lunas',
            Pos_model::$table_pos.'.verifikasi_transkasi_manager',
        ];

        if(isset($request) && $request->list){
            switch ($request->list) {
                case 'pos_manager':
                    $field = [
                        Pos_model::$table_pos.'.no_invoice',
                        Pos_model::$table_dokter.'.no_member', 
                        Pos_model::$table_dokter.'.nama',
                        Pos_model::$table_pos.'.is_approved',
                        Pos_model::$table_pos.'.status_lunas',
                        Pos_model::$table_pos.'.verifikasi_pos_manager',
                    ];

                    $sWhere = [
                        Pos_model::$table_pos.'.is_rejected' => 0,
                        Pos_model::$table_pos.'.is_approved' => 0,
                        Pos_model::$table_pos.'.verifikasi_pos_manager' => 0,
                    ];
                    if(is_manager()){    
                        $sWhere[Pos_model::$table_sales.'.manager_sales_id'] = sales_id();
                    }
                    if (Auth::user()->bpom == 1) {
                        $sWhere[Pos_model::$table_pos.".bpom"]  = Auth::user()->bpom;
                    }
                break;
                
                case 'diskon_manager':
                    $field = [
                        Pos_model::$table_pos.'.no_invoice',
                        Pos_model::$table_dokter.'.no_member', 
                        Pos_model::$table_dokter.'.nama',
                        Pos_model::$table_pos.'.is_approved',
                        Pos_model::$table_pos.'.status_lunas',
                        Pos_model::$table_pos.'.verifikasi_diskon_manager',
                    ];

                    $sWhere = [
                        Pos_model::$table_pos.'.is_rejected' => 0,
                        Pos_model::$table_pos.'.is_approved' => 0,
                        Pos_model::$table_pos.'.verifikasi_diskon_manager' => 0,
                    ];
                    if(is_manager()){    
                        $sWhere[Pos_model::$table_sales.'.manager_sales_id'] = sales_id();
                    }
                    if (Auth::user()->bpom == 1) {
                        $sWhere[Pos_model::$table_pos.".bpom"]  = Auth::user()->bpom;
                    }
                break;

                case 'diskon_direktur':
                    $field = [
                        Pos_model::$table_pos.'.no_invoice',
                        Pos_model::$table_dokter.'.no_member', 
                        Pos_model::$table_dokter.'.nama',
                        Pos_model::$table_pos.'.is_approved',
                        Pos_model::$table_pos.'.status_lunas',
                        Pos_model::$table_pos.'.verifikasi_diskon_direktur',
                    ];

                    $sWhere = [
                        Pos_model::$table_pos.'.is_rejected' => 0,
                        Pos_model::$table_pos.'.is_approved' => 0,
                        Pos_model::$table_pos.'.verifikasi_diskon_direktur' => 0,
                    ];
                    if (Auth::user()->bpom == 1) {
                        $sWhere[Pos_model::$table_pos.".bpom"]  = Auth::user()->bpom;
                    }
                break;
                
                default:
                    $field = [
                        Pos_model::$table_pos.'.no_invoice',
                        Pos_model::$table_dokter.'.no_member', 
                        Pos_model::$table_dokter.'.nama',
                        Pos_model::$table_pos.'.status_lunas',
                    ];
                break;
            }
        }
        $sWhere[Pos_model::$table_pos.'.flag'] = 1; // Bukan E-Commerce
        return Response::json(Pos_model::pos_datatables_template($field,$sWhere), 200);
    }
    

    public function approve_pos_manager(){
        $request = request_handler();
        $proc = Pos_model::approve_pos_manager($request->uid);

        if($proc) return Response::json(['message' => 'Approval Berhasil dilakukan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function reject_pos_manager(){
        $request = request_handler();
        $proc = Pos_model::reject_pos_manager($request->uid, $request->desc);

        if($proc) return Response::json(['message' => 'Transkasi dibatalkan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function approve_diskon_manager(){
        $request = request_handler();
        $proc = Pos_model::approve_diskon_manager($request->uid);

        if($proc) return Response::json(['message' => 'Approval Berhasil dilakukan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function reject_diskon_manager(){
        $request = request_handler();
        $proc = Pos_model::reject_diskon_manager($request->uid, $request->desc);

        if($proc) return Response::json(['message' => 'Transkasi dibatalkan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function approve_diskon_direktur(){
        $request = request_handler();
        $proc = Pos_model::approve_diskon_direktur($request->uid);

        if($proc) return Response::json(['message' => 'Approval Berhasil dilakukan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function reject_diskon_direktur(){
        $request = request_handler();
        $proc = Pos_model::reject_diskon_direktur($request->uid, $request->desc);

        if($proc) return Response::json(['message' => 'Transkasi dibatalkan'], 200);
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function rejected_description(){
        $request = request_handler();
        if($request && isset($request->uid)){
            $pos_id = Pos_model::getID($request->uid,'uid');
            $_get = Pos_model::getHistoryApproval('reject', $pos_id);
            return Response::json($_get,200);
        }
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function get_detail_pos(){
        $request = request_handler();
        if($request && isset($request->uid)){
            $found = Pos_model::where('uid',$request->uid)->first();
            if($found){
                $_get = Pos_model::getTransaksiDetail($request->uid);
                return Response::json($_get,200);
            }
            return Response::json(['message' => 'Data tidak ditemukan'], 400);
        }
        return Response::json(['message' => 'Terjadi Kesalahan'], 400);
    }

    public function print_invoice(){
        $request = request_handler();
        $data = Pos_model::findByUID($request->uid);
        if ($data['detail_transaksi']['bpom'] == 1) {
            $data['no_rek'] =  $this->db->query("SELECT * FROM m_no_rekening WHERE id = 2 ORDER BY id")->row();
        }else{
            $data['no_rek'] =  $this->db->query("SELECT * FROM m_no_rekening ORDER BY id")->row();
        }
        // dd($data);
        $html = $this->load->view('sales/print/pdf/invoice', $data, true);
        // Margins
        //$ml = 10;
        //$mr = 10;
        //$mt = 10;
        //$mb = 10;
        $ml = 15;
        $mr = 10;
        $mt = 15;
        $mb = 0;

        # Create PDF
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
			//'format' => 'A4-L',
            'format' => [222.0, 150.0],
			'margin_left' => $ml,
			'margin_right' => $mr,
			'margin_top' => $mt,
			'margin_bottom' => $mb,
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->SetHTMLFooter('
            <table width="100%" style="position: initial; top: 10px;">
                <tr>
                    <td colspan="5"></td>
                    <td colspan="1" valign="top"><p>&nbsp;HORMAT KAMI</p></td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td colspan="1" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td colspan="1" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"> <p style="font-size: 12px;" class="text-bold">NB: - <u>Pembayaran hanya melalui '.$data['no_rek']->nama_bank.' 
                    No. Rek : '.$data['no_rek']->no_rekening.' Atas Nama '.$data['no_rek']->nama_pemilik.'.</u></p></td>
                    <td colspan="1" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"><p style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;- Barang yang telah dibeli tidak dapat dikembalikan / ditukar.</p></td>
                    <td colspan="1" valign="top"><p>(&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;)</p></td>
                </tr>
            </table>
            ');
        $name = "Invoice-".$data['detail_transaksi']['no_invoice'].'.pdf';
        $mpdf->Output($name, 'I');
    }

    public function print_tanda_terima(){
        $request = request_handler();
        $data = Pos_model::findByUID($request->uid);
        $data['no_rek'] =  $this->db->query("SELECT * FROM m_no_rekening ORDER BY id")->row();
        // dd($data);
        $html = $this->load->view('sales/print/pdf/tanda_terima', $data, true);

        $ml = 15;
        $mr = 10;
        $mt = 15;
        $mb = 0;

        # Create PDF
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            //'format' => 'A4-L',
            'format' => [222.0, 150.0],
            'margin_left' => $ml,
            'margin_right' => $mr,
            'margin_top' => $mt,
            'margin_bottom' => $mb,
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->SetHTMLFooter('
        <table style="width: 100%; margin-left: 100px; margin-right: 100px;">
            <tr>
                <td colspan="3"><hr></td>
            </tr>  
            <tr>
                <td style="width: 50%">
                    <p>&ensp;DITERIMA DENGAN BAIK</p>
                    <br><br><br><br><br>
                    <p>(&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;)</p>
                </td>
                <td style="width: 50%" align="right">
                    <p>HORMAT KAMI&ensp;</p>
                    <br><br><br><br><br>
                    <p>(&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;)</p>
                </td>
            </tr>
        </table>');
        $name = "Invoice-".$data['detail_transaksi']['no_invoice'].'.pdf';
        $mpdf->Output($name, 'I');
    }

    public function print_kwitansi(){
        $request = request_handler();
        $data = Pos_model::findByUID($request->uid);
        $data['param']['nominal'] = $request->nominal; 
        $data['param']['ket'] = $request->ket;
        $data['param']['no_kuitansi'] = $request->no_kuitansi;
        $data['param']['tanggal_transaksi'] = $request->tanggal;
        $data['param']['sisa_pembayaran'] = $request->sisa_pembayaran;

        // dd($data);
        $html = $this->load->view('sales/print/pdf/kuitansi', $data, true);
        // Margins
        $ml = 5;
        $mr = 5;
        $mt = 10;
        $mb = 10;

        # Create PDF
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
			'format' => 'A4',
			'margin_left' => $ml,
			'margin_right' => $mr,
			'margin_top' => $mt,
			'margin_bottom' => $mb,
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->Output('Kwitansi.pdf', "I");
    }
}