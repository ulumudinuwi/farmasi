<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Po extends CI_Controller 
{
	protected $table_def = "t_gudang_farmasi_po";
	protected $table_def_detail = "t_gudang_farmasi_po_detail";
	protected $table_def_po_biaya_lain = "t_gudang_farmasi_po_biaya_lain";
	protected $table_def_stock = "t_gudang_farmasi_stock";
	protected $table_def_barang = "m_barang";
	protected $table_def_pabrik = "m_pabrik";
	
	function __construct() {
		parent::__construct();
		$this->lang->load('kurs');
		$this->lang->load('barang');

		$this->load->model('gudang_farmasi/Po_model', 'main');
		$this->load->model('gudang_farmasi/Po_detail_model', 'main_detail');
		$this->load->model('gudang_farmasi/Po_biaya_lain_model', 'biaya_lain');
		$this->load->model('gudang_farmasi/Pesan_barang_model', 'pesan_main');
	}

	/**
	 * Load data
	 */
	public function load_data_draft(){
		$status  = $_POST['status'];

		$aColumns = array('pabrik_id', '');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
				$iLimit = intval( $_POST['length'] );
				$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
				$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
				$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aLikes = array();
		if($_POST['search']['value'] != "") {
				for ($i = 0; $i < count($aColumns); $i++) {
						if($_POST['columns'][$i]['searchable'] == "true") {
								switch ($aColumns[$i]) {
									case 'pabrik_id':
										$aLikes[] = "{$this->table_def_pabrik}.nama LIKE '%".$_POST['search']['value']."%'";
										break;
								}
						}
				}
		}

		if (count($aLikes) > 0) {
				$sLike = "(".implode(' OR ', $aLikes).")";
				$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "AND ".$sLike;
		}

		$sStatus = "";
		if($_POST['status'] != "") $sStatus = $_POST['status'];

		$list = $this->pesan_main->datatable($iLimit, $iOffset, $sWhere, $sOrder, $sStatus);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
				"draw" => intval($_POST['draw']),
				"recordsTotal" => $iTotal,
				"recordsFiltered" => $iFilteredTotal,
				"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
				$data = get_object_vars($obj);
				$data['no'] = ($i+1);
				$rows[] = $data;
				$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function load_data(){
		$tanggal_dari  = $this->input->post('tanggal_dari') ? : "";
		$tanggal_sampai  = $this->input->post('tanggal_sampai') ? : "";
		$sifat  = $this->input->post('sifat') ? : "";
		$pabrik_id  = $this->input->post('pabrik_id') ? : "";
		$status  = $this->input->post('status') ? : "";

		$aColumns = array('kode', 'tanggal', 'pabrik_id', 'sifat', 'status', '');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
				$iLimit = intval( $_POST['length'] );
				$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
				$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
				$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
		if($sifat != "") $aWheres[] = "{$this->table_def}.sifat = '{$sifat}'";
		if($pabrik_id != "") $aWheres[] = "{$this->table_def}.pabrik_id = ".base64_decode($pabrik_id);
		if($status != "") $aWheres[] = "{$this->table_def}.status = {$status}";
		if (count($aWheres) > 0) {
				$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
				$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
				$sLike = "(".implode(' OR ', $aLikes).")";
				$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.kode",
			"{$this->table_def}.tanggal",
			"{$this->table_def}.sifat",
			"{$this->table_def}.status",
			"{$this->table_def}.keterangan",
			"CONCAT(pb.kode, ' - ', pb.nama) pabrik",
			"CONCAT(pengajuan_by.first_name, ' ',pengajuan_by.last_name) pengajuan_by",
			"CONCAT(diubah_by.first_name, ' ', diubah_by.last_name) diubah_by",
		);
		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
				"draw" => intval($_POST['draw']),
				"recordsTotal" => $iTotal,
				"recordsFiltered" => $iFilteredTotal,
				"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$obj->sifat = ucfirst($obj->sifat);
			$obj->status_desc = $this->config->item('status_po')[$obj->status];

			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function save() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		if (isset($obj->uid) && $obj->uid != "") {
			$result = $this->main->update($obj);
		} else {
			$result = $this->main->create($obj);
		}

		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200);
		echo json_encode($result);
	}

	public function get_data($uid = 0) {
		if (!$this->input->is_ajax_request())
			exit();

		if ($uid) {
			$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
			$obj->sifat_desc = ucfirst($obj->sifat);
			$obj->status_desc = $this->config->item('status_po')[$obj->status];
			$obj->details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.po_id = {$obj->id}")['data'];
			$obj->biaya_lains = $this->biaya_lain->get_all(0, 0, "WHERE {$this->table_def_po_biaya_lain}.po_id = {$obj->id}")['data'];
		} else {
			$data = $this->input->get('data');
			if ($data) {
				$data = json_decode(base64_decode($this->input->get('data')));

				$sWhere = "";
				$aWheres = array();
				$aWheres[] = "{$this->table_def_barang}.status = 1";
				$aWheres[] = "{$this->table_def_stock}.dipesan = 0";
				$aWheres[] = "{$this->table_def_stock}.qty <= {$this->table_def_stock}.reorder";
				$aWheres[] = "{$this->table_def_barang}.pabrik_id = {$data->pabrik_id}";
				if (count($aWheres) > 0) {
					$sWhere = implode(' AND ', $aWheres);
				}
				if (!empty($sWhere)) {
					$sWhere = "WHERE ".$sWhere;
				}
				$listBarang = $this->pesan_main->get_all(0, 0, $sWhere, "ORDER BY {$this->table_def_barang}.nama ASC")['data'];
				foreach ($listBarang as $row) {
					$row->qty = $row->maximum - $row->stock;
				}

				$obj = new stdClass();
				$obj->pabrik_id = $data->pabrik_id;
				$obj->details = $listBarang;
				$obj->biaya_lains = [];
			}
		}
		echo json_encode(['data' => $obj]);
	}

	/**
	 * Form Data Object
	 */
	private function _getDataObject() {
		$his = date('H:i:s');
		$obj = new stdClass();
		$obj->id = $this->input->post('id');
		$obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
		$obj->tanggal = get_date_accepted_db($this->input->post('tanggal')).' '.$his;
		$obj->pabrik_id = $this->input->post('pabrik_id');
		$obj->kurs_id = $this->input->post('kurs_id') ? : null;
		$obj->kurs = $this->input->post('kurs');
		$obj->sifat = $this->input->post('sifat');
		$obj->no_invoice = $this->input->post('no_invoice');
		$obj->total = $this->input->post('total');
		$obj->total_kurs = $this->input->post('total_kurs');
		$obj->is_ppn = $this->input->post('is_ppn');
		$obj->total_ppn = $this->input->post('total_ppn');
		$obj->total_ppn_kurs = $this->input->post('total_ppn_kurs');
		$obj->total_biaya_lain = $this->input->post('total_biaya_lain');
		$obj->total_biaya_lain_kurs = $this->input->post('total_biaya_lain_kurs');
		$obj->grand_total = $obj->total + $obj->total_ppn + $obj->total_biaya_lain;
		$obj->grand_total_kurs = $obj->total_kurs + $obj->total_ppn_kurs + $obj->total_biaya_lain_kurs;
		if($this->input->post('status')) $obj->status = $this->input->post('status');
		
		$details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.po_id = {$obj->id}");
		$aDetails = array();
		array_map(function($each) use (&$aDetails) {
				unset($each->kode_barang);
				unset($each->barang);
				unset($each->satuan_po);
				$each->data_mode = Data_mode_model::DATA_MODE_DELETE;
				$aDetails[$each->id] = $each;

				return $each;
		}, $details['data']);

		if (isset($_POST['detail_id'])) {
			for ($i = 0; $i < count($_POST['detail_id']); $i++) {
				$detail_id = $_POST['detail_id'][$i];

				$detail = new StdClass();
				$detail->id = $detail_id;
				$detail->barang_id = $_POST['barang_id'][$i];
				$detail->satuan_id = $_POST['satuan_id'][$i];
				$detail->isi_satuan = $_POST['isi_satuan'][$i];
				$detail->minimum = $_POST['minimum'][$i];
				$detail->stock = $_POST['stock'][$i];
				$detail->maximum = $_POST['maximum'][$i];
				$detail->qty = $_POST['qty'][$i];
				$detail->harga = $_POST['harga'][$i];
				$detail->harga_kurs = $_POST['harga_kurs'][$i];
				$detail->harga_diterima = $_POST['harga'][$i];
				$detail->harga_diterima_kurs = $_POST['harga_kurs'][$i];
				$detail->total = ($_POST['qty'][$i] * $_POST['harga'][$i]);
				$detail->total_kurs = ($_POST['qty'][$i] * $_POST['harga_kurs'][$i]);

				if (!array_key_exists($detail_id, $aDetails)) {
					$detail->data_mode    = Data_mode_model::DATA_MODE_ADD;
					$aDetails[uniqid()] = $detail;
				} else {
					$detail->data_mode  = Data_mode_model::DATA_MODE_EDIT;
					$aDetails[$detail_id] = $detail;
				}    
			}
		}
		$obj->details = $aDetails;

		$biaya_lains = $this->biaya_lain->get_all(0, 0, "WHERE {$this->table_def_po_biaya_lain}.po_id = {$obj->id}");
		$aDetails = array();
		array_map(function($each) use (&$aDetails) {
			$each->data_mode = Data_mode_model::DATA_MODE_DELETE;
			$aDetails[$each->id] = $each;
			return $each;
		}, $biaya_lains['data']);

		if (isset($_POST['biaya_lain_detail_id'])) {
			for ($i = 0; $i < count($_POST['biaya_lain_detail_id']); $i++) {
				$biaya_lain_detail_id = $_POST['biaya_lain_detail_id'][$i];

				$detail = new StdClass();
				$detail->id = $biaya_lain_detail_id;
				$detail->deskripsi = $_POST['biaya_lain_deskripsi'][$i];
				$detail->total = $_POST['biaya_lain'][$i];
				$detail->total_kurs = $_POST['biaya_lain_kurs'][$i];

				if (!array_key_exists($biaya_lain_detail_id, $aDetails)) {
					$detail->data_mode    = Data_mode_model::DATA_MODE_ADD;
					$aDetails[uniqid()] = $detail;
				} else {
					$detail->data_mode  = Data_mode_model::DATA_MODE_EDIT;
					$aDetails[$detail->id] = $detail;
				}    
			}
		}
		$obj->biaya_lains = $aDetails;
		return $obj;
	}

	public function cetak($uid = "")
	{ 
		$data = $uid ? json_decode(base64_decode($uid)) : FALSE;
        if(!$data) {
            show_404(); exit();
        }
        $uid = $data->uid;
        $view = 'gudang-farmasi/po/cetak';
        if($data->browse == "po_admin") $view = 'gudang-farmasi/po/admin/cetak';

		$obj = $this->main->get_by("WHERE ({$this->table_def}.uid = \"{$uid}\")");
		$obj->sifat_desc = ucfirst($obj->sifat);
		$obj->status_desc = $this->config->item('status_po')[$obj->status];
		$obj->indo_tanggal = konversi_to_id(date("d M Y", strtotime($obj->tanggal)));
		$obj->terbilang = numbers_to_words($obj->grand_total);
		$obj->details = $this->main_detail->get_all(0, 0, "WHERE ({$this->table_def_detail}.po_id = ".$obj->id.")", "ORDER BY {$this->table_def_detail}.id ASC")['data'];
		$obj->biaya_lains = $this->biaya_lain->get_all(0, 0, "WHERE ({$this->table_def_po_biaya_lain}.po_id = ".$obj->id.")", "ORDER BY {$this->table_def_po_biaya_lain}.id ASC")['data'];

		# Get User Pencetak
		$current_user = "";
		$user = $this->user_model->get_by_id($this->auth->userid());
		if($user) {
			$current_user = $user->first_name." ".($user->last_name ? $user->last_name : "");
		}

		$current_date = konversi_to_id(date("d M Y")).' '.date('H:i');
		$data = array(
			'obj' => $obj,
			'current_date' => $current_date,
			'current_user' => $current_user,
		);

		$html = $this->load->view($view, $data, TRUE);
		
		# Create PDF
		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => 'A4',
			'orientation' => 'P',
		]);
		$mpdf->WriteHTML($html);
		$mpdf->Output('Purchase Order '.$obj->kode.'.pdf', "I");
	}
}