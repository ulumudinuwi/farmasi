<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'events/EventProcess.php');

class Stock extends CI_Controller 
{
	protected $table_def = "t_logistik_stock";
	protected $table_def_kartu_stock = "t_logistik_kartu_stock";
	protected $table_def_barang = "m_barang";
	protected $table_def_jenis = "m_jenisbarang";
	protected $table_def_satuan = "m_satuan";
	
	function __construct() {
		parent::__construct();
		$this->load->model('logistik/Stock_model', 'main');
		$this->load->model('logistik/Kartu_stock_model', 'kartu_main');
	}

	/**
	 * Load data
	 */
	public function load_data(){
		$status  = $_POST['status'];

		$aColumns = array('kode', 'nama', 'satuan',  'maximum', 'minimum', 'reorder', 'qty', '');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						case "kode":
						case "nama":
						$aOrders[] = $this->table_def_barang.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
						break;
						default:
						$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
						break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def_barang}.status = 1";
		if($status != "") {
			switch ($status) {
				case 1:
				$aWheres[] = "{$this->table_def}.qty > {$this->table_def}.reorder";
				break;
				case 2:
				$aWheres[] = "{$this->table_def}.qty BETWEEN ({$this->table_def}.minimum+1) AND {$this->table_def}.reorder";
				break;
				case 3:
				$aWheres[] = "{$this->table_def}.qty <= {$this->table_def}.minimum";
				break;
			}
		}
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						case "kode":
						case "nama":
						$aLikes[] = "{$this->table_def_barang}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
						break;
						default:
						$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
						break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.qty",
			"{$this->table_def}.maximum",
			"{$this->table_def}.minimum",
			"{$this->table_def}.reorder",
			"{$this->table_def}.dipesan",
			"{$this->table_def_barang}.kode",
			"{$this->table_def_barang}.nama",
			"{$this->table_def_barang}.alias",
			"CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan",
		);

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function load_data_kartu_stock() 
	{
		$uid = $this->input->get('uid');
		$aColumns = array('created_at', 'kode', 'keterangan', 'masuk', 'keluar',  'saldo');
		
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
						$aOrders[] = $this->table_def_kartu_stock.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def_kartu_stock}.barang_id = ".json_decode(base64_decode($uid));
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
						$aLikes[] = "{$this->table_def_kartu_stock}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$list = $this->kartu_main->get_all($iLimit, $iOffset, $sWhere, $sOrder);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$obj->keterangan = $obj->keterangan ? $obj->keterangan : "-";
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function load_data_modal(){
		$mode = $this->input->get('mode') ? $this->input->get('mode') : '';
		$aColumns = array('kode', 'nama');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) 
					$aOrders[] = $aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def_barang}.status = 1";
		if ($mode == "po") $aWheres[] = "{$this->table_def}.dipesan = 0";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						case 'kode':
							$aLikes[] = "{$this->table_def_barang}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
						case 'nama':
							$aLikes[] = "({$this->table_def_barang}.nama LIKE '%".$_POST['search']['value']."%' OR {$this->table_def_barang}.alias LIKE '%".$_POST['search']['value']."%')";
							break;
						default:
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def_barang}.id",
			"{$this->table_def_barang}.kode",
			"{$this->table_def_barang}.nama",
		);

		switch ($mode) {
			case "po":
				$aSelect[] = "({$this->table_def}.minimum / {$this->table_def_barang}.isi_satuan_penggunaan) minimum";
				$aSelect[] = "({$this->table_def}.qty / {$this->table_def_barang}.isi_satuan_penggunaan) stock";
				$aSelect[] = "({$this->table_def}.maximum / {$this->table_def_barang}.isi_satuan_penggunaan) maximum";
				$aSelect[] = "{$this->table_def_barang}.harga_pembelian harga";
				$aSelect[] = "{$this->table_def_barang}.satuan_pembelian_id satuan_id";
				$aSelect[] = "{$this->table_def_barang}.isi_satuan_penggunaan isi_satuan";
				$aSelect[] = "CONCAT(satuan_pembelian.nama, ' (', satuan_pembelian.singkatan, ')') satuan";
				break;
			default:
				$aSelect[] = "{$this->table_def}.minimum";
				$aSelect[] = "{$this->table_def}.qty stock";
				$aSelect[] = "{$this->table_def}.maximum";
				$aSelect[] = "{$this->table_def_barang}.satuan_penggunaan_id satuan_id";
				$aSelect[] = "1 isi_satuan";
				$aSelect[] = "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan";
				break;
		}

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function setting() {

		if (!$this->input->is_ajax_request())
			exit();

		$id = $this->input->post('setting_id');

		$setting = new stdClass();
		$setting->maximum = $this->input->post("setting_maximum");
		$setting->reorder = $this->input->post("setting_reorder");
		$setting->minimum = $this->input->post("setting_minimum");
		$setting->update_by = $this->session->userdata('auth_user');
		$setting->update_at = date('Y-m-d H:i:s');

		$this->db->where('id', $id)
							->update($this->table_def, $setting);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();
			$this->output->set_status_header(200);
			echo json_encode(['status' => 'success']);
		}
		else {
			$this->db->trans_rollback();
			$this->output->set_status_header(500);
			echo json_encode(['status' => 'error']);
		}
	}

	public function listen() {
		session_write_close(); # !Important
		require_once(APPPATH.'events/gudang/GlobalEvent.php');

		$uid = $this->input->get('uid');
		$modul = $this->input->get('modul') ? $this->input->get('modul') : 'logistik';
		/*$sse = new Sse\SSE();
		$sse->addEventListener($modul.'-'.$uid, new GlobalEvent($uid));*/

		$sse = new EventProcess($uid, $modul.'-'.$uid);
		$sse->start();
	}
}