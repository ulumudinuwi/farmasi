<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends CI_Controller 
{
  protected $table_def = "t_logistik_po";
  
  function __construct() {
    parent::__construct();
    $this->lang->load('barang');

    $this->load->model('logistik/Po_model', 'main');
  }

  /**
   * Load data
   */
  public function load_data(){
      $browse = $this->input->get('browse');
      $tanggal_dari  = $_POST['tanggal_dari'];
      $tanggal_sampai  = $_POST['tanggal_sampai'];
      $sifat  = $_POST['sifat'];
      $pabrik_id  = $_POST['pabrik_id'];
      $status  = $_POST['status'];

      $aColumns = array('kode', 'tanggal', 'pabrik_id',  'vendor_id', 'sifat', '');
      if($browse == 2) $aColumns = array('kode', 'tanggal', 'pabrik_id',  'vendor_id', 'sifat', 'status', 'persetujuan_by');
      /* 
       * Paging
       */
      if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
          $iLimit = intval( $_POST['length'] );
          $iOffset = intval( $_POST['start'] );
      }

      /*
       * Ordering
       */
      $sOrder = "";
      $aOrders = array();
      for ($i = 0; $i < count($aColumns); $i++) {
        if($_POST['columns'][$i]['orderable'] == "true") {
          if($i == $_POST['order'][0]['column']) {
            switch ($aColumns[$i]) {
              default:
                $aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
                break;
            }
          }
        }
      }
      if (count($aOrders) > 0) {
          $sOrder = implode(', ', $aOrders);
      }
      if (!empty($sOrder)) {
          $sOrder = "ORDER BY ".$sOrder;
      }

      /*
       * Where
       */
      $sWhere = "";
      $aWheres = array();
      if($browse == 2) {
        $aWheres[] = $this->table_def.".status >= ".$this->config->item('status_po_purchasing');
      } else $aWheres[] = $this->table_def.".status = ".$this->config->item('status_po_waiting_for_approval');
      if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
      if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
      if($sifat != "") $aWheres[] = "{$this->table_def}.sifat = '{$sifat}'";
      if($pabrik_id != "") $aWheres[] = "{$this->table_def}.pabrik_id = ".base64_decode($pabrik_id);
      if($status != "") $aWheres[] = "{$this->table_def}.status = {$status}";
      if (count($aWheres) > 0) {
          $sWhere = implode(' AND ', $aWheres);
      }
      if (!empty($sWhere)) {
          $sWhere = "WHERE ".$sWhere;
      }

      $aLikes = array();
      if($_POST['search']['value'] != "") {
          for ($i = 0; $i < count($aColumns); $i++) {
              if($_POST['columns'][$i]['searchable'] == "true") {
                  switch ($aColumns[$i]) {
                    default:
                      $aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                      break;
                  }
              }
          }
      }

      if (count($aLikes) > 0) {
          $sLike = "(".implode(' OR ', $aLikes).")";
          $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
      }

      $aSelect = array(
        "{$this->table_def}.id",
        "{$this->table_def}.uid",
        "{$this->table_def}.kode",
        "{$this->table_def}.tanggal",
        "{$this->table_def}.sifat",
        "{$this->table_def}.status",
        "CONCAT(pb.kode, ' - ', pb.nama) pabrik",
        "CONCAT(vd.kode, ' - ', vd.nama) vendor",
        "CONCAT(pengajuan_by.first_name, ' ',pengajuan_by.last_name) pengajuan_by"
      );
      if($browse == 2) {
        $aSelect[] = "{$this->table_def}.keterangan";
        $aSelect[] = "{$this->table_def}.persetujuan_at";
        $aSelect[] = "CONCAT(persetujuan_by.first_name, ' ',persetujuan_by.last_name) persetujuan_by";
      }
      $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

      $rResult = $list['data'];
      $iFilteredTotal = $list['total_rows'];
      $iTotal = $list['total_rows'];

      /*
       * Output
       */
      $output = array(
          "draw" => intval($_POST['draw']),
          "recordsTotal" => $iTotal,
          "recordsFiltered" => $iFilteredTotal,
          "data" => array(),
      );

      $rows = array();
      $i = $iOffset;
      foreach ($rResult as $obj) {
          $obj->sifat = ucfirst($obj->sifat);
          $obj->status_desc = $this->config->item('status_po')[$obj->status];

          $data = get_object_vars($obj);
          $data['no'] = ($i+1);
          $data['keterangan'] = empty($obj->keterangan) || is_null($obj->keterangan) ? '-' : $obj->keterangan;
          $rows[] = $data;
          $i++;
      }
      $output['data'] = $rows;

      echo json_encode($output);
  }

  public function act_exe() {
    
    if (!$this->input->is_ajax_request())
      exit();
    
    if ($this->input->post('uid')) {
      $uid = $this->input->post('uid');
      $mode = $this->input->post('mode');
      $alasan = $this->input->post('alasan');
      $result = $this->main->act_exe($uid, $alasan, $mode);

      $this->output->set_status_header(200);
      echo $result;
    } else {
      $this->output->set_status_header(500);
      echo json_encode(array('message' => 'error', 'status' => false));
    }
  }
}