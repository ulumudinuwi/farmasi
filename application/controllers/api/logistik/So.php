<?php defined('BASEPATH') OR exit('No direct script access allowed');

class So extends CI_Controller 
{
  protected $table_def = "t_logistik_stock_opname";
  protected $table_def_detail = "t_logistik_stock_opname_detail";
  protected $table_def_stock = "t_logistik_stock";
  protected $table_def_barang = "m_barang";
  protected $table_def_satuan = "m_satuan";
  
  function __construct() {
    parent::__construct();
    $this->load->model('logistik/So_model', 'main');
    $this->load->model('logistik/So_detail_model', 'main_detail');
    $this->load->model('logistik/Stock_model', 'stock_main');
  }

  /**
   * Load data
   */
  public function load_data_barang(){
    /*
     * Ordering
     */
    $sOrder = "";
    $aOrders = array();
    if (count($aOrders) > 0) {
      $sOrder = implode(', ', $aOrders);
    }
    if (!empty($sOrder)) {
      $sOrder = "ORDER BY ".$sOrder;
    }

    /*
     * Where
     */
    $sWhere = "";
    $aWheres = array();
    $aWheres[] = "{$this->table_def_barang}.status = 1";
    $aWheres[] = "{$this->table_def_stock}.status = 1"; // YANG BELUM PEMERIKSAAN PERTAMA
    if (count($aWheres) > 0) {
      $sWhere = implode(' AND ', $aWheres);
    }
    if (!empty($sWhere)) {
      $sWhere = "WHERE ".$sWhere;
    }

    $aLikes = array();
    if (count($aLikes) > 0) {
      $sLike = "(".implode(' OR ', $aLikes).")";
      $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
    }

    $aSelect = array(
      "{$this->table_def_stock}.id stock_id",
      "{$this->table_def_stock}.barang_id",
      "{$this->table_def_stock}.qty stock",
      "{$this->table_def_barang}.kode kode_barang",
      "{$this->table_def_barang}.nama barang",
      "{$this->table_def_barang}.satuan_penggunaan_id satuan_id",
      "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan",
    );

    $list = $this->stock_main->get_all(0, 0, $sWhere, $sOrder, $aSelect);
    echo json_encode(['data' => $list['data']]);
  }  

  public function load_data(){
    $browse = $this->input->get('browse');
    $tanggal_dari  = isset($_POST['tanggal_dari']) ? $_POST['tanggal_dari'] : "";
    $tanggal_sampai  = isset($_POST['tanggal_sampai']) ? $_POST['tanggal_sampai'] : "";

    $aColumns = array('kode', 'tanggal', '');
    /* 
     * Paging
     */
    if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
      $iLimit = intval( $_POST['length'] );
      $iOffset = intval( $_POST['start'] );
    }

    /*
     * Ordering
     */
    $sOrder = "";
    $aOrders = array();
    for ($i = 0; $i < count($aColumns); $i++) {
      if($_POST['columns'][$i]['orderable'] == "true") {
        if($i == $_POST['order'][0]['column']) {
          switch ($aColumns[$i]) {
            default:
              $aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
            break;
          }
        }
      }
    }
    if (count($aOrders) > 0) {
      $sOrder = implode(', ', $aOrders);
    }
    if (!empty($sOrder)) {
      $sOrder = "ORDER BY ".$sOrder;
    }

    /*
     * Where
     */
    $sWhere = "";
    $aWheres = array();
    if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
    if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
    if($browse == 1) {
      $aWheres[] = "{$this->table_def}.status = 1";
    } else $aWheres[] = "{$this->table_def}.status = 2";
    if (count($aWheres) > 0) {
      $sWhere = implode(' AND ', $aWheres);
    }
    if (!empty($sWhere)) {
      $sWhere = "WHERE ".$sWhere;
    }

    $aLikes = array();
    if($_POST['search']['value'] != "") {
      for ($i = 0; $i < count($aColumns); $i++) {
        if($_POST['columns'][$i]['searchable'] == "true") {
          switch ($aColumns[$i]) {
            default:
              $aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
              break;
          }
        }
      }
    }

    if (count($aLikes) > 0) {
      $sLike = "(".implode(' OR ', $aLikes).")";
      $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
    }

    $aSelect = array(
      "{$this->table_def}.uid",
      "{$this->table_def}.kode",
      "{$this->table_def}.tanggal",
      "{$this->table_def}.pengecekan1_at",
      "{$this->table_def}.pengecekan2_at",
      "CONCAT(pengecekan1_by.first_name, ' ',pengecekan1_by.last_name) pengecekan1_by",
      "CONCAT(pengecekan2_by.first_name, ' ',pengecekan2_by.last_name) pengecekan2_by",
    );

    $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

    $rResult = $list['data'];
    $iFilteredTotal = $list['total_rows'];
    $iTotal = $list['total_rows'];

    /*
     * Output
     */
    $output = array(
      "draw" => intval($_POST['draw']),
      "recordsTotal" => $iTotal,
      "recordsFiltered" => $iFilteredTotal,
      "data" => array(),
    );

    $rows = array();
    $i = $iOffset;
    foreach ($rResult as $obj) {
      $data = get_object_vars($obj);
      $data['no'] = ($i+1);
      $rows[] = $data;
      $i++;
    }
    $output['data'] = $rows;

    echo json_encode($output);
  }  

  public function get_data($uid = 0) {
    if (!$this->input->is_ajax_request())
      exit();

    $obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
    $obj->details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.so_id = {$obj->id}")['data'];
    echo json_encode(['data' => $obj]);
  }

  public function save() {
    
    if (!$this->input->is_ajax_request())
      exit();

    $his = date('H:i:s');
    $obj = new stdClass();
    if ($this->input->post('uid')) {
      $obj->id = $this->input->post('id');
      $obj->uid = $this->input->post('uid');
      $obj->kode = $this->input->post('kode');
      $obj->tanggal = $this->input->post('tanggal');
      $obj->status = 2;
      $obj->pengecekan2_at = date('Y-m-d H:i:s');
      $obj->pengecekan2_by = $this->auth->userid();

      $aDetails = array();
      if (isset($_POST['detail_id'])) {
        for ($i = 0; $i < count($_POST['detail_id']); $i++) {
          $detail_id = $_POST['detail_id'][$i];

          $detail = new StdClass();
          $detail->id = $detail_id;
          $detail->stock_id = $_POST['stock_id'][$i];
          $detail->pengecekan2_stock_fisik = $_POST['stock_fisik'][$i] ? $_POST['stock_fisik'][$i] : $_POST['pengecekan1_stock_fisik'][$i];
          $detail->pengecekan2_selisih = $_POST['stock_fisik'][$i] ? $_POST['selisih'][$i] : $_POST['pengecekan1_selisih'][$i];
          $detail->data_mode = Data_mode_model::DATA_MODE_EDIT;
          $aDetails[$detail_id] = $detail;
        }
      }
      $obj->details = $aDetails;
      $result = $this->main->saveSecondCheck($obj);
    } else {
      $obj->tanggal = get_date_accepted_db($this->input->post('tanggal')).' '.$his;
      $obj->pengecekan1_at = date('Y-m-d H:i:s');
      $obj->pengecekan1_by = $this->auth->userid();
      $obj->created_at = date('Y-m-d H:i:s');
      $obj->created_by = $this->auth->userid();

      $aDetails = array();
      if (isset($_POST['barang_id'])) {
        for ($i = 0; $i < count($_POST['barang_id']); $i++) {
          $barang_id = $_POST['barang_id'][$i];

          $detail = new StdClass();
          $detail->stock_id = $_POST['stock_id'][$i];
          $detail->barang_id = $_POST['barang_id'][$i];
          $detail->satuan_id = $_POST['satuan_id'][$i];
          $detail->isi_satuan = $_POST['isi_satuan'][$i];
          $detail->stock_sistem = $_POST['stock_sistem'][$i];
          $detail->pengecekan1_stock_fisik = $_POST['stock_fisik'][$i];
          $detail->pengecekan1_selisih = $_POST['selisih'][$i];
          $detail->data_mode = Data_mode_model::DATA_MODE_ADD;
          $aDetails[uniqid()] = $detail;
        }
      }
      $obj->details = $aDetails;
      $result = $this->main->saveFirstCheck($obj);
    }

    if(!$result) 
      $this->output->set_status_header(500);

    $this->output->set_status_header(200);
    echo json_encode($result);
  }
}