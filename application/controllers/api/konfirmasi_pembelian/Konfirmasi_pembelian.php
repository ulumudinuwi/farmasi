<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Carbon\Carbon;

class Konfirmasi_pembelian extends CI_Controller 
{
	protected $table_def = "t_pos_checkout";
	protected $table_def_pesanan = "t_pos_pesanan";
	protected $table_def_member = "m_dokter_member";
	protected $table_def_barang = "m_barang";
	protected $table_def_pos = "t_pos";
	protected $table_def_pos_detail = "t_pos_detail";
  	protected $table_def_pos_pembayaran = "t_pos_pembayaran";
	
	function __construct() {
		parent::__construct();
		$this->lang->load('barang');

		$this->load->model('Konfirmasi_pembelian_model', 'main');
		$this->load->model('Checkout_model', 'main');
		$this->ori_dir = './uploads/barang/original';
        $this->thumb_dir = './uploads/barang/thumbnail';
		$this->thumb_upload = './uploads/bukti_pembayaran';

        $this->load->helper('gudang_farmasi');
        $this->load->helper('notifikasi_ecommerce');
	}

	/**
	 * Load data
	 */
	public function load_data(){
		$tanggal_dari  = $_POST['tanggal_dari'];
		$tanggal_sampai  = $_POST['tanggal_sampai'];
		
		$aColumns = array('keterangan');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		$aOrders[] = "{$this->table_def}.status desc";
		$aOrders[] = "{$this->table_def}.id asc";
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def}.no_resi IS NULL";
		$aWheres[] = "{$this->table_def}.status != 5";
		$aWheres[] = "{$this->table_def}.status_pembayaran != 2";
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) <= '{$tanggal_sampai}'";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.tanggal_transaksi",
			"{$this->table_def}.no_invoice",
			"{$this->table_def}.total_harga",
			"{$this->table_def}.bukti_pembayaran",
			"{$this->table_def}.status_pembayaran",
			"{$this->table_def}.status",
			"{$this->table_def}.alasan_penolakan",
			"dokter_member.nama",
			"dokter_member.no_member",
		);

        //$sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total " : "GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total";

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function load_data_history(){
		$tanggal_dari  = $_POST['tanggal_dari'];
		$tanggal_sampai  = $_POST['tanggal_sampai'];
		
		$aColumns = array('keterangan');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		$aOrders[] = "{$this->table_def}.status desc";
		$aOrders[] = "{$this->table_def}.id asc";
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def}.status != 5";
		$aWheres[] = "{$this->table_def}.status_pembayaran >= 2";
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) <= '{$tanggal_sampai}'";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.tanggal_transaksi",
			"{$this->table_def}.no_invoice",
			"{$this->table_def}.total_harga",
			"{$this->table_def}.bukti_pembayaran",
			"{$this->table_def}.status_pembayaran",
			"{$this->table_def}.status",
			"{$this->table_def}.alasan_penolakan",
			"dokter_member.nama",
			"dokter_member.no_member",
		);

        //$sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total " : "GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total";

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function save() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		if($obj->uid == ''){
			$result = $this->main->create($obj);
		}else{
			$result = $this->main->update($obj);
		}

		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200);
		echo json_encode($result);
	}

	public function get_data() {
		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->get('uid') ? : '';
		/*
         * Order
         */
		$sOrder = "";
        $aOrders = array();
        $aOrders[] = "{$this->table_def}.tanggal_transaksi DESC";
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = "{$this->table_def}.uid = '{$uid}'";
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $aSelect = array(
        	"{$this->table_def}.id",
        	"{$this->table_def}.uid",
        	"{$this->table_def}.tanggal_transaksi",
        	"{$this->table_def}.no_invoice as no_invoice",
        	"{$this->table_def}.total_harga as total_harga",
        	"{$this->table_def}.bukti_pembayaran as bukti_pembayaran",
        	"{$this->table_def}.ekspedisi as ekspedisi",
        	"{$this->table_def}.alasan_penolakan as alasan_penolakan",
        	"{$this->table_def}.ongkir as ongkir",
        	"{$this->table_def}.status as status",
        );

		$list = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'", $aSelect);
		$list->bukti_pembayaran = $this->thumb_upload.'/'.$list->bukti_pembayaran;

			switch ($list->status) {
				case 1:
					$list->status_label = 'Belum Bayar';
				break;
				case 2:
					$list->status_label = 'Dikemas';
				break;
				case 3:
					$list->status_label = 'Dikirim';
				break;
				case 4:
					$list->status_label = 'Selesai';
				break;
				case 5:
					$list->status_label = 'Dibatalkan';
				break;
			}

		$list->dataDetail = $this->db->select('a.id checkout_id, a.qty, a.harga, a.diskon,  b.nama')
                                    ->join($this->table_def_barang.' b', 'a.product_uid=b.uid')
                                    ->where('a.checkout_id', $list->id)
                                    ->where('a.status !=', '1')
                                    ->get($this->table_def_pesanan.' a')->result();

		$this->output->set_status_header(200)
			->set_content_type('application/json')
            ->set_output(json_encode($list));
	}

    public function cancel_data() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $desc = $this->input->post('desc');

        $result = $this->main->cancel_data($uid, $desc);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $data =	$this->db->select('no_invoice, member_id')
	            ->where('uid', $uid)
	            ->get($this->table_def)
	            ->row();
        notif_msg_member($data->member_id, 'Pesanan Ditolak', "Pesanan No. {$data->no_invoice} ditolak karena {$desc}.", "/akun/profile/pesanan");

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function confirm_data() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');

        $data =	$this->db->select('*')
	            ->where('uid', $uid)
	            ->get($this->table_def)
	            ->row();

	    $data->detail = $this->db->select('a.id, a.qty, a.harga, a.diskon, a.total, a.status, b.nama, b.id as product_id, b.uid as product_uid, b.bpom, b.diskon')
                        ->join($this->table_def_barang.' b', 'a.product_uid=b.uid')
			            ->where('checkout_id', $data->id)
			            ->get($this->table_def_pesanan.' a')
			            ->result();;

		//set bpom
		$bpom = array('data' => [], 'total' => 0, 'bpom' => 0);
		$nonbpom = array('data' => [], 'total' => 0, 'bpom' => 0);

		foreach ($data->detail as $row) {
            if ($row->bpom == 1) {
            	$bpom['data'][] = $row;
            	$bpom['total'] += $row->harga * $row->qty;
            	$bpom['bpom'] += 1;
            }else{
            	$nonbpom['data'][] = $row;
            	$nonbpom['total'] += $row->harga * $row->qty;
            	$nonbpom['bpom'] += 0;
            }
		}

		$successBpom = false;
		$successNonBpom = false;

		if (count($bpom['data']) > 0) {
			$successBpom = $this->_saveInvoice($data, $bpom);
		}
		if (count($nonbpom['data']) > 0) {
			$successNonBpom = $this->_saveInvoice($data, $nonbpom);
		}

        if ($successBpom || $successNonBpom) {
        	notif_msg_member($data->member_id, 'Pesanan dikonfirmasi', "Pesanan No. {$data->no_invoice} telah terkonfirmasi dan segera untuk disiapkan.", "/akun/profile/pesanan");
	    	$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode(["message" => "Product has been Checkout."]));
		} else {
			$this->output->set_status_header(400)
					->set_content_type('application/json')
	            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak ditemukan."]));
		}
    }

	private function _saveInvoice($data = array(), $detailProduct = array()) {
		$obj = new stdClass();
		$obj->no_invoice = $data->no_invoice;
		$obj->member_id = $data->member_id;
		$obj->sales_id = $data->sales_id;
		$obj->piutang = 0;
		$obj->tanggal_transaksi = $data->tanggal_transaksi;
		//$obj->total_harga = $data->total_harga;
		$obj->jenis_diskon = 'persen';
		//$obj->grand_total = $data->total_harga;
		$obj->sisa_pembayaran = 0;
		$obj->status_lunas = 1;
		$obj->status_pembayaran = 2;
		//$obj->point_didapat = $data->total_harga / 10000;
		$obj->diskon = 0;
		$obj->internal_used = 0;
		// $obj->tgl_jatuh_tempo = Carbon::parse($data->tanggal_transaksi)->addDays(30);
        $obj->tgl_jatuh_tempo = Carbon::parse($data->tanggal_transaksi);
		$obj->verifikasi_pos_manager = 1;
		$obj->verifikasi_diskon_direktur = 1;
		$obj->verifikasi_diskon_manager = 1;
		$obj->keterangan_verifikasi = json_encode($detailProduct);
		$obj->json_keterangan = json_encode($detailProduct);
		$obj->is_kurir = $data->ekspedisi ? 1 : 0;
		$obj->is_approved = 1;
		$obj->ekspedisi = $data->ekspedisi;
		$obj->ongkir_free = 0;
		$obj->ongkir = $data->ongkir;
		//$obj->total_pembayaran = $data->total_harga;
		$obj->bpom = $detailProduct['bpom'];
		// $obj->ppn = $detailProduct['bpom'] == 1 ? 10 : 0;
        $obj->ppn = 0;
		$obj->flag = 2; //pembelian online

        $insert = $this->main->create($obj);
        $total_harga_before_diskon = 0;
        $total_harga = 0;
        $grand_total = 0;
        $total_pembayaran = 0;

        if ($insert) {
        	foreach ($detailProduct['data'] as $row) {
        		$detailPesanan = new stdClass();
        		$detailPesanan->member_id = $data->member_id;
				$detailPesanan->sales_id = $data->sales_id;
				$detailPesanan->pos_id = $insert;
				$detailPesanan->no_fbp = get_no_fbp();
				$detailPesanan->barang_id = $row->product_id;
				$detailPesanan->harga = $row->harga;
				$detailPesanan->qty = $row->qty;
				$detailPesanan->diskon = $row->diskon;
				$detailPesanan->diskon_plus = 0;
				$detailPesanan->total = $row->harga * $row->qty;
				$detailPesanan->jenis_diskon = 'persen';
				$detailPesanan->grand_total = $detailPesanan->total - ($row->diskon != 0 ? ($detailPesanan->total * ($row->diskon / 100)) : 0);
				$detailPesanan->json_response = json_encode($row);
	            $detailID = $this->main->save_detail($detailPesanan);

	            $total_harga_before_diskon += $detailPesanan->total;
	            $total_harga += $detailPesanan->grand_total;
	            $grand_total += $detailPesanan->grand_total;
				$total_pembayaran += $detailPesanan->grand_total;

        		$dataCheckout = new stdClass();
	            $dataCheckout->status_pembayaran = 2;
	            $dataCheckout->update_by = $this->session->userdata('auth_user');
	            $dataCheckout->update_at = date('Y-m-d H:i:s');
	            $this->db->where('id', $data->id);
	            $this->db->update($this->table_def, $dataCheckout);

	            //kurangi stock
        		$config_tipe = $this->config->item('tipe_kartu_stock_pengeluaran');

	            $obj = new stdClass();
                $obj->tanggal = $data->tanggal_transaksi;
                $obj->kode = $data->no_invoice;
                $obj->id = $insert;
                $obj->keterangan = 'Pengeluaran Barang POS Pembelian Online  NO.INVOICE '.$data->no_invoice;

                $detail = new stdClass();
                $detail->barang_id = $row->product_id;
                $detail->qty = $row->qty;
                $detail->id = $detailID;
                 
                $json_response = procStockKeluar($obj,$detail,$config_tipe);
        	}

        	$dataPesanan = new stdClass();
        	$point_didapat = $grand_total / 10000;
        	$dataPesanan->total_harga_before_diskon = $total_harga_before_diskon;
        	$dataPesanan->total_harga = $total_harga;
        	$dataPesanan->grand_total = $grand_total;
        	$dataPesanan->point_didapat = $point_didapat;
        	$dataPesanan->total_pembayaran = $total_pembayaran + $data->ongkir;

            $this->db->where('id', $insert);
            $this->db->update($this->table_def_pos, $dataPesanan);

            //kuitansi
            $kuitansi = get_no_kuitansi();
			$pembayaran = new stdClass();
			$pembayaran->tanggal_bayar = $data->tanggal_transaksi;
			$pembayaran->no_kuitansi = $kuitansi;
			$pembayaran->member_id = $data->member_id;
			$pembayaran->sales_id = $data->sales_id;
			$pembayaran->pos_id = $insert;
			$pembayaran->total_harus_dibayar = $grand_total + $data->ongkir;
			$pembayaran->jenis_pembayaran = 'transfer';
			$pembayaran->nominal_pembayaran = $grand_total + $data->ongkir;
			$pembayaran->bank_id = 1;
			$pembayaran->sisa_pembayaran = 0;
			$pembayaran->created_by = $this->session->userdata('auth_user');
			$pembayaran->created_at = date('Y-m-d H:i:s');
			$this->db->set('uid', 'UUID()', FALSE);;

			$this->db->insert($this->table_def_pos_pembayaran, $pembayaran);
			$kwId = $this->db->insert_id();

			//laba - rugi
			$dataLabaRugi = new stdClass();
			$dataLabaRugi->modul = JENIS_MODUL_POS;
            $dataLabaRugi->perkiraan_id = get_option('perkiraan_pos');
            $dataLabaRugi->transaksi_id = $kwId;
            $dataLabaRugi->bpom = $detailProduct['bpom'];;
            $dataLabaRugi->total = $grand_total + $data->ongkir;
            $dataLabaRugi->keterangan = 'Pembayaran POS dengan metode pembayaran transfer <b>No Kwitansi : '.$kuitansi.'</b>';
            $dataLabaRugi->tipe = JENIS_KREDIT;
            insertLabarugi($dataLabaRugi);
            
            $dataLabaRugi->perkiraan_id = get_option('perkiraan_kas');
            $dataLabaRugi->tipe = JENIS_DEBIT;
            insertLabarugi($dataLabaRugi);

            //Point Masuk

		    $member = $this->db->select('id, point')
		            ->where('id', $data->member_id)
		            ->get('m_dokter_member')
		            ->row();

            $_object = new stdClass();
            $_object->member_id = $member->id;
            $_object->point = $member->point;
            $_object->sales_id = $data->sales_id;

            $ket = 'Transkasi Online Baru';
            point_masuk_online($_object,$point_didapat,$insert,$ket);

			return true;
		}
		return false;
	}

    public function get_perkiraan() {
	    $query = $this->db->select('id, nama')
	            ->where('parent_id !=', 0)
	            ->where('deleted_flag', 0)
	            ->order_by('nama', 'asc')
	            ->get('m_perkiraan')
	            ->result();

	    $output['list'] = $query;

	    echo json_encode($output);
	}
}