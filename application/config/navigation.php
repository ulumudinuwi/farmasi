<?php

/**
 * Main Navigation.
 * Primarily being used in views/layouts/admin.php
 *
 */
$config['navigation'] = array(
    'dashboard' => array(
        'uri' => 'home',
        'title' => 'Dashboard',
        'icon' => 'fa fa-dashboard',
    ),
    'master' => array(
        'title' => 'Master',
        'icon' => 'fa fa-hdd-o',
        'children' => array(
            'master_pos' => array(
                'title' => 'Master POS',
                'children' => array(
                    'manager_sales' => array(
                        'title' => 'Manager Sales',
                        'uri' => 'master/manager_sales',
                    ),
                    'sales' => array(
                        'title' => 'Sales',
                        'uri' => 'master/sales',
                    ),
                    'dokter' => array(
                        'title' => 'Dokter',
                        'uri' => 'master/dokter',
                    ),
                    'area' => array(
                        'title' => 'Area',
                        'uri' => 'master/area',
                    ),
                ),
            ),
            'master_barang' => array(
                'title' => 'Data Barang',
                'children' => array(
                    'master_kurs' => array(
                        'title' => 'Kurs',
                        'uri' => 'master/kurs'
                    ),
                    'master_satuan' => array(
                        'title' => 'Satuan',
                        'uri' => 'master/satuan'
                    ),
                    'master_jenis_barang' => array(
                        'title' => 'Jenis',
                        'uri' => 'master/jenis_barang'
                    ),
                    'master_kategori_barang' => array(
                        'title' => 'Kategori',
                        'uri' => 'master/kategori_barang'
                    ),
                    'master_principal' => array(
                        'title' => 'Principal',
                        'uri' => 'master/principal'
                    ),
                    'master_kelompok_barang' => array(
                        'title' => 'Kelompok',
                        'uri' => 'master/kelompok_barang'
                    ),
                    'master_barang' => array(
                        'title' => 'List Barang',
                        'children' => array(
                            'master_barang' => array(
                                'title' => 'Barang',
                                'uri' => 'master/barang'
                            ),
                            'master_harga_jual' => array(
                                'title' => 'Harga Jual',
                                'uri' => 'master/barang/harga_jual'
                            ),
                        ),
                    ),
                ),
            ),
            // Metode Keuangan
            'master_keuangan' => array(
                'title' => 'Data Keuangan',
                'children' => array(
                    'perkiraan' => array(
                        'uri' => 'master/perkiraan',
                        'title' => 'Perkiraan'
                    ),
                    'bank' => array(
                        'uri' => 'master/bank',
                        'title' => 'Bank'
                    ),
                    'mesin_edc' => array(
                        'uri' => 'master/mesin_edc',
                        'title' => 'Mesin EDC'
                    ),
                    'no_rekening' => array(
                        'uri' => 'master/no_rekening',
                        'title' => 'No Rekening'
                    ),
                ),
            ),
            'master_promo' => array(
                'title' => 'Promo',
                'uri' => 'master/promo'
            ),
            'master_settings' => array(
                'title' => 'Settings',
                'uri' => 'master/settings',
            ),/*
            'master_satelite_unit' => array(
                'title' => 'Satelite Unit',
                'uri' => 'master/satelite_unit'
            ),*/
        )
    ),
    'gudang_farmasi' => array(
        'title' => 'Gudang Farmasi',
        'icon' => 'icon-med-i-pharmacy',
        'children' => array(
            'po_admin' => array(
                'title' => 'PO Admin',
                'uri' => 'gudang_farmasi/po_admin'
            ),
            'po_direktur' => array(
                'title' => 'PO Direktur',
                'uri' => 'gudang_farmasi/po_direktur'
            ),
            /*'approval' => array(
                'title' => 'Approval PO',
                'uri' => 'gudang_farmasi/approval'
            ),*/
            'penerimaan' => array(
                'title' => 'Penerimaan',
                'uri' => 'gudang_farmasi/penerimaan'
            ),
            'afkir' => array(
                'title' => 'Afkir Barang',
                'uri' => 'gudang_farmasi/afkir'
            ),
            /*'retur' => array(
                'title' => 'Retur Barang',
                'uri' => 'gudang_farmasi/retur_barang'
            ),
            'pengeluaran' => array(
                'title' => 'Pengeluaran',
                'uri' => 'gudang_farmasi/pengeluaran'
            ),
            'penerimaan_retur' => array(
                'title' => 'Penerimaan Retur',
                'uri' => 'gudang_farmasi/penerimaan_retur'
            ),*/
            'stock' => array(
                'title' => 'Stock Barang',
                'uri' => 'gudang_farmasi/stock'
            ),
            'so' => array(
                'title' => 'Stock Opname',
                'uri' => 'gudang_farmasi/so'
            ),
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'gudang_farmasi/laporan',
            ),
        )
    ),
    'sales' => array(
        'title' => 'Sales',
        'icon' => 'icon-med-i-billing',
        'children' => array(
            'pos' => array(
                'title' => 'Point Of Sales (POS)',
                'uri' => 'sales/pos/transaksi'
            ),
            'list_pos' => array(
                'title' => 'Daftar Transaksi',
                'uri' => 'sales/pos/list_transaksi'
            ),
            'list_approval' => array(
                'title' => 'Daftar Approval',
                'uri' => 'sales/pos/list_approval'
            ),
            'pembayaran' => array(
                'title' => 'Pembayaran Cicilan',
                'uri' => 'sales/cicilan/list_pembayaran',
            ),
            'retur_penjualan' => array(
                'title' => 'Retur Penjualan',
                'uri' => 'sales/retur_penjualan',
            ),
            'laporan' => array(
                'title' => 'Laporan',
                'uri' => 'sales/laporan',
            ),
        )
    ),
    'pengalihan_invoice' => array(
        'title' => 'Pengalihan Invoice',
        'icon' => 'icon-hand',
        'children' => array(
            'daftar_invoice' => array(
                'title' => 'Daftar Invoice',
                'uri' => 'sales/pengalihan_invoice/daftar_invoice'
            ),
            'approve_daftar_invoice' => array(
                'title' => 'Approval Daftar Invoice',
                'uri' => 'sales/pengalihan_invoice/list_approval'
            ),
        )
    ),
    'keuangan' => array(
        'title' => 'Keuangan',
        'icon' => 'icon-cash',
        'children' => array(
            'kas_bank' => array(
                'title' => 'Kas / Bank',
                'children' => array(
                    'penerimaan_kas_bank' => array(
                        'title' => 'Penerimaan Kas Bank',
                        'uri' => 'keuangan/kas_bank/penerimaan_kas_bank'
                    ),
                    'pengeluaran_kas_bank' => array(
                        'title' => 'Pengeluaran Kas Bank',
                        'uri' => 'keuangan/kas_bank/pengeluaran_kas_bank'
                    ),
                ),
            ),
            'pembukuan' => array(
                'title' => 'Pembukuan',
                'children' => array(
                    'jurnal_umum' => array(
                        'title' => 'Jurnal Umum',
                        'uri' => 'keuangan/jurnal_umum'
                    ),
                    'neraca_saldo' => array(
                        'title' => 'Neraca Saldo',
                        'uri' => 'keuangan/neraca_saldo'
                    ),
                ),
            ),
            'laporan_keuangan' => array(
                'title' => 'Laporan',
                'uri' => 'keuangan/laporan'
            ),
            
        )
    ),
    'pembelian_online' => array(
        'title' => 'Daftar Pembelian Online',
        'icon' => 'icon-people',
        'children' => array(
            'konfirmasi_pembelian' => array(
                'title' => 'Konfirmasi Pembelian Online',
                'uri' => 'pembelian_online/konfirmasi_pembelian'
            ),
            'pengiriman_pembelian' => array(
                'title' => 'Pengiriman Pembelian Online',
                'uri' => 'pembelian_online/pengiriman_pembelian'
            ),
            
        )
    ),
    'insentif' => array(
        'uri' => 'sales/insentif',
        'title' => 'Detail Insentif',
        'icon' => 'fa fa-exchange',
    ),
    /*'logistik' => array(
        'title' => 'Logistik',
        'icon' => 'icon-cabinet icon',
        'children' => array(
            'po' => array(
                'title' => 'PO',
                'uri' => 'logistik/po'
            ),
            'approval' => array(
                'title' => 'Approval PO',
                'uri' => 'logistik/approval'
            ),
            'penerimaan' => array(
                'title' => 'Penerimaan',
                'uri' => 'logistik/penerimaan'
            ),
            'retur' => array(
                'title' => 'Retur Barang',
                'uri' => 'logistik/retur_barang'
            ),
            'pengeluaran' => array(
                'title' => 'Pengeluaran',
                'uri' => 'logistik/pengeluaran'
            ),
            'penerimaan_mutasi' => array(
                'title' => 'Penerimaan Mutasi',
                'uri' => 'logistik/penerimaan_mutasi'
            ),
            'stock' => array(
                'title' => 'Stock Barang',
                'uri' => 'logistik/stock'
            ),
            'so' => array(
                'title' => 'Stock Opname',
                'uri' => 'logistik/so'
            ),
        )
    ),
    'request' => array(
        'title' => 'Form Request',
        'icon' => 'icon-stack-plus',
        'children' => array(
            'permintaan_unit' => array(
                'uri' => 'request/permintaan_unit',
                'title' => 'Permintaan Unit'
            ),
            'mutasi_unit' => array(
                'uri' => 'request/mutasi_unit',
                'title' => 'Mutasi Unit'
            ),
        ),
    ),*/
    'pesan' => array(
        'uri' => 'pesan/pesan',
        'title' => 'Pesan',
        'icon' => 'fa fa-envelope-o',
    ),
    'user-management' => array(
        'uri' => 'auth/user',
        'title' => 'User Management',
        'icon' => 'fa fa-user'
    ),
    'acl' => array(
        'title' => 'ACL',
        'icon' => 'fa fa-unlock-alt',
        'children' => array(
            'rules' => array(
                'uri' => 'acl/rule',
                'title' => 'Rules'
            ),
            'roles' => array(
                'uri' => 'acl/role',
                'title' => 'Roles'
            ),
            'resources' => array(
                'uri' => 'acl/resource',
                'title' => 'Resources'
            )
        )
    ),
    'utils' => array(
        'title' => 'Utils',
        'icon' => 'fa fa-wrench',
        'children' => array(
            'style_guides' => array(
                'uri' => 'utils/style_guides',
                'title' => 'Style Guides'
            ),
            'system_logs' => array(
                'uri' => 'utils/logs/system',
                'title' => 'System Logs'
            ),
            'deploy_logs' => array(
                'uri' => 'utils/logs/deploy',
                'title' => 'Deploy Logs'
            ),
            'info' => array(
                'uri' => 'utils/info',
                'title' => 'Info'
            )
        )
    ),
);
