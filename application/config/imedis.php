<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Data Mode
define('DATA_MODE_VIEW', 0);
define('DATA_MODE_ADD', 1);
define('DATA_MODE_EDIT', 2);
define('DATA_MODE_DELETE', 3);

// Jenis Kelamin
define('JENIS_KELAMIN_LAKI_LAKI', 1);
define('JENIS_KELAMIN_PEREMPUAN', 2);

// Agama
define('AGAMA_ISLAM', 1);
define('AGAMA_HINDU', 2);
define('AGAMA_BUDHA', 3);
define('AGAMA_KATOLIK', 4);
define('AGAMA_PROTESTAN', 5);
define('AGAMA_KONGHUCU', 6);
define('AGAMA_LAIN_LAIN', 7);

// Jenis Wilayah
define('WILAYAH_PROVINSI', 1);
define('WILAYAH_KABUPATEN', 2);
define('WILAYAH_KOTA', 3);
define('WILAYAH_KECAMATAN', 4);
define('WILAYAH_KELURAHAN', 5);
define('WILAYAH_DESA', 6);

// Golongan Darah
define('GOLONGAN_DARAH_O', 1);
define('GOLONGAN_DARAH_A', 2);
define('GOLONGAN_DARAH_B', 3);
define('GOLONGAN_DARAH_AB', 4);

// Status Kawin
// Golongan Darah

// Cara Pembayaran
define('CARA_BAYAR_UMUM', 1);
define('CARA_BAYAR_BPJS', 2);
define('CARA_BAYAR_ASURANSI', 3);
define('CARA_BAYAR_PERUSAHAAN', 4);

// Unit Layanan
define('UNIT_LAYANAN_RAWAT_JALAN', 1);
define('UNIT_LAYANAN_IGD', 2);
define('UNIT_LAYANAN_RAWAT_INAP', 3);
define('UNIT_LAYANAN_PENUNJANG_MEDIS', 4);
define('UNIT_LAYANAN_AMBULANCE', 5);
define('UNIT_LAYANAN_LAIN_LAIN', 6);

// PENUNJANG MEDIS
define('PENUNJANG_MEDIS_LABORATORIUM', 1);
define('PENUNJANG_MEDIS_RADIOLOGI', 2);
define('PENUNJANG_MEDIS_OK', 3);

/*
|--------------------------------------------------------------------------
| Jenis Kelamin
|--------------------------------------------------------------------------
*/

$config['jenis_kelamin'] = array(
	JENIS_KELAMIN_LAKI_LAKI => 'Laki-laki',
	JENIS_KELAMIN_PEREMPUAN => 'Perempuan'
);

/*
|--------------------------------------------------------------------------
| Golongan Darah
|--------------------------------------------------------------------------
*/

$config['golongan_darah'] = array(
	GOLONGAN_DARAH_O	=> 'O',
	GOLONGAN_DARAH_A	=> 'A',
	GOLONGAN_DARAH_B	=> 'B',
	GOLONGAN_DARAH_AB	=> 'AB'
);

/*
|--------------------------------------------------------------------------
| Layanan
|--------------------------------------------------------------------------
*/

$config['layanan'] = array(
	UNIT_LAYANAN_RAWAT_JALAN		=> 'Rawat Jalan',
	UNIT_LAYANAN_IGD				=> 'IGD',
	UNIT_LAYANAN_RAWAT_INAP			=> 'Rawat Inap',
	UNIT_LAYANAN_PENUNJANG_MEDIS	=> 'Penunjang Medis',
	UNIT_LAYANAN_AMBULANCE			=> 'Ambulance',
	UNIT_LAYANAN_LAIN_LAIN			=> 'Lain-lain'
);	

// TIPE KARTU STOCK
$config['tipe_kartu_stock_penerimaan'] = 1;
$config['tipe_kartu_stock_retur'] = 2;
$config['tipe_kartu_stock_penerimaan_retur'] = 3;
$config['tipe_kartu_stock_pengeluaran'] = 4;
$config['tipe_kartu_stock_mutasi'] = 5;
$config['tipe_kartu_stock_penerimaan_mutasi'] = 6;
$config['tipe_kartu_stock_stock_opname'] = 7;
$config['tipe_kartu_stock_afkir'] = 8;
$config['tipe_kartu_stock'] = array(
	$config['tipe_kartu_stock_penerimaan'] => 'Penerimaan',
	$config['tipe_kartu_stock_retur'] => 'Retur',
	$config['tipe_kartu_stock_penerimaan_retur'] => 'Penerimaan Retur',
	$config['tipe_kartu_stock_pengeluaran'] => 'Pengeluaran',
	$config['tipe_kartu_stock_mutasi'] => 'Mutasi',
	$config['tipe_kartu_stock_penerimaan_mutasi'] => 'Penerimaan Mutasi',
	$config['tipe_kartu_stock_stock_opname'] => 'Stock Opname',
	$config['tipe_kartu_stock_afkir'] => 'Afkir',
);

// STATUS PURCHASE ORDER
$config['status_po_canceled'] = 0;
$config['status_po_waiting_for_approval'] = 1;
$config['status_po_purchasing'] = 2;
$config['status_po_waiting_for_delivery'] = 3;
$config['status_po_partial_received'] = 4;
$config['status_po_closed'] = 5;
$config['status_po_rejected'] = 6;
$config['status_po_diverted'] = 7;
$config['status_po'] = array(
	$config['status_po_canceled'] => 'Dibatalkan',
	$config['status_po_waiting_for_approval'] => 'Menunggu Konfirmasi Direktur',
	$config['status_po_purchasing'] => 'Purchasing',
	$config['status_po_waiting_for_delivery'] => 'Menunggu Pengiriman',
	$config['status_po_partial_received'] => 'Diterima Sebagian',
	$config['status_po_closed'] => 'Diterima Semua',
	$config['status_po_rejected'] => 'Ditolak',
	$config['status_po_diverted'] => 'Dialihkan',
);

// SIFAT PURCHASE ORDER
$config['sifat_po'] = array(
	'cito' => 'Cito',
	'rutin' => 'Rutin',
	'biasa' => 'Biasa',
);

$config['status_partial'] = 1;
$config['status_closed'] = 2;
$config['status_done'] = 3;

// STATUS PENERIMAAN
$config['status_penerimaan'] = array(
	$config['status_partial'] => 'Diterima Sebagian',
	$config['status_closed'] => 'Diterima Semua',
);

// STATUS PENGELUARAN
$config['status_pengeluaran'] = array(
	$config['status_partial'] => 'Dikirim Sebagian',
	$config['status_closed'] => 'Dikirim Semua',
	$config['status_done'] => 'Diterima oleh Unit',
);

// STATUS REQUEST
$config['status_request_unprocessed'] = 1;
$config['status_request_partial'] = 2;
$config['status_request_closed'] = 3;
$config['status_request'] = array(
	$config['status_request_unprocessed'] => 'Belum Diproses',
	$config['status_request_partial'] => 'Dikirim Sebagian',
	$config['status_request_closed'] => 'Dikirim Semua',
);

// STATUS MUTASI
$config['status_mutasi_unprocessed'] = 1;
$config['status_mutasi_processed'] = 2;
$config['status_mutasi'] = array(
	$config['status_mutasi_unprocessed'] => 'Belum Diproses',
	$config['status_mutasi_processed'] => 'Berhasil',
);

// STATUS STOCK OPNAME
$config['status_so_pemeriksaan_pertama'] = 1;
$config['status_so_pemeriksaan_kedua'] = 2;


// Jenis Perkiraan
define('JENIS_PERKIRAAN_HARTA', 1);
define('JENIS_PERKIRAAN_KEWAJIBAN', 2);
define('JENIS_PERKIRAAN_MODAL', 3);
define('JENIS_PERKIRAAN_PENDAPATAN', 4);
define('JENIS_PERKIRAAN_BEBAN_ATAS_PENDAPATAN', 5);
define('JENIS_PERKIRAAN_BEBAN_OPERASIONAL', 6);
define('JENIS_PERKIRAAN_BEBAN_NON_OPERASIONAL', 7);
define('JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN', 8);
define('JENIS_PERKIRAAN_BEBAN_LAIN_LAIN', 9);
/*
|--------------------------------------------------------------------------
| Jenis Perkiraan
|--------------------------------------------------------------------------
*/

$config['jenis_perkiraan'] = array(
	JENIS_PERKIRAAN_HARTA => 'Harta',
	JENIS_PERKIRAAN_KEWAJIBAN => 'Kewajiban',
	JENIS_PERKIRAAN_MODAL => 'Modal',
	JENIS_PERKIRAAN_PENDAPATAN => 'Pendapatan',
	JENIS_PERKIRAAN_BEBAN_ATAS_PENDAPATAN => 'Beban Atas Pendapatan',
	JENIS_PERKIRAAN_BEBAN_OPERASIONAL => 'Beban Operasional',
	JENIS_PERKIRAAN_BEBAN_NON_OPERASIONAL => 'Beban Non Operasional',
	JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN => 'Pendapatan Lain-lain',
	JENIS_PERKIRAAN_BEBAN_LAIN_LAIN => 'Beban Lain-lain',
);

define('JENIS_MODUL_POS', 1);
define('JENIS_MODUL_PENERIMAAN_KAS_BANK', 2);
define('JENIS_MODUL_PENGELUARAN_KAS_BANK', 3);
define('JENIS_MODUL_GUDANG_FARMASI', 4);
define('JENIS_MODUL_LAPORAN', 5);
define('JENIS_DEBIT', 1);
define('JENIS_KREDIT', 2);