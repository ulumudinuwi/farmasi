$(() => {
    let disableForm = () => {
        FORM.find('button').prop('disabled', true);
        BTN_CANCEL.prop('disabled', false);
        BTN_RESTORE.prop('disabled', false);
        BTN_DELETE.prop('disabled', false);
    }
    let enableForm = () => {
        FORM.find('button').prop('disabled', false);
        BTN_CANCEL.prop('disabled', false);
        BTN_RESTORE.prop('disabled', false);
        BTN_DELETE.prop('disabled', false);
    }

    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#kode").val(data.kode);
                $("#nama").val(data.nama);
                $("#deskripsi").val(data.deskripsi);
                $("#disp_deskripsi").summernote('code', data.deskripsi);

                if (data.deleted_flag == 1) {
                    BTN_DELETE.hide();
                    BTN_RESTORE.show();
                    LABEL_DELETED.show();
                    disableForm();
                } else {
                    BTN_RESTORE.hide();
                    BTN_DELETE.show();
                    LABEL_DELETED.hide();
                    enableForm();
                }
            }
        });
    }

    let initialize = (uid) => {
        FORM.validate({
            rules: {
                kode: { required: true },
                nama: { required: true },
            },
            messages: {
                kode: "Kode Diperlukan.",
                nama: "Nama Diperlukan.",
            },
            focusInvalid: true,
            errorPlacement: function(error, element) {
                var inputGroup = $(element).closest('.input-group');
                var checkbox = $(element).closest('.checkbox-inline');

                if (inputGroup.length) {
                    error.insertAfter(inputGroup);
                } else if (checkbox.length) {
                    checkbox.append(error);
                } else {
                    $(element).closest("div").append(error);
                }
            },
            submitHandler: function (form) {
                blockPage();
                $("#deskripsi").val($("#disp_deskripsi").summernote('code'));
                var postData = $(form).serializeArray();
                var formData = new FormData($(form)[0]);

                for (var i = 0; i < postData.length; i++) {
                    if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                        formData.delete(postData[i].name);
                        formData.append(postData[i].name, postData[i].value);
                    }
                }

                $.ajax({
                    url: URL.save,
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (result) {
                        result = JSON.parse(result);
                        var data = result.data;
                        console.log(data);
                        successMessage('Success', "Ukuran Foto berhasil disimpan.");

                        $("#btn-simpan").hide();

                        setTimeout(() => {
                            window.location.assign(URL.index);
                        }, 3000);
                    },
                    error: function () {
                        $.unblockUI();
                        errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan ukuran foto.");
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            }
        });

        /**
         * BUTTON
         */
        BTN_DELETE.on('click', (e) => {
            let data = {
                uid: FORM.find('[name=uid]').val()
            };
            confirmDialog({
                title: 'Hapus Data Tersebut?',
                text: '',
                btn_confirm: 'Hapus',
                url: URL.delete,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Dihapus.');
                    // window.location.reload(false);
                    fillForm(FORM.find('[name=uid]').val());
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Dihapus.');
                },
            });
        });

        BTN_RESTORE.on('click', (e) => {
            let data = {
                uid: FORM.find('[name=uid]').val()
            };
            confirmDialog({
                title: 'Restore Data Tersebut?',
                text: '',
                btn_confirm: 'Restore',
                url: URL.restore,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Direstore.');
                    // window.location.reload(false);
                    fillForm(FORM.find('[name=uid]').val());
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Direstore.');
                },
            });
        });

        BTN_CANCEL.on('click', (e) => {
            window.location.assign(URL.index);
        });

        if (uid) {
            fillForm(uid);
        } else {
            BTN_DELETE.hide();
            BTN_RESTORE.hide();
        }
    }

    initialize(UID);
});