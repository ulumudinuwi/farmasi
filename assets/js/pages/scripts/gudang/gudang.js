function showHistoryBarang(url, columns) {
  if(columns == null) {
    columns = [
      { "data": "tipe" },
      {
        "data": "tanggal",
        "render": function (data, type, row, meta) {
          let tmp = moment(data).format('DD-MM-YYYY HH:mm');
          return tmp;
        },
      },
      { "data": "kode" },
      { 
          "data": "qty",
          "render": function (data, type, row, meta) {
              return numeral(data).format('0.0,');
          },
          "className": "text-right"
      },
      { 
          "data": "sisa_sum",
          "render": function (data, type, row, meta) {
              return numeral(data).format('0.0,');
          },
          "className": "text-right"
      },
    ];
  }

  $.getJSON(url.replace(':UID', uid), function (data, status) {
    if (status === 'success') {
      data = data.data;

      $("#detail-history-barang").html(data.barang);
      $("#detail-history-satuan").html(data.satuan);
      
      var isDTable = $.fn.dataTable.isDataTable($("#table-history-barang"));
      if(isDTable === true) $("#table-history-barang").DataTable().destroy();

      $("#table-history-barang").DataTable({
        "info": false,
        "ordering": false,
        "paginate": false,
        "processing": true,
        "aaData": data.details,
        "columns": columns
      });
      $("#history-barang-modal").modal('show');
    }
  });
}

function showDetailStock(obj) {
  let title = obj.barang +
      `<br/><span class="text-size-mini text-bold">Satuan: ${obj.satuan}</span>`;
  $('#detail-stock-modal').find('.modal-title').html(title);
  $.getJSON(obj.url, function (data, status) {
      if (status === 'success') {
        data = data.data;

        var isDTable = $.fn.dataTable.isDataTable($('#table_detail_stock'));
        if(isDTable === true) $('#table_detail_stock').DataTable().destroy();

        $('#table_detail_stock').DataTable({
          "ordering": false,
          "processing": true,
          "data": data,
          "columns": [
              { 
                "data": "no_batch",
              },
              {
                "data": "expired_date",
                "render": function (data, type, row, meta) {
                    return data ? moment(data).format('DD/MM/YYYY') : '&mdash;';
                },
              },
              { 
                "data": "harga",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
              },
              { 
                "data": "qty",
                "render": function (data, type, row, meta) {
                    return numeral(data).format('0.0,');
                },
                "className": "text-right"
              },
          ]
        });
        $('#detail-stock-modal').modal('show');
      }
  });
  $('#detail-stock-modal').modal('show');
}