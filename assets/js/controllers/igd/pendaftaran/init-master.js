$(document).ready(function() {

    $('#tgl_lahir, #tgl_register').formatter({
        pattern: '{{99}}/{{99}}/{{9999}}'
    });
    $('[name="tgl_lahir"]').on('blur', function() {
        var dateFormat = "DD/MM/YYYY";
        var valDate = $(this).val();
        if (moment(valDate, dateFormat, true).isValid() === false) {
            $('#tgl_lahir-error').removeClass("validation-valid-label");
            return false;
        }
    });
    $('[name="tgl_register"]').on('blur', function() {
        var dateFormat = "DD/MM/YYYY";
        var valDate = $(this).val();
        if (moment(valDate, dateFormat, true).isValid() === false) {
            $('#tgl_register-error').removeClass("validation-valid-label");
            return false;
        }
    });






    function loadDefaultTindakan(val) {
        $('#default-layanan tbody').empty();
        var urlToLoad = base_url + "api/master/tindakan/pendaftaran?newPatient=";
        $.ajax({
            url: urlToLoad + val,
            type: 'GET',
            dataType: 'json',
            success: function(json) {
                if (json.status === '1') {
                    $.each(json.result, function(i, value) {
                        $('#default-layanan tbody').append('<tr>' +
                            '<td>' + '<input type="hidden" name="layayan_id[]"" value="' + value.id + '" />  ' + value.uraian + '</td>' +
                            '<td>' + value.biaya + '</td>' +
                            '<td class="text-center;"><button class="btn btn-default vmt-button-small" type="button"><i class="icon-bin vmt-icon-small"></i></button></td>' +
                            '</tr>');
                    });
                }
            }
        });
    };

    $.ajax({
        url: base_url + "api/master/identitas",
        type: 'GET',
        dataType: 'json',
        success: function(json) {
            $('#identitas_id').append($('<option>').text('').attr('value', ''));
            $.each(json.result, function(i, value) {
                $('#identitas_id').append($('<option>').text(value.nama).attr('value', value.id));
            });
        }
    });
    $.ajax({
        url: base_url + "api/master/provinsi",
        type: 'GET',
        dataType: 'json',
        success: function(json) {
            $('#provinsi_id').append($('<option>').text('').attr('value', ''));
            $.each(json.result, function(i, value) {
                $('#provinsi_id').append($('<option>').text(value.nama).attr('value', value.id));
            });
        }
    });

    var urlApiAgama = base_url + "api/master/agama";
    $(document).getDropBoxApi(urlApiAgama, 'agama_id');

    var urlApiPendidikan = base_url + "api/master/pendidikan";
    $(document).getDropBoxApi(urlApiPendidikan, 'pendidikan_id');

    var urlApiPekerjaan = base_url + "api/master/pekerjaan";
    $(document).getDropBoxApi(urlApiPekerjaan, 'pekerjaan_id');

    var urlApiRujukan = base_url + "api/master/rujukan";
    $(document).getDropBoxApi(urlApiRujukan, 'rujukan_id');

    var urlApiKelPasien = base_url + "api/master/kelompok_pasien";
    $(document).getDropBoxApi(urlApiKelPasien, 'kelompokpasien_id');

    var urlApiPaket = base_url + "api/master/paket_jaminan";
    $(document).getDropBoxApi(urlApiPaket, 'paket_id');

    var urlApiDokter = base_url + "api/master/pegawai";
    $(document).getDropBoxApi(urlApiDokter, 'dokter_id');

    var urlApiHubPasien = base_url + "api/master/hubungan_pasien";
    $(document).getDropBoxApi(urlApiHubPasien, 'hubpasien_id');


    $("#provinsi_id").on('change', function() {
        $('#kabupaten_id').empty();
        provinsiId = this.value;
        $.ajax({
            url: base_url + "api/master/kota/",
            type: 'GET',
            data: {
                'provinsiId': provinsiId
            },
            dataType: 'json',
            success: function(json) {
                $('#kabupaten_id').append($('<option>').text('').attr('value', ''));
                $.each(json.result, function(i, value) {
                    $('#kabupaten_id').append($('<option>').text(value.nama).attr('value', value.id));
                });
            }
        });
    });

    $("#kabupaten_id").on('change', function() {
        $('#kecamatan_id').empty();
        kotaId = this.value;
        $.ajax({
            url: base_url + "api/master/kecamatan",
            type: 'GET',
            data: {
                'kotaId': kotaId
            },
            dataType: 'json',
            success: function(json) {
                $('#kecamatan_id').append($('<option>').text('').attr('value', ''));
                $.each(json.result, function(i, value) {
                    $('#kecamatan_id').append($('<option>').text(value.nama).attr('value', value.id));
                });
            }
        });
    });

    $("#kecamatan_id").on('change', function() {
        $('#kelurahan_id').empty();
        kecamatanId = this.value;
        $.ajax({
            url: base_url + "api/master/kelurahan",
            type: 'GET',
            data: {
                'kecamatanId': kecamatanId
            },
            dataType: 'json',
            success: function(json) {
                $('#kelurahan_id').append($('<option>').text('').attr('value', ''));
                $.each(json.result, function(i, value) {
                    $('#kelurahan_id').append($('<option>').text(value.nama).attr('value', value.uid));
                });
            }
        });
    });

});