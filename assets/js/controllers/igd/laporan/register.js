/**
 * Created by agungrizkyana on 6/12/17.
 */
var formFilter,
    tanggalDari,
    tanggalSampai;

$(document).ready(function(){
    formFilter = $("#form-filter");
    tanggalDari = $("#tanggal_dari");
    tanggalSampai = $("#tanggal_sampai");

    tanggalDari.datepicker({
        autoclose: true
    });

    tanggalSampai.datepicker({
        autoclose: true
    });


    formFilter.submit(function(e){
        e.preventDefault();
        window.location.href = base_url + '/igd/laporan/register/data?' + formFilter.serialize();
        return;
    })
});

function logError(err){
    console.log(err);
}