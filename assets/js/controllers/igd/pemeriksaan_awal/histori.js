app.controller('PemeriksaanAwalFormController', ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$location',
    function ($rootScope, $scope, api, $http, CONFIG, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $location) {

        var url = base_url + '/api/igd/pemeriksaan_awal/list_rawat_jalan/0_1';
        $scope.rows = [];

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('ajax', {
                url: url,
                type: 'POST',
                data: function (data, dtInstance) {

                }
            })
            // or here
            .withDataProp('data')
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('order', [[0, 'desc']])
            .withOption('createdRow', function (row) {
                $rootScope.$emit('ajax:stop');
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);

            })
            .withPaginationType('full_numbers');

        $scope.dtInstance = {};
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {
                return moment(row.tanggal).format('DD-MMM-YYYY H:mm:s');
                // return '<a href="' + base_url + '/rawat_jalan/pemeriksaan_awal?s=' + row.pelayanan_id + '&rawat_jalan=' + row.id + '">' + moment(row.tanggal).format('DD-MMM-YYYY H:mm:s') + '</a>';
            }).withOption('searchable', false),
            DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No RM'),
            DTColumnBuilder.newColumn('no_register').withTitle('No Register').renderWith(function (data, type, row, meta) {
                $scope.rows.push(row);
                return '<a  ng-click="openDetailPasien(' + row.id + ')">' + data + '</a>';
            }),
            DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
            DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
            DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter')
        ];

        $scope.dtInstance = {};


        $scope.openDetailPasien = function (row) {

            var obj = _.find(angular.copy($scope.rows), {id: row.toString()});

            $scope.modalData = obj;

            console.log(obj);

            if (obj.diagnosa) {
                $http.get(base_url + "/api/rawat_jalan/pemeriksaan_dokter/icd10_ids", {
                    params: {
                        ids: $scope.rows[idx].diagnosa
                    }
                }).then(function(result){
                    $scope.rows[idx].diagnosa_text = result.data.diagnosa;
                    $scope.modalData.fromNow = moment($scope.modalData.created_at).fromNow();
                    $('#modal_detail').modal();
                }, function(err){
                    console.log(err);
                });
            } else {
                $scope.modalData.fromNow = moment($scope.modalData.created_at).fromNow();
                $('#modal_detail').modal();
            }




        };


    }]);