/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddBphpController', modalAddBphpController);

/**
 * Modal Add BHP / Alkes
 * @type {string[]}
 */
modalAddBphpController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', '$log', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'items'];
function modalAddBphpController($scope, api, $http, $uibModalInstance, $log, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, items) {

    console.log("items", items);

    var url = base_url + "/api/master/obat/bhp/" + items.layanan_id;
    var _tarif = [];
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            // Either you specify the AjaxDataProp here
            // dataSrc: 'data',
            url: url,
            type: 'POST'
        })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('serverSide', true)
        .withPaginationType('full_numbers');
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('kode_obat').withClass('row-5 text-center').withTitle('Pilih').notSortable().renderWith(function (data, type, full, meta) {
            return '<input type="checkbox"  ng-model="tarif' + full.id + '" ng-click="selectTarif(tarif' + full.id + ',' + full.id + ')"/>';
        }),
        DTColumnBuilder.newColumn('kode_obat').withTitle('Kode').withClass('row-10'),
        DTColumnBuilder.newColumn('obat').withTitle('Obat'),
        DTColumnBuilder.newColumn('jumlah').withTitle('Stock'),
        DTColumnBuilder.newColumn('satuan').withTitle('Satuan'),
    ];

    $scope.selectTarif = function (data, id, str) {
        console.log(id);
        if (data) {
            $http.get(base_url + '/api/master/obat/cek_tarif_bhp', {
                params: {
                    id: id,
                    pelayanan_id: items.id,
                    layanan: items.layanan_id
                }
            }).then(function (tarif) {
                _tarif.push(tarif.data);
            })
        } else {
            _.remove(_tarif, function (data) {
                return data.id == id;
            });
        }
    };

    $scope.close = function () {
        console.log("data : ", angular.copy(_tarif));
        $uibModalInstance.close(_tarif);
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

}

