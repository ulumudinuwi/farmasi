/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddNonRacikanController', modalAddRacikanController);

/**
 * Modal Add Racikan
 * @type {string[]}
 */
modalAddRacikanController.$inject = ['$rootScope', '$scope', 'api', '$http', '$uibModalInstance', '$log', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'items', '$filter', '$resource'];
function modalAddRacikanController($rootScope, $scope, api, $http, $uibModalInstance, $log, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, items, $filter, $resource) {

    var url = base_url + "/api/master/obat/non_racikan/" + items.pasien.kelompok_pasien_id;
    var _tarif = [];

    $rootScope.searchObat = base_url + '/api/master/obat/non_racikan?kelompok_pasien='+items.pasien.kelompok_pasien_id+'&s=';

    $scope.dataObat = [];
    $scope.obat = {
        catatan: items.catatan
    };

    $scope.selectedObat = selectedObat;
    $scope.remove = remove;
    $scope.selectCallback = selectCallback;

    $scope.total = 0;

    $scope.close = function () {
        console.log("data : ", angular.copy({
            obat : $scope.dataObat,
            catatan: $scope.obat.catatan
        }));
        $uibModalInstance.close({
            obat : $scope.dataObat,
            catatan: $scope.obat.catatan
        });
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onChangeQty = onChangeQty;
    $scope.onChangeDiskon = onChangeDiskon;

    $scope.$on('CHANGE:TOTAL', onChangeTotal);

    if(items.isEdit){
        console.log("edit obat nonracikan", items.dataObat);
        $scope.dataObat.push(items.dataObat);

        onChangeTotal();
    }

    $scope.tags = [];
    $scope.loadTags = function(query) {
        return $http.get(base_url + '/api/master/signa/search?q=' + query);
    };

    function onChangeQty(idx, qty) {
        if (qty >= 1) {
            $scope.dataObat[idx].jumlah = $scope.dataObat[idx].harga;
            $scope.$emit('CHANGE:TOTAL');
        }
    }

    function onChangeDiskon(idx, diskon) {
        if (diskon) {
            if (diskon >= 1) {
                var qty = $scope.dataObat[idx].qty;
                var harga = qty * $scope.dataObat[idx].harga;
                $scope.dataObat[idx].jumlah = harga - ((harga / diskon) * 100);
                $scope.$emit('CHANGE:TOTAL');
            }
        }
    }

    function selectedObat(item) {
        if (item) {
            var obat = item.originalObject;
            obat.qty = 1;
            obat.diskon = 0;
            obat.jumlah = obat.harga;
            obat.signa = [];
            $scope.dataObat.push(obat);

            $scope.total = _.sum(_.map($scope.dataObat, function (val) {
                return parseInt(val.jumlah);
            }));
        }
    }

    function selectCallback(val) {

        $scope.signa = val;

    }

    function remove(index) {
        $scope.dataObat.splice(index, 1);
    }

    function onChangeTotal() {
        $scope.total = _.sum(_.map($scope.dataObat, function (val) {
            return parseInt(val.jumlah);
        }));
    }


}

