/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddFisioterapiController', modalAddLaboratorium);
modalAddLaboratorium.$inject = ['$rootScope', '$scope', 'api', '$http', '$uibModalInstance', '$log', '$uibModal', 'items', 'toastr', '$timeout', '$httpParamSerializer'];
function modalAddLaboratorium($rootScope, $scope, api, $http, $uibModalInstance, $log, $uibModal, items, toastr, $timeout, $httpParamSerializer) {

    $http.get(base_url + '/api/rujukan_lab/fisioterapi/get_last_uid?uid=' + items.pasien.fisioterapi_uid).then(function (result) {

        var tindakan = [];
        var isBatal = false;

        $scope.rujukan = {};

        $scope.rujukanId = -1;

        var diagnosa = result.data.response ? result.data.response.diagnosa : '';

        if(result.data.response){
            var rujukan = JSON.parse(result.data.response.rujukan);
            if(rujukan.rujuk_fisioterapi == 3){
                $scope.rujukanId = -1;
                isBatal = false;
            }else if(rujukan.rujuk_fisioterapi == 2 || rujukan.rujuk_fisioterapi == 1){
                $scope.rujukanId = result.data.response.id;
                isBatal = true;
            }
        }

        $scope.urlTindakan = base_url + '/api/penunjang_medik/fisioterapi_pendaftaran/load_data_tindakan';
        $scope.initTindakan = items.tindakan;

        $scope.data = items.pasien;
        $scope.data.diagnosa = diagnosa;
        $scope.data.tanggalPendaftaran = moment().format('DD/MM/YYYY');
        $scope.data.kelamin = $scope.data.jenis_kelamin == 1 ? 'Laki - Laki' : 'Perempuan';
        $scope.data.rujukan.fisioterapi = (isBatal) ? 1 : 0;

        $scope.data.tindakan = [];

        $scope.choose = {};
        $scope.qty = 1;
        $scope.labs = [];

        $scope.save = save;
        $scope.batal = batal;
        $scope.close = close;
        $scope.dismiss = dismiss;

        $http.get(base_url + '/api/master/kelas').then(function (result) {
            $scope.dataKelas = result.data.response;
        }).catch(function (err) {
            toastr.error('Gagal mengambil kelas', 'Error');
        });

        $scope.selectedNodes = function (nodes) {

            console.log("selected nodes", nodes);

            if (nodes) {
                // console.log("length of children", nodes.children);
                if (nodes.children.length > 0) {
                    _.each(nodes.children, function (child) {
                        $scope.data.tindakan.push(child);
                    });

                    $scope.$apply();
                }
            }

        };

        $scope.unSelectedNodes = function(nodes){

            var index = _.findIndex($scope.data.tindakan, {id: nodes.id});
            console.log("index delete", index);

            $scope.data.tindakan.splice(index, 1);
            $scope.$apply();
        };

        $scope.delete = function (i) {
            $scope.labs.splice(i, 1);
        };

        $scope.selected = function (item) {
            if (item) {
                $scope.labs.push(item.originalObject);
                console.log($scope.labs);
            }
        };

        $scope.onChoose = function (tindakan, qty) {
            // console.log(tindakan);
            $scope.choose = tindakan;
        };

        $scope.openModalSubLaboratorium = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalAddSubLaboratorium.html',
                controller: 'ModalAddSubLaboratoriumController',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataObat.push(selectedItem);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        function batal() {
            $.ajax({
                url: base_url + '/api/fisioterapi_pendaftaran/batal?uid=' + result.data.response.uid,
                method: 'get'
            }).then(function (result) {
                toastr.success('Sukses Batalkan Rujukan Fisioterapi', 'Sukses !');
                $timeout(function () {
                    close([]);
                }, 500);
            }, function (err) {
                toastr.error('Gagal Batal Rujukan Fisioterapi', 'Gagal !');
            });

        }

        function save() {
            // marshall


            console.log("data tindakan, ", {
                tindakan: angular.copy($scope.data)
            });

            var req = {
                method: 'POST',
                url: base_url + '/api/rujukan_lab/fisioterapi/save',
                data: $httpParamSerializer({data: angular.copy($scope.data)}),
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                }
            };
            $http(req).then(function (result) {
                console.log(result.data.response);
                $scope.rujukan = result.data.response;
                toastr.success('Berhasil Daftar Fisioterapi', 'Sukses!');
                $timeout(function () {
                    close({
                        uid: result.data.response,
                        data: angular.copy($scope.data.tindakan)
                    });
                }, 500);
            }, function (err) {
                console.log(err);
                toastr.error('Terjadi Kesalahan Sistem', 'Gagal');
            });
        }

        function close(data) {

            console.log("data : ", angular.copy(data));
            $uibModalInstance.close(angular.copy(data));
        }

        function dismiss() {
            $uibModalInstance.dismiss();
        }
    });
}