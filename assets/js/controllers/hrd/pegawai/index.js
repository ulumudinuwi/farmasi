app.controller('DaftarPegawaiIndexController', ['$scope', 'api', '$http', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$q', '$uibModal',
    function($scope, api, $http, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $q, $uibModal, $location) {


        $scope.data = {};
        $scope.umum = {};

        $scope.data = {

            tanggalDari: moment().format('DD/MM/YYYY'),
            tanggalSampai: moment().format('DD/MM/YYYY')

        };

        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/wilayah/provinsi"),
                $http.get(base_url + "/api/master/wilayah/kota/", {
                    params: {
                        'provinsiId': "9" // default kepulauan bangka belitung
                    }
                }),
                $http.get(base_url + "/api/master/identitas")

            ]).then(function(results) {
                $scope.alamatProvinsi = angular.copy(results[0].data);
                $scope.alamatKota = angular.copy(results[1].data);
                $scope.dataIdentitas = angular.copy(results[2].data);

            }).catch(function(err) {
                console.log(err);
            });
        }

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        initData();



        $scope.onChangeRangeWaktu = function(range) {
            var tanggalDari = moment(range.substr(0, 10)).format('YYYY-MM-DD'),
                tanggalSampai = moment(range.substr(13, 10)).format('YYYY-MM-DD');
            buildTable(null, {
                tanggalDari: tanggalDari,
                tanggalSampai: tanggalSampai
            });
        };

        $scope.openProfile = function(id) {

            window.location.href = base_url + '/master/pegawai/profile/' + id;
            //  console.log( $location.path());
        };

        $scope.dateFilter = function() {
            console.log('filter', {
                tanggalDari: moment(angular.copy($scope.data.tanggalDari)).format('YYYY-MM-DD'),
                tanggalSampai: moment(angular.copy($scope.data.tanggalSampai)).format('YYYY-MM-DD')
            });

            buildTable(null, {
                tanggalDari: moment(angular.copy($scope.data.tanggalDari)).format('YYYY-MM-DD'),
                tanggalSampai: moment(angular.copy($scope.data.tanggalSampai)).format('YYYY-MM-DD')
            });
        };

        //init datatable
        // console.log(base_url + '/api/rawat_jalan/daftar_kunjungan/data_daftar_kunjungan');

        function buildTable(noRekamMedis, dateFilter) {
            //var url = (noRekamMedis) ? base_url + '/api/rawat_jalan/daftar_kunjungan/data_daftar_kunjungan?no_rm=' + noRekamMedis : base_url + '/api/hrd/daftar_pegawai/data_daftar_pegawai';
            var url = base_url + '/api/hrd/daftar_pegawai/data_daftar_pegawai';

            // if (dateFilter) {
            //     url += "?tanggalDari=" + dateFilter.tanggalDari + "&tanggalSampai=" + dateFilter.tanggalSampai
            // }
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax', {
                    url: url,
                    type: 'POST',
                })
                // or here
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('order', [
                    [0, 'desc']
                ])
                .withOption('createdRow', function(row) {
                    // Recompiling so we can bind Angular directive to the DT
                    $compile(angular.element(row).contents())($scope);
                })
                .withPaginationType('full_numbers');

            $scope.dtInstance = {};
            $scope.dtColumns = [

                DTColumnBuilder.newColumn('nik').withTitle('NIP').withClass('text-center').renderWith(function(data, type, row, meta) {
                    return '<a ng-click="openProfile(\'' + row.uid + '\')"">' + row.nik + '</a>';
                }).withOption('searchable', true),
                DTColumnBuilder.newColumn('nama').withTitle('NAMA'),
                DTColumnBuilder.newColumn('tgl_kerja').withTitle('TGL GABUNG').withClass('text-center').renderWith(function(data, type, row, meta) {
                    return moment(row.tgl_kerja).format('DD-MMM-YYYY ');
                }).withOption('searchable', false),
                DTColumnBuilder.newColumn('unit_kerja').withTitle('UNIT KERJA'),
                DTColumnBuilder.newColumn('departemen').withTitle('DEPARTEMEN'),
                DTColumnBuilder.newColumn('jabatan').withTitle('JABATAN').withOption('searchable', true)

            ];
            console.log($scope.dtInstance);
        }

        buildTable();


        $scope.reload = function() {
            // $scope.dtInstance._renderer.rerender();

        }

    }
]);