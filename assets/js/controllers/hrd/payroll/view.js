var the_table;

var pegawai_id          = '';
var periode_id          = '';
var tahun               = '';
var unit_usaha          = '';
var bulan               = '';
var gaji_income_id      = '';
var id_gaji             = '';
var list_delete         = [];

$unitusaha              = $('#unit_usaha');
$bulan                  = $('#bulan');
$tahun                  = $('#tahun');
$periode                = $('#periode');
$titleperiode           = $('#title-periode');

$modalperiode           = $('#modal-periode');
$modalpegawai           = $('#modal-pegawai');
$modalpegawaiid         = $('#modal-pegawai-id');
$btneditslip            = $('#btn-edit-slip');

$modaleditperiode       = $('#modal-edit-periode');
$modaleditpegawai       = $('#modal-edit-pegawai');
$modaleditpegawaiid     = $('#modal-edit-pegawai-id');
$modaltotaledit         = $('#edit-total-amount');
$modaltotaldeduction    = $('#edit-total-deduction');
$modaltotalhomepay      = $('#edit-total-home-pay');

$formedit               = $('#form-edit-slip-income');
$counterIncome          = $('#counter-income');

$formeditD              = $('#form-edit-slip-deduction');
$counterDeduction       = $('#counter-deduction');

//Komponen Detail
$gajiincome             = $('#gaji-income');
$gajideduction          = $('#gaji-deduction');

$totalgajiincome        = $('#total-gaji-income');
$totalgajideduction     = $('#total-gaji-deduction');
$takehomepay            = $('#take-home-pay');

var page_url = '/api/hrd/payroll/get_personil';
$(function(){
    $(document).find('.autonumber').autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
    $unitusaha.change(function(){
        unitusaha   = $unitusaha.val();
        bulan  = $bulan.val();
        tahun   = $tahun.val();
        get_data(bulan,tahun,unitusaha);
    });

    $bulan.change(function(){
        unitusaha   = $unitusaha.val();
        bulan  = $bulan.val();
        tahun   = $tahun.val();
        get_data(bulan,tahun,unitusaha);
    });

    $tahun.change(function(){
        unitusaha   = $unitusaha.val();
        bulan  = $bulan.val();
        tahun   = $tahun.val();
        get_data(bulan,tahun,unitusaha);
    });

    unitusaha   = $unitusaha.val();
    bulan       = $bulan.val();
    tahun       = $tahun.val();
    get_data(bulan,tahun,unitusaha);

    $("#komponen").remoteChained({
        parents : "#jenis",
        url : base_url+"/api/hrd/payroll/get_komponen"
    });
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function nama_bulan(bln)
{
    bulan = [];
    bulan['01'] = 'Januari';
    bulan['02'] = 'Februari';
    bulan['03'] = 'Maret';
    bulan['04'] = 'April';
    bulan['05'] = 'Mei';
    bulan['06'] = 'Juni';
    bulan['07'] = 'Juli';
    bulan['08'] = 'Agustus';
    bulan['09'] = 'September';
    bulan['10'] = 'Oktober';
    bulan['11'] = 'November';
    bulan['12'] = 'Desember';
    return bulan[bln];
}

function get_data(bulan,tahun,unitusaha){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    $periode.text(nama_bulan(bulan)+' '+tahun);
    the_table = $("#dataTable_lowongan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+page_url,
            "type": "POST",
            "dataType":"json",
            "data": {bulan:bulan,tahun:tahun,unit_usaha:unitusaha}
        },
        "columns": [
            {
                "data": "nama",
                "render": function(data, type, row, meta)
                {
                    return '<i class="icon-user"></i><span class="pegawai">'+data+'</span><br><span class="nik">'+row.nik+'</span><input class="pegawai_id" type="hidden" value="'+row.pegawai_id+'">';
                }
            },
            {
                "data": "status_absensi",
                "searchable": false
            },
            {
                "data": "bayar",
                "searchable": false,
                "orderable": false,
                "render": function(data, type, row, meta)
                {
                    belum       = 'disabled';
                    classBelum  = 'btn-primary';
                    sudah       = '';
                    classSudah  = 'btn-default';
                    if(data == 1)
                    {
                        belum       = '';
                        classBelum  = 'btn-default';
                        sudah       = 'disabled';
                        classSudah  = 'btn-primary';
                    }

                    return  '<div class="btn-group">'+
                            '<button type="button" onclick="bayar(this,'+row.id+')" value="0" class="bayar-belum btn '+classBelum+'" '+belum+'>Belum</button>'+
                            '<button type="button" onclick="bayar(this,'+row.id+')" value="1" class="bayar-sudah btn '+classSudah+'" '+sudah+'>Sudahkan</button>'+
                            '</div>';
                }
            },
            {
                "data": "aksi",
                "searchable": false,
                "orderable": false,
                "render": function(data, type, row, meta) {
                    return '<button type="button" href="#'+row.id+'" onclick="get_slip(this)" class="btn btn-link">Lihat Slip</button>';
                }
            }

        ]
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}

var element_pegawai = '';
function get_slip(element){
    element_pegawai = element;
    $tr             = $(element).parent().parent();

    hrefId          = $(element).attr('href');
    var id          = hrefId.replace('#','');

    pegawai     = $tr.find('.pegawai').text();
    nik         = $tr.find('.nik').text();
    pegawai_id  = $tr.find('.pegawai_id').val();
    bulan       = $bulan.find(':selected').text();
    tahun       = $tahun.find(':selected').text();

    console.log('pegawai_id ',pegawai_id);
    console.log('nik ',nik);
    $btneditslip.val(id);
    $modalpegawai.text(pegawai);
    $modalpegawaiid.text('('+nik+')');
    $modalperiode.text($titleperiode.text());
    var link_cetak  = base_url+'/hrd/payroll/print_slip_gaji/?bulan='+bulan+'&tahun='+tahun+'&nama_pegawai='+pegawai+'&nik_pegawai='+nik+'&gaji_income_id='+id+'&pegawai_id='+pegawai_id;
    $.ajax({
        url: base_url+'/api/hrd/payroll/get_slip',
        type: 'POST',
        data: {
            gaji_income:id,
        },
        dataType: 'JSON',
        success : function(data){
            console.log(data);
            $gajiincome.find('tbody').html('');
            var totalIncome = 0;
            data.income.forEach(function(v,i){
                if(v.amount != null){
                    jml = parseFloat(v.amount);
                    console.log('jml', jml);
                    totalIncome += jml;
                }

                var unit = '';
                var rate = '';

                var amount = 'Rp '+numeral(v.amount).format('0,0');

                if(v.tipe != 'pokok' && v.tipe != 'tetap')
                {
                    if(v.rate > 0){
                        rate = 'Rp '+numeral(v.rate).format('0,0');
                    }

                    if(v.unit > 0)
                    {
                        if(v.tipe == 'wajib' || v.komponen == 'Tunjangan Fungsional')
                            unit = numeral(v.unit).format('0,0.00')+'%';
                        else
                            unit = v.unit;
                    }
                }
                else
                {
                    if(v.tipe == 'tetap' && v.komponen == 'Tunjangan Fungsional')
                    {
                        rate = 'Rp '+numeral(v.rate).format('0,0');
                        unit = numeral(v.unit).format('0,0.00')+'%';
                    }
                }

                console.log(v);
                var komponen = v.komponen_nama;
                if(komponen == null)
                    komponen = v.komponen;
                $gajiincome.find('tbody').append('<tr><td>'+komponen+'</td><td>'+unit+'</td><td class="text-right">'+rate+'</td><td class="text-right">'+amount+'</td></tr>');
            });
            $totalgajiincome.text('Rp '+numeral(totalIncome).format('0,0')).addClass('text-right');

            $gajideduction.find('tbody').html('');
            var totalDeduction = 0;
            data.deduction.forEach(function(v,i){
                if(v.amount != null){
                    jml = parseFloat(v.amount);
                    console.log('jml', jml);
                    totalDeduction += jml;
                }
                var unit = '';
                var rate = '';

                var amount = 'Rp '+numeral(v.amount).format('0,0');

                if(v.rate > 0)
                    rate = 'Rp '+numeral(v.rate).format('0,0');

                if(v.unit > 0)
                    unit = v.unit;

                console.log(v);
                var komponen = v.komponen_nama;
                if(komponen == null)
                    komponen = v.komponen;
                $gajideduction.find('tbody').append('<tr><td>'+komponen+'</td><td class="text-right">'+amount+'</td></tr>');
            });
            $totalgajideduction.text('Rp '+numeral(totalDeduction).format('0,0')).addClass('text-right');

            grandTotal = totalIncome - totalDeduction;
            $takehomepay.text('Rp '+numeral(grandTotal).format('0,0')).addClass('text-right');
            $('#detail_modal').find('#btn-cetak').prop('href',link_cetak);
            $('#detail_modal').modal('show');
        }
    });
}

function bayar(element,id){
    $tr = $(element).parent().parent();

    var nilai = $(element).val();
    $.ajax({
        url: base_url+'/api/hrd/payroll/bayar',
        type: 'POST',
        data: {
            gaji_income:id,
            nilai:nilai
        },
        dataType: 'JSON',
        success : function(data){
            console.log(data);
            if(data)
            {
                if(nilai == 1)
                {
                    $tr.find('.bayar-belum').prop('disabled',false);
                    $tr.find('.bayar-sudah').prop('disabled',true);

                    $tr.find('.bayar-belum').removeClass('btn-primary');
                    $tr.find('.bayar-belum').addClass('btn-default');

                    $tr.find('.bayar-sudah').removeClass('btn-default');
                    $tr.find('.bayar-sudah').addClass('btn-primary');
                }
                else
                {
                    $tr.find('.bayar-belum').prop('disabled',true);
                    $tr.find('.bayar-sudah').prop('disabled',false);

                    $tr.find('.bayar-belum').removeClass('btn-default');
                    $tr.find('.bayar-belum').addClass('btn-primary');

                    $tr.find('.bayar-sudah').removeClass('btn-primary');
                    $tr.find('.bayar-sudah').addClass('btn-default');
                }
            }
            else
            {
                alert('Transaksi Gagal');
            }
        }
    });
}

function edit_slip(element){
    var id = $(element).val();
    var data = [];
    $modaleditperiode.text($modalperiode.text());
    $modaleditpegawai.text($modalpegawai.text());
    $modaleditpegawaiid.text($modalpegawaiid.text());
    data['id'] = id;
    id_gaji = id;
    request_slip(data,function(){
        $('#edit_modal').modal('show');
    });
}

function request_slip(data,cb)
{
    console.log(data);
    list_delete = [];
    $.ajax({
        url: base_url+'/api/hrd/payroll/get_slip',
        type: 'POST',
        data: {
            gaji_income:data['id'],
        },
        dataType: 'JSON',
        success : function(data){
            console.log(data);
            periode_id      = data.periode_id;
            unit_usaha      = data.unit_usaha;
            bulan           = data.bulan;
            gaji_income_id  = data.id;

            $formedit.find('tbody').html('');
            var totalIncome = 0;
            var counter = 1;
            data.income.forEach(function(v,i){
                console.log(v)
                if(v.amount != null){
                    jml = parseFloat(v.amount);
                    console.log('jml', jml);
                    totalIncome += jml;
                }
                var amount = 0;
                if(v.amount != null)
                    amount = v.amount;
                var rate = 'Rp '+numeral(v.rate).format('0,0');
                console.log('amount ',amount);

                if(v.tipe == 'pokok')
                {

                    var trHtml = '<tr>'+
                        '<td style="width:200px;">'+v.komponen_nama+'</td>'+
                        '<td colspan="3">'+
                            '<div class="input-group">'+
                                '<span class="input-group-addon">Rp.</span>'+
                                '<input type="text" class="form-control autonumber" onkeyup="convert_to_value(this)" name="income['+counter+'][amount]" value="'+amount+'">'+
                                '<input type="hidden" name="income['+counter+'][id]" value="'+v.id+'">'+
                                '<input type="hidden" name="income['+counter+'][amount]" class="amount" value="'+v.amount+'">'+
                            '</div>'+
                        '</td>'+
                    '</tr>';
                }
                else
                {

                    var komponen = v.komponen_nama;
                    if(komponen == null)
                        komponen = v.komponen;

                    var trHtml = '<tr>';


                    if(v.tipe == "tunjangan")
                    {
                        trHtml += '<td>'+komponen+'</td>'+
                        '<td style="width: 100px;">'+
                        '<input type="number" class="form-control" onchange="hitung_amount(this)" name="income['+counter+'][unit]" value="'+v.unit+'">'+
                        '<input type="hidden" class="rate" value="'+v.rate+'">'+
                        '</td>'+
                        '<td style="width: 150px;">'+rate+'</td>';
                    }
                    else
                    {
                        if(v.komponen == 'Tunjangan Fungsional')
                        {
                            var checked = '';
                            if(v.unit == 80)
                            {
                                checked = 'checked="true"';
                            }
                            trHtml += '<td>'+komponen+'</td>'+
                            '<td style="width: 100px;">'+
                            '<div class="checkbox">'+
                            '<label>'+
                            '<input type="checkbox" '+checked+' onclick="hitung_amount_persen(this)" name="income['+counter+'][unit]" value="80">80%'+
                            '<input type="hidden" class="rate" value="'+v.rate+'">'+
                            '</label>'+
                            '</div>'+
                            '</td>'+
                            '<td style="width: 150px;">'+rate+'</td>';
                        }
                        else
                        {
                            trHtml +=
                            '<td style="width:200px;" colspan="3">'+komponen+
                                '<button type="button" class="btn btn-link text-danger" onclick="hapus_income(this)"><i class="fa fa-trash-o"></i></button>'+
                            '</td>';
                        }
                    }

                    trHtml +=
                        '<td>'+
                            '<div class="input-group">'+
                                '<span class="input-group-addon">Rp.</span>'+
                                '<input type="text" class="form-control autonumber" disabled value="'+amount+'">'+
                                '<input type="hidden" name="income['+counter+'][amount]" class="amount" value="'+v.amount+'">'+
                                '<input type="hidden" name="income['+counter+'][id]" class="id" value="'+v.id+'">'+
                                '<input type="hidden" name="income['+counter+'][rate]" value="'+v.rate+'">'+
                            '</div>'+
                        '</td>'+
                    '</tr>';
                }

                $formedit.find('tbody').append(trHtml);
                counter++;
            });
            $counterIncome.val(counter);
            $modaltotaledit.val(numeral(totalIncome).format('0,0')).addClass('text-right');

            $formeditD.find('tbody').html('');
            var totalDeduction = 0;
            var counterD = 1;
            data.deduction.forEach(function(v,i){
                console.log(v);
                if(v.amount != null){
                    jml = parseFloat(v.amount);
                    console.log('jml', jml);
                    totalDeduction += jml;
                }

                var amount = 0;
                if(v.amount != null)
                    amount = v.amount;
                var rate = 'Rp '+numeral(v.rate).format('0,0');

                /*pegawai_id      = v.pegawai_id;
                tahun           = v.tahun;
                bulan           = v.bulan;
                periode_id      = v.periode_id;
                unit_usaha      = v.unit_usaha;
                gaji_income_id  = v.gaji_income_id;*/

                var komponen = v.komponen_nama;
                if(komponen == null)
                    komponen = v.komponen;

                var trHtml = '<tr>';
                if(v.komponen_nama != null)
                {
                    trHtml += '<td style="width:200px;">'+komponen+'</td>';
                }
                else
                {
                    trHtml +=
                    '<td style="width:200px;">'+komponen+
                        '<button type="button" class="btn btn-link text-danger" onclick="hapus_income(this)"><i class="fa fa-trash-o"></i></button>'+
                    '</td>';
                }

                trHtml +=
                    '<td>'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">Rp.</span>'+
                            '<input type="text" class="form-control autonumber" onkeyup="convert_to_value(this)" name="deduction['+counterD+'][amount]" value="'+amount+'">'+
                            '<input type="hidden" name="deduction['+counterD+'][id]" class="id" value="'+v.id+'">'+
                            '<input type="hidden" name="deduction['+counterD+'][amount]" class="amount" value="'+v.amount+'">'+
                        '</div>'+
                    '</td>';
                trHtml += '</tr>';

                $formeditD.find('tbody').append(trHtml);
                counterD++;
            });
            $counterDeduction.val(counterD);
            $modaltotaldeduction.val(numeral(totalDeduction).format('0,0')).addClass('text-right');
            var homepay = totalIncome - totalDeduction;
            $modaltotalhomepay.val(numeral(homepay).format('0,0')).addClass('text-right');
            $formedit.find('.autonumber').autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
            $formeditD.find('.autonumber').autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
            return cb();
        }
    });
}

function add_income(element,form)
{
    var tipe    = $(element).val();
    $formIncome = $('#'+form);
    var nama    = $formIncome.find('input[name=nama]').val();
    var amount  = $formIncome.find('input[name=amount]').autoNumeric('get');
    console.log(tipe);
    if(tipe == 'income')
    {
        var counter = $counterIncome.val();
        var trHtml = '<tr>'+
                            '<td colspan="3">'+nama+
                                '<button type="button" class="btn btn-link text-danger" onclick="hapus_income(this)"><i class="fa fa-trash-o"></i></button>'+
                            '</td>'+
                            '<td>'+
                                '<div class="input-group">'+
                                    '<span class="input-group-addon">Rp.</span>'+
                                    '<input type="text" class="form-control autonumber" onkeyup="convert_to_value(this)" value="'+amount+'">'+
                                    '<input type="hidden" name="income['+counter+'][amount]" class="amount" value="'+amount+'">'+
                                    '<input type="hidden" name="income['+counter+'][nama]" value="'+nama+'">'+
                                    '<input type="hidden" name="income['+counter+'][pegawai_id]" value="'+pegawai_id+'">'+
                                    '<input type="hidden" name="income['+counter+'][periode_id]" value="'+periode_id+'">'+
                                    '<input type="hidden" name="income['+counter+'][tahun]" value="'+tahun+'">'+
                                    '<input type="hidden" name="income['+counter+'][unit_usaha]" value="'+unit_usaha+'">'+
                                    '<input type="hidden" name="income['+counter+'][bulan]" value="'+bulan+'">'+
                                    '<input type="hidden" name="income['+counter+'][gaji_income_id]" value="'+gaji_income_id+'">'+
                                    '<input type="hidden" name="income['+counter+'][jenis]" value="income">'+
                                '</div>'+
                            '</td>'+
                        '</tr>';
        $formedit.find('tbody').append(trHtml);
        if(form != 'modal_form_kasbon_income')
            $formIncome.find('input[name=nama]').val('');
        $formIncome.find('input[name=amount]').val('');
        $formedit.find('.autonumber').autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
        counter++;
        $counterIncome.val(counter);
    }
    else if(tipe == 'deduction')
    {
        var counter = $counterDeduction.val();
        var trHtml = '<tr>'+
                            '<td>'+nama+
                                '<button type="button" class="btn btn-link text-danger" onclick="hapus_income(this)"><i class="fa fa-trash-o"></i></button>'+
                            '</td>'+
                            '<td>'+
                                '<div class="input-group">'+
                                    '<span class="input-group-addon">Rp.</span>'+
                                    '<input type="text" class="form-control autonumber" onkeyup="convert_to_value(this)" value="'+amount+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][amount]" class="amount" value="'+amount+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][nama]" value="'+nama+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][pegawai_id]" value="'+pegawai_id+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][periode_id]" value="'+periode_id+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][tahun]" value="'+tahun+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][unit_usaha]" value="'+unit_usaha+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][bulan]" value="'+bulan+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][gaji_income_id]" value="'+gaji_income_id+'">'+
                                    '<input type="hidden" name="deduction['+counter+'][jenis]" value="deduction">'+
                                '</div>'+
                            '</td>'+
                        '</tr>';
        $formeditD.find('tbody').append(trHtml);
        if(form != 'modal_form_kasbon_income')
            $formIncome.find('input[name=nama]').val('');
        $formIncome.find('input[name=amount]').val('');
        $formeditD.find('.autonumber').autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
        counter++;
        $counterDeduction.val(counter);
    }
}

function convert_to_value(element)
{
    var counter = $counterIncome.val();
    var value   = $(element).autoNumeric('get');
    $tr         = $(element).parent().find('input.amount').val(value);
}

function hitung_amount(element)
{
    var counter = $counterIncome.val();
    var value   = $(element).val();
    unit        = $(element).val();
    rate        = $(element).parent().find('.rate').val();

    var amount = parseFloat(unit)*parseFloat(rate);
    $tr = $(element).parent().parent().find('input.amount').val(amount);
    $tr = $(element).parent().parent().find('.input-group .autonumber').autoNumeric('set',amount);
}

function hitung_amount_persen(element)
{
    if($(element).is(':checked'))
    {
        unit        = $(element).val();
    }
    else
    {
        unit        = 100;
    }
    var counter = $counterIncome.val();
    rate        = $(element).parent().find('.rate').val();

    var amount = parseFloat(unit)*parseFloat(rate)/100;
    $tr = $(element).parent().parent().parent().parent().find('input.amount').val(amount);
    $tr = $(element).parent().parent().parent().parent().find('.input-group .autonumber').autoNumeric('set',amount);
}

function hapus_income(element)
{
    if(confirm('Apakah anda yakin ingin menghapus item ini?'))
    {
        $tr     = $(element).parent().parent();
        var id  = $tr.find('.id').val();
        console.log(id);
        if(id)
            list_delete.push(id);
        $tr.remove();
    }
}

function save_income(element)
{
    $formIncome     = $('#form-income');
    $btnSubmit      = $(element);
    var captionBtn  = $btnSubmit.text();

    var target_url  = $formIncome.attr('action');
    var data        = new FormData($formIncome[0]);

    console.log('data save : ',data);
    if(list_delete.length > 0)
    {
        $.ajax({
            url         : base_url+'/api/hrd/payroll/delete_slip',
            type        : "POST",
            data        : {'deleted':list_delete},
            dataType    : "html",
            beforeSend: function() {
                $btnSubmit.html('Please wait....');
                $btnSubmit.prop('disabled',true);
            },
            success:function(resp)
            {
                $.ajax({
                    url         : target_url,
                    type        : "POST",
                    data        : data,
                    dataType    : "json",
                    mimeType    : "multipart/form-data",
                    contentType : false,
                    cache       : false,
                    processData : false,
                    success:function(data)
                    {
                        // var data = [];
                        $btnSubmit.html(captionBtn);
                        $btnSubmit.prop('disabled',false);
                        console.log(data);
                        get_slip(element_pegawai);
                        console.log("element pegawai ",element_pegawai);
                        data['id'] = id_gaji;
                        request_slip(data,function(){});
                        // $('#edit_modal').toggle();
                    }
                });
            }
        });
    }
    else{
        $.ajax({
            url         : target_url,
            type        : "POST",
            data        : data,
            dataType    : "json",
            mimeType    : "multipart/form-data",
            contentType : false,
            cache       : false,
            processData : false,
            beforeSend: function() {
                $btnSubmit.html('Please wait....');
                $btnSubmit.prop('disabled',true);
            },
            success:function(data)
            {
                // var data = [];
                $btnSubmit.html(captionBtn);
                $btnSubmit.prop('disabled',false);
                console.log(data);
                get_slip(element_pegawai);
                console.log("element pegawai ",element_pegawai);
                data['id'] = id_gaji;
                request_slip(data,function(){});
                // $('#edit_modal').toggle();
            }
        });
    }
}
