$(function(){
    var the_table;
    $unitusaha  = $('#unitusaha');
    $status     = $('#status');
    var status  = $status.val();
    var unitusaha = $unitusaha.val();

    /*
    $tgl        = $('#tgl_penyelenggaraan');
    $tgl.daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
          format: 'DD/MM/YYYY'
        },
        startDate: {
            value: moment().subtract(29, 'days')
        },
        endDate: {
            value: moment()
        }
    });

    $tgl.on('apply.daterangepicker', function(ev, picker) {
        console.log('jalan');
        tgl_mulai = picker.startDate.format('YYYY-MM-DD');
        tgl_akhir = picker.endDate.format('YYYY-MM-DD');
        unitusaha = $unitusaha.val();
        get_data(tgl_mulai,tgl_akhir,unitusaha);
    });*/

    $unitusaha.change(function(){
        /*tgl_mulai = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
        tgl_akhir = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        console.log('mulai ',tgl_mulai);
        console.log('akhir ',tgl_akhir);*/
        status      = $status.val();
        unitusaha   = $unitusaha.val();
        get_data(unitusaha,status);
    });

    $status.change(function(){
        status      = $status.val();
        unitusaha   = $unitusaha.val();
        get_data(unitusaha,status);
    });

    get_data(unitusaha,status);
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}
function get_data(unitusaha,status){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_rekrutment").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/rekrutment/rekrutment',
            "type": "POST",
            "dataType":"json",
            "data": {unitusaha:unitusaha,status:status}
        },
        "columns": [
            {
                "data": "no",
                "searchable": false
            },
            {
                "data": "unit_usaha",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/hrd/rekrutment/rekrutment/view/' + row.uid + '" onClick="view_detail(this)">' + data + '</span>';
                }
            },
            {
                "data": "unit_nama"
            },
            {
                "data": "posisi"
            },
            {
                "data": "kebutuhan"
            },
            {
                "data": "status_pengajuan",
                "render": function(data, type, row, meta) {
                    var caption = '';
                    var notif   = '';
                    switch(data)
                    {
                        case '1' :
                        notif   = 'label-success';
                        caption = 'Diterima';
                        break;
                        case '2' :
                        notif   = 'label-info';
                        caption = 'Bersyarat';
                        break;
                        case '3' :
                        notif   = 'label-danger';
                        caption = 'Ditolak';
                        break;
                        default :
                        notif   = 'label-default';
                        caption = 'Menunggu';
                    }
                    return '<span class="label '+notif+'">' + caption + '</span>';
                }
            }

        ],
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        }
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}
