var page_url    = '/api/hrd/pegawai/';
$notif          = $('#notif');
$formDemosi     = $('#form-demosi');
$btnSubmitDemosi= $('#save-button');
$detailPegawai  = $('#detail-pegawai');

$(function(){
    var the_table;
    $unitusaha      = $('#unitusaha');
    $unitkerja      = $('#unitkerja');
    $cari           = $('#cari');
    $btncari        = $('#btn-cari');
    $departemen     = $('#departemen');

    $unitusaha.change(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });

    $unitkerja.change(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });

    $btncari.click(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });

    $departemen.change(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $detailPegawai.html(data);
            $detailPegawai.fadeIn('slow');
        }
    });
    return false;
}

function get_data(cari,unitusaha,unitkerja,departemen){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable-pegawai").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/pegawai',
            "type": "POST",
            "dataType":"json",
            "data": {cari:cari,unitusaha:unitusaha,unitkerja:unitkerja,departemen:departemen}
        },
        "columns": [
            {
                "data": "nik",
                "render": function(data, type, row, meta) {
                    return '<span class="btn-link" onClick="view_detail(this)" style="cursor:pointer" href="'+base_url+'/api/hrd/pegawai/get_pegawai/' + row.uid + '">' + data + '</span>';
                }
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<span class="btn-link" onClick="view_detail(this)" style="cursor:pointer" href="'+base_url+'/api/hrd/pegawai/get_pegawai/' + row.uid + '">' + data + '</span>';
                }
            },
            {
                "data": "unitusaha_nama"
            },
            {
                "data": "unitkerja_nama"
            },
            {
                "data": "departemen_nama"
            },
            {
                "data": "jabatan_nama"
            }

        ]
    });
}

$(function(){
    $('input.form-control').keypress(function (e) {
      if (e.which == 13) {
        return false;    //<---- Add this line
      }
    });

    $('.pickadate').pickadate({
        // options
        format: 'dd-mm-yyyy'
    });

    $formDemosi.validate({
        rules: {
            "tgl_berhenti": {
                required: true,
            },
            "status_berhenti": {
                required: true,
            },
            "alasan_berhenti": {
                required: true,
            }
        },
        submitHandler: function(form) {
            var pegawai_id = $(document).find('input[name=pegawai_id]').val();
            if(!pegawai_id)
            {
                alert('Silahkan Pilih Dulu Pegawai!');
                return false;
            }

            var target_url  = base_url+'/api/hrd/demosi/save/'+pegawai_id;
            var captionBtn  = $btnSubmitDemosi.html();
            var data = new FormData($(form)[0]);
            console.log(data);
            $.ajax({
                url         : target_url,
                type        : "POST",
                data        : data,
                dataType    : 'json',
                mimeType    : "multipart/form-data",
                contentType : false,
                cache       : false,
                processData : false,
                beforeSend: function() {
                        $btnSubmitDemosi.html('Please wait....');
                },
                success:function(data)
                {
                    console.log(data);
                    if(data.status == 1)
                    {
                        $formDemosi[0].reset();
                        $detailPegawai.html('');
                        $btncari.click();
                    }
                    $btnSubmitDemosi.html(captionBtn);
                    $notif.text(data.info);
                    $notif.fadeIn('slow').delay('3500').fadeOut('slow');
                }
            });
            return false;
        }
    });
});

function delete_kontak(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    MyModel.delete(url,function(data){
        console.log(data);
        get_kontak();
        $notif.text(data.info);
        $notif.fadeIn('slow').delay('3500').fadeOut('slow');
    });
    return false;
}

function tambah_kontak(){
    $('#add_kontak_modal').modal('show');
}

function convert_array_to_object(dataArray){
    var result = {};
    dataArray.forEach(function(item,index){
        result[item.name] = item.value;
    });
    return result;
}

var MyModel = {
    page_url : '/api/hrd/pegawai/',
    insert : function(page_url,data,callback){
        $.ajax({
            'url':base_url+page_url,
            'type':'POST',
            'dataType':'json',
            'data':data,
            'success': function(data){
                return callback(data);
            }
        });
    },
    delete : function(url,callback){
        $.ajax({
            'url':url,
            'type':'POST',
            'dataType':'json',
            'success': function(data){
                return callback(data);
            }
        });
    },
    get : function(tabel,columns,data){
        console.log('run');
        var page_url = this.page_url;
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        the_table = $("#"+tabel).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url+page_url,
                "type": "POST",
                "dataType":"json",
                "data": data
            },
            "columns": columns
        });
    }
}
