var page_url    = '/api/hrd/pegawai/';
$notif          = $('#notif');
$formMutasi     = $('#form-mutasi');
$btnSubmitResign= $('#save-button');
$detailPegawai  = $('#detail-pegawai');

$.validator.addMethod("onlyNumber",
    function (value, element, options)
    {
        var req = /^\d+$/;

        return (value.match(req));
    },
    "Please enter a number."
);

$(function(){
    $('input.form-control').keypress(function (e) {
      if (e.which == 13) {
        return false;    //<---- Add this line
      }
    });

    $('.pickadate').pickadate({
        // options
        format: 'dd-mm-yyyy'
    });

    set_validation({
            "tahun_awal": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            },
            "tahun_akhir": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            },
            "tahun_mulai": {
                required: "required"
            },
            "jenis": {
                required: true,
            }
        });

    $('.berkas').click(function(){
        if($(this).val() == '1'){
            $('#upload_berkas').fadeIn();
        }
        else{
            $('#upload_berkas').fadeOut();
        }
    });

    $('.jenis').click(function(){
        if($(this).val() == 'atp'){
            console.log($(this).prop('checked'));
            if($(this).prop('checked')){
                $('#diajukan').fadeIn();
                set_validation({
                    "tahun_awal": {
                        required: "required",
                        onlyNumber: true,
                        maxlength: 10
                    },
                    "tahun_akhir": {
                        required: "required",
                        onlyNumber: true,
                        maxlength: 10
                    },
                    "tahun_mulai": {
                        required: "required",
                    },
                    "jenis": {
                        required: true,
                    },
                    "diajukan": {
                        required: true,
                    }
                });
            }
            else{
                $('#diajukan').fadeOut();
                set_validation({
                    "tahun_awal": {
                        required: "required",
                        onlyNumber: true,
                        maxlength: 10
                    },
                    "tahun_akhir": {
                        required: "required",
                        onlyNumber: true,
                        maxlength: 10
                    },
                    "tahun_mulai": {
                        required: "required",
                        onlyNumber: true,
                        maxlength: 10
                    },
                    "jenis": {
                        required: true,
                    }
                });
            }
        }
    });
});

function set_validation(rules)
{
    if(typeof formValidator != 'undefined')
        formValidator.destroy();

    formValidator = $formMutasi.validate({
        rules: rules
    });
}

function convert_array_to_object(dataArray){
    var result = {};
    dataArray.forEach(function(item,index){
        result[item.name] = item.value;
    });
    return result;
}

var MyModel = {
    page_url : '/api/hrd/pegawai/',
    insert : function(page_url,data,callback){
        $.ajax({
            'url':base_url+page_url,
            'type':'POST',
            'dataType':'json',
            'data':data,
            'success': function(data){
                return callback(data);
            }
        });
    },
    delete : function(url,callback){
        $.ajax({
            'url':url,
            'type':'POST',
            'dataType':'json',
            'success': function(data){
                return callback(data);
            }
        });
    },
    get : function(tabel,columns,data){
        console.log('run');
        var page_url = this.page_url;
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        the_table = $("#"+tabel).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url+page_url,
                "type": "POST",
                "dataType":"json",
                "data": data
            },
            "columns": columns
        });
    }
}
