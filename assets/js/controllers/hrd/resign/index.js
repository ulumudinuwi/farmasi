$(function(){
    var tanggal = false;
    var the_table;
    $tgl        = $('#tgl_penyelenggaraan');
    $unitusaha  = $('#unitusaha');
    $cari       = $('#cari');
    $btncari       = $('#btn-cari');
    $departemen = $('#departemen');

    $tgl.daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
          format: 'DD/MM/YYYY'
        },
        startDate: {
            value: moment().subtract(29, 'days')
        },
        endDate: {
            value: moment()
        }
    });

    $tgl.on('apply.daterangepicker', function(ev, picker) {
        tanggal = true;
        console.log('jalan');
        tgl_mulai   = picker.startDate.format('YYYY-MM-DD');
        tgl_akhir   = picker.endDate.format('YYYY-MM-DD');
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen);
    });

    $unitusaha.change(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen);
    });

    $btncari.click(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen);
        // $(document).find('#dataTable_resign_filter input[type=search]').val(cari);
    });
    $departemen.change(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen);
    });

    get_data();
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_resign").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/resign',
            "type": "POST",
            "dataType":"json",
            "data": {cari:cari,tgl_mulai:tgl_mulai,tgl_akhir:tgl_akhir,unitusaha:unitusaha,departemen:departemen}
        },
        "columns": [
            {
                "data": "nik",
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<a href="'+base_url+'/hrd/resign/edit/' + row.uid + '">' + data + '</a>';
                }
            },
            {
                "data": "tgl_diangkat",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "tgl_berhenti",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "unitkerja_nama"
            },
            {
                "data": "departemen_nama"
            },
            {
                "data": "jabatan_nama"
            }

        ]
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {

    } ).draw();*/
}
