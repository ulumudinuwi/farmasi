$formSkill     = $('#form-skill');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalSkill    = $('#add_skill_modal');

$(function(){
    $formSkill.validate({
        rules:{
            'keterampilan_level':{
                required:true,
                min: 1,
                max: 5
            }
        },
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_skill/'+pegawai_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_skill(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Skill.getSkill();
                $modalSkill.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formSkill[0].reset();
            });
            return false;
        }
    });
});

var Skill = {
    getSkill: function() {
        var page_url    = '/api/hrd/pegawai/get_skill/'+pegawai_id;
        var columns     = [
            {
                "data": "skill",
            },
            {
                "data": "keterampilan_level",
            },
            {
                "data": "tahun_pengalaman",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/pegawai/hapus_skill/' + row.uid + '" onClick="Skill.deleteSkill(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-skill',columns);
    },
    deleteSkill: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Skill.getSkill();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahSkill: function(){
        $modalSkill.modal('show');
    }
}
