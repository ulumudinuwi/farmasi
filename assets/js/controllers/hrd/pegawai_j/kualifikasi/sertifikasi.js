$formSertifikasi         = $('#form-sertifikasi');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalSertifikasi        = $('#add_sertifikasi_modal');

$(function(){
    $formSertifikasi.validate({
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_sertifikasi/'+pegawai_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_sertifikasi(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Sertifikasi.getSertifikasi();
                $modalSertifikasi.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formSertifikasi[0].reset();
            });
            return false;
        }
    });
});

var Sertifikasi = {
    getSertifikasi: function() {
        var page_url    = '/api/hrd/pegawai/get_sertifikasi/'+pegawai_id;
        var columns     = [
            {
                "data": "sertifikasi_nama",
            },
            {
                "data": "deskripsi",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/pegawai/hapus_sertifikasi/' + row.uid + '" onClick="Sertifikasi.deleteSertifikasi(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-sertifikasi',columns);
    },
    deleteSertifikasi: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Sertifikasi.getSertifikasi();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahSertifikasi: function(){
        $modalSertifikasi.modal('show');
    }
}
