$formDokumen        = $('#form-dokumen');
$notifKualifikasi   = $('#notif-berkas');
$modalDokumen       = $('#add_dokumen_modal');
$btnSubmitDokumen   = $('#btn-submit-dokumen');
base_file           = base_url.replace('/index.php','');
$.validator.addMethod("onlyNumber",
    function (value, element, options)
    {
        var req = /^\d+$/;

        return (value.match(req));
    },
    "Please enter a number."
);

$(function(){
    $formDokumen.validate({
        submitHandler: function(form) {
            // var dataForm    = $(form).serializeArray();
            // var data        = convert_array_to_object(dataForm);
            var target_url  = base_url+page_url+'save_dokumen/'+pegawai_id;
            var captionBtn  = $btnSubmitDokumen.html();
            var data = new FormData($(form)[0]);
            console.log(data);
            $.ajax({
                url         : target_url,
                type        : "POST",
                dataType    : "JSON",
                data        : data,
                mimeType    : "multipart/form-data",
                contentType : false,
                cache       : false,
                processData : false,
                beforeSend: function() {
                        $btnSubmitDokumen.html('Please wait....');
                },
                success:function(data)
                {
                    $btnSubmitDokumen.html(captionBtn);
                    console.log(data);
                    Dokumen.getDokumen();
                    $modalDokumen.modal('hide');
                    $notifKualifikasi.text(data.info);
                    console.log(data.info);
                    $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                    $formDokumen[0].reset();
                }
            });
            return false;
        }
    });
});

var Dokumen = {
    getDokumen: function() {
        var page_url    = '/api/hrd/pegawai/get_dokumen/'+pegawai_id;
        var columns     = [
            {
                "data": "nama_file",
                "render": function(data, type, row, meta){
                    return '<a class="btn btn-link text-center" href="'+base_file + row.uri_file + '" >'+data+' <i class="fa fa-download"></i></a>';
                }
            },
            {
                "data": "deskripsi",
            },
            {
                "data": "ukuran_file",
                "render": function(data){
                    return data+' kb'
                }
            },
            {
                "data": "oleh",
            },
            {
                "data": "waktu",
                "render": function(data)
                {
                    console.log(data);
                    // console.log(moment(data).format(''))
                    return moment(data).format('Do/MM/YYYY')+'<br>'+moment(data).format('hh:mm:ss');
                }
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/pegawai/hapus_dokumen/' + row.uid + '" onClick="Dokumen.deleteDokumen(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-dokumen',columns);
    },
    deleteDokumen: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Dokumen.getDokumen();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahDokumen: function(){
        $modalDokumen.modal('show');
    }
}
