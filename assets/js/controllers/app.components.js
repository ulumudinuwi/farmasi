angular.module('app.components', [])
    .directive('datepicker', ['$rootScope', function ($rootScope) {

        function link(scope, elem, attr, controller) {
            var opt = isSinglePicker(scope.picker);
            $(elem).daterangepicker({singleDatePicker: opt}, function (start, end, label) {
                scope.dateModel = moment(start.toISOString()).format(scope.dateFormat);
            });
        }

        function isSinglePicker(picker) {
            if (picker == 'single') {
                return true;
            }
            return false;
        }

        return {
            restrict: 'A',
            scope: {
                dateFormat: '@',
                dateModel: '=',
                picker: '@'
            },
            link: link
        }
    }])
    .directive('select2', ['$rootScope', function ($rootScope) {

        function link(scope, elem, attr, controller) {
            console.log(scope.remoteUrl);
            if (scope.remoteUrl) {
                console.log('masuk remote url');
                elem.select2({
                    data: scope.initData,
                    placeholder: scope.selectPlaceholder || 'Pilih Data',
                    ajax: {
                        url: scope.remoteUrl,
                        dataType: 'json',
                        data: function (param) {
                            return {
                                delay: 0.3,
                                q: param.term
                            }
                        },
                        processResults: function (data) {
                            console.log("data select2, ", data);
                            return {
                                results: _.map(data.items, function (obj) {
                                    return {
                                        id: obj.id,
                                        text: obj.text,
                                    }
                                })
                            }
                        },
                        cache: true,
                    }
                })
            } else {
                elem.select2({
                    placeholder: scope.placeholder || 'Pilih Data'
                }).select2('val', null);
            }
        }

        return {
            restrict: 'A',
            scope: {
                selectPlaceholder: '=',
                remoteUrl: '=',
                dataModel: '@',
                initData: '='

            },
            link: link
        }
    }])
    .directive('fileUpload', ['$rootScope', '$http', function ($rootScope, $http) {
        function link(scope, elem, attr, controller) {
            console.log('use directive file upload');
            var inputForm = $(elem),
                inputName = attr.name,
                inputFile = {},
                inputFileName = '';

            var formData = new FormData();

            inputForm.on('change', function (e) {
                console.log(inputForm[0].files[0]);
                inputFile = inputForm[0].files[0];
                inputFileName = inputFile.name;
                formData.append('file', inputFile, inputFileName);
                setFileCallback(formData);

                // if (scope.autoUpload == 'auto') {
                //     console.log("form data", formData);
                //     $http({
                //         url: base_url + '/file/upload',
                //         method: 'POST',
                //         data: formData
                //     }).then(function (result) {
                //         console.log("result auto upload", result);
                //         scope.fileServerPath = result;
                //     }).catch(function (err) {
                //         console.log(err);
                //     });
                // }

            });

            function setFileCallback(formData) {
                if (typeof scope.fileCallback === 'function') {
                    scope.fileCallback(formData);
                } else {
                    scope.fileCallback = formData;
                }
            }
        }

        return {
            restrict: 'A',
            scope: {
                fileUploadType: '@',
                fileCallback: '=',
                autoUpload: '@',
                fileServerPath: '='
            },
            link: link
        }
    }]);