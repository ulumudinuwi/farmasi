/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddLaboratoriumController', modalAddLaboratorium);
modalAddLaboratorium.$inject = ['$rootScope', '$scope', 'api', '$http', '$uibModalInstance', '$log', '$uibModal', 'items', 'toastr', '$timeout', '$httpParamSerializer', '$q', '$compile'];
function modalAddLaboratorium($rootScope, $scope, api, $http, $uibModalInstance, $log, $uibModal, items, toastr, $timeout, $httpParamSerializer, $q, $compile) {

    var pelayanan_id = items.pasien.pelayanan_id;
    var layanan_id = items.pasien.layanan_id;
    var RESULT = null;

    $scope.fillForm = fillForm;
    $scope.save = save;
    $scope.batal = batal;
    $scope.close = close;
    $scope.dismiss = dismiss;
    $scope.kunjungan = [];

    // var uri = "";
    // if(items.pasien.laboratorium_uid) {
    //     uri = base_url + '/api/rujukan_lab/laboratorium/get_last_uid?uid=' + items.pasien.laboratorium_uid;
    // }else{
    //     uri = base_url + '/api/rujukan_lab/laboratorium/get_last_uid';
    // }
 

    function fillForm(laboratorium_uid) {
        var uri = "";
        if (laboratorium_uid) {
            uri = base_url + '/api/rujukan_lab/laboratorium/get_last_uid?uid=' + laboratorium_uid + '&pelayanan_id=' + pelayanan_id + '&layanan_id=' + layanan_id;
        } else {
            uri = base_url + '/api/rujukan_lab/laboratorium/get_last_uid?pelayanan_id=' + pelayanan_id + '&layanan_id=' + layanan_id;
        }

        $http.get(uri).then(function (result) {
            console.log("data, ", result.data);
            RESULT = result;

            var tindakan = [];
            var isBatal = false;
            var ids = null;
            var diagnosa = result.data.response ? result.data.response.diagnosa : '';
            $scope.rujukan = {};

            $scope.rujukanId = -1;

            var urlTindakan = base_url + '/api/penunjang_medik/laboratorium_pendaftaran/load_data_tindakan';

            if (result.data.response) {
                var rujukan = JSON.parse(result.data.response.rujukan);
                $scope.status = result.data.response.status;

                switch (parseInt($scope.status)) {
                    case -1: // Dirujuk
                    case 0: // Sedang Diperiksa
                    case 1: // Selesai
                    case 2: // Batal
                        $scope.rujukanId = result.data.response.id;
                        isBatal = true;
                        ids = result.data.response.tindakan_ids.join(',');
                        urlTindakan = base_url + '/api/penunjang_medik/laboratorium_pendaftaran/load_data_tindakan?ids=' + ids + '&jenis=0';
                        break;
                    default: 
                        $scope.rujukanId = -1;
                        isBatal = false;
                        break;
                }

                // if (rujukan.rujuk_laboratorium == 3) {
                //     $scope.rujukanId = -1;
                //     isBatal = false;

                // } else if (rujukan.rujuk_laboratorium == 2 || rujukan.rujuk_laboratorium == 1) {
                //     $scope.rujukanId = result.data.response.id;
                //     isBatal = true;
                //     ids = result.data.response.tindakan_ids.join(',');
                //     urlTindakan = base_url + '/api/penunjang_medik/laboratorium_pendaftaran/load_data_tindakan?ids=' + ids + '&jenis=0';
                // }
            }

            // Kunjungan
            $scope.kunjungan = result.data.kunjungan;

            console.log("url tindakan, ", urlTindakan);

            $scope.urlTindakan = urlTindakan;

            $scope.initTindakan = items.tindakan;

            $scope.data = items.pasien;
            $scope.data.diagnosa = diagnosa;
            $scope.data.tanggalPendaftaran = moment().format('DD/MM/YYYY');
            $scope.data.kelamin = $scope.data.jenis_kelamin == 1 ? 'Laki - Laki' : 'Perempuan';
            console.log("ISBATAL: ", isBatal);
            $scope.data.rujukan.rujuk_laboratorium = (isBatal) ? 1 : 0;

            $scope.data.tindakan = [];

            $scope.choose = {};
            $scope.qty = 1;
            $scope.labs = [];

            $http.get(base_url + '/api/master/kelas').then(function (result) {
                $scope.dataKelas = result.data.response;
            }).catch(function (err) {
                toastr.error('Gagal mengambil kelas', 'Error');
            });
        }, function (err) {
            console.log(err);
        });

        $compile($("#tree-tindakan"));
    }

    $scope.selectedNodes = function (nodes) {

        console.log("selected nodes", nodes);

        if (nodes) {
            // console.log("length of children", nodes.children);
            if (nodes.children.length > 0) {
                _.each(nodes.children, function (child) {
                    $scope.data.tindakan.push(child);
                });

                $scope.$apply();
            }
        }

    };

    $scope.unSelectedNodes = function(nodes){

        var index = _.findIndex($scope.data.tindakan, {id: nodes.id});
        console.log("index delete", index);

        $scope.data.tindakan.splice(index, 1);
        $scope.$apply();
    };

    $scope.delete = function (i) {
        $scope.labs.splice(i, 1);
    };

    $scope.selected = function (item) {
        if (item) {
            $scope.labs.push(item.originalObject);
            console.log($scope.labs);
        }
    };

    $scope.onChoose = function (tindakan, qty) {
        // console.log(tindakan);
        $scope.choose = tindakan;
    };

    $scope.openModalSubLaboratorium = function (size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddSubLaboratorium.html',
            controller: 'ModalAddSubLaboratoriumController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.dataObat.push(selectedItem);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    function batal() {
        $.ajax({
            url: base_url + '/api/laboratorium_pendaftaran/batal?uid=' + RESULT.data.response.uid,
            method: 'get'
        }).then(function (result) {
            toastr.success('Sukses Batalkan Rujukan Laboratorium', 'Sukses !');
            $timeout(function () {
                close([]);
            }, 500);
        }, function (err) {
            toastr.error('Gagal Batal Rujukan Laboratorium', 'Gagal !');
        });

    }

    function save() {
        // marshall


        console.log("data tindakan, ", {
            tindakan: angular.copy($scope.data)
        });

        var req = {
            method: 'POST',
            url: base_url + '/api/rujukan_lab/laboratorium/save',
            data: $httpParamSerializer({data: angular.copy($scope.data)}),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).then(function (result) {
            console.log(result.data.response);
            $scope.rujukan = result.data.response;
            toastr.success('Berhasil Daftar Laboratorium', 'Sukses!');
            $timeout(function () {
                close({
                    uid: result.data.response,
                    data: angular.copy($scope.data.tindakan)
                });
            }, 500);
        }, function (err) {
            console.log(err);
            toastr.error('Terjadi Kesalahan Sistem', 'Gagal');
        });
    }

    function close(data) {

        console.log("data : ", angular.copy(data));
        $uibModalInstance.close(angular.copy(data));
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }

    fillForm(items.pasien.laboratorium_uid);

}