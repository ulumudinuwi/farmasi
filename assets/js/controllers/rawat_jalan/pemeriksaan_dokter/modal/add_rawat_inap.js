/**
 * Created by agungrizkyana on 10/5/16.
 */
/**
 * Modal Reservasi OK
 * @type {string[]}
 */
app.controller('ModalAddRawatInapController', modalAddRawatInapController);
modalAddRawatInapController.$inject = [
    '$rootScope',
    '$scope',
    '$http',
    '$uibModalInstance',
    '$log',
    'DTOptionsBuilder',
    'DTColumnBuilder',
    'DTColumnDefBuilder',
    '$compile',
    'items',
    'CONFIG',
    'toastr',
    '$timeout',
    'EVENT'
];
function modalAddRawatInapController($rootScope,
                                     $scope,
                                     $http,
                                     $uibModalInstance,
                                     $log,
                                     DTOptionsBuilder,
                                     DTColumnBuilder,
                                     DTColumnDefBuilder,
                                     $compile,
                                     items,
                                     CONFIG,
                                     toastr,
                                     $timeout,
                                     EVENT) {

    console.log("items", items);

    var _ruang = {};
    var _bed = {};
    var _kelas = {};

    $scope.data = items;
    $scope.openJadwalOperasi = false;
    $scope.dataRuang = [];
    $scope.dataKelas = [];
    $scope.dataBed = [];


    $scope.onChangeKelas = onChangeKelas;
    $scope.onChangeRuang = onChangeRuang;
    $scope.onChangeBed = onChangeBed;

    $scope.pesanKamarRawatInap = pesanKamarRawatInap;
    $scope.batalPesanRawatInap = batalPesanRawatInap;

    $scope.close = close;
    $scope.dismiss = dismiss;
    if($scope.data.data_rujukan.lab.length !=0){
        //
        // $scope.data.tanggalOperasi= moment($scope.data.data_rujukan.kunjungan_ulang.tanggal).format('DD-MM-YYYY');
        // $scope.data.jamMulaiOperasi = $scope.data.data_rujukan.kunjungan_ulang.jam_mulai ;
        // $scope.data.jamSelesaiOperasi = $scope.data.data_rujukan.kunjungan_ulang.jam_akhir ;
    }

    $http.get(CONFIG.RUANG).then(successReqRuang, errorReqRuang);
    $http.get(CONFIG.KELAS).then(successReqKelas, errorReqKelas);

    $http.get(base_url + "/api/rawat_jalan/pemeriksaan_dokter/icd10_ids", {
        params: {
            ids: JSON.stringify(items.diagnosa)
        }
    }).then(function(result){
        $scope.data.diagnosa_text = result.data.diagnosa;
        $scope.data.diagnosa_kerja = JSON.stringify(items.diagnosa);
    }, function(err){
        console.log(err);
    });

    function successReqRuang(result) {

        $scope.dataRuang = result.data.response;
    }

    function errorReqRuang(err) {
        console.log(err);
    }

    function successReqKelas(result) {

        $scope.dataKelas = result.data.response
    }

    function errorReqKelas(err) {
        console.log(err);
    }

    function onChangeRuang(ruang) {
        _ruang = ruang;
    }

    function onChangeKelas(kelas) {
        _kelas = kelas;
        $http.get(CONFIG.BED + '?ruang=' + _ruang.id + '&kelas=' + kelas.id).then(successReqBed, errorReqBed);

        function successReqBed(result) {
            $scope.dataBed = result.data.response;
        }

        function errorReqBed(err) {
            console.log(err);
        }
    }

    function onChangeBed(bed) {
        _bed = bed;
    }

    function pesanKamarRawatInap() {
        var _item = angular.copy($scope.data);
        var _data = {
            tanggal: moment().format('YYYY-MM-DD hh:mm:ss'),
            rawat_inap: {
                jumlah_hari: 0
            },
            pasien: _item,
            dokter_pjp: ($scope.data.dokter_pjp) ? $scope.data.dokter_pjp : _item.dokter_id,
            diagnosa_kerja : $scope.data.diagnosa_kerja
        };

        var req = {
            method: 'POST',
            url: base_url + '/api/rawat_inap/registrasi/save',
            data: "data=" + JSON.stringify(_data),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http.post(base_url+'/api/rawat_inap/registrasi/save', angular.copy(_data)).then(success, error);

        function success(result) {
            toastr.success('Berhasil Pesan Kamar Rawat Inap', 'Sukses!');
            $timeout(function () {
                close({
                    result: result,
                    rujuk_rawatinap: 1
                });
            }, 500);
        }

        function error(err) {
            console.log(err);
        }
    }

    function batalPesanRawatInap() {
        var uid = 0;
        if ($scope.data.rawatInap) {
            uid = $scope.data.rawatInap.data.response[0].uid;
        } else if ($scope.data.data_rujukan.inap.length > 0) {
            uid = $scope.data.data_rujukan.inap[0].uid;
        }

        $http.get(base_url + '/api/rawat_inap/registrasi/batal/' + uid).then(function (result) {
            toastr.success('Berhasil Batal Pesan Kamar Rawat Inap', 'Sukses!');
            $timeout(function () {
                close({
                    result: result,
                    rujuk_rawatinap: 0
                });
            }, 500);
        }, function (err) {
            toastr.error('Registrasi Rawat Inap telah diproses.', 'Gagal');
            console.log(err);
        });
    }

    function close(reservasiOk) {
        $uibModalInstance.close(reservasiOk);
        $rootScope.$emit(EVENT.CANCEL_RESERVASI_OK);
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }


    // jquery
    $timeout(function () {


        $('#multi-diagnosa-kerja').select2({

            placeholder:'PILIH ICD',
            ajax: {
                url: base_url + '/api/rawat_jalan/pemeriksaan_dokter/icd10',
                dataType: 'json',
                method: 'post',
                data: function (param) {
                    return {
                        delay: 0.3,
                        q: param.term
                    }
                },
                processResults: function (data) {

                    return {
                        results: _.map(data.items, function (obj) {
                            return {
                                id: obj.id,
                                text: obj.code + ' - ' + obj.description,
                            }
                        })
                    }
                },
                cache: true,
            }
        })
    }, 1000);

}