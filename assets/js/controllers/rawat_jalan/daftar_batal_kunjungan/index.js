app.controller('DaftarBatalKunjunganIndexController', ['$scope', 'api', '$http', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr',
    function ($scope, api, $http, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr) {

        //simpan


        $scope.data = {};

        $scope.save = function () {
            console.log(angular.copy($scope.data));
        };

        $scope.onChangeRangeWaktu = function (range) {
            var tanggalDari = moment(range.substr(0, 10)).format('YYYY-MM-DD'),
                tanggalSampai = moment(range.substr(13, 10)).format('YYYY-MM-DD');
            buildTable({
                tanggalDari: tanggalDari,
                tanggalSampai: tanggalSampai
            });
        };

        $scope.onChangeKelompokPasien = function(kelompok_pasien){

            buildTable({
                kelompok_pasien : kelompok_pasien
            });
        };

        $scope.onChangeLayanan = function(layanan){
            buildTable({
                layanan : layanan.id
            });
        };

        $scope.onChangeDokter = function(dokter){
            buildTable({
                dokter : dokter.id
            });
        };

        $scope.selected = function (item) {

            if (item) {
                $scope.data = angular.copy(item.originalObject);
                $scope.data.tanggal = new Date($scope.data.tanggal);
            }
            console.log(angular.copy($scope.data));
            // $scope.reload();
            buildTable(item.originalObject.no_rekam_medis);
        };

        $http.get(base_url + "/api/master/pegawai").then(function success(result) {
            //console.log("provinsi");

            $scope.dataPegawai = angular.copy(result.data);
        }, function error(err) {
            //console.log(err);
        });

        $http.get(base_url + "/api/master/kelompok_pasien").then(function success(result) {
            //console.log("provinsi");

            $scope.dataKelompokPasien = angular.copy(result.data);
        }, function error(err) {
            //console.log(err);
        });

        $http.get(base_url + "/api/master/layanan").then(function success(result) {
            //console.log("provinsi");

            $scope.dataLayanan = angular.copy(result.data);
        }, function error(err) {
            //console.log(err);
        });

        $scope.cancelKunjungan = function (id) {
            var _confirm = confirm('Apakah anda yakin?');
            if (_confirm) {
                $http.get(base_url + '/api/rawat_jalan/daftar_batal_kunjungan/batalkan_kunjungan/' + id).then(function (result) {
                    console.log(result);
                    toastr.success('Batalkan kunjungan untuk ID : ' + id, 'Berhasil !');

                    setTimeout(function () {
                        $.ajax();
                        window.location.href = base_url + '/rawat_jalan/daftar_kunjungan';
                    }, 1200);
                }, function (err) {
                    console.log(err);
                });
            }

        };

        $scope.dateFilter = function () {
            console.log('filter', {
                tanggalDari: moment(angular.copy($scope.data.tanggalDari)).format('YYYY-MM-DD'),
                tanggalSampai: moment(angular.copy($scope.data.tanggalSampai)).format('YYYY-MM-DD')
            });

            buildTable(null, {
                tanggalDari: moment(angular.copy($scope.data.tanggalDari)).format('YYYY-MM-DD'),
                tanggalSampai: moment(angular.copy($scope.data.tanggalSampai)).format('YYYY-MM-DD')
            });
        };

        //init datatable


        function buildTable(filter) {
            var url = base_url + '/api/rawat_jalan/daftar_batal_kunjungan/data_daftar_kunjungan';
            if (filter) {
                if (filter.noRekamMedis) {
                    url += '?no_rm=' + filter.noRekamMedis;
                }
                if (filter.tanggalDari) {
                    url += "?tanggalDari=" + filter.tanggalDari + "&tanggalSampai=" + filter.tanggalSampai;
                }
                if (filter.kelompok_pasien) {
                    url += "?kelompok_pasien=" + filter.kelompok_pasien;
                }
                if (filter.layanan) {
                    url += "?layanan=" + filter.layanan;
                }
                if (filter.dokter) {
                    url += '?dokter=' + filter.dokter;
                }

            }
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax', {
                    url: url,
                    type: 'POST',
                })
                // or here
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('order', [[0, 'desc']])
                .withOption('createdRow', function (row) {
                    // Recompiling so we can bind Angular directive to the DT
                    $compile(angular.element(row).contents())($scope);
                })
                .withPaginationType('full_numbers');

            $scope.dtInstance = {};
            $scope.dtColumns = [
                DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {

                    return moment(row.tanggal).format('DD-MMM-YYYY H:mm:s');
                }).withOption('searchable', true),
                DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No MR'),
                DTColumnBuilder.newColumn('no_register').withTitle('No Register'),
                DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
                DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
                DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter').withOption('searchable', true),
                DTColumnDefBuilder.newColumnDef(5).withTitle('Action').withClass('text-center').renderWith(function (data, type, row, meta) {

                    var link = '<ul class="icons-list">' +
                        '<li class="dropdown">' +
                        '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
                        '<i class="icon-menu9"></i>' +
                        '</a>' +

                        '<ul class="dropdown-menu dropdown-menu-right">' +
                        '<li><a ><i class="icon-file-excel"></i> Nota</a></li>' +
                        '<li><a href="#"><i class="icon-file-word"></i> Tracer</a></li>' +
                        '<li><a href="#"><i class="icon-file-word"></i> Status</a></li>' +
                        '<li><a href="#"><i class="icon-file-word"></i> SJP</a></li>' +
                        '</ul>' +
                        '</li>' +
                        '</ul>';
                    // return '<a ng-click="cancelKunjungan(\'' + row.id + '\')" class="btn btn-danger">Batalkan <i class="fa fa-trash-o"></i></a>';
                    return link;
                }).withOption('searchable', false)
            ];
            console.log($scope.dtInstance);
        }

        buildTable();


        $scope.reload = function () {
            // $scope.dtInstance._renderer.rerender();

        }


    }
]);