var the_table;
var urlReq = base_url + "/odc_ods/master_alat/list_alat";
var def = false;
var phari = false
var ppoli = false;
var save_method; //for save method string
var table;

$(window).load(function () {
    //Bagian Pasien

    $('#btn-cari').click(function () {
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        // phari = false;
        // ppoli = false;
        // ppoli = false;
        create_table();
    });


    $('#poli').change(function () {
        console.log($(this).val());
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        // ppoli = true;
        create_table();
    });
    $('#hari').change(function () {
        console.log($(this).val());
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        // phari = true;
        create_table();
    });
    $('#daftar-jadwal').click(function () {
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        // phari = false;
        // ppoli = false;
        create_table();
    });


    phari = false;
    ppoli = false;
    create_table();

});

function create_table() {


    var hari = $('#hari').val();
    var poli = $('#poli').val();

    the_table = $("#dataTable_user").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": urlReq,
            "type": "POST",

            "data": {
                hari: hari,
                poli: poli,
                phari: phari,
                ppoli: ppoli
            },
        },
        "columns": [
            {
                "data": "nama"
            },
            {
                "data": "nama_detail"
            },
            {
                "data": "kode"
            },
            {
                "data": "serial_number"
            },
            {
                "data": "kondisi"
            },
            {
                "data": "status_penggunaan"
            },
            // {
            //     "data": "ruangan"
            // },
            {
                "data": "action",
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person(' + "'" + row.id + "'" + ')"><i class="glyphicon glyphicon-pencil"></i></a>  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person(' + "'" + row.id + "'" + ')"><i class="glyphicon glyphicon-trash"></i> </a>';
                }
            }
        ]
    })
    ;
}


$(document).ready(function () {
    // $('#mulai_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $('#akhir_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $("#mulai_praktek").inputmask("h:s", {"placeholder": "hh/mm"});
    // $("#akhir_praktek").inputmask("h:s", {"placeholder": "hh/mm"});

    //set input/textarea/select event when change value, remove class error and remove text help block
    $("input").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select2").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});


function add_jadwal() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#addJadwal').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Jadwal'); // Set Title to Bootstrap modal title
}

function edit_person(id) {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: base_url + "/odc_ods/master_alat/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            console.log(data);

            $('[name="id"]').val(data.id);
            $('[name="dokter_praktek"]').select2().val(data.id_dokter).trigger("change");
            $('[name="hari_praktek"]').select2().val(data.hari).trigger("change");
            $('[name="mulai_praktek"]').val(data.jam_mulai);
            $('[name="akhir_praktek"]').val(data.jam_berakhir);
            $('[name="klinik_praktek"]').select2().val(data.id_klinik).trigger("change");
            // $('[name="ruangan_praktek"]').val(data.catatan);

            $('#addJadwal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax
}

function save() {
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled', true); //set button disable
    var url;

    if (save_method == 'add') {
        url = base_url + "/odc_ods/master_alat//ajax_add";
    } else {
        url = base_url + "/odc_ods/master_alat/ajax_update";
    }

    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function (data) {
            console.log("INIIIIIIIII");
            console.log(data);
            if (data.status) //if success close modal and reload ajax table
            {
                toastr.success('Data Telah Tersimpan', 'BERHASIL!');
                $('#addJadwal').modal('hide');
                the_table.destroy();
                create_table();
            }
            else {
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable
            // toastr.success('Simpan Jadwal Berhasil !', 'Sukses');
            // the_table.destroy();
            // create_table();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error('Error adding / update data', 'GAGAL!');

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable

        }
    });
}

function delete_person(id) {
    swal({
            title: "Apakah anda yakin ?",
            text: "data yang telah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "YA, Hapus",
            cancelButtonText: "TIDAK!, Batalkan",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: base_url + "/odc_ods/master_alat/ajax_delete/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        //if success reload ajax table
                        $('#modal_form').modal('hide');
                        the_table.destroy();
                        create_table();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                    }
                });
                swal("Terhapus!", "Data Telah Terhapus", "success");

            } else {
                swal("Batal", "Data Tidak Terhapus :)", "error");
            }
        });

}