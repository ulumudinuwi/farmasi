app.controller('PemeriksaanAwalFormController', ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$location',
    function ($rootScope, $scope, api, $http, CONFIG, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $location) {


        $scope.data = {};
        $scope.rows = [];


        $http.get(base_url + '/api/odc_ods/pemeriksaan_awal/kunjungan/' + $('#id').val()).then(function(kunjungan) {
            // init
            $scope.noRekamMedis = kunjungan.data.no_rekam_medis + " " + kunjungan.data.nama_pasien;

            $scope.data.nama = kunjungan.data.nama_pasien;
            $scope.data.tanggal = new Date(kunjungan.data.tanggal);
            $scope.data.umur = moment().diff(moment(kunjungan.data.tgl_lahir, "YYYY-MM-DD").format(), 'years');
            $scope.data.tPelayananId = kunjungan.data.id;
            $scope.data.jenis_kelamin = kunjungan.data.jenis_kelamin;
            $scope.data.no_register = kunjungan.data.no_register;
            $scope.data.kelompokpasien = kunjungan.data.kelompokpasien;
            $scope.data.nama_layanan = kunjungan.data.nama_layanan;
            $scope.data.nama_pegawai = kunjungan.data.nama_dokter;


            $scope.isSubmit = false;
            var url = base_url + '/api/odc_ods/pemeriksaan_awal/save';

            if (window.location.search) {
                $http.get(base_url + '/api/odc_ods/pemeriksaan_awal/search' + window.location.search).then(function (result) {

                    var obj = angular.copy(result.data.results[0]);
                    var rawatJalan = angular.copy(result.data.rawat_jalan[0]);
                    $scope.data = obj;
                    $scope.data.tanggal = new Date($scope.data.tanggal);
                    $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');
                    $scope.data.tPelayananId = obj.t_pelayananId;
                    $scope.noRekamMedis = obj.no_rekam_medis;
                    $scope.data.tinggiBadan = rawatJalan.tinggi;
                    $scope.data.beratBadan = rawatJalan.berat;
                    $scope.data.suhuBadan = rawatJalan.suhu;
                    $scope.data.tensi1 = rawatJalan.tensi;
                    $scope.data.tensi2 = rawatJalan.tensi;
                    $scope.data.anamnesaAwal = rawatJalan.anamnesa;
                    $scope.data.rawatJalanId = rawatJalan.id;
                    url = base_url + '/api/odc_ods/pemeriksaan_awal/update';

                });
            } else {
                console.log('ga ada search');
            }

            $scope.selected = function (item) {

                if (item) {
                    // console.log(item);
                    var obj = angular.copy(item.originalObject);
                    $scope.data = obj;
                    $scope.data.tanggal = new Date($scope.data.tanggal);
                    $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');
                    $scope.data.tPelayananId = obj.t_pelayananId;
                } else {
                    $scope.data = {};
                }
            };

            $scope.tes = 'tes';

            $scope.save = function () {

                var _data = angular.copy($scope.data);
                var data_a;

                blockPage();

                $.ajax({
                    url: url,
                    method: 'post',
                    data: _data,
                    dataType: 'json'
                }).then(function (result) {
                    // console.log("ini resultt");
                    // console.log(result[0]);
                    data_a = result[0];
                    $.ajax({
                        url: base_url + '/api/odc_ods/pemeriksaan_awal/cekdouble',
                        method: 'post',
                        data: data_a,
                        dataType: 'json'
                    }).then(function (result) {
                         console.log("sukses");
                        // console.log(result);
                    }, function (err) {
                        console.log("error");
                       // console.log(err)
                    });
                    toastr.success('Registrasi Pemeriksaan ODC / ODS', 'Sukses !');
                    $scope.isSubmit = true;

                    $.unblockUI();
                }, function (err) {
                    console.log(err);
                    toastr.error('Terdapat Kesalah Input', 'Gagal !');

                    $.unblockUI();
                });

            };

            $scope.reset = function () {
                window.location.href = base_url + '/odc_ods/pemeriksaan_awal/list_pemeriksaan';
            };

        });



    }
]);