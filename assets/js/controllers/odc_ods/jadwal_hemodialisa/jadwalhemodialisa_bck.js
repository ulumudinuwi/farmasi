var the_table;
var urlReq = base_url+ "/rawat_jalan/jadwal_dokter/list_jadwal_dokter";
var def = false;
var pdokter= false
var ppoli=false;
var save_method; //for save method string
var table;
var visible=true;

$(window).load(function () {
    //Bagian Pasien

    $('#btn-cari').click(function(){
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        pdokter=false;
        ppoli=false;
        create_table();
    });


    $('#poli').change(function () {
        console.log($(this).val());
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        ppoli = true;
        create_table();
    });
    $('#dokter').change(function () {
        console.log($(this).val());
        if (typeof the_table != 'undefined') {
            the_table.destroy();
        }
        pdokter = true;
        create_table();
    });
    $('#daftar-pasien').click(function(){
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        pdokter=false;
        ppoli=false;
        $('#tambah').attr('disabled',false); //set button enable
        create_table();
    });
    $('#kalender-jadwal').click(function(){

        $('#tambah').attr('disabled',true); //set button disabled
        location.reload();
    });


    pdokter=false;
    ppoli=false;
    create_table();

});

function create_table() {

    var dokter = $('#dokter').val();
    var poli = $('#poli').val();

    the_table = $("#dataTable_user").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": urlReq,
            "type": "POST",

            "data": {
                dokter: dokter,
                poli: poli,
                pdokter:pdokter,
                ppoli:ppoli
            },
        },
        "order" :[[ 5, 'asc' ], [ 2, 'asc' ]],
        "columns": [
            {
                "data": "poliklinik"
            },
            {
                "data": "dokter"
            },
            {
                "data": "senin",
                "searchable":false
            },
            {
                "data": "selasa",
                "searchable":false
            },
            {
                "data": "rabu",
                "searchable":false
            },
            {
                "data": "kamis",
                "searchable":false
            },
            {
                "data": "jumat",
                "searchable":false
            },
            {
                "data": "sabtu",
                "searchable":false
            },
            {
                "data": "minggu",
                "searchable":false
            },

            {
                "data": "action",
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('+"'"+row.id+"'"+')"><i class="glyphicon glyphicon-pencil"></i></a>  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('+"'"+row.id+"'"+')"><i class="glyphicon glyphicon-trash"></i> </a>';
                }
            }
        ]
    });
}


$(document).ready(function() {
    // $('#mulai_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $('#akhir_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $("#mulai_praktek").inputmask("h:s",{ "placeholder": "hh/mm" });
    // $("#akhir_praktek").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#senin_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#senin_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#selasa_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#selasa_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#rabu_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#rabu_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#kamis_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#kamis_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#jumat_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#jumat_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#sabtu_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#sabtu_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });

    $("#minggu_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#minggu_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });

    $('#tambah').attr('disabled',true); //set button disabled

    //set input/textarea/select event when change value, remove class error and remove text help block
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select2").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    // $('#dataTable_user').DataTable( {
    //     order: [[ 5, 'asc' ], [ 2, 'asc' ]]
    // } );

});



function add_jadwal()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#addJadwal').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Jadwal'); // Set Title to Bootstrap modal title
}

function edit_person(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : base_url+"/rawat_jalan/jadwal_dokter/ajax_edit/"+ id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="dokter_praktek"]').select2().val(data.id_dokter).trigger("change");
            $('[name="senin_mulai"]').val(data.senin_mulai);
            $('[name="senin_akhir"]').val(data.senin_akhir);
            $('[name="selasa_mulai"]').val(data.selasa_mulai);
            $('[name="selasa_akhir"]').val(data.selasa_akhir);
            $('[name="rabu_mulai"]').val(data.rabu_mulai);
            $('[name="rabu_akhir"]').val(data.rabu_akhir);
            $('[name="kamis_mulai"]').val(data.kamis_mulai);
            $('[name="kamis_akhir"]').val(data.kamis_akhir);
            $('[name="jumat_mulai"]').val(data.jumat_mulai);
            $('[name="jumat_akhir"]').val(data.jumat_akhir);
            $('[name="sabtu_mulai"]').val(data.sabtu_mulai);
            $('[name="sabtu_akhir"]').val(data.sabtu_akhir);
            $('[name="minggu_mulai"]').val(data.minggu_mulai);
            $('[name="minggu_akhir"]').val(data.minggu_akhir);
            $('[name="klinik_praktek"]').select2().val(data.id_klinik).trigger("change");
            // $('[name="ruangan_praktek"]').val(data.catatan);

            $('#addJadwal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;

    if(save_method == 'add') {
        url = base_url+"/rawat_jalan/jadwal_dokter/ajax_add";
    } else {
        url = base_url+"/rawat_jalan/jadwal_dokter/ajax_update";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            console.log("INIIIIIIIII");
            console.log(data);
            if(data.status) //if success close modal and reload ajax table
            {
                toastr.success('Data Telah Tersimpan', 'BERHASIL!');
                $('#addJadwal').modal('hide');
                the_table.draw();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++)
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
            // toastr.success('Simpan Jadwal Berhasil !', 'Sukses');
            // the_table.destroy();
            // create_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            toastr.error('Error adding / update data', 'GAGAL!');

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable

        }
    });
}

function delete_person(id)
{

    swal({
            title: "Apakah anda yakin ?",
            text: "data yang telah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "YA, Hapus",
            cancelButtonText: "TIDAK!, Batalkan",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : base_url+"/rawat_jalan/jadwal_dokter/ajax_delete/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table
                        $('#modal_form').modal('hide');
                        the_table.draw();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });
                swal("Terhapus!", "Data Telah Terhapus", "success");

            } else {
                swal("Batal", "Data Tidak Terhapus :)", "error");
            }
        });

}