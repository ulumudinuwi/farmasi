/**
 * Created by agungrizkyana on 10/5/16.
 */
/**
 * Modal Reservasi OK
 * @type {string[]}
 */
app.controller('ModalKunjunganUlangController', modalKunjunganUlangController);
modalKunjunganUlangController.$inject = [
    '$rootScope',
    '$scope',
    'api',
    '$http',
    '$uibModalInstance',
    '$log',
    'DTOptionsBuilder',
    'DTColumnBuilder',
    'DTColumnDefBuilder',
    '$compile',
    'items',
    'CONFIG',
    'toastr',
    '$timeout',
    'EVENT'
];
function modalKunjunganUlangController($rootScope,
                                       $scope,
                                       api,
                                       $http,
                                       $uibModalInstance,
                                       $log,
                                       DTOptionsBuilder,
                                       DTColumnBuilder,
                                       DTColumnDefBuilder,
                                       $compile,
                                       items,
                                       CONFIG,
                                       toastr,
                                       $timeout,
                                       EVENT) {
    // $rootScope.isProcessing = true;
    // $rootScope.$emit('LOADING:EVENT:PROCESSING');

    $scope.data = items;
    if(typeof $scope.data.kunjunganUlang !="undefined"){

        $scope.dataCancel = {
            id : $scope.data.kunjunganUlang.id,
            alasan : ''
        };

    }


    $scope.onChangeTanggalOperasi = onChangeTanggalOperasi;
    $scope.close = close;
    $scope.dismiss = dismiss;
    $scope.pesanJadwal = pesanJadwal;
    $scope.batal = batal;
    if(typeof $scope.data.data_rujukan != "undefined" ){
        if($scope.data.data_rujukan.kunjungan_ulang.length !=0){
            $scope.data.tanggalOperasi= moment($scope.data.data_rujukan.kunjungan_ulang.tanggal).format('DD-MM-YYYY');
            $scope.data.jamMulaiOperasi = $scope.data.data_rujukan.kunjungan_ulang.jam_mulai ;
            $scope.data.jamSelesaiOperasi = $scope.data.data_rujukan.kunjungan_ulang.jam_akhir ;
        }
    }

    function pesanJadwal() {
        console.log("ini item kunjungan");

        var reservasi = {
            tanggal: moment($scope.data.tanggalOperasi).format('YYYY-MM-DD'),
            mulai: $scope.data.jamMulaiOperasi,
            selesai: $scope.data.jamSelesaiOperasi,
            pasien: $scope.data,
        };

        console.log("ini item reservasi");
        console.log(reservasi);

        var req = {
            method: 'POST',
            url: CONFIG.RESERVASI_KUNJUNGAN_ULANG,
            data: $.param(reservasi),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http.post(req.url, angular.copy(reservasi)).then(success, error);
        function success(result) {
            console.log('ini iteemmmmmmmmmm Kunjungan');
            console.log(result);
            console.log($scope.data.rujukan);
            //   $http.post(req.url, angular.copy(reservasi)).then(sukses, gagal);

            toastr.success('Berhasil Daftar Kunjungan Ulang', 'Sukses!');
            $timeout(function () {
                close({
                    result: result,
                    kunjungan_ulang  : 1
                });
            }, 500);

        }

        function error(err) {
            console.log(err);
        }
    }




    function batal() {
        console.log("data batal");
        console.log($scope.dataCancel);

        $http.post(base_url + '/api/kunjungan_ulang/batal', $scope.dataCancel).then(function(result){
            toastr.success('Batalkan Kunjungan Ulang ODC / ODS Berhasil !', 'Sukses');
            close({result: result, kunjungan_ulang: 0});
        }, function(err){
            toastr.error('Terdapat kesalahan', 'Gagal');
            console.log(err);
        });

        // api.kunjunganUlang.batalkanKunjungan(angular.copy($scope.dataCancel)).$promise.then(function(result){
        //     console.log(result);
        //     toastr.success('Batalkan Kunjungan Ulang Rawat Jalan Berhasil !', 'Sukses');
        //
        //     close({result: result, kunjungan_ulang: 0});
        // }).catch(function(err){
        //
        //     console.log(err);
        // });
        // close({});
    }


    function batal1(){

        // var uid = items.kunjunganUlang.data.uid;
        var uid = $scope.data.kunjunganUlang.uid;

        $http.post(base_url + '/api/kunjungan_ulang/batal', {uid: uid}).then(function(result){
            close({result: result, kunjungan_ulang: 0});
            toastr.success('Berhasil Daftar Kunjungan Ulang', 'Sukses!');
        }, function(err){
            console.log(err);
        });
    }

    function onChangeTanggalOperasi() {
        var tanggalOperasi = $scope.data.tanggalOperasi;
        $scope.openJadwalOperasi = true;
    }

    function close(reservasiOk) {
        $uibModalInstance.close(reservasiOk);
        $rootScope.$emit(EVENT.CANCEL_RESERVASI_OK);
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }

}