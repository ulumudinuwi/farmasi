/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalChangeTarifTindakanController', modalChangeTarifTindakanController);
modalChangeTarifTindakanController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$log', '$compile', 'items'];
function modalChangeTarifTindakanController($scope, api, $http, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $log, $compile, items) {

    var url = base_url + "/api/master/tindakan/tarif/1";
    var _tarif = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: url,
            type: 'POST'
        })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('serverSide', true)
        .withPaginationType('full_numbers');
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('kode').withClass('row-5 text-center').withTitle('Pilih').notSortable().renderWith(renderPilih),
        DTColumnBuilder.newColumn('kode').withTitle('Kode').withClass('row-10'),
        DTColumnBuilder.newColumn('nama').withTitle('Nama'),
        DTColumnBuilder.newColumn('biaya').withTitle('Biaya').withClass('row-10')
    ];


    $scope.selectTarif = selectTarif;
    $scope.close = close;
    $scope.dismiss = dismiss;


    function renderPilih(data, type, full, meta) {
        return '<input type="checkbox"  ng-model="tarif' + full.id + '" ng-click="selectTarif(tarif' + full.id + ',' + full.id + ')"/>';
    }

    function selectTarif(data, id) {
        if (data) {
            $http.get(base_url + '/api/master/tindakan/tarif_by', {
                params: {
                    id: id
                }
            }).then(function (tarif) {
                _tarif.push(tarif.data);
            })
        } else {
            _.remove(_tarif, function (data) {
                return data.id == id;
            });
        }
    }


    function dismiss() {
        $uibModalInstance.dismiss();
    }

    function close() {
        console.log("data : ", angular.copy(_tarif));
        $uibModalInstance.close(_tarif);
    }



}