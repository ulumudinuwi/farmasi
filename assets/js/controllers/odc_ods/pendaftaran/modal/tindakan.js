/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalPendaftaranTindakanController', modalPendaftaranTindakanController);
modalPendaftaranTindakanController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$log', '$compile', 'items', '$timeout', 'toastr'];
function modalPendaftaranTindakanController($scope, api, $http, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $log, $compile, items, $timeout, toastr) {

    console.log("items tindakan, ", items);

    if (!items.kelompokPasien || !items.layanan) {
        toastr.error('Pilih layanan dan kelompok pasien dahulu', 'Gagal');
    }

    var layanan = {
        kelas: (items.jenis_layanan == 4) ? 2 : 0, // default rawat jalan
        kelompok_pasien: items.kelompokPasien,
        detail: {
            id: items.layanan
        },
        dokter: {
            id_pegawai: items.dokter
        }
    };

    var url = base_url + "/api/master/tindakan/tarif_layanan?layanan=" + JSON.stringify(layanan);
    var _tarif = [];

    console.log({
        items: items,
        url: url
    });

    $scope.selectTarif = selectTarif;
    $scope.close = close;
    $scope.dismiss = dismiss;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: url,
            type: 'GET'
        })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);

            if (data.jenis == 'Kelompok') {
                $('td', row).addClass('bg-slate-300');
                $('td', row).eq(0).find('input[type=checkbox]').hide();
                $('td', row).eq(3).find('.tarif').hide();
            }
        })
        .withOption('serverSide', false)
        .withPaginationType('full_numbers');
    $scope.dtColumns = [
        // t.id as tarif_id, td.id as tarif_detail_id, t.kode, t.nama, t.kelas, td.kelas_id, td.jenis_kelompok_pasien, td.jenis_dokter, td.jenis, td.tarif, td.jasa_sarana, td.dokter, td.intensif_karyawan, td.rumah_sakit, td.dokter_operator, td.dokter_pendamping, td.anethesi, td.sewa_ok
        DTColumnBuilder.newColumn('kode').withClass('row-5 text-center').withTitle('Pilih').notSortable().renderWith(renderPilih),
        DTColumnBuilder.newColumn('kode').withTitle('Kode').withClass('row-10'),
        DTColumnBuilder.newColumn('nama').withTitle('Nama'),
        DTColumnBuilder.newColumn('tarif').withTitle('Tarif').renderWith(function (row, type, data, meta) {

            return '<span class="tarif">{{ ' + data.tarif + ' | currency : "Rp. "  }}</span>';
        }),
        // DTColumnBuilder.newColumn('jasa_sarana').withTitle('Jasa Sarana').renderWith(function (row, type, data, meta) {
        //     return '{{ ' + data.jasa_sarana + ' | currency : "Rp. "  }}';
        // })
    ];


    function renderPilih(data, type, full, meta) {

        return '<input type="checkbox"  ng-model="tarif' + full.tarif_detail_id + '" ng-click="selectTarif(tarif' + full.tarif_detail_id + ',' + full.tarif_detail_id + ',' + items.layanan + ',' + items.kelompokPasien + ')"/>';
    }

    function selectTarif(data, id, layanan_id, kelompok_pasien_id) {

        console.log("data tarif", data);

        if (data) {
            $http.get(base_url + '/api/master/tindakan/tarif_by', {
                params: {
                    id: id,
                    layanan_id: layanan_id,
                    kelompok_pasien_id: kelompok_pasien_id
                }
            }).then(function (tarif) {
                console.log("tarif terpilih, ", tarif);

                if(tarif.data.jenis_tarif == 'Rincian') {
                    $http.get(base_url + '/api/master/tindakan/tarif_parent', {
                        params: {
                            id: tarif.data.tarif_pelayanan_id
                        }
                    }).then(function(resultParent){
                        console.log(resultParent);
                        tarif.data.nama = resultParent.data.nama + " " + tarif.data.nama;
                        _tarif.push(tarif.data);
                    });
                }


            })
        } else {
            _.remove(_tarif, function (data) {
                return data.id == id;
            });
        }
    }


    function dismiss() {
        $uibModalInstance.dismiss();
    }

    function close() {
        console.log("data : ", angular.copy(_tarif));
        $uibModalInstance.close(_tarif);
    }


}