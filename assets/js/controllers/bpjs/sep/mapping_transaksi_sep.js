/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsSepMappingTransController', bpjsSepMappingTransController);

bpjsSepMappingTransController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsSepMappingTransController($rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.sep = {};

    vm.submit = function(){
        var _data =  {
            "request" : {
                "t_sep" : angular.copy(vm.sep)
            }
        };

        $.ajax({
            url: base_url + '/api/bpjs/sep/map_trans',
            type: 'POST',
            data: 'data=' + JSON.stringify(_data),
            success: function (result) {
                var _result = JSON.parse(result);
                console.log(_result);
                if (!_result) {
                    toastr.error('Sistem Error', '');
                    return;
                }

                if (_result.response.metadata.code != 200) {
                    toastr.warning(_result.response.metadata.message, _result.response.metadata.code);
                }
            },
            error: function (err) {
                console.log(err);
            }
        });


    };

    console.log("controller bpjs peserta");
}