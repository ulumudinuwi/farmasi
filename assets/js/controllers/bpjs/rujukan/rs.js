/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsRujukanRsController', bpjsRujukanRsController);

bpjsRujukanRsController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsRujukanRsController($rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.peserta = {};

    vm.cekNoKartu = function () {
        $resource(base_url + '/api/bpjs/rujukan/no_kartu_rs/').get({
            no_kartu : vm.peserta.noKartu
        }).$promise.then(function (result) {
            var result = JSON.parse(result.data);
            console.log(result);
            vm.message = result.metadata.message;

            if(result.response.peserta){
                vm.peserta = result.response.peserta;
            }

        }).catch(function (err) {
            console.log(err);
        });
    };

    vm.cekNoRujukan = function () {
        $resource(base_url + '/api/bpjs/rujukan/no_rujukan_rs/').get({
            no_rujukan : vm.peserta.noRujukan
        }).$promise.then(function (result) {
            console.log(result.data);
            var result = JSON.parse(result.data);
            console.log(result);
            vm.messageNik = result.metadata.message;

            if(result.response.peserta){
                vm.peserta = result.response.peserta;
            }

        }).catch(function (err) {
            console.log(err);
        });
    };

    console.log("controller bpjs peserta");
}