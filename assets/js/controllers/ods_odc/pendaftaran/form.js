app.controller('PendaftaranController', pendaftaranController);
pendaftaranController.$inject = ['$rootScope', '$scope', 'api', '$http', 'toastr', '$uibModal', '$q', 'select2ChainedData', 'EVENT', 'CONFIG'];
function pendaftaranController($rootScope, $scope, api, $http, toastr, $uibModal, $q, select2ChainedData, EVENT, CONFIG) {


    var _dataPerusahaan = [
        {
            id: 1,
            nama: 'Rumah Sakit Umum Daerah Soreang'
        },
        {
            id: 2,
            nama: 'RS Pertamina Cirebon (RSPC)'
        },
        {
            id: 3,
            nama: 'RS ARJAWINANGUN'
        },
        {
            id: 4,
            nama: 'RSTP SIDAWANGI'
        },
        {
            id: 5,
            nama: 'RSUD WALED'
        },
        {
            id: 6,
            nama: 'RS Putera Bahagia Cirebon (RSPB)'
        }
    ];
    var _dataRekanan = [
        {
            id: 1,
            nama: 'AA International',
        },
        {
            id: 2,
            nama: 'Abadi Symlinks, PT',
        },
        {
            id: 3,
            nama: 'Adira Dinamika',
        },
        {
            id: 4,
            nama: 'Askum Bumiputera 1912, PT',
        },
        {
            id: 5,
            nama: 'Asuransi AIA Finance (AIG Life)',
        },
    ];

    $scope.pendaftaran = {};

    $scope.dataRekanan = _dataRekanan;

    $scope.data = {};
    $scope.data.jenisKelamin = 1;
    $scope.data.kewarganegaraan = 1;
    $scope.data.tanggalregister = moment().format('DD/MM/YYYY');
    $scope.data.tgl_reg = moment().format('DD-MM-YYYY');
    $scope.data.tgl_lahir = '';
    $scope.data.isOldPasien = false;
    $scope.dataPerusahaanRujukan = _.orderBy(_dataPerusahaan, ['nama'], ['asc']);

    $scope.data.tindakan = [];

    $scope.isTambah = true;
    $scope.isTambahRumahSakit = false;
    $scope.isTambahPerusahaan = false;
    $scope.isBiodataDisabled = false;
    $scope.isTambahRumahSakit = false;
    $scope.isSubmit = false;
    $scope.isTambahRumahSakit = false;
    $scope.isEditTarifTindakan = false;
    $scope.isTindakanEdit = false;
    $scope.wrongDate = false;


    $scope.tambahRumahSakit = tambahRumahSakit;
    $scope.cetakNota = cetakNota;
    $scope.cetakTracer = cetakTracer;
    $scope.cetakStatus = cetakStatus;
    $scope.cetakFJP = cetakFJP;
    $scope.tambahDataPerusahaanRujukan = tambahDataPerusahaanRujukan;
    $scope.onChangeTitle = onChangeTitle;
    $scope.selectedPegawai = selectedPegawai;
    $scope.selected = selected;
    $scope.save = save;
    $scope.tambahData = tambahData;
    $scope.onChangeProvinsi = onChangeProvinsi;
    $scope.onChangeKota = onChangeKota;
    $scope.onChangeKecamatan = onChangeKecamatan;
    $scope.onChangeDesa = onChangeDesa;
    $scope.onChangeKelompokPasien = onChangeKelompokPasien;
    $scope.onChangeTanggalLahir = onChangeTanggalLahir;
    $scope.onChangeUmur = onChangeUmur;
    $scope.onChangeLayanan = onChangeLayanan;
    $scope.onChangeTarifTindakanQty = onChangeTarifTindakanQty;
    $scope.onChangeTarifTindakanDiskon = onChangeTarifTindakanDiskon;
    $scope.tindakanQtyChange = tindakanQtyChange;
    $scope.tindakanDiskonChange = tindakanDiskonChange;
    $scope.verifikasiSep = verifikasiSep;
    $scope.openModalPendaftaranTindakan = openModalPendaftaranTindakan;
    $scope.toggleAnimation = toggleAnimation;
    $scope.deleteTindakan = deleteTindakan;


    function tambahRumahSakit() {
        $scope.isTambahRumahSakit = true;
    }

    function cetakNota() {

        var pendaftaran = angular.copy($scope.pendaftaran);
        console.log(pendaftaran);
        window.open(base_url + '/api/ods_odc/pendaftaran/cetak_nota/' + pendaftaran.pelayanan.id + '/' + pendaftaran.nomor_antrian, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakTracer() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/ods_odc/pendaftaran/cetak_tracer/' + pendaftaran.pelayanan.id + '/' + pendaftaran.nomor_antrian, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakStatus() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/ods_odc/pendaftaran/cetak_status?no_rekam_medis=' + '02.00.000005', '_blank', 'location=yes,height=720,width=920,scrollbars=yes,status=yes');
    }

    function cetakFJP() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/ods_odc/pendaftaran/cetak_fjp/umum/11?penanggung=1', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function tambahDataPerusahaanRujukan(fkrtl) {
        var last = $scope.dataPerusahaanRujukan.length;
        var obj = {
            id: last + 1,
            nama: fkrtl.namaRumahSakit
        };
        _dataPerusahaan.push(obj);
        $scope.dataPerusahaanRujukan = _.orderBy(_dataPerusahaan, ['nama'], ['asc']);
        $scope.isTambahRumahSakit = false;
    }

    function resetKelompokPasien() {
        $scope.isUmum = false;
        $scope.isBpjs = true;
        $scope.isMcs = false;
        $scope.isPerusahaan = false;
        $scope.isAsuransi = false;
        $scope.isInternal = false;
    }

    function onChangeTitle(title) {

        switch (title.id) {
            case '1':

                $scope.data.jenisKelamin = 1;
                break;
            case '2':
            case '3':
                $scope.data.jenisKelamin = 0;
                break;
            default:

                $scope.data.jenisKelamin = 3;
                break;
        }
    }

    function selectedPegawai(pegawai) {
        if (pegawai) {
            console.log(pegawai);
            $scope.data.pegawaiId = pegawai.originalObject;
        }

    }

    function selected(item) {

        if (item) {
            resetKelompokPasien();
            var obj = item.originalObject;

            var selectedProvinsi = angular.copy($scope.dataProvinsi)[_.findIndex(angular.copy($scope.dataProvinsi), {id: obj.provinsi_id})];
            var selectedKota = angular.copy($scope.dataKota)[_.findIndex(angular.copy($scope.dataKota), {id: obj.kabupaten_id})];

            console.log(obj);

            $scope.$emit(EVENT.OLD_PATIENT);
            $scope.isBiodataDisabled = true;

            $scope.data = angular.copy(obj);
            $scope.data.tanggal = new Date($scope.data.tanggal);
            $scope.data.tanggalregister = moment().format('DD/MM/YYYY');
            $scope.data.tgl_reg = moment().format('DD-MM-YYYY');

            $scope.data.namaPasien = obj.nama;

            $scope.data.provinsi = obj.provinsi_id;
            $scope.data.desa = obj.kelurahan_id;
            $scope.data.kota = obj.kabupaten_id;
            $scope.data.kecamatan = obj.kecamatan_id;

            console.log("data provinsi dan kota, ", {
                provinsi: $scope.data.provinsi,
                kota: $scope.data.kota,
                dataKota: $scope.dataKota,
                kabupatenId: obj.kabupaten_id
            });

            $scope.data.identitasId = angular.copy($scope.dataIdentitas)[_.findIndex(angular.copy($scope.dataIdentitas), {id: obj.identitas_id})];
            $scope.data.noIdentitas = obj.no_identitas;
            $scope.data.telepon1 = obj.no_telepon_1;
            $scope.data.tempatlahir = obj.tempat_lahir;
            $scope.data.tanggallahir = moment(obj.tgl_lahir, "YYYY-MM-DD").format("DD/MM/YYYY");
            $scope.data.tgl_lahir = moment(obj.tgl_lahir, "YYYY-MM-DD").format("DD/MM/YYYY");
            $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');
            $scope.data.golonganDarah = obj.gol_darah_id;
            $scope.data.agamaId = angular.copy($scope.dataAgama)[_.findIndex(angular.copy($scope.dataAgama), {id: obj.agama_id})];
            $scope.data.statusKawin = angular.copy($scope.dataStatusKawin)[_.findIndex(angular.copy($scope.dataStatusKawin), {id: obj.status_kawin_id})];
            $scope.data.pendidikanId = angular.copy($scope.dataPendidikan)[_.findIndex(angular.copy($scope.dataPendidikan), {id: obj.pendidikan_id})];
            $scope.data.pekerjaanId = angular.copy($scope.dataPekerjaan)[_.findIndex(angular.copy($scope.dataPekerjaan), {id: obj.pekerjaan_id})];

            $scope.data.titleId = angular.copy($scope.dataTitle)[_.findIndex(angular.copy($scope.dataTitle), {id: obj.title_id})];
            $scope.data.kelompokpasienId = angular.copy($scope.dataKelompokPasien)[_.findIndex(angular.copy($scope.dataKelompokPasien), {id: "2"})]; //default id kepulauan bangka belitun
            $scope.data.jenisKelamin = obj.jenis_kelamin;
            $scope.data.kewarganegaraan = obj.kewarganegaraan;
            $scope.data.golonganDarah = obj.gol_darah_id;
            $scope.data.isOldPasien = true;

            $scope.data.rujukanId = angular.copy($scope.dataRujukan)[2];


            $q.all([
                $http.get(base_url + "/api/master/wilayah/search/", {
                    params: {
                        'id': obj.kabupaten_id
                    }
                }),
                $http.get(base_url + "/api/master/wilayah/search/", {
                    params: {
                        'id': obj.kecamatan_id
                    }
                }),
                $http.get(base_url + "/api/master/wilayah/search/", {
                    params: {
                        'id': obj.kelurahan_id
                    }
                }),
                $http.get(base_url + "/api/master/tindakan/init", {
                    params: {
                        status_pasien: 'lama'
                    }
                })
            ]).then(function (results) {
                console.log("selected wilayah untuk pasien lama, ", results);

                // $scope.dataKota = results[0].data;
                // $scope.dataKecamatan = results[1].data;
                // $scope.dataDesa = results[2].data;
                //
                // $scope.data.kota = angular.copy($scope.dataKota)[0];
                // $scope.data.kecamatan = angular.copy($scope.dataKecamatan)[0];
                // $scope.data.desa = angular.copy($scope.dataDesa)[0];
                $scope.dataTindakan = _.map(results[3].data.tarif, function (data) {
                    data.qty = 1;
                    data.total = data.biaya;
                    return data;
                });
                $scope.tarifTotal = results[3].data.total;

            });

        } else {
            $scope.isBiodataDisabled = false;
        }
    }

    function deleteTindakan(idx) {

        $scope.dataTindakan.splice(idx, 1);
        $scope.tarifTotal = 0;
        $scope.tarifTotal = _.sum(_.map($scope.dataTindakan, function (data) {
            return parseFloat(data.total);
        }));

    }

    function initData() {
        $q.all([
            $http.get(base_url + "/api/master/wilayah/provinsi"),
            $http.get(base_url + "/api/master/wilayah/kota/", {
                params: {
                    'provinsiId': "9" // default kepulauan bangka belitung
                }
            }),
            $http.get(base_url + "/api/master/identitas"),
            $http.get(base_url + "/api/master/status_kawin"),
            $http.get(base_url + "/api/master/agama"),
            $http.get(base_url + "/api/master/pendidikan"),
            $http.get(base_url + "/api/master/pekerjaan"),
            $http.get(base_url + "/api/master/rujukan"),
            $http.get(base_url + "/api/master/kelompok_pasien"),
            $http.get(base_url + "/api/master/paket_jaminan"),
            $http.get(base_url + "/api/master/layanan/rawat_jalan"),
            $http.get(base_url + "/api/master/hubungan_pasien"),
            $http.get(base_url + "/api/master/dokter_poli"),
            $http.get(base_url + "/api/master/title"),
            $http.get(base_url + "/api/master/tindakan/init", {
                params: {
                    status_pasien: 'baru'
                }
            })

        ]).then(function (results) {


            $scope.dataProvinsi = angular.copy(results[0].data);
            $scope.dataKota = angular.copy(results[1].data);

            $scope.dataIdentitas = angular.copy(results[2].data);
            $scope.data.identitasId = angular.copy($scope.dataIdentitas)[_.findIndex(angular.copy($scope.dataIdentitas), {id: "1"})]; //default id kepulauan bangka belitun

            $scope.dataStatusKawin = angular.copy(results[3].data);


            $scope.data.provinsi = angular.copy($scope.dataProvinsi)[_.findIndex(angular.copy($scope.dataProvinsi), {id: "9"})]; //default id kepulauan bangka belitun

            $scope.dataAgama = angular.copy(results[4].data);
            $scope.dataPendidikan = angular.copy(results[5].data);
            $scope.dataPekerjaan = angular.copy(results[6].data);
            $scope.dataRujukan = angular.copy(results[7].data);


            //default rujukan : pasien data sendiri
            $scope.data.rujukanId = angular.copy($scope.dataRujukan)[_.findIndex(angular.copy($scope.dataRujukan), {id: "3"})]; //default id kepulauan bangka belitun;

            $scope.dataKelompokPasien = angular.copy(results[8].data);
            $scope.data.kelompokpasienId = angular.copy($scope.dataKelompokPasien)[_.findIndex(angular.copy($scope.dataKelompokPasien), {id: "2"})]; //default id kepulauan bangka belitun
            $scope.dataPaketJaminan = angular.copy(results[9].data);
            $scope.dataLayanan = angular.copy(results[10].data);
            $scope.dataHubunganPasien = angular.copy(results[11].data);

            $scope.dataPegawai = [];
            $scope.dataTitle = angular.copy(results[13].data);

            _.remove(results[14].data.tarif, {
                kode: '3.02.01.03.03.00.02'
            });

            $scope.dataTindakan = _.map(angular.copy(results[14].data.tarif), function (data, i) {
                data.total = data.biaya;
                data.qty = 1;
                return data;
            });

            console.log("RESULT 14, ", $scope.dataTindakan);

            $scope.tarifTotal = results[14].data.total;
            // $scope.dataKecamatan = angular.copy(results[15].data);
            $scope.data.kota = angular.copy($scope.dataKota)[_.findIndex(angular.copy($scope.dataKota), {id: "179"})]; //default id kepulauan bangka belitun
            $scope.data.tanggalregister = moment().format('DD/MM/YYYY');
        }).catch(function (err) {
            console.log(err);
        });
    }

    function save() {

        console.log("data save, ", $scope.formAngular);

        $rootScope.isProcessing = true;
        $rootScope.$emit('LOADING:EVENT:PROCESSING');

        api.Nomor.getNomorIgd().$promise.then(function (result) {
            $scope.data.nomorAntrian = result[1].no_reg;
            if (!$scope.data.isOldPasien) {
                $scope.data.no_rekam_medis = result[0];
            }

            $scope.data.tindakan = $scope.dataTindakan;
            var _data = angular.copy($scope.data);

            var req = {
                method: 'POST',
                url: base_url + '/api/ods_odc/pendaftaran/save',
                data: 'data=' + JSON.stringify(_data),
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                }
            };
            return $http(req);
        }).then(function (result) {

            toastr.success('Simpan ODS / ODC Berhasil !', 'Sukses');
            $rootScope.isProcessing = false;
            $scope.isTambah = false;
            $scope.isSubmit = true;
            $scope.pendaftaran = result.data;
            $rootScope.$emit('LOADING:EVENT:FINISH');

        }).catch(function (err) {
            toastr.error('Terjadi Kesalahan Sistem', 'Gagal Daftar');
            $rootScope.$emit('LOADING:EVENT:FINISH');
        });


    }

    function tambahData() {
        window.location.href = base_url + "/rawat_jalan/pendaftaran";
    }

    function onChangeProvinsi(provinsi) {

        $http.get(base_url + "/api/master/wilayah/kota/", {
            params: {
                'provinsiId': provinsi.id
            }
        }).then(
            function success(result) {
                $scope.dataKota = _.map(result.data, function (data) {
                    return {
                        id: data.id,
                        nama: data.nama
                    }
                });
                $scope.$broadcast('change::select2');

            }, function error(err) {
                //console.log(err);
            })
    }

    function onChangeKota(kota) {
        var defer = $q.defer();

        if (kota) {
            $http.get(base_url + "/api/master/wilayah/kecamatan/", {
                params: {
                    'kotaId': kota.id
                }
            }).then(
                function success(result) {
                    $scope.dataKecamatan = _.map(result.data, function (data) {
                        return {
                            id: data.id,
                            nama: data.nama
                        }
                    });
                }, function error(err) {
                    defer.reject(err);
                });

        }


    }

    function onChangeKecamatan(kecamatan) {
        var defer = $q.defer();
        if (kecamatan) {


            $http.get(base_url + "/api/master/wilayah/kelurahan/", {
                params: {
                    'kecamatanId': kecamatan.id
                }
            }).then(
                function success(result) {
                    //console.log("desa");
                    //console.log(result);
                    $scope.dataDesa = _.map(result.data, function (data) {
                        return {
                            id: data.id,
                            nama: data.nama
                        }
                    });
                }, function error(err) {
                    //console.log(err);
                    defer.reject(err);
                })
        }
    }

    function onChangeDesa(desa) {

        var defer = $q.defer();
        if (desa) {
            $http.get(base_url + "/api/master/wilayah/search/", {
                params: {
                    'id': desa
                }
            }).then(
                function success(result) {
                    //console.log("desa");
                    console.log(result);
                    $scope.data.kodepos = angular.copy(result.data[0].kodepos);
                    // $scope.dataDesa = angular.copy(result.data);
                }, function error(err) {
                    //console.log(err);
                    defer.reject(err);
                })
        }
    }

    function onChangeKelompokPasien(kelompokPasien) {
        switch (kelompokPasien.nama) {
            case 'Asuransi' :
                $scope.isAsuransi = true;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isPerusahaan = false;
                $scope.isInternal = false;
                break;
            case 'BPJS' :
                $scope.isBpjs = true;
                $scope.isUmum = false;
                $scope.isMcs = false;
                $scope.isPerusahaan = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                break;
            case 'Internal' :
                $scope.isInternal = true;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isPerusahaan = false;
                $scope.isAsuransi = false;
                break;
            case 'MCS' :
                $scope.isMcs = true;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isPerusahaan = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                break;
            case 'Kontraktor' :
                $scope.isPerusahaan = true;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                break;
            case 'Umum' :
                $scope.isPerusahaan = false;
                $scope.isUmum = true;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                break;
            default:
                resetKelompokPasien();
                break;

        }
    }

    function onChangeTanggalLahir(tanggalLahir) {
        console.log("tanggal lahir, ", tanggalLahir);
        var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
        var bulan = parseInt(tanggalLahir.toString().substr(2, 2));
        var tahun = parseInt(tanggalLahir.toString().substr(4, 4));
        console.log("tanggal dan bulan, ", {
            tanggal: tanggal,
            bulan: bulan,
            tahun: tahun
        });


        console.log("umur tahun, ", umur);
        if (tanggal > 31 || bulan > 12) {
            $scope.wrongDate = true;
        } else {
            $scope.wrongDate = false;
            var mTanggalLahir = moment(tanggalLahir, "DDMMYYYY");
            console.log(mTanggalLahir.format("DD-MM-YYYY"));

            var umur = {
                tahun: moment().diff(mTanggalLahir, 'years')
            };
            console.log("umur ", umur);

            $scope.data.umur = umur.tahun;
            $scope.data.tgl_lahir = mTanggalLahir.format("DD-MM-YYYY");

        }

    }

    function onChangeUmur(umur) {
        var age = parseInt(umur);
        var yearNow = moment().year(),
            yearAgo = ((yearNow - age) > 0) ? yearNow - umur : 0;

        var dob = moment([yearAgo, moment().month(), moment().date()]).format();
        $scope.data.tanggallahir = moment(new Date(dob)).format("DD/MM/YYYY");
    }

    function onChangeLayanan(layanan) {


        $q.all([
            $http.get(base_url + "/api/master/dokter_poli/layanan", {
                params: {
                    layanan: layanan.id
                }
            }),
            $http.get(base_url + "/api/master/tindakan/tarif", {
                params: {
                    layanan: layanan.id
                }
            })
        ]).then(function (results) {

            console.log("tarif by layanan default, ", results[1]);

            $scope.dataPegawai = results[0].data.results;
            var tarif = _.map(angular.copy(results[1].data.tarif), function (data, i) {
                data.total = data.biaya;
                data.qty = 1;
                return data;
            });

            if (tarif[0]) {
                $scope.dataTindakan.push(tarif[0]);
                $scope.tarifTotal += parseFloat(_.sum(_.map($scope.dataTindakan, function (data) {
                    return parseFloat(data.total);
                })));
            }


        }).catch(function (err) {
            console.log("error, ", err);
        });
    }

    function onChangeTarifTindakanQty(tindakan) {


        if (tindakan.qty <= 0) {
            // tindakan.qty = 1;
        } else {

            tindakan.biaya *= tindakan.qty;
        }


        // if(tindakan.biaya){
        //     tindakan.biaya *= tindakan.qty;
        // }else {
        //     tindakan.biaya = tindakan.biaya;
        // }
    }

    function onChangeTarifTindakanDiskon(tindakan) {

        if (tindakan.qty && tindakan.diskon) {
            tindakan.biaya -= ((tindakan.qty * tindakan.biaya) * (tindakan.diskon / 100))
        } else {
            tindakan.biaya = tindakan.biaya;
        }
        $scope.tarifTotal += parseFloat(tindakan.biaya);
    }

    function tindakanQtyChange(idx, qty) {
        $scope.dataTindakan[idx].qty = qty;
        $scope.dataTindakan[idx].total = $scope.dataTindakan[idx].biaya * qty;
        $scope.dataTindakan[idx].anthesi = $scope.dataTindakan[idx].anthesi * qty;
        $scope.dataTindakan[idx].dokter = $scope.dataTindakan[idx].dokter * qty;
        $scope.dataTindakan[idx].dokter_operator = $scope.dataTindakan[idx].dokter_operator * qty;
        $scope.dataTindakan[idx].dokter_pendamping = $scope.dataTindakan[idx].dokter_pendamping * qty;
        $scope.dataTindakan[idx].jasa_sarana = $scope.dataTindakan[idx].jasa_sarana * qty;
        $scope.dataTindakan[idx].rumah_sakit = $scope.dataTindakan[idx].rumah_sakit * qty;
        $scope.dataTindakan[idx].sewa_ok = $scope.dataTindakan[idx].sewa_ok * qty;
        $scope.dataTindakan[idx].tenaga_ahli = $scope.dataTindakan[idx].tenaga_ahli * qty;
        $scope.tarifTotal = 0;
        $scope.tarifTotal += parseFloat(_.sum(_.map($scope.dataTindakan, function (data) {
            return parseFloat(data.total);
        })));
    }

    function tindakanDiskonChange(idx, diskon) {

        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalAnthesi = ($scope.dataTindakan[idx].anthesi * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalDokter = ($scope.dataTindakan[idx].dokter * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalDokterOperator = ($scope.dataTindakan[idx].dokter_operator * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalDokterPendamping = ($scope.dataTindakan[idx].dokter_pendamping * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalJasaSarana = ($scope.dataTindakan[idx].jasa_sarana * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalRumahSakit = ($scope.dataTindakan[idx].rumah_sakit * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalSewaOk = ($scope.dataTindakan[idx].sewa_ok * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalTenagaAhli = ($scope.dataTindakan[idx].tenaga_ahli * $scope.dataTindakan[idx].qty) * diskon;

        var total = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) - diskonTotal;
        var anthesi = ($scope.dataTindakan[idx].anthesi * $scope.dataTindakan[idx].qty) - diskonTotalAnthesi;
        var dokter = ($scope.dataTindakan[idx].dokter * $scope.dataTindakan[idx].qty) - diskonTotalDokter;
        var dokterOperator = ($scope.dataTindakan[idx].dokter_operator * $scope.dataTindakan[idx].qty) - diskonTotalDokterOperator;
        var dokterPendamping = ($scope.dataTindakan[idx].dokter_pendamping * $scope.dataTindakan[idx].qty) - diskonTotalDokterPendamping;
        var jasaSarana = ($scope.dataTindakan[idx].jasa_sarana * $scope.dataTindakan[idx].qty) - diskonTotalJasaSarana;
        var rumahSakit = ($scope.dataTindakan[idx].rumah_sakit * $scope.dataTindakan[idx].qty) - diskonTotalRumahSakit;
        var sewaOk = ($scope.dataTindakan[idx].sewa_ok * $scope.dataTindakan[idx].qty) - diskonTotalSewaOk;
        var tenagaAhli = ($scope.dataTindakan[idx].tenaga_ahli * $scope.dataTindakan[idx].qty) - diskonTotalTenagaAhli;

        $scope.dataTindakan[idx].total = total;
        $scope.dataTindakan[idx].anthesi = anthesi;
        $scope.dataTindakan[idx].dokter = dokter;
        $scope.dataTindakan[idx].dokter_operator = dokterOperator;
        $scope.dataTindakan[idx].dokter_pendamping = dokterPendamping;
        $scope.dataTindakan[idx].jasa_sarana = jasaSarana;
        $scope.dataTindakan[idx].rumah_sakit = rumahSakit;
        $scope.dataTindakan[idx].sewa_ok = sewaOk;
        $scope.dataTindakan[idx].tenaga_ahli = tenagaAhli;


        $scope.tarifTotal = 0;
        $scope.tarifTotal += parseFloat(_.sum(_.map($scope.dataTindakan, function (data) {
            return parseFloat(data.total);
        })));
        console.log($scope.dataTindakan[idx]);
    }

    function verifikasiSep(nosep) {
        $rootScope.$emit('LOADING:EVENT:PROCESSING');
        $http.get(CONFIG.VERIFIKASI_SEP, {
            params: {
                no_sep: nosep
            }
        }).then(function (result) {
            console.log(result);
            if (!result.data.response) {
                toastr.error('NO SEP belum di inputkan', 'error');
                $rootScope.$emit('LOADING:EVENT:FINISH');
                return;
            }
            if (result.data.response.metadata.code == 200) {
                $scope.data.namapeserta = result.data.response.response.peserta.nama;
                toastr.success(result.data.response.metadata.message, 'sukses');
            } else {
                toastr.error(result.data.response.metadata.message, 'error');
            }
            $rootScope.$emit('LOADING:EVENT:FINISH');
            console.log("result verifikasi, ", result);
        });
    }

    function openModalPendaftaranTindakan(size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalPendaftaranTindakan.html',
            controller: 'ModalPendaftaranTindakanController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        layanan: $scope.data.layananId,
                        kelompokPasien: $scope.data.kelompokpasienId.id
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            _.each(selectedItem, function (data) {
                var total = parseFloat(angular.copy($scope.tarifTotal));
                total += parseFloat(data.biaya);
                $scope.tarifTotal = total;
                data.total = data.biaya;
                data.qty = 1;
                $scope.dataTindakan.push(data);
            });
        }, function () {

        });
    }


    function toggleAnimation() {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    }

    function reloadSelectProvinsi(mapProvinsi) {
        var data = [{'id': 9, 'text': 'KEPULAUAN BANGKA BELITUNG'}];
        if (mapProvinsi) {
            console.log("map provinsi ada");
            data = [mapProvinsi];
        }

        $selectProvinsi.empty();
        $selectProvinsi.select2({
            data: data,
            placeholder: 'PILIH PROVINSI',
            ajax: {
                url: base_url + '/api/master/wilayah/provinsi',
                dataType: 'json',
                data: function (param) {
                    return {
                        delay: 0.3,
                        q: param.term
                    }
                },
                processResults: function (data) {
                    return {
                        results: _.map(data.items || data, function (obj) {
                            return {
                                id: obj.id,
                                text: obj.text || obj.nama,
                            }
                        })
                    }
                },
                cache: false,
                minimumInputLength: 3,
            }
        });
    }

    initData();
    resetKelompokPasien();


    $rootScope.$on(EVENT.SWITCH_SHIFT_CLOSE, function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalDaftarKunjungan.html',
            controller: 'ModalDaftarKunjunganController',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.dataTindakan.push(selectedItem);
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    });


}



