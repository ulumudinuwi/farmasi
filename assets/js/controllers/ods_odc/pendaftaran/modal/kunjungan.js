/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalDaftarKunjunganController', modalDaftarKunjunganController);
modalDaftarKunjunganController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$log', '$compile'];
function modalDaftarKunjunganController($scope, api, $http, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $log, $compile) {

    $scope.choose = {};

    var today = {
        tanggalDari : moment().format('YYYY-MM-DD'),
        tanggalSampai : moment().format('YYYY-MM-DD'),
    };

    var url = base_url + '/api/rawat_jalan/daftar_kunjungan/data_daftar_kunjungan?tanggalDari=' + today.tanggalDari + '&tanggalSampai=' + today.tanggalSampai;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: url,
            type: 'POST',
        })
        // or here
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('order', [[0, 'desc']])
        .withOption('createdRow', function (row) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withPaginationType('full_numbers');

    $scope.dtInstance = {};
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {

            return moment(row.tanggal).format('DD-MMM-YYYY H:mm:s');
        }).withOption('searchable', true),
        DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No MR'),
        DTColumnBuilder.newColumn('no_register').withTitle('No Register'),
        DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
        DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
        DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter').withOption('searchable', true),
        DTColumnDefBuilder.newColumnDef(5).withTitle('Action').renderWith(function (data, type, row, meta) {
            var link = '<ul class="icons-list">' +
                '<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
                '<i class="icon-menu9"></i>' +
                '</a>' +

                '<ul class="dropdown-menu dropdown-menu-right">' +
                '<li><a href="#"><i class="icon-circle-close"></i> Batalkan Kunjungan</a></li>' +
                '<li><a ng-click="cancelKunjungan(\'' + row.id + '\')"><i class="icon-file-excel"></i> Nota</a></li>' +
                '<li><a href="#"><i class="icon-file-word"></i> Tracer</a></li>' +
                '<li><a href="#"><i class="icon-file-word"></i> Status</a></li>' +
                '<li><a href="#"><i class="icon-file-word"></i> SJP</a></li>' +
                '</ul>' +
                '</li>' +
                '</ul>';
            // return '<a ng-click="cancelKunjungan(\'' + row.id + '\')" class="btn btn-danger">Batalkan <i class="fa fa-trash-o"></i></a>';
            return link;
        }).withOption('searchable', false)
    ];

    $scope.onChoose = function (tindakan) {
        // console.log(tindakan);
        $scope.choose = tindakan;
    };


    $scope.print = function () {

    };

    $scope.close = function () {
        console.log("data : ", angular.copy($scope.choose));
        $uibModalInstance.close(angular.copy($scope.choose));
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

}