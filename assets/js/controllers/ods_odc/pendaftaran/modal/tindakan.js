/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalPendaftaranTindakanController', modalPendaftaranTindakanController);
modalPendaftaranTindakanController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$log', '$compile', 'items', '$timeout', 'toastr'];
function modalPendaftaranTindakanController($scope, api, $http, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $log, $compile, items, $timeout, toastr) {

    console.log("layanan, ", items);

    if (!items.kelompokPasien || !items.layanan) {
        toastr.error('Pilih layanan dan kelompok pasien dahulu', 'Gagal');
    }
    var layanan = {
        kelas : 0, // default rawat jalan
        kelompok_pasien: items.kelompokPasien,
        detail : items.layanan
    };
    var url = base_url + "/api/master/tindakan/tarif_layanan?layanan=" + JSON.stringify(layanan);
    var _tarif = [];

    console.log({
        items: items,
        url: url
    });

    $scope.selectTarif = selectTarif;
    $scope.close = close;
    $scope.dismiss = dismiss;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: url,
            type: 'POST'
        })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('serverSide', true)
        .withPaginationType('full_numbers');
    $scope.dtColumns = [
        // t.id as tarif_id, td.id as tarif_detail_id, t.kode, t.nama, t.kelas, td.kelas_id, td.jenis_kelompok_pasien, td.jenis_dokter, td.jenis, td.tarif, td.jasa_sarana, td.dokter, td.intensif_karyawan, td.rumah_sakit, td.dokter_operator, td.dokter_pendamping, td.anethesi, td.sewa_ok
        DTColumnBuilder.newColumn('kode').withClass('row-5 text-center').withTitle('Pilih').notSortable().renderWith(renderPilih),
        DTColumnBuilder.newColumn('kode').withTitle('Kode').withClass('row-10'),
        DTColumnBuilder.newColumn('nama').withTitle('Nama'),
        DTColumnBuilder.newColumn('tarif').withTitle('Tarif').renderWith(function (row, type, data, meta) {
            return '{{ ' + data.tarif + ' | currency : "Rp. "  }}';
        }),
        // DTColumnBuilder.newColumn('jasa_sarana').withTitle('Jasa Sarana').renderWith(function (row, type, data, meta) {
        //     return '{{ ' + data.jasa_sarana + ' | currency : "Rp. "  }}';
        // })
    ];


    function renderPilih(data, type, full, meta) {

        return '<input type="checkbox"  ng-model="tarif' + full.tarif_detail_id + '" ng-click="selectTarif(tarif' + full.tarif_detail_id + ',' + full.tarif_detail_id + ')"/>';
    }

    function selectTarif(data, id) {

        console.log("data tarif", id);

        if (data) {
            $http.get(base_url + '/api/master/tindakan/tarif_by', {
                params: {
                    id: id
                }
            }).then(function (tarif) {
                _tarif.push(tarif.data);
            })
        } else {
            _.remove(_tarif, function (data) {
                return data.id == id;
            });
        }
    }


    function dismiss() {
        $uibModalInstance.dismiss();
    }

    function close() {
        console.log("data : ", angular.copy(_tarif));
        $uibModalInstance.close(_tarif);
    }


}