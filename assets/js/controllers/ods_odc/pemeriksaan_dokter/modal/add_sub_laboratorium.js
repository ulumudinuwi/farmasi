/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddSubLaboratoriumController', modalAddSubLaboratorium);
modalAddSubLaboratorium.$inject = ['$scope', 'api', '$http', '$uibModalInstance', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', '$log', '$uibModal'];
function modalAddSubLaboratorium($scope, api, $http, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, $log, $uibModal) {

    $scope.choose = {};
    $scope.qty = 1;

    var url = base_url + '/api/master/laboratorium';
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: url,
            type: 'POST',
        })
        // or here
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('createdRow', function (row) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withPaginationType('full_numbers');

    $scope.dtInstance = {};
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(null).withClass('row-5 text-center').withTitle('Pilih').notSortable().renderWith(function (data, type, full, meta) {


            return '<input type="checkbox"  ng-model="tarif' + data.id + '" ng-click="selectTarif(tarif' + data.id + ',' + data.id + ')"/>';
        }).notVisible(),
        DTColumnBuilder.newColumn('nama').withTitle('Nama').withClass('row-10'),
        DTColumnBuilder.newColumn('status').withTitle('Status').notVisible(),

    ];
    $scope.onChoose = function (tindakan, qty) {
        // console.log(tindakan);
        $scope.choose = tindakan;
    };


    $scope.close = function () {
        console.log("data : ", angular.copy($scope.choose));
        $uibModalInstance.close(angular.copy($scope.choose));
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

}