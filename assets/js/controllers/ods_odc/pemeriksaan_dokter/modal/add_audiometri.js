/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddAudiometriController', modalAddLaboratorium);
modalAddLaboratorium.$inject = ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$uibModal', 'items'];
function modalAddLaboratorium($scope, api, $http, $uibModalInstance, $log, $uibModal, items) {

    console.log(items);

    $scope.choose = {};
    $scope.qty = 1;
    $scope.labs = [];

    $scope.delete = function (i) {
        $scope.labs.splice(i, 1);
    };

    $scope.selected = function (item) {
        if (item) {
            $scope.labs.push(item.originalObject);
            console.log($scope.labs);
        }
    };

    $scope.onChoose = function (tindakan, qty) {
        // console.log(tindakan);
        $scope.choose = tindakan;
    };

    $scope.openModalSubLaboratorium = function (size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddSubLaboratorium.html',
            controller: 'ModalAddSubLaboratoriumController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.dataObat.push(selectedItem);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.close = function () {
        console.log("data : ", angular.copy($scope.choose));
        $uibModalInstance.close(angular.copy($scope.labs));
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

}