var DETAIL_SALES = new function(){
    this.el = {
        table : '#detail',
    };
    this.data = {

    };
    this.method = {
        
    };
    this.onCreate = new function(){
        this.setup = () => {
            $('#member').DataTable();
        };
        this.datatable = () => {
            $(DETAIL_SALES.el.table).DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: provider.url.api.master.sales.list_dt_transaksi,
                    type: "POST",
                    data: (p) => {
                        global.method.blockElement(DETAIL_SALES.el.table);
                        p.sales_id = sales_id;
                    }
                },
                searchDelay: 600,
                processing: true,
                columns: [
                    {data: 'no_fbp', name: 'no_fbp', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return data;
                        }
                    },
                    {data: 'tgl_transaksi', name: 'tgl_transaksi', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return data;
                        }
                    },
                    {data: 'nama_dokter', name: 'nama_dokter', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return data;
                        }
                    },
                    {data: 'nama_barang', name: 'nama_barang', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return data;
                        }
                    },
                    {data: 'harga', name: 'harga', className: 'text-center',
                        render: (data, type, row, meta) => {
                            data = parseFloat(data);
                            return 'Rp.'+global.method.toRupiah(data);
                        }
                    },
                    {data: 'qty', name: 'qty', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return data;
                        }
                    },
                    {data: 'total', name: 'total', className: 'text-center',
                        render: (data, type, row, meta) => {
                            data = parseFloat(data);
                            return 'Rp.'+global.method.toRupiah(data);
                        }
                    },
                    {data: 'diskon', name: 'diskon', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return data+ '%';
                        }
                    },
                    {data: 'grand_total', name: 'grand_total', className: 'text-center',
                        render: (data, type, row, meta) => {
                            data = parseFloat(data);
                            return 'Rp.'+global.method.toRupiah(data);
                        }
                    },
                ],
                aaSorting: [],
                drawCallback: (settings) => {
                    $(DETAIL_SALES.el.table).unblock();
                }
            });

        };
    };
    this.onCreated = () => {
        DETAIL_SALES.onCreate.setup();
        DETAIL_SALES.onCreate.datatable();
    };
}

$(() => {
    DETAIL_SALES.onCreated();
})