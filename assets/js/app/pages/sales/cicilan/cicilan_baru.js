var VM_NEW_PEMBAYARAN = new Vue({
    el: '#pembayaran_baru',
    data : {
        uid_pos : '',
        show_detail : false,
        find_invoice : '',
        detail_member : '',
        detail_transaksi : '',
        detail_pembayaran : '',
        detail_pembelian : '',
        detail_cicilan : '',
        no_kuitansi : '',
        master_cara_bayar: [
            {id: 'kartu_kredit', text: 'KARTU KREDIT'},
            {id: 'kartu_debit', text: 'KARTU DEBIT'},
            {id: 'transfer', text: 'TRANSFER'},
            {id: 'tunai', text: 'TUNAI'},
        ],
        master_ekspedisi: [
            {id: 'JNE', text: 'JNE'},
            {id: 'POS Indonesia', text: 'POS Indonesia'},
            {id: 'J&T', text: 'J&T'},
            {id: 'SiCepat', text: 'SiCepat'},
            {id: 'Ninja Express', text: 'Ninja Express'},
            {id: 'TiKi', text: 'TiKi'},
            {id: 'First Logistics', text: 'First Logistics'},
            {id: 'Indah Logistik', text: 'Indah Logistik'},
            {id: 'Wahana Logistik', text: 'Wahana Logistik'},
            {id: 'Pandu Logistik', text: 'Pandu Logistik'},
            {id: 'RPX', text: 'RPX'},
            {id: 'Cahaya Logistik', text: 'Cahaya Logistik'},
            {id: 'GO-SEND', text: 'GO-SEND'},
            {id: 'Grab Express', text: 'Grab Express'},
        ],
        master_bank : [],
        master_edc : [],  
        invoice_url: '',
        kuitansi_url: '',
        disabled_button: false,
        template_select2 : {
            template_selection : '',
            template_result : '',
        },
        f_title: '',
        cicilan_ke: '',
        pembayaran: {
            metode_pembayaran: [],
            sisa_pembayaran: 0,
            total_pembayaran: 0,
        },
        batas_pembayaran: [],
    },
    methods: {
        find_cara_bayar(find){
            let found = this.master_cara_bayar.find(function(value, key) {
                // console.log('cara_bayar', value.id);
                return value.id == find;
            });

            return found ? found : false;
        },
        find_bank(find){
            let found = this.master_bank.find(function(value, key) {
                return value.id == find;
            });

            return found ? found.text : '-';
        },
        find_edc(find){
            let found = this.master_edc.find(function(value, key) {
                return value.id == find;
            });

            return found ? found.text : '-';
        },
        getDetailPos(){
            global.method.vueBlockElement('#detail_invoice');
            let uid = this.uid_pos;

            axios.post(`${this.global_url.api.sales.pos.get_detail_pos}?uid=${uid}`)
            .then(response => {
                let data = response.data;
                this.detail_member = data.detail_member;
                this.detail_transaksi = data.detail_transaksi;
                this.detail_pembayaran = data.detail_pembayaran;
                this.detail_pembelian = data.detail_pembelian;
                this.detail_cicilan = data.detail_cicilan;
                this.no_kuitansi = data.no_kuitansi;
                this.invoice_url = `${this.global_url.api.sales.pos.print_invoice}?uid=${this.detail_transaksi.uid}`;
                this.kuitansi_url = `${this.global_url.api.sales.pos.print_kuitansi}?uid=${this.detail_transaksi.uid}`;
                // VM_PAGE_HEADER.title = '<i class="icon-credit-card"></i> Pembayaran Baru '+this.detail_transaksi.no_invoice;
                $('#detail_invoice').unblock();
                this.f_title = 'No.Invoice : '+this.detail_transaksi.no_invoice;
            }).catch(xhr => {
               alert('Server Bermasalah')
               $('#detail_invoice').unblock();
            })     
        },
        getCicilanKe(){
            let uid = this.uid_pos;

            axios.post(`${this.global_url.api.sales.cicilan.get_cicilan_ke}?uid=${uid}`)
            .then(response => {
                let data = response.data;
                this.cicilan_ke = data.cicilan_ke;
            }).catch(xhr => {
                alert('Server Bermasalah');
            })     
        },
        get_master_bank() {
            axios.post(this.global_url.api.master.bank.list_data)
            .then(response => {
                console.log('bank',response);
                this.master_bank = this.mapOption(response.data.list);
            }).catch(xhr => {
                console.log(xhr)
            })
        },
        get_master_edc() {
            axios.post(this.global_url.api.master.mesin_edc.list_data)
            .then(response => {
                console.log('edc',response);
                this.master_edc = this.mapOption(response.data.list);
            }).catch(xhr => {
                console.log(xhr)
            })
        },
        total_pembayaran_per_cicilan(cicilan){
            let total = 0;
            // console.log('cicilan ke',cicilan)
            if(this.detail_cicilan[cicilan].length > 0){
                for(item of this.detail_cicilan[cicilan]){
                    total += parseFloat(item.nominal_pembayaran);
                }
            }            
            return total;
        },
        cicilanKe(title){
            return title.replace('_',' ').replace('_','-').toUpperCase();
        },
        text_cicilan(str){
            return str.replace('_',' ').replace('_','-');
        },
        getKetCicilan(cicilan){
            let text = 'Pembayaran '+this.cicilanKe(cicilan)+' No.Invoice - '+this.detail_transaksi.no_invoice;
            return text;
        },
        getKetTotal(){
            let text = 'Pembayaran Kontan No.Invoice - '+this.detail_transaksi.no_invoice;
            return text;
        },
        kuitansi_proc(nominal, ket, no_kuitansi, tanggal){
            return `${this.kuitansi_url}&nominal=${nominal}&ket=${ket}&no_kuitansi=${no_kuitansi}&tanggal=${tanggal}`;
        },
        getSearch(event){
            console.log(event);
            this.uid_pos = event.uid_pos;
            if(!this.uid_pos){
                alert('DATA TIDAK DITEMUKAN');
            }else{
                this.getDetailPos();
                this.getCicilanKe();
                this.show_detail = true;
                this.clearMetodeBayar();
            }
        },
        addPembayaran(){
            let n_p = {
                'cara_bayar' :'',
                'receipt_no' :'',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            };

            let batas_baru = {
                currencySymbol: 'Rp. ',
                minimumValue: 0,
                emptyInputBehavior: 'min',
                currencySymbolPlacement: 'p',
                decimalCharacter: ',',
                decimalPlacesShownOnFocus: 2,
                digitGroupSeparator: '.',
                minimumValue: '0',
                maximumValue: this.sisapembayaran,
            };
            this.batas_pembayaran.push(batas_baru);
            this.pembayaran.metode_pembayaran.push(n_p);
        },
        removePembayaran(i){
            if(this.pembayaran.metode_pembayaran.length == 1)return false;
            this.pembayaran.metode_pembayaran.splice(i,1);
            this.batas_pembayaran.splice(i,1);
        },
        mapOption(array){
            if(typeof array == 'object'){
                let change = array.map(function(item){
                    return {
                        id: item.id,
                        text: item.nama,
                    }
                });

                return change;
            }
        },
        validasiPembayaran(){
            let filter = this.pembayaran.metode_pembayaran.filter((item)=>{
                console.log('ini',item.nominal)
                if(item.cara_bayar == 'tunai'){
                    if(item.cara_bayar == 'tunai' && item.nominal == 0){
                        return true;
                    }else{
                        return false;
                    } 
                }else if(item.cara_bayar != 'tunai' || item.cara_bayar == ''){
                    if(item.nominal == 0 || !item.bank_id){
                        return true;
                    }else{
                        return false;
                    } 
                }
            });
            if(this.pembayaran.metode_pembayaran.length == 0){
                return false;
            }
            if(filter.length > 0){
                return false;
            }
            return true;
        },
        validasiTotalPembayaran(){
            if(this.new_total_pembayaran > this.detail_transaksi.sisa_pembayaran){
                return false;
            }
            return true;
        },
        getFormData(){
            let formData = new FormData();
            formData.append('pos', JSON.stringify(this.detail_transaksi));
            formData.append('member', JSON.stringify(this.detail_member.member));
            formData.append('sales', JSON.stringify(this.detail_member.sales));
            formData.append('pembayaran', JSON.stringify(this.pembayaran));
            return formData;
        },
        onProcess(){
            let _process = new Promise((resolve, reject) => {
                let _vtp = this.validasiTotalPembayaran();
                let _vp = this.validasiPembayaran();
                
                if(!_vp){
                    Swal.fire({
                        type: 'error',
                        title: 'Metode pembayaran belum lengkap',
                    });
                }

                if(!_vtp){
                    Swal.fire({
                        type: 'error',
                        title: 'Pembayaran melebihi total harga yang dibeli',
                    });
                }

                if(_vtp && _vp) resolve({
                    'status': true,
                });

                if(_vtp || _vp) reject({
                    'status': false,
                });
            });
            
            _process.then(()=>{         
                Sweet.fire({
                    title: 'Pembayaran akan dilanjut?',
                    text: "Pastikan informasi yang ada masukan sudah benar !",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '<b><i class="icon-checkmark4"></i></b> Ya',
                    cancelButtonText: '<b><i class="icon-cancel-square"></i></b> Tidak',
                    // reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        global.method.vueBlockUI();
                        
                        let formData = this.getFormData();

                        axios.post(this.global_url.api.sales.cicilan.pembayaran_cicilan, formData)
                        .then(response => {
                            console.log(response);
                            $.unblockUI();
                            Swal.fire({
                                type: 'success',
                                title: response.data.message,
                            });
                            setTimeout(()=>{
                                window.location.href = this.global_url.page.sales.cicilan.new_pembayaran;
                            },2000)
                           
                        }).catch(xhr => {
                            console.log(xhr);
                            Swal.fire({
                                type: 'error',
                                title: 'Terjadi kesalahan',
                            });
                            $.unblockUI();
                        })     
                    }
                });
            }).catch((err) => {
                console.log(err);
                return false;
            });
        },
        clearMetodeBayar(){
            this.pembayaran.metode_pembayaran = [];
            this.batas_pembayaran = [];
        },
    },
    computed: {
        total_harga_barang() {
            let total = 0;
            if(this.detail_pembelian.length > 0){
                for(item of this.detail_pembelian){
                    total += parseFloat(item.grand_total);
                }
            }      
            return total;
        },
        total_pembayaran() {
            let total = 0;
            if(this.detail_pembayaran.length > 0){
                for(item of this.detail_pembayaran){
                    total += parseFloat(item.nominal_pembayaran);
                }
            }            
            return total;
        },
        new_total_pembayaran() {
            let total = 0;
            // console.log(this.pembayaran.metode_pembayaran);
            if(this.pembayaran.metode_pembayaran.length > 0){
                for(item of this.pembayaran.metode_pembayaran){
                    total += parseFloat(item.nominal);
                }
            }          
            // console.log(total,'test')  
            this.pembayaran.total_pembayaran = total;
            return total;
        },
        sisapembayaran(){
            let sisa_pembayaran = this.detail_transaksi.sisa_pembayaran - this.new_total_pembayaran;
            this.pembayaran.sisa_pembayaran = sisa_pembayaran;
            return sisa_pembayaran;
        },
    },
    created() {
        VM_PAGE_HEADER.title = '<i class="icon-credit-card"></i> Pembayaran Baru';
        this.get_master_bank();
        this.get_master_edc(); 
        this.template_select2.template_result = function(data){
            let markup = "<div class='select2-result-repository clearfix'>"+
                "<div class='select2-result-repository__avatar'><img style='border-radius: 0px !important' src='https://img.icons8.com/cotton/64/000000/receipt-approved.png'/></div>"+
                "<div class='select2-result-repository__meta'>"+
                "<div class='select2-result-repository__title'>"+data.text+"</div>";

            markup += "<div class='select2-result-repository__description'>"+data.nama_dokter+"</div>";
            
            markup += "<div class='select2-result-repository__statistics'>"+
                "<div class='select2-result-repository__forks'><i class='fa fa-calendar'></i>"+data.tanggal_transaksi+" </div>" +
                "</div>"+
                "</div>";

            markup += "</div>";

            return markup;
        };
    },
})