var VM_LIST_PEMBAYARAN = new Vue({
    el: '#list_pembayaran',
    data: {
        datatable : '',
        filter_range :  [moment(),moment()],
    },
    methods: {
        drawFilter(){
           this.datatable.draw();
        },
    },
    computed: {
        get_filter_range(){
            let tgl_awal = moment(this.filter_range[0]).format('YYYY-MM-DD');
            let tgl_akhir = moment(this.filter_range[1]).format('YYYY-MM-DD');
            return [tgl_awal, tgl_akhir];
        }
    },
    created() {
        VM_PAGE_HEADER.title = '<i class="fa fa-reorder"></i> Pembayaran Cicilan';
        VM_PAGE_HEADER.action = `<a href="${this.global_url.page.sales.cicilan.new_pembayaran}" class="btn btn-default btn-labeled"><b><i class="icon-file-plus"></i></b> Pembayaran</button>`;
    },
    mounted() {
        this.datatable = $('#myTable').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: this.global_url.api.sales.cicilan.list_pembayaran_dt,
                type: "POST",
                data: (data) => {
                    data.filter_range = this.get_filter_range;
                    global.method.vueBlockElement('#myTable');
                }
            },
            searchDelay: 600,
            processing: true,
            columns: [
                {data: 'tanggal', name: 'tanggal', render: (data, type, row) => {
                    let proc = moment(data).format('DD/MM/YYYY HH:mm:ss');
                    return proc;
                }},
                {data: 'no_member', name: 'no_member', render: (data, type, row) => {
                    
                    return data;
                }},
                {data: 'no_kuitansi', name: 'no_kuitansi', render: (data, type, row) => {
                    
                    return data;
                }},
                {data: 'nama_dokter', name: 'nama_dokter', render: (data, type, row) => {
                    return `<div class="info-detail">
                        <span class="dokter_info">${data}</span>
                        <span class="sales_info">Sales : ${row.nama_sales}</span>
                    </div>`;
                }},
                {data: 'no_invoice', name: 'no_invoice', render: (data, type, row) => {
                    
                    return data;
                }},
            ],
            drawCallback: (settings) => {
                $('#myTable').unblock();
            },
            aaSorting: [],
        });
    },
});

$(() => {
    let event_name = 'list-transaksi-event';
    let uid = global.method.Base64_encode("t_pos_pembayaran");
    let url = `${provider.url.api.event.subscribe.stream}?uid=${uid}&event_name=${event_name}`;
    let callback = function(data) {
        VM_LIST_TRANSAKSI.datatable.draw();
    };
    global.method.listenEvent(url,event_name, callback);
});
