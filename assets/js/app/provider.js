var provider = new function(){
  this.baseUrl = $('meta[name="base_url"]').attr('content');
  this.url = {
    'page' : {
      'master' : {
        'sales' : {
          'index' : `${this.baseUrl}master/sales`,
          'form' : `${this.baseUrl}master/sales/form?uid=:uid`,
          'detail' : `${this.baseUrl}master/sales/detail?uid=:uid`,
        },
        'manager_sales' : {
          'index' : `${this.baseUrl}master/manager_sales`,
          'form' : `${this.baseUrl}master/manager_sales/form?uid=:uid`,
          'detail' : `${this.baseUrl}master/manager_sales/detail?uid=:uid`,
        },
        'dokter' : {
          'index' : `${this.baseUrl}master/dokter`,
          'form' : `${this.baseUrl}master/dokter/form?uid=:uid`,
        },
      },
      'sales' : {
        'pos' : {
          'index' :  `${this.baseUrl}sales/pos/transaksi`,
          'detail_transaksi' : `${this.baseUrl}sales/pos/detail_transaksi`,
        },
        'pengalihan_invoice' : {
          'index' :  `${this.baseUrl}sales/pengalihan_invoice/daftar_invoice`,
          'detail_transaksi' : `${this.baseUrl}sales/pengalihan_invoice/detail_transaksi`,
        },
        'cicilan' : {
          'index' :  `${this.baseUrl}sales/cicilan/list_pembayaran`,
          'new_pembayaran' :  `${this.baseUrl}sales/cicilan/new_pembayaran`,
        },
        'insentif' : {
          'index' :  `${this.baseUrl}sales/insentif`,
        },
      },
    },
    'api' : {
      'event' : {
        'subscribe' : {
          'stream' : `${this.baseUrl}api/event/subscribe/stream`,
        },
      },
      'master' : {
        'sales' : {
          'list_dt': `${this.baseUrl}api/master/sales/list_dt`,
          'list_dt_transaksi': `${this.baseUrl}api/master/sales/list_dt_transaksi`,
          'switch_status': `${this.baseUrl}api/master/sales/switch_status?uid=:uid`,
          'store': `${this.baseUrl}api/master/sales/store`,
          'update': `${this.baseUrl}api/master/sales/update`,
          'show': `${this.baseUrl}api/master/sales/show?uid=:uid`,
        },
        'manager_sales' : {
          'list_dt': `${this.baseUrl}api/master/manager_sales/list_dt`,
          'switch_status': `${this.baseUrl}api/master/manager_sales/switch_status?uid=:uid`,
          'store': `${this.baseUrl}api/master/manager_sales/store`,
          'update': `${this.baseUrl}api/master/manager_sales/update`,
          'show': `${this.baseUrl}api/master/manager_sales/show?uid=:uid`,
        },
        'dokter' : {
          'list_dt': `${this.baseUrl}api/master/dokter/list_dt`,
          'switch_status': `${this.baseUrl}api/master/dokter/switch_status?uid=:uid`,
          'store': `${this.baseUrl}api/master/dokter/store`,
          'update': `${this.baseUrl}api/master/dokter/update`,
          'show': `${this.baseUrl}api/master/dokter/show?uid=:uid`,
        },
        'bank' : {
          'list_data': `${this.baseUrl}api/master/bank/list_data`,
        },
        'mesin_edc' : {
          'list_data': `${this.baseUrl}api/master/mesin_edc/list_data`,
        },
      },
      'sales' : {
        'pos' : {
          'get_barang' : `${this.baseUrl}api/sales/pos/get_all_barang?page=:page`, 
          'get_member' : `${this.baseUrl}api/sales/pos/get_member`, 
          'search_member' : `${this.baseUrl}api/sales/pos/search_member?q=:q`, 
          'save_temporary' : `${this.baseUrl}api/sales/pos/save_temporary`, 
          'proccess_order' : `${this.baseUrl}api/sales/pos/proccess_order`, 
          'update_proccess_order' : `${this.baseUrl}api/sales/pos/update_proccess_order`, 
          'list_member_select2' : `${this.baseUrl}api/sales/pos/list_member_select2`, 
          'list_transaksi_dt' : `${this.baseUrl}api/sales/pos/list_transaksi_dt`, 
          'list_approval_dt' : `${this.baseUrl}api/sales/pos/list_approval_dt`, 
          'approve_pos_manager' : `${this.baseUrl}api/sales/pos/approve_pos_manager`, 
          'approve_diskon_manager' : `${this.baseUrl}api/sales/pos/approve_diskon_manager`, 
          'approve_diskon_direktur' : `${this.baseUrl}api/sales/pos/approve_diskon_direktur`, 
          'reject_pos_manager' : `${this.baseUrl}api/sales/pos/reject_pos_manager`, 
          'reject_diskon_manager' : `${this.baseUrl}api/sales/pos/reject_diskon_manager`, 
          'reject_diskon_direktur' : `${this.baseUrl}api/sales/pos/reject_diskon_direktur`, 
          'rejected_description' : `${this.baseUrl}api/sales/pos/rejected_description`, 
          'get_detail_pos' : `${this.baseUrl}api/sales/pos/get_detail_pos`, 
          'print_invoice' : `${this.baseUrl}api/sales/pos/print_invoice`, 
          'print_tanda_terima' : `${this.baseUrl}api/sales/pos/print_tanda_terima`, 
          'print_kuitansi' : `${this.baseUrl}api/sales/pos/print_kwitansi`, 
        },
        'pengalihan_invoice' : {
          'list_transaksi_dt' : `${this.baseUrl}api/sales/pengalihan_invoice/list_transaksi_dt`, 
          'proccess_order' : `${this.baseUrl}api/sales/pengalihan_invoice/proccess_order`, 
          'update_proccess_order' : `${this.baseUrl}api/sales/pengalihan_invoice/update_proccess_order`,
          'list_approval_dt' : `${this.baseUrl}api/sales/pengalihan_invoice/list_approval_dt`,
          'approve_pengalihan' : `${this.baseUrl}api/sales/pengalihan_invoice/approve_pengalihan`, 
          'reject_pengalihan' : `${this.baseUrl}api/sales/pengalihan_invoice/reject_pengalihan`, 
        },
        'cicilan' : {
          'list_pembayaran_dt' : `${this.baseUrl}api/sales/cicilan/list_pembayaran_dt`, 
          'list_invoice_select2' : `${this.baseUrl}api/sales/cicilan/list_invoice_select2`,
          'get_cicilan_ke' : `${this.baseUrl}api/sales/cicilan/get_cicilan_ke`,
          'pembayaran_cicilan' : `${this.baseUrl}api/sales/cicilan/pembayaran_cicilan`,
        },
      }
    },
    
  };

  this.service = {
    api: {
      master: {
        sales: {
          switch_status: (uid) => {
            return $.get(provider.url.api.master.sales.switch_status.replace(':uid',uid))
          },
          store: (formData) => {
            return $.ajax({
              url: provider.url.api.master.sales.store,
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
          update: (formData) => {
            return $.ajax({
              url: provider.url.api.master.sales.update,
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
          show: (formData,uid) => {
            return $.ajax({
              url: provider.url.api.master.sales.show.replace('uid',uid),
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
        },
        manager_sales: {
          switch_status: (uid) => {
            return $.get(provider.url.api.master.manager_sales.switch_status.replace(':uid',uid))
          },
          store: (formData) => {
            return $.ajax({
              url: provider.url.api.master.manager_sales.store,
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
          update: (formData) => {
            return $.ajax({
              url: provider.url.api.master.manager_sales.update,
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
          show: (formData,uid) => {
            return $.ajax({
              url: provider.url.api.master.manager_sales.show.replace('uid',uid),
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
        },
        dokter: {
          switch_status: (uid) => {
            return $.get(provider.url.api.master.dokter.switch_status.replace(':uid',uid))
          },
          store: (formData) => {
            return $.ajax({
              url: provider.url.api.master.dokter.store,
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
          update: (formData) => {
            return $.ajax({
              url: provider.url.api.master.dokter.update,
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
          show: (formData,uid) => {
            return $.ajax({
              url: provider.url.api.master.dokter.show.replace('uid',uid),
              data: formData,
              processData: false,
              contentType: false,
              type: 'POST',
            })
          },
        }
      }
    }
  }
}

/**
 * @author Ari Ardiansyah
 * Insert Global Variables to Vue
 * */ 
Vue.prototype.global_url = provider.url;
Vue.prototype.base_url = provider.baseUrl;
