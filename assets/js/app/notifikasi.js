let fillNotif = ()=> {
        $.getJSON(loadNotifikasi.replace(':ROLE_ID', ROLE_ID), function(res, status) {
            if(status === 'success') {
                let data = res;
                if (data) {
                    $('body').find('#badge_mananger').html(data.badge).fadeIn('200'); 
                    if (data.badge <= 0) {
                        $('body').find('#badge_mananger').html(data.badge).hide(); 

                    }


                    let html = "";

                    for (var i = 0; i < data.list.length ; i++) {
                        html += `<li class="media">
                                    <a data-href="${base_url}/${data.list[i].link}" data-id="${data.list[i].id}" class="updateNotif media-heading">
                                        <div class="media-body">
                                                <span class="text-muted">${data.list[i].description}</span>
                                        </div>
                                    </a>
                                </li>`;

                        //data.list[i]
                        $('.section_notif').html(html);
                    }
                }
            }
        });
    }

let listen_notif = (event) => {
    eventSource = new EventSource(listenNotifikasi.replace(':EVENT', event));
    eventSource.addEventListener(event, function(e) {
       fillNotif();
    }, false);
}

$('body').find('#badge_click').click(function(){
    $('body').find('#badge_mananger').fadeOut('200'); 
});

$('body').on('click', '.updateNotif', function(){
    let link = $(this).data('href'),
        url = base_url+'/api/event/notifikasi/updateNotif',
        id = $(this).data('id');
        //updateStatus
        $.ajax({
            type: "POST",
            url: url,
            data: {'id' : id},
            dataType: 'JSON',

            success:function(data) {
                console.log(data)
              window.location.assign(link);
            }
      });
});

fillNotif();
listen_notif("layar_notifikasi");